#!/usr/bin/perl
use warnings;
use strict;

use DBIx::Class::Schema::Loader qw(make_schema_at);

make_schema_at(
    'Zaaksysteem::Schema', 
    {
        debug                   => 0,
	use_namespaces          => 0,
	# Enabling this requires some rewrites in Zaaksysteem, stripping most _id calls.
	#naming                  => 'v4',
        overwrite_modifications => 0,
        dump_directory          => './lib',
        components              => [
            "InflateColumn::DateTime", 
            "TimeStamp"
        ],
    }, 
    [
        'dbi:Pg:dbname="zs_test"', 'vagrant', '',
    ],
);


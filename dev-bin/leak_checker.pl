#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;
use Test::WWW::Mechanize::Catalyst;
use lib 'lib';

my ( $APP, $AUTHURL, $URL, $REQUEST_COUNT, $leaks );

sub BEGIN {
    $APP           = 'Zaaksysteem';
    $AUTHURL       = '/auth/login';
    $URL           = $ARGV[0] || '/auth/login';
    $REQUEST_COUNT = $ARGV[1] || 10;
    $leaks         = 0;

    eval "use Devel::LeakGuard::Object qw(leakguard)";
    my $leakguard = ! $@;
    eval "use Catalyst::Test '$APP'";
    my $catalyst_test = ! $@;

    plan $leakguard && $catalyst_test
    ? ( tests => 2 )
    : ( skip_all => 
        'Devel::LeakGuard::Object and Catalyst::Test
        are needed for this test' ) ;
}

my $m = Test::WWW::Mechanize::Catalyst->new( catalyst_app => 'Zaaksysteem');
$m->post_ok('http://10.44.0.11' . $AUTHURL, { username => 'admin', 'password' => 'admin' });

use Memory::Usage;

my $mu = Memory::Usage->new();

### Run once to get a status quo
$m->get_ok('http://10.44.0.11/' . $URL);

$mu->record('before');


leakguard {
    #$m->get_ok('http://10.44.0.11/beheer/bibliotheek') for 1 .. $REQUEST_COUNT;
    #$m->get_ok('http://10.44.0.11/search/results') for 1 .. $REQUEST_COUNT;
    #$m->get_ok('http://10.44.0.11/') for 1 .. $REQUEST_COUNT;

    ### Fixed:
    #$m->get_ok('http://10.44.0.11/beheer/bibliotheek/2') for 1 .. $REQUEST_COUNT;
    #$m->get_ok('http://10.44.0.11/') for 1 .. $REQUEST_COUNT;
    
    ### Does not look good:
    #$m->get_ok('http://10.44.0.11/zaak/146') for 1 .. $REQUEST_COUNT;



    $m->get_ok('http://10.44.0.11/' . $URL) for 1 .. $REQUEST_COUNT;
};


$mu->record('after');


$mu->dump;

# $m->get_ok('http://10.44.0.11/' . $URL);

# {
#     use Memory::Usage;

#     my $mu = Memory::Usage->new();

#     $mu->record('before');

#     $m->get_ok('http://10.44.0.11/' . $URL) for 1 .. $REQUEST_COUNT;

#     $mu->record('after');
#     $mu->dump;
# }



#on_leak => sub {
#    my $report = shift;
#
#    print "We got some memory leaks: \n";
#
#    for my $pkg ( sort keys %$report ) {
#        printf "%s %d %d\n", $pkg, @{ $report->{$pkg} };
#    }
#    $leaks++;
#};
#

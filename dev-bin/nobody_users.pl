#!/usr/bin/perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI;
use Zaaksysteem::Constants qw(SUBJECT_TYPE_EMPLOYEE);

my $cli = Zaaksysteem::CLI->init;

my %opt = %{ $cli->options };

$cli->do_transaction(
    sub {
        my $self   = shift;
        my $schema = shift;

        my $nobody = $schema->resultset('Subject')->create_nobody_user(
            username     => $opt{username}     // 'nobody',
            subject_type => $opt{subject_type} // SUBJECT_TYPE_EMPLOYEE,
            system_user  => $opt{system_user} ? 1 : 0,
        );
    }
);

1;

__END__

=head1 NAME

nobody_users.pl - Create nobody users

=head1 SYNOPSIS

nobody_users.pl OPTIONS [ -o username=nobody ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over 8

=item * username

The username of the nobody user, defaults to nobody.

=item * system_user

Booleanish value. Set the user as system user, defaults to false

=item * subject_type

Set the type of user, defaults to 'employee', the only supported type at this moment.

=back

=over

=item * n

Run it and do not commit the changes, in other words a dry run.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

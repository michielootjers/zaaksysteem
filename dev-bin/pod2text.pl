#!/usr/bin/perl
use warnings;
use strict;

use Pod::Text;

my $parser = Pod::Text->new(sentence => 0, width => 78);
foreach (qw(LICENSE CONTRIBUTORS)) { 
    $parser->parse_from_file("lib/Zaaksysteem/$_.pod", $_);
}

#!/bin/bash

if [ $# -lt "1" ]; then
    echo "USAGE: $0 databasename [database.sql]"
    echo
    echo "Imports db/test-template.sql (or optionally database.sql) to databasename"
    exit;
fi

if [ ! -d db ] || [ ! -d lib ]; then
    echo "Please run this script from your source tree root"
    exit;
fi

DATABASE=$1;
TEMPLATE=$2;
DBFILLED=`dev-bin/is_database_loaded.sh $DATABASE`;

if [ "$#" -lt 2 ]; then TEMPLATE="db/test-template.sql"; fi

if [ -n "$DBFILLED" ]; then
    exit;
fi

psql $DATABASE < $TEMPLATE;

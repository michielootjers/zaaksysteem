#!/bin/bash

export PERL5LIB=./lib
if [ -e .mytest ] || [ -e .mycatalist ] ; then
    prove t/9*.t
fi

HARNESS_PERL_SWITCHES=-MDevel::Cover=+select,^lib/,+select,^t/inc/.*\.pm,+ignore,^t/.+\.t prove $@
unset PERL5LIB

cover

base=cover_db
report=$base/coverage.html

if [ -e "$report" ] ; then
    mv $report $base/index.html
    chmod 755 $base
fi
rm -f *.err

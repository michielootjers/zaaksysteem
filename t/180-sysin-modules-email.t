#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;

### Test header end

use_ok('Zaaksysteem::Backend::Sysin::Modules::Email');
use_ok('Zaaksysteem::Backend::Mailer');


sub mock_email_sender {
    my $mock = Test::MockObject->new();
    $mock->mock('send', sub {
        my $self = shift;
        diag "sending_email:", explain \@_;
        return "message";
    });
    return $mock;
}


sub create_interface {
    return $zs->create_named_interface_ok({
        module => 'email',
        name   => 'email',
    });
}


sub get_email_module {
    my $email_sender = mock_email_sender;
    my $module = Zaaksysteem::Backend::Sysin::Modules::Email->new;
    $module->email_sender($email_sender);
    return $module;
}


$zs->zs_transaction_ok(sub {

    my $module = get_email_module;

    my $record = $zs->create_transaction_record_ok;
    my $case = $zs->create_case_ok;
    my $notificatie = $zs->create_notificatie_ok;

    $case->aanvrager_object->email('jw@mintlab.nl');

    my $message = $module->_process_mail($record, {
        body => 'test body',
        notification => {
            id                          => $notificatie->id,
            recipient_type              => 'aanvrager',
            behandelaar                 => 'betrokkene-medewerker-1',
            email                       => 'jw@mintlab.nl',
            case_id                     => $case->id,
        },
        case_id                     => $case->id,
    });

    ok $message =~ m|^Zaak ID|, "Beginning of message looks good";

}, 'Tested: Interface/Modules/Email');


zs_done_testing();

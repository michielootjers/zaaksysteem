#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


$zs->zs_transaction_ok(sub {
    my $test_dir = 'Test Directory';

    my $rs = $schema->resultset('Directory');
    my $case = $zs->create_case_ok;

    my $invalidNames = [
        '!@#:$%^&*()ffdfdfdfdf', 
        '!#@#$t:est ',
        'lpt1'
    ];

    push @$invalidNames, qw/com1 com2 com3 com4 com5 com6 com7 com8 com9 lpt1 lpt2 lpt3 lpt4 lpt5 lpt6 lpt7 lpt8 lpt9 con nul prn/;

    foreach my $invalidName (@$invalidNames) {

        my $validified = $rs->validify({
            case_id  => $case->id,
            name     => $invalidName
        });

        ok ($validified ne $invalidName, "Directory $invalidName should not be valid");
    }

    my $validNames = [
        'ffdfdfdfdf', 
        'testddd',
        'testddd',
        'testddd',
        'dfdffd(1)',
        '0349039340fjsdkfjhsdoifj2409fosidflj24-09fjpsdflkj24-9efjpodsflj2-09efjodslifjf249-fjoldfksjdf-2490fjldskfj9-'
    ];

    foreach my $name (@$validNames) {

        my $validified = $rs->validify({
            case_id  => $case->id,
            name     => $name,
        });

        if ($validified ne $name) {

            my $secondvalidified = $rs->validify({
                case_id  => $case->id,
                name     => $validified,
            });

            ok($validified eq $secondvalidified, "Directory name $name made valid - $validified, $secondvalidified");
        }

        my $result = $rs->directory_create({
            case_id  => $case->id,
            name     => $validified,
        });

        ok $result, "Created directory $validified";
        is $result->case_id->id, $case->id, "Directory case is the correct case";
    }
}, 'directory_create()');


$zs->zs_transaction_ok(sub {
    my $test_dir = 'Test Directory';

    my $case = $zs->create_case_ok;

    my $result = $schema->resultset('Directory')->directory_create({
        case_id  => $case->id,
        name     => $test_dir,
    });

    ok $result, "Created directory $result";
    is $result->name, $test_dir, "Directory name is $test_dir";
    is $result->case->id, $case->id, "Directory case is the correct case";
}, 'directory_create()');


$zs->zs_transaction_ok(sub {
    my $test_dir  = 'Test Directory';
    my $directory = $zs->create_directory_ok;
    
    throws_ok sub {
        $schema->resultset('Directory')->directory_create({
            case_id => $directory->case->id,
            name    => $directory->name,
        });
    }, qr/Found existing/, "Duplicate directory names within same case fails";
}, 'directory_create() duplicate entry');


$zs->zs_transaction_ok(sub {
    my $case      = $zs->create_case_ok;
    my $directory = $zs->create_directory_ok;
    my $rename    = 'Renamed Directory';

    my $result = $directory->update_properties({
        name    => $rename,
        case_id => $case->id,
    });
    ok $result, 'Updated directory';
    is $result->name, $rename, 'Directory is renamed';
    is $result->case->id, $case->id, 'New case ID set';
}, 'update_properties rename + assign case');


$zs->zs_transaction_ok(sub {
    my $case      = $zs->create_case_ok;
    my $directory = $zs->create_directory_ok;
    my $rename    = 'Renamed Directory';

    my $result = $directory->update_properties({
        name    => $rename,
        case_id => $case->id,
    });

    my $second_directory = $zs->create_directory_ok;

    dies_ok sub {
        $directory->update_properties({
            name    => $rename,
            case_id => $case->id,
        });
    }, 'Making duplicate directories dies';

}, 'update_properties existing name(+case) fails');



$zs->zs_transaction_ok(sub {
    my $directory = $zs->create_directory_ok;
    my $result    = $directory->delete;
    ok $result, 'Deleted directory';
}, 'delete()');


$zs->zs_transaction_ok(sub {
    my $directory = $zs->create_directory_ok;
    my $file      = $zs->create_file_ok;
    ok $file->update({directory => $directory}), 'Set directory on file';
    throws_ok(sub {
        $directory->delete;
    }, qr/There are still files/, 'delete() failed');
}, 'delete() on non-empty directory');


$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('Directory')->directory_create()
    }, qr/invalid input/, 'directory_create() noargs';
    throws_ok sub {
        $zs->create_directory_ok->update_properties();
    }, qr/invalid input/, 'update_properties() noargs';
}, 'noargs calls');

zs_done_testing();

#! perl

### Test header start
#use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Backend::Tools::WorkingDays qw/add_working_days/;
use DateTime;

### Test header end


$zs->zs_transaction_ok(sub {

    my $datetime = DateTime->new({year => 2013, month => 12, day => 10});

    my $result = add_working_days({
        datetime => $datetime, 
        working_days => 0
    });
    #diag "0: " . $result;

    ok $result->strftime('%F') eq '2013-12-10';

    $result = add_working_days({
        datetime => $datetime,
        working_days => -20
    });

    #diag "-20: " . $result;
    ok $result->strftime('%F') eq '2013-11-12';

    # 876 days should cover it
    $result = add_working_days({
        datetime => $datetime,
        working_days => 876
    });

    #diag "+876: " . $result;
    ok $result->strftime('%F') eq '2017-05-24';


}, 'Validation tests');




zs_done_testing();
#!perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Test::Moose;

use Zaaksysteem::Backend::Object::Attribute;

{
    my $attr = Zaaksysteem::Backend::Object::Attribute->new(
        name           => 'foobar',
        value          => {
            bag_id => 'openbareruimte-1234567',
            human_identifier => 'Test Openbareruimte',
        },
        attribute_type => 'bag',
    );

    does_ok($attr, 'Zaaksysteem::Backend::Object::Attribute::BAG', 'New attribute');

    is($attr->human_value, 'Test Openbareruimte',    "Human-readable value is correct");
    is($attr->index_value, 'openbareruimte-1234567', "Indexable value is correct");
}

zs_done_testing;

#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing');



# Unable to test, because of betrokkene
# $zs->zs_transaction_ok(sub {
#     my $interface = $schema ->resultset('Interface')
#             ->interface_create(
#                 {
#                     module              => 'key2burgerzakenverhuizing',
#                     name                => 'Key2Burgerzaken Verhuizing',
#                     active              => 1,
#                     interface_config    => {
#                         endpoint        => 'http://localhost/',
#                         action_bvh07    => 'early_close',
#                         attribute_mapping  => [
#                             {
#                                 internal_name   => 'Verhuisdatum',
#                                 external_name   => 'test_01'
#                             },
#                             {
#                                 internal_name   => 'StatusCode',
#                                 external_name   => 'test_08'
#                             }
#                         ]
#                     },
#                 },                                
#             );

#     my $transaction = $interface->process_trigger(
#         'request_verhuizing',
#         {
#             case    => $zs->create_case_ok(),
#         }
#     );

#     note(explain($transaction));

# }, 'Tested: Verhuizing Request');


zs_done_testing();

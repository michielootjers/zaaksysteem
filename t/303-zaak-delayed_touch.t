#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use_ok('Zaaksysteem::Zaken::DelayedTouch');

# add a non-existing case to the touch list, make sure the toucher
# will ignore it
# when an action performed on multiple cases
$zs->zs_transaction_ok(sub {
    #$schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

    my $touch_object = new_ok('Zaaksysteem::Zaken::DelayedTouch');

    my $non_existent = $schema->resultset('Zaak')->search->get_column('id')->max + 1;
    ok !$schema->resultset('Zaak')->find($non_existent), "No case found with non existent id";

    $touch_object->add_case($non_existent);

    lives_ok( sub {
        $touch_object->execute($schema);
    }, "No die when executing on a non-existent case");

}, 'Case: no die when non-existing case in touch list.');


$zs->zs_transaction_ok(sub {
    ### Prepare case by setting a new date
    ###
    my $touch_object = new_ok('Zaaksysteem::Zaken::DelayedTouch');

    is($touch_object->add_case(4434), 1, 'Added case to touch object');
    is($touch_object->add_case(434), 2, 'Add second case to touch object');
    is($touch_object->add_case(434), 2, 'Prevent duplicate cases');


    throws_ok(
        sub {
            $touch_object->execute;
        },
        qr/Invalid schema given/,
        'Check validation of DB::Schema'
    );
}, 'finised Z::Zaken::DelayedTouch tests');

$zs->zs_transaction_ok(sub {
    ### Prepare case by setting a new date
    ###
    my $case            = $zs->create_case_ok;

    my $last_modified   = $case->last_modified;
    ok(sleep(1), 'Slept for a second to prevent timing issues');

    ok($case->touch, 'Touched case');

    ok(($case->last_modified > $last_modified), 'last_modified newer after touch');

}, 'Case: touched the direct way.');

$zs->zs_transaction_ok(sub {
    ### Prepare case by setting a new date
    ###
    $schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

    my $case    = $zs->create_case_ok;

    my $last_modified   = $case->last_modified;
    ok(sleep(1), 'Slept for a second to prevent timing issues');

    ok($case->touch, 'Touched case');

    ok(!($case->last_modified > $last_modified), 'last_modified not changed after direct touch');

    ok(
        $schema->default_resultset_attributes->{delayed_touch}->execute($schema),
        'Executed delayed touch'
    );

    $case    = $zs->create_case_ok;

    ok(($case->last_modified > $last_modified), 'last_modified changed after delayed touch');
}, 'Case: touched the delayed way.');

$zs->zs_transaction_ok(sub {
    ### Prepare case by setting a new date
    ###
    $schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

    my $case    = $zs->create_case_ok;

    ok($case->touch, 'Touched case');

    ok(
        $schema->default_resultset_attributes->{delayed_touch}->execute($schema),
        'Executed delayed touch'
    );

    ok($case->object_data, 'Got object_data record');

    #note(explain($case->object_data->object_attributes));

    is($case->object_data->object_class, 'case', 'object_data: Got correct object_class');
    is($case->object_data->object_id, $case->id, 'object_data: Got correct object_id');

    ok($case->object_data->index_hstore, 'object_data: Got correct hstore index');

    note(explain($case->object_data->properties));
}, 'Case: check link with object_data.');



zs_done_testing();
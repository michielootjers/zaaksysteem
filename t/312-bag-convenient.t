#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use File::Basename;

$ENV{DISABLE_ZAAKSYSTEEM_GEO_CODE} = 1;

$zs->zs_transaction_ok(sub {
    $zs->create_bag_records;

    my $record = $schema->resultset('BagNummeraanduiding')->get_record_by_source_identifier(
        'nummeraanduiding-9876543218375842'
    );

    is ($record->identificatie, '9876543218375842',
        'found belonging record nummeraanduiding'
    );

    $record = $schema->resultset('BagNummeraanduiding')->get_record_by_source_identifier(
        'openbareruimte-1234567890123456'
    );

    is ($record->identificatie, '1234567890123456',
        'found belonging record openbareruimte'
    );

    $record = $schema->resultset('BagNummeraanduiding')->get_record_by_source_identifier(
        'woonplaats-9001'
    );

    is ($record->identificatie, '9001',
        'found belonging record woonplaats'
    );
}, 'bag: get_record_by_source_identifier find objects');


$zs->zs_transaction_ok(sub {
    $zs->create_bag_records;

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_address_data_by_source_identifier(
                    'nummeraanduiding-9876543218375842'
                );

    is (
        grep (
            { defined($record->{ $_ }) && $record->{ $_ } }
            qw/
                huisletter
                huisnummer
                huisnummertoevoeging
                postcode
                straat
                woonplaats
            /
        ),
        6,
        'Got complete nummeraanduiding'
    );

    note(explain($record));
}, 'bag: get_address_data_by_source_identifier by nummeraanduiding');


$zs->zs_transaction_ok(sub {
    $zs->create_bag_records;

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_address_data_by_source_identifier(
                    'openbareruimte-1234567890123456'
                );

    is (
        grep (
            { defined($record->{ $_ }) && $record->{ $_ } }
            qw/
                huisletter
                huisnummer
                huisnummertoevoeging
                postcode
                straat
                woonplaats
            /
        ),
        2,
        'Got complete openbareruimte'
    );
}, 'bag: get_address_data_by_source_identifier by openbareruimte');


$zs->zs_transaction_ok(sub {
    $zs->create_bag_records;

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_human_identifier_by_source_identifier(
                    'nummeraanduiding-9876543218375842'
                );

    is (
        $record,
        'Donker Curtiusstraat 23A-1rec',
        'Got human identifier'
    );
}, 'bag: get_human_identifier_by_source_identifier by nummeraanduiding');


$zs->zs_transaction_ok(sub {
    $zs->create_bag_records;

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_human_identifier_by_source_identifier(
                    'nummeraanduiding-9876543218375842',
                    {
                        prefix_with_city => 1
                    }
                );

    is (
        $record,
        'Amsterdam - Donker Curtiusstraat 23A-1rec',
        'Got human identifier'
    );
}, 'bag: get_human_identifier_by_source_identifier by nummeraanduiding with city');

zs_done_testing();

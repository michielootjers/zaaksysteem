#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Constants qw(
  SJABLONEN_TARGET_FORMATS
  ZAAKTYPE_NODE_TITLE
  ZAAK_CREATE_PROFILE_BETROKKENE
  PROFILE_BAG_TYPES_OK
  BAG_TYPES
);

{
    note("SJABLONEN_TARGET_FORMATS tests");
    my %tests = (
        pdf => 1,
        odt => 1,
        jpg => 0,
    );

    foreach my $format (sort keys %tests) {
        my $res = SJABLONEN_TARGET_FORMATS->($format);
        is(
            $res,
            $tests{$format},
            sprintf(
                "%s is a%s %scorrect target format",
                $format,
                $tests{$format} ? '' : 'n',
                $tests{$format} ? '' : 'in'
            ));
    }
}

{
    note("ZAAKTYPE_NODE_TITLE tests");
    my %tests = (
        'ditisgoed'                    => 1,
        'This is good 2 _'             => 1,
        ''                             => 0,
        '`~!@#$%^&*()-+=?><,.;:\'\\|"' => 0,
    );

    foreach my $test (sort keys %tests) {
        my $ok = $test =~ ZAAKTYPE_NODE_TITLE;
        is(!$ok, !$tests{$test}, "Test '$test' is correct");
    }
}

{
    note("ZAAK_CREATE_PROFILE_BETROKKENE tests");

    my %tests = (
        blessed => {
            betrokkene => bless({}, "Betrokkene"),
            rc         => 1
        },
        hash => {
            betrokkene => {
                betrokkene      => 1,
                create          => 1,
                betrokkene_type => 1,
                betrokkene_id   => 1,
                verificatie     => 1
            },
            rc => 1
        },
        no_verification => {
            betrokkene => {
                betrokkene      => 1,
                create          => 1,
                betrokkene_type => 1,
                betrokkene_id   => 1,
                verificatie     => 0,
            },
            rc => 0
        },
        create_has_no_type => {
            betrokkene => {
                betrokkene      => 0,
                create          => 1,
                betrokkene_id   => 1,
                betrokkene_type => 0
            },
            rc => 0
        },
        no_betrokkene_with_id_type => {
            betrokkene => {
                betrokkene      => 0,
                create          => 1,
                betrokkene_id   => 1,
                betrokkene_type => 1,
                verificatie     => 1,
            },
            rc => 1
        },
        no_betrokkene_type_no_id => {
            betrokkene => {
                betrokkene      => 0,
                create          => 1,
                betrokkene_id   => 0,
                betrokkene_type => 1,
                verificatie     => 1,
            },
            rc => 1
        },
        no_create_type_no_id => {
            betrokkene => {
                betrokkene      => 1,
                create          => 0,
                betrokkene_id   => 0,
                betrokkene_type => 1,
                verificatie     => 1,
            },
            rc => 1
        },

        array_ok => {
            betrokkene => [
                bless({}, "Betrokkene"), {
                    betrokkene      => 1,
                    create          => 1,
                    betrokkene_type => 1,
                    betrokkene_id   => 1,
                    verificatie     => 1
                },
            ],
            rc => 1
        },
        scalar => {
            betrokkene => "1",
            rc => 0
        },
    );

    for my $key (keys %tests) {
        my $ok = ZAAK_CREATE_PROFILE_BETROKKENE->($tests{$key}{betrokkene});
        is($ok, $tests{$key}{rc}, "$key: Correct return code");
    }
}

{
    note("BAG");
    my @bag_types = @{BAG_TYPES()};
    foreach (@bag_types) {
        ok(PROFILE_BAG_TYPES_OK->($_), "BAG type $_ is correct");
    }
    foreach (qw(invalid meuk)) {
        ok(!PROFILE_BAG_TYPES_OK->($_), "BAG type $_ is not correct");
    }
}

zs_done_testing();

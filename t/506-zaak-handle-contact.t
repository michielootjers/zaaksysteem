#! /usr/bin/perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Controller::Zaak;

sub handle_contact {
	return Zaaksysteem::Controller::Zaak::_create_zaak_handle_contact({}, @_);
}

$zs->zs_transaction_ok(sub {

	my $c = Test::MockObject->new;

	my $zaak = $zs->create_case_ok;
	my $stash = { zaak => $zaak };
	$c->mock('stash', sub { $stash });

	is handle_contact($c), undef, 'Returns undef if called without session';

	my $email = 'jw@mintlab.nl';
	handle_contact($c, {'npc-email' => $email});
	is $zaak->aanvrager_object->email, $email, 'Correct email set';

	handle_contact($c, {'npc-email' => ''});
	is $zaak->aanvrager_object->email, '', 'Email emptied';


	$email = 'jw2@mintlab.nl';
	handle_contact($c, {'npc-email' => $email});
	is $zaak->aanvrager_object->email, $email, '2nd correct email set';

	my $incorrect_email = 'incorrect_mintlab.nl';
	handle_contact($c, {'npc-email' => $incorrect_email});
	isnt $zaak->aanvrager_object->email, $incorrect_email, 'Incorrect email not set';
	is $zaak->aanvrager_object->email, $email, 'Previous email retained';


	my $mobiel = '12345678';
	handle_contact($c, {'npc-mobiel' => $mobiel});
	is $zaak->aanvrager_object->mobiel, $mobiel, 'Correct mobiel set';

	my $bad_mobiel = 'aaaa'; # haha batmobile
	handle_contact($c, {'npc-mobiel' => $bad_mobiel});
	isnt $zaak->aanvrager_object->mobiel, $bad_mobiel, 'Incorrect mobiel not set';
	is $zaak->aanvrager_object->mobiel, $mobiel, 'Previous mobiel retained';


	my $telefoonnummer = '12345678';
	handle_contact($c, {'npc-telefoonnummer' => $telefoonnummer});
	is $zaak->aanvrager_object->telefoonnummer, $telefoonnummer, 'Correct telefoon set';

	my $bad_telefoonnummer = 'aaaa';
	handle_contact($c, {'npc-telefoonnummer' => $bad_telefoonnummer});
	isnt $zaak->aanvrager_object->telefoonnummer, $bad_telefoonnummer, 'Incorrect telefoon not set';

	handle_contact($c, {'bla' => 'something unrelated'});
	is $zaak->aanvrager_object->email, $email, 'Previous email still retained';
	is $zaak->aanvrager_object->telefoonnummer, $telefoonnummer, 'Previous telefoonnummer still retained';
	is $zaak->aanvrager_object->mobiel, $mobiel, 'Previous mobiel still retained';


}, "Controller/Zaak/_create_zaak_handle_contact");

zs_done_testing;

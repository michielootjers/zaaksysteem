#! perl
use warnings;
use strict;

# Test header
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Test::Moose;
use_ok('Zaaksysteem::Zaaktypen::Import');

# /Test header


sub get_importer_ok {
    my $importer = Zaaksysteem::Zaaktypen::Import->new(dbic => $schema);

    isa_ok $importer, 'Zaaksysteem::Zaaktypen::Import', "Got the right class";

    return $importer;
}


$zs->zs_transaction_ok(sub {
    my $importer = get_importer_ok;
    my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(value_type => 'select');

    my $merged = $importer->merge_local_options({
        bibliotheek_kenmerken_id => $kenmerk->id,
        remote_options => []
    });
    is scalar @$merged, 0, "No options put in, result is empty list";


    $merged = $importer->merge_local_options({
        bibliotheek_kenmerken_id => $kenmerk->id,
        remote_options => [{ value => 'boter'}]
    });
    is scalar @$merged, 1, "One remote option put in, result is list with 1";


    $kenmerk->save_options({
        options => [
            { value => 'boter', active => 1},
        ]
    });
    $merged = $importer->merge_local_options({
        bibliotheek_kenmerken_id => $kenmerk->id,
        remote_options => [{ value => 'boter'}]
    });
    is scalar @$merged, 1, "One local matching one remote, result is list with one";


    $merged = $importer->merge_local_options({
        bibliotheek_kenmerken_id => $kenmerk->id,
        remote_options => [{ value => 'not-boter'}]
    });
    is scalar @$merged, 2, "One local not-matching one remote, result is list with one";


    $kenmerk->save_options({
        options => [
            { value => 'boter', active => 1},
            { value => 'kaas', active => 0}
        ]
    });

    my $remote_options = [
        { value => 'boter' },
        { value => 'melk' }
    ];

    $merged = $importer->merge_local_options({
        bibliotheek_kenmerken_id => $kenmerk->id,
        remote_options => $remote_options
    });

    is scalar @$merged, 3, "Three elements remain";

    is $merged->[0]->{value}, 'boter', 'First element value is correct';
    ok !exists $merged->[0]->{active}, 'First element is not inactive';

    is $merged->[1]->{value}, 'melk', 'Second element value is correct';
    ok !exists $merged->[0]->{active}, 'Second element is not inactive';

    is $merged->[2]->{value}, 'kaas', 'Third element value is correct';
    ok !$merged->[1]->{active}, 'Third element is inactive';

}, 'Unit test: merge_local_options');


$zs->zs_transaction_ok(sub {
    my $importer = get_importer_ok;

    my $remote_options = $importer->get_remote_options({});
    is_deeply $remote_options, [], "Returns empty list if no options are presented";

    my $extended_options = [1,2,3];
    $remote_options = $importer->get_remote_options({
        extended_options => $extended_options
    });

    is_deeply $remote_options, $extended_options, "Returns extended options if present";


    my $options = [qw/kaas boter/];

    $remote_options = $importer->get_remote_options({ options => $options });
    my $expected = [map {{value => $_, active => 1 }} @$options];

    is_deeply $remote_options, $expected, "Returns decorated options";


    $remote_options = $importer->get_remote_options({ options => $options });

}, 'Unit test: get_remote_options');


$zs->zs_transaction_ok(sub {
    my $importer = get_importer_ok;
    $importer->session({});
    my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(value_type => 'select');

    my $extended_options = [
        { value => 'kaas', active => 1 },
        { value => 'boter', active => 0 }
    ];

    my $remote_record = { extended_options => $extended_options };

    my $field_options = $importer->determine_field_options($remote_record, $kenmerk->id);

    $_->{remote} = 1 for @$extended_options;

    is_deeply $field_options, $extended_options, "Extended options returned";

    # add a local inactive option
    $kenmerk->save_options({
        options => [
            { value => 'eieren', active => 1},
        ]
    });

    $field_options = $importer->determine_field_options($remote_record, $kenmerk->id);

    my $expected = [@$extended_options, {value => 'eieren', active => 0}];

    is_deeply $field_options, $expected, "Local option is retained with active = 0";

}, 'Unit test: determine_field_options');



zs_done_testing();

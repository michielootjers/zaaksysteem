#! perl

### Test header start
use warnings;
use strict;

use DateTime;
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


$zs->zs_transaction_ok(sub {

    my $case = $zs->create_case_ok;

    my $file = $zs->create_file_ok;
    ok $file->update({case_id => undef}), 'Case ID unset';

    my @files = $case->export_files;

    is scalar @files, 0, "Zero files initially";

    my $update = $file->update_properties({
        subject  => $zs->get_subject_ok,
        case_id  => $case->id,
    });

    @files = $case->export_files;
    is scalar @files, 1, "One files after updating";

    my ($exported) = @files;

    isa_ok $exported, 'ARRAY', 'Exported file record is array';

    my ($name, $mimetype, $created, $tag, $status, $case_id, $version) = @$exported;

    is $name, $file->name, "Filename matches";
    is $mimetype, $file->filestore->mimetype, "Mimetype matches";

    my $today = DateTime->now->strftime('%d-%m-%Y');
    ok $created =~ m|^$today|, "Created matches today ok";

    is $tag, 'zaakbehandeling', 'Tag ok';
    is $status, ($case->status eq 'open' ? 'Open' : 'Gearchiveerd'), 'Status OK';
    is $case_id, $case->id, "Case id ok";
    is $version, 1, "Version OK";

}, 'Unit test for Zaak::export_files');



zs_done_testing();
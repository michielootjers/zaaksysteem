#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use File::Spec::Functions;


use_ok('Zaaksysteem::Backend::Sysin::Modules::STUFVBO');


sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF CONFIG MODULE',
            interface_config    => {
                mk_async_url    => 'http://localhost:3331/stuf',
                mk_sync_url     => 'http://localhost:3331/stuf',
                mk_ontvanger    => 'TESM',
                %{ $params }
            }
        },                                
    );  
}


$zs->zs_transaction_ok(sub {
    create_config_interface();

    # my $VALIDATION_MAP = {
    #     'BagVerblijfsobject'     => {
    #         'oppervlakte'                  => '1015XK',
    #     }
    # };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufvbo',
            name            => 'STUF VBO Parsing',
        },                                
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/vbo/101-vbo-create.xml')
                                ),
    });

    ok($transaction, 'VBO Transaction completed');
    ok(!$transaction->error_count, 'VBO Transaction: no errors');
    is($transaction->success_count, 1, 'VBO Transaction: 1 success');
    is($transaction->external_transaction_id, 'MK0002605943', 'PRS Transaction: external transaction id');
    is($transaction->automated_retry_count, undef, 'VBO Transaction: automated retry count');
    ok($transaction->date_created, 'VBO Transaction: date created');
    ok(!$transaction->date_next_retry, 'VBO Transaction: no date next_retry');
    ok($transaction->date_last_retry, 'VBO Transaction: date last retry');


    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;

    is($record->transaction_record_to_objects, 5, 'Got one mutation');

    my $bms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'BagVerblijfsobject',
            'transaction_record_id' => $record->id,
        }
    );

    is($bms->count, 1, 'Got single Bedrijf mutation record');

    my $bm         = $bms->first;

    my $b          = $schema->resultset('BagVerblijfsobject')->find(
        $bm->local_id
    );

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'BagVerblijfsobject',
                'local_id'      => $b->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    # for my $key (keys %{ $VALIDATION_MAP->{BagVerblijfsobject} }) {
    #     my $givenvalue = $b->$key;
    #     my $wantedvalue = $VALIDATION_MAP->{BagVerblijfsobject}->{ $key };

    #     is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    # }

}, 'Tested: Create single VBO mutation');

# $zs->zs_transaction_ok(sub {
#     create_config_interface();

#     my $interface = $zs->create_named_interface_ok(
#         {
#             module          => 'stufadr',
#             name            => 'STUF ADR Parsing',
#         },                                
#     );

#     ### Last created object_subscription
#     my $transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/101-adr-create.xml'
#                                 ),
#     });

#     ok($transaction, 'ADR Transaction completed');
#     ok(!$transaction->error_count, 'ADR Transaction completed');
#     is($transaction->records->count, 1, 'Got single transaction record');

#     my $second_transaction  = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/101-adr-create.xml'
#                                 ),
#     });

#     ok($second_transaction, 'ADR second transaction completed');

#     ok($second_transaction->error_count, 'Failure to add duplicated Bedrijf');
#     ok($second_transaction->error_fatal, 'Permanent error');
#     ok(!$second_transaction->date_next_retry, 'ADR Transaction: no date next_retry');
#     ok($second_transaction->date_last_retry, 'ADR Transaction: date last retry');

# }, 'Tested: Prevent creation of duplicated ADR mutation');


# $zs->zs_transaction_ok(sub {
#     create_config_interface();

#     my $interface = $zs->create_named_interface_ok(
#         {
#             module          => 'stufadr',
#             name            => 'STUF ADR Parsing',
#         },                                
#     );

#     my $transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/121-adr-update.xml'
#                                 ),
#     });

#     like($transaction->records->first->output, qr/no_entry_found/, 'Missing subscription');
# }, 'Tested: ADR Update Mutation, missing object subscription');

# $zs->zs_transaction_ok(sub {
#     create_config_interface();

#     my $VALIDATION_MAP = {
#         'BagNummeraanduiding'     => {
#             'postcode'                  => '1051JL',
#             'huisnummer'                => '70',
#             'huisnummertoevoeging'      => '521',
#         },
#     };

#     my $interface = $zs->create_named_interface_ok(
#         {
#             module          => 'stufadr',
#             name            => 'STUF ADR Parsing',
#         },                                
#     );

#     ###
#     ### CREATE FIRST ENTRY
#     ###
#     my $transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/101-adr-create.xml'
#                                 ),
#     });


#     ###
#     ### MUTATE CREATED ENTRY
#     ###

#     my $second_transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/121-adr-update.xml'
#                                 ),
#     });


#     my $record     = $second_transaction->records->first;

#     my $mutation   = $record->transaction_record_to_objects->search(
#         {
#             'local_table' => 'BagNummeraanduiding',
#             'transaction_record_id' => $record->id,
#         }
#     )->first;

#     ok($mutation, 'Found BagNummeraanduiding mutation');

#     my $np          = $schema->resultset('BagNummeraanduiding')->find(
#         $mutation->local_id
#     );

#     ok(!$second_transaction->error_count, 'Succesfully mutated NNP entry');

#     for my $key (keys %{ $VALIDATION_MAP->{BagNummeraanduiding} }) {
#         my $givenvalue = $np->$key;
#         my $wantedvalue = $VALIDATION_MAP->{BagNummeraanduiding}->{ $key };

#         is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
#     }

# }, 'Tested: ADR Update Mutation');

# $zs->zs_transaction_ok(sub {
#     create_config_interface();

#     my $interface = $zs->create_named_interface_ok(
#         {
#             module          => 'stufadr',
#             name            => 'STUF ADR Parsing',
#         },                                
#     );

#     ###
#     ### CREATE FIRST ENTRY
#     ###
#     my $transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/101-adr-create.xml'
#                                 ),
#     });

#     my $record     = $transaction->records->first;

#     is(
#         $schema->resultset('ObjectSubscription')->search_active->count,
#         1,
#         'Single object subscription'
#     );

#     my $mutation   = $record->transaction_record_to_objects->search(
#         {
#             'local_table' => 'BagNummeraanduiding',
#             'transaction_record_id' => $record->id,
#         }
#     )->first;

#     ok($mutation, 'Found BagNummeraanduiding mutation');

#     ###
#     ### DELETE CREATED ENTRY
#     ###
#     my $second_transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/131-adr-delete.xml'
#                                 ),
#     });

#     my $np          = $schema->resultset('BagNummeraanduiding')->find(
#         $mutation->local_id
#     );

#     ok(!$second_transaction->error_count, 'Succesfully deleted NNP entry');
#     ok($np->einddatum, 'BagNummeraanduiding is deleted: ' . $np->einddatum);

#     ok(
#         !$schema->resultset('ObjectSubscription')->search_active->count,
#         'Object subscription removed'
#     );
# }, 'Tested: ADR Delete');

# $zs->zs_transaction_ok(sub {
#     create_config_interface();

#     my $interface = $zs->create_named_interface_ok(
#         {
#             module          => 'stufadr',
#             name            => 'STUF ADR Parsing',
#         },                                
#     );

#     my $inserted_transaction = $interface->process({
#         input_data              => $zs->get_file_contents_as_string(
#                                     catfile(STUF_TEST_XML_PATH, '0204', '/adr/101-adr-create.xml'
#                                 ),
#     });

#     my $transaction = $interface->process_trigger(
#         'disable_subscription',
#         {
#             subscription_id => $interface->object_subscriptions->first->id,
#         }
#     );

# }, 'Tested: ADR remove afnemerindicatie');


zs_done_testing();

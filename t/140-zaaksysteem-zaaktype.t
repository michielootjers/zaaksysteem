#! perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Backend::Object;


$zs->zs_transaction_ok(sub {
    my $auth = $zs->create_zaaktype_authorisation_ok;

    ok can_ok($auth, 'security_identity'), 'zaaktype/object implements security identity method';

    my $model = Zaaksysteem::Backend::Object->new(schema => $schema);
    my $casetype_object = $model->rs->first;
    my $casetype_acl = $casetype_object->object_acl_entries->first;

    my %id = $auth->security_identity;

    ok exists $id{ position }, 'zaaktype/object security_identity returns a position';

    is $casetype_acl->entity_type, 'position', 'zaaktype/object position is set';
    is $casetype_acl->entity_id, $id{ position }, 'zaaktype/object position ident correct';
    is $casetype_acl->get_column('object_uuid'), $casetype_object->uuid, 'zaaktype/object object_uuid matches casetype';

    # this last check is for the sanity of the testsuite itself, this file
    # must be reviewed since the underlying TestUtils API (or something else)
    # has broken. Also it checks that the 'recht' zaak_edit is mapped to 'read'
    is $casetype_acl->capability, 'read', 'zaaktype/object default permission is read';

}, 'zaaktype/object object_data sync ok');

$zs->zs_transaction_ok(sub {
    my (@auths) = $zs->create_zaaktype_authorisation_ok( recht => 'zaak_edit' );

    my $model = Zaaksysteem::Backend::Object->new(schema => $schema);
    my $casetype_acl = $model->rs->first->object_acl_entries->first;

    is $casetype_acl->capability, 'write', 'zaaktype/object zaak_edit permission mapped to write';
});

zs_done_testing;

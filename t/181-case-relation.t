#! perl
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::TestUtils;


sub newskool_systeemkenmerk {
    my ($case, $name) = @_;

    my %attribute_map = map { $_->name => $_->value } @{ $case->object_attributes };
    return $attribute_map{$name};
}

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    # check single
    ok $case->systeemkenmerk('alle_relaties') eq '', "case confesses its lack of relationship";
    TODO: {
        local $TODO = "Bug met object attributes";
        is newskool_systeemkenmerk($case, 'case.relations'), '',
          "also in newskool";
    }

    my $second = $zs->create_case_ok;
    my $relation = $schema->resultset('CaseRelation')->add_relation($case->id, $second->id);
    $case->touch();

    # check 1 relation
    ok $case->systeemkenmerk('alle_relaties') eq $second->id, "case confesses its relationship";
    is newskool_systeemkenmerk($case, 'case.relations'), $second->id, "also in newskool";

    my $third = $zs->create_case_ok;
    $relation = $schema->resultset('CaseRelation')->add_relation($case->id, $third->id);
    $case->touch();

    # check 2 relations
    my $desired = $second->id . ", " . $third->id;
    ok $case->systeemkenmerk('alle_relaties') eq $desired, "case confesses its multiple relationships";
    is newskool_systeemkenmerk($case, 'case.relations'), $desired, "also in newskool";

},'set_vernietigingdatum',);



zs_done_testing();

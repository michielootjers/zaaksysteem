#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

remove_test_db_ok(connect_test_db_ok());

$zs->empty_filestore_ok();

zs_done_testing();

#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;### Test header end

use Test::DummySearchTerm;
use Zaaksysteem::Search::Conditional;

use Moose::Util qw(apply_all_roles);

{
    my @TERM1;
    my $term1 = Test::DummySearchTerm->new(source => \@TERM1);

    my @TERM2;
    my $term2 = Test::DummySearchTerm->new(source => \@TERM2); 

    my $cond = Zaaksysteem::Search::Conditional->new(
        lterm  => $term1,
        rterm => $term2,
        operator   => '=',
    );

    @TERM1 = (qw(aap noot mies));
    @TERM2 = (qw(foo bar baz));
    my $rv = $cond->evaluate();

    is_deeply(
        $rv, 
        \[ "(aap = foo)", qw(noot mies bar baz) ],
        'Evaluation a condition with two terms returns the correct value',
    );

    @TERM1 = (qw(aap));
    @TERM2 = (qw(foo bar baz));
    $rv = $cond->evaluate();

    is_deeply(
        $rv, 
        \[ "(aap = foo)", qw(bar baz) ],
        'Evaluation a condition with two terms (1st without placeholders) returns the correct value',
    );

    @TERM1 = (qw(aap noot mies));
    @TERM2 = (qw(foo));
    $rv = $cond->evaluate();

    is_deeply(
        $rv, 
        \[ "(aap = foo)", qw(noot mies) ],
        'Evaluation a condition with two terms (2nd without placeholders) returns the correct value',
    );
}

zs_done_testing();

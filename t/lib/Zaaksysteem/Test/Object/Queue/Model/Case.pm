package Zaaksysteem::Test::Object::Queue::Model::Case;
use Zaaksysteem::Test;

use Zaaksysteem::Backend::Object::Queue::Component;
use Zaaksysteem::Object::Queue::Model;

=head1 NAME

Zaaksysteem::Test::Object::Queue::Model::Case - Test queue items related to cases

=head1 DESCRIPTION

Test "case" queue items.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Queue::Model::Case

=head2 test_add_case_subject

Test the "add case subject" queue item handling.

=cut

sub test_add_case_subject {
    my $role  = mock_moose_class({
        roles => ['Zaaksysteem::Object::Queue::Model::Case', 'MooseX::Log::Log4perl'],
    });

    my $rv = {
        send_mail => 0,
        create_subject => 0,
    };

    my $relateren_args;
    my $item = qobj(
        object_data => qobj(
            get_source_object => qobj(
                betrokkene_relateren => qmeth {
                    my $self = shift;

                    # Copy values, not references.
                    $relateren_args = [@_];

                    return 1;
                },
                mailer => qobj(
                    send_case_notification => qmeth {
                        $rv->{send_mail} = 1;
                        return 1;
                    },
                ),
                result_source => qobj(
                    schema  => qobj(
                        betrokkene_model => qobj(
                            get_by_string   => qmeth {
                                return qobj(
                                    can     => qmeth {
                                        return 1
                                    },
                                    gm_extern_np => qobj(
                                        can     => qmeth {
                                            return 1,
                                        },
                                        deleted_on => 0
                                    ),
                                    email => qmeth { 'arniek@example.com' },
                                    display_name => 'Jadda',
                                )
                            }
                        ),
                        resultset => qmeth {
                            return qobj(
                                ### RS: Config
                                get => qmeth {
                                    return 12345;
                                },
                                ### RS: BibliotheekNotificaties
                                find => qmeth { 1 }
                            )
                        }
                    )
                )
            ),
        ),
        data => {
            betrokkene_identifier => 'betrokkene-id',
            magic_string_prefix   => 'magicstring',
            rol                   => 'dit is hoe ik rol',
            gemachtigd            => 1,
            notify                => 1,
        },
    );
    $role->add_case_subject($item);

    is_deeply(
        $relateren_args,
        [
            {
                betrokkene_identifier  => 'betrokkene-id',
                magic_string_prefix    => 'magicstring',
                rol                    => 'dit is hoe ik rol',
                pip_authorized         => 1,
                send_auth_confirmation => 1,
            },
        ],
    );

    ok($rv->{send_mail}, "Mail send");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

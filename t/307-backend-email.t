#! perl

### Test header start
use warnings;
use strict;

use Data::Dumper;
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use_ok('Zaaksysteem::Backend::Email');
use_ok('Zaaksysteem::Backend::Mailer');


sub mock_email_sender {
    my $mock = Test::MockObject->new();
   	$mock->mock('send', sub {
   		my $self = shift;
   		diag "sending_email:", explain \@_;
   		return "message";
   	});
   	return $mock;
}

$zs->zs_transaction_ok(sub {

    my $mailer = Zaaksysteem::Backend::Email->new(
        default_from_email      => 'test@mintlab.nl',
        schema                  => $schema,
        email_sender            => Zaaksysteem::Backend::Mailer->new,
    );

	my $case = $zs->create_case_ok;
	$case->aanvrager_object->email('jw@mintlab.nl');

    my $prepared_notification       = $case->prepare_notification({
        recipient_type  => 'aanvrager',
        body            => 'bericht tekst',
        subject         => 'test mail voor 307-backend-email.t',
    });

    my $body = $mailer->send_from_case({
        case                => $case,
        notification        => $prepared_notification,
        betrokkene_id       => 'betrokkene-medewerker-1'
    });

}, 'Unit test for mailer');


zs_done_testing();
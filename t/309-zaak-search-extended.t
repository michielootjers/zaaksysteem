#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(sub {

    my $rs = $schema->resultset('Zaak');
	my $initial_count = $rs->search->count;

    is $rs->search_extended->count, $initial_count, 'All cases retrieved by default';

    # create case as admin
    my $case = $zs->create_case_ok;

    my $subject = $zs->create_subject_ok(username => 'gebruiker');
    $rs->{attrs}->{current_user} = $zs->set_current_user(subject => $subject);

    is $rs->search_extended->count, 0, 'No cases present when logged as unprivileged user';

    $case->aanvrager_gm_id($subject->id);
    $case->update;

    is $rs->search_extended->count, 0, 'No cases present when logged as unprivileged user, even when aanvrager_gm_id matches my user id';

}, 'ResultsetZaken::search_extended');

zs_done_testing();

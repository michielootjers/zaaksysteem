package Test::DummyResultSet;
use Moose;
use namespace::autoclean;

has search_args => (
    is => 'rw',
    isa => 'ArrayRef',
    default => sub { [] },
);

has hstore_column => (
    is => 'ro',
    isa => 'Str',
    default => 'hstore_properties',
);

has text_vector_column => (
    is => 'ro',
    isa => 'Str',
    default => 'text_vector'
);

sub result_source {
    my $mock_dbh = Test::MockObject->new();
    $mock_dbh->mock('quote' => sub {
        my $self = shift;
        return sprintf(q{%s (quoted)}, $_[0]);
    });

    my $mock_storage = Test::MockObject->new();
    $mock_storage->set_always('dbh' => $mock_dbh);

    my $mock_rs = Test::MockObject->new();
    $mock_rs->set_always('storage' => $mock_storage);
}

sub search {
    my $self = shift;
    $self->search_args(
        [
            @{ $self->search_args },
            [@_]
        ]
    );
}

1;

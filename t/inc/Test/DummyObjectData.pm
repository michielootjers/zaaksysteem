package Test::DummyObjectData;
use Moose;

has attributes => (
    is => 'rw',
    isa => 'HashRef[Zaaksysteem::Backend::Object::Attribute]',
    default => sub { {} },
);

has source_object => (
    is => 'rw',
);

=head1 NAME

Test::DummyObjectData - Fake ObjectData object

=head1 SYNOPSIS

    $od = Test::DummyObjectData->new();

    $oa = Zaaksysteem::Backend::Object::Attribute->new(...);

    $od->add_object_attributes($oa, ...);
    $attr = $od->get_object_attribute('foo');

=head1 METHODS

=head2 add_object_attributes($oa, $oa, ...)

Add the specified L<Zaaksysteem::Backend::Object::Attribute> instances to the
internal list of attributes.

=cut

sub add_object_attributes {
    my $self = shift;

    for my $attr (@_) {
        $self->attributes->{ $attr->name } = $attr;
    }

    return $self;
}

=head2 get_object_attribute

Retrieve an attribute by name.

=cut

sub get_object_attribute {
    my $self = shift;

    return $self->attributes->{ $_[0] };
}

=head2 get_source_object

Retrieve the "source object" for this (fake) ObjectData instance.

=cut

sub get_source_object {
    my $self = shift;
    return $self->source_object;
}

__PACKAGE__->meta->make_immutable();

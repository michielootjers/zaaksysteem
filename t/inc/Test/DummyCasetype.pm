package Test::DummyCasetype;
use Moose;
use namespace::autoclean;


=head2 dummy_casetype

a casetype is born as a session hash mostly in the controller layer.
this makes it kinda hard to replicate from the suite. a better solution is
to rewrite the casetype creation process using an api:

my $casetype = new Casetype;

$casetype->title('Test casetype');
$casetype->add_middle_phase('Besluitvorming');
$casetype->registration_phase->attributes->add_attribute({ settings });

and then

my $casetype_as_session = $casetype->as_session;
.. and so on.

Since that could easily take two weeks of work - we'd need to make sure it's 
used exclusively in the application to be reliable - I'll do the hardcoded
thingy for now.

=cut

use constant DUMMY_CASETYPE => {
  'authorisaties' => {},
  'betrokkenen' => {
                     '1' => {
                              'betrokkene_type' => 'natuurlijk_persoon',
                              'id' => '1541',
                              'zaaktype_node_id' => '235'
                            },
                     '2' => {
                              'betrokkene_type' => 'natuurlijk_persoon_na',
                              'id' => '1542',
                              'zaaktype_node_id' => '235'
                            },
                     '3' => {
                              'betrokkene_type' => 'niet_natuurlijk_persoon',
                              'id' => '1543',
                              'zaaktype_node_id' => '235'
                            },
                     '4' => {
                              'betrokkene_type' => 'niet_natuurlijk_persoon_na',
                              'id' => '1544',
                              'zaaktype_node_id' => '235'
                            },
                     '5' => {
                              'betrokkene_type' => 'preset_client',
                              'id' => '1545',
                              'zaaktype_node_id' => '235'
                            },
                     '6' => {
                              'betrokkene_type' => 'medewerker',
                              'id' => '1546',
                              'zaaktype_node_id' => '235'
                            },
                     '7' => {
                              'betrokkene_type' => 'medewerker',
                              'id' => '1547',
                              'zaaktype_node_id' => '235'
                            }
                   },
  'definitie' => {
                   'aard' => undef,
                   'afhandeltermijn' => '1',
                   'afhandeltermijn_type' => 'kalenderdagen',
                   'besluittype' => undef,
                   'custom_webform' => '',
                   'extra_informatie' => '',
                   'grondslag' => '1',
                   'handelingsinitiator' => 'aangaan',
            #       'id' => '235',
                   'iv3_categorie' => undef,
                   'omschrijving_upl' => undef,
                   'openbaarheid' => undef,
                   'pdc_description' => undef,
                   'pdc_meenemen' => undef,
                   'pdc_tarief' => '',
                   'pdc_voorwaarden' => undef,
                   'preset_client' => 'betrokkene-natuurlijk_persoon-1',
                   'procesbeschrijving' => '',
                   'selectielijst' => undef,
                   'servicenorm' => '1',
                   'servicenorm_type' => 'kalenderdagen'
                 },
  'node' => {
              'aanvrager_hergebruik' => undef,
              'active' => undef,
              'adres_aanvrager' => undef,
              'adres_andere_locatie' => undef,
              'adres_relatie' => undef,
              'automatisch_aanvragen' => undef,
              'automatisch_behandelen' => 1,
              'bedrijfid_wijzigen' => undef,
              'code' => '1',
              'contact_info_intake' => undef,
              'deleted' => undef,
              'extra_relaties_in_aanvraag' => undef,
            #  'id' => '235',
              'is_public' => undef,
              'online_betaling' => undef,
              'prevent_pip' => undef,
              'properties' => {
                                'aanleiding' => '',
                                'archiefclassicatiecode' => '',
                                'bag' => 'Nee',
                                'beroep_mogelijk' => 'Nee',
                                'doel' => '',
                                'e_formulier' => '',
                                'lex_silencio_positivo' => 'Nee',
                                'lock_registration_phase' => '1',
                                'lokale_grondslag' => '',
                                'opschorten_mogelijk' => 'Nee',
                                'pdc_tarief_balie' => '',
                                'pdc_tarief_behandelaar' => '',
                                'pdc_tarief_email' => '',
                                'pdc_tarief_post' => '',
                                'pdc_tarief_telefoon' => '',
                                'publicatie' => 'Nee',
                                'publicatietekst' => '',
                                'queue_coworker_changes' => '1',
                                'verantwoordelijke' => '',
                                'verantwoordingsrelatie' => '',
                                'verdagingstermijn' => '',
                                'verlenging_mogelijk' => 'Nee',
                                'verlengingstermijn' => '',
                                'vertrouwelijkheidsaanduiding' => '-',
                                'wet_dwangsom' => 'Nee',
                                'wkpb' => 'Nee'
                              },
              'titel' => 'Kenmerken wijzigen',
              'toelichting' => undef,
              'toewijzing_zaakintake' => undef,
              'trigger' => 'internextern',
              'version' => 16,
              'webform_authenticatie' => undef,
              'webform_toegang' => 1,
            #  'zaaktype_definitie_id' => '235',
              'zaaktype_id' => 8,
              'zaaktype_omschrijving' => '',
              'zaaktype_rt_queue' => undef,
              'zaaktype_trefwoorden' => '',
              'zaaktype_vertrouwelijk' => undef
            },
  'statussen' => {
                   '1' => {
                            'definitie' => {
                                             'checklist' => undef,
                                             'fase' => 'Registreren',
                                             'id' => '481',
                                             'naam' => 'Geregistreerd',
                                             'ou_id' => '10001',
                                             'role_id' => '20001',
                                             'role_set' => undef,
                                             'status' => 1,
                                             'status_type' => undef
                                           },
                            'elementen' => {
                                             'checklists' => {},
                                             'kenmerken' => {
                                                              '1' => {
                                                                       'bag_zaakadres' => undef,
                                                                       'bibliotheek_kenmerken_id' => 11,
                                                                       'created' => undef,
                                                                       'date_fromcurrentdate' => undef,
                                                                       'help' => undef,
                                                                       'help_extern' => undef,
                                                                       'id' => '777',
                                                                       'is_group' => undef,
                                                                       'is_systeemkenmerk' => undef,
                                                                       'label' => undef,
                                                                       'last_modified' => undef,
                                                                       'naam' => 'Toelichting',
                                                                       'options' => [],
                                                                       'pip' => undef,
                                                                       'pip_can_change' => undef,
                                                                       'publish_public' => undef,
                                                                       'referential' => undef,
                                                                       'required_permissions' => undef,
                                                                       'type' => 'textarea',
                                                                       'value_default' => undef,
                                                                       'value_mandatory' => undef,
                                                                       'version' => 1,
                                                                       'zaak_status_id' => '481',
                                                                       'zaakinformatie_view' => undef,
                                                                       'zaaktype_node_id' => '235'
                                                                     }
                                             },
                                             'notificaties' => {},
                                             'regels' => {},
                                             'relaties' => {},
                                             'resultaten' => {},
                                             'sjablonen' => {}
                                           }
                          },
                   '2' => {
                            'definitie' => {
                                             'checklist' => undef,
                                             'fase' => 'Afhandelen',
                                             'id' => '482',
                                             'naam' => 'Afgehandeld',
                                             'ou_id' => '10001',
                                             'role_id' => '20001',
                                             'role_set' => undef,
                                             'status' => 2,
                                             'status_type' => undef
                                           },
                            'elementen' => {
                                             'checklists' => {},
                                             'kenmerken' => {},
                                             'notificaties' => {},
                                             'regels' => {},
                                             'relaties' => {},
                                             'resultaten' => {
                                                               '1' => {
                                                                        'archiefnominatie' => 'Bewaren (B)',
                                                                        'bewaartermijn' => 62,
                                                                        'comments' => '',
                                                                        'dossiertype' => undef,
                                                                        'id' => '314',
                                                                        'ingang' => 'vervallen',
                                                                        'label' => 'sdffsdfds',
                                                                        'resultaat' => 'aangehouden',
                                                                        'selectielijst' => '',
                                                                        'zaaktype_node_id' => '235',
                                                                        'zaaktype_status_id' => '482'
                                                                      }
                                                              },
                                             'sjablonen' => {}
                                           }
                          }
                 },
  'zaaktype' => {
                  'active' => 1,
                  'bibliotheek_categorie_id' => '3',
                  'deleted' => undef,
                  #'id' => 8,
                  'object_type' => 'zaaktype',
                  'search_index' => undef,
                  'search_term' => '8 Kenmerken wijzigen',
                  'searchable_id' => 47,
                  'version' => undef,
#                  'zaaktype_node_id' => '235'
                }
};


1;

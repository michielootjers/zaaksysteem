package Test::DummySearchTerm;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Search::Term';

has source => (
    is => 'ro',
    isa => 'ArrayRef',
);

override 'evaluate' => sub {
    my $self = shift;

    return @{ $self->{source} };
};

__PACKAGE__->meta->make_immutable();

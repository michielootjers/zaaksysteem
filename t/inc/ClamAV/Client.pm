package ClamAV::Client;
use Moose;

sub ping { 1 }

sub scan_stream {
    my ($self, $fh) = @_;
    my $content = join "", <$fh>;
    return $content eq 'i am infected';
}

1;

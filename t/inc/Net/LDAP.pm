#! perl
use strict;
use warnings;

package Net::LDAP;
use Exporter 'import';

use Data::Dumper;
use Net::LDAP::LDIF;
use Zaaksysteem::Exception;

my %ldap_entries;

{
    my $ldif = Net::LDAP::LDIF->new(
        './db/zaaksysteem.ldif',
        'r',
        onerror => 'warn'
    );

    my ($entry, $error);
    while (!$ldif->eof()) {
        $entry = $ldif->read_entry();
        $error = $ldif->error();
        if ($error) {
            warn(
                sprintf "Error msg: %s\nError lines: %s",
                $error,
                $ldif->error_lines(),
            );
        }
        else {
            _entry_to_class($entry);
        }
    }
}

sub _entry_to_class {
    my $entry = shift;
    my @objectclass = $entry->get_value('objectClass');
    if (grep { 'posixAccount' eq $_ } @objectclass ) {
        push(@{$ldap_entries{users}}, $entry);
    }
    elsif (grep { 'posixGroup' eq $_ } @objectclass) {
        push(@{$ldap_entries{groups}}, $entry);
    }
    elsif (grep { 'organizationalUnit' eq $_ } @objectclass) {
        push(@{$ldap_entries{departments}}, $entry);
    }
    else {
        push(@{$ldap_entries{others}}, $entry);
    }
}

sub new {
    return bless({}, "Net::LDAP");
}

sub _ldap_message {
    my $self = shift;
    my $entries = shift;
    return Net::LDAP::Message->new($entries);
};

sub search {
    my $self = shift;
    my $args;
    if (@_ > 1) {
        $args = {@_};
    }
    else {
        $args->{filter} = shift;
    }
    my $entries;
    if ($args->{filter} =~ /organizationalUnit/) {
        $entries = $ldap_entries{departments};
    }
    elsif ($args->{filter} =~ /posixGroup/) {
        $entries = $ldap_entries{groups};
    }
    elsif ($args->{filter} =~ /posixAccount/) {
        $entries = $ldap_entries{users};
    } else {
        # poor man's LDAP search, only exact match for now.
        # find all the matching objects
        $entries = [
            grep { $_->dn eq $args->{base} }
            map { @{ $ldap_entries{$_} } } qw/departments groups users/
        ];
    }
    return $self->_ldap_message($entries);
}

sub bind {
    my $self = shift;
    return $self->_ldap_message();
}

sub add {
    my $self = shift;
    my $dn = shift;
    my $entry = Net::LDAP::Entry->new($dn, @_);
    _entry_to_class($entry);
    return $self->_ldap_message([$entry]);
}

sub modify {
    my $self = shift;
    return $self->_ldap_message()
}

sub delete {
    my $self = shift;
    return $self->_ldap_message();
}

sub get_roles {
    my $self = shift;
    my $user = shift;

    my $groups = $ldap_entries{groups};
    my $dn = $user->properties->{dn};

    return grep {
        # yo dawg, i heard you like grep
        grep { $_ eq $dn } $_->get_value('memberUid');
    } @$groups;
}

1;

package Net::LDAP::Message;
use Net::LDAP;

sub new {
    my $self = shift;
    my $entries = shift;
    return bless({entries => $entries}, "Net::LDAP::Message");
}

sub code {
    my $self = shift;
    return undef;
}

sub error {
    my $self = shift;
    return undef;
}

sub control {
    my $self = shift;
    return;
}

sub count {
    my $self = shift;
    if (ref $self->{entries}) {
        return scalar @{$self->{entries}};
    }
    else {
        return 0;
    }
}

sub entries {
    my $self = shift;
    return @{$self->{entries}};
}

sub entry {
    my $self = shift;
    return @{$self->{entries}}[-1];
}

sub pop_entry {
    my $self = shift;

    my @entries = $self->entries;
    my $ret = pop @entries;

    $self->{ entries } = [ @entries ];

    return $ret;
}

1;

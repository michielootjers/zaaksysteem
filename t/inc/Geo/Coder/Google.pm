#! perl
use strict;
use warnings;

package Geo::Coder::Google;
use Moose;
use LWP::UserAgent;

has 'ua' => (
    is      => 'rw',
    default => sub {return LWP::UserAgent->new},
);

has apiver   => (is => 'rw');
has apikey   => (is => 'rw');
has language => (is => 'rw');
has region   => (is => 'rw');

sub geocode {
    my $self = shift;

    return {
        'address_components' => [{
                'long_name'  => '7',
                'short_name' => '7',
                'types'      => ['street_number']
            }, {
                'long_name'  => 'Donker Curtiusstraat',
                'short_name' => 'Donker Curtiusstraat',
                'types'      => ['route']
            }, {
                'long_name'  => 'Westerpark',
                'short_name' => 'Westerpark',
                'types'      => [
                    'sublocality',
                    'political'
                ]
            }, {
                'long_name'  => 'Amsterdam',
                'short_name' => 'Amsterdam',
                'types'      => [
                    'locality',
                    'political'
                ]
            }, {
                'long_name'  => 'Amsterdam',
                'short_name' => 'Amsterdam',
                'types'      => [
                    'administrative_area_level_2',
                    'political'
                ]
            }, {
                'long_name'  => 'Noord-Holland',
                'short_name' => 'NH',
                'types'      => [
                    'administrative_area_level_1',
                    'political'
                ]
            }, {
                'long_name'  => 'Nederland',
                'short_name' => 'NL',
                'types'      => [
                    'country',
                    'political'
                ]
            }, {
                'long_name'  => '1051 JL',
                'short_name' => '1051 JL',
                'types'      => ['postal_code']}
        ],
        'formatted_address' =>
          'Donker Curtiusstraat 7, 1051 JL Amsterdam, Nederland',
        'geometry' => {
            'location' => {
                'lat' => '52.3790049',
                'lng' => '4.8717269'
            },
            'location_type' => 'ROOFTOP',
            'viewport'      => {
                'northeast' => {
                    'lat' => '52.3803538802915',
                    'lng' => '4.8730758802915'
                },
                'southwest' => {
                    'lat' => '52.3776559197085',
                    'lng' => '4.8703779197085'
                }}
        },
        'types' => ['street_address']};
}

1;

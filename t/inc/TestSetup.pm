package TestSetup;

use warnings;
use strict;
use feature 'state';

use autodie qw(:all);
use Config::Auto;
use DBI;
use File::Temp qw(tempfile);
use JSON;
use LWP::UserAgent;
use POSIX qw(strftime);
use Params::Check qw(check);
use Test::Builder::Module;
use Test::Exception;
use Test::More;
use Test::NoWarnings ();
use Test::MockObject;
use Zaaksysteem::Model::DB;
use Zaaksysteem::TestUtils;
use Zaaksysteem::Log::CallingLogger;

use base 'Test::Builder::Module';
my $tb = __PACKAGE__->builder;

# 8378 = "TEST" on a phone keyboard
my $TEST_HOST = "127.0.0.1";
my $TEST_PORT = 8378;
my $TEST_URL = "http://$TEST_HOST:$TEST_PORT/";

my $log = Zaaksysteem::Log::CallingLogger->new();

our @EXPORT = (
    qw(
        connect_test_db_ok
        remove_test_db_ok
        zs_done_testing

        start_test_server_ok
        ensure_test_server_ok
        stop_test_server_ok

        read_test_config
        parse_zapi_response_ok

        STUF_TEST_XML_PATH

        initialize_test_globals_ok
        $zs
        $schema
    ),
    @Test::More::EXPORT,
    @Test::Exception::EXPORT,
    @Test::NoWarnings::EXPORT,
);
use constant STUF_TEST_XML_PATH => 't/inc/API/StUF';

# Connect to the test database, create one if no mytest is found.
sub connect_test_db_ok {
    my %credentials;
    if (-e '.mytest') {
        %credentials = _read_mytest();
    }
    else {
        %credentials = _create_mytest();
    }

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn      => $credentials{dsn},
            username => $credentials{db_user} || undef,
            password => $credentials{db_password} || undef,
        });

    my $schema = Zaaksysteem::Model::DB->new->schema;
    $schema->log($log);

    $tb->ok($schema, "Connected to database $credentials{dsn}");

    my %required_config = (
        filestore_location      => 't/inc/Teststore',
        tmp_location            => 't/inc/tmp',
        file_username_seperator => '-',
    );
    foreach my $t (keys %required_config) {
        my $x = $schema->resultset('Config')->update_or_create({
            parameter => $t,
            value     => $required_config{$t},
        });
        $tb->ok($x, "Added $t to config");
    }

    return $schema;
}

sub _read_mytest {
    my $config = Config::Auto::parse('./.mytest');

    my $args = check(
        {
            dsn         => {required => 1},
            db_user     => {required => 0},
            db_password => {required => 0},
        },
        $config,
    );


    return %$args;
}

sub _create_mytest {
    my $args = read_test_config() or die('Could not read test_config');
    my $dbh = _connect_template($args);

    my $unix_username = getpwuid($<);
    my $dbname = strftime("zaaksysteem_test_${unix_username}_%Y%m%d_%H%M%S", localtime);

    $dbh->do("CREATE DATABASE $dbname ENCODING 'UTF-8'");
    $dbh->disconnect();

    my @mytest;

    {
        local $ENV{PGOPTIONS}  = "--client-min-messages=warning";
        local $ENV{PGDATABASE} = $dbname;
        local $ENV{PGHOST};
        local $ENV{PGUSER};
        local $ENV{PGPASSWORD};

        my $real_dsn = "dbi:Pg:dbname=$dbname";

        if ($args->{db_host}) {
            $ENV{PGHOST} = $args->{db_host};
            $real_dsn .= ";host=$args->{db_host}";
        }
        push(@mytest, "dsn = $real_dsn");
        if ($args->{db_user}) {
            $ENV{PGUSER} = $args->{db_user};
            push(@mytest, "db_user = $args->{db_user}");
        }
        if ($args->{db_password}) {
            $ENV{PGPASSWORD} = $args->{db_password};
            push(@mytest, "db_password = $args->{db_password}");
        }

        if (
            system(qw(
                psql
                    -X
                    -q
                    --single-transaction
                    --pset pager=off
                    --set ON_ERROR_STOP=1
                    -f db/test-template.sql
            )) != 0
        ) {
            $tb->BAIL_OUT("Could not load database schema: $@");
        }
    }

    open my $mytest, '>', '.mytest';
    print $mytest join("\n", @mytest, "\n");
    close $mytest;

    return _read_mytest();
}

# Set the test-plan automatically and include test-lib(s)
sub import {
    warnings->import();
    strict->import();
    Test::NoWarnings->import();
    goto &Test::Builder::Module::import;
}

# Clean up the test database. Should really only be called by 999-cleanup.t
sub remove_test_db_ok {
    ### TODO: maybe let it figure out the handle by itself somehow.
    my ($handle) = @_;

    # Destroy the DB-handle to prevent db-in-use-errors.
    $handle->storage->disconnect();
    $tb->ok(1, "Destroyed database-handle");

    my $test_config = read_test_config();
    my $dbh = _connect_template($test_config);

    my %mytest = _read_mytest();

    my ($dbname) = $mytest{dsn} =~ /dbname=([^;]+)/;

    unlink('./.mytest');

    $dbh->do("SELECT pg_terminate_backend(procpid) FROM pg_stat_activity WHERE procpid <> pg_backend_pid() AND datname = \'$dbname\'");
    $dbh->do("DROP DATABASE $dbname");

    $tb->ok(1, "Removed database $mytest{dsn} and .mytest");

    return;
}

=head2 start_test_server_ok

Start a "test" zaaksysteem server for controller tests.

=cut

sub start_test_server_ok {
    my $test_msg = "";
    if (-e '.mycatalyst') {
        $tb->BAIL_OUT("Running test server found. Aborted a previous test run? Run t/998-kill-test_server.t");
    }
    else {
        my $pid = _start_test_server($TEST_HOST, $TEST_PORT);
        $test_msg = "New test server started, pid = $pid";
    }
    $ENV{CATALYST_SERVER} = $TEST_URL;

    $tb->ok(1, $test_msg);
    return;
}

sub _start_test_server {
    my ($host, $port) = @_;

    pipe my $reader, my $writer;
    my $pid = fork();
    if ($pid) {
        close $writer;

        my $got_accepting = 0;
        while (my $line = readline($reader)) {
            if ($line =~ /Accepting connections/) {
                close $reader;
                $got_accepting = 1;
                last;
            }
        }

        if (!$got_accepting && !$ENV{ZS_CATALYST_TRACE}) {
            $tb->BAIL_OUT(
                "Zaaksysteem test server died before starting to accept connections (hint: netstat -ltp)"
            );
        }
    }
    else {
        close STDIN;
        close STDOUT;
        close $reader;

        unless ($ENV{ZS_CATALYST_TRACE}) {
            open(STDERR, ">&=" . fileno($writer)) or die $!;
        }

        my ($zs_config, $filename) = tempfile(
            'testconfigXXXX',
            TMPDIR => 1,
            SUFFIX => '.conf',
        );
        my %credentials = _read_mytest();

        open (my $template, "<", "etc/zaaksysteem.conf.test-template");
        while (my $line = <$template>) {
            # Template substitution
            $line =~ s/\[%\s+dsn\s+%\]/$credentials{dsn}/g;
            $line =~ s/\[%\s+dbuser\s+%\]/$credentials{db_user}/g;
            $line =~ s/\[%\s+dbpass\s+%\]/$credentials{db_password}/g;
            print $zs_config $line;
        }

        $ENV{ZAAKSYSTEEM_CONF} = $filename;
        my @execlist = (
            "./script/zaaksysteem_server.pl",
            "--host"    => $host,
            "--port"    => $port,
            "--pidfile" => ".mycatalyst",
        );
        exec(@execlist) or die $!;
    }

    return $pid;
}

=head2 ensure_test_server_ok

Ensure the test server is still running, and set the environment variable
required to make L<Catalyst::Test> work.

=cut

sub ensure_test_server_ok {
    if (!-e '.mycatalyst') {
        $tb->BAIL_OUT("Test server not found.");
    }

    my $ua = LWP::UserAgent->new(timeout => 15);
    my $res = $ua->get($TEST_URL);
    if (!$res->is_success) {
        $tb->BAIL_OUT("Test server running, but GET / doesn't work.");
    }

    $ENV{CATALYST_SERVER} = $TEST_URL;
    $tb->ok(1, "Test server found");
    return;
}

=head2 stop_test_server_ok

Stop the "test" catalyst server, started by ensure_test_server_ok.

Should probably only be run once, at the end of the test run (starting a new
test server takes a long time).

=cut

sub stop_test_server_ok {
    open(my $fh, "<", ".mycatalyst");

    my $pid = readline($fh);
    chomp $pid;

    kill(TERM => $pid);

    close($fh);
    unlink(".mycatalyst");

    $tb->ok(1, "Test server stopped (pid = $pid)");
    return;
}

=head2 parse_zapi_response_ok

Check if the response looks like a ZAPI response, return the JSON data structure.

=cut

sub parse_zapi_response_ok {
    my ($response) = @_;

    my $ok = 1;
    $ok = 0 if ($response->content_type ne 'application/json');

    my $json = decode_json($response->content);

    my @expected_keys = qw(at comment next num_rows prev result rows status_code);
    $ok = 0 if keys %$json != @expected_keys;

    for my $expected_key (@expected_keys) {
        $ok = 0 unless exists $json->{ $expected_key };
    }

    $tb->ok($ok, "Looks like a ZAPI response");
    return $json;
}
=head2 zs_done_testing

Expand the default done_testing a bit with a static no warnings check instead
of relying on do_end_test (which only runs in some cases).

=cut

sub zs_done_testing {
    Test::NoWarnings::had_no_warnings();
    $Test::NoWarnings::do_end_test = 0;
    $tb->done_testing(@_);
}

sub read_test_config {
    my $config = eval {
        Config::Auto::parse('zaaksysteem-test.conf', path => ['./etc', '/etc/zaaksysteem/']);
    };
    if($@) {
        $tb->BAIL_OUT("zaaksysteem-test.conf not found");
    }

    return check(
        {
            db_host              => {required => 0},
            db_user              => {required => 0},
            db_password          => {required => 0},
            create_db_user       => {required => 0},
            create_db_password   => {required => 0},
            filestore_location   => {required => 1},
            filestore_test_file_path => {required => 1},
            filestore_test_file_name => {required => 1},
        },
        $config,
    );
}

sub _connect_template {
    my $args = shift;

    # Assumptions :)
    my $dsn = "dbi:Pg:dbname=template1";
    $dsn .= ";host=$args->{db_host}" if defined $args->{db_host};

    return DBI->connect(
        $dsn,
        $args->{create_db_user},
        $args->{create_db_password},
        {
            RaiseError => 1,
            PrintError => 0,
            AutoCommit => 1,
        },
    );
}

our $zs;
our $schema;

sub initialize_test_globals_ok {
    if ($zs) {
        $tb->ok(1, "Test framework already initialized");
        return;
    }

    $zs = Zaaksysteem::TestUtils->new();
    $schema = $zs->schema;

    $tb->ok(1, "Test framework initialized");
    return;
}

1;

package FakeLogger;
use Moose;
use namespace::autoclean;

=head1 NAME

FakeLogger - Log class that stores everything in memory for testing

=head1 DESCRIPTION

Sometimes it's useful to test whether the code under test logged the expected
values. This module helps with that.

=head1 SYNOPSIS

    my $log = FakeLogger->new();

    my $something = Something->new(log => $log);
    $something->do_work();

    # Retrieve log entries:
    is(
        $log->log->{debug}[0],
        "This is debug",
        "First line of 'debug' log is as expected"
    );

    # This empties out the log:
    $log->reset();

=head1 METHODS

=head2 Log methods

The following log methods are supported:

=over

=item debug

=item info

=item warn

=item error

=item fatal

=back

These all store their logs in the appropriate key of the "log" attribute.

=head2 reset

Resets the "log" attribute to an empty hash.

=cut

has "log" => (
    is       => 'rw',
    isa      => 'HashRef',
    init_arg => undef,
    default  => sub { {} },
);

sub reset { shift->log({}); }

sub debug { push @{ shift->log->{debug} }, @_; }
sub info  { push @{ shift->log->{info} },  @_; }
sub warn  { push @{ shift->log->{warn} },  @_; }
sub error { push @{ shift->log->{error} }, @_; }
sub fatal { push @{ shift->log->{fatal} }, @_; }

__PACKAGE__->meta->make_immutable();

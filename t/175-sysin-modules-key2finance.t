#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Backend::Sysin::Modules::Key2Finance;


sub check_export_case {
    my $aanvrager = shift;

    my $module = Zaaksysteem::Backend::Sysin::Modules::Key2Finance->new;

    my $case = $zs->create_case_ok(aanvragers => [$aanvrager]);

    return $module->export_case({
        case => $case,
        attributes => $module->attribute_list,
        naw_attributes => $module->attribute_list_naw
    });
}


$zs->zs_transaction_ok(sub {

    my $aanvrager = $zs->create_aanvrager_np_ok;
    my $result = check_export_case($aanvrager);

    is $result->{stdnaw_row}->{NAAM},
        $aanvrager->{create}->{'np-geslachtsnaam'},
        "Natuurlijk persoon geslachtsnaam filled in correctly";

}, 'Tested: Key2Finance export_case - natuurlijk_persoon');



$zs->zs_transaction_ok(sub {

    my $aanvrager = $zs->create_aanvrager_bedrijf_ok;
    my $result = check_export_case($aanvrager);

    is $result->{stdnaw_row}->{NAAM},
        $aanvrager->{create}->{'handelsnaam'},
        "Bedrijf handelsnaam filled in correctly";

}, 'Tested: Key2Finance export_case - bedrijf');


zs_done_testing();
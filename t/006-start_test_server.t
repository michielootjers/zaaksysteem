#! /usr/bin/perl
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
# Start met ZS_CATALYST_TRACE=1 om Catalyst debug output te zien.
start_test_server_ok();
zs_done_testing();

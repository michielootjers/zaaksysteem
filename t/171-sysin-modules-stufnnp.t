#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use File::Spec::Functions;


use_ok('Zaaksysteem::Backend::Sysin::Modules::STUFNNP');


sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                mk_async_url    => 'http://localhost:3331/stuf',
                mk_sync_url     => 'http://localhost:3331/stuf',
                mk_ontvanger    => 'TESM',
                %{ $params }
            }
        },                                
    );  
}


$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 1',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'rechtsvorm'                => '41',
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_woonplaats'      => 'Amsterdam',
        },
    };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/101-nnp-create-mintlab.xml')
                                ),
    });

    ok($transaction, 'NNP Transaction completed');
    ok(!$transaction->error_count, 'NNP Transaction: no errors');
    is($transaction->success_count, 1, 'NNP Transaction: 1 success');
    is($transaction->external_transaction_id, 'MK0002680371', 'PRS Transaction: external transaction id');
    is($transaction->automated_retry_count, undef, 'NNP Transaction: automated retry count');
    ok($transaction->date_created, 'NNP Transaction: date created');
    ok(!$transaction->date_next_retry, 'NNP Transaction: no date next_retry');
    ok($transaction->date_last_retry, 'NNP Transaction: date last retry');


    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;

    is($record->transaction_record_to_objects, 1, 'Got one mutation');

    my $bms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    );

    is($bms->count, 1, 'Got single Bedrijf mutation record');

    my $bm         = $bms->first;

    my $b          = $schema->resultset('Bedrijf')->find(
        $bm->local_id
    );

    #note(explain({ $b->get_columns }));
    ok($b->authenticated, 'Record is authenticated');
    is($b->authenticatedby, 'kvk', 'Record is authenticated by GBA');

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'Bedrijf',
                'local_id'      => $b->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $b->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Tested: Create single PRS mutation');


$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/121-nnp-update-mintlab.xml')
                                ),
    });

    like($transaction->records->first->output, qr/no_entry_found/, 'Missing subscription');
}, 'Tested: NNP Update Mutation, missing object subscription');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 11',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'rechtsvorm'                => '41',
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_woonplaats'      => 'Amsterdam',
            'vestigingsnummer'          => '556738987634',
        },
    };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/101-nnp-create-mintlab.xml')
                                ),
    });


    ###
    ### MUTATE CREATED ENTRY
    ###

    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/121-nnp-update-mintlab.xml')
                                ),
    });


    my $record     = $second_transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found Bedrijf mutation');

    my $np          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully mutated NNP entry');

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $np->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Tested: NNP Update Mutation');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 11',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_woonplaats'      => 'Amsterdam',
        },
    };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    my $existing_bedrijf = $schema->resultset('Bedrijf')->create(
        {
            fulldossiernummer       => '519874650001',
            authenticated           => 1,
            authenticatedby         => 'kvk',
            handelsnaam             => 'Mintlab B.V. 0001',
        }
    );
    ###
    ### MUTATE CREATED ENTRY
    ###

    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/121-nnp-update-mintlab.xml')
                                ),
    });


    my $record     = $second_transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found Bedrijf mutation');

    my $np          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully mutated NNP entry');

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $np->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    is($existing_bedrijf->id, $np->id, 'Existing Bedrijf overwritten with new subscription');


}, 'Tested: NNP Update Mutation BWCOMPAT mode');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 21',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'vestiging_straatnaam'      => 'Voorstraat',
            'vestiging_huisnummer'      => '43',
            'vestiging_woonplaats'      => 'Vianen',
        },
    };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/102-nnp-create-bug_no_address.xml')
                                ),
    });

    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    my $b          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $b->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Tested: NNP Missing Addres Bug');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/101-nnp-create-mintlab.xml')
                                ),
    });

    my $record     = $transaction->records->first;

    is(
        $schema->resultset('ObjectSubscription')->search_active->count,
        1,
        'Single object subscription'
    );

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found Bedrijf mutation');

    ###
    ### DELETE CREATED ENTRY
    ###
    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/131-nnp-delete-mintlab.xml')
                                ),
    });

    my $np          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully deleted NNP entry');
    ok($np->deleted_on, 'Bedrijf is deleted');

    ok(
        !$schema->resultset('ObjectSubscription')->search_active->count,
        'Object subscription removed'
    );
}, 'Tested: PRS Delete');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },                                
    );

    my $inserted_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, '0204', '/nnp/101-nnp-create-mintlab.xml')
                                ),
    });

    my $transaction = $interface->process_trigger(
        'disable_subscription',
        {
            subscription_id => $interface->object_subscriptions->first->id,
        }
    );

}, 'Tested: PRS remove afnemerindicatie');


zs_done_testing();

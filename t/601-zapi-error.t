#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use Data::FormValidator;
use Zaaksysteem::Log::CallingLogger;

BEGIN { use_ok('Zaaksysteem::ZAPI::Error'); }

$zs->zs_transaction_ok(sub {
    my $zapi_error   = Zaaksysteem::ZAPI::Error->new(
        messages    => 'Internal Server Error',
        type        => 'test/error',
    );

    my $response    = $zapi_error->response;

    ok(
        exists($response->{result}->[0]->{$_}),
        'Found attribute: ' . $_
    ) for qw/messages type stacktrace data/;
}, 'Error Handling');


$zs->zs_transaction_ok(sub {
    my $profile     = {
        required            => [qw/huisnummer straat/],
        optional            => [qw/toevoeging huisletter/],
        constraint_methods  => {
            'huisletter'        => qr/^\w{0,1}$/,
        }
    };

    my $result      = Data::FormValidator->check(
        {
            straat      => 'marnixstraat',
            toevoeging  => 'rechts',
            huisletter  => 'bier',
        },
        $profile
    );

    my $zapi_error   = Zaaksysteem::ZAPI::Error->new(
        type        => 'ZAPIProfile',
        data        => $result
    );

    ok(UNIVERSAL::isa($zapi_error->data, 'ARRAY'), 'Data is correct array');
    is(@{ $zapi_error->data }, 2, 'Got two errors');

    for my $paramdata (@{ $zapi_error->data }) {
        if ($paramdata->{parameter} eq 'huisnummer') {
            is($paramdata->{result}, 'missing', 'Huisnummer missing');
        } elsif ($paramdata->{parameter} eq 'huisletter') {
            is($paramdata->{result}, 'invalid', 'Huisletter invalid');
        }
    }
}, 'ZAPIProfile Error handling');



zs_done_testing();

#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use Net::LDAP;
use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Betrokkene;

my $logger = Zaaksysteem::Log::CallingLogger->new();
isa_ok($logger, 'Zaaksysteem::Log::CallingLogger');

my $basedn          = 'o=zaaksysteem,dc=zaaksysteem,dc=nl';
my $customer_config = {
    start_config => {
        template    => 'zaak_v1',
        customer_id => 100,
        files       => 't/inc/files',
        publish     => 1,
        LDAP        => {basedn => 'o=zaaksysteem,dc=zaaksysteem,dc=nl'}
    },
};

my $config = {
    LDAP => {
        hostname => 'localhost',
        port     => '389',
        admin    => 'cn=admin,dc=zaaksysteem,dc=nl',
        password => 'zaaksysteem123',
    },
};

my $ldap = Net::LDAP->new();
my $log  = Zaaksysteem::Log::CallingLogger->new();

my $model = Zaaksysteem::Betrokkene->new(
    customer => $customer_config,
    config   => $config,
    log      => $log,
    ldap     => $ldap,
    dbic     => $schema,
);
isa_ok($model, 'Zaaksysteem::Betrokkene');


sub create_natuurlijk_persoon_ok {
    my $create = {
        'np-voornamen'           => 'Anton Ano',
        'np-voorletters'         => 'A.A.',
        'np-geslachtsnaam'       => 'Zaaksysteem',
        'np-huisnummer'          => 42,
        'np-postcode'            => '1011PZ',
        'np-straatnaam'          => 'Muiderstraat',
        'np-woonplaats'          => 'Amsterdam',
        'np-geslachtsaanduiding' => 'M',
    };
    my $np = $model->create('natuurlijk_persoon', $create);
    ok($np, "Natuurlijk persoon is aangemaakt");

    return $np;
}

# various checks to see that the cache has been filled up properly
sub check_cache {
    my ($cache, $input) = @_;

    ok exists $cache->{__Betrokkene_Cache}, "Creates cache member";

    my $betrokkene_cache = $cache->{__Betrokkene_Cache};
    isa_ok $betrokkene_cache, "HASH", "Cache member is a hashref";

    my $cache_key = $model->generate_cache_key;
    is $betrokkene_cache->{$cache_key}, $input, "Input is stored in given cache";
}


$zs->zs_transaction_ok(sub {
    my $options = {};

    my %cache_keys;
    my $cache_key = $model->generate_cache_key($options);
    $cache_keys{$cache_key} = 1;

    for my $key (qw/intern extern type id/) {
        $options->{$key} = 1;
        $cache_key = $model->generate_cache_key($options);
        $cache_keys{$cache_key} = 1;
    }

    is scalar keys %cache_keys, 5, "Created a unique key for all options";

}, "Unit test: generate_cache_key");


$zs->zs_transaction_ok(sub {

    ok !$model->set_cache, "Simply returns if called without params";

}, "Unit test: _bo_set_cache input validation");


$zs->zs_transaction_ok(sub {
    my $cache = {};
    my $input = "something";
    $model->set_cache({stash => $cache}, $input);

    check_cache($cache, $input);

    is $model->from_cache({stash => $cache}), $input, "Returns input from cache";

}, "Unit test: _bo_set_cache on stash");


$zs->zs_transaction_ok(sub {
    my $cache = {};
    $model->dbic->cache($cache);

    my $input = "something else";
    $model->set_cache({}, $input);

    check_cache($cache, $input);

    is $model->from_cache, $input, "Returns input from cache";

}, "Unit test: _bo_set_cache on schema");



$zs->zs_transaction_ok(sub {
    my $id = create_natuurlijk_persoon_ok;

    ok $id, "Created natuurlijk_persoon-" . $id;
    # TODO: make it bork when no proper input is passed

    ok !$model->get({}, ""), "Simply returns if the id parameter doesn't have something remotely useful";

    # TODO figure out the scenarios for the type input

    ok $model->get({ type => 'natuurlijk_persoon' }, $id), "Found natuurlijk_persoon-$id";

    ok !$model->get({ type => 'natuurlijk_persoon' }, $id + 1), "Didnt find natuurlijk_persoon-" . ($id + 1);

    my $cache = {};

    ok $model->get({ type => 'natuurlijk_persoon', stash => $cache }, $id), "Found natuurlijk_persoon-$id";

    my $cache_key = $model->generate_cache_key({type => 'natuurlijk_persoon', id => $id});

    ok exists $cache->{__Betrokkene_Cache}->{$cache_key}, "Cache has been populated";
    my $cached_betrokkene = $cache->{__Betrokkene_Cache}->{$cache_key};
    is $cached_betrokkene->{gmid}, $id, "Betrokkene id is present in cached object";

    my $display_name = $model->get({}, 'betrokkene-natuurlijk_persoon-' .$id)->display_name;
    is $display_name, 'Anton Ano Zaaksysteem', "Display name retrieved properly";

    my $cached_display_name = $cached_betrokkene->display_name;
    is $display_name, $cached_display_name, "Cached display_name equals direct display_name";
}, "Get betrokkene display_name");



# my $BETROKKENE_TYPES = {
# 'medewerker'            => 'Medewerker',
# 'natuurlijk_persoon'    => 'NatuurlijkPersoon',
# 'org_eenheid'           => 'OrgEenheid',
# 'bedrijf'               => 'Bedrijf',
# };

$zs->zs_transaction_ok(
    sub {
        my $mw = $model->create('medewerker', {});
        ok(!$mw, "Medewerkers worden aangemaakt via LDAP!");
    },
    "Zaaksysteem::Betrokkene::Medewerker"
);

$zs->zs_transaction_ok(
    sub {
        my $create = {
            'np-voornamen'           => 'Anton Ano',
            'np-voorletters'         => 'A.A.',
            'np-geslachtsnaam'       => 'Zaaksysteem',
            'np-huisnummer'          => 42,
            'np-postcode'            => '1011PZ',
            'np-straatnaam'          => 'Muiderstraat',
            'np-woonplaats'          => 'Amsterdam',
            'np-geslachtsaanduiding' => 'M',
        };
        my $np = $model->create('natuurlijk_persoon', $create);
        ok($np, "Natuurlijk persoon is aangemaakt");
    },
    "Zaaksysteem::Betrokkene::NatuurlijkPersoon"
);

zs_done_testing();

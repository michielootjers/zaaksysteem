#!perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;

use Zaaksysteem::Backend::Object;

initialize_test_globals_ok;

$zs->zs_transaction_ok(sub {
    my $object = $zs->create_object_data_ok;

    lives_ok { $object->object_actions } 'object_actions call lives';

    ok !defined $object->object_actions, 'object_actions is undef by default';
}, 'plain object allows action retrieval');

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    my $objects = Zaaksysteem::Backend::Object->new(schema => $zs->schema)->rs;

    my $object = $objects->find({
        object_class => 'case',
        object_id => $case->id
    });

    lives_ok
        { $object->object_actions }
        'object_actions is callable for case objects';

    is ref $object->object_actions, 'ARRAY',
        'object_actions for case returns arrayref';
}, '');

zs_done_testing;

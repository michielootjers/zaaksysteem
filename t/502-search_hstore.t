#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;### Test header end

use utf8;
use DateTime;
use Encode qw(encode_utf8);
use Test::DummyResultSet;
use Zaaksysteem::Search::HStoreResultSet;
use Zaaksysteem::Search::Term::Literal;
use Zaaksysteem::Search::Term::Column;
use Zaaksysteem::Search::Conditional;

use Moose::Util qw(apply_all_roles);

{
    my $obj = Test::DummyResultSet->new();
    apply_all_roles($obj, 'Zaaksysteem::Search::HStoreResultSet');
    my $now = DateTime->now();

    $obj->search_hstore(
        Zaaksysteem::Search::Conditional->new(
            lterm => Zaaksysteem::Search::Conditional->new(
                lterm => Zaaksysteem::Search::Conditional->new(
                    lterm => Zaaksysteem::Search::Term::Literal->new(value => 'foo'),
                    rterm => Zaaksysteem::Search::Term::Literal->new(value => 'bar'),
                    operator   => '<=>',
                ),
                rterm => Zaaksysteem::Search::Conditional->new(
                    lterm => Zaaksysteem::Search::Term::Column->new(value => 'baz'),
                    rterm => Zaaksysteem::Search::Term::Literal->new(value => 'quux'),
                    operator   => '=',
                ),
                operator => 'and',
            ),
            rterm => Zaaksysteem::Search::Conditional->new(
                lterm => Zaaksysteem::Search::Conditional->new(
                    lterm => Zaaksysteem::Search::Term::Column->new(value => 'aap'),
                    rterm => Zaaksysteem::Search::Term::Literal->new(value => $now),
                    operator   => '=',
                ),
                rterm => Zaaksysteem::Search::Conditional->new(
                    lterm => Zaaksysteem::Search::Term::Column->new(value => 'noot'),
                    rterm => Zaaksysteem::Search::Term::Literal->new(value => $now),
                    operator   => '>',
                ),
                operator => 'and',
            ),
            operator => 'and',
        ),
    );

    #diag(explain($obj->search_args->[0][0]));
    is_deeply(
        $obj->search_args->[0][0],
        \[
            '(((? <=> ?) and (hstore_properties @> ?)) and ((hstore_properties @> ?) ' .
            'and (CAST(hstore_properties->noot (quoted) AS TIMESTAMP) > ?)))',
            [ {}, 'foo' ],
            [ {}, 'bar'],
            [ {}, '"baz" => "quux"'],
            [ {}, '"aap" => "' . $now->iso8601 . '"'],
            [ {}, $now],
        ],
        '->search() was called correctly.'
    );
    is($obj->search_args->[0][1], undef, "No options were passed.");
}

{
    my $obj = Test::DummyResultSet->new();
    apply_all_roles($obj, 'Zaaksysteem::Search::HStoreResultSet');

    $obj->search_hstore(
        Zaaksysteem::Search::Conditional->new(
            lterm => Zaaksysteem::Search::Term::Column->new(value => 'foo'),
            rterm => Zaaksysteem::Search::Term::Literal->new(value => 6.66),
            operator   => '<',
        ),
        { 'some' => 'option' },
    );

    #diag(explain($obj->search_args->[0][0]));
    is_deeply(
        $obj->search_args->[0][0],
        \[
            '(CAST(hstore_properties->foo (quoted) AS NUMERIC) < ?)',
            [ {}, '6.66' ], 
        ],
        '->search() was called correctly.'
    );
    is_deeply(
        $obj->search_args->[0][1],
        { 'some' => 'option' },
        "Options were passed.",
    );
}

{
    my $obj = Test::DummyResultSet->new();
    apply_all_roles($obj, 'Zaaksysteem::Search::HStoreResultSet');

    $obj->search_hstore(
        Zaaksysteem::Search::Conditional->new(
            lterm => Zaaksysteem::Search::Conditional->new(
                lterm => Zaaksysteem::Search::Term::Column->new(value => 'föö'),
                rterm => Zaaksysteem::Search::Term::Literal->new(value => 'bår'),
                operator => '=',
            ),
            rterm => Zaaksysteem::Search::Conditional->new(
                lterm => Zaaksysteem::Search::Term::Column->new(value => 'föö'),
                rterm => Zaaksysteem::Search::Term::Literal->new(value => 'bår'),
                operator => '>',
            ),
            operator => 'and',
        ),
        { 'some' => 'option' },
    );

    #diag(explain($obj->search_args->[0][0]));
    is_deeply(
        $obj->search_args->[0][0],
        \[
            encode_utf8('((hstore_properties @> ?) and (hstore_properties->föö (quoted) > ?))'),
            [ {}, encode_utf8(qq{"föö" => "bår"}) ],
            [ {}, encode_utf8(qq{bår}) ],
        ],
        '->search() was called correctly (with a complex query).'
    );
    is_deeply(
        $obj->search_args->[0][1],
        { 'some' => 'option' },
        "Options were passed.",
    );
}

{
    my $obj = Test::DummyResultSet->new();
    apply_all_roles($obj, 'Zaaksysteem::Search::HStoreResultSet');

    $obj->search_hstore(
        Zaaksysteem::Search::Conditional->new(
            lterm => Zaaksysteem::Search::Term::Column->new(value => 'foo'),
            rterm => Zaaksysteem::Search::Term::Literal->new(value => 123),
            operator   => '=',
        ),
        { select => { distinct => 'foobar' } }
    );

    #diag(explain($obj->search_args->[0][0]));
    is_deeply(
        $obj->search_args->[0][0],
        \[
            '(hstore_properties @> ?)',
            [ {}, '"foo" => "123"'],
        ],
        '->search() was called correctly.'
    );
    is_deeply(
        $obj->search_args->[0][1],
        {
#   Failed test '->search() was called correctly.'
#   at t/502-search_hstore.t line 61.
#     Structures begin differing at:
#     ${     $got}->[1] = ARRAY(0x97e1938)
#     ${$expected}->[1] = 'foo'

            select => { distinct => 'hstore_properties->foobar (quoted)' },
        },
        'select/distinct clause parsed and replaced correctly',
    );
}

{
    my $rs = Test::DummyResultSet->new;
    apply_all_roles($rs, 'Zaaksysteem::Search::HStoreResultSet');

    is_deeply $rs->_parse_option_group_by([qw[a b c]]), [
        'hstore_properties->a (quoted)',
        'hstore_properties->b (quoted)',
        'hstore_properties->c (quoted)'
    ], 'select/group_by _parse_option_group_by returns quoted columnnames';

    $rs->search_hstore(undef, { group_by => [qw[a b c]] });

    is_deeply $rs->search_args->[0][1], {
        as => [ 'count' ],
        select => [ { count => 'hstore_properties' } ],
        group_by => [
            'hstore_properties->a (quoted)',
            'hstore_properties->b (quoted)',
            'hstore_properties->c (quoted)',
        ]
    }, 'select/group_by clause parsed and mangled properly';

    throws_ok sub { $rs->search_hstore(undef, { group_by => { a => 1 } }) },
        'Zaaksysteem::Exception::Base',
        'select/group_by with incorrect callstyle throws an exception';
}

{
    my $obj = Test::DummyResultSet->new();
    apply_all_roles($obj, 'Zaaksysteem::Search::HStoreResultSet');

    throws_ok(
        sub {
            $obj->search_hstore(
                Zaaksysteem::Search::Conditional->new(
                    lterm => Zaaksysteem::Search::Term::Column->new(value => 'foo'),
                    rterm => Zaaksysteem::Search::Term::Literal->new(value => 123),
                    operator   => '=',
                ),
                {select => [ { not_distinct => [] } ] }
            );
        },
        'Zaaksysteem::Exception::Base',
        '"select" option with no "distinct" key in the hashref.',
    );
}

{
    my $rs = Test::DummyResultSet->new;
    apply_all_roles($rs, 'Zaaksysteem::Search::HStoreResultSet');

    $rs->search_hstore(undef, { order_by => { alphanumeric => [ { -asc => 'case.id' } ] } });

    is_deeply $rs->search_args->[0][1],
        { order_by => [ { -asc => 'hstore_properties->case.id (quoted)' } ] },
        'hstore/order_by returns quoted hstore_properties key';
}

{
    my $rs = Test::DummyResultSet->new;
    apply_all_roles($rs, 'Zaaksysteem::Search::HStoreResultSet');

    $rs->search_hstore(undef, { order_by => { numeric => [ { -asc => 'case.id' } ] } });

    is_deeply(
        $rs->search_args->[0][1],
        {
            order_by => [
                {
                    -asc => \[
                        '(NULLIF(SUBSTRING(hstore_properties->case.id (quoted) FROM \'([0-9]+(\\.[0-9]+)?)\'), \'\'))::NUMERIC'
                    ]
                }
            ]
        },
        'hstore/order_by with numeric option returns quoted and casted hstore_properties field',
    );
}

{
    my $rs = Test::DummyResultSet->new;
    apply_all_roles($rs, 'Zaaksysteem::Search::HStoreResultSet');

    $rs->search_hstore(undef, { order_by => { alphanumeric => [ { -asc => 'case.id' } ] } });

    is_deeply $rs->search_args->[0][1],
        { order_by => [ { -asc => 'hstore_properties->case.id (quoted)' } ] },
        'hstore/order_by with numeric option returns quoted and non-casted hstore_properties field';
}

zs_done_testing();

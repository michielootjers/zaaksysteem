#! perl

### Test header start
use warnings;
use strict;
use Data::Dumper;
$Data::Dumper::Maxdepth = 2;
use JSON;
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use File::Spec::Functions;

use_ok('Zaaksysteem::Backend::Sysin::Modules::Buitenbeter');

my @buitenbeter_fields = Zaaksysteem::Backend::Sysin::Modules::Buitenbeter::BUITENBETER_ATTRIBUTES;
ok scalar @buitenbeter_fields > 0, "Buitenbeter fields loaded";



# because cases with many attributes are created, it's much faster to delay that to the end.
$schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

# 15 minutes
use constant QUARTER => 60 * 15;

sub read_directory {
    my $directory = shift;

    my $dh;
    opendir $dh, $directory;
    my @files = readdir $dh;
    closedir $dh;

    return @files;
}

sub create_casetype {
    my ($opts) = shift;
    my $zaaktype_definitie = $zs->create_zaaktype_definitie_ok(
        preset_client => 'betrokkene-natuurlijk_persoon-1'
    );
    my $zaaktype_node = $zs->create_zaaktype_node_ok(
        zaaktype_definitie => $zaaktype_definitie
    );

    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    foreach my $field (@buitenbeter_fields) {

        # allow the testscript to generate incomplete casetypes
        if (my $exclude = $opts->{exclude_fields}) {
            next if exists $exclude->{$field};
        }

        my $bibliotheek_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
            naam         => $field,
            magic_string => 'magic_string_' . $field,
            value_type   => 'text'
        );

        my $zaaktype_kenmerk = $zs->create_zaaktype_kenmerk_ok(
            status              => $zaaktype_status,
            bibliotheek_kenmerk => $bibliotheek_kenmerk
        );
    }

    $zs->create_zaaktype_regel_ok(
        status => $zaaktype_status,
        # node => $zaaktype_node,
        naam => 'Buitenbeter status update',
        settings => {
          'actie_1' => 'send_external_system_message',
          'actie_1_message' => 'Melding is in behandeling genomen door Zaaksysteem',
          'actie_1_status' => 'In behandeling',
          'actie_1_type' => 'buitenbeter',
          'acties' => '1',
          'naam' => 'Status bericht versturen',
          'voorwaarde_1_kenmerk' => 'aanvrager',
          'voorwaarde_1_kenmerk_previous' => 'aanvrager',
          'voorwaarde_1_value' => [
                                    'Natuurlijk persoon',
                                    'Niet natuurlijk persoon'
                                  ],
          'voorwaarde_1_value_checkbox' => '1',
          'voorwaarden' => '1'
        }
    );

    return $casetype;
}

sub create_config_interface {
    my ($casetype, $options) = @_;

    $options ||= {};

    # prefix is used to purposely mess up the config. this can happen in
    # the wild when a configuration is made, and then the casetype is updated
    # and a kenmerk is removed.
    my $mess_up = $options->{mess_up} || {};

    my @attributes = map {{
        attribute_type => 'magic_string',
        case_type_id => $casetype->id,
        checked => 1,
        external_name => $_,
        internal_name => {
            searchable_object_id => ($mess_up->{$_} || ''). 'magic_string_' . $_,
            searchable_object_label => $_,
            searchable_object_type => 'magicstring'
        },
        optional => 0
    }} @buitenbeter_fields;

    my $interface = $zs->create_named_interface_ok({
        module              => 'buitenbeter',
        name                => 'Buitenbeter',
        interface_config    => {
            endpoint     => 'https://meldingtest.yucat.com:6443/interfaces/emps/MeldingService.svc',
            username     => 'test',
            password     => 'test',
            contracttype => '8',
            gemeente_id  => '377',
            spoofmode    => 1,
            interval     => 30
        },
        zaaktype => $casetype,
    });

    # misconfigure on purpose
    unless ($options->{no_attributes}) {
        $interface->set_attribute_mapping({attributes => \@attributes});
    }

    return $interface;
}

sub get_buitenbeter_module {
    return new Zaaksysteem::Backend::Sysin::Modules::Buitenbeter;
}


sub check_logging {
    my ($case_id) = @_;

    my $logging = $schema->resultset('Logging')->search({
        zaak_id => $case_id,
        event_type => 'case/send_external_system_message'
    });

    ok $logging->count == 1, "Logging entry for send_external_system_message present";
    my $entry = $logging->first;
    my $event_data = from_json($entry->event_data);

    ok $event_data->{destination} eq 'Buitenbeter', 'Destination set correctly';
    ok $event_data->{input} =~ m/PostStatusUpdate/, "PostStatusUpdate present in input";
    ok $event_data->{output} =~ m|<a:result>true</a:result>|, "Positive result in output";
}

sub check_first_case {
    my ($interface, $case_id) = @_;

    my $case = $schema->resultset('Zaak')->find($case_id);
    isa_ok($case, 'Zaaksysteem::Model::DB::Zaak');
    my $field_values = $case->field_values;

    # contains naam => id pairs
    my %magic_strings_lookup = map {
        $_->bibliotheek_kenmerken_id->naam => $_->bibliotheek_kenmerken_id->id
    } $interface->case_type_id->zaaktype_node_id->zaaktype_kenmerken->search->all;

    ok scalar keys %magic_strings_lookup, 'lookup contains keys';

    my $reference = {
        datum => '2012-09-27T11:34:16',
        buitenbeter_id => '37324',
        'melder.geslacht' => 'V',
        'melder.voornaam' => 'Barbara',
        'melder.telefoonnummer' => '0343529529',
        'melder.email' => 'h.devries@yucat.com',
        probleem => 'Eikenprocessierups'
    };

    foreach my $field (keys %$reference) {
        my $bibliotheek_kenmerken_id = $magic_strings_lookup{$field};
        ok $field_values->{$bibliotheek_kenmerken_id} eq $reference->{$field}, "field $field has been correctly saved in the case";
    }

    check_logging($case_id);
}


sub verify_geo_result {
    my ($result, $expected) = @_;

    ok ref $result eq 'HASH', "result is a hash";
    ok exists $result->{success}, 'has a success member';
    ok exists $result->{result}, 'has a result member';

    return $result->{success} ?
        $result->{result} eq $expected :
        $result->{result} eq '';
}


# getnewmeldingen twice, must not add new cases
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $interface = create_config_interface($casetype);
    my $module = get_buitenbeter_module;
    $module->interface($interface);

    my $test_responses = $interface->process_trigger('get_test_responses');

    ok $interface->get_interface_config->{interval} == 30, "interval is set to 30 minutes";

    my $cases = $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => QUARTER * 2,
    });

    ok scalar @$cases == 2, '2 cases created';

    ok !$module->find_case_with_buitenbeter_id('1234'), "Not found a case with bb id 1234";

    # ids come from the demo xml file
    ok $module->find_case_with_buitenbeter_id('41051'), "Found a case with bb id 41051";
    ok $module->find_case_with_buitenbeter_id('37324'), "Found a case with bb id 37324";

    # now do it again, the system should not create new cases since
    # we already have those
    $cases = $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => QUARTER * 2,
    });

    ok scalar @$cases == 0, 'No new cases created, already present';


    # override so we can test :)
    $cases = $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => QUARTER * 2,
        allow_duplicates => 1,
    });

    ok scalar @$cases == 2, 'Two new cases created in override';


}, 'Tested: Buitenbeter check for existing doubles');


# purposely mess up configuration
$zs->zs_transaction_ok(sub {
    # make an incompletely configured casetype
    my $casetype = create_casetype({
        exclude_fields => { straat => 1}
    });

    my $interface = create_config_interface($casetype, {
        mess_up => {
            'melder.straat' => 'bla'
        }
    });

    my $test_responses = $interface->process_trigger('get_test_responses');

    ok $interface->get_interface_config->{interval} == 30, "interval is set to 30 minutes";

    my $cases = $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => QUARTER * 2,
    });

    ok scalar @$cases == 2, '2 cases created';

    # analyze the first case, see if it's field values match
    my ($case_id) = @$cases;

    check_first_case($interface, $case_id);

}, 'Tested: Buitenbeter kenmerk misconfig');


## TODO: This should not be in BuitenBeter, we do BAG/Geo lookups elsewhere too?
# verify geo lookup
#$zs->zs_transaction_ok(sub {
#    my $module = get_buitenbeter_module;
#
#    my $result = $module->lookup_geo_address({
#        latitude => '21211212',
#        longitude => 'dffdfd'
#    });
#
#    ok verify_geo_result($result), "Geo lookup for bs coordinaties returns correct result";
#
#    $result = $module->lookup_geo_address({
#        latitude => '51.5349429120725',
#        longitude => '4.46232474231697'
#    });
#
# ok verify_geo_result($result, "Dunantstraat 59, 4701 GZ Roosendaal, Nederland"),
#        "Geo lookup for known place gives back correct result";
#
#}, 'Tested: Buitenbeter bag_address lookup');



# verify bag address lookup
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $module = get_buitenbeter_module;
    my $interface = create_config_interface($casetype);
    $module->interface($interface);

    my $rs = $schema->resultset('BagOpenbareruimte');

    ok $rs->count == 0, "Initially no address in table";

    my $location = {
        straat => 'Dam',
        huisnummer => '42',
        plaats => 'Amsterdam'
    };

    my $bag_id = $module->lookup_bag_address($location);
    is $bag_id, '', "No match from empty database";

    $zs->create_bag_records;
    is $rs->count, 1, "One address in table";

    is $module->lookup_bag_address($location), '', "No match from non-existing address";

    $bag_id = $module->lookup_bag_address({
        straat => 'Donker Curtiusstraat',
        huisnummer => '23',
        plaats => 'Niet-Amsterdam'
    });

    is $bag_id, '', 'No match if woonplaats is different';

    my $expected_bag_id = 'nummeraanduiding-9876543218375842';
    $bag_id = $module->lookup_bag_address({
        straat => 'Donker Curtiusstraat',
        huisnummer => '23',
        plaats => 'Amsterdam'
    });
    is $bag_id, $expected_bag_id, "Match from existing address";

    # test integration in preprocess_value
    $bag_id = $module->preprocess_value({
        melding => {
            straat => 'Donker Curtiusstraat',
            huisnummer => '23',
            plaats => 'Amsterdam'
        },
        external_name => 'bag_adres',
        value_type => 'not_bag'
    });

    is $bag_id, undef, "Undef returned if value_type is not bag_*";

    # test integration in preprocess_value
    $bag_id = $module->preprocess_value({
        melding => {
            straat => 'Donker Curtiusstraat',
            huisnummer => '23',
            plaats => 'Amsterdam',
            melder => {
                straat => 'Dam',
                huisnummer => '1'
            }
        },
        external_name => 'bag_adres',
        value_type => 'bag_something'
    });

    is $bag_id, $expected_bag_id, "Match from existing melding address - integrated, used melding address, not melder's.";

}, 'Tested: Buitenbeter bag_address lookup');


# verify check_schedule
$zs->zs_transaction_ok(sub {

    my $module = get_buitenbeter_module;

    ok $module->check_schedule({time => QUARTER * $_, interval_minutes => 15}) == 1,
        "if interval is 1 quarter, always return true"
            for (qw/0 1 2 5 23 12233/);

    ok $module->check_schedule({time => QUARTER * 2 * $_, interval_minutes => 30}) == 1,
        "if interval is 2 quarters, return true on even quarters"
            for (qw/0 1 2 5 23 12233/);

    ok $module->check_schedule({time => QUARTER *  $_, interval_minutes => 30}) == 0,
        "if interval is 2 quarters, return false for uneven quarters"
            for (qw/1 3 7 5 23 12233/);

    ok $module->check_schedule({time => QUARTER * 4 * $_, interval_minutes => 60}) == 1,
        "if interval is 4 quarters, return true for fourth quarters"
            for (qw/0 1 2 5 23 12233/);

    ok $module->check_schedule({time => QUARTER * $_, interval_minutes => 60}) == 0,
        "if interval is 4 quarters, return false for fourth plus one quarters"
            for (qw/5 21 401/);

}, 'Tested: Buitenbeter check_schedule');


# get_buitenbeter_id misconfig
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $interface = create_config_interface($casetype, {no_attributes => 1});
    my $module = get_buitenbeter_module;
    $module->interface($interface);

    # test both the delegated sub and the overseeing client sub
    throws_ok( sub {
        $module->get_mapped_bibliotheek_kenmerken_id('buitenbeter_id');
    }, qr/Buitenbeter_id is niet gekoppeld aan een kenmerk, controleer de configuratie/,
        "Bork out when no proper attribute config is present");

    throws_ok( sub {
        $module->get_buitenbeter_id;
    }, qr/Buitenbeter_id is niet gekoppeld aan een kenmerk, controleer de configuratie/,
        "Bork out when no proper attribute config present");

}, 'Tested get_buitenbeter_id with misconfig');


# assert_valid_melding
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $module = get_buitenbeter_module;
    my $interface = create_config_interface($casetype);

    my $test_responses = $interface->process_trigger('get_test_responses');
    $module->test_responses($test_responses);

    my $config = $interface->get_interface_config;
    my $response = $module->callSOAP($interface, 'GetNewMeldingen', {
        gemeente_id  => $config->{gemeente_id},
        contracttype => $config->{contracttype}
    });

    ok exists $response->{meldingen}->{Melding}, "meldingen gevonden";
    my $meldingen = $response->{meldingen}->{Melding};
    my ($melding) = @$meldingen;

    throws_ok (sub {
        $module->assert_valid_melding;
    }, qr/Melding niet gevonden/, "fails when no melding is passed");

    lives_ok (sub {
        $module->assert_valid_melding($melding);
    }, "Passes when valid melding is supplied");

    throws_ok (sub {
        delete $melding->{buitenbeter_id};
        $module->assert_valid_melding($melding);
    }, qr/Veld buitenbeter_id niet aanwezig in melding/, "fails when invalid melding is passed");

}, 'Tested: Buitenbeter::validate_melding');


# save_attachment
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $interface = create_config_interface($casetype, {no_attributes => 1});
    my $module = get_buitenbeter_module;
    $module->interface($interface);
    my $case = $zs->create_case_ok;

    throws_ok( sub {
        $module->save_attachment({wrong => 'parameters'});
    }, qr/Validation of profile failed/, 'Exception thrown when called with wrong parameters');

    my $test_responses = $interface->process_trigger('get_test_responses');
    $module->test_responses($test_responses);

    my $config = $interface->get_interface_config;
    my $response = $module->callSOAP($interface, 'GetNewMeldingen', {
        gemeente_id  => $config->{gemeente_id},
        contracttype => $config->{contracttype}
    });

    ok exists $response->{meldingen}->{Melding}, "meldingen gevonden";
    my $meldingen = $response->{meldingen}->{Melding};
    ok @$meldingen == 2, "2 meldingen gevonden";
    my ($melding) = @$meldingen;

    my $tmp_location = $schema->resultset('Config')->get_value('tmp_location');
    ok $tmp_location, "Tmp location found";

    my $original_count = scalar read_directory($tmp_location);

    $module->save_attachment({
        case_id => $case->id,
        preset_client => 'betrokkene-medewerker-1',
        attachment => $melding->{attachment}
    });

    my @files = $case->files;
    ok @files == 1, "One file linked to the case";
    my ($file) = @files;

    ok $file->name eq 'attachment', "file is called attachment";
    ok $file->extension eq '.jpg', "extension is jpg";
    my $filestore = $file->filestore_id;
    ok $filestore->size == 41556, "filesize is correct";


    my $final_count = scalar read_directory($tmp_location);
    is $final_count, $original_count, "No new tmp files have been left hanging";

}, 'Tested get_buitenbeter_id with misconfig');


# the get_buitenbeter_id proper config
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $interface = create_config_interface($casetype);

    my $module = get_buitenbeter_module;
    $module->interface($interface);

    my $kenmerk = $schema->resultset('BibliotheekKenmerken')->search({
        magic_string => 'magic_string_buitenbeter_id'
    })->first;

    ok $kenmerk, "Found stored kenmerk for buitenbeter_id";

    ok $module->get_mapped_bibliotheek_kenmerken_id('buitenbeter_id') == $kenmerk->id, "Found the mapped field";

    my $id = $module->get_buitenbeter_id({
        $kenmerk->id => 'this is the buitenbeter id you are looking for'
    });

    ok $id eq 'this is the buitenbeter id you are looking for', 'Found correct buitenbeter_id';

    throws_ok( sub {
        my $wrong_id = $module->get_buitenbeter_id({
            incorrect_id => 'this is not the buitenbeter id you are looking for'
        });
    }, qr/Buitenbeter_id is leeg in de zaak/, 'Exception thrown when buitenbeter_id not set in the case');

}, 'Tested: Buitenbeter::get_buitenbeter_id');



# make sure we get a tidy test_responses config from the module
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $interface = create_config_interface($casetype);
    my $module = get_buitenbeter_module;

    ok $module->get_test_xml($interface, $_), "xml available for $_"
        for qw/PostStatusUpdate GetNewMeldingen/;

    throws_ok( sub {
        $module->get_test_xml($interface, 'blabla');
    }, qr/kon niet geopend worden/, "no xml available for fake call");

    my $test_responses = $interface->process_trigger('get_test_responses');

    ok ref $test_responses eq 'HASH', 'got a spoof responses hash';

    ok $test_responses->{PostStatusUpdate} &&
        $test_responses->{GetNewMeldingen} &&
        keys %$test_responses == 2, "got two keys with the right actions";

}, 'Tested: Buitenbeter get_test_xml and get_test_responses');


# getnewmeldingen and see that cases are created
$zs->zs_transaction_ok(sub {
    my $casetype = create_casetype;
    my $interface = create_config_interface($casetype);

    my $transaction_records = $schema->resultset('TransactionRecord')->search->count;

    is($transaction_records,  0, "initially no transaction records");

    my $test_responses = $interface->process_trigger('get_test_responses');

    ok $interface->get_interface_config->{interval} == 30, "interval is set to 30 minutes";

    my $cases = $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => QUARTER,
    });

    is(@$cases, 0, "15 minutes means = no action in the scheduler");

    # now try again with 2 quarters, schedule should act
    $cases = $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => QUARTER * 2,
    });

    is scalar @$cases, 2, '2 cases created';

    $transaction_records = $schema->resultset('TransactionRecord')->search->count;
    ok $transaction_records == 3, "three transaction records after single request";

    my @records = $schema->resultset('TransactionRecord')->search(
        undef,
        {order_by => {-asc => 'id'}
    })->all;

    my ($create, $status1, $status2) = @records;
    ok $create->preview_string =~ m|Nieuwe melding|, "Preview string tells create";

    my @mutations = $create->transaction_record_to_objects->search({
        'local_table'           => 'Zaak',
        'transaction_record_id' => $create->id,
    }, {
        order_by => {-asc => 'id'}
    })->all;

    ok scalar @mutations == 2, "Two mutations found";

    for my $index (0..1) {
        ok $mutations[$index]->local_table eq 'Zaak', 'Local table is zaak';
        ok $mutations[$index]->mutation_type eq 'create', 'Mutation type eq create';
        ok $$cases[$index] eq $mutations[$index]->local_id, "case ids match up";
        my $mutations = from_json($mutations[$index]->mutations);
        ok scalar @$mutations == 19, "19 mutations found";
    }

    # analyze the first case, see if it's field values match
    my ($case_id) = @$cases;

    check_first_case($interface, $case_id);

}, 'Tested: Buitenbeter');


zs_done_testing();

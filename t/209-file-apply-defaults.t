#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use JSON;

### Test header end


sub create_files_with_defaults {
    my ($pip, $publish_public) = @_;

    my $case_document = $zs->get_case_document_ok;
    my $subject = $zs->get_subject_ok;

    ok $case_document->update({
        pip => $pip,
        publish_public => $publish_public,
    }), 'PIP publish true, public publish false';

    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by        => $subject,
        },
        case_document_ids => [$case_document->id],
        file_path         => $zs->config->{filestore_test_file_path},
        name              => 'case_document.txt',
    });

    my $second_file = $zs->create_file_ok;

    $second_file->move_case_document({
        subject => $subject,
        from    => $result->id,
        case_document_id => $case_document->id,
    });

    return ($result, $second_file);
}


$zs->zs_transaction_ok(sub {


    my ($first, $second) = create_files_with_defaults(0, 1);


    ok !$first->publish_pip, 'Pip set correctly';
    ok $first->publish_website, 'Publish public set correctly';

    ok !$second->publish_pip, 'Pip 2 set correctly';
    ok $second->publish_website, 'Publish public 2 set correctly';

    # test the reverse scenario, wrote it out again because tampering with these
    # fake booleans yields a lot of problems (we get booleanized responses)

    ($first, $second) = create_files_with_defaults(1, 0);

    ok $first->publish_pip, 'Pip set correctly';
    ok !$first->publish_website, 'Publish public set correctly';

    ok $second->publish_pip, 'Pip 2 set correctly';
    ok !$second->publish_website, 'Publish public 2 set correctly';

}, 'file_create apply_defaults');

zs_done_testing();

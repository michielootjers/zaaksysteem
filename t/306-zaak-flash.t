#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Constants;


### Test header end


sub check_messages {
    my ($case, $status, $expected_message) = @_;

    $case->payment_status($status);
    my @messages = $case->display_flash_messages;

    is scalar @messages, 1, "One message generated";
    my ($message) = @messages;
    is $message->{type}, 'error', 'Message type is error';
    is $message->{message}, $expected_message, 'Message text correct';
}


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    check_messages($case,
        CASE_PAYMENT_STATUS_FAILED,
        'Let op, betaling niet succesvol'
    );
}, 'Case flash messages: failed');



$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    check_messages($case,
        CASE_PAYMENT_STATUS_PENDING,
        'Let op, betaling (nog) niet afgerond'
    );
}, 'Case flash messages: pending');


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    my $aanvrager = $case->aanvrager_object;
    ok $aanvrager->can('messages'), "Aanvrager supports messages";

    my $messages = $aanvrager->messages;

    is scalar keys %$messages, 0, "By default no messages";
}, 'Case flash messages: aanvrager without messages');


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    my $aanvrager = $case->aanvrager_object;

    $aanvrager->is_briefadres(1);
    $aanvrager->in_onderzoek(1);
    $aanvrager->is_overleden(1);
    $aanvrager->indicatie_geheim(1);
    $aanvrager->woonplaats('Kaas'); # change woonplaats to trigger is_verhuisd

    my @messages = $case->display_flash_messages;

    is scalar @messages, 1, "One combined message";

    my ($message) = @messages;
    is $message, 'Let op: Betrokkene is verhuisd, Betrokkene staat in onderzoek, Betrokkene heeft een briefadres, Betrokkene heeft een indicatie "Geheim", Betrokkene is overleden', "Message as expected";
}, 'Case flash messages: aanvrager with all messages');


zs_done_testing();

#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Backend::Sysin::Modules::BAGCSV');
use_ok('Zaaksysteem::Backend::BagWoonplaats::ResultSet');


$zs->zs_transaction_ok(sub {
    my $interface = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    throws_ok(
        sub {
            $interface->process({
                input_data              => 'baababa',
                external_transaction_id => DateTime->now->iso8601,
            });
        },
        qr/we only allow filestore references/,
        'BAGCSV: Valid checking for filename only'
    );

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ok($transaction->$_, 'Transaction attribute set: ' . $_) for qw/
        date_created
        date_last_retry
        external_transaction_id
        input_file
        interface_id
    /;



    is(
        $transaction->interface_id->id,
        $interface->id,
        'Transaction matches interface'
    );

    is(
        $transaction->input_file->id,
        $filestore->id,
        'Transaction matches filestore_id'
    );

}, 'Tested: File and transaction processing basics');


$zs->zs_transaction_ok(sub {
    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    is(
        $transaction->transaction_records->count,
        5,
        'Found 5 transaction records'
    );

    my $first_record = $transaction->transaction_records->first;

    #note(explain({ $first_record->get_columns }));
    ok($first_record->$_, 'Transaction record attribute set: ' . $_) for qw/
        date_executed
        input
        output
        transaction_id
    /;

}, 'Tested: Transaction records basic');


$zs->zs_transaction_ok(sub {
    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ### Check mutations
    my $entry = $schema->resultset('BagWoonplaats')->search()->first;
    ok($entry, 'Found city');
    is($entry->identificatie, '1331', 'Correct ID on woonplaats');
    is($entry->naam, 'Bussum', 'Correct name on woonplaats');
    ok(($entry->begindatum =~ /2007\-\d{2}\-\d{2}/), 'Correct begindatum on woonplaats');

    my $mutations   = $schema
                    ->resultset('TransactionRecordToObject')
                    ->search(
                        {
                            local_table     => 'BagWoonplaats',
                            local_id        => $entry->id
                        }
                    );
    my $mutation    = $mutations->first;

    is($mutations->count, 5, 'Found five transaction mutations');
    ok($mutation->transaction_record_id, 'Mutation has transaction_record_id');
    ok($mutation->mutations, 'Mutation has detailed mutation info');


}, 'Tested: BAGCSV woonplaats results');

$zs->zs_transaction_ok(sub {
    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ### Check mutations
    my $entry = $schema->resultset('BagOpenbareruimte')->search({},{order_by => 'id'})->first;
    ok($entry, 'Found openbare ruimte');
    is($entry->identificatie, '0381300000100001', 'Correct ID on entry');
    is($entry->naam, 'Aaltje Noordewierlaan', 'Correct name on entry');
    ok(($entry->begindatum =~ /2009\-\d{2}\-\d{2}/), 'Correct begindatum on entry');

}, 'Tested: BAGCSV openbareruimte results');

$zs->zs_transaction_ok(sub {
    my $cols = {
        identificatie           => '0381200000100500',
        huisnummer              => 1,
        huisletter              => 'A',
        huisnummertoevoeging    => '1-R',
        postcode                => '1403JA',
        inonderzoek             => 'J',
        type                    => 'Verblijfsobject',
        status                  => 'Naamgeving uitgegeven',
    };

    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ### Check mutations
    my $entry = $schema->resultset('BagNummeraanduiding')->search({},{order_by => 'id'})->first;
    ok($entry, 'Found nummeraanduiding');

    is($entry->$_, $cols->{ $_ }, 'Correct ' . $_ . ' on entry') for keys %{ $cols} ;

}, 'Tested: BAGCSV nummeraanduiding results');

$zs->zs_transaction_ok(sub {
    my $cols = {
        oppervlakte             => 67,
        status                  => 'Verblijfsobject in gebruik',
        inonderzoek             => 'N',
    };

    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ### Check mutations
    my $entry = $schema->resultset('BagVerblijfsobject')->search({},{order_by => 'id'})->first;
    ok($entry, 'Found verblijfsobject');

    is($entry->$_, $cols->{ $_ }, 'Correct ' . $_ . ' on entry') for keys %{ $cols} ;

}, 'Tested: BAGCSV verblijfsobject results');

$zs->zs_transaction_ok(sub {
    my $cols = {
        identificatie           => '0381010000020398',
        gebruiksdoel            => 'woonfunctie',
    };

    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ### Check mutations
    my $entry = $schema->resultset('BagVerblijfsobjectGebruiksdoel')->search({},{order_by => 'id'})->first;
    ok($entry, 'Found gebruiksdoel');

    is($entry->$_, $cols->{ $_ }, 'Correct ' . $_ . ' on entry') for keys %{ $cols} ;

}, 'Tested: BAGCSV gebruiksdoel results');

$zs->zs_transaction_ok(sub {
    my $cols = {
        identificatie           => '0381100000118244',
        bouwjaar                => '1992',
        inonderzoek             => 'N',
        status                  => 'Pand in gebruik'
    };

    my $interface   = $schema ->resultset('Interface')
            ->interface_create(
                {
                    module          => 'bagcsv',
                    name            => 'BAG Import',
                    active          => 1,
                },                                
            );

    my $filestore   = $zs->create_filestore_ok;

    my $transaction = $interface->process({
        input_filestore_uuid    => $filestore->uuid,
        external_transaction_id => DateTime->now->iso8601,
    });

    ok(
        $schema->resultset('BagVerblijfsobjectPand')->search(
            {
                identificatie       => '0381010000020398',
                pand                => '0381100000118244',

            }
        )->count,
        'Found pand koppeling'
    );

    ### Check mutations
    my $entry = $schema->resultset('BagPand')->search({},{order_by => 'id'})->first;
    ok($entry, 'Found pand');

    is($entry->$_, $cols->{ $_ }, 'Correct ' . $_ . ' on entry') for keys %{ $cols} ;

}, 'Tested: BAGCSV gebruiksdoel results');


zs_done_testing();

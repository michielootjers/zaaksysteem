#! perl
use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;

use JSON;
use Test::Deep;
use File::Spec::Functions;

BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    woonplaats_identificatie                => '630',
    woonplaats_naam                         => 'Amsterdam',

    openbareruimte_identificatie            => '0307300000306747',
    openbareruimte_naam                     => 'Donker Curtiusstraat',
    nummeraanduiding_identificatie          => '0307200000431313',
    nummeraanduiding_begindatum             => '1993-09-14',
    nummeraanduiding_huisnummer             => '7',
    nummeraanduiding_huisnummertoevoeging   => '521',
    nummeraanduiding_postcode               => '1051JL',
    nummeraanduiding_status                 => 'Naamgeving uitgegeven',

    verblijfsobject_identificatie           => '0307010000431314',
};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', 'aoa/101-aoa-create-home.xml'),
        '0310'
    );

    #note(explain($stuf->as_params));

    is($stuf->entiteittype, 'AOA', 'Found entiteittype AOA');

    my $r02_params = $stuf->get_params_for_adr;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats');

    my $rv = Data::FormValidator->check(
        $r02_params,
        Params::Profile->get_profile('method' => 'Zaaksysteem::Backend::BagWoonplaats::ResultSet::bag_create_or_update')
    );

    for my $key (keys %{ $VALIDATION_MAP }) {
        my $givenvalue = $rv->valid($key);
        my $wantedvalue = $VALIDATION_MAP->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    #note(explain($r02_params));
    #note(explain($rv));

    ok($rv->success, 'Valid data for BagWoonplaats profile');

}, 'Check OPR native params');


zs_done_testing;

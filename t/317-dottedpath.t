#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Backend::Tools::DottedPath qw/get_by_dotted_path set_by_dotted_path/;

# TODO: Fix this 
no warnings;

$zs->zs_transaction_ok(sub {
    throws_ok(
        sub { get_by_dotted_path() },
        qr/Input not a HashRef/,
        'Missing Input'
    );

    throws_ok(
        sub { get_by_dotted_path({}) },
        qr/Validation of profile failed/,
        'Missing Input'
    );

    my $hashref = {};

    ok(undef == get_by_dotted_path({target => $hashref, path => 'bla'}));

    throws_ok(
        sub { set_by_dotted_path() },
        qr/Input not a HashRef/,
        'Missing Input'
    );

    throws_ok(
        sub { set_by_dotted_path({}) },
        qr/Validation of profile failed/,
        'Missing Input'
    );

    ok(set_by_dotted_path({target => $hashref, path => 'bla', value => '1'}));
    ok('1' eq get_by_dotted_path({target => $hashref, path => 'bla'}));

    ok(set_by_dotted_path({target => $hashref, path => 'boter.kaas.eieren', value => '1'}));

    ok('1' eq get_by_dotted_path({target => $hashref, path => 'bla'}));

    throws_ok(
        sub { get_by_dotted_path({target => $hashref, path => 'bla.bka.bka'}) },
        qr/non-existing path requested/,
        'non existing path requested'
    );

}, 'Validation tests');




zs_done_testing();

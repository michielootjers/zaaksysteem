#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


$zs->zs_transaction_ok(sub {
    throws_ok(
        sub {
            $schema->resultset('ScheduledJobs')->create_task()
        },
        qr/input error: no task/,
        'validation fail: need at least a task'
    );


    throws_ok(
        sub {
            $schema->resultset('ScheduledJobs')->create_task(
                {
                    task => 'case/update_kenmerkINVALID',
                }
            )
        },
        qr/input error: invalid task/,
        'validation fail: need a valid task'
    );
}, 'Validation tests');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;

    throws_ok(
        sub {
            $schema ->resultset('ScheduledJobs')
                    ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        created_by => $zs->get_subject_ok,
                    }
            )
        },
        qr/ScheduledJobs\[.*Profile Error/,
        'validation fail: need a valid task'
    );

    ### Create task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    is($task->task, 'case/update_kenmerk', 'Found task in db');
    is($task->schedule_type, 'manual', 'schedule_type: manual');
    ok(!$task->scheduled_for, 'schedule_for: not set');
    is($task->parameters->{value}, "Completely new value", 'Found value');
    is(
        $task->parameters->{bibliotheek_kenmerken_id},
        9,
        'Found bibliotheek_kenmerken_id'
    );

    is(
        $task->parameters->{case_id}, 
        $case->id,
        'Found case_id'
    );
}, 'Create manual task: update_kenmerk');




$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;

    ### Create empty task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => '',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ### Run task
    ###
    ok($task->apply_roles->approve, 'Approved update kenmerk');

    ### Check kenmerk
    my $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    ok(!$kenmerk, 'Task run: empty set');

    ### Create empty task
    ###
    $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'bla',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ### Run task
    ###
    ok($task->apply_roles->approve, 'Approve update kenmerk with value');

    ### Check kenmerk
    $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    is($kenmerk->value, 'bla', 'Double run: 1 found kenmerk');

    $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => '',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ok($task->apply_roles->approve, 'Ran empty task');

    ### Check kenmerk
    $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    ok(!$kenmerk, 'Double run: 2 kenmerk unset');
}, 'Manual task: Run update_kenmerk / empty value');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;

    ### Create task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    is($task->task, 'case/update_kenmerk', 'Found task in db');
    ok($task->reject, 'Rejected task');
    ok($task->deleted, 'Task deleted');
}, 'Manual task: Reject update_kenmerk');




$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;
    my $subject = $zs->get_subject_ok();

    ### Create 3 tasks
    ###
    $schema ->resultset('ScheduledJobs')
            ->create_task(
            {
                task => 'case/update_kenmerk',
                bibliotheek_kenmerken_id => '9',
                value => 'Completely new value',
                case  => $case,
                created_by => $subject,
            }
        ) for (1..3);

    my $tasks = $schema ->resultset('ScheduledJobs')
                        ->search_update_field_tasks(
                            {
                                case_id     => $case->id,
                            }
                        );

    is($tasks->count, 3, 'Found 3 task');
}, 'task: update_kenmerk: search by case_id');



zs_done_testing();
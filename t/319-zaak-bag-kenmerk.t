#! perl

### Test header start
use warnings;
use strict;
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use File::Spec::Functions;



sub create_case {
    my $zaaktype_node = $zs->create_zaaktype_node_ok;
    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    # i seem to need to explicitly create afhandelfase, otherwise $zaak->is_afgehandeld
    # will bork out.
    # if that we're available by default this script could be a shorter
    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    my $bibliotheek_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'bag',
        magic_string => 'magic_string_bag',
        value_type => 'bag_openbareruimte'
    );

    my $zaaktype_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $bibliotheek_kenmerk
    );

    my $case = $zs->create_case_ok(zaaktype => $casetype);
    my $kenmerk_id = $bibliotheek_kenmerk->id;

    return ($case, $zaaktype_kenmerk);
}


$zs->zs_transaction_ok(sub {

    my $rs = $schema->resultset('ZaakKenmerk');

    my $non_bag_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'non_bag',
        magic_string => 'non_bag',
        value_type => 'text'
    );

    my $kenmerk = $rs->create({
        zaak_id                     => $zs->create_case_ok->id,
        bibliotheek_kenmerken_id    => $non_bag_kenmerk->id,
        value                       => 3
    });

    is $kenmerk->get_bag_kenmerk_value('bla'), undef, "Calling on non bag kenmerk yields undef";
    is $kenmerk->get_bag_kenmerk_value('nummeraanduiding-12334'), undef, "Even with proper bag value";

 
}, "Negative unit test get_bag_kenmerk_value");


$zs->zs_transaction_ok(sub {

    my $rs = $schema->resultset('ZaakKenmerk');

    my $bag_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'bag',
        magic_string => 'bag',
        value_type => 'bag_openbareruimte'
    );

    my $kenmerk = $rs->create({
        zaak_id                     => $zs->create_case_ok->id,
        bibliotheek_kenmerken_id    => $bag_kenmerk->id,
        value                       => 3
    });

    is $kenmerk->get_bag_kenmerk_value('bla'), undef, "Calling on bag kenmerk with bad value yields undef";

    my $result = $kenmerk->get_bag_kenmerk_value('nummeraanduiding-12334');
    isa_ok $result, 'HASH', "With proper value returns a hashref";
    is $result->{type}, 'nummeraanduiding', "Type is correct";
    is $result->{id}, '12334', 'Id is correct';
    is scalar keys $result, 2, "Two keys";
 
}, "Positive unit test get_bag_kenmerk_value");


$zs->zs_transaction_ok(sub {
    my ($case, $zaaktype_kenmerk) = create_case;
    my $kenmerk_id = $zaaktype_kenmerk->bibliotheek_kenmerken_id->id;
    
    lives_ok( sub {
        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerk_id,
            zaak_id                     => $case->id,
            values                      => [undef]
        });
    }, 'Does not bork out when passed undef');

}, "Storing non bag value in bag kenmerk lives");



$zs->zs_transaction_ok(sub {
    my ($case, $zaaktype_kenmerk) = create_case;
    my $kenmerk_id = $zaaktype_kenmerk->bibliotheek_kenmerken_id->id;
   
    my $faulty_bag_value = 'kaas';
    lives_ok( sub {
        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerk_id,
            zaak_id                     => $case->id,
            values                      => [$faulty_bag_value]
        });
    }, 'Does not bork out when passed a non-bag value');

    my $kenmerken = $case->field_values;
    is $kenmerken->{$kenmerk_id}, $faulty_bag_value, "Faulty bag value is stored without problems";
}, "Storing non bag value in bag kenmerk lives");


$zs->zs_transaction_ok(sub {
    my ($case, $zaaktype_kenmerk) = create_case;
    my $kenmerk_id = $zaaktype_kenmerk->bibliotheek_kenmerken_id->id;

    my $identificatie = '1221212121';

    $zs->create_bag_openbareruimte_ok(
        identificatie => $identificatie,
    );

    my $good_bag_value = 'openbareruimte-' . $identificatie;

    $case->zaak_kenmerken->replace_kenmerk({
        bibliotheek_kenmerken_id    => $kenmerk_id,
        zaak_id                     => $case->id,
        values                      => [$good_bag_value]
    });

    my $kenmerken = $case->field_values;
    is $kenmerken->{$kenmerk_id}, $good_bag_value, "Good bag value is stored as bag";

}, 'Storing good bag value stored proper bag');


zs_done_testing();

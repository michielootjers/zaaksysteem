#! /usr/bin/perl

use warnings;
use strict;
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
stop_test_server_ok();
zs_done_testing();

#! perl
use TestSetup;
initialize_test_globals_ok;

use Cwd;
use DateTime;
use Zaaksysteem::Zaaktypen;
use Zaaksysteem::Backend::Tools::DottedPath qw/get_by_dotted_path/;

# TODO: Fix this too
no warnings;
# TODO: Use the TestSuite!
use Test::DummyCasetype;


sub get_zaaktypen_model {
    my $mock_log = Test::MockObject->new();

    $mock_log->mock('error', sub { my $self = shift; diag "error:" . shift });
    $mock_log->mock('info',  sub { my $self = shift; diag "info: " . shift });

    return new Zaaksysteem::Zaaktypen(dbic => $schema, log => $mock_log);
}


sub get_new_casetype {
    my $casetype = Test::DummyCasetype::DUMMY_CASETYPE;

    # add a mock category and tell the casetype to live there
    my $category = $schema->resultset('BibliotheekCategorie')->create({naam => 'test cat'});
    $casetype->{zaaktype}{bibliotheek_categorie_id} = $category->id;
    $casetype->{node}{titel} = "test zaaktype . " . localtime();
    delete $casetype->{zaaktype}{id};
    return $casetype;
}

sub parse_xml {
    my $model = shift;
    # given an XML file, get a list of settings
    my $filename = getcwd() . "/t/inc/INavigator/Demo_ICR1.5_2procs.xml";

    return $model->inavigator_xml_to_json({ filename => $filename });
}

sub validate_segment_structure {
    my ($label, $segment, $fields) = @_;

    foreach my $item (@$segment) {
        ok exists $item->{$_}, "$label has a $_ member" for map { $_->{field} } @$fields;
    }
}

=pod

I-Navigator consists of two parts: The parser and the applier.
The parser will import an XML file and create a structure like:

{ general => {..}, documents => [..], results => [..] , checklistitems => [..]}

So the test will be:
- Parser creates correct structure

The structure is given to the GUI, where the user will enrich it with
settings. After adding these settings, the structure is given back
to the backend for phase 2: Applying the changes. The following changes
are supported:
- Add a new casetype
- Update an existing one
- Modify title of existing casetype
- Modify a number of general attributes
- Add document to a specific phase using an existing bibliotheek_kenmerk
- Add document to a specific phase using a newly created bibliotheek_kenmerk
- Update settings for a document (mandatory, publish_pip, publish_html)
- Add checklist item to a given phase
- Add a result
- Update a result, matching on a reference id

=cut


$zs->zs_transaction_ok(sub {

    my $model = get_zaaktypen_model;

    my $untrimmed = " kaas ";
    is $model->trim($untrimmed), 'kaas', "whitespace trimmed from beginning and end";

    my $casetypes = parse_xml($model);

    my $definitie_grondslag = $casetypes->{'B0601'}{general}{'definitie.grondslag'};
    ok $definitie_grondslag =~ m/<a href=/, "Definitie grondslag contains link";
    ok $definitie_grondslag =~ m/Besluit administratieve bepalingen inzake het we/, "contains title";

    my $handelingsinitiator = $casetypes->{'B0041'}{general}{'definitie.handelingsinitiator'};

    is $handelingsinitiator, lc($handelingsinitiator), "Handelingsinitator has been made lowercase";

}, "Check grondslag and handelingsinitiator parsing");


$zs->zs_transaction_ok(sub {
    my $results = {
        1 => {
            external_reference => 'boter',
            label => 'niet worst'
        },
        2 => {
            external_reference => 'kaas',
            label => 'ook niet worst'
        },
    };

    my $model = get_zaaktypen_model;

    $model->update_existing_result($results, {id => 'kaas', label => 'worst'});

    ok $results->{1}->{label} eq 'niet worst', 'non-matching result not updated';
    ok $results->{2}->{label} eq 'worst', 'matching result updated';

    throws_ok ( sub {
        $model->update_existing_result($results, {id => 'eieren', label => 'worst'});
    }, qr/Systeemfout: Resultaat niet gevonden/,
        'Attempt to update non existing result fails');

}, 'INavigator update_existing_result tests');

$zs->zs_transaction_ok(sub {

    my $model = get_zaaktypen_model;

    # put a new casetype in the database
    my $casetype_id = $model->commit_session(
        session => get_new_casetype(),
        commit_message => 'Added by testscript',
    )->zaaktype_id->id;

    my $casetype_settings = {
        documents => [{
            label => 'Aanvraag vergunning',
            naam => 'Aanvraag vergunning',
            value_mandatory => 1,
            phase => {
                value => 1
            },
        }]
    };

    throws_ok( sub {
        my $updated_casetype_id = $model->import_inavigator_casetype({
            not_action => 'update',
            not_casetype_id => $casetype_id,
            not_settings => $casetype_settings,
            not_commit_message => 'test i-navigator update'
        });

    }, qr/Validation of profile failed/, "invalid profile rejected");

    throws_ok( sub {
        my $updated_casetype_id = $model->import_inavigator_casetype({
            action => 'update',
            casetype_id => 1023329, # unlikely high id for casetype
            settings => $casetype_settings,
            commit_message => 'test i-navigator update'
        });

    }, qr/Unable to retrieve casetype with id/, "invalid casetype_id rejected");


    throws_ok( sub {
        my $updated_casetype_id = $model->import_inavigator_casetype({
            action => 'update',
            casetype_id => $casetype_id, # unlikely high id for casetype
            settings => $casetype_settings,
            commit_message => 'test i-navigator update'
        });

    }, qr/ Validation of profile failed/, "needs all four keys present");

    $casetype_settings->{general} = {};
    $casetype_settings->{checklistitems} = [];
    $casetype_settings->{results} = [];

    throws_ok( sub {
        my $updated_casetype_id = $model->import_inavigator_casetype({
            action => 'update',
            casetype_id => $casetype_id,
            settings => $casetype_settings,
            commit_message => 'test i-navigator update'
        });

        my $updated_casetype = $model->retrieve_session($updated_casetype_id);
    }, qr/expected/, "expects general fields to be filled");

    $casetype_settings->{general} = { map { $_->{field} => 'dummy' } (Zaaksysteem::Zaaktypen::INavigator::GENERAL_FIELDS)};

    # ready for the real deal now

    my $updated_casetype_id = $model->import_inavigator_casetype({
        action => 'update',
        casetype_id => $casetype_id,
        settings => $casetype_settings,
        commit_message => 'test i-navigator update'
    });

    my $updated_casetype = $model->retrieve_session($updated_casetype_id);

}, 'INavigator add document');


$zs->zs_transaction_ok(sub {

    my $model = get_zaaktypen_model;
    my $casetypes = parse_xml($model);

    ok ref $casetypes eq 'HASH', 'parser returned a hashref';

    foreach my $casetype_settings (values %$casetypes) {

        # make sure the XML parses correctly
        # make sure the expected structure is present
        ok exists $casetype_settings->{$_}, "casetype has a $_ member"
            for (qw/general documents checklistitems results/);

        ok exists $casetype_settings->{general}{$_}, "casetype->{general} has a $_ member"
            for map { $_->{field} } (Zaaksysteem::Zaaktypen::INavigator::GENERAL_FIELDS);

        validate_segment_structure('document',
            $casetype_settings->{documents},
            [Zaaksysteem::Zaaktypen::INavigator::DOCUMENT_FIELDS]
        );

        validate_segment_structure('checklistitem',
            $casetype_settings->{checklistitems},
            [Zaaksysteem::Zaaktypen::INavigator::CHECKLIST_FIELDS]
        );

        validate_segment_structure('result',
            $casetype_settings->{results},
            [Zaaksysteem::Zaaktypen::INavigator::RESULT_FIELDS]
        );


        # put a new casetype in the database
        my $casetype_id = $model->commit_session(
            session => get_new_casetype(),
            commit_message => 'Added by testscript',
        )->zaaktype_id->id;

        ok $casetype_id =~ m|^\d+$|, "casetype added by testscript";

        # this is always necessary to change the titel, the GUI normally
        # takes care of that
        $casetype_settings->{general}->{modify_title} = 1;

        # create a new one
        my $created_casetype_id = $model->import_inavigator_casetype({
            action => 'create',
            casetype_id => $casetype_id,
            settings => $casetype_settings,
            commit_message => 'test i-navigator update'
        });

        ok $created_casetype_id =~ m|^\d+$|, "import function returned a numeric casetype_id";

        my $created_casetype = $model->retrieve_session($created_casetype_id);

        ok $created_casetype && ref $created_casetype eq 'HASH', "created_casetype succesfully retrieved";

        # now check if all modifications have been succesfully saved
        for (Zaaksysteem::Zaaktypen::INavigator::GENERAL_FIELDS) {

            # checkbox values are translated to JSON so the GUI understands. to
            # test this here, first need to translate back.
            my $value = $_->{type} eq 'checkbox' ?
                ($casetype_settings->{general}{$_->{field}} ? 'Ja' : 'Nee') :
                $casetype_settings->{general}{$_->{field}};

            ok $value eq get_by_dotted_path({target => $created_casetype, path => $_->{field}}),
                "field $_->{field} has been saved succesfully";
        }


        $casetype_settings->{general}->{modify_title} = 1;

        # update existing
        my $updated_casetype_id = $model->import_inavigator_casetype({
            action => 'update',
            casetype_id => $created_casetype_id,
            settings => $casetype_settings,
            commit_message => 'test i-navigator update'
        });


        my $updated_casetype = $model->retrieve_session($updated_casetype_id);

        ok $updated_casetype->{node}->{titel} eq $casetype_settings->{general}->{'node.titel'},
            "updated title when using modify_title";
    }

}, 'INavigator modifications tests');

zs_done_testing();

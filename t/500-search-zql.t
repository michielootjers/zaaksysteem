#! perl

use strict;
use warnings;

use lib 't/inc';

use Moose::Util qw(apply_all_roles);
use TestSetup;
initialize_test_globals_ok;
use Test::DummyResultSet;

BEGIN { use_ok 'Zaaksysteem::Search::ZQL' }

sub isa_cond_ok ($) {
    my $condition = shift;

    isa_ok $condition, 'Zaaksysteem::Search::Conditional', 'isa_cond_ok: condition is proper object';

    return $condition;
}

sub isa_set_ok ($) {
    my $set = shift;

    isa_ok $set, 'Zaaksysteem::Search::Term::Set', 'isa_set_ok: set is a proper object';

    return @{ $set->values };
}

sub isa_fn_ok ($) {
    my $fn = shift;

    isa_ok $fn, 'Zaaksysteem::Search::Term::Function', 'isa_fn_ok: function is a proper object';

    return $fn;
}

# base case
{
    my $query = 'SELECT zaaknummer FROM case WHERE zaaknummer = 1';

    my $zql = Zaaksysteem::Search::ZQL->new($query);
    my $cmd = $zql->cmd;
    
    is $cmd->object_type, 'case', 'SELECT::FROM correctly parsed';

    is $cmd->probes->[0]->value, 'zaaknummer', 'SELECT::COLUMN parsed with correct column name';

    my $args = isa_cond_ok $cmd->dbixify_args;

    is $args->lterm->value, 'zaaknummer', 'conditional lterm grokked';
    is $args->rterm->value, '1', 'conditional rterm grokked';
    is $args->operator, '=', 'conditional operator grokked';
}

# dots in column name
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE zaaknummer = 1');
    my $cmd = $zql->cmd;

    is $cmd->probes->[0]->value, 'case.id', 'SELECT::COLUMN parses columnames with dots in them';
}

# select jpath
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT $.aanvrager.display_name FROM case WHERE zaaknummer = 1');

    # Hack return value from a literal JPath, it stringifies just fine, but the suite only sees an object
    is $zql->cmd->probes->[0]->value . '', '$.aanvrager.display_name', 'SELECT::JPATH probe parses correctly';
}

# select multiple columns
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT id, case.id, $.aanvrager.display_name FROM case WHERE zaaknummer = 1');

    my ($normal, $dotted, $jpath) = @{ $zql->cmd->probes };

    is $normal->value, 'id', 'SELECT::COLUMN::ARRAY parses ok, multi-column select, 1/3';
    is $dotted->value, 'case.id', 'SELECT::COLUMN::ARRAY parses ok, multi-column select, 2/3';
    is $jpath->value . '', '$.aanvrager.display_name', 'SELECT::COLUMN::ARRAY parses ok, multi-column select, 3/3';
}

{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT id FROM case WHERE casetype = "blabla\"asas"');

    my $args = isa_cond_ok $zql->cmd->dbixify_args;

    is $args->rterm->value, 'blabla"asas', 'ZQL parsed quoted strings with escaped quote char';
}

# optioned query
{
    my $nop = 'SELECT zaaknummer FROM case WHERE zaaknummer = 1 ORDER BY zaaknummer';
    my $asc = 'SELECT zaaknummer FROM case WHERE zaaknummer = 1 ORDER BY zaaknummer ASC';
    my $desc = 'SELECT zaaknummer FROM case WHERE zaaknummer = 1 ORDER BY zaaknummer DESC';

    my $zql = Zaaksysteem::Search::ZQL->new($nop);
    my $zql1 = Zaaksysteem::Search::ZQL->new($asc);
    my $zql2 = Zaaksysteem::Search::ZQL->new($desc);

    is_deeply $zql->cmd->dbixify_opts,
        { order_by => { alphanumeric => [ { -asc => 'zaaknummer' } ] } },
        'SELECT::ORDER_BY parsed correctly';

    is_deeply $zql1->cmd->dbixify_opts,
        { order_by => { alphanumeric => [ { -asc => 'zaaknummer' } ] } },
        'SELECT::ORDER_BY (ASC) parsed correctly';

    is_deeply $zql2->cmd->dbixify_opts,
        { order_by => { alphanumeric => [ { -desc => 'zaaknummer' } ] } },
        'SELECT::ORDER_BY (DESC) parsed correctly';
}

#optioned limit query
{
    my $query = 'SELECT zaaknummer FROM case WHERE zaaknummer = 1 LIMIT 45';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    is_deeply $zql->cmd->dbixify_opts, { rows => 45 }, 'SELECT::LIMIT parsed correctly';
}

# lower-cased queries
{
    my $query = 'select zaaknummmer from case where zaaknummer = 1';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    is $zql->object_type, 'case', 'Lowercased query parses SELECT::FROM';

    my $args = isa_cond_ok $zql->cmd->dbixify_args;

    is $args->lterm->value, 'zaaknummer';
    is $args->rterm->value, '1';
    is $args->operator, '=';
}

# lowercased option query
{
    my $query = 'select zaaknummer from case where zaaknummer = 1 order by zaaknummer asc';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    is_deeply $zql->cmd->dbixify_opts,
        { order_by => { alphanumeric => [ { -asc => 'zaaknummer' } ] } },
        'Lowercased query SELECT::ORDER_BY parsed correctly';
}

# lowercased limit query
{
    my $query = 'select zaaknummer from case where zaaknummer = 1 limit 45';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    is_deeply $zql->cmd->dbixify_opts, { rows => 45 }, 'Lowercased query SELECT::LIMIT parsed correctly';
}

# failure modes
{
    throws_ok { Zaaksysteem::Search::ZQL->new(''); } 'Zaaksysteem::Exception::Base', 'Empty query throws error';
    throws_ok { Zaaksysteem::Search::ZQL->new('SELECT 4 FROM :'); } 'Zaaksysteem::Exception::Base', 'Syntax error throws error';
}

# Without where clause
{
    my $query = 'select zaaknummer from case';
    my $query_limit = 'select zaaknummer from case limit 10';

    lives_ok { Zaaksysteem::Search::ZQL->new($query) } 'WHERE-less query parses OK';

    lives_and(
        sub {
            my $zql = Zaaksysteem::Search::ZQL->new($query_limit);
            is_deeply $zql->cmd->dbixify_opts, { rows => 10 }, 'SELECT::LIMIT parsed correctly in WHERE-less query';
        },
        'WHERE-less query with LIMIT parses'
    );
}

# AND nesting
{
    my $query = 'SELECT zaaknummer FROM case WHERE (zaaknummer = 1) AND (onderwerp ~ "Onderwerpish")';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    my $args = isa_cond_ok $zql->cmd->dbixify_args;
    my $zaak_cnd = isa_cond_ok $args->lterm;
    my $subj_cnd = isa_cond_ok $args->rterm;

    is $args->operator, 'and', 'nested conditions chained with AND';

    is $zaak_cnd->lterm->value, 'zaaknummer', 'nested conditions: first condition lterm grokked';
    is $zaak_cnd->rterm->value, 1, 'nested conditions: first condition term grokked';
    is $zaak_cnd->operator, '=', 'nested conditions: first condition operator grokked';

    is $subj_cnd->lterm->value, 'onderwerp', 'nested conditions: second condition lterm grokked';
    is $subj_cnd->rterm->value, 'Onderwerpish', 'nested conditions: second condition rterm grokked';
    is $subj_cnd->operator, 'ILIKE', 'nested conditions: second condition operator grokked';
}

# OR nesting
{
    my $query = 'SELECT zaaknummer FROM case WHERE (zaaknummer = 1) OR (onderwerp > "derp")';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->operator, 'or', 'nested conditions chained with OR';
}

# 2+ AND nesting
{
    my $query = 'SELECT zaaknummer FROM case WHERE (zaaknummer = 1) AND (onderwerp ~ "Onderwerpish") AND (titel = "Titelish")';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;
    my $first = isa_cond_ok $cond->lterm;
    my $cond2 = isa_cond_ok $cond->rterm;
    my $second = isa_cond_ok $cond2->lterm;
    my $third = isa_cond_ok $cond2->rterm;
}

# AND/OR nesting
{
    my $query = 'SELECT id FROM case WHERE (zaaknummer = 1) AND (onderwerp > "derp") OR (titel = "lang")';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->operator, 'and', 'and/or: first condition is and-ed with rest';

    my $cond2 = isa_cond_ok $cond->rterm;

    is $cond2->operator, 'or', 'and/or: second condition is or-ed with rest';
}

# OR/AND nesting
{
    my $query = 'SELECT id FROM case WHERE (zaaknummer = 1) OR (onderwerp > "derp") AND (titel = "lang")';

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->operator, 'or', 'or/and: first condition is or-ed with the rest';

    my $cond2 = isa_cond_ok $cond->rterm;

    is $cond2->operator, 'and', 'or/and: second condition is and-ed with the rest';
}

# SELECT DISTINCT tests
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT DISTINCT zaaktype_id FROM case');
    my $zql_plain = Zaaksysteem::Search::ZQL->new('SELECT zaaktype_id FROM case');

    ok $zql->cmd->distinct, 'SELECT::DISTINCT is parsed';
    ok !$zql_plain->cmd->distinct, 'SELECT::DISTINCT not set when not in query';

    my $expected = {
        'as' => [
            'zaaktype_id'
        ],
        'group_by' => [
            'zaaktype_id'
        ],
    };
    $expected->{'select'} = $expected->{'group_by'};
    is_deeply($zql->cmd->dbixify_opts, $expected, 'SELECT::DISTINCT returns correct dbixified opts'); 

    $zql = Zaaksysteem::Search::ZQL->new('SELECT DISTINCT zaaktype_id, case.zaaknummer FROM case');
    $expected = {
        'as' => [
            'zaaktype_id',
            'case$zaaknummer'
        ],
        'group_by' => [
            'zaaktype_id',
            'case.zaaknummer'
        ],
    };
    $expected->{'select'} = $expected->{'group_by'};
    is_deeply($zql->cmd->dbixify_opts, $expected, 'SELECT::DISTINCT returns correct dbixified opts (multi-column)');
}

# SET test
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT id FROM case WHERE zaaknummer IN (1, 2, 3, 4, 5)');

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    my @set = isa_set_ok $cond->rterm;

    for my $n (qw[1 2 3 4 5]) {
        my $term = shift @set;

        is $term->value, $n, 'set item has proper value';
    }
}

# SET test mixed content
{
    my $rs = Test::DummyResultSet->new();
    apply_all_roles($rs, 'Zaaksysteem::Search::HStoreResultSet');
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT id FROM case WHERE case.aanvrager IN ("Marjolein", 1, false, 2014-10-10T15:16:17Z)');

    my $cond = $zql->cmd->dbixify_args;

    isa_ok $cond, 'Zaaksysteem::Search::Conditional', 'dbixify_args returns Conditional object';

    is $cond->lterm->value, 'case.aanvrager', 'dbixify_args conditional object has correct lterm';

    isa_ok $cond->rterm, 'Zaaksysteem::Search::Term::Set', 'dbixify_args conditional object rterm is a set';
    
    is $cond->rterm->values->[0]->value, 'Marjolein', 'dbixify_args conditional object rterm contains correct value';
    is $cond->rterm->values->[1]->value, 1, 'dbixify_args conditional object rterm contains correct value';
    is $cond->rterm->values->[2]->value, 0, 'dbixify_args conditional object rterm contains correct value';
    is $cond->rterm->values->[3]->value, '2014-10-10T15:16:17', 'dbixify_args conditional object rterm contains correct value';

    my @evaluated = $cond->rterm->evaluate($rs, $cond);

    is_deeply(
        \@evaluated,
        [
            '( ?, ?, ?, ? )',
            [{}, 'Marjolein'],
            [{}, 1],
            [{}, 0],
            [{}, '2014-10-10T15:16:17' ],
        ],
        "Evaluated value of set is correct"
    );
}

# DateTime test
{
    use DateTime;

    my $dt = DateTime->now;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT id FROM case WHERE case.regdatum > ' . $dt->iso8601 . 'Z');

    ok $dt == $zql->cmd->where->rterm->value,
        'SELECT::WHERE::DATETIME returns similar object';

    # TODO do something useful with dates
    # diag explain $zql->search_args;
}

# Datetime millisecond support
{
    use DateTime;

    my $dt = DateTime->now;

    my $date = sprintf('%s.%sZ', $dt->iso8601, int(rand(1000)));

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT id FROM case WHERE case.regdatum = ' . $date);

    ok $dt == $zql->cmd->where->rterm->value, 'DateTime with millisecond precision equals parsed value (milliseconds are ignored)';
}

# BETWEEN range
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case WHERE id BETWEEN 10 AND 20');

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->operator, 'and';

    my $between_a = isa_cond_ok $cond->lterm;
    my $between_b = isa_cond_ok $cond->rterm;

    is $between_a->lterm->value, 'id', 'between expr: arg1 properly grokked with columnname';
    is $between_a->operator, '>=', 'between expr: arg1 operator properly set';
    is $between_a->rterm->value, '10', 'between expr: arg1 rterm value properly set';

    is $between_b->lterm->value, 'id', 'between expr: arg2 properly grokked with columnname';
    is $between_b->operator, '<=', 'between expr: arg2 operator properly set';
    is $between_b->rterm->value, '20', 'between expr: arg2 rterm value properly set';
}

# Complexer BETWEEN range
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case WHERE (zaaknummer = 22) AND (id BETWEEN 10 AND 20)');

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->operator, 'and', 'complexe where: first and second condition chained with and operator';

    my $cond1 = isa_cond_ok $cond->lterm;
    my $between_cnd = isa_cond_ok $cond->rterm;
}

# BAW test
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT zaaktype_node_id, case.id, $.case.type.title FROM case WHERE case.id = 1');

    # NOP test
}

# DESCRIBE
{
    my $zql = Zaaksysteem::Search::ZQL->new('DESCRIBE case');

    # NOP test
}

# WITH DESCRIPTION
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT WITH DESCRIPTION {} FROM case');

    ok $zql->cmd->describe, 'WITH DESCRIPTION SELECT option parsed correctly';

    my $zql2 = Zaaksysteem::Search::ZQL->new('SELECT DISTINCT WITH DESCRIPTION case.id FROM case');

    ok $zql2->cmd->describe, 'WITH DESCRIPTION combined with DISTINCT parses correctly';
}

# FULLTEXT SEARCH
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case MATCHING "a b c"');

    is $zql->cmd->matching->value, 'a b c', 'SELECT::MATCHING parses ok';
}

# ZS-1967 Sort order style
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case NUMERIC ORDER BY case.id');

    is_deeply $zql->cmd->opts->[0],
        { order_by => { -asc => 'case.id' }, sort_style => 'numeric' },
        'select/order_by query with NUMERIC option is parsed correctly';

    is_deeply $zql->cmd->dbixify_opts,
        { order_by => { numeric => [ { -asc => 'case.id' } ] } },
        'select/order_By query with NUMERIC option is dbixified correctly';
}

# ZS-1889
# Extend expression parsing to allow arbitrary parenthesis parsing
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE (case.id = 4)');

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->operator, '=', 'zs-1889/extra_parenthesis operator grokked';
    is $cond->lterm->value, 'case.id', 'zs-1889/extra_parenthesis lterm value grokked';
    is $cond->rterm->value, 4, 'zs-1889/extra_parenthesis rterm value grokked';
}

# FUNCTION CALL
{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE urgency() = "high"');

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    my $fn = isa_fn_ok $cond->lterm;

    is $fn->function, 'urgency', 'function: function name correctly grokked';
    is $fn->object_type, 'case', 'function: object type correctly deduced';
}

# ZS-2097 - Add IS NULL-ish support
{
    my $zql;

    lives_ok sub {
        $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE assignee.id = NULL');
    }, 'zql/is_null query doesn\'t except on instantiation';

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->rterm->value, undef, 'zql/is_null condition term is undef';
}

{
    my $zql;

    lives_ok sub {
        $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE NOT(assignee.id = "abc")');
    }, 'zql/logical_not query lives';

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->invert, 1, 'zql/logical_not embedded condition is inverted';

    lives_ok sub {
        $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE (a = 2) AND (NOT (b = 3))');
    }, 'zql/logical_not complexer query lives';

    $cond = isa_cond_ok $zql->cmd->dbixify_args;

    is $cond->rterm->invert, 1, 'zql/logical_not embedded condition is inverted when nested +1 level';
}

{
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id FROM case WHERE something.other > 0.01');

    my $cond = isa_cond_ok $zql->cmd->dbixify_args;
    isa_ok(
        $cond->lterm,
        'Zaaksysteem::Search::Term::Column',
        "Column name is parsed as a column term"
    );
    is($cond->lterm->value, 'something.other', ' .. and the value is correct');
    isa_ok(
        $cond->rterm,
        'Zaaksysteem::Search::Term::Literal',
        "Number with decimals is parsed ar a single (literal) term"
    );
    is($cond->rterm->value, '0.01', ' .. and the value is correct');
}

zs_done_testing;

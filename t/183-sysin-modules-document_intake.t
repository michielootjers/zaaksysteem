#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Backend::Sysin::Modules::Scanstraat;
use File::Spec::Functions qw(catfile);

document_intake();

sub document_intake {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'scanstraat',
                name             => 'Scanstraat',
                interface_config => {api_key => 'meuk'},
            );

            my $dir      = 't/inc/Documents';
            my $file     = 'openoffice_document.odt';
            my $filepath = catfile($dir, $file);

            my $pt = $interface->process_trigger(
                'upload_document', {
                    api_key  => 'meuk',
                    filename => $file,
                    filepath => $filepath,
                });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $tr = $zs->get_from_zaaksysteem(
                resultset => 'TransactionRecord',
                search    => {transaction_id => $pt->id},
                options   => {rows => 1},
            )->first;

            like(
                $tr->preview_string,
                qr/Document '$file' \[\d+\] is geupload/,
                "Preview string matches"
            );

            throws_ok(
                sub {
                    $interface->process_trigger(
                        'upload_document', {
                            api_key  => 'invalid',
                            filename => $file,
                            filepath => $filepath,
                        },
                    );
                },
                qr/process_document_upload: Incorrect API key/,
                "API key is fout"
            );

            throws_ok(
                sub {
                    $interface->process_trigger(
                        'upload_document', {
                            api_key  => 'meuk',
                            filename => $file,
                            filepath => "/invalid/$filepath",
                        },
                    );
                },
                qr#filestore/clamscan/file_not_found#,
                "Bestand niet gevonden"
            );
        },
        "Scanstraat",
    );
}

zs_done_testing();


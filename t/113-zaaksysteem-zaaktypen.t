#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use FakeLogger;
use Zaaksysteem::Zaaktypen;

my $logger = FakeLogger->new();

my $zaaktypen = Zaaksysteem::Zaaktypen->new(
    dbic => $schema,
    log  => $logger,
);

$zs->zs_transaction_ok(sub {
    {
        my $session = {
            version => 1337,
        };

        my $result = $zaaktypen->_commit_zaaktype({ zaaktype => $session });
        isa_ok($result, 'Zaaksysteem::Schema::Zaaktype', 'Result of _commit_zaaktype');
        is($result->version, 1337, "Correct version created");
    }

    {
        my $session = {
            id => undef,
            version => 42,
        };

        my $result = $zaaktypen->_commit_zaaktype({ zaaktype => $session });
        isa_ok(
            $result,
            'Zaaksysteem::Schema::Zaaktype',
            'Result of _commit_zaaktype with undef in "id" field',
        );
        is($result->version, 42, "Correct version created");
    }

    {
        my $zt = $zs->create_zaaktype_ok(version => 665);
        my $session = {
            id      => $zt->id,
            version => 666,
        };

        my $result = $zaaktypen->_commit_zaaktype({ zaaktype => $session });
        isa_ok(
            $result,
            'Zaaksysteem::Schema::Zaaktype',
            'Result of _commit_zaaktype with undef in "id" field',
        );
        is($result->id, $zt->id, "Correct zaaktype was returned");
        is($result->version, 665, "Version untouched");
    }

    {
        my $category = $zs->create_bibliotheek_categorie_ok();
        my $category2 = $zs->create_bibliotheek_categorie_ok();
        my $zt = $zs->create_zaaktype_ok(bibliotheek_categorie_id => $category);

        my $session = {
            id => $zt->id,
            bibliotheek_categorie_id => $category2->id,
        };

        my $result = $zaaktypen->_commit_zaaktype({ zaaktype => $session });
        isa_ok(
            $result,
            'Zaaksysteem::Schema::Zaaktype',
            'Result of _commit_zaaktype with undef in "id" field',
        );
        is($result->id, $zt->id, "Correct zaaktype was returned");
        is($result->bibliotheek_categorie_id->id, $category2->id, "bibliotheek_category_id was updated");


    }

}, 'Creating a casetype');

zs_done_testing();

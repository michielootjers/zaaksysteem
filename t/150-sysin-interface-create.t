#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;
### Test header end

$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('Interface')->interface_create({
    	name 		 => 'Test Interface',   ### Module label (db field)
    	module       => 'bagcsv',           ### Module name
        interface_config => {
            how_hot      => 'should pass',  ### CSV custom field (interface_config)
        }
	});

    ok $result, 'Created interface';
    my @set = ('name', 'module');
    for my $s (@set) {
        ok $result->$s, "$s is set";
    }

    like(
        $result->interface_config,
        qr/how_hot.*should pass/,
        'Interface config is set'
    );

}, 'interface_create');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
    	$schema->resultset('Interface')->interface_create({});
    }, qr/Validation of profile/, 'No parameters dies';
}, 'interface_create no params');

zs_done_testing;
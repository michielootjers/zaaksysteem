#! perl
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(
    sub {
        my $case = $zs->create_case_ok();

        my $resultaat = $zs->create_zaaktype_resultaat_ok(node => $case->zaaktype_node_id);
        my $bt = $resultaat->bewaartermijn;

        $case->afhandeldatum(undef);
        my $r = $case->set_vernietigingsdatum;
        is($r, undef, "No afhandeldatum set");

        my $now = DateTime->now();
        $case->afhandeldatum($now);
        $r = $case->set_vernietigingsdatum;
        isa_ok($r, "DateTime", "Return value is of correct type");
        is($r, $now->clone->add(days => $bt), "Destruction date is correct");

        $case->resultaat(undef);
        $r = $case->set_vernietigingsdatum;
        is($r, $now->add(years => 1), "No result results in a destruction one year from now");

        throws_ok(
            sub {
                $case->resultaat("This will break stuff");
                $case->set_vernietigingsdatum;
            },
            qr/Unable to find resultaat/,
            "DB discrepancy found"
        );
    },
    'set_vernietigingdatum',
);

$zs->zs_transaction_ok(
    sub {
        my $case = $zs->create_case_ok();
        while($case->set_volgende_fase) {
        }
        ok($case->is_in_phase('afhandel_fase'), "Zit in afhandelfase");
    },
    'is_in_phase'
);

zs_done_testing();

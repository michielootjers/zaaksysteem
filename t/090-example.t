#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(
    sub {
        note 'Example';
    },
    'Test description'
);

zs_done_testing();

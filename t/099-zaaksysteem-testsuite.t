#! perl

## Test header start
use warnings;
use strict;

use lib 't/inc';

use Zaaksysteem::TestUtils;
use TestSetup;
initialize_test_globals_ok;## Test header end

use Zaaksysteem::Constants qw(BAG_TYPES PROFILE_BAG_TYPES_OK);
# TODO: Test all functions of the Testsuite

$zs->zs_transaction_ok(
    sub {

        my $isa      = 'Zaaksysteem::Schema::BibliotheekCategorie';
        my $category = $zs->create_bibliotheek_categorie_ok();
        isa_ok($category, $isa);
        my $pid = $category->id;
        ok($pid, "Has an ID");

        my %args = (
            naam        => "Testnaam",
            label       => "Testlabel",
            description => "Testdescription",
            help        => "Testhelp",
        );
        my $sub_category = $zs->create_bibliotheek_categorie_ok(%args, parent => $category);
        isa_ok($sub_category, $isa);
        ok($sub_category->id, "Has an ID");

        $args{search_term} =
          join(" ", $args{naam}, $args{description}, $args{label});

        my %result = map {$_ => $sub_category->$_}
          qw(naam search_term label description help);
        is_deeply(\%result, \%args, "equals input params");

        subtest 'correct_parent' => sub {
            isa_ok(
                $sub_category->pid,
                "Zaaksysteem::Schema::BibliotheekCategorie"
            );
            is($sub_category->pid->id, $pid, "Parent category is correct");
        };
    },
    'Category',
);

$zs->zs_transaction_ok(
    sub {
        my $sjabloon = $zs->create_sjabloon_ok();
        isa_ok($sjabloon, 'Zaaksysteem::Schema::BibliotheekSjablonen');
        ok($sjabloon->id, "Has an ID");

        my %args = (
            naam        => "testnaam",
            label       => "testlabel",
            description => "testdescription",
            help        => "testhelp",
        );
        $sjabloon = $zs->create_sjabloon_ok(%args);
        isa_ok($sjabloon, 'Zaaksysteem::Schema::BibliotheekSjablonen');
        $args{search_term} = $args{naam};

        my %result =
          map {$_ => $sjabloon->$_} qw(naam search_term label description help);
        is_deeply(\%result, \%args, "equals input params");

    },
    'Sjablonen',
);

$zs->zs_transaction_ok(
    sub {
        my $k = $zs->create_bibliotheek_kenmerk_ok();
        isa_ok($k, 'Zaaksysteem::Schema::BibliotheekKenmerken');
        ok($k->id, "Has an ID");

        my $c = $zs->create_bibliotheek_categorie_ok();

        my %args = (
            naam        => "testnaam",
            label       => "testlabel",
            description => "testdescription",
            help        => "testhelp",
        );
        $k = $zs->create_bibliotheek_kenmerk_ok(%args, bibliotheek_categorie => $c);
        isa_ok($k, 'Zaaksysteem::Schema::BibliotheekKenmerken');

        $args{search_term} = $args{naam};
        my %result =
          map {$_ => $k->$_} qw(naam search_term label description help);
        is_deeply(\%result, \%args, "equals input params");

        subtest 'correct_bibliotheek_categorie' => sub {

            # This doesn't make sense, if you want the ID, give the ID.
            # $k->bibliotheek_categorie should return the object
            my $x = $k->bibliotheek_categorie_id;
            isa_ok(
                $x,
                "Zaaksysteem::Schema::BibliotheekCategorie"
            );
            is($x->id, $c->id, "Category is correct");
            done_testing();
        };

        subtest correct_kenmerk_values => sub {
            my $valued = $zs->create_bibliotheek_kenmerk_ok(values => [
                { value => 'a', active => 1, sort_order => 99 },
                { value => 'b', active => 0, sort_order => 0 }
            ]);

            ok $valued, 'Creation for valued attributes is ok';

            for my $val ($valued->bibliotheek_kenmerken_values->all) {
                is $val->value, $val->active ? 'a' : 'b', 'value correctly set';
                is $val->sort_order, $val->active ? 99 : 0, 'sort_order correctly set';
            }

            done_testing;
        };

        # TODO: Add more complex kenmerken
    },
    'Kenmerken',
);

$zs->zs_transaction_ok(sub {

        my $isa = 'Zaaksysteem::Model::DB::Interface';

        subtest 'create_interface_ok' => sub {
            my $i = $zs->create_interface_ok();
            isa_ok($i, $isa);
            ok($i->id, "$isa has an ID");
        };

        subtest 'create_interface_ok_args' => sub {
            my %args = (
                name        => "Towel",
                max_retries => 42,
                multiple    => 0,
                module      => 'authldap',
            );

            my $ztk = $zs->create_interface_ok(%args);
            isa_ok($ztk, $isa);
            ok($ztk->id, "$isa has an ID");

            my %result =
              map {$_ => $ztk->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };


}, "create_interface_ok");

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::Subject';
        my $k   = $zs->create_subject_ok();
        isa_ok($k, $isa);
        ok($k->id, "Has an ID");

        my %args = (
            username     => 'marty',
            subject_type => 'employee',
        );
        $k = $zs->create_subject_ok(%args);
        isa_ok($k, $isa);

        my %result =
          map {$_ => $k->$_} keys %args;
        is_deeply(\%result, \%args, "equals input params");

        throws_ok(
            sub {
                $zs->create_subject_ok(subject_type => 'nono');
            },
            qr/invalid: subject_type/,
            "Subject type must start with employee"
        );
    },
    'Subject',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::UserEntity';
        my $k   = $zs->create_userentity_ok();
        isa_ok($k, $isa);
        ok($k->id,      "Has an ID");
        ok(!$k->active, "Is not active");

        my $interface = 'authldap';
        my $ldap = $zs->schema->resultset('Interface')->find_by_module_name('authldap');

        subtest 'authldap' => sub {
            isa_ok($k->source_interface_id, "Zaaksysteem::Schema::Interface");
            is(
                $k->source_interface_id->id, $ldap->id,
                "Interface found is authldap"
            );
        };

        my $s = $zs->create_subject_ok(username => 'marty');
        my $i = $zs->create_interface_ok();

        $k = $zs->create_userentity_ok(
            subject    => $s,
            interface  => $i,
            active     => 1,
            properties => {hello => 'world'},
        );
        isa_ok($k, $isa);
        ok($k->active, "Is active");

        subtest 'interface' => sub {
            isa_ok($k->source_interface_id, "Zaaksysteem::Schema::Interface");
            is(
                $k->source_interface_id->id, $i->id,
                "Interface found is the same as provided"
            );
        };

        subtest 'subject' => sub {
            isa_ok($k->subject_id, "Zaaksysteem::Schema::Subject");
            is(
                $k->subject_id->id, $s->id,
                "Subject found is the same as provided"
            );
            is(
                $k->source_identifier, $s->username,
                "Subject username found as source_identifier"
            );
        };
    },
    'Userentity',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::UserEntity';
        my $k   = $zs->create_medewerker_ok();
        isa_ok($k, $isa);
        ok($k->id,      "Has an ID");
        ok(!$k->active, "Is not active");

        subtest 'subject_default_medewerker' => sub {
            isa_ok($k->subject_id, "Zaaksysteem::Schema::Subject");
            isnt($k->subject_id->id, undef, "Subject found is the same as provided");
            isnt($k->source_identifier, undef, "Subject username found as source_identifier");
        };

        my $username = 'mcfly';
        $k = $zs->create_medewerker_ok(
            username => $username,
        );
        is(
            $k->source_identifier, $username,
            "Username of the subject is correct"
        );

        my $interface = 'authldap';
        my $ldap = $zs->schema->resultset('Interface')->find_by_module_name('authldap');

        subtest 'authldap' => sub {
            isa_ok($k->source_interface_id, "Zaaksysteem::Schema::Interface");
            is(
                $k->source_interface_id->id, $ldap->id,
                "Interface found is authldap"
            );
        };

        my $s = $zs->create_subject_ok(username => 'marty');
        my $i = $zs->create_interface_ok();

        $k = $zs->create_medewerker_ok(
            subject    => $s,
            interface  => $i,
            active     => 1,
            properties => {'dn' => "iets"},
        );
        isa_ok($k, $isa);
        ok($k->active, "Is active");

        subtest 'interface' => sub {
            isa_ok($k->source_interface_id, "Zaaksysteem::Schema::Interface");
            is(
                $k->source_interface_id->id, $i->id,
                "Interface found is the same as provided"
            );
        };

        subtest 'subject' => sub {
            isa_ok($k->subject_id, "Zaaksysteem::Schema::Subject");
            is(
                $k->subject_id->id, $s->id,
                "Subject found is the same as provided"
            );
            is(
                $k->source_identifier, $s->username,
                "Subject username found as source_identifier"
            );
        };

        throws_ok(
            sub {
                $zs->create_medewerker_ok(
                    username => 'blaat',
                    subject  => $s,
                );
            },
            qr/You cannot define a username and a subject/,
            "Defining both username and subject fails",
        );

    },
    'Medewerker',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::BibliotheekNotificaties';

        subtest 'create_ok' => sub {
            my $k = $zs->create_notificatie_ok();
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");
            isa_ok($k->bibliotheek_categorie_id, "Zaaksysteem::Model::DB::BibliotheekCategorie");
        };

        subtest 'create_ok_bieb_cat' => sub {
            my $cat = $zs->create_bibliotheek_categorie_ok();
            my $k = $zs->create_notificatie_ok(bibliotheek_categorie => $cat);
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");

            isa_ok($k->bibliotheek_categorie_id, "Zaaksysteem::Model::DB::BibliotheekCategorie");
            is($k->bibliotheek_categorie_id->id, $cat->id, "Zelfde ding!");
        };

        subtest 'create_ok_args' => sub {
            my %args = (
                object_type   => 'du object',
                label         => 'maker',
                subject       => 'to',
                message       => 'Fancy a',
                created       => DateTime->now->add(days => 2),
                last_modified => DateTime->now->add(days => 3),
                deleted       => DateTime->now->add(days => 4),
            );
            my $k = $zs->create_notificatie_ok(%args);
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");

            my %result = map {$_ => $k->$_} keys %args;
            is_deeply(\%result, \%args, "Equals input params");
        };
    },
    'Bibliotheek Notificaties'
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::ZaaktypeNotificatie';

        subtest 'create_ok' => sub {
            my $k = $zs->create_zaaktype_notificatie_ok();
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");
            isa_ok($k->bibliotheek_notificaties_id, 'Zaaksysteem::Model::DB::BibliotheekNotificaties');
        };

        subtest 'create_ok_bieb_notif' => sub {
            my $notif = $zs->create_notificatie_ok();
            my $k = $zs->create_zaaktype_notificatie_ok(bibliotheek_notificatie => $notif);
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");

            isa_ok($k->bibliotheek_notificaties_id, "Zaaksysteem::Model::DB::BibliotheekNotificaties");
            is($k->bibliotheek_notificaties_id->id, $notif->id, "Zelfde ding!");
        };

        subtest 'create_ok_args' => sub {
            my %args = (
                rcpt          => 'Receipent',
                label         => 'maker',
                onderwerp     => 'Zaaksysteem',
                bericht       => 'Is awesome!!',
                intern_block  => 42,
                email         => 'zaaksysteem@example.com',
                behandelaar   => 'Dr. Vd. Ploeg',
                automatic     => 0,
                created       => DateTime->now->add(days => 2),
                last_modified => DateTime->now->add(days => 3),
            );
            my $k = $zs->create_zaaktype_notificatie_ok(%args);
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");

            my %result = map {$_ => $k->$_} keys %args;
            is_deeply(\%result, \%args, "Equals input params");
        };
    },
    'Zaaktype status notificaties'
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::ZaaktypeDefinitie';
        my $k   = $zs->create_zaaktype_definitie_ok();
        isa_ok($k, $isa);
        ok($k->id, "$isa has an id");
    },
    'Zaaktype Definitie',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::ZaaktypeNode';

        subtest zaaknode_default => sub {
            my $ztn = $zs->create_zaaktype_node_ok();
            isa_ok($ztn, $isa);
            ok($ztn->id, "Has an ID");
            is($ztn->version, 1, "Correct version");
            is($ztn->active,  1, "Is active");

            isa_ok(
                $ztn->zaaktype_definitie_id,
                "Zaaksysteem::Schema::ZaaktypeDefinitie"
            );
        };

        subtest zaaknode_args => sub {

            my $zt_def = $zs->create_zaaktype_definitie_ok();
            my %args   = (
                aanvrager_hergebruik   => 1,
                active                 => 0,
                adres_relatie          => "Hoppatee 1",
                automatisch_behandelen => 1,
                code                   => '10-4',
                titel                  => 'Test node',
                toelichting            => 'In een test',
                toewijzing_zaakintake  => 1,
                version                => 2,
                webform_authenticatie  => 1,
                webform_toegang        => 1,

                #trigger                => 'extern',
            );
            my $ztn = $zs->create_zaaktype_node_ok(
                %args,
                zaaktype_definitie => $zt_def
            );
            isa_ok($ztn, $isa);
            ok($ztn->id, "Has an ID");

            my %result =
              map {$_ => $ztn->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
            my $zt = $ztn->zaaktype_definitie_id;
            is($zt->id, $zt_def->id, "Zaaktype definition is OK");
          }

    },
    'Zaaktype node',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::ZaaktypeStatus';
        my $k   = $zs->create_zaaktype_status_ok();
        isa_ok($k, $isa);
        ok($k->id, "$isa has an id");

        my $node = $k->zaaktype_node_id;

        my $naam = "reg_phase";
        my %args = (
            fase        => $naam,
            status      => $k->status + 1,
            status_type => $naam,
            naam        => $naam,
            checklist   => 1,
            ou_id       => 1,
            role_set    => 0,
        );
        $k = $zs->create_zaaktype_status_ok(%args, node => $node);
        isa_ok($k, $isa);

        subtest 'node->zaaktype_statusses' => sub {
            my @status = $node->zaaktype_statussen;
            is(@status, 2, "Multiple states found");

            @status = sort {$a->id <=> $b->id} @status;

            my @keys   = keys %args;
            my %expect = map {$_ => $k->$_} @keys;
            my %result = map {$_ => $status[-1]->$_} @keys;

            is_deeply(\%result, \%expect, "We have our node");
        };
    },
    'Zaaktype Status',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::ZaaktypeStatusChecklistItem';

        subtest create_zaaktype_status_checklist_item_ok => sub {
            my $k = $zs->create_zaaktype_status_checklist_item_ok();
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");
            isa_ok($k->casetype_status_id, "Zaaksysteem::Schema::ZaaktypeStatus");
        };

        subtest create_ok_with_status => sub {
            my $zts = $zs->create_zaaktype_status_ok();
            my $k = $zs->create_zaaktype_status_checklist_item_ok(status => $zts);
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");

            isa_ok($k->casetype_status_id, "Zaaksysteem::Schema::ZaaktypeStatus");
            is($k->casetype_status_id->id, $zts->id, "Zaaktype status found by ID");
        };

        subtest create_ok_with_args => sub {
            my %args = ( label => "Een label", external_reference => "Externe referentie");
            my $k = $zs->create_zaaktype_status_checklist_item_ok(%args);
            isa_ok($k, $isa);
            ok($k->id, "$isa has an id");

            my %result = map {$_ => $k->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };
    },
    "Zaaktype Status Checklist Item"
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::Zaaktype';

        subtest 'without_args' => sub {
            my $k = $zs->create_zaaktype_ok();
            isa_ok($k, $isa);
            ok($k->id, "Has an ID");
            is($k->version, 1, "Correct version");
            is($k->active,  1, "Is active");
            isa_ok(
                $k->zaaktype_node_id, 'Zaaksysteem::Model::DB::ZaaktypeNode',
                "Has a node"
            );
            isa_ok(
                $k->bibliotheek_categorie_id,
                'Zaaksysteem::Model::DB::BibliotheekCategorie',
                "Has a library category"
            );
        };

        subtest 'with_node_and_cat' => sub {
            my $node = $zs->create_zaaktype_node_ok();
            my $cat  = $zs->create_bibliotheek_categorie_ok();

            my $k = $zs->create_zaaktype_ok(
                node     => $node,
                category => $cat,
            );
            isa_ok($k, $isa);
            ok($k->id, "Has an ID");
            isa_ok(
                $k->zaaktype_node_id, 'Zaaksysteem::Model::DB::ZaaktypeNode',
                "Has a node"
            );
            isa_ok(
                $k->bibliotheek_categorie_id,
                'Zaaksysteem::Model::DB::BibliotheekCategorie',
                "Has a library category"
            );
            is($k->zaaktype_node_id->id,         $node->id, "Same node");
            is($k->bibliotheek_categorie_id->id, $cat->id,  "Same category");
        };

        subtest 'with_active_and_version' => sub {
            my %args = (
                active  => 0,
                version => 2,
            );
            my $k = $zs->create_zaaktype_ok(%args);
            isa_ok($k, $isa);
            ok($k->id, "Has an ID");

            my %result =
              map {$_ => $k->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");

            %result =
              map {$_ => $k->zaaktype_node_id->$_} keys %args;
            is_deeply(\%result, \%args, "Node has the same properties");
        };

        {    # error handling
            my $node = $zs->create_zaaktype_node_ok();
            my $error =
              qr/Conflicts! You cannot pass a node and active\/version params/;
            throws_ok(
                sub {
                    $zs->create_zaaktype_ok(
                        active => 1, node => $node,
                    );
                },
                $error,
                "Conflicting params: active/node"
            );

            throws_ok(
                sub {
                    $zs->create_zaaktype_ok(
                        version => 1, node => $node,
                    );
                },
                $error,
                "Conflicting params: version/node"
            );

            # this should pass
            $zs->create_zaaktype_ok(
                version => undef, active => undef, node => $node,
            );
        }
    },
    'Zaaktype',
);

$zs->zs_transaction_ok(
    sub {

        my $isa = 'Zaaksysteem::Schema::Bag';

        my @bag_types = @{BAG_TYPES()};
        foreach (@bag_types) {
            my $func = sprintf("create_bag_%s_ok", $_);
            if ($zs->can($func)) {
                my $bag = $zs->$func();
                isa_ok($bag, $isa . ucfirst($_));
                ok($bag->id, "Has an ID");
            }
            else {
                TODO: {
                    local $TODO = "unsupported";
                    fail("Cannot perform $func");
                }
            }
        }

    },
    'BAG',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = "Zaaksysteem::Model::DB::GmNatuurlijkPersoon";

        {
            my $np = $zs->create_gm_natuurlijk_persoon_ok();
            isa_ok($np, $isa);
            ok($np->id, "Has an ID");
        }

        {
            my $np = $zs->create_gm_natuurlijk_persoon_ok(
                adres_id => 2,
            );
            isa_ok($np, $isa);
            ok($np->id, "Has an ID");

            isa_ok($np->adres_id, "Zaaksysteem::Model::DB::GmAdres");
            is($np->adres_id->id, 2, "adres_id is correct");
        }
    },
    'GmNatuurlijkPersoon',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = "Zaaksysteem::Model::DB::ZaaktypeRegel";
        my $regel = $zs->create_zaaktype_regel_ok();
        isa_ok($regel, $isa);
    },
    "zaaktype_regel"
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::ZaaktypeResultaten';
        subtest zaaktype_resultaat_ok => sub {
            my $ztk = $zs->create_zaaktype_resultaat_ok();
            isa_ok($ztk, $isa);
            ok($ztk->id, "$isa has an ID");

            isa_ok($ztk->zaaktype_node_id, 'Zaaksysteem::Model::DB::ZaaktypeNode');
            isa_ok($ztk->zaaktype_status_id, 'Zaaksysteem::Model::DB::ZaaktypeStatus');

            foreach (qw(resultaat ingang bewaartermijn created last_modified dossiertype label selectielijst archiefnominatie)) {
                ok($ztk->$_, "$_ is defined");
            }
        };

        subtest zaaktype_resultaat_ok_with_zaaktype_sub_stuff => sub {
            my $node   = $zs->create_zaaktype_node_ok();
            my $status = $zs->create_zaaktype_status_ok();

            my $ztk = $zs->create_zaaktype_resultaat_ok(
                node => $node,
            );
            is($ztk->zaaktype_node_id->id, $node->id, 'Node is correct');

            $ztk = $zs->create_zaaktype_resultaat_ok(
                status => $status,
            );
            is($ztk->zaaktype_status_id->id, $status->id, 'Status is correct');

            throws_ok(
                sub {
                    $zs->create_zaaktype_resultaat_ok(
                        status => $status,
                        node   => $node,
                    );
                },
                qr/Conflicting arguments: node and status/,
                "Node en status mogen niet beide opgegeven worden"
            );
        };

        subtest zaaktype_resultaat_ok_with_args => sub {
            my $m = "Meuk";
            my %args = (
                archiefnominatie => $m,
                comments         => $m,
                created          => DateTime->now->add(days => 1),
                dossiertype      => $m,
                ingang           => $m,
                label            => $m,
                last_modified    => DateTime->now->add(months => 1),
                resultaat        => $m,
                selectielijst    => $m,
                bewaartermijn    => 100,
            );

            my $ztk = $zs->create_zaaktype_resultaat_ok(%args);
            isa_ok($ztk, $isa);
            ok($ztk->id, "$isa has an ID");

            my %result =
              map {$_ => $ztk->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };

    },
    'Zaaktype Kernmerk',
);
$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::ZaaktypeKenmerken';
        subtest zaaktype_kenmerk_ok => sub {
            my $ztk = $zs->create_zaaktype_kenmerk_ok();
            isa_ok($ztk, $isa);
            ok($ztk->id, "$isa has an ID");

            isa_ok($ztk->bibliotheek_kenmerken_id, 'Zaaksysteem::Model::DB::BibliotheekKenmerken');
            isa_ok($ztk->zaaktype_node_id, 'Zaaksysteem::Model::DB::ZaaktypeNode');
            isa_ok($ztk->zaak_status_id, 'Zaaksysteem::Model::DB::ZaaktypeStatus');

        };

        subtest zaaktype_kenmerk_ok_with_zaaktype_sub_stuff => sub {
            my $bieb   = $zs->create_bibliotheek_kenmerk_ok();
            my $node   = $zs->create_zaaktype_node_ok();
            my $status = $zs->create_zaaktype_status_ok();

            my $ztk = $zs->create_zaaktype_kenmerk_ok(
                bibliotheek_kenmerk => $bieb,
            );
            is($ztk->bibliotheek_kenmerken_id->id, $bieb->id, 'BibliotheekKenmerken correct');

            $ztk = $zs->create_zaaktype_kenmerk_ok(
                node => $node,
            );
            is($ztk->zaaktype_node_id->id, $node->id, 'Node is correct');

            $ztk = $zs->create_zaaktype_kenmerk_ok(
                status => $status,
            );
            is($ztk->zaak_status_id->id, $status->id, 'Status is correct');

            throws_ok(
                sub {
                    $zs->create_zaaktype_kenmerk_ok(
                        status => $status,
                        node   => $node,
                    );
                },
                qr/Conflicting arguments: node and status/,
                "Node en status mogen niet beide opgegeven worden"
            );
        };

        subtest zaaktype_kenmerk_ok_with_args => sub {
            my %args = (
                help                 => "Help me!",
                help_extern          => "Help me naar buiten!",
                is_group             => 1,
                is_systeemkenmerk    => 1,
                label                => "Label me",
                pip                  => 1,
                pip_can_change       => 0,
                publish_public       => 0,
                referential          => 'Str',
                required_permissions => 'Str',
                value_default        => 'default value',
                value_mandatory      => 1,
                version              => 2,
                zaakinformatie_view  => 3,
            );

            my $ztk = $zs->create_zaaktype_kenmerk_ok(%args);
            isa_ok($ztk, $isa);
            ok($ztk->id, "$isa has an ID");

            my %result =
              map {$_ => $ztk->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };

    },
    'Zaaktype Kernmerk',
);

$zs->zs_transaction_ok(
    sub {
        my $profile = Zaaksysteem::TestUtils::AANVRAGER_NP_PROFILE();

        my %info = map { 'np-' . $_ => $profile->{defaults}{$_} } keys %{$profile->{defaults}};
        my $aanvrager = $zs->create_aanvrager_np_ok();
        my $expect = {
            create          => \%info,
            betrokkene_type => 'natuurlijk_persoon',
            verificatie     => 'medewerker',
        };
        is_deeply($expect, $aanvrager, "Aanvrager is ok");

        my %args = map { $_ => 1 } keys %{$profile->{optional}};
        $expect = {
            create          => \%args,
            betrokkene_type => 'natuurlijk_persoon',
            verificatie     => 'medewerker',
        };

        my $isa = "Zaaksysteem::Model::DB::NatuurlijkPersoon";
        my $np = $zs->create_natuurlijk_persoon_ok();
        isa_ok($np, $isa);

    },
    "Natuurlijk persoon"
);

$zs->zs_transaction_ok(
    sub {
        my $isa = "Zaaksysteem::Model::DB::Bedrijf";
        my $np = $zs->create_bedrijf_ok();
        isa_ok($np, $isa);
    },
    "Bedrijf"
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::ZaaktypeSjablonen';
        subtest zaaktype_sjabloon_ok => sub {
            my $zts = $zs->create_zaaktype_sjabloon_ok();
            isa_ok($zts, $isa);
            ok($zts->id, "$isa has an ID");

            isa_ok($zts->bibliotheek_kenmerken_id, 'Zaaksysteem::Model::DB::BibliotheekKenmerken');
            isa_ok($zts->zaaktype_node_id, 'Zaaksysteem::Model::DB::ZaaktypeNode');
            isa_ok($zts->zaak_status_id, 'Zaaksysteem::Model::DB::ZaaktypeStatus');

            ok($zts->help, "Help is set");
            ok($zts->created, "Created is correct");
            ok($zts->last_modified, "Last modified is correct");
            is($zts->target_format, 'odt', "Default target_format is set");
            ok($zts->automatisch_genereren, "Automatisch genereren is set");

        };

        subtest zaaktype_sjabloon_ok_with_zaaktype_sub_stuff => sub {
            my $bieb     = $zs->create_bibliotheek_kenmerk_ok();
            my $sjabloon = $zs->create_sjabloon_ok();
            my $node     = $zs->create_zaaktype_node_ok();
            my $status   = $zs->create_zaaktype_status_ok();

            my $ztk = $zs->create_zaaktype_sjabloon_ok(
                bibliotheek_kenmerk => $bieb,
            );
            is($ztk->bibliotheek_kenmerken_id->id, $bieb->id, 'BibliotheekKenmerken correct');

            $ztk = $zs->create_zaaktype_sjabloon_ok(
                bibliotheek_sjabloon => $sjabloon,
            );
            is($ztk->bibliotheek_sjablonen_id->id, $sjabloon->id, 'BibliotheekSjablonen correct');

            $ztk = $zs->create_zaaktype_sjabloon_ok(
                node => $node,
            );
            is($ztk->zaaktype_node_id->id, $node->id, 'Node is correct');

            $ztk = $zs->create_zaaktype_sjabloon_ok(
                node => $node,
            );
            is($ztk->zaaktype_node_id->id, $node->id, 'Node is correct');

            $ztk = $zs->create_zaaktype_sjabloon_ok(
                status => $status,
            );
            is($ztk->zaak_status_id->id, $status->id, 'Status is correct');

            throws_ok(
                sub {
                    $zs->create_zaaktype_sjabloon_ok(
                        status => $status,
                        node   => $node,
                    );
                },
                qr/Conflicting arguments: node and status/,
                "Node en status mogen niet beide opgegeven worden"
            );
        };

        subtest zaaktype_sjabloon_ok_with_args => sub {
            my $now = DateTime->now;
            my $last_modif = $now->add(months => 1);
            my %args = (
                help                  => "Help me!",
                created               => $now,
                last_modified         => $last_modif,
                target_format         => 'pdf',
                automatisch_genereren => 0,
            );

            my $ztk = $zs->create_zaaktype_sjabloon_ok(%args);
            isa_ok($ztk, $isa);
            ok($ztk->id, "$isa has an ID");

            my %result =
              map {$_ => $ztk->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };

    },
    'Zaaktype Sjabloon',
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Model::DB::ZaaktypeRelatie';

        subtest 'create_zaaktype_relatie_ok' => sub {
            my $rel = $zs->create_zaaktype_relatie_ok();
            isa_ok($rel, $isa);
            ok($rel->id, "$isa has an ID");
        };

        subtest 'create_zaaktype_relatie_withmeuk' => sub {
            my %args = (
                relatie_type           => "LAT",
                eigenaar_type          => "Getrouwd",
                created                => DateTime->now(),
                last_modified          => DateTime->now(),
                start_delay            => "straks",
                kopieren_kenmerken     => 0,
                status                 => 1,
                ou_id                  => 2,
                role_id                => 2,
                automatisch_behandelen => 0,
                required               => "Mwah",
                parent_advance_results => "Als hij er blij van wordt",
            );
            my $rel = $zs->create_zaaktype_relatie_ok(%args);
            isa_ok($rel, $isa);
            ok($rel->id, "$isa has an ID");

            my %result =
              map {$_ => $rel->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };

        subtest 'create_zaaktype_relatie_ok_met_andere_testsuite_ok' => sub {

            my $node   = $zs->create_zaaktype_node_ok();
            my $zt     = $zs->create_zaaktype_ok();
            my $status = $zs->create_zaaktype_status_ok();

            subtest 'avec_node' => sub {
                my $rel = $zs->create_zaaktype_relatie_ok(
                    zt_node   => $node,
                );
                isa_ok($rel, $isa);
                ok($rel->id, "$isa has an ID");
                is($rel->zaaktype_node_id->id, $node->id, "Correct node");

                is($rel->zaaktype_status_id->zaaktype_node_id->id, $node->id, "Found the node via status");
                is($rel->relatie_zaaktype_id->zaaktype_node_id->id, $node->id, "Found the node via zaaktype");
            };

            subtest 'avec_status' => sub {
                my $rel = $zs->create_zaaktype_relatie_ok(
                    zt_status => $status,
                );
                isa_ok($rel, $isa);
                ok($rel->id, "$isa has an ID");
                is($rel->zaaktype_status_id->id, $status->id, "Correct zaaktype_status");

                my $node = $status->zaaktype_node_id->id;
                is($rel->relatie_zaaktype_id->zaaktype_node_id->id, $node, "Found the node via zaaktype");
                is($rel->zaaktype_node_id->id, $node, "Found the node via the node");
            };

            subtest 'avec_zaaktype' => sub {
                my $rel = $zs->create_zaaktype_relatie_ok(
                    zaaktype => $zt,
                );
                isa_ok($rel, $isa);
                ok($rel->id, "$isa has an ID");
                is($rel->relatie_zaaktype_id->id, $zt->id, "Correct zaaktype");

                my $node = $zt->zaaktype_node_id->id;
                is($rel->zaaktype_node_id->id, $node, "Found the node via the node");
                is($rel->zaaktype_status_id->zaaktype_node_id->id, $node, "Found the node via status");
            };

            throws_ok(
                sub {
                    $zs->create_zaaktype_relatie_ok(
                        zt_node   => $node,
                        zt_status => $status,
                        zaaktype  => $zt,
                    );

                },
                qr/zt_node, zt_status and zaaktype/,
                "Zaaktype, node en status zijn geen vriendjes"
            );
            throws_ok(
                sub {
                    $zs->create_zaaktype_relatie_ok(
                        zt_status => $status,
                        zaaktype  => $zt,
                    );

                },
                qr/zt_node, zt_status and zaaktype/,
                "Zaaktype en status ook niet",
            );
            throws_ok(
                sub {
                    $zs->create_zaaktype_relatie_ok(
                        zt_status => $status,
                        zaaktype  => $zt,
                    );

                },
                qr/zt_node, zt_status and zaaktype/,
                "Zaaktype en node ook niet",
            );
            throws_ok(
                sub {
                    $zs->create_zaaktype_relatie_ok(
                        zt_node   => $node,
                        zt_status => $status,
                    );

                },
                qr/zt_node and zt_status/,
                "Node en status zijn geen vriendjes"
            );
        };
    },
    "Relatie"
);

$zs->zs_transaction_ok(
    sub {
        my $isa = 'Zaaksysteem::Schema::Zaak';

        subtest 'create_case_ok()' => sub {
            my $case = $zs->create_case_ok();
            isa_ok($case, $isa);
            is($case->confidentiality, 'public', "Default vertrouwelijkheid");
        };

        subtest 'create_case_ok_with_args' => sub {
            my $zaaktype = $zs->create_zaaktype_ok();
            $zs->create_zaaktype_status_ok(
                status => 1,
                fase   => 'registratiefase',
                node   => $zaaktype->zaaktype_node_id(),
            );

            my $bag = $zs->create_bag_openbareruimte_ok();
            my $now = DateTime->now();

            my $aanvrager = $zs->create_aanvrager_np_ok();
            my %args = (
                aanvragers       => [$aanvrager],
                zaaktype         => $zaaktype,
                registratiedatum => $now,
                aanvraag_trigger => 'extern',
                contactkanaal    => 'email',
                locatie_zaak     => {
                    bag_type => 'openbareruimte',
                    bag_id   => $bag->id
                },
                confidentiality => 'internal',
            );

            my $case = $zs->create_case_ok(%args);
            is($case->confidentiality, 'internal', "'interne' vertrouwelijkheid");
            isa_ok($case, $isa);
        };

        subtest 'create_case_ok_with_bedrijf' => sub {
            my $aanvrager = $zs->create_aanvrager_bedrijf_ok();
            my %args = (aanvragers => [$aanvrager],);
            my $case = $zs->create_case_ok(%args);
            isa_ok($case, $isa);
            ok($case->id, "$isa has an ID");
        };

        subtest 'create_case_ok_with_existing_betrokkene' => sub {
            my $bedrijf = $zs->create_bedrijf_ok();
            my %args = (
                aanvragers => [{
                        betrokkene  => 'betrokkene-bedrijf-' . $bedrijf->id,
                        verificatie => 'medewerker'
                    }
                ],
            );
            my $case = $zs->create_case_ok(%args);
            isa_ok($case, $isa);
            ok($case->id, "$isa has an ID");
        };

        subtest 'create_case_ok_with_kenmerken' => sub {
            my $zaaktype = $zs->create_zaaktype_ok();
            my $status   = $zs->create_zaaktype_status_ok(
                status => 1,
                fase   => 'registratiefase',
                node   => $zaaktype->zaaktype_node_id(),
            );

            my $ztk = $zs->create_zaaktype_kenmerk_ok(
                status => $status,
                label  => 'testkenmerk',
            );

            my $kenmerk_value = 'De testsuite is awesome!!';
            my $case = $zs->create_case_ok(
                zaaktype  => $zaaktype,
                kenmerken => [ { 1 => $kenmerk_value } ]
            );
            isa_ok($case, $isa);

            my $res = $zs->get_from_zaaksysteem(resultset => 'ZaakKenmerk', search => {zaak_id => $case->id}, options => {row => 1})->single;
            my $val = $res->value;
            is($val, $kenmerk_value, "We have what we are looking for");
        };

        subtest 'create_case_ok_with_sjablonen' => sub {
            my $zaaktype = $zs->create_zaaktype_ok();
            my $status   = $zs->create_zaaktype_status_ok(
                status => 1,
                fase   => 'registratiefase',
                node   => $zaaktype->zaaktype_node_id(),
            );

            $status = $zs->create_zaaktype_status_ok(
                status   => 2,
                fase     => 'afhandelfase',
                node     => $zaaktype->zaaktype_node_id(),
                role_set => 0,
                role_id  => 0,
            );

            my $zts = $zs->create_zaaktype_sjabloon_ok(
                status => $status,
            );

            my $resultaat = "Testsuite awesomesaus";
            $zs->create_zaaktype_resultaat_ok(
                resultaat => $resultaat,
                node      => $zaaktype->zaaktype_node_id,
            );

            my $case = $zs->create_case_ok(
                zaaktype => $zaaktype,
            );
            isa_ok($case, $isa);

            my $res = $zs->get_from_zaaksysteem(
                resultset => 'File',
                search    => {case_id => $case->id}, options => {row => 1}
            )->single;
            ok(!$res, "Did not found the file");

            my $subject = $zs->create_subject_ok();

            $case->wijzig_behandelaar({
                    'betrokkene_identifier' => 'betrokkene-medewerker-'
                      . $subject->id
            });
            $case->resultaat($resultaat);
            $case->advance({context => 1});

            $res = $zs->get_from_zaaksysteem(
                resultset => 'File',
                search    => {case_id => $case->id}, options => {row => 1}
            )->single;
            ok($res, "Found the file");
        };
    },
    'create_case_ok looks functional'
);

$zs->zs_transaction_ok(sub {
    subtest create_zaaktype_authorisation_ok_with_defaults => sub {
        my $auth;

        lives_ok sub { $auth = $zs->create_zaaktype_authorisation_ok(); },
            'create_zaaktype_authorisation_ok doesn\'t die';

        isa_ok $auth, 'Zaaksysteem::DB::Component::ZaaktypeAuthorisation',
            'create_zaaktype_authorisation_ok returns proper object';

        ok $auth->get_column('zaaktype_id'),
            'create_zaaktype_authorisation_ok has a zaaktype_id (implying that create_zaaktype_ok is implicitly called)';

        ok $auth->get_column('ou_id'),
            'create_zaaktype_authorisation_ok has a default ou_id';

        ok $auth->get_column('role_id'),
            'create_zaaktype_authorisation_ok has a default role_id';

        ok $auth->get_column('recht'),
            'create_zaaktype_authorisation_ok has a default recht';
    };

    subtest create_zaaktype_authorisation_ok_with_params => sub {
        my $auth;

        my %params = (
            ou_id => int(rand(100000)),
            role_id => int(rand(100000)),
            recht => 'zaak_edit'
        );

        lives_ok sub { $auth = $zs->create_zaaktype_authorisation_ok(%params); },
            'create_zaaktype_authorisation_ok doesn\'t die';

        isa_ok $auth, 'Zaaksysteem::DB::Component::ZaaktypeAuthorisation',
            'create_zaaktype_authorisation_ok returns proper object';

        is $auth->get_column('ou_id'), $params{ ou_id },
            'create_zaaktype_authorisation_ok has a default ou_id';

        is $auth->get_column('role_id'), $params{ role_id },
            'create_zaaktype_authorisation_ok has a default role_id';

        is $auth->get_column('recht'), $params{ recht },
            'create_zaaktype_authorisation_ok has a default recht';
    };

    subtest create_zaaktype_authorisation_ok_with_zaaktype => sub {
        my $auth;

        my %params = (
            zaaktype => $zs->create_zaaktype_ok
        );

        lives_ok sub { $auth = $zs->create_zaaktype_authorisation_ok(%params); },
            'create_zaaktype_authorisation_ok doesn\'t die';

        isa_ok $auth, 'Zaaksysteem::DB::Component::ZaaktypeAuthorisation',
            'create_zaaktype_authorisation_ok returns proper object';

        is $auth->get_column('zaaktype_id'), $params{ zaaktype }->id,
            'create_zaaktype_authorisation_ok has a zaaktype_id (implying that create_zaaktype_ok is implicitly called)';
    };
}, 'create_zaaktype_authorisation_ok looks functional');

$zs->zs_transaction_ok(
    sub {

        my $isa = 'Zaaksysteem::Model::DB::Checklist';

        subtest 'checklist_ok' => sub {
            my $c = $zs->create_case_checklist_ok();
            isa_ok($c, $isa);
            ok($c->id, "$isa has an ID");

            isa_ok($c->case_id, "Zaaksysteem::Schema::Zaak");
        };

        subtest 'checklist_ok_with_case' => sub {
            my $case = $zs->create_case_ok();
            my $c = $zs->create_case_checklist_ok(zaak => $case);
            isa_ok($c, $isa);
            ok($c->id, "$isa has an ID");

            isa_ok($c->case_id, "Zaaksysteem::Schema::Zaak");
            is($c->case_id->id, $case->id, "Case found!");
        };

        subtest 'checklist_ok_with_milestone' => sub {
            my $c = $zs->create_case_checklist_ok(milestone => 42);
            isa_ok($c, $isa);
            ok($c->id, "$isa has an ID");
            is($c->case_milestone, 42, "Correct milestone");
        };
    },
    "Checklist for a case"
);

$zs->zs_transaction_ok(
    sub {

        my $isa = 'Zaaksysteem::Model::DB::ChecklistItem';

        subtest 'checklist_item_ok' => sub {
            my $c = $zs->create_case_checklist_item_ok();
            isa_ok($c, $isa);
            ok($c->id, "$isa has an ID");

            isa_ok($c->checklist_id, 'Zaaksysteem::Model::DB::Checklist');
        };

        subtest 'checklist_ok_with_case' => sub {
            my $cl = $zs->create_case_checklist_ok();
            my $c = $zs->create_case_checklist_item_ok(checklist => $cl);
            isa_ok($c, $isa);
            ok($c->id, "$isa has an ID");

            isa_ok($c->checklist_id, 'Zaaksysteem::Model::DB::Checklist');
            is($c->checklist_id->id, $cl->id, "Checklist found!");
        };

        subtest 'checklist_ok_with_args' => sub {
            my %args = (
                label             => 'Meuk',
                state             => 1,
                user_defined      => 0,
                deprecated_answer => 'Blub'
            );

            my $c = $zs->create_case_checklist_item_ok(%args);
            isa_ok($c, $isa);
            ok($c->id, "$isa has an ID");

            my %result = map {$_ => $c->$_} keys %args;
            is_deeply(\%result, \%args, "equals input params");
        };
    },
    "Checklist item for a case"
);

$zs->zs_transaction_ok(
    sub {

        subtest 'set_current_user' => sub {
            my $current_user = $zs->set_current_user();
            isa_ok($current_user, 'Zaaksysteem::Schema::Subject');
        };

        subtest 'set_current_user_with_username' => sub {
            my $username = "wesleys";
            my $current_user = $zs->set_current_user(username => "wesleys");
            isa_ok($current_user, 'Zaaksysteem::Schema::Subject');
            is($current_user->username, "wesleys", "Correct username");
        };

        subtest 'set_current_user_with_subject' => sub {
            my $subject = $zs->create_subject_ok();
            my $current_user = $zs->set_current_user(subject => $subject);
            isa_ok($current_user, 'Zaaksysteem::Schema::Subject');
            is($current_user->id, $subject->id, "Same subject");
        };

        subtest 'set_current_user_with_userentity' => sub {
            my $ue = $zs->create_userentity_ok();
            my $current_user = $zs->set_current_user(userentity => $ue);
            isa_ok($current_user, 'Zaaksysteem::Schema::Subject');
            is($current_user->id, $ue->subject_id->id, "Subject from userentity");
        };

    },
    "Set current user"
);


zs_done_testing();

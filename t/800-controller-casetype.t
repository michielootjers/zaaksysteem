#! /usr/bin/perl
use TestSetup;
initialize_test_globals_ok;

use Test::WWW::Mechanize::Catalyst;

ensure_test_server_ok();

my $mech = Test::WWW::Mechanize::Catalyst->new(catalyst_app => 'Zaaksysteem');

$mech->get_ok('/', "GET / works");
$mech->submit_form_ok(
    {
        with_fields => {
            username => 'admin',
            password => 'admin',
        },
    },
    'Logged in successfully',
);

my $casetype_id;
{
    $mech->get_ok('/casetype');
    my $json = parse_zapi_response_ok($mech->response);

    is($json->{result}[0]{object_type}, 'zaaktype', "Got zaaktype from result");
    is($json->{rows}, 1, "Got single zaaktype from result");

    $casetype_id = $json->{result}[0]{id};

    $mech->post_ok(
        "/beheer/zaaktypen/$casetype_id/offline",
        {
            confirmed => 1,
            commit_message => 'bla',
        },
    );
}

{
    $mech->get_ok('/casetype');
    my $json = parse_zapi_response_ok($mech->response);
    is($json->{rows}, 0, "Got no zaaktypes from result");
}

{
    $mech->get_ok('/casetype?inactive=1', "Requesting all zaaktypes, including inactive");
    my $json = parse_zapi_response_ok($mech->response);

    is($json->{result}[0]{object_type}, 'zaaktype', "Got zaaktype from result");
    is($json->{rows}, 1, "Got no zaaktypes from result");

    is($json->{result}[0]{id}, $casetype_id, "Got correct zaaktype from result");
}

# Put it back how it was.
{
    $mech->post_ok(
        "/beheer/zaaktypen/$casetype_id/online",
        {
            confirmed => 1,
            commit_message => 'bla',
        },
    );
    $mech->get_ok('/casetype');
    my $json = parse_zapi_response_ok($mech->response);
    is($json->{rows}, 1, "Got single zaaktypes after re-enabling");
}

zs_done_testing();

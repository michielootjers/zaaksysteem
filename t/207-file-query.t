#! perl

### Test header start
use warnings;
use strict;
    
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


sub create_file {
    my ($rs, $destroyed, $deleted) = @_;

    my $name = ($destroyed ? 'destroyed' : 'notdestroyed') . '-' .
                   ($deleted ? 'deleted' : 'notdeleted') . '.txt';

    my $file = $rs->file_create({
        db_params => {
            created_by        => $zs->get_subject_ok,
        },
        file_path => $zs->config->{filestore_test_file_path},
        name      => $name,
        destroyed => $destroyed,
    });

    $file->update({date_deleted => DateTime->now}) if $deleted;
    $file->update({destroyed => 1}) if $destroyed;
}


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    my $rs = $schema->resultset('File');

    is scalar $case->active_files, 0, "Initially zero files present";

    my $file = $zs->create_file_ok;
    $file->update({ case_id => $case->id });

    is scalar $case->active_files, 1, "One file present after adding one";

    my $result = $file->update_file({
        subject       => $zs->get_subject_ok,
        original_name => 'Original Name.jpg',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    is $file->get_root_file->id, $file->id, 'Got self as root';
    is $result->get_root_file->id, $file->id, 'Got first as root from second';

    is scalar $case->active_files, 1, "Still one file present after creating a new version";

    ok($result->active_version, 'Copy has active_version');

}, 'File: Select relevant case files for display');


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    my $rs = $schema->resultset('File');

    is scalar $case->active_files, 0, "Initially zero files present";

    my $file = $zs->create_file_ok;
    $file->update({ case_id => $case->id });

    is scalar $case->active_files, 1, "One file present after adding one";

    $file->update({ destroyed => 1 });

    is scalar $case->active_files, 0, "No files present after destroying one";

}, 'File: Select relevant case files for display, check destroy');


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    my $rs = $schema->resultset('File');

    is scalar $rs->active_files, 0, "Initially zero files present";

    my $file = $zs->create_file_ok;
    is scalar $rs->active_files, 1, "One file present after adding one";

    $file->update({ destroyed => 1 });

    is scalar $rs->active_files, 0, "No files present after destroying one";

}, 'File: Select relevant files by id for display');


$zs->zs_transaction_ok(sub {

    my $rs = $schema->resultset('File');

    # all permutations of destroyed and deleted
    for my $destroyed (0, 1) {
        for my $deleted (0, 1) {
            create_file($rs, $destroyed, $deleted);
        }
    }

    is ($rs->count, 4, '4 files created');
    is ($rs->active->count, 1, '1 of them is considered active');
    is ($rs->active->first->name, 'notdestroyed-notdeleted', 'and we call it notdestroyed-notdeleted.txt');

}, 'file: active only get active files: not destroyed and not deleted');




zs_done_testing();

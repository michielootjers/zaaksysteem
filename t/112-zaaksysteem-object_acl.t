#! perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Backend::Object;
use Zaaksysteem::Search::ZQL;


$zs->zs_transaction_ok(sub {
    my $auth = $zs->create_zaaktype_authorisation_ok;

    my $acls = $schema->resultset('ObjectAclEntry');

    is $acls->count, 1, 'Implicitly created ACL entry through zaaktype_authorisation creation';
}, 'object/acl creating zaaktype creates acl entries');

# ZS-2157 - ACLs must be deleted with an object deletion
$zs->zs_transaction_ok(sub {
    my $auth = $zs->create_zaaktype_authorisation_ok;

    my $acls = $schema->resultset('ObjectAclEntry');

    is $acls->count, 1, 'Implicitly created an ACL entry';

    my $objects = Zaaksysteem::Backend::Object->new( schema => $schema );

    my $object = $objects->rs->find({ object_class => 'casetype', object_id => $auth->get_column('zaaktype_id') });

    isa_ok $object, 'Zaaksysteem::Backend::Object::Data::Component', 'object found';

    lives_ok sub { $object->delete }, 'object/acl object with acls deletes ok';

    is $acls->count, 0, 'object/acl acls implicitly delted through cascade';
});

# TODO: Extend tests when create_case_ok doesn't fail

# The following two tests 'prove' the most important aspects of ObjectAcl
# interactions. The proof lies with the subquery being properly generated.
# The 'properness' of the subquery is of course my interpretation.
$zs->zs_transaction_ok(sub {
    lives_ok(
        sub { Zaaksysteem::Backend::Object->new( schema => $schema )->rs },
        'object/acl object returns resultset'
    );
}, 'object/acl returns resultset without user');

$zs->zs_transaction_ok(sub {
    my $user = $zs->create_subject_ok;
    my $ldap = $zs->connect_ldap_ok;

    my $object = Zaaksysteem::Backend::Object->new(
        schema => $schema,
        user => $user,
        ldap => $ldap
    );

    my @positions;

    for my $ou ($ldap->user_ous($user)) {
        for my $role ($ldap->get_roles($user)) {
            push @positions, {
                entity_type => 'position',
                object_uuid => '<class_uuid>',
                capability => 'read',
                verdict => 'permission',
                entity_id => sprintf(
                    '%s|%s',
                    $ou->get_value('l'),
                    $role->get_value('gidNumber')
                )
            };
        }
    }
    
    is_deeply $object->acl_items('read', '<object_uuid>', '<class_uuid>'), {
        -or => [
            {
                entity_id => $user->username,
                entity_type => 'user',
                object_uuid => '<object_uuid>',
                verdict => 'permission',
                capability => 'read'
            },
            @positions
        ]
    }, 'object/acl valid sql query';
}, 'object/acl query generator is ok');

$zs->zs_transaction_ok(sub {
    my $user = $zs->create_subject_ok;
    my $ldap = $zs->connect_ldap_ok;

    my $user_ou_id = $ldap->user_ou($user)->get_value('l');
    my ($user_role_id) = map { $_->get_value('gidNumber') } $ldap->get_roles($user);

    my $auth = $zs->create_zaaktype_authorisation_ok(
        ou_id => $user_ou_id,
        role_id => $user_role_id
    );

    my $zaaktype = $auth->zaaktype_id;

    my $test_object = $zs->create_zaaktype_authorisation_ok->zaaktype_id->_object;
    $test_object->object_class('case');
    $test_object->class_uuid($zaaktype->_object->uuid);
    $test_object->update;

    # First we remove all ACLs so we can verify ACL-injected resultsets
    # really hide the content
    my $object = Zaaksysteem::Backend::Object->new( schema => $schema );

    $object->rs->find($test_object->get_column('class_uuid'))->object_acl_entries->delete_all;

    ok $object->rs->find($test_object->uuid),
        'object/acl/casetype acl entries deleted, but object remains';

    is $object->rs->find($test_object->get_column('class_uuid'))->object_acl_entries->count, 0,
        'object/acl/casetype acls removed';

    my $object_user = Zaaksysteem::Backend::Object->new(
        schema => $schema,
        user => $user,
        ldap => $ldap
    );

    is $object_user->rs->search({ object_class => 'case' })->count, 0, 'object/acl/casetype user cant find object';

    my $updated_zaaktype_object = $zaaktype->_sync_object;
    
    ok $updated_zaaktype_object->object_acl_entries->count > 0,
        'object/acl/casetype sync_objects adds acls';
    
    $object_user = Zaaksysteem::Backend::Object->new(
        schema => $schema,
        user => $user,
        ldap => $ldap
    );

    is $object_user->rs->search({ object_class => 'case' })->count, 1, 'object/acl/casetype user can find object after sync';
}, 'object/acl ACLs actually hide objects in resultset');

# TODO: Extend tests here with a zs_transaction_ok that checks multi-object permissions
# This is not yet possible because of limitations in LDAP mocking (every user has every 
# role, so you can't contrast one user's result to another)

zs_done_testing;

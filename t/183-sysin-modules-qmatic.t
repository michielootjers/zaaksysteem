#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

use_ok 'Zaaksysteem::Backend::Sysin::Modules::QMatic';

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;

sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'qmatic',
            name                => 'QMatic module',
            interface_config    => {
            }
        },                                
    );  
}


$zs->zs_transaction_ok(sub {
    create_config_interface();

}, 'Create QMatic interface');

zs_done_testing();

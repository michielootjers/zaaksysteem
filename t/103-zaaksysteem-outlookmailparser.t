#! perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Data::Dumper;
use Cwd;

BEGIN { use_ok('Zaaksysteem::OutlookMailParser') };


my $outlookMailParser = Zaaksysteem::OutlookMailParser->new({tmpDir => '/tmp'});
my $testFolder = getcwd() . "/t/inc/OutlookTestMails/";

sub parse {
    my ($filename) = @_;

    my $body = $outlookMailParser->parseMSG({
        path => $testFolder . $filename,
        original_name => $filename
    });

    my $attachments = $outlookMailParser->attachments;

    return ($body, $attachments);    
}

$zs->zs_transaction_ok(sub {

    my $settings = {
        body => 'dit is dan zeg maar de body',
        from => 'en dit de afzender',
        to => 'hierzo de ontvanger',
        subject => 'en hier gaat het over',
    };

    throws_ok( sub {
        $outlookMailParser->format({kaas => 1});
    }, qr/Validation of profile failed/, 'Fails when params are not correct');


    my $formatted = $outlookMailParser->format($settings);

    for my $key (qw/body from to subject/) {
        my $value = $settings->{$key};
        ok $formatted =~ m|$value|s, "$value is present in formatted string";
    }

    ok $formatted =~ m|Bijlagen\: geen|, "Contains header 'Bijlagen: geen'";

    # now add attachments
    $settings->{attachments} = [{
        'email_filename' => 'Test A.pdf',
        'file_id' => 84,
        'filename' => 'lukeskywalker.pdf'
    }, {
        'email_filename' => 'bla.pdf',
        'file_id' => 85,
        'filename' => 'darthvader.pdf'
    }];

    $formatted = $outlookMailParser->format($settings);

    ok $formatted =~ m|Bijlagen\:|, "Contains header 'Bijlagen'";
    ok $formatted =~ m|darthvader.pdf|, "Contains PDF attachment";
    ok $formatted =~ m|lukeskywalker.pdf|, "Contains PDF attachment";

}, 'Test format mail body');


$zs->zs_transaction_ok(sub {

	my $subject = $zs->get_subject_ok;
    my $case = $zs->create_case_ok();


    dies_ok sub {
        my ($body, $attachments) = parse('Non existing file');
    }, 'Should die on non-existing file';


    my ($body, $attachments) = parse('Tekst only.msg');

    ok $body, 'Parse succeeds';
    ok $body->{ mime } eq 'text/plain', 'Should have a body in text/plain format';
    ok scalar @{ $attachments } == 0, 'Plain text only message should have no attachments';

    ($body, $attachments) = parse('Tekst met bijlage - Word.msg');
    ok @$attachments == 1, 'Should have one attachment';
    ok $$attachments[0]->{name} eq '20130913 test verslag contractbeheer.docx', 'Should have this name';


    ($body, $attachments) = parse('Tekst met bijlages - Plaatje excel word pdf.msg');
    ok @$attachments == 5, 'Should have five attachments';
    ok $$attachments[0]->{name} eq '20130913 test verslag contractbeheer.docx', 'Should have this name';


}, 'Parse Outlook .MSG mail into different parts');

zs_done_testing();

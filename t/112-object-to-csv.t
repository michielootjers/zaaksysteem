#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Backend::Object');

my @REQUIRED_OBJECT_RESULT_PARAMS = qw/
    object_type
    id
    values
/;

use Pg::hstore;



sub create_object_ok {
    my $case_id         = shift;
    $case_id        ||= 1;
    my $relationship    = shift;

    $schema->resultset('ObjectData')->create(
        {
            object_class    => 'case',
            object_id       => $case_id,
            #text_vector     => \"'a bit of a test string'::tsvector",
            properties      => {
                values => {
                    'case.id'           => Zaaksysteem::Backend::Object::Attribute->new(
                        name            => 'case.id',
                        value           => $case_id,
                        attribute_type  => 'integer',
                    )->TO_JSON,
                    'zaaktype.titel'    => Zaaksysteem::Backend::Object::Attribute->new(
                        name            => 'zaaktype.titel',
                        value           => 'Casetype title',
                        attribute_type  => 'text',
                    )->TO_JSON,
                },
            },
            index_hstore => Pg::hstore::encode({
                'case.id'           => Zaaksysteem::Backend::Object::Attribute->new(
                    name            => 'case.id',
                    value           => $case_id,
                    attribute_type  => 'integer',
                )->human_value,
                'zaaktype.titel'    => Zaaksysteem::Backend::Object::Attribute->new(
                    name            => 'zaaktype.titel',
                    value           => 'Casetype title',
                    attribute_type  => 'text',
                )->human_value,
            }),
        }
    );
}

$zs->zs_transaction_ok(sub {
    create_object_ok($_) for 1..10;
    my $objects = Zaaksysteem::Backend::Object->new(schema => $schema)->rs;

    is($objects->count, 10, 'Created 10 objects');

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case WHERE case.id = 1');

    my $rs = $zql->apply_to_resultset($objects);

    is($rs->count, 1, 'Applied ZQL to resultset, found 1 entry');

    my $first = $rs->first;

    is_deeply($first->csv_header, ['case.id','zaaktype.titel'], '$row->csv_header: Found all requested attributes in csv_header');
    is(@{ $first->csv_header }, 2 , '$row->csv_header: Exactly two columns in header');

    is_deeply($first->human_values, [ 1, 'Casetype title'], '$row->human_values: Found human values');

    is_deeply($first->csv_data, [ 1, 'Casetype title'], '$row->csv_data: Correct content');
    is(@{ $first->csv_data }, 2 , '$row->csv_data: Exactly two columns in data');


}, 'Test CSV functionality of ObjectData');

$zs->zs_transaction_ok(sub {
    create_object_ok($_) for 1..10;
    my $objects = Zaaksysteem::Backend::Object->new(schema => $schema)->rs;

    is($objects->count, 10, 'Created 10 objects');

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case WHERE case.id = 1');

    my $rs = $zql->apply_to_resultset($objects);

    is($rs->count, 1, 'Applied ZQL to resultset, found 1 entry');

    my $first = $rs->first;

    is_deeply($first->csv_header, ['object.uuid', 'object.id', 'object.data_created','object.date_modified'], '$row->csv_header: Found all requested attributes in csv_header');
    is(@{ $first->csv_header }, 4 , '$row->csv_header: Exactly two columns in header');

    TODO: {
        local $TODO = "Broken, make sure we can retrieve object.uuid and object.id from properties";

        is_deeply($first->csv_data, [ 1, 'Casetype title'], '$row->csv_data: Correct content');
        is(@{ $first->csv_data }, 2 , '$row->csv_data: Exactly two columns in data');
    };

}, 'Test CSV functionality of ObjectData: no predefined columns');

zs_done_testing;

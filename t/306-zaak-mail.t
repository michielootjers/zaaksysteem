#! perl
use TestSetup;
initialize_test_globals_ok;

no warnings;

$zs->zs_transaction_ok(sub {

    my $case = $zs->create_case_ok;

    throws_ok(sub {
	    $case->prepare_notification;
    }, qr/Input not a HashRef, unable to assert profile/, "Fails when called without arguments");

    throws_ok(sub {
	    $case->prepare_notification({kaas => 1});
    }, qr/Validation of profile failed/, "Fails when called without all required arguments");

    my $body = 'test áéàèçö test';
    my $result = $case->prepare_notification({
    	recipient_type => 'aanvrager',
    	body => $body,
    	subject => 'subject'
    });

    is $result->{body}, Encode::encode("utf-8", $body), 'Body has been encoded and filled in';
    is $result->{subject}, 'subject', 'Subject has been filled in';
    is $result->{recipient_type}, 'aanvrager', "Recipient type has been filled in";
    TODO: {
        local $TODO = "JW gaat dit fixen";
        is $result->{to}, 'laura@mintlab.nl', "To has been determined";
    }

}, 'Unit test for Zaak::prepare_notification');



zs_done_testing();

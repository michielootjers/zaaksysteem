#! perl

use warnings;
use strict;

use Test::Compile;
use Test::More;

my $ok = all_pm_files_ok();
if (!$ok) {
    BAIL_OUT("Compilation of modules failed");
}

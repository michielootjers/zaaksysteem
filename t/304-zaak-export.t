#! perl

### Test header start
use warnings;
use strict;
use Data::Dumper;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Test::Warn;

### Test header end


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    my $files = $file->case_id->attached_files;
    ok keys %$files == 1, "One attached file";

    my ($filename) = keys %$files;
    ok $filename eq 'FilestoreTest.txt', "Correct name including extension given";


    # TODO: {
    # since export yield warnings, and the test-department feels more
    # investigation is necessary, i'll comment this out for the next
    # person who will refine this.
    #     # put it in the trash can
    #     my $result = $file->update_properties({
    #         subject  => $zs->get_subject_ok,
    #         destroyed => 1,
    #         user_has_zaak_beheer_permissions => 1
    #     });

    #     throws_ok(sub {
    #         my $export = $file->case_id->export;
    #     }, qr/called with invalid input data structure/,
    #         "Fails when called without params");

    #     throws_ok(sub {
    #         my $export = $file->case_id->export({kaas => 'worst'});
    #     }, qr/Parameters incorrect/,
    #         "Fails when called with wrong params");

    #     my $tmp_path = $schema->resultset('Config')->get_value('tmp_location');

    #     lives_ok sub {
    #         my $export = $file->case_id->export({
    #             filepath => $tmp_path,
    #             disable_prefetch => 1
    #         });
    #     }, "no die when passing a proper path";

    #     $files = $file->case_id->_attached_files;
    #     ok keys %$files == 0, "No attached files because the files we attached were destroyed";
    # };

}, 'Tested zaak->export exclude deleted documents');

zs_done_testing();
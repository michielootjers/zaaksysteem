#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok(registratiedatum => DateTime->now()->subtract(days => 1));

    my $def     = $case->zaaktype_node_id->zaaktype_definitie_id;
    $def->servicenorm(5);
    $def->servicenorm_type('kalenderdagen');
    $def->afhandeltermijn(10);
    $def->afhandeltermijn_type('kalenderdagen');
    $def->update;

    my $cases   = $schema->resultset('Zaak')->search({'me.id' => $case->id});
    $case       = $cases->first;

    is($case->days_left, 3, 'Got correct days left');
    ok($case->days_perc > 15, 'Got correct days percentage');
}, 'Case: correct time on bootstrap. Set on 5 days.');


zs_done_testing();

#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use_ok('Zaaksysteem::General::Actions');

$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok();

    my $c       = Test::MockObject->new();
    $c->mock('stash' => sub { return { zaak => $case } });

    ok(Zaaksysteem::General::Actions::can_upload($c), '$c->can_upload allowed to ' . $case->status . ' case: ' . $case->id);

    ### Forcefully resolve case:
    for (qw/new stalled open/) {
        $case->status($_);

        ok(Zaaksysteem::General::Actions::can_upload($c), '$c->can_upload allowed to ' . $_ . ' case: ' . $case->id);
    }

    ### Forcefully resolve case:
    for (qw/resolved deleted overdragen/) {
        $case->status($_);

        ok(!Zaaksysteem::General::Actions::can_upload($c), '$c->can_upload NOT allowed to ' . $_ . ' case: ' . $case->id);
    }
}, '$c->can_upload Check if uploading is allowed for case');


zs_done_testing();

#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Exception');

eval {
    throw('error/test','This is a test message');
};

ok(UNIVERSAL::isa($@, 'Zaaksysteem::Exception::Base'), 'Got proper exception');

my $json = $@->TO_JSON;

ok(exists $json->{result}->[0]->{ $_ }, 'Found proper JSON attr: ' . $_) for qw/
    messages
    type
    stacktrace
    data
/;

zs_done_testing();

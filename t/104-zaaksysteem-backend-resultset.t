#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

$zs->zs_transaction_ok(sub {
    my $rs_active   = $schema
                    ->resultset('Zaaktype')
                    ->search_active;

    ok(
        $rs_active->count,
        'Found active casetypes'
    );

    while (my $active = $rs_active->next) {
        $active->active(0);
        $active->update;
    }

    $rs_active      = $schema
                    ->resultset('Zaaktype')
                    ->search_active;

    ok(
        ! $rs_active->count,
        'Correctly removed active casetypes'
    );


}, 'Tested search_active');

$zs->zs_transaction_ok(sub {

    my $resultset   = $schema
                    ->resultset('Zaaktype');

    ok(
        $resultset->search_freeform('test')->count,
        'Found casetypes matching test'
    );

    ok(
        ! $resultset->search_freeform('bla$#Fn2983at')->count,
        'Did not find casetypes matching bla$#Fn2983at'
    );

    ok(
        $resultset->search_freeform('TeSt')->count,
        'Correct case insensitive search for TeSt'
    );

    ok(
        $resultset->search_freeform('     test      ')->count,
        'Correct search with too much spaces'
    );
}, 'Tested search_freeform');

zs_done_testing();

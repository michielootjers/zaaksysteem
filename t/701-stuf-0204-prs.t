#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
initialize_test_globals_ok;
use Test::Deep;

use File::Spec::Functions qw(catfile);


BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };
BEGIN { use_ok('Zaaksysteem::StUF::Body::Field') };


my $VALIDATION_MAP      = {
    'PRS'   => {
        'a-nummer'                  => '1234567890',
        'bsn-nummer'                => '987654321',
        'voornamen'                 => 'Tinus',
        'voorletters'               => 'T',
        'geslachtsnaam'             => 'Testpersoon',
        'geboortedatum'             => '19620529',
        'geslachtsaanduiding'       => 'M',
    },
    'ADR'   => {
        'postcode'                  => '1015JL',
        'woonplaatsnaam'            => 'Amsterdam',
        'straatnaam'                => 'Donker Curtiusstraat',
        'huisnummer'                => '7',
        'huisnummertoevoeging'      => '521',
    },
    'PRSHUW'   => {
        'a-nummer'                  => '5654321023',
        'bsn-nummer'                => '568316589',
        'geslachtsnaam'             => 'TestpartnernaamGOOD',
    },
    'PRS_Moved'   => {
        'a-nummer'                  => '1234567890',
        'bsn-nummer'                => '987654321',
        'voornamen'                 => 'Minus',
        'voorletters'               => 'M',
        'geslachtsnaam'             => 'Mestpersoon',
        'geslachtsaanduiding'       => 'M',
    },

};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml')
    );

    is($stuf->entiteittype, 'PRS', 'Found entiteittype PRS');

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{PRS} }) {
        my $givenvalue = $params->{PRS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{PRS}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    #note(explain($params));
    #note(explain($stuf->parser->xml));
    #note(explain($stuf->parser->data));


}, 'Checked PRS native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml')
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{ADR} }) {
        my ($active_rel)    = grep { $_->{is_active} } @{ $params->{PRS}->{PRSADRVBL} };
        my $givenvalue      = $active_rel->{ADR}->{ $key };
        my $wantedvalue     = $VALIDATION_MAP->{ADR}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Checked PRS related ADR params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/102-prs-create-huwelijk.xml')
    );

    my $params      = $stuf->as_params;
    my $partner     = $stuf->get_active_partner;

    note(explain($params));


    #note(explain($params));
    #note(explain($stuf->parser->data));
    is($partner->{geslachtsnaam}, $VALIDATION_MAP->{PRSHUW}->{geslachtsnaam}, 'Correcte partner');

    is(
        $params->{PRS}->{aanduidingNaamgebruik}, 'P', 'Naamgebruik Partner'
    );
}, 'Checked PRS for active Huwelijk');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/102-prs-create-huwelijk.xml')
    );

    my $params  = $stuf->get_params_for_natuurlijk_persoon;
    ok($params->{ $_ }, 'Found filled key: ' . $_) for qw/
        a_nummer
        burgerservicenummer
        geboortedatum
        geslachtsaanduiding
        geslachtsnaam
        partner_a_nummer
        partner_burgerservicenummer
        partner_geslachtsnaam
        partner_voorvoegsel
        voorletters
        voornamen
    /;
    ok ($params, 'Checked: $stuf->get_params_for_natuurlijk_persoon');

    $params  = $stuf->get_params_for_natuurlijk_persoon_adres;
    ok($params->{ $_ }, 'Found filled key: ' . $_) for qw/
        functie_adres
        huisnummer
        postcode
        straatnaam
        woonplaats
    /;
    ok ($params, 'Checked: $stuf->get_params_for_natuurlijk_persoon_adres');

}, 'Checked PRS helper functions');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/121-prs-update-tinus.xml')
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{PRS_Moved} }) {
        my $givenvalue = $params->{PRS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{PRS_Moved}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    ok(
        !exists($params->{PRS}->{geboortedatum}),
        'Geboortedatum ignored'
    );

    subtest 'huisnummer_toevoeging' => sub { 
        ok(exists $params->{PRS}->{PRSADRCOR}->[0]->{ADR}->{huisnummertoevoeging}, "exists");
        is($params->{PRS}->{PRSADRCOR}->[0]->{ADR}->{huisnummertoevoeging}, undef, "not defined");
    };

}, 'Checked PRS Wijziging');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->new(
        entiteittype    => 'PRS',
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => 'CGM',
            },
        ),
    )->set_afnemerindicatie(
        {
            reference_id    => 29237824,
            date            => DateTime->now(),
            sleutelGegevensbeheer => 33,
        }
    );

    my $xml     = $stuf->to_xml;

    my $stuf2   = Zaaksysteem::StUF->from_xml($xml);

    #note(explain($stuf2->to_xml));

}, 'Set afnemerindicatie');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->new(
        entiteittype    => 'PRS',
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => 'CGM',
            },
        ),
    )->set_afnemerindicatie(
        {
            reference_id    => 29237824,
            date            => DateTime->now(),
            sleutelGegevensbeheer => 33,
            afnemerindicatie => 0,
        }
    );

    #note(explain($stuf->to_xml));

}, 'Set afnemerindicatie');


$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/151-prs-vicrea-geboorte.xml')
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{PRS} }) {
        my $givenvalue = $params->{PRS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{PRS}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{ADR} }) {
        my ($active_rel)    = grep { $_->{is_active} } @{ $params->{PRS}->{PRSADRVBL} };
        my $givenvalue      = $active_rel->{ADR}->{ $key };
        my $wantedvalue     = $VALIDATION_MAP->{ADR}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Vicrea: Geboorte');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/152-prs-vicrea-huwelijk.xml')
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{PRS} }) {
        my $givenvalue = $params->{PRS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{PRS}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{ADR} }) {
        my ($active_rel)    = grep { $_->{is_active} } @{ $params->{PRS}->{PRSADRVBL} };
        my $givenvalue      = $active_rel->{ADR}->{ $key };
        my $wantedvalue     = $VALIDATION_MAP->{ADR}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Vicrea: Huwelijk');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'prs/154-prs-vicrea-overlijden.xml')
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{PRS} }) {
        my $givenvalue = $params->{PRS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{PRS}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{ADR} }) {
        my ($active_rel)    = grep { $_->{is_active} } @{ $params->{PRS}->{PRSADRVBL} };
        my $givenvalue      = $active_rel->{ADR}->{ $key };
        my $wantedvalue     = $VALIDATION_MAP->{ADR}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    is($params->{PRS}->{datumOverlijden}, '20140201', 'datumOverlijden set');

}, 'Vicrea: Overlijden');

zs_done_testing;

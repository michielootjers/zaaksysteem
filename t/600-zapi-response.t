#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use Zaaksysteem::Log::CallingLogger;

BEGIN { use_ok('Zaaksysteem::ZAPI::Response'); }

$zs->zs_transaction_ok(sub {

    my $resultset   = $schema
                    ->resultset('Logging')
                    ->search({});

    throws_ok(
        sub {
            Zaaksysteem::ZAPI::Response->new(
                resultset  => DateTime->now(),
                uri_prefix  => URI->new('http://localhost/index'),
            );
        },
        qr/Not a valid ResultSet/,
        'Invalid ResultSet'
    );

    SKIP: {
        skip 'Broken', 1;
        throws_ok(
            sub {
                Zaaksysteem::ZAPI::Response->new(
                    resultset  => $resultset,
                    uri_prefix  => URI->new('http://localhost/index'),
                );
            },
            qr/Can't create pager for non-paged/,
            'No pager'
        );
    }

    $resultset   = $schema
                    ->resultset('Logging')
                    ->search({}, { rows => 25, page => 1 });

    my $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
        resultset   => $resultset,
        uri_prefix  => URI->new('http://localhost/index'),
    );

    is(
        $zapi_resp->rows,
        ($resultset->count < 25 ? $resultset->count : 25),
        'Correct number of rows: ' . $zapi_resp->rows
    );

    is(
        $zapi_resp->num_rows,
        $resultset->pager->total_entries,
        'Correct number of total rows: ' . $zapi_resp->num_rows
    );

    is(
        scalar(@{ $zapi_resp->result }),
        $zapi_resp->rows,
        'Correct number of total entries in result: ' . $zapi_resp->rows
    );

    $zapi_resp->rows(undef);
    throws_ok(
        sub {
            $zapi_resp->_validate_response()
        },
        qr/Missing required attributes/,
        'Check missing attribute error'
    );

    $resultset   = $schema
                    ->resultset('Logging')
                    ->search({}, { rows => 25, page => 1 });

    $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
        unknown  => $resultset,
        uri_prefix  => URI->new('http://localhost/index'),
    );

    $zapi_resp->result({});
    SKIP: {
        skip 'Broken', 1;
        throws_ok(
            sub {
                $zapi_resp->_validate_response()
            },
            qr/Invalid format for result/,
            'Check invalid result error'
        );

        is($zapi_resp->_input_type, 'resultset', 'Found resultset from unknown input source');
    }
}, 'Request a ResultSet');


$zs->zs_transaction_ok(sub {
    my @array       = ();

    my $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
        unknown     => \@array,
        uri_prefix  => URI->new('http://localhost/index'),
        no_pager    => 1,
    );

    is(
        $zapi_resp->rows,
        0,
        'Correct number of rows: ' . $zapi_resp->rows
    );

    is(
        $zapi_resp->num_rows,
        scalar(@array),
        'Correct number of total rows: ' . $zapi_resp->num_rows
    );

    is(
        scalar(@{ $zapi_resp->result }),
        $zapi_resp->rows,
        'Correct number of total entries in result: ' . $zapi_resp->rows
    );
}, 'Request an ARRAY');

$zs->zs_transaction_ok(sub {
    my @array       = (1..75);

    my $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
        unknown     => \@array,
        uri_prefix  => URI->new('http://localhost/index'),
    );

    is(
        $zapi_resp->rows,
        25,
        'Correct number of rows: ' . $zapi_resp->rows
    );

    is(
        $zapi_resp->num_rows,
        scalar(@array),
        'Correct number of total rows: ' . $zapi_resp->num_rows
    );

    is(
        scalar(@{ $zapi_resp->result }),
        $zapi_resp->rows,
        'Correct number of total entries in result: ' . $zapi_resp->rows
    );

    ### Move up page
    $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
        unknown         => \@array,
        page_current    => 2,
        uri_prefix  => URI->new('http://localhost/index'),
    );

    is($zapi_resp->result->[0], 26, 'Correct paging');
}, 'Request an ARRAY');


$zs->zs_transaction_ok(sub {
    my @array       = (1..75);

    my $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
        unknown     => \@array,
        page_current => 2,
        uri_prefix  =>
        URI->new('http://localhost/index?kenmerk_id_1=25&kenmerk_id_2=33'),
    );

    TODO: {
        local $TODO = 'Broken, need to be looked at';
        like(
            $zapi_resp->prev,
            qr/prev=1/,
        );

        like(
            $zapi_resp->next,
            qr/next=3/,
        );


        $zapi_resp   = Zaaksysteem::ZAPI::Response->new(
            unknown     => \@array,
            page_current => 2,
            uri_prefix  =>
            URI->new('http://localhost/index?kenmerk_id_1=25&kenmerk_id_2=33&next=5'),
        );

        like(
            $zapi_resp->prev,
            qr/prev=1/,
        );
        note(explain($zapi_resp->response));
    };

}, 'URI Handling');


zs_done_testing();

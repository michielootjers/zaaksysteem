#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Backend::Sysin::Modules::AuthLDAP');


sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'authldap',
            name                => 'LDAP Authentication',
            interface_config    => {
                %{ $params }
            }
        },                                
    );  
}

zs_done_testing();

#! perl

### Test header start
use TestSetup;
initialize_test_globals_ok();
### Test header end


$zs->zs_transaction_ok(sub {

    my $case = $zs->create_case_ok;
    my $new_case = $zs->create_case_ok();

    my $rs_checklist = $schema->resultset('Checklist')->search({ case_milestone => 2 });
    my $rs_item = $schema->resultset('ChecklistItem')->search;

    is($rs_checklist->search({case_id => $case->id})->count, 1, "Checklist present in the test case");
    my $old_checklist = $rs_checklist->first;

    my $max = 3;

    for (1 .. $max - 1) {
        $zs->create_case_checklist_item_ok(
            state        => 1,
            sequence     => $_,
            user_defined => 0,
            checklist    => $old_checklist,
        );
    }

    my $count = $rs_item->search({checklist_id => $old_checklist->id })->count;
    is($count, $max, "All checklist items found");

    throws_ok(sub {
        $case->duplicate_checklist_items({bla => 1});
    }, qr/Validation of profile failed/, "Should bail out when called without proper params");

    $case->duplicate_checklist_items({ target => $new_case });

    is($rs_checklist->search({case_id => $new_case->id})->count, 1, "Checklist present in the new case");
    my $new_checklist = $rs_checklist->first;
    $count = $rs_item->search({checklist_id => $new_checklist->id })->count;
    is($count, $max, "All checklist items found");

}, 'Check duplicate_checklist_items');



zs_done_testing();
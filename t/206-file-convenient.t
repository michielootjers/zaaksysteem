#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


$zs->zs_transaction_ok(sub {
    my $case_document = $zs->get_case_document_ok;
    $schema->resultset('File')->file_create({
        db_params => {
            created_by        => $zs->get_subject_ok,
        },
        case_document_ids => [$case_document->id],
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'case_type_document.txt',
    });



    my $result  = $schema
                ->resultset('File')
                ->search_by_case_document_id(
                    $case_document->id
                )->first;

    my ($result_case_document) = $result->case_documents;

    is ($result_case_document->case_document->id, $case_document->id,
        'case_document_id matches get_case_document_ok_id'
    );
}, 'search first find file by case_type_document_id');


$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by              => $zs->get_subject_ok,
        },
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'new_filename.txt',
    });

    is $result->filename, 'new_filename.txt', 'filename retrieved';
    is $result->name, 'new_filename', 'filename without extension';
    is $result->extension, '.txt', 'filename extension';
}, 'Return filename from file');


zs_done_testing();

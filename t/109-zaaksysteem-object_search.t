#!perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Model::Attributes');


my $attributes = Zaaksysteem::Model::Attributes->new(schema => $schema);

$zs->zs_transaction_ok(sub {
    my @attrs = $attributes->search('uname');

    ok scalar(@attrs), 'Search for system attribute returns at least on result';

    my $attr = (grep { $_->{ object }{ source } eq 'sys-attr' } @attrs)[0];

    is_deeply $attr, {
        id => 'system.uname',
        label => 'Uname',
        object_type => 'attribute',
        object => {
            column_name => 'system.uname',
            source => 'sys-attr',
            value_type => 'text'
        }
    }, 'Formatting of returned system attribute is correct';
}, 'attr uname hydrated properly by model');

$zs->zs_transaction_ok(sub {
    my $bibkenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'somecomplexname',
        magic_string => 'testes123',
        values => [
            { value => 'Optie A', active => 1 },
            { value => 'Optie B', active => 0 }
        ],
    );

    is $bibkenmerk->bibliotheek_kenmerken_values->count, 2, 'attr/values create_bibliotheek_kenmerk creates values';

    my $attributes = Zaaksysteem::Model::Attributes->new(schema => $schema);
    my ($attr) = $attributes->search('somecomplexname');

    is ref $attr, 'HASH', 'hydration returns a hashref';
    is ref $attr->{ object }, 'HASH', 'hydration returns hashref object';
    is ref $attr->{ object }{ values }, 'ARRAY', 'hydration returns arrayref for attr values';
    is     $attr->{ object }{ column_name }, "attribute.testes123";

    my @active_opts = grep { $_->{ active } == 1 } @{ $attr->{ object }{ values } };
    my @inactive_opts = grep { $_->{ active } == 0 } @{ $attr->{ object }{ values } };

    is scalar(@active_opts), 1, 'exactly one active option';
    is scalar(@inactive_opts), 1, 'exactly one inactive option';

    is $active_opts[0]->{ value }, 'Optie A', 'value correctly injected';
    is $inactive_opts[0]->{ value }, 'Optie B', 'value correctly injected';
}, 'attr/values creating bibliotheek_kenmerken with values hydrate as expected');

# ZS-2145
$zs->zs_transaction_ok(sub {
    my $bib = $zs->create_bibliotheek_kenmerk_ok(naam => 'testytesty');

    my $attributes = Zaaksysteem::Model::Attributes->new(schema => $schema);

    my @attrs = $attributes->search('testytesty');
    my $count = scalar(@attrs);

    ok $count > 0, 'found test attribute';

    $bib->delete;

    @attrs = $attributes->search('testytesty');
    
    is scalar(@attrs), $count - 1, 'did not find attribute after deletion';
}, 'attr deleted attributes are not hydrated in objectsearch');

sub create_test_case {
    my $zaaktype_node = $zs->create_zaaktype_node_ok;
    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    my $valuta_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'valuta1',
        magic_string => 'magic_string_valuta',
        value_type => 'valuta',
    );
    my $bag_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'bag',
        magic_string => 'magic_string_bag',
        value_type => 'bag_openbareruimte',
    );

    my $valuta_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $valuta_kenmerk,
    );
    my $bag_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $bag_kenmerk,
    );

    my $case = $zs->create_case_ok(zaaktype => $casetype);

    return ($case, { bag => $bag_kenmerk->id, valuta => $valuta_kenmerk->id });
}

$zs->zs_transaction_ok(sub {
    my ($case, $kenmerken) = create_test_case();

    my $bag_openbareruimte = $zs->create_bag_openbareruimte_ok();

    $case->zaak_kenmerken->replace_kenmerk({
        bibliotheek_kenmerken_id    => $kenmerken->{bag},
        zaak_id                     => $case->id,
        values                      => [$bag_openbareruimte->identificatie]
    });
    $case->zaak_kenmerken->replace_kenmerk({
        bibliotheek_kenmerken_id    => $kenmerken->{valuta},
        zaak_id                     => $case->id,
        values                      => ["3.14"]
    });

    my $od = $schema->resultset("ObjectData")->find_or_create_by_object_id(case => $case->id);

    is_deeply(
        $od->get_object_attribute('attribute.magic_string_valuta')->value,
        '3.14',
        "Valuta attribute value was stored correctly"
    );

    is_deeply(
        $od->get_object_attribute('attribute.magic_string_bag')->value,
        {
            bag_id           => $bag_openbareruimte->identificatie,
            human_identifier => undef,
            address_data     => undef,
        },
        "BAG attribute was stored correctly"
    );

    $case->zaak_kenmerken->replace_kenmerk({
        bibliotheek_kenmerken_id    => $kenmerken->{valuta},
        zaak_id                     => $case->id,
        values                      => ["6,28"] # note: ',' not '.'
    });

    $od->discard_changes();
    is_deeply(
        $od->get_object_attribute('attribute.magic_string_valuta')->value,
        '6.28',
        "Valuta attribute value was stored correctly even with , separator"
    );
}, 'Casetype attribute type mapping');

zs_done_testing;

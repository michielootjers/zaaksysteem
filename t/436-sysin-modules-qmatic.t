#! perl
use TestSetup;
initialize_test_globals_ok;

use DateTime;
use Zaaksysteem::Backend::Sysin::Modules::QMatic;

my $now = "1970-01-01T16:45:00.000Z";
my $module = Zaaksysteem::Backend::Sysin::Modules::QMatic->new;

sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'qmatic',
            name                => 'My QMatic interface',
            interface_config    => {
                interface_endpoint => 'http://www.google.com/',
                productLinkID => '1',
                %{ $params }
            }
        }
    );
}

{
    no warnings 'redefine';
    *Zaaksysteem::Backend::Sysin::Modules::QMatic::compile_call = sub {
        my $self = shift;
        my ($interface, $action) = @_;

        my $response = Test::MockObject->new;
        $response->set_always('content', "response_content");

        my $trace = Test::MockObject->new;
        $trace->set_always('request', $response); # for now it will do the same
        $trace->set_always('response', $response);

        # Emulate the structure of QMatic/SOAP XML
        return sub {
            my $answer = {
                parameters => {
                    $action . 'Return' => $module->spoof($action)
                }
            };
            return ($answer, $trace);
        };
    };

    *Zaaksysteem::Backend::Sysin::Modules::QMatic::retrieve_aanvrager = sub {
        my $self = shift;

        my $mock = Test::MockObject->new();

        $mock->set_always('naam',                'Naam');
        $mock->set_always('street_address',      'Adres');
        $mock->set_always('city',                'Woonplaats');
        $mock->set_always('postal_code',         '1234XX');
        $mock->set_always('telefoonnummer',      'Telefoon');
        $mock->set_always('mobiel',              'mobielnr');
        $mock->set_always('email',               'emailadres');
        $mock->set_always('btype',               'natuurlijk_persoon');
        $mock->set_always('burgerservicenummer', 'bsn');
        $mock->set_always('geslachtsnaam',       'geslacht');

        return $mock;
    };
}

$zs->zs_transaction_ok(sub {
    my $interface = create_config_interface();

    my $result = $interface->process_trigger('getAvailableProducts', {});

    is_deeply $result,
        $module->spoof('getAvailableProducts'),
        'SOAP call to getAvailableProducts';


    $result = $interface->process_trigger('getAvailableAppointmentDays', {
        productLinkID => 'foobar'
    });

    is_deeply(
        $result,
        $module->spoof('getAvailableAppointmentDays'),
        'getAvailableAppointmentDays works as expected',
    );


    $result = $interface->process_trigger('getAvailableAppointmentTimes', {
        productLinkID => 42,
        appDate       => $now,
    });

    is_deeply(
        $result,
        $module->spoof('getAvailableAppointmentTimes'),
        'getAvailableAppointmentTimes works as expected'
    );

    $result = $interface->process_trigger('deleteAppointment', {
        appointmentId => 1337
    });

    is_deeply(
        $result,
        [$module->spoof('deleteAppointment')],
        "deleteAppointment works as expected"
    );

    $result = $interface->process_trigger('bookAppointment', {
        aanvrager     => 1337,
        productLinkID => 42,
        appDate       => $now,
        appTime       => '12:34',
    });

    is_deeply(
        $result,
        [$module->spoof('bookAppointment')],
        "bookAppointment works as expected"
    );

    is_deeply $module->addTimezones([]), [], "addTimezones works on empty list";

    my $dates = [
        '2013-12-25T12:00:00.000Z',
        '2013-12-26T12:00:00.000Z',
        '2014-04-26T12:00:00.000Z'
    ];

    my $with_timezones = [
        '2013-12-25T12:00:00.000+0100',
        '2013-12-26T12:00:00.000+0100',
        '2014-04-26T12:00:00.000+0100'
    ];

    $result = [$module->addTimezones(@$dates)];

    is_deeply $result,
        $with_timezones,
        "addTimezones adds timezones as advertised";

    $result = [map { $module->removeTimezone($_) } @$with_timezones];
    is_deeply $result,
        $dates,
        "removeTimezone removes timezone as advertised";

}, 'Tested: invokeQMatic');




zs_done_testing();

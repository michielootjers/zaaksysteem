#! perl
use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;

use JSON;
use Test::Deep;
use File::Spec::Functions;


BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    'ADR'   => {
      'aanduidingGegevensInOnderzoek' => 'N',
      'authentiekeIdentificatieVerblijfsobject' => '0620020000025912',
      'datumBegin' => '18000101',
      'eindDatum' => undef,
      'indicatieGeconstateerd' => 'N',
      'statusCode' => '07'
    }
};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', '/vbo/101-vbo-create.xml'),
    );

    is($stuf->entiteittype, 'VBO', 'Found entiteittype VBO');

    my $params  = $stuf->as_params;
    #note(explain($params));

    for my $key (keys %{ $VALIDATION_MAP->{VBO} }) {
        my $givenvalue = $params->{VBO}->{'extraElementen'}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{VBO}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }
    my $vbo_params = $stuf->get_params_for_vbo;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats');

    my $rv = Data::FormValidator->check(
        $vbo_params,
        Params::Profile->get_profile('method' => 'Zaaksysteem::Backend::BagWoonplaats::ResultSet::bag_create_or_update')
    );
    #note(explain($vbo_params));
    is($vbo_params->{verblijfsobject_gebruiksdoel}->[0], 'woonfunctie', 'Found codegebruiksdoel');

    ok($rv->success, 'Valid data for BagWoonplaats profile');

}, 'Check VBO native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', '/vbo/101-vbo-create.xml'),
    );

    my $vbo_params = $stuf->get_params_for_vbo;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats')->bag_create_or_update(
        $vbo_params,
    );

}, 'Import VBO in database');


zs_done_testing;

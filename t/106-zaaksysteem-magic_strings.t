#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Attributes qw[ZAAKSYSTEEM_SYSTEM_ATTRIBUTES];

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;

    isa_ok(
        $case->list_magic_strings,
        'ARRAY',
        'Got list of magic strings for case'
    );

    ok((@{ $case->list_magic_strings } > 5), 'Found more than 5 magic strings');
}, 'Verified listing of magic strings');

$zs->zs_transaction_ok(sub {
    my %attributes_seen;
    for my $attr (@{ ZAAKSYSTEEM_SYSTEM_ATTRIBUTES()->() }) {
        $attributes_seen{$attr->name} //= 0;
        $attributes_seen{$attr->name} += 1;
    }

    my @duplicate_keys = grep { $attributes_seen{$_} != 1 } keys %attributes_seen;
    if (! is(@duplicate_keys, 0, "No duplicate attributes found") ) {
        diag("Duplicate attributes: ", join(", ", @duplicate_keys));
    }

}, 'Attribute listing');

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    $case->update({onderwerp => 'Wat zal ik hier eens neerzetten?'});

    my $case_obj = $schema->resultset('ObjectData')->find_or_create_by_object_id(
        'case', $case->id
    );

    is($case_obj->get_source_object->id, $case->id, "Source object retrieved correctly.");

    isa_ok(
        $case_obj->object_attributes,
        'ARRAY',
        'Got filled list of magic strings for case'
    );

    for my $attr (@{ $case_obj->object_attributes }) {
        isa_ok(
            $attr,
            'Zaaksysteem::Backend::Object::Attribute',
            sprintf("object_attribute %s", $attr->name),
        );
    }

    ok(
        (scalar(grep { $_->value } @{ $case_obj->object_attributes }) > 5),
        'At least 5 magic string values found'
    );

    ### Check human attributes
    my $reg_datum = $case_obj->get_object_attribute('case.date_of_registration');
    is(
        $reg_datum->human_value,
        $reg_datum->value->dmy,
        "Human-readable date is formatted as d-m-y",
    );

    my $unaccepted_updates = $case_obj->get_object_attribute('case.num_unaccepted_updates');
    is($unaccepted_updates->value, 0, "No unaccepted updates on empty case");

    my $unaccepted_files = $case_obj->get_object_attribute('case.num_unaccepted_files');
    is($unaccepted_files->value, 0, "No unaccepted files on empty case");

    TODO: {
        local $TODO = "This is a bug: ZS-2493";
        my $subject = $case_obj->get_object_attribute('case.subject');
        is(
            $subject->human_value,
            $case->onderwerp,
            "Subject is correct",
        );
    }

    for my $attribute (qw(
        case.phase
        casetype.version
        case.number_status
        case.price
        case.number_parent
        case.number_master
        case.lead_time_legal
        case.lead_time_service
        casetype.id
        assignee.id
        coordinator.id
    )) {
        my $a = $case_obj->get_object_attribute($attribute);

        is(
            $a->attribute_type,
            'integer',
            "$attribute is an integer",
        );
    }
}, 'Verified listing of magic string content');


zs_done_testing();

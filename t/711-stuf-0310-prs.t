#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
initialize_test_globals_ok;
use Test::Deep;

use File::Spec::Functions qw(catfile);


BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };
BEGIN { use_ok('Zaaksysteem::StUF::Body::Field') };


my $VALIDATION_MAP      = {
    'NPS'   => {
        'inp.a-nummer'              => '1234567890',
        'inp.bsn'                   => '987654321',
        'voornamen'                 => 'Tinus',
        'voorletters'               => 'T',
        'geslachtsnaam'             => 'Testpersoon',
        'geboortedatum'             => '19620529',
        'geslachtsaanduiding'       => 'M',
    },
    'verblijfsadres'   => {
        'aoa.postcode'              => '1015JL',
        'wpl.woonplaatsNaam'        => 'Amsterdam',
        'gor.straatnaam'            => 'Donker Curtiusstraat',
        'aoa.huisnummer'            => '7',
        'aoa.huisnummertoevoeging'  => '521',
    },
    'PRSHUW'   => {
        'a-nummer'                  => '5654321023',
        'bsn-nummer'                => '568316589',
        'geslachtsnaam'             => 'TestpartnernaamGOOD',
    },
    'NPS_Moved'   => {
        'inp.a-nummer'              => '1234567890',
        'inp.bsn'                   => '987654321',
        'voornamen'                 => 'Minus',
        'voorletters'               => 'M',
        'geslachtsnaam'             => 'Mestpersoon',
        'geslachtsaanduiding'       => 'M',
    },

};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', 'prs/101-prs-create-tinus.xml'),
        '0310'
    );

    # note(explain($stuf->parser->data));
    # note(explain($stuf->as_params));

    # note(explain($stuf->stuurgegevens));
    # note(explain($stuf->body));

    is($stuf->entiteittype, 'NPS', 'Found entiteittype NPS');

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{NPS} }) {
        my $givenvalue = $params->{NPS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{NPS}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    #note(explain($params));
    #note(explain($stuf->parser->xml));
    #note(explain($stuf->parser->data));


}, 'Checked PRS native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', 'prs/101-prs-create-tinus.xml'),
        '0310'
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{verblijfsadres} }) {
        my ($active_rel)    = grep { $_->{is_active} } @{ $params->{NPS}->{verblijfsadres} };
        my $givenvalue      = $active_rel->{verblijfsadres}->{ $key };
        my $wantedvalue     = $VALIDATION_MAP->{verblijfsadres}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Checked PRS related ADR params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', 'prs/101-prs-create-tinus.xml'),
        '0310'
    );


    my $params      = $stuf->as_params;
    my $partner     = $stuf->get_active_partner;

    #note(explain($params));
    #note(explain($stuf->parser->data));
    is($partner->{geslachtsnaam}, $VALIDATION_MAP->{PRSHUW}->{geslachtsnaam}, 'Correcte partner');

    is(
        $params->{NPS}->{aanduidingNaamgebruik}, 'P', 'Naamgebruik Partner'
    );
}, 'Checked PRS for active Huwelijk');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', 'prs/101-prs-create-tinus.xml'),
        '0310'
    );

    #note(explain($stuf->as_params));

    my $params  = $stuf->get_params_for_natuurlijk_persoon;
    ok($params->{ $_ }, 'Found filled key: ' . $_) for qw/
        a_nummer
        burgerservicenummer
        geboortedatum
        geslachtsaanduiding
        geslachtsnaam
        partner_a_nummer
        partner_burgerservicenummer
        partner_geslachtsnaam
        partner_voorvoegsel
        voorletters
        voornamen
    /;
    ok ($params, 'Checked: $stuf->get_params_for_natuurlijk_persoon');

    $params  = $stuf->get_params_for_natuurlijk_persoon_adres;
    ok($params->{ $_ }, 'Found filled key: ' . $_) for qw/
        functie_adres
        huisnummer
        postcode
        straatnaam
        woonplaats
    /;
    ok ($params, 'Checked: $stuf->get_params_for_natuurlijk_persoon_adres');

}, 'Checked PRS helper functions');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', 'prs/121-prs-update-huwelijk.xml'),
        '0310'
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{NPS_Moved} }) {
        my $givenvalue = $params->{NPS}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{NPS_Moved}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    ok(
        !exists($params->{NPS}->{geboortedatum}),
        'Geboortedatum ignored'
    );

    # subtest 'huisnummer_toevoeging' => sub { 
    #     ok(exists $params->{PRS}->{PRSADRCOR}->[0]->{ADR}->{huisnummertoevoeging}, "exists");
    #     is($params->{PRS}->{PRSADRCOR}->[0]->{ADR}->{huisnummertoevoeging}, undef, "not defined");
    # };

}, 'Checked PRS Wijziging');

# $zs->zs_transaction_ok(sub {
#     my $stuf    = Zaaksysteem::StUF->new(
#         entiteittype    => 'PRS',
#         stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
#             zender              => {
#                 applicatie          => 'ZSNL',
#             },
#             ontvanger           => {
#                 applicatie          => 'CGM',
#             },
#         ),
#     )->set_afnemerindicatie(
#         {
#             reference_id    => 29237824,
#             date            => DateTime->now(),
#             sleutelGegevensbeheer => 33,
#         }
#     );

#     my $xml     = $stuf->to_xml;

#     my $stuf2   = Zaaksysteem::StUF->from_xml($xml);

#     #note(explain($stuf2->to_xml));

# }, 'Set afnemerindicatie');

# $zs->zs_transaction_ok(sub {
#     my $stuf    = Zaaksysteem::StUF->new(
#         entiteittype    => 'PRS',
#         stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
#             zender              => {
#                 applicatie          => 'ZSNL',
#             },
#             ontvanger           => {
#                 applicatie          => 'CGM',
#             },
#         ),
#     )->set_afnemerindicatie(
#         {
#             reference_id    => 29237824,
#             date            => DateTime->now(),
#             sleutelGegevensbeheer => 33,
#             afnemerindicatie => 0,
#         }
#     );

#     #note(explain($stuf->to_xml));

# }, 'Set afnemerindicatie');


zs_done_testing;

#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;
### Test header end

$zs->zs_transaction_ok(sub {
    my $i = $zs->create_interface_ok;

    ok $i->interface_update({
        name        => 'Nieuwe naam',
        max_retries => 20,
        active      => 1,
    }), 'Updated interface';

    is $i->name, 'Nieuwe naam', 'Changed name';
    is $i->max_retries, 20, 'Changed maximum retries';
    ok $i->active, 'Interface is active';

}, 'interface_update');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $zs->create_interface_ok->interface_update({
            max_retries => 'BA NA NA',
        });
    }, qr/invalid: max_retries/, 'Ran update with invalid params'; 
}, 'interface_update invalid params');

$zs->zs_transaction_ok(sub {
	ok $zs->create_interface_ok->interface_update({}), 'Ran update without params';	
}, 'interface_update no params');

zs_done_testing;

#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;
    $case->update({milestone => 2});
    my $subject = $zs->get_subject_ok;

    my $file = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Unieke Filename.jpg',
        db_params => {
            created_by => $subject,
            case_id    => $case->id,
            accepted   => 1,
        },
    });
    ok $file, 'Created file';
    ok $file->accepted, 'File is accepted';

    my $second_file = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Unieke Filename.jpg',
        db_params => {
            created_by => $subject,
            case_id    => $case->id,
        },
    });
    ok $second_file, 'Created second file';
    ok !$second_file->accepted, 'File not accepted';
    is $second_file->name, 'Unieke Filename', 'Name untouched';
}, 'file_create on existing filename not accepted');

$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;
    my $subject = $zs->get_subject_ok;

    my $file = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Unieke Filename.jpg',
        db_params => {
            created_by => $subject,
            case_id    => $case->id,
            accepted   => 1,
        },
    });
    ok $file, 'Created file';
    ok $file->accepted, 'File is accepted';

    my $second_file = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Unieke Filename.jpg',
        db_params => {
            created_by => $subject,
            case_id    => $case->id,
            accepted   => 1,
        },
    });
    ok $second_file, 'Created second file';
    ok !$second_file->accepted, 'File is accepted';
    is $second_file->name, 'Unieke Filename', 'Name changed';

}, 'file_create on existing filename with accepted');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->create_case_ok;
    my $subject = $zs->get_subject_ok;

    my $file = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Unieke Filename.jpg',
        db_params => {
            created_by => $subject,
            case_id    => $case->id,
            accepted   => 1,
        },
    });
    ok $file, 'Created file';
    ok $file->accepted, 'File is accepted';

    my $second_file = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Unieke Filename.jpg',
        db_params => {
            created_by => $subject,
            case_id    => $case->id,
        },
    });
    ok $second_file, 'Created second file';
    ok !$second_file->accepted, 'File not accepted';
    is $second_file->name, 'Unieke Filename', 'Name untouched';

    my $update = $second_file->update_properties({
        accepted => 1,
        subject  => $subject,
    });
    ok $update, 'Update succeeded';
    ok $update->accepted, 'File is accepted';
    is $second_file->name, 'Unieke Filename (1)', 'Name changed';
}, 'file_create on existing filename, accept through update_properties');


$zs->zs_transaction_ok(sub {
    my $subject = $zs->get_subject_ok;
    my $file    = $zs->create_file_ok;
    $file->update_properties({accepted => 1, subject => $subject});
    my $second_file = $zs->create_file_ok;
    ok !$second_file->accepted, 'Second file is not accepted';
    is $file->name, $second_file->name, 'Names match';

    ok $file->update_existing({
        subject => $subject,
        existing_file_id => $second_file->id,
    }), 'Replaced file';
    $second_file->discard_changes;
    is $second_file->root_file->id, $file->id, 'Files are now related';
    is $file->name, $second_file->name, 'Names still match';
}, 'update_existing should not rename');


$zs->zs_transaction_ok(
    sub {
        my $subject     = $zs->get_subject_ok;
        my $case        = $zs->create_case_ok();
        my $file        = $zs->create_file_ok;
        my $second_file = $zs->create_file_ok;

        ok $file->update({name => 'Silly Rabbit', accepted => 1}),
          'Set test parameters';

        ok $second_file->update({accepted => 1}), 'Set test parameters';
        isnt $file->name, $second_file->name, 'Names do not match';

        my $updated = $second_file->update_properties({
                subject => $subject,
                name    => $file->name,
        });
        $second_file->discard_changes;
        is $second_file->name, 'Silly Rabbit', "File isn't renamed";
    },
    'update_properties to existing name'
);


$zs->zs_transaction_ok(sub {
    my $subject = $zs->get_subject_ok;
    my $file    = $zs->create_file_ok;
    ok $file->update({name => 'Silly Rabbit', accepted => 1}), 'Set test parameters';
    my $second_file = $zs->create_file_ok;
    isnt $file->name, $second_file->name, 'Names do not match';

    my $updated = $second_file->update_properties({
        subject  => $subject,
        accepted => 1,
        name     => $file->name,
    });
    $second_file->discard_changes;
    is $second_file->name, 'Silly Rabbit', "File isn't renamed";
}, 'update_properties to existing name and accept in same call');

zs_done_testing();

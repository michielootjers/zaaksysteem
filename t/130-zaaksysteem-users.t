#! perl
use TestSetup;
initialize_test_globals_ok();

my $users = $schema->default_resultset_attributes->{users};
isa_ok($users, 'Zaaksysteem::Users');
my $basedn = 'o=zaaksysteem,dc=zaaksysteem,dc=nl';

my $users_ldap = $users->ldaph;
isa_ok($users_ldap, "Net::LDAP");

my $expect_users = {
    'admin' => {
        'cn'          => 'admin',
        'displayName' => 'A. Admin',
        'dn' => 'cn=admin,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '50001',
        'givenName'       => 'Anton',
        'homeDirectory'   => '/home/admin',
        'initials'        => 'A',
        'loginShell'      => '/nologin',
        'mail'            => 'demo-gebruiker@zaaksysteem.nl',
        'sn'              => 'Admin',
        'telephoneNumber' => '0612345678',
        'uid'             => 'admin',
        'uidNumber'       => '500001',
        'userPassword'    => '{SSHA}iwbtUYAf4wwRPgUW221L6BGrUrB4iWP+CgPBRA=='
    },
    'gebruiker' => {
        'cn'          => 'gebruiker',
        'displayName' => 'G. Bruiker',
        'dn' => 'cn=gebruiker,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '55101',
        'givenName'       => 'Bruiker',
        'homeDirectory'   => '/home/gebruiker',
        'initials'        => 'G.',
        'loginShell'      => '/nologin',
        'mail'            => 'demo-gebruiker@zaaksysteem.nl',
        'sn'              => 'Bruiker',
        'telephoneNumber' => '0612345678',
        'uid'             => 'Ge',
        'uidNumber'       => '55101',
        'userPassword'    => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA=='
    },
    'dudeski' => {
        'cn'          => 'dudeski',
        'displayName' => 'De Dude',
        'dn' => 'cn=dudeski,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '55102',
        'homeDirectory'   => '/home/dudeski',
        'initials'        => 'D.',
        'loginShell'      => '/nologin',
        'mail'            => 'dudeski@zaaksysteem.nl',
        'sn'              => 'Dude',
        'telephoneNumber' => '0612345678',
        'uid'             => 'De',
        'uidNumber'       => '55102',
        'userPassword'    => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA=='
    },
};
my $local_users = $users->_retrieve_local_users($users_ldap);
is_deeply($local_users, $expect_users, "_retreive_local_users");

my $cmp_expect = {
    delete => {},
    modify => {},
    create => {},
};

my $cmp_users = $users->_compare_users(
    local_users  => {},
    remote_users => {},
);
is_deeply($cmp_users, $cmp_expect, "_compare_users: empty");

$cmp_expect = {
    delete => {},
    modify => {},
    create => $local_users,
};
$cmp_users = $users->_compare_users(
    local_users  => {},
    remote_users => $local_users,
);
is_deeply($cmp_users, $cmp_expect, "_compare_users: create");

$cmp_expect = {
    delete => $local_users,
    modify => {},
    create => {},
};
$cmp_users = $users->_compare_users(
    local_users  => $local_users,
    remote_users => {},
);
is_deeply($cmp_users, $cmp_expect, "_compare_users: delete");

my %cp_local = %{$local_users};
$cp_local{gebruiker}{mail} = 'devnull@zaaksysteem.nl';
$cp_local{admin}{mail}     = 'devnull@zaaksysteem.nl';
$cmp_expect                = {
    delete => {},
    modify => \%cp_local,
    create => {},
};
$cmp_users = $users->_compare_users(
    local_users  => $local_users,
    remote_users => \%cp_local,
);
is_deeply($cmp_users, $cmp_expect, "_compare_users: modify");

$cmp_expect = {
    delete => {},
    modify => $local_users,
    create => {},
};
$cmp_users = $users->_compare_users(
    local_users  => $local_users,
    remote_users => $local_users,
);
is_deeply($cmp_users, $cmp_expect,
"_compare_users: if everything is the same, we still want to modify them users"
);

$users->schema($schema);
is($users->ldapbase(), $basedn, "Basedn is correct");

$zs->zs_transaction_ok(
    sub {
        my $data = {
            'dn' =>
              'cn=wesleys,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
            'cn'              => 'wesleys',
            'uid'             => 'Wesley',
            'displayName'     => 'W.G. Schwengle',
            'initials'        => 'W.G.',
            'sn'              => 'Schwengle',
            'givenName'       => 'Schwengle',
            'mail'            => 'wesley@mintlab.nl',
            'telephoneNumber' => '0612345678',
            'homeDirectory'   => '/home/wesleys',
            'loginShell'      => '/nologin',
            'uidNumber'       => '55404',
            'gidNumber'       => '55101',
            'userPassword' => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA==',
            department     => 'Ruimte',
        };

        my $ldap_user = $users->_create_ldap_user(
            username => 'wesleys',
            details  => $data,
        );
        ok($ldap_user, "LDAP user created");

        {
            no warnings qw(once redefine);
            local *Net::LDAP::Message::code = sub {return 100};
            local *Net::LDAP::Message::error =
              sub {return "This is an LDAP error"};

            throws_ok(
                sub {
                    $users->_create_ldap_user(
                        username => 'wesleys',
                        details  => $data,
                    );
                },
                qr/failed to add entry 'cn=wesleys/m,
                "User can only be created once in LDAP",
            );
        }

        $data = {};
        throws_ok(
            sub {
                $users->_create_ldap_user(
                    username => 'wesleys',
                    details  => $data
                );
            },
            qr/Minimal LDAP data not found/,
            "No data supplied",
        );

        $data = {cn => '', userPassword => '', displayName => ''};
        throws_ok(
            sub {
                $users->_create_ldap_user(
                    username => 'wesleys',
                    details  => $data,
                );
            },
            qr/Minimal LDAP data not found/,
            "Data is empty",
        );
        $data = {cn => 'x', userPassword => 'y'};
        throws_ok(
            sub {
                $users->_create_ldap_user(
                    username => 'wesleys',
                    details  => {cn => 'bla', userPassword => 'test'},
                );
            },
            qr/Minimal LDAP data not found/,
            "Minimal LDAP data not found"
        );
    },
    "_create_ldap_user",
);

$zs->zs_transaction_ok(
    sub {
        my $mw       = $zs->create_medewerker_ok();
        my $username = $mw->source_identifier;

        my $data = {
            'cn'              => $username,
            'uid'             => lcfirst($username),
            'displayName'     => 'Pietje Puk',
            'initials'        => 'PP',
            'sn'              => 'Puk',
            'givenName'       => 'Puk',
            'mail'            => 'devnull@mintlab.nl',
            'telephoneNumber' => '0612345678',
            'homeDirectory'   => '/home/ppuk',
            'loginShell'      => '/nologin',
            'dn'              => "cn=$username,ou=Meuk",
            'userPassword' => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA==',
        };

        {
            no warnings qw(once redefine);
            local *Zaaksysteem::Users::find = sub {
                return undef;
            };

            my $res = $users->_update_properties_from_ldap_user($data->{dn});
            is($res, undef, "User not found in LDAP");

            local *Zaaksysteem::Users::find = sub {
                return Net::LDAP::Entry->new($data->{dn}, %$data);
            };

            $res = $users->_update_properties_from_ldap_user($data->{dn});
            ok($res, "User found in LDAP");

            my $ldap_user = $users->_create_ldap_user(
                username => $username,
                details  => $data,
            );
            ok($ldap_user, "LDAP user created");
        }
    },
    "_update_properties_from_ldap_user",
);

$zs->zs_transaction_ok(
    sub {
        my $mw       = $zs->create_medewerker_ok();
        my $username = $mw->source_identifier;

        my $data = {
            'cn'              => $username,
            'uid'             => lcfirst($username),
            'displayName'     => 'Pietje Puk',
            'initials'        => 'PP',
            'sn'              => 'Puk',
            'givenName'       => 'Puk',
            'mail'            => 'devnull@mintlab.nl',
            'telephoneNumber' => '0612345678',
            'homeDirectory'   => '/home/ppuk',
            'loginShell'      => '/nologin',
            'dn'              => "cn=$username,ou=Meuk",
            'userPassword' => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA==',
        };

        my $ldap_user = $users->_delete_ldap_user(
            username => $username,
            dn       => $data->{dn},
        );
        ok(!$ldap_user, "LDAP user not deleted - force not applied");
        my $ue = $users->_get_user_entity_from_username($username);
        ok(!$ue->date_deleted, "User is not deleted");

        no warnings qw(once redefine);
        local *Zaaksysteem::Users::find = sub {
            return undef;
        };

        $ldap_user = $users->_delete_ldap_user(
            username => $username,
            dn       => $data->{dn},
            force    => 1,
        );
        ok($ldap_user, "LDAP user deleted");
        $ue = $users->_get_user_entity_from_username($username);
        ok($ue->date_deleted, "User is deleted");

        throws_ok(
            sub {
                $users->_delete_ldap_user(
                    username => 'admin',
                    dn       => $data->{dn},
                    force    => 1,
                );
            },
            qr/Refusing to delete admin user!/,
            "admin user may not be removed"
        );

        {
            no warnings qw(once redefine);
            local *Net::LDAP::Message::code = sub {return 100};
            local *Net::LDAP::Message::error =
              sub {return "This is an LDAP error"};

            throws_ok(
                sub {
                    $users->_delete_ldap_user(
                        username => $username,
                        dn       => $data->{dn},
                        force    => 1,
                    );
                },
qr#Failed to remote user '$username': $data->{dn} / This is an LDAP error#,
                "User deletion failed",
            );
        }

    },
    "_delete_ldap_user",
);

$zs->zs_transaction_ok(
    sub {
        my $mw       = $zs->create_medewerker_ok();
        my $username = $mw->source_identifier;

        my $old = {
            dn => "cn=$username,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl",
            cn            => $username,
            uid           => lcfirst($username),
            displayName   => 'Piö Puk',
            initials      => 'PP',
            sn            => 'Puk',
            givenName     => 'Puk',
            mail          => 'devnull@mintlab.nl',
            homeDirectory => '/home/ppuk',
            loginShell    => '/nologin',
            uidNumber     => '55666',
            gidNumber     => '55101',
            telephoneNumber => '0201234567',
            userPassword  => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA==',
            department    => 'Ruimte',
        };

        my $new = {%$old};

        my $ldap_user = $users->_modify_ldap_user(
            username        => $username,
            details         => $new,
            current_details => $old,
        );
        is($ldap_user, undef, "No modifications");

        $new->{mail}            = 'bitbucket@mintlab.nl';
        $new->{telephoneNumber} = '0612345678';
        $new->{displayName}     = "Pi\x{00f6} Puk";

        $ldap_user = $users->_modify_ldap_user(
            username        => $username,
            details         => $new,
            current_details => $old,
        );
        isa_ok($ldap_user, "Net::LDAP::Message");

        {
            no warnings qw(once redefine);
            local *Net::LDAP::Message::code = sub {return 100};
            local *Net::LDAP::Message::error =
              sub {return "This is an LDAP error"};

            throws_ok(
                sub {
                    $users->_modify_ldap_user(
                        username        => $username,
                        details         => $new,
                        current_details => $old,
                    );
                },
qr#Failed to modify entry '$old->{dn}': This is an LDAP error /#,
                "User deletion failed",
            );
        }

    },
    "_modify_ldap_user",
);

$zs->zs_transaction_ok(
    sub {
        my $roles = $users->get_all_roles;
        is(@$roles, 5, 'get_all_roles');

        # Fails for unknown reasons..
        #$roles = $users->get_array_of_roles();
        #is(@$roles, 5, 'get_array_of_roles');

        no warnings qw(once redefine);
        local *Net::LDAP::Message::count = sub { return 0 } ;

        $roles = $users->get_all_roles;
        ok(!@$roles, "No roles found");
    },
    "get_roles calls"
);

zs_done_testing();

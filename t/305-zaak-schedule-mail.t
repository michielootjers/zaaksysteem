#! perl

### Test header start
use warnings;
use strict;

use Data::Dumper;
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


$zs->zs_transaction_ok(sub {

    my $case = $zs->create_case_ok;

    ok !$case->get_email_interface, "no interface configured, falsy returned";

    my $interface = $zs->create_named_interface_ok({
        module              => 'email',
        name                => 'Email',
        interface_config    => {},
    });

    ok $case->get_email_interface, "interface configured, and returned";


}, 'Check check email interface');



zs_done_testing();

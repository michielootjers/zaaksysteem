#! /usr/bin/perl
use TestSetup;
initialize_test_globals_ok;

use Test::WWW::Mechanize::Catalyst;

ensure_test_server_ok();

my $mech = Test::WWW::Mechanize::Catalyst->new(catalyst_app => 'Zaaksysteem');

$mech->get_ok('/', "GET / works");

{
    my @inputs = $mech->grep_inputs({ name => qr/^username$/ });
    is(@inputs, 1, "One input named 'username' found");
}

{
    my @inputs = $mech->grep_inputs({ name => qr/^password$/ });
    is(@inputs, 1, "One input named 'password' found");
}

zs_done_testing();

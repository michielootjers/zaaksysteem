#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Backend::Object;
use Zaaksysteem::Search::ZQL;


$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    $case->update_hstore();

    my $rs = Zaaksysteem::Backend::Object->new(schema => $schema)->rs;
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case WHERE case.number = ' . $case->id);
    my $case_through_hstore = $zql->apply_to_resultset($rs)->single;

    is($case->id, $case_through_hstore->object_id, "Case was found through hstore search");

    my $hstore_properties = $case_through_hstore->index_hstore;

    my %object_attributes = map {
        $_->name => $_->index_value || undef
    } grep {
        !$_->dynamic_class
    }@{ $case->object_attributes };

    # case->object_attributes is a separate implementation
    while (my ($k, $v) = each %object_attributes) {
        is($hstore_properties->{$k}, $v, "Object attribute $k matches hstore property value");
    }
}, 'Updating hstore_properties');

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    $case->update_hstore;

    my $rs = Zaaksysteem::Backend::Object->new(schema => $schema)->rs;

    is $rs->count, 2, 'ObjectData row created after $case->update_hstore';

    my $object = $rs->search({ object_class => 'case' })->first;

    is $object->get_column('object_class'), 'case', 'object has proper class';
    is $object->get_column('object_id'), $case->id, 'object has proper id';

    $case->_delete_zaak;

    is $rs->count, 1, 'ObjectData row deleted after $case->_delete_zaak';

}, 'Deleted cases lose ObjectData row');

zs_done_testing();

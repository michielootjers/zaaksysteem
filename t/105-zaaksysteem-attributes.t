#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Types::MappedString;

use JSON;

use_ok('Zaaksysteem::Backend::Object::Attribute');


###
###
### Single Zaaksysteem::Backend::Object::Attribute
###
###
$zs->zs_transaction_ok(sub {
    throws_ok(
        sub {            
            Zaaksysteem::Backend::Object::Attribute->new();
        },
        qr/Attribute \(.*?\) is required/,
        'Check required attributes'
    );

    throws_ok(
        sub {            
            Zaaksysteem::Backend::Object::Attribute->new(
                name                => 'world',
                attribute_type      => 'textnot',
                value               => '1337',
            );
        },
        qr/Validation failed for 'AttributeTypes'/,
        'Check attribute validation'
    );

    throws_ok(
        sub {            
            Zaaksysteem::Backend::Object::Attribute->new(
                name                => [],
                attribute_type      => 'textnot',
                value               => '1337',
            );
        },
        qr/Attribute \(name\).*?Validation failed for 'Str'/,
        'Check attribute "name" validation'
    );


    my $attribute   = Zaaksysteem::Backend::Object::Attribute->new(
        name                => 'world',
        attribute_type      => 'text',
        value               => '1337',
        human_value         => 'hello',
    );

    isa_ok($attribute, 'Zaaksysteem::Backend::Object::Attribute');

    can_ok(
        $attribute,
        qw/
            attribute_type
            value
            human_value
            object_table
            object_id
            is_systeemkenmerk
            is_filter
        /
    );

    is($attribute->human_value, '1337', "Human value of simple text attribute");
    is($attribute->index_value, '1337', "Index value of simple text attribute");

    $attribute = Zaaksysteem::Backend::Object::Attribute->new(
        name                => 'world',
        attribute_type      => 'text',
        value               => Zaaksysteem::Types::MappedString->new(
            original => 'foreignese',
            mapped   => 'frontendese',
        ),
        human_value         => 'hello',
    );
    is($attribute->human_value, 'frontendese', "Human value of MappedString text attribute");
    is($attribute->index_value, 'foreignese', "Index value of MappedString text attribute");

}, 'Verified Zaaksysteem::Backend::Object::Attribute syntax');


$zs->zs_transaction_ok(sub {
    my $map         = {
        'name'              => 'world',
        'attribute_type'    => 'text',
        'value'             => 'hello',
    };

    my $attribute   = Zaaksysteem::Backend::Object::Attribute->new(
        %{ $map }
    );

    ok(
        $attribute->DOES('Zaaksysteem::Backend::Object::Attribute::Text'),
        'Succesfully consumed role for Text'
    );

    is($attribute->value, $attribute->human_value, 'Human value identical to value');
    my $json = $attribute->TO_JSON;

    isa_ok($json, 'HASH');

    is($json->{ $_ }, $attribute->$_, 'Correct JSON response for attribute "' . $_ . '"')
        for (keys %{ $map }, 'human_value');
}, 'Checked role "Attribute::Text"');


###
###
### Multiple Zaaksysteem::Backend::Object::Attributes
###
###


### REMOVE AFTER ZQL PROJECT, POC CODE:
# $zs->zs_transaction_ok(sub {
#     throws_ok(
#         sub {            
#             Zaaksysteem::Backend::Object::Attributes->new();
#         },
#         qr/Attribute \(.*?\) is required/,
#         'Check required attribute "source"'
#     );

#     my $attributes   = Zaaksysteem::Backend::Object::Attributes->new(
#         source  => $zs->create_case_ok,
#     );

#     ok(
#         $attributes->DOES('Zaaksysteem::Backend::Object::Attributes::SourceCase'),
#         'Succesfully consumed role for Case'
#     );

#     #note(explain($attributes->attributes));

#     ok(
#         JSON->new->allow_blessed->convert_blessed->pretty->encode($attributes->attributes)
#             =~ /zaak/,
#         'Can convert attributes to JSON'
#     );

# }, 'Verified Zaaksysteem::Backend::Object::Attributes syntax with case as source');

zs_done_testing();

#!/usr/bin/perl -w
use strict;

use Moose;
use Data::Dumper;
use JSON;
use Time::HiRes qw(gettimeofday tv_interval);

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw[ConfigLoader];

use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Model::DB;
use Zaaksysteem::Model::LDAP;

use Config::Any;

my $log = Zaaksysteem::Log::CallingLogger->new();

error("USAGE: $0 [hostname] [configfile] [customer.d-dir] [commit]") unless @ARGV && scalar(@ARGV) >= 3;
my ($hostname, $config_file, $config_dir, $commit)    = @ARGV;

my $mig_config                              = {};

my $current_hostname;
sub load_customer_d {
    my $config_root = $config_dir;

    # Fetch configuration files
    opendir CONFIG, $config_root or die "Error reading config root: $!";
    my @confs = grep {/\.conf$/} readdir CONFIG;

    info("Found configuration files: @confs");

    for my $f (@confs) {
        my $config      = _process_config("$config_root/$f");
        my @customers   = $config;

        for my $customer (@customers) {
            for my $config_hostname (keys %$customer) {
                my $config_data     = $customer->{$config_hostname};

                if ($hostname eq $config_hostname ) {
                    info('Upgrading hostname: ' . $hostname);
                    $current_hostname = $hostname;
                    $mig_config->{database_dsn}         = $config_data->{'Model::DB'}->{connect_info}->{dsn};
                    $mig_config->{database_password}    = $config_data->{'Model::DB'}->{connect_info}->{password};
                    $mig_config->{ldap_config_basedn}   = $config_data->{LDAP}->{basedn};
                    $mig_config->{customer_info}        = { gemeente => $config_data->{customer_info} };
                }
            }
        }
    }
}

sub load_zaaksysteem_conf {
    my $config_root = 'etc/customer.d/';
    my $config      = _process_config($config_file);

    $mig_config->{ldap_config_hostname}         = $config->{LDAP}->{hostname};
    $mig_config->{ldap_config_password}         = $config->{LDAP}->{password};
    $mig_config->{ldap_config_user}             = $config->{LDAP}->{admin};
    $mig_config->{ldap_config}                  = $config->{LDAP};
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

load_customer_d();
load_zaaksysteem_conf();

error('Cannot find requested hostname: ' . $hostname) unless $current_hostname;

error('Missing one of required config params: ' . Data::Dumper::Dumper($mig_config))
    unless (
        $mig_config->{database_dsn}
    );

my $dbic = database($mig_config->{database_dsn}, $mig_config->{user}, $mig_config->{database_password});
my $ldap = build_ldap();

$dbic->ldap($ldap);
$dbic->betrokkene_model->ldap($ldap);

$dbic->txn_do(sub {
    initialize_hstore($dbic);

    unless ($commit) {
        die("ROLLBACK, COMMIT BIT not given\n");
    }
});

$log->info('All done.');
$log->_flush;

sub build_ldap {
    my $ldap = Net::LDAP->new(
        $mig_config->{ldap_config_hostname},
        version => 3,
    );

    $ldap->bind(
        $mig_config->{ldap_config_user},
        password    => $mig_config->{ldap_config_password}
    ) or die "Could not bind";

    return Zaaksysteem::Backend::LDAP::Model->new(
        ldap       => $ldap,
        base_dn    => $mig_config->{ldap_config_basedn},
        ldapconfig => $mig_config->{ldap_config},
        ldapcache  => {},
    );
}

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    my $schema = Zaaksysteem::Model::DB->new->schema;

    $schema->log($log);

    $schema->betrokkene_model->log($log);
    $schema->betrokkene_model->config($mig_config->{customer_info});

    return $schema;
}

sub error {
    my $error = shift;

    $log->error($error);
    $log->_flush;

    die("\n");
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub initialize_hstore {
    my $dbic = shift;

    my $rs = $dbic->resultset('Zaak')->search_extended(undef, {order_by => 'me.id'});
    my $count = $rs->count();

    my $starttime = [gettimeofday];
    my $cases_done = 0;
    while(my $zaak = $rs->next) {
        $cases_done++;

        info(
            sprintf(
                "Updating hstore for zaak %d of %d (%.3f/second)",
                $cases_done,
                $count,
                $cases_done / tv_interval($starttime, [gettimeofday]),
            )
        );
        $zaak->touch();
    };
}

1;

BEGIN;

    ALTER TABLE file_metadata ADD COLUMN origin_date date;

COMMIT;

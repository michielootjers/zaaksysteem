# Zaaksysteem.nl

This is the main code repository for the Zaaksysteem framework.

For more information, visit our [website](http://www.zaaksysteem.nl/).

## Short technical introduction

All development for Zaaksysteem follows the following procedure when it comes
to getting the code in this codebase. Since this is opensource software, you
are free to fork, modify, redistribute and open pull-requests, under the
limitations of the
[EUPL license](http://joinup.ec.europa.eu/software/page/eupl).

### Branches

* master
    The master branch contains the latest *stable* and released version of
    Zaaksysteem. This is the version most people want.

* quarterly
    The quarterly branch has a common ancestor at the current master branch,
    and contains all new features, bugfixes and other modifications done in
    our three-monthly release cycle. This branch is considered *stable*, as it
    has been tested internally, but it has not seen a production environment
    yet.

* sprint-\*
    This branch contains active and ongoing development modifications, it is
    *unstable* as it may contain modifications that have not been tested fully.
    It is part of our bi-weekly sprint cycle, and is named according to the ISO
    weeknumber of the week the sprint started in.

### Contributing

You are free to submit pull-requests for improvements to Zaaksysteem. Please
target those requests to our most recent sprint-\* branch, and describe
liberally what the change does, why it does so, and what you believe the impact
will be.


-- 
The [Mintlab](http://www.mintlab.nl/) Team

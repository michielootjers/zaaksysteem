package Zaaksysteem::Kennisbank::ComponentGeneric;

use strict;
use warnings;

use Moose;
use Data::Dumper;

use HTML::Entities;

use Zaaksysteem::Constants;

extends 'DBIx::Class';

sub last_seen {
    my $self    = shift;

    my $source_name = $self->result_source->name;

    return $self->result_source->schema->resultset('Seen')->search(
        {
            component               => $source_name,
            $source_name . '_id'    => ($self->pid || $self->id),
        },
        {
            rows                    => 1,
            order_by                => { '-desc'    => 'created' },
        }
    )->first;
}

sub duplicate {
    my $self = shift;
    my $rs = $self->result_source->resultset;

    my @final;
    my @columns = (
        @{ $rs->get_edit_profile->{ required } },
        @{ $rs->get_edit_profile->{ optional } }
    );

    for my $column ($self->result_source->columns) {
        next unless (grep { $_ eq $column } @columns);

        push(@final, $column);
    }

    my %opts = map { $_ => $self->get_column($_) } @final;

    return $rs->bewerk_entry(\%opts);
}

sub titel_url {
    my $self    = shift;

    my $titel   = $self->naam;


    $titel      =~ s/[^\d\w_\-]+$//g;
    $titel      =~ s/[^\d\w_\-]/_/g;

    return lc($titel);
}

sub entry_url {
    my $self    = shift;

    my $url     = '/kennisbank/';
    
    if ($self->result_source->name =~ /product/) {
        $url    .= 'product/';
    } elsif ($self->result_source->name =~ /vragen/) {
        $url    .= 'vraag/';
    }

    my $bibliotheek_categorie_id = 0;
    if($self->bibliotheek_categorie_id) {
        $bibliotheek_categorie_id = $self->bibliotheek_categorie_id->id;
    }
    $url .= $bibliotheek_categorie_id . '/' . $self->id . '/' . $self->titel_url;

    return lc($url);
}

sub author_name {
    my $self        = shift;

    my $author_id   = $self->author;

    my $author = $self->result_source
        ->schema
        ->resultset('Zaak')
        ->betrokkene_model->get(
            {},
            $author_id
        );

    return unless $author;
    return $author->naam;
}

sub gerelateerde_zaaktypen {
    my $self    = shift;

    $self->_search_gerelateerd('zaaktypen');
}

sub gerelateerde_vragen {
    my $self    = shift;

    $self->_search_gerelateerd('vragen');
}

sub gerelateerde_producten {
    my $self    = shift;

    $self->_search_gerelateerd('producten');
}

sub _search_gerelateerd {
    my $self            = shift;
    my $related_table   = shift;

    ### Calculate current class from result_source
    ### kennisbank_producten becomes kennisbank_producten_id
    my $name            = $self->result_source->name;
    my $local_column    = $name . '_id';

    my $relaties        = $self->kennisbank_relaties->search(
        {
            $local_column   => $self->id,
        }
    );

    if ($related_table eq 'zaaktypen') {
        return $self->_search_gerelateerde_zaaktypen($relaties);
    } else {
        return $self->_search_gerelateerde_kennisbank_items(
            $relaties,
            $related_table
        );
    }
}

sub _search_gerelateerde_kennisbank_items {
    my $self            = shift;
    my $relaties        = shift;
    my $related_table   = shift;

    my $foreign_column  = 'kennisbank_' . $related_table . '_id';

    return $self->result_source->schema->resultset(
        'Kennisbank' . ucfirst($related_table)
    )->search(
        {
            '-or'   => [
                id  => {
                    '-in' => $relaties->get_column($foreign_column)->as_query
                },
                pid => {
                    '-in' => $relaties->get_column($foreign_column)->as_query
                },
            ],
            'deleted'   => undef,
        },
        {
            order_by    => 'naam',
        }
    );
}

sub _search_gerelateerde_zaaktypen {
    my $self        = shift;
    my $relaties    = shift;

    return $self->result_source->schema->resultset(
        'Zaaktype'
    )->search(
        {
            'me.id'  => {
                '-in' => $relaties->get_column('zaaktype_id')->as_query
            }
        },
        {
            prefetch    => 'zaaktype_node_id',
            order_by    => 'zaaktype_node_id.titel',
        }
    );
}

sub search_versies {
    my $self        = shift;

    my @ids = ($self->id);
    push(@ids, $self->pid->id) if $self->pid;

    return $self->result_source->resultset->search(
        {
            '-or'   => [
                { 'id'  => { '-in' => \@ids } },
                { 'pid' => ($self->pid ? $self->pid->id : $self->id) }
            ],
        },
        {
            order_by    => { '-desc' => 'id' },
        }
    );
}

sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);}

sub update {
    my ($self) = shift;

    $self->_set_search_term();
    return $self->next::method(@_);
}

sub _set_search_term {
    my ($self) = @_;
    
    if(ref $self eq 'Zaaksysteem::Model::DB::KennisbankProducten') {        
        my $search_term = $self->_strip_html($self->naam || '');
        $self->search_term($search_term);
    } elsif(ref $self eq 'Zaaksysteem::Model::DB::KennisbankVragen') {        
        my $search_term = join " ", (
            $self->_strip_html($self->naam || ''), 
            $self->_strip_html($self->vraag || '') , 
            $self->_strip_html($self->antwoord|| '') 
        );
#        warn "vragen search_term: [" . $search_term . "]";
        $self->search_term($search_term);
    }
}

sub _strip_html {
    my ($self, $html) = @_;

    $html =~ s|&#160;||gis;

    my $tree = HTML::TreeBuilder->new;
    $tree->parse($html);
    $tree->eof();

    return $tree->as_text;
}

sub TO_JSON {
    my $self            = shift;

    my @public_columns  = qw/
        voorwaarden
        omschrijving
        kosten
        aanpak
        naam
        created
        publication
        
        vraag
        antwoord
    /;

    my $fields  = {};
    my $cols    = { $self->get_columns };
    for my $field ($self->result_source->columns) {
        next unless grep { $field eq $_ } @public_columns;

        $fields->{ $field } = $cols->{ $field };
    }

    ### Zaaktype
    $fields->{related_zaaktypen} = [];

    my $ztn = $self->gerelateerde_zaaktypen;
    while (my $zt = $ztn->next) {
        push(
            @{ $fields->{related_zaaktypen} },
            {
                url     => '/aanvraag/' .
                    $zt->zaaktype_node_id->url_title . '/unknown',
                title   => $zt->zaaktype_node_id->titel,
                id      => $zt->id,
            }
        )
    }

    return $fields;
}

1;

package Zaaksysteem::StatsD;

use Moose;
use Scalar::Util qw/blessed/;

=head1 NAME

Zaaksysteem::StatsD - StatsD Module for Zaaksysteem

=head1 SYNOPSIS

    ### Increment by one
    $c->statsd->increment('employee.login.failed', 1);

=head1 DESCRIPTION

Interface to statsd daemon

=head1 METHODS

=head2 $c->statsd

Return value: $STATSD_OBJECT

    my $statsd_object = $c->statsd;

Returns the L<Zaaksysteem::StatsD::Backend> object, which is documented below

=cut

has 'statsd' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $c       = shift;

        die('Cannot run StatsD from unblessed object') unless blessed($c);

        return Zaaksysteem::StatsD::Backend->new(
            config          => $c->config->{statsd},
            active          => $c->customer_instance->{start_config}->{statsd_enabled},
            calledhost      => $c->req->uri->host,
        );
    }
);


package Zaaksysteem::StatsD::Backend;

use Net::Statsd;
use Moose;

has 'config'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub { {}; }
);

has [qw/active calledhost prefix/]    => (
    'is'        => 'rw',
);

sub BUILD {
    my $self            = shift;

    return unless $self->active;

    unless (
        UNIVERSAL::isa($self->config, 'HASH') &&
        defined $self->config->{host} &&
        $self->config->{host}
    ) {
        $self->active(undef);
        return;
    }

    $self->_load_prefix;
}

sub _load_prefix {
    my $self            = shift;

    my $prefix          = 'host.';
    if ($self->calledhost) {
        $prefix .= $self->calledhost;
    } else {
        $prefix .= 'unknown';
    }

    $prefix .= '.metric.zaaksysteem.';

    $self->prefix($prefix);
}


sub _prepare {
    my $self            = shift;

    return unless $self->active;

    $Net::Statsd::HOST = $self->config->{host};

    if (defined $self->config->{port} && $self->config->{port}) {
        $Net::Statsd::PORT = $self->config->{port};
    }

    return 1;
}

=head1 BACKEND METHODS

=head2 $c->statsd->increment($STRING_PREFIX, $INTEGER)

Return value: $BOOL_SUCCESS

    $c->statsd->increment('employee.login.failed', 1);

Increments a statsd counter with one.

=cut

sub increment {
    my $self            = shift;
    my $metric          = shift;

    return unless $self->_prepare;

    return Net::Statsd::increment($self->prefix . $metric, @_);
}

=head2 $c->statsd->timing($STRING_PREFIX, $TIME_MICROSECONDS)

Return value: $BOOL_SUCCESS

    $c->statsd->timing('request.time', '2500');

Increments a statsd counter with one.

=cut

sub timing {
    my $self            = shift;
    my $metric          = shift;

    return unless $self->_prepare;

    return Net::Statsd::timing($self->prefix . $metric, @_);
}


1;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut



package Zaaksysteem::Model::Bibliotheek::Kenmerken;

use strict;
use warnings;
use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
    PARAMS_PROFILE_DEFAULT_MSGS
/;

use parent 'Catalyst::Model';

use Data::Dumper;
use JSON::XS ();

use constant KENMERKEN              => 'kenmerken';
use constant KENMERKEN_DB           => 'DB::BibliotheekKenmerken';
use constant KENMERKEN_OPTIONS_DB   => 'DB::BibliotheekKenmerkenValues';

use constant KENMERKEN_DB_MAP       => {


};

use Moose;

has 'c' => (
    is  => 'rw',
);


{
    Zaaksysteem->register_profile(
        method  => 'bewerken',
        profile => {
            required => [ qw/
                kenmerk_naam
                kenmerk_type
                bibliotheek_categorie_id
                commit_message
            /],
            optional => [ qw/
                id
                kenmerk_help
                kenmerk_value_default
                kenmerk_magic_string
                kenmerk_options
                kenmerk_document_categorie
                kenmerk_type_multiple
            /],
            missing_optional_valid => 1,
            dependencies        => {
                'kenmerk_type'   => sub {
                    my ($dfv, $value) = @_;

                    if (
                        $value &&
                        exists(ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                                $value
                        }->{multiple}) &&
                        ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                            $value
                        }->{multiple}
                    ) {
                        return [ 'kenmerk_options' ];
                    }

                    return [];
                },
            },
            constraint_methods  => {
                kenmerk_magic_string    => qr/^[\w0-9_]+$/,
                kenmerk_naam            => qr/^.{2,64}$/,
            },
            field_filters       => {
                kenmerk_type_multiple => sub {
                    my ($field) = @_;
                    
                    return $field ? 1 : 0;
                },
            },
            msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
        }
    );

    sub bewerken {
        my ($self, $params) = @_;

        my $dv = $self->c->check(
            params  => $params,
        );
        return unless $dv->success;

        ### Magic string check
        return if (
            !$params->{id} &&
            (
                !$params->{kenmerk_magic_string} ||
                $params->{kenmerk_magic_string} ne
                $self->c->model('DB::BibliotheekKenmerken')->generate_magic_string(
                    $params->{kenmerk_magic_string}
                )
            )
        );

        $self->c->log->debug('Trying to create kenmerk');

        my $valid_options = $dv->valid;

        delete $valid_options->{commit_message};
        ### Rewrite some values
        my %options = map {
            my $key = $_;
            $key =~ s/^kenmerk_//;
            $key => $valid_options->{ $_ }
        } keys %{ $valid_options };

        $options{value_type} = $options{type};
        delete($options{type});

        ### Delete options from options
        my $kenmerk_options;
        if (
            $options{options} &&
            exists(ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                    $options{value_type}
            }->{multiple})
        ) {
            $kenmerk_options = $options{options};
        }

        delete($options{options});
        delete($options{id}) unless $options{id};
        delete($options{kenmerken});

        my $kenmerk = $self->c->model(KENMERKEN_DB)->update_or_create(\%options);
        return unless $kenmerk;

        $kenmerk->bump_version;

        my $options = JSON::XS->new->decode($dv->valid->{kenmerk_options});
        if ($options && ref $options eq 'ARRAY' && @$options) {
            $kenmerk->save_options({options => $options});
        }


        # Create or update a file_metadata entry if any values are given for it.
        my $orig_params = $self->c->req->params;
        my %md_create;
        my @md_keys = grep {$_ =~ /^metadata_/} keys %{ $orig_params };
        for my $md_key (@md_keys) {
            my $value = $orig_params->{$md_key};

            if ($value && $value ne '') {
                $md_key =~ s/metadata_//;
                $md_create{$md_key} = $value;
            }
        }
        if (%md_create && $kenmerk->file_metadata_id) {
            $kenmerk->file_metadata_id->update(\%md_create);
        }
        elsif (%md_create) {
            my $file_md = $self->c->model('DB::FileMetadata')->create(\%md_create);
            $kenmerk->update({file_metadata_id => $file_md->id});
        }

        return $kenmerk;
    }
}


sub get {
    my ($self, %opt) = @_;
    my %rv;

    unless (defined $opt{id} && $opt{id}) {
        $self->c->log->warn('No ID supplied for kenmerk retrieval, bailing out.');

        return;
    }

    my $kenmerk         = $self->c->model(KENMERKEN_DB)->find($opt{id});

    unless ($kenmerk) { 
        $self->c->log->error(sprintf 'Kenmerk not found by supplied ID (%s).', $opt{id});

        return;
    }

    my @rv_map;
    {
        my $edit_profile    = $self->c->get_profile(
            'method'=> 'bewerken',
            'caller' => __PACKAGE__
        ) or return;

        @rv_map = grep {$_ ne 'commit_message'} @{ $edit_profile->{optional} };

        if ($edit_profile->{required}) {
            push(@rv_map, @{ $edit_profile->{required} });
        }
    }


    $rv{kenmerk_options} = $kenmerk->options;

    if ($kenmerk->file_metadata_id) {
        $rv{file_metadata_id} = $kenmerk->file_metadata_id;
    }

    for my $key (@rv_map) {
        if ($key eq 'kenmerk_options') { next; }
        if ($key eq 'commit_message')  { next; }
        my $dbkey   = $key;

        $dbkey =~ s/^kenmerk_//g;
        if ($dbkey eq 'type') { $dbkey = 'value_type'; }
        $rv{$key} = $kenmerk->$dbkey;
    }

    return \%rv;
}


sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->{c} = $c;

    return $self;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


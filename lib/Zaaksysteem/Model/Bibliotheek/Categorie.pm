package Zaaksysteem::Model::Bibliotheek::Categorie;

use strict;
use warnings;
use Zaaksysteem::Constants;

use parent 'Catalyst::Model';

use Data::Dumper;
use Digest::MD5::File qw/-nofatals file_md5_hex/;

use Encode qw/encode/;
use Params::Profile;

use constant CATEGORIE_DB           => 'DB::BibliotheekCategorie';

use Moose;

use utf8;

has 'c' => (
    is  => 'rw',
    weak_ref => 1,
);

has 'crumbs' => (
    is => 'rw',
    weak_ref => 1,
);




sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->c($c);

    return $self;
}



Params::Profile->register_profile(
    method  => 'get_crumb_path',
    profile => {
        required => [ qw/bibliotheek_categorie_id/ ]
    }
);

sub get_crumb_path {
    my ($self, $params) = @_;
    
    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for get_crumb_path" unless $dv->success;

    my $bibliotheek_categorie_id = $params->{bibliotheek_categorie_id};

    return [] if not defined $bibliotheek_categorie_id;

    my $pid = $bibliotheek_categorie_id;

    my $crumbs = []; 
    
    while($pid) {
        my $category = $self->c->model(CATEGORIE_DB)->find($pid);
        $pid = $category->get_column('pid');

        unshift (@$crumbs, {id => $category->id, name => $category->naam});
    }

    return $crumbs;
}



1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Model::Attributes;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::Attributes qw[ZAAKSYSTEEM_SYSTEM_ATTRIBUTES];

with 'Catalyst::Component::InstancePerContext';

has schema => ( is => 'ro', required => 1 );
has log => ( is => 'ro', isa => 'Catalyst::Log' );

=head1 NAME

Zaaksysteem::Model::Attributes - Generic interface to all object-like attributes

=head1 SYNOPSIS

    my @attrs = $c->model('Attributes')->search('keyword');

=cut

=head1 CONSTRUCTORS

=head2 build_per_context_instance

This function instantiates this model object according to L<Catalyst>'s rules
regarding context instantiation. It roughly means this model gets a fresh object
every request.

This constructor captures a hard reference to the C<< $c->model('DB') >> object.

=cut

sub build_per_context_instance {
    my $class = shift;
    my $c = shift;

    return $class->new(
        log => $c->log,
        schema => $c->model('DB')->schema
    );
}

=head1 METHODS

=head2 search

Main entry to this model, allows full-text free-form querying of all attributes
known to Zaaksysteem.

    my $attrs = $c->model('Attributes');

    for my $attr ($attrs->search('bag')) {
        ...
    }

If no query is provided, only system attributes will be returned.

=cut

sub search {
    my $self = shift;
    my $keyword = shift;

    my @ret;

    if($keyword) {
        push @ret, $self->_search_db($keyword);
    }

    push @ret, $self->_search_sys($keyword);

    return @ret;
}

=head1 INTERNAL METHODS

=head2 _search_sys

=cut

sub _search_sys {
    my $self = shift;
    my $keyword = shift;

    my @ret;

    for my $attr (@{ ZAAKSYSTEEM_SYSTEM_ATTRIBUTES()->() }) {
        next unless $attr->bwcompat_name;

        my $name = $attr->bwcompat_name;

        if (ref $name) {
            $name = $name->[0];
        }

        my @label_parts = map { ucfirst } split '_', $name;

        # Pass over the current attribute if it doesn't match our query
        # OR add by default if no keyword is provided.
        if (length $keyword) {
            next unless grep { $keyword =~ m[$_]i } @label_parts;
        }

        push @ret, {
            id => $attr->name,
            label => join(' ', @label_parts),
            object_type => 'attribute',
            object => {
                source => 'sys-attr',
                value_type => $attr->attribute_type,
                column_name => $attr->name
            }
        };
    }

    return @ret;
}

=head2 _search_db

=cut

sub _search_db {
    my $self = shift;
    my $keyword = sprintf('%%%s%%', shift);

    my @ret;

    my $attributes = $self->schema->resultset('BibliotheekKenmerken')->search({
        deleted => undef,
        -or => [
            naam => { 'ilike' => $keyword },
            label => { 'ilike' => $keyword }
        ]
    });

    for my $attr ($attributes->all) {
        my %result = (
            id => $attr->id,
            label => $attr->naam,
            object_type => 'attribute',
            object => {
                source => 'db-attr',
                value_type => $attr->value_type,
                column_name => "attribute." . ($attr->magic_string // '')
            }
        );

        if ($attr->bibliotheek_kenmerken_values->count) {
            $result{ object }{ values } = [ map
                { { value => $_->value, active => $_->active, sort_order => $_->sort_order } }
                $attr->bibliotheek_kenmerken_values->all
            ];
        }

        push @ret, \%result;
    }

    return @ret;
}

1;

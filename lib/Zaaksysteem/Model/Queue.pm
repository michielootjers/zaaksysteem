package Zaaksysteem::Model::Queue;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Object::Queue::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Queue - Catalyst model factory for
L<Zaaksysteem::Object::Queue::Model>.

=head1 SYNOPSIS

    my $queue_model = $c->model('Queue');

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Object::Queue::Model->new>>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        table => $c->model('DB::Queue'),
        base_uri => $c->uri_for('/'),
        object_model => $c->model('Object'),
        subject_model => $c->model('Betrokkene'),
        subject_table => $c->model('DB::Subject'),
        geocoder => $c->model('Geo')
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

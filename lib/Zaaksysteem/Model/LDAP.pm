package Zaaksysteem::Model::LDAP;

use Moose;

use Zaaksysteem::Backend::LDAP::Model;

with 'Catalyst::Component::InstancePerContext';

=head1 NAME

Zaaksysteem::Model::LDAP - Bind Zaaksysteem config to an
L<Zaaksysteem::Backend::LDAP::Model> instance.

=head1 SYNOPSIS

This package exists merely to instantiate the actual LDAP model. The lifecycle
of the model is per context.

=head1 CONSTRUCTORS

=head2 build_per_context_instance

Build a new context instance of L<Zaaksysteem::Backend::LDAP::Model>.

=cut

sub build_per_context_instance {
    my $class = shift;
    my $c = shift;

    return Zaaksysteem::Backend::LDAP::Model->new(
        ldap => Net::LDAP->new($c->config->{ LDAP }->{ hostname }),
        base_dn => $c->customer_instance->{ start_config }->{ LDAP }->{ basedn },
        ldapconfig => $c->config->{ LDAP },
        ldapcache => ($c->stash->{__ldap_cache} = {})
    );
}

__PACKAGE__->meta->make_immutable;

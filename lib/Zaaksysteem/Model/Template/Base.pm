package Zaaksysteem::Model::Template::Base;

use Moose;

has document => (is => 'ro');

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig({ document => shift });
};

1;

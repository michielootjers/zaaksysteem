package Zaaksysteem::Model::DB;

use Moose;
use Scalar::Util;

use Zaaksysteem::Zaken::DelayedTouch;

extends 'Catalyst::Model::DBIC::Schema';

with 'Catalyst::Component::InstancePerContext';

__PACKAGE__->config(
    schema_class => 'Zaaksysteem::Schema',
);

sub build_per_context_instance {
    my $self    = shift;
    my $c       = shift;

    my $new = (ref($self) ? $self->new(%$self) : $self->new);

    my $customer_instance   = $c->customer_instance;

    ### Database connection per request:
    unless ($c->stash->{__dbh}) {
        $c->stash->{__dbh} = $self->schema->connect(
            {
                %{
                    $customer_instance->{start_config}->{'Model::DB'}->{connect_info}
                },
                auto_savepoint => 1,
            }
        );

        $new->schema->default_resultset_attributes->{ delayed_touch } = Zaaksysteem::Zaken::DelayedTouch->new;
    }

    $new->schema(
        $c->stash->{__dbh}
    );

    $new->schema->default_resultset_attributes->{ log } = $c->log;
    $new->schema->default_resultset_attributes->{ config } = $c->config;
    $new->schema->default_resultset_attributes->{ users } = $c->model('Users');
    $new->schema->default_resultset_attributes->{ ldap } = $c->model('LDAP');
    $new->schema->default_resultset_attributes->{ cache } = $c->cache;

    ### Accessors on schema object
    $new->schema->log($c->log);
    $new->schema->catalyst_config($c->config);
    $new->schema->customer_instance($customer_instance);
    $new->schema->users($c->model('Users'));
    $new->schema->cache($c->cache);
    $new->schema->ldap($c->model('LDAP'));

    my $betrokkene_stash    = {};
    $betrokkene_stash->{$_} = $c->stash->{$_} for qw/
        order
        order_direction
        paging_rows
        paging_page
        paging_total
        paging_lastpage
        paging_total
        paging_lastpage
    /;

    $new->schema->betrokkene_pager($betrokkene_stash);

    # if (ref($c)) {
    #     if ($c->user_exists) {
    #         $new->schema->current_user(
    #             $new->schema->default_resultset_attributes->{current_user} = $c->user
    #         );
    #     }
    # }

    # if (ref($c) && $c->user_exists) {
    #     $attributes->{current_user} = $c->user;
    #     $attributes->{current_user_ou_ancestry} = $self->get_current_user_ou_ancestry($c);
    # }

    foreach my $attribute (qw/log config users ldap cache/) {
        if(!Scalar::Util::isweak($new->schema->default_resultset_attributes->{$attribute})) {
            Scalar::Util::weaken($new->schema->default_resultset_attributes->{$attribute});
        }
    }

    return $new;
}


=head2 get_current_user_ou_ancestry

Users will inherit the rights of their parent ou's.

Expensive though: This sub may execute 4-5 ldap queries.

A structural solution could be to store casetype ou rights
as LDAP strings instead of id's, that way the ancestry would
already be in the strings.

=cut

sub get_current_user_ou_ancestry {
    my ($self, $c) = @_;

    my $users = $c->model('Users');

    my $ou_id = $users->get_user_ou_id($c->user->ldap_entry);
    my $ou_entry = $users->get_ou_by_id($ou_id);

    return $users->get_parent_ou_ids($ou_entry);
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


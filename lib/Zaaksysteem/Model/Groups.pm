package Zaaksysteem::Model::Groups;

use strict;
use warnings;
use Net::LDAP;
use parent 'Catalyst::Model';

use Data::Dumper;

use Moose;

has ldap => ( is => 'rw', isa => 'Zaaksysteem::Backend::LDAP::Model' );
has c => ( is => 'rw', isa => 'Zaaksysteem', 'weak_ref' => 1);

sub is_member {
    my ($self, $gid) = @_;

    return unless $self->c->user_exists;

    my $rs = $self->ldap->search(sprintf(
        '(&(objectClass=posixGroup)(memberUid=%s)(gidNumber=%s))',
        $self->c->user->ldap_entry->dn,
        $gid
    ));

    return $rs->count > 1;
}

sub return_roles_by_member {
    my ($self, $member) = @_;

    return unless $self->c->user_exists;

    my $rs = $self->ldap->search(sprintf(
        '(&(objectClass=posixGroup)(memberUid=%s))',
        $member
    ));

    return $rs->entries;
}

sub search {
    my $self        = shift;

    my $rs = $self->ldap->search('(&(objectClass=posixGroup))');

    my @results;

    foreach my $entry ($rs->entries) {
        my $dn          = $entry->dn;
        my $cn          = $entry->get_value('cn');

        my ($parent_ou) = $dn =~ /cn=$cn,ou=(.*?),/;

        push(@results, {
            'ou'            => $parent_ou,
            'naam'          => $entry->get_value('description'),
            'short_name'    => $cn,
            'id'            => $entry->get_value('gidNumber'),
        });
    }

    return $self->sort_by_depth(\@results);
}

sub get_ou_by_id {
    my $self        = shift;
    my $id          = shift;

    my $rs = $self->ldap->search(sprintf(
        '(&(objectClass=organizationalUnit)(l=%s))',
        $id
    ));

    return unless $rs->entry(0);

    return $rs->entry(0)->get_value('ou');
}

sub get_role_by_id {
    my $self        = shift;
    my $id          = shift;

    my $rs = $self->ldap->search(sprintf(
        '(&(objectClass=posixGroup)(gidNumber=%s))',
        $id
    ));

    return unless $rs->entry(0);

    return $rs->entry(0)->get_value('cn');
}

sub search_ou {
    my $self        = shift;
    
    my $rs = $self->ldap->search('(&(objectClass=organizationalUnit))');

    my @results;

    foreach my $entry ($rs->entries) {
        push(@results, {
            ou  => $entry->get_value('ou'),
            id  => $entry->get_value('l')
        });
    }

    return [ sort @results ];
}

sub sort_by_depth {
    my ($self, $rollen) = @_;

    my @sorted = sort { ($a->{ou}||'') cmp ($b->{ou}||'') } @{ $rollen };

    return \@sorted;
}

sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->ldap($c->model('LDAP'));
    $self->c($c);

    return $self;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


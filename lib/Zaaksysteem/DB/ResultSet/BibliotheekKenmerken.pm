package Zaaksysteem::DB::ResultSet::BibliotheekKenmerken;

use Moose;

extends 'Zaaksysteem::Backend::ResultSet';

use constant MAGIC_STRING_DEFAULT => 'doc_variable';

sub generate_magic_string {
    my ($self, $suggestion) = @_;
    my $suggestion_ok       = 0;
    my $suggestion_counter  = 0;

    $suggestion             = lc($suggestion);

    ### Replace suggestion
    $suggestion             =~ s/ /_/g;
    $suggestion             =~  s/[^\w0-9_]//g;

    if (!$suggestion) {
        $suggestion = MAGIC_STRING_DEFAULT . ++$suggestion_counter;
    }

    ### Search database for given suggestion
    while (!$suggestion_ok) {
        my $rv = $self->search({magic_string => $suggestion});

        if (!$rv->count) {
            $suggestion_ok  = 1;
        } else {
            if ($suggestion_counter > 0) {
                $suggestion =~ s/$suggestion_counter$//;
            }
            $suggestion     .= ++$suggestion_counter;
        }
    }

    return $suggestion;
}

1;

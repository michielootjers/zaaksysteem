package Zaaksysteem::DB::ResultSet::BibliotheekCategorie;

use strict;
use warnings;

use Moose;


extends 'DBIx::Class::ResultSet';

__PACKAGE__->load_components(qw/Helper::ResultSet::SetOperations/);


# TODO recursion protection
sub tree {
	my ($self, $pid, $depth) = @_;

	$depth ||= 0;

	my $rs = $self->search({pid => $pid}, {order_by => 'naam'});

	my @children;

	while (my $child = $rs->next()) {

		push @children, {
			id => $child->id,
			name => ('-' x $depth) . $child->naam,
			depth => $depth,
			type => 'category'
		};

		push @children, $self->tree($child->id, $depth + 1);
	}

	return @children;
}

1;

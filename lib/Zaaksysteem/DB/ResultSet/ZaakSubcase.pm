package Zaaksysteem::DB::ResultSet::ZaakSubcase;

use Moose;
use Data::Dumper;
use Params::Profile;

use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';


Params::Profile->register_profile(
    method  => 'update_subcase_ids',
    profile => {
        required => [ qw/zaak_id/],
        optional => [ qw/zaaktype_relatie_ids keep_zaaktype_ids/ ],
    }
);
sub update_subcase_ids {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options" . Dumper $dv unless $dv->success;

    die "need zaaktype_relatie_ids" unless("ids: " . $params->{zaaktype_relatie_ids});

    eval {
        $self->result_source->schema->txn_do(sub {
            $self->result_source->resultset->search({
                zaak_id => $params->{zaak_id},
                zaaktype_relatie_id => { 
                    -not_in => $params->{keep_zaaktype_ids} 
                },
            })->delete;

            for my $zaaktype_relatie_id (@{$params->{zaaktype_relatie_ids}}) {        


                my $zaak_subcase = $self->create({
                    zaaktype_relatie_id         => $zaaktype_relatie_id,
                });

                my $zaaktype_relatie = $self->result_source->schema
                    ->resultset('ZaaktypeRelatie')
                    ->find($zaaktype_relatie_id);
            
                die "couldn't load zaaktype_relatie obj for id = $zaaktype_relatie_id" 
                    unless($zaaktype_relatie);
                    
                foreach my $field (qw/eigenaar_type kopieren_kenmerken ou_id role_id/) {
                    warn "field: $field";
                    warn "value: " . $zaaktype_relatie->$field;
                    $zaak_subcase->$field($zaaktype_relatie->$field);
                }

                $zaak_subcase->update();                
            }
        });
    };

    if ($@) {
        warn 'Arguments: ' . Dumper($params);
        die 'update_subcase_ids failed: ' . $@;
    }    

}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::DB::ResultSet::ZaaktypeRelatie;

use strict;
use warnings;

use Moose;
use Data::Dumper;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

use constant    PROFILE => {
    required        => [qw/
    /],
    optional        => [qw/
    /],
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile);
}


sub _commit_session {
    my $self                    = shift;

    ### Remove old authorisations
    my $node                    = shift;
    my $element_session_data    = shift;

    while (my ($key, $data) = each %{ $element_session_data }) {

        ### There is a default in de DB to make can_advance_parent false. But
        ### our commit code will try to insert a NULL. This row is a NOT NULL
        ### constraint, so this throws an error. This is the reason for the
        ### code below. It forces a default when no other value is set.
        if (
            !defined($data->{can_advance_parent}) ||
            !$data->{can_advance_parent}
        ) {
            $data->{can_advance_parent} = 0;
        }
    }

    foreach my $key (keys %$element_session_data) {
        my $item = $element_session_data->{$key};
        # because integers of the format '' are not allowed, null values are cool.
        if(exists $item->{ou_id} && $item->{ou_id} eq '') {
            delete $item->{ou_id};
        }
        $item->{eigenaar_type} ||= 'aanvrager';
    }

    $self->next::method(
        $node,
        $element_session_data,
        {
            status_id_column_name   => 'zaaktype_status_id',
        }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


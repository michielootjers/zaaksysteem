package Zaaksysteem::DB::Component::ZaaktypeNode;

use Moose;

BEGIN { extends 'DBIx::Class::Row'; }

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Data::Serializer;

sub is_huidige_versie {
    my $self    = shift;

    if (
        $self->zaaktype_id &&
        $self->zaaktype_id->zaaktype_node_id->id eq $self->id
    ) {
        return 1;
    }

    return;
}

sub url_title {
    my ($self, $c)  = @_;

    my $title = lc($self->titel);

    $title =~ s/^\s+|\s+$//g;   ## Trim whitespace from front and back
    $title =~ s/\s+/-/;         ## Replace whitespace with dash

    return $title;
}

define_profile rules => (
    optional => ['status'],
    constraint_methods => {
        status => qr/^\d+$/
    }
);

sub rules {
    my ($self, $args) = @_;

    assert_profile($args);
    
    my $rules_rs = $self->zaaktype_regels->search({}, { order_by => 'id' });

    if($args->{status}) {
        my $status_row = $self->zaaktype_statussen->search({
            status  => $args->{status},
        })->first;

        $rules_rs = $rules_rs->search({
            zaak_status_id => $status_row->id,
        });
    }

    return $rules_rs;
}

sub get_steps {
    my $self = shift;
    my $stepnumber = shift;

    my $stati = $self->zaaktype_statuses->search({}, {
        order_by => { -asc => 'id' }
    });

    if($stepnumber) {
        $stati = $stati->search({ status => $stepnumber });
    }

    return map { $_->get_steps } $stati->all;
}

1;

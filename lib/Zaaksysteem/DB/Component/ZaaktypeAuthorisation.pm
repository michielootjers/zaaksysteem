package Zaaksysteem::DB::Component::ZaaktypeAuthorisation;

use strict;
use warnings;

use base qw/DBIx::Class::Row/;

sub security_identity {
    my $self = shift;

    return (
        position => sprintf('%s|%s', $self->ou_id, $self->role_id)
    );
}

1;

package Zaaksysteem::DB::Component::ZaaktypeDefinitie;

use Moose;

BEGIN { extends 'DBIx::Class::Row'; }

sub preset_client_name {
    my $self = shift;

    return '' unless $self->preset_client;

    my $subject = $self->result_source->schema->betrokkene_model->get(
        {},
        $self->preset_client
    );

    return $subject ? $subject->display_name : '';
}

1;

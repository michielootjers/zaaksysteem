package Zaaksysteem::DB::Component::ZaaktypeRegel;

use strict;
use warnings;

use Data::Serializer;
use Data::Dumper;
use DateTime;
use Date::Calendar;
use Date::Calendar::Profiles qw/$Profiles/;
use Zaaksysteem::Constants;
use Zaaksysteem::Geo::BAG;
use Zaaksysteem::Backend::Tools::WorkingDays qw/add_working_days/;

use base qw/DBIx::Class/;


sub as_hashref {
    my ($self) = @_;

         
    my $rule = {
        'naam' => $self->naam 
    };

    my $serializer = Data::Serializer->new(serializer => 'JSON');

    my $deserialized = $serializer->deserialize($self->settings);

    foreach my $key (keys %$deserialized) {
        $rule->{$key} = $deserialized->{$key};
    }

    return $rule;
}


sub execute {
    my ($self, $opts) = @_;
   
    my $kenmerken   = $opts->{kenmerken}    or die "need kenmerken";
    my $result      = $opts->{result}       or die "need result";
    my $casetype    = $opts->{casetype}     or die "need casetype";

    my $voorwaarde_result = $self->_check_conditions($opts);

    if($voorwaarde_result) {
        $self->_execute_actions({
            kenmerken   => $kenmerken,
            which       => 'acties',
            prefix      => 'actie',
            result      => $result,
        });
    } else {
        $self->_execute_actions({
            kenmerken   => $kenmerken,
            which       => 'anders', 
            prefix      => 'ander',
            result      => $result,
        });
    }
}


sub _execute_actions {
    my ($self, $opts) = @_;    

    my $which       = $opts->{which}        or die "need which";
    my $prefix      = $opts->{prefix}       or die "need prefix";
    my $kenmerken   = $opts->{kenmerken}    or die "need kenmerken";
    my $result      = $opts->{result}       or die "need result";

    my $rule        = $self->as_hashref;

    my $acties      = $rule->{$which};
    return unless $acties;

    $acties = $self->_assert_array($acties);

    foreach my $index (@$acties) {

        my $actie = $self->_filter_hash($rule, "${prefix}_${index}_"); 
        my $actie_type = $rule->{"${prefix}_${index}"};

        my $kenmerk_id = $actie->{kenmerk};
        if($actie_type eq 'toon_kenmerk') {
            # unhide. the rules are executed 
            # in order. so the last rule has the last say - and the rules behave predictable
            delete $result->{verberg_kenmerk}->{$kenmerk_id};
        } elsif($actie_type eq 'verberg_kenmerk') {
            $result->{verberg_kenmerk}->{$kenmerk_id} = {actie => 'verberg_kenmerk'};

        } elsif($actie_type eq 'pauzeer_aanvraag') {
            $actie->{actie} = 'pauzeer_aanvraag';

            $result->{pauzeer_aanvraag}->{
                $result->{last_kenmerk}
            } = $actie;

            $actie->{condition_fields} = $self->{condition_fields};

            $result->{pause_application} = $actie;
        } elsif($actie_type eq 'vul_waarde_in') {        
            # enter the value
            if($kenmerk_id eq 'case_result') {
                $result->{set_case_result} = $actie;
            } elsif($kenmerk_id eq 'price') {

                # to sanitize the values, provide some extra help to devs.
                unless ($actie->{value} =~ m|^\d+(\,\d+)?$|) {
                    warn "incorrect price in rules: " . $actie->{value};
                }
                $result->{price} = $actie;
            } else {
                # and make the field read only
                $result->{vul_waarde_in}->{$kenmerk_id} = $actie;
                $kenmerken->{$kenmerk_id} = $actie->{value};
            }
        } elsif($actie_type eq 'wijzig_afhandeltermijn') {
            $result->{wijzig_afhandeltermijn} = {
                termijn => $actie->{afhandeltermijn},
                type    => $actie->{afhandeltermijntype},
            };

        } elsif($actie_type eq 'sjabloon_genereren') {      
            my $templates = $result->{templates} || [];
            push @$templates, $actie->{sjabloon};
            
            $result->{templates} = $templates;
        } elsif($actie_type eq 'start_case') {      

            $result->{cases} ||= [];
            my $cases = $result->{cases};
            push @$cases, $actie->{case_index};
            
        } elsif($actie_type eq 'toewijzing') {
            $result->{toewijzing} = {
                ou_id   => $rule->{ou_id},
                role_id => $rule->{role_id}
            };
        } elsif($actie_type eq 'schedule_mail') {
            $self->execute_schedule_mail_action({
                actie => $actie,
                result => $result,
                kenmerken => $kenmerken,
            });
        } elsif($actie_type eq 'send_external_system_message') {
            $result->{send_external_system_message} = $actie;
        }

    }

}


sub execute_schedule_mail_action {
    my ($self, $options) = @_;

    my $actie   = $options->{actie}     or die "need actie";
    my $result  = $options->{result}    or die "need result";
    my $kenmerken = $options->{kenmerken} or die "need kenmerken";

    unless(
        defined $actie->{datum_bibliotheek_kenmerken_id} &&
        defined $actie->{notificatie_index}
    ) {
        warn "incorrect schedule_mail rule: " . Dumper $actie; 
    }

    my $notificatie_index               = $actie->{notificatie_index};
    my $datum_bibliotheek_kenmerken_id  = $actie->{datum_bibliotheek_kenmerken_id} || '';

    if($datum_bibliotheek_kenmerken_id eq 'phase_transition') {

        $actie->{phase_transition} = 1;

    } else {

        unless ($datum_bibliotheek_kenmerken_id =~ m|^\d+$|) {
           warn "incorrect value in schedule_mail: $datum_bibliotheek_kenmerken_id";
        }

        my $delay                           = $actie->{delay};
        my $delay_type                      = $actie->{delay_type};
        my $direction                       = $actie->{direction};

        $result->{active_fields}->{$datum_bibliotheek_kenmerken_id} = 1;

        my $startdate = $kenmerken->{$datum_bibliotheek_kenmerken_id};
        if($startdate) {

            ($startdate) = @$startdate if ref $startdate && ref $startdate eq 'ARRAY';

            $actie->{send_date} = $self->_calculate_date({
                start           => $startdate,
                addtime         => $delay,
                addtime_type    => $delay_type,
                direction       => $direction,
            });

        } else {
            warn "peildatum is empty, planning mail at phase transition\n";
            $actie->{phase_transition} = 1;
        }
    }

    $result->{schedule_mail}->{$notificatie_index} = $actie;
}


#
# loop through the voorwaarden, if they all are satisfied return 1, otherwise 0.
#
sub _check_conditions {    
    my ($self, $opts) = @_;

    my $kenmerken           = $opts->{kenmerken}            or die "need kenmerken";
    my $aanvrager           = $opts->{aanvrager}            or die "need aanvrager";
    my $contactchannel      = $opts->{contactchannel}       or die "need contactchannel";

    die "need payment_status" unless exists $opts->{payment_status};
    my $payment_status      = $opts->{payment_status};

    my $confidentiality     = $opts->{confidentiality}; #optional, can be non existing or undef
    my $casetype            = $opts->{casetype}             or die "need casetype";

    my $aanvrager_type      = $aanvrager->type              or die "need aanvrager type";
    my $aanvrager_postcode  = $aanvrager->postcode          or die "need aanvrager postcode";
    my $aanvrager_huisnummer = $aanvrager->huisnummer       or die "need aanvrager huisnummer";


    my $result              = $opts->{result}               or die "need result";

    my $regel_obj           = $self->as_hashref;
    my $voorwaarden         = $self->_assert_array($regel_obj->{voorwaarden});

    $result->{active_fields} ||= {};
    my $voorwaarde_result = 1;

    foreach my $voorwaarde_index (@$voorwaarden) {

        my $voorwaarde = $self->_filter_hash($regel_obj, "voorwaarde_${voorwaarde_index}_");

        #warn "voorwaarde $voorwaarde_index: " . Dumper $voorwaarde;
        my $kenmerk = $voorwaarde->{kenmerk};
        $result->{active_fields}->{$kenmerk} = 1;     

        my $kenmerk_values = $result->{verberg_kenmerk}->{$kenmerk} ? [] : $self->_assert_array($kenmerken->{$kenmerk});

        # the real culprit here is mixed types - bibliotheek_kenmerken_id with logical fields like these.
        unless($kenmerk eq 'aanvrager' || $kenmerk eq 'aanvrager_postcode' || $kenmerk eq 'aanvrager_wijk' || $kenmerk eq 'contactchannel') {
            $result->{last_kenmerk} = $kenmerk;
            $self->{condition_fields}->{$kenmerk} += 1;
        }

        if($kenmerk eq 'aanvrager_wijk') {
            $kenmerk_values = $self->parkeergebied({
                huisnummer  => $aanvrager_huisnummer,
                postcode    => $aanvrager_postcode,
            });
        } elsif($kenmerk eq 'payment_status') {
            $kenmerk_values = [$payment_status];
        } elsif($kenmerk eq 'confidentiality') {
            $kenmerk_values = [$confidentiality];
        } elsif($kenmerk ~~ ZAAKSYSTEEM_CONSTANTS->{CASETYPE_RULE_PROPERTIES}) {
            $kenmerk_values = $casetype->properties ? [$casetype->properties->{$kenmerk}] : [];
        }

        # special case: empty value. Assert that no value has been entered 
        # for this kenmerk
        if(!$voorwaarde->{value}) {
            # see that there's minimally one element in the values array that
            # is not an empty string            
            $kenmerk_values = UNIVERSAL::isa($kenmerk_values, 'ARRAY') ? $kenmerk_values : [$kenmerk_values];
            my $joined_values = join '', @$kenmerk_values;

            if(length $joined_values) {
                #$self->log->debug("non empty value found => NOT SATISFIED");
                $voorwaarde_result = 0;
                last;
            } else {
                #$self->log->debug("empty value found => SATISFIED");
                $voorwaarde_result &&= 1;
                next;
            }
        }

        my $voorwaarde_values = { 
            map { $_ => 1 } 
            @{$self->_assert_array($voorwaarde->{value})} 
        };

        if($kenmerk eq 'aanvrager') {
            my $formatted_aanvrager_type = $aanvrager_type eq 'natuurlijk_persoon' ? 
                'Natuurlijk persoon': 
                'Niet natuurlijk persoon';

            $kenmerk_values = [$formatted_aanvrager_type];
        } elsif($kenmerk eq 'aanvrager_postcode') {
            $kenmerk_values = [$aanvrager_postcode];
        } elsif($kenmerk eq 'contactchannel') {
            $kenmerk_values = [$contactchannel];
        } elsif($kenmerk =~ m|(\d+)-wijk|) {
            my $wijk_kenmerk_id = $1;
            $result->{active_fields}->{$wijk_kenmerk_id} = 1;

            $kenmerk_values = $self->wijk({
                bag_value => $kenmerken->{$wijk_kenmerk_id}
            });
        }

        my $zipcode = $self->_extract_zipcode({ values => $kenmerk_values });

        if(
            $zipcode &&
            $voorwaarde->{value_postcode_from} &&
            $voorwaarde->{value_postcode_to}
        ) {
            #warn "checking postcode: " . Dumper $voorwaarde;
            # Special case - check if postcode is between a and b
            if($self->_zipcode_in_range({
                value   => $zipcode,
                from    => $voorwaarde->{value_postcode_from},
                to      => $voorwaarde->{value_postcode_to},
            })) {
                $voorwaarde_result &&= 1;
            } else {
                $voorwaarde_result = 0;
            }
            #warn "voorwaarde result: " . $voorwaarde_result;
            next;
        }

        foreach my $value (@$kenmerk_values) {
            # only one checked checkbox satisfies the voorwaarde
            if(exists $voorwaarde_values->{$value}) {
                $voorwaarde_result &&= 1;
                last;
            }
        }    
        
        if(grep {exists $voorwaarde_values->{$_}} @$kenmerk_values) {
            $voorwaarde_result &&= 1;
        } else {
            # no hits - so it's a no
            $voorwaarde_result = 0;
        }
    }
    #warn "active_fields: " . Dumper $result->{active_fields};
    #warn "voorwaarde result final: " . $voorwaarde_result;
    return $voorwaarde_result;
}


sub _extract_zipcode {
    my ($self, $opts) = @_;
    
    my $values = $opts->{values} or die "need values";
    
    return unless @$values;

    my ($potential_bag_value) = @$values;

    my $dispatch = {
        '^nummeraanduiding-(\d+)$' => sub {
            $self->result_source->schema->resultset('BagNummeraanduiding')->find({
                    identificatie => $1
            })->postcode
        },
        # plain Dutch zipcode
        '^([1-9][0-9]{3}\w{2})$'     => sub {
            $1
        }
    };

    # execute regex keys, if we have a hit, execute its sub to retrieve the matching zip
    map {
        return $dispatch->{$_}->()
            if $potential_bag_value =~ m|$_|s;
    } keys %$dispatch;
    
    return;
}


sub _zipcode_in_range {
    my ($self, $opts) = @_;
    
    my $value   = $opts->{value}    or die "need value";
    my $to      = $opts->{to}       or die "need to";
    my $from    = $opts->{from}     or die "need from";

    my $value_numeric   = $self->_zipcode_to_number({ zipcode => $value });
    my $to_numeric      = $self->_zipcode_to_number({ zipcode => $to    });
    my $from_numeric    = $self->_zipcode_to_number({ zipcode => $from  });
    #warn "value_numeric: " .$value_numeric;
    #warn "to_numeric: " . $to_numeric;
    #warn "from_numeric: " . $from_numeric;
    
    return 
        $value_numeric >= $from_numeric &&
        $value_numeric <= $to_numeric;
}


#
# generate a unique ordered number from a dutch zip
# the letters get a numeric value with trailing zero - a = 00, z = 25
# the numbers are effictively multiplied by 10000
#
# 1000aa => 10000000
# 1000bb => 10000101
# 1000zz => 10002525
# 9999aa => 99990000
# 9999zz => 99992525
#
# now a numeric compare can do the rest
sub _zipcode_to_number {
    my ($self, $opts) = @_;
    
    my $zipcode = $opts->{zipcode} or die "need zipcode";

    $zipcode = uc($zipcode);

    die "incorrect zipcode" 
        unless $zipcode =~ m|^[1-9][0-9]{3}[A-Z]{2}$|;

    # ord = numeric value of a char.
    my $base = ord('A');
    
    $zipcode =~ s|
        ([A-Z])
        |
        sprintf("%02d", ord($1)-$base)
    |gsex;

    return int $zipcode;
}


sub _assert_array {
    my ($self, $value) = @_;
    
    return [] unless $value;
    $value = [$value] unless(ref $value && ref $value eq 'ARRAY');

    return $value;
}


sub _filter_hash {
    my ($self, $source, $filter) = @_;
    
    my $result = {};
    foreach my $key (keys %$source) {
        if($key =~ m|^${filter}(.*)|) { 
            $result->{$1} = $source->{$key};
        }
    }
    return $result;
}



sub _calculate_date {
    my ($self, $options) = @_;

    my $addtime         = $options->{addtime};      # optional
    my $addtime_type    = $options->{addtime_type}; # optional
    my $direction       = $options->{direction};    # optional

    my ($newdate);

    my $dt = DateTime->now;

    if(my $start = $options->{start}) {
        my ($day, $month, $year) = $start =~ m|(\d+)-(\d+)-(\d+)|s;

        $dt = DateTime->new(
            year    => $year,
            month   => $month,
            day     => $day
        );
    }

    return $dt->add(hours => 9) unless $addtime_type;

    # reverse calculations
    if(
        $direction && 
        $direction eq 'minus' && 
        $addtime && $addtime_type ne ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM}
    ) {
        $addtime = -$addtime;
    }

    # on the next overhaul, use Backend::Tools::Term for this.
    my $dispatch_table = {
        ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN} => sub {
            my $resultdate = add_working_days({
                datetime => $dt,
                working_days => $addtime
            });
            $resultdate->add(days => 1); # three days before friday is tuesday
            return $resultdate->add(hours => 9);
        },
        ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN} => sub {
            $dt->add(hours => 9);
            $dt->add(days   => $addtime);
            return $dt;
        },
        ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN} => sub {
            $dt->add(hours => 9);
            $dt->add(weeks   => $addtime);
            return $dt;
        },
        ZAAKSYSTEEM_NAMING->{TERMS_TYPE_MONTHS} => sub {
            $dt->add(hours => 9);
            $dt->add(months   => $addtime);
            return $dt;
        },
        ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM} => sub {
            my ($day, $month, $year) = $addtime =~ /^(\d{2})-(\d{2})-(\d{4})$/;
            return DateTime->new(
                year    => $year,
                month   => $month,
                day     => $day,
                hour    => 9,
            );
        },
        ZAAKSYSTEEM_NAMING->{TERMS_TYPE_MINUTES} => sub {
            return DateTime->now->add(minutes => $addtime);
        },
    };
    return $dt unless exists $dispatch_table->{$addtime_type};

    my $result =  $dispatch_table->{$addtime_type}->();
    return $result;
}

sub parkeergebied  {
    my ($self, $options) = @_;

    # aanvrager may not be postcode/huisnummer enabled
    my $parkeergebied = [];
    eval {
        $parkeergebied = $self->result_source->schema->resultset('Parkeergebied')->find_parkeergebied($options);
    };
    if($@) {
        warn "error during parkeergebied lookup: " . $@;
    }
    return $parkeergebied;
}

sub wijk {
    my ($self, $options) = @_;

    my $bag_value = $options->{bag_value} 
        or return [];

    my $geo_bag_rs = $self->result_source->schema->resultset('BagNummeraanduiding');

    my $address_data;
    if(
        $bag_value =~ m|^nummeraanduiding-\d+$| || 
        $bag_value =~ m|^openbareruimte-\d+$| 
    ) {
        $address_data = $geo_bag_rs->get_address_data_by_source_identifier($bag_value);
    } else {
        my $bag_row = $geo_bag_rs->lookup_nearest_bag_object($bag_value);
        return [] unless $bag_row;

        my $bag_string;
        if($bag_row->can('openbareruimte')) {
            $bag_string = 'openbareruimte-' . $bag_row->openbareruimte->identificatie;
        } elsif($bag_row->can('nummeraanduiding')) {
            $bag_string = 'nummeraanduiding-' . $bag_row->nummeraanduiding->identificatie;
        } else {
            warn "could not find bag string";
            return [];
        }
        $address_data = $geo_bag_rs->get_address_data_by_source_identifier($bag_string);
    }

    return $self->parkeergebied({
        postcode                => $address_data->{postcode},
        huisnummer              => $address_data->{huisnummer},
        huisnummertoevoeging    => $address_data->{huisnummertoevoeging},
        huisletter              => $address_data->{huisletter},
        woonplaats              => $address_data->{woonplaats},
        straatnaam              => $address_data->{straat}, # different naming alert
    });
}


1;

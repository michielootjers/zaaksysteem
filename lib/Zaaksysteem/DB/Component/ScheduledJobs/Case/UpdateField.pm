package Zaaksysteem::DB::Component::ScheduledJobs::Case::UpdateField;

use Data::Dumper;
use Moose::Role;


sub approve {
	my ($self) = @_;

    my $parameters = $self->parameters;
    my $case_id    = $parameters->{case_id};
    my $case       = $self->result_source->schema->resultset('Zaak')->find($case_id);

    my $update_fields_params = {
        new_values  => {
            $parameters->{bibliotheek_kenmerken_id} => $parameters->{value},
        },
        zaak_id     => $case_id,
        message => 'Wijziging goedgekeurd door behandelaar'
    };

    $case->zaak_kenmerken->update_fields($update_fields_params);
    $case->touch();

    # reject soft-deletes the scheduled job, we likey.
    $self->reject();
}


=head2 description

A case field is updated, and this must be recorded in the logs and displayed as a file.
Some fields have different view angles.

=cut

sub description {
	my ($self, $values) = @_;

    return $self->get_bibliotheek_kenmerk->apply_roles->format_as_string($values);
}

sub get_bibliotheek_kenmerk {
	my ($self) = @_;

    my $bibliotheek_kenmerken_id = $self->parameters->{bibliotheek_kenmerken_id};

    my $schema = $self->result_source->schema;

    return $schema->resultset('BibliotheekKenmerken')->find($bibliotheek_kenmerken_id);
}


sub handle_rejection {
	my ($self) = @_;

    return $self->get_bibliotheek_kenmerk->apply_roles->reject_pip_change_request($self->parameters);
}


1;

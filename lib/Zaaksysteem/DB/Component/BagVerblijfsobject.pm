package Zaaksysteem::DB::Component::BagVerblijfsobject;

use strict;
use warnings;

use base qw/Zaaksysteem::DB::Component::BagGeneral/;

sub TO_JSON {
    my $self = shift;

    my $json = {
        $self->get_columns,
        id => 'verblijfsobject-' . $self->id,
    };

    $json->{object_subscription}    = undef;

    if ($self->can('subscription_id') && $self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

1;

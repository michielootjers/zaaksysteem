package Zaaksysteem::DB::Component::ScheduledJobs;


use Data::Dumper;
use DateTime;
use Moose;
use Moose::Util qw/apply_all_roles/;

extends 'DBIx::Class::Row';



sub scheduled_for_utc {
    shift->scheduled_for->set_time_zone('UTC');
}


sub new {
    shift->next::method(@_)->apply_roles;
}


=head2 apply_roles

When expanding this behaviour we can add a more sophisticated system
for finding the suited role. The naming conventions clash a bit, and
it's not worth writing an algorithm for one case.

=cut

sub apply_roles {
    my ($self) = @_;

    if ($self->task eq 'case/update_kenmerk') {
        apply_all_roles($self, __PACKAGE__ . '::Case::UpdateField');
    }
    return $self;
}


sub reject {
    my $self            = shift;

    $self->handle_rejection;

    $self->deleted(DateTime->now());
    $self->update;

}


sub handle_rejection {
    #nop, just here to prevent errors.
}


=head2 description

Default behaviour for displaying values

=cut

sub description {
    my ($self, $values) = @_;

    return @$values;
}


sub format_created_by {
    my ($self) = @_;

    my $created_by = $self->parameters->{created_by} or return 'Aanvrager';
    my ($id) = $created_by =~ m/^betrokkene-medewerker-(\d+)$/;

    die "implement other user types, currently only medewerker is supported" unless $id;

    my $subject = $self->result_source->schema->resultset('Subject')->search({
        id => $id,
        subject_type => 'employee'
    })->first;

    return 'Onbekende behandelaar' unless $subject;
    return $subject->properties->{displayname};
}

1;

package Zaaksysteem::DB::Component::Bedrijf;

use strict;
use warnings;

use Moose;

use Data::Dumper;


extends 'DBIx::Class';





sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_string({insert => 1});
    $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $params  = shift;

    if ($params && UNIVERSAL::isa($params, 'HASH')) {
        for my $key (keys %{ $params }) {
            $self->$key($params->{ $key });
        }
    }

    $self->_set_search_string();
    return $self->next::method(@_);
}


sub _set_search_string {
    my ($self) = @_;


    my $search_string = '';
    
    if($self->handelsnaam) {
        $search_string .= $self->handelsnaam . ' ';
    }
    if($self->dossiernummer) {
        $search_string .= $self->dossiernummer . ' ';
    }

    $self->search_term($search_string);
}

sub TO_JSON {
    my $self = shift;

    my $json = {
        id => $self->id,
        name => $self->handelsnaam,
        $self->get_columns,
    };

    if ($self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

1; #__PACKAGE__->meta->make_immutable;


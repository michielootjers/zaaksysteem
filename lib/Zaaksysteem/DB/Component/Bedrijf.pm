package Zaaksysteem::DB::Component::Bedrijf;
use Moose;

extends 'DBIx::Class';

with qw/Zaaksysteem::BR::Subject::Component/;

=head1 ATTRIBUTES

=head2 display_name

=cut

has 'display_name' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->handelsnaam;
    },
);

=head2 cached_authentication

Caches the related authentication row

=cut

has 'cached_authentication' => (
    is          => 'rw',
    isa         => 'ArrayRef',
);

=head2 bid

Convenience method to get the betrokkene ID of a natural person.

=cut

sub bid {
    my $self = shift;
    return 'betrokkene-bedrijf-' . $self->id;
}

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_string({insert => 1});
    $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $params  = shift;

    if ($params && UNIVERSAL::isa($params, 'HASH')) {
        for my $key (keys %{ $params }) {
            $self->$key($params->{ $key });
        }
    }

    $self->_set_search_string();
    return $self->next::method(@_);
}


sub _set_search_string {
    my ($self) = @_;


    my $search_string = '';

    # Clear search string on deletion
    if ($self->deleted_on) {
        $self->search_term($search_string);
        $self->search_order($search_string);
        return;
    }

    if($self->handelsnaam) {
        $search_string .= $self->handelsnaam . ' ';
    }
    if($self->dossiernummer) {
        $search_string .= $self->dossiernummer . ' ';
    }

    if ($self->vestiging_postcode) {
        $search_string .= $self->vestiging_postcode . ' ' . $self->vestiging_straatnaam;
    } else {
        $search_string .= $self->vestiging_adres_buitenland1 . ' ' . $self->vestiging_adres_buitenland2 . ' ';
    }

    $search_string .= ($self->get_contact_data_search_term || '');

    $self->search_term($search_string);
    $self->search_order($search_string);
}


sub get_contact_data_search_term {
    my $self = shift;

    my $contact_data = $self->result_source->schema->resultset('ContactData')->search({
        gegevens_magazijn_id => $self->id,
        betrokkene_type => 2 # 1 for natuurlijk_persoon, 2 for bedrijf
    })->first;

    return $contact_data ? $contact_data->email : '';
}


sub TO_JSON {
    my $self = shift;

    my $json = {
        id => $self->id,
        name => $self->handelsnaam,
        $self->get_columns,
    };

    if ($self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

1; #__PACKAGE__->meta->make_immutable;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 get_contact_data_search_term

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut


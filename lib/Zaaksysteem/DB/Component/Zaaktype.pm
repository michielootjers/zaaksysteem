package Zaaksysteem::DB::Component::Zaaktype;

use strict;
use warnings;

use Moose;

use Data::Dumper;

extends 'Zaaksysteem::Backend::Component';

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_string({insert => 1});
    my $ret = $self->next::method(@_);

    $self->_sync_object;

    return $ret;
}

sub update {
    my $self    = shift;
    my $columns = shift;
    
    $self->_set_search_string;
    my $ret = $self->next::method(@_);
    
    $self->_sync_object;

    return $ret;
}

sub title {
    my $self = shift;

    if($self->zaaktype_node_id) {
        return $self->zaaktype_node_id->titel;
    }

    return $self->id;
}

sub _set_search_string {
    my ($self) = @_;
    
    my $search_string = ($self->id || '') . ' ';
        
    if($self->zaaktype_node_id) {
    
        if($self->zaaktype_node_id->titel) {
            $search_string .= $self->zaaktype_node_id->titel;
        }

        if($self->zaaktype_node_id->zaaktype_trefwoorden) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_trefwoorden;
        }

        if($self->zaaktype_node_id->zaaktype_omschrijving) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_omschrijving;
        }
    }

    $self->search_term($search_string);
}

=head2 Attributes for L<Zaaksysteem::Backend::Component::Searchable>

=cut

has '_searchable_object_id' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->id; }
);


has '_searchable_object_label' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->zaaktype_node_id->titel; }
);


has '_searchable_object_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->zaaktype_node_id->zaaktype_omschrijving; }
);

=head2 _object

Retrieves the ObjectData object for this Zaaktype.

=cut

sub _object {
    my $self = shift;

    return $self->result_source->schema->resultset('ObjectData')->find_or_create_by_object_id(
        'casetype', $self->id
    );
}

=head2 _sync_object

=cut

sub _sync_object {
    my $self = shift;

    my $object = $self->_object;

    my %map = (
        zaak_edit => 'write',
        zaak_read => 'read'
    );

    $self->result_source->schema->txn_do(sub {
        $object->object_acl_entries->delete_all;

        for my $auth ($self->zaaktype_authorisations) {
            next unless exists $map{ $auth->recht };

            $object->grant($auth, $map{ $auth->recht });
        }
    });

    return $object;
}

=head2 JSON Extension for L<Zaaksysteem::Backend::Component>

=cut

before 'TO_JSON' => sub {
    my $self                    = shift;

    $self->_json_data(
        {
            %{ $self->_json_data },
            $self->get_columns,
        }
    );

    $self->_json_data->{zaaktype_node_id}   = { $self->zaaktype_node_id->get_columns };

    my $title = $self->seo_friendly_title;

    $self->_json_data->{public_url} = {
        person       => '/aanvragen/' . $self->id . '/persoon/' . $title,
        organisation => '/aanvragen/' . $self->id . '/organisatie/' . $title,
        unknown      => '/aanvragen/' . $self->id . '/onbekend/' . $title,
    };
};


=head2 seo_friendly_title

Format the title with only a-z, 0-9 and substitute a dash for anything else

=cut

sub seo_friendly_title {
    my $self = shift;
    my $title = lc $self->zaaktype_node_id->titel;

    $title =~ s/[^a-z0-9]+/-/gi;

    return $title;
}

=head2 set_active_node

Every zaaktype links to one zaaktype_node record. This system is used to allow multiple versions
of the zaaktype to exist. This method changes the active zaaktype_node. Every other node
must be disabled.

my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);
$zaaktype->set_active_node($zaaktype_node_id);

=cut

sub set_active_node {
    my ($self, $zaaktype_node_id) = @_;

    die "need zaaktype_node_id" unless $zaaktype_node_id;

    my $schema = $self->result_source->schema;

    my $zaaktype_node_rs = $schema->resultset('ZaaktypeNode');
    die "new zaaktype_node_id does not exist" unless $zaaktype_node_rs->find($zaaktype_node_id);

    eval {
        $schema->txn_do(sub {
            # set currently node to deleted
            $self->zaaktype_node_id->deleted(DateTime->now());
            $self->zaaktype_node_id->update();

            $self->zaaktype_node_id($zaaktype_node_id);
            $self->update();

            # le roi est mort, vive le roi!
            $self->zaaktype_node_id->deleted(undef);
            $self->zaaktype_node_id->update();
        });
    };

    if ($@) {
        die "could update version: $@";
    }
}


1; #__PACKAGE__->meta->make_immutable;


package Zaaksysteem::DB::Component::ChecklistItem;

use Moose;

use JSON;

BEGIN { extends 'DBIx::Class'; }

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        label => $self->label_with_deprecated_answer,
        user_defined => $self->user_defined ? JSON::true : JSON::false,
        checked => $self->state ? JSON::true : JSON::false
    }
}

sub label_with_deprecated_answer {
    my $self = shift;

    my $label = $self->get_column('label');
    my $deprecated_answer = $self->get_column('deprecated_answer');

    return $deprecated_answer ? sprintf('%s (%s)', $label, $deprecated_answer) : $label;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 label_with_deprecated_answer

TODO: Fix the POD

=cut


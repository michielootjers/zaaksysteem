package Zaaksysteem::DB::Component::ContactData;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub TO_JSON {
    my $self = shift;

    my $data = {
        id => $self->id,
        email_addresses => [
            $self->email
        ],
        #created => $self->created->datetime,
        #last_modified => $self->last_modified->datetime,
    };

    my @phonenumbers = ();

    for my $key ('mobiel', 'telefoonnummer') {
        if($self->$key) {
            push(@phonenumbers, {
                type => $key eq 'mobiel' ? 'mobile' : 'landline',
                number => $self->$key
            });
        }
    }

    $data->{ phonenumbers } = [ @phonenumbers ];
    $data->{ identifier } = $self->identifier;

    if($self->natural_person) {
        $data->{ natural_person } = $self->natural_person;
    }

    if($self->non_natural_person) {
        $data->{ non_natural_person } = $self->non_natural_person;
    }

    return $data;
}

sub identifier {
    my $self = shift;

    if($self->natural_person) {
        return sprintf('betrokkene-natuurlijk_persoon-%d', $self->gegevens_magazijn_id);
    }

    if($self->non_natural_person) {
        return sprintf('betrokkene-bedrijf-%d', $self->gegevens_magazijn_id);
    }

    return;
}

sub natural_person {
    my $self = shift;

    if($self->betrokkene_type == 1) {
        return $self->result_source->schema->resultset('NatuurlijkPersoon')->find($self->gegevens_magazijn_id);
    }

    return;
}

sub non_natural_person {
    my $self = shift;

    if($self->betrokkene_type == 2) {
        return $self->result_source->schema->resultset('Bedrijf')->find($self->gegevens_magazijn_id);
    }

    return;
}

1;

package Zaaksysteem::DB::Component::Logging::Case::Document::Remove;

use Moose::Role;

sub onderwerp {
    sprintf('Document "%s" definitief verwijderd', shift->data->{ file_name });
}

1;

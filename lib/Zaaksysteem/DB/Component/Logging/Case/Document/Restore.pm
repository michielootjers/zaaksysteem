package Zaaksysteem::DB::Component::Logging::Case::Document::Restore;

use Moose::Role;

sub onderwerp {
    sprintf('Document "%s" hersteld uit de prullenbak.', shift->file->name);
}

1;

package Zaaksysteem::DB::Component::Logging::Case::Document::Reject;

use Moose::Role;
use Data::Dumper;

sub onderwerp {
    my $self = shift;

    my $message = $self->data->{reject_to_queue} ?
        'Document "'. $self->data->{filename} .'" geweigerd en teruggeplaatst naar de documentintake.' :
        'Document "' . $self->data->{filename} . '" geweigerd en permanent verwijderd."';

    return $message . ($self->data->{rejection_reason} ? " Reden: " . $self->data->{rejection_reason} : '');
}

1;

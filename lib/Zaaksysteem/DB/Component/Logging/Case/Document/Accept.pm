package Zaaksysteem::DB::Component::Logging::Case::Document::Accept;

use Moose::Role;

sub onderwerp {
    sprintf('Document "%s" geaccepteerd', shift->file->name);
}

1;

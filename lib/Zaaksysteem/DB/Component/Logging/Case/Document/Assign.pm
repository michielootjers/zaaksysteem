package Zaaksysteem::DB::Component::Logging::Case::Document::Assign;

use Moose::Role;

sub onderwerp {
    sprintf('Document "%s" toegevoegd aan wachtrij', shift->file->filename);
}

1;

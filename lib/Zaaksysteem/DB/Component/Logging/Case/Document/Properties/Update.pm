package Zaaksysteem::DB::Component::Logging::Case::Document::Properties::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    if($self->data->{ updated_properties}{ accepted }) {
        return sprintf(
            'Document "%s" geaccepteerd',
            $self->file->name
        );
    }

    return sprintf(
        'Document "%s" aangepast',
        $self->file->name
    );
}

1;

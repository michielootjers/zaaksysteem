package Zaaksysteem::DB::Component::Logging::Case::Document::Create;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp') || sprintf('Document "%s" toegevoegd', $self->file->name);
}

1;

package Zaaksysteem::DB::Component::Logging::Case::Publish;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return "Zaak gepubliceerd.";
}

sub onderwerp_extended {
	my $self = shift;

    return $self->onderwerp . "<br/><strong>Unpublish</strong>: " .
       $self->htmlify($self->data->{unpublish}) . "<br/><strong>Publish</strong>: " .
       $self->htmlify($self->data->{publish});
}

sub htmlify {
    my ($self, $xml) = @_;

    $xml =~ s|\<|&lt;|gis;
    $xml =~ s|\>|&gt;|gis;

    return '<pre class="logging-code">' . $xml . '</pre>';
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ content } = "Unpublish:\n" . $self->data->{unpublish} .
        "\n\nPublish: " . $self->data->{publish};

    $data->{ expanded } = JSON::false;

    return $data;
};

1;

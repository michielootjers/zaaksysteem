package Zaaksysteem::DB::Component::Logging::Case::Duplicated;

use Moose::Role;

has duplicated_from => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaak')->find($self->data->{ duplicated_from });
});

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak gekopie&euml;rd van zaak %d (%s)',
        $self->duplicated_from->id,
        $self->duplicated_from->zaaktype_node_id->titel
    );
}

1;

package Zaaksysteem::DB::Component::Logging::Case::PIP::RejectUpdate;

use Moose::Role;

sub onderwerp {
    sprintf('Kenmerk aanpassingen "%s" afgewezen', shift->data->{ reason });
}

1;

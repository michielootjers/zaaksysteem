package Zaaksysteem::DB::Component::Logging::Case::Pip::Updatefield;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my $new_values = $self->data->{ new_values };

    my $description = "Aanvrager heeft een wijziging voorgesteld voor kenmerk '" .
    	$self->data->{kenmerk} . "' met de volgende waarde: '".
		(ref $new_values ? join (", ", @$new_values) : $new_values) . "'";

    if ($self->data->{reason}) {
        $description .= '. Toelichting: ' . $self->data->{ reason };
    }

    return $description;
}

1;
package Zaaksysteem::DB::Component::Logging::Case::Update::Department;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak %s gewijzigd naar afdeling: %s - %s',
        $self->data->{ case_id },
        $self->data->{ ou_dn },
        $self->data->{ role_dn }
    );
}

sub event_category { 'case-mutation'; }

1;

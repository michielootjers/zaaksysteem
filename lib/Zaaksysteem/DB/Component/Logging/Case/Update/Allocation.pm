package Zaaksysteem::DB::Component::Logging::Case::Update::Allocation;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return sprintf(
        'Toewijzing gewijzigd naar afdeling "%s" en rol "%s"',
        $self->data->{ ou_name },
        $self->data->{ role_name }
    );
}

1;

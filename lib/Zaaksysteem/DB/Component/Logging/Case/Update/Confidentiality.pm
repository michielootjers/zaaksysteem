package Zaaksysteem::DB::Component::Logging::Case::Update::Confidentiality;

use Moose::Role;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

sub onderwerp {
    my $self = shift;

    my $old = ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$self->data->{old}};
    my $new = ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$self->data->{new}};

    return 'Vertrouwelijkheid gewijzigd van "' . $old . '" naar "' . $new . '"';
}

sub event_category { 'case-mutation'; }

1;

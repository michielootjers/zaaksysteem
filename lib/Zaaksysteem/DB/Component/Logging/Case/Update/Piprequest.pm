package Zaaksysteem::DB::Component::Logging::Case::Update::Piprequest;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my $new_values = $self->data->{ new_values };

    my $description = "Behandelaar heeft wijzigingsvoorstel " .
        ($self->data->{ action } eq 'approve' ? 'goedgekeurd' : 'afgekeurd') . 
    	" voor kenmerk '" .
    	$self->data->{kenmerk} . "' met de volgende waarde: '".
		(ref $new_values ? join (", ", @$new_values) : $new_values) . "'";

    return $description;
}


1;

package Zaaksysteem::DB::Component::Logging::Case::Update::TargetDate;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf('Streefafhandeldatum voor zaak: %d gewijzigd naar: %s',
        $self->data->{ case_id } // 0,
        $self->data->{ target_date } // '<onbekend>'
    );
}

sub event_category { 'case-mutation'; }

1;

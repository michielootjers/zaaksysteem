package Zaaksysteem::DB::Component::Logging::Case::Update::Relation;

use Moose::Role;

use Data::Dumper;

sub onderwerp {
    my $self = shift;

    return sprintf(
        $self->data->{ deleted } ? 'Relatie met zaak %s (%s) verbroken' : 'Zaak %s (%s) gerelateerd',
        $self->related_case->id,
        $self->related_case->zaaktype_node_id->titel
    );
}

has related_case => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->result_source->schema->resultset('Zaak')->find($self->data->{ relation_id });
});

sub event_category { 'case-mutation'; }

1;

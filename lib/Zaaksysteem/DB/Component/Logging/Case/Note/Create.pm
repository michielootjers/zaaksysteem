package Zaaksysteem::DB::Component::Logging::Case::Note::Create;

use Moose::Role;
use Data::Dumper;

sub onderwerp {
    'Notitie toegevoegd'
}

sub event_category { 'note' }

sub creator_from_betrokkene {
    my $self = shift;

    return '' unless($self->betrokkene_id);

    my $betrokkene  = $self
                    ->result_source
                    ->schema
                    ->betrokkene_model
                    ->get({}, $self->betrokkene_id);

    return 'Onbekend' unless $betrokkene;

    return $betrokkene->naam;
}

around 'creator' => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_) || $self->creator_from_betrokkene;
};

1;

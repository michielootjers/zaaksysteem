package Zaaksysteem::DB::Component::Logging::Case::SendExternalSystemMessage;

use Moose::Role;
use JSON;


sub onderwerp {
    my $self = shift;

    return 'Bericht verstuurd naar ' . ($self->data->{destination} || 'extern systeem');
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ content } = "Request:\n" . $self->data->{ input } . "\n\nResponse:\n". $self->data->{output};
    $data->{ expanded } = JSON::false;

    return $data;
};

sub event_category { 'case-mutation'; }

1;

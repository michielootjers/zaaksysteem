package Zaaksysteem::DB::Component::Logging::Case::Reopen;

use Moose::Role;

sub onderwerp {
    sprintf('Zaak %s heropend.', shift->data->{ case_id });
}

1;

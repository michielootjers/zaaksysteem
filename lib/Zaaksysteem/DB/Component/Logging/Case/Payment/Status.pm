package Zaaksysteem::DB::Component::Logging::Case::Payment::Status;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return sprintf(
        decode_action($self->data->{ status_code }),
        $self->data->{ amount }
    );
}

sub decode_action {
    my $status = shift;

    my %lookup = (
        'pending' => 'Transactie gestart (bedrag: &euro;%s)',
        'failed' => 'Transactie mislukt.',
        'success' => 'Transactie geslaagd (bedrag: &euro;%s)'
    );

    return $lookup{ $status } || $status;
}

sub event_category { 'case-mutation'; }

1;

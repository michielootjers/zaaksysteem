package Zaaksysteem::DB::Component::Logging::Case::Attribute::Removebulk;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my $onderwerp = sprintf(
        'Kenmerken "%s" verwijderd: %s.',
        $self->data->{ attributes },
        $self->data->{ reason }
    );

    # db field = varchar(255)
    # we could make a larger field in the db but it has a big impact,
    # need to consider performance, disk space and migrate.
    # for a limit.
    #
    # also this fix should be applied to the parent.
    #
    if (length $onderwerp > 255) {
        $onderwerp = substr($onderwerp, 0, 252) . '...';
    }

    return $onderwerp;
}

sub event_category { 'case-mutation'; }

1;

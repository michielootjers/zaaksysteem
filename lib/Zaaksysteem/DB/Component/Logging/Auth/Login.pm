package Zaaksysteem::DB::Component::Logging::Auth::Login;

use Moose::Role;

has subject => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Subject')->find($self->data->{ subject_id });
});

sub onderwerp {
    my $self = shift;

    return sprintf('Gebruiker "%s" ingelogd.', ($self->subject ? $self->subject->cn : ''));
}

sub event_category { 'system'; }

1;

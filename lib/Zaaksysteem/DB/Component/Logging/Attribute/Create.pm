package Zaaksysteem::DB::Component::Logging::Attribute::Create;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp') unless $self->attribute;

    sprintf(
        'Kenmerk "%s" toegevoegd: %s',
        $self->attribute->naam,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    my $self = shift;

    $self->meta->add_attribute('attribute' => (is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        return $self->result_source->schema->resultset('BibliotheekKenmerken')->find($self->data->{ attribute_id });
    }));
}

1;

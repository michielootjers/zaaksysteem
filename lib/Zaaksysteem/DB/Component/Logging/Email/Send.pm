package Zaaksysteem::DB::Component::Logging::Email::Send;

use Data::Dumper;
use Moose::Role;
use HTML::Strip;

use Zaaksysteem::Constants;

has case => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaak')->find($self->data->{ case_id });
});

sub onderwerp {
    my $self = shift;

    my ($subject) = HTML::Strip->new->parse($self->data->{ subject }) =~ m/^\s*(.*?)\s*$/;

    sprintf('E-mail "%s" verstuurd naar "%s"', $subject, $self->data->{ recipient });
}


=head2 export_as_pdf

Returns a PDF version the e-mail. This is a binary stream that is intended
to be downloaded directly to the browser. At this point there's no requirement
for storing it.

=cut

sub export_as_pdf {
    my $self = shift;

    my $data = $self->data;

    my $tmpDir = $self->rs('Config')->get_value('tmp_location');
    my $outlookMailParser = Zaaksysteem::OutlookMailParser->new({tmpDir => $tmpDir});

    my $gemeente = $self->result_source->schema->default_resultset_attributes->{config}->{gemeente};

    my $formatted = $outlookMailParser->format({
        to => $data->{recipient},
        from => $gemeente->{naam} . " <" . $gemeente->{zaak_email} . ">",
        subject => $data->{subject},
        body => $data->{content},
        attachments => $data->{attachments}
    });

    my $converted = $self->rs('Filestore')->send_to_converter(
        Content      => $formatted,
        Content_Type =>  MIMETYPES_ALLOWED->{'.txt'}{mimetype},
        Accept       => 'application/pdf'
    );

    return $converted;
}

sub event_category { 'contactmoment' }

1;

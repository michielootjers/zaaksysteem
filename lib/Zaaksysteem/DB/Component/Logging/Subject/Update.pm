package Zaaksysteem::DB::Component::Logging::Subject::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf('Betrokkene "%s" geupdated', $self->subject->display_name);

    # sprintf(
    #     'Betrokkene "%s" geupdated',
    #     $self->subject->naam,
    #     join(', ',
    #         map { sprintf('%s: %s', $_, $self->data->{ parameters }{ $_ }) }
    #         grep { $_ =~ m|^np| }
    #         keys %{ $self->data->{ parameters } }
    #     )
    # );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('subject' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->betrokkene_model->get({}, $self->data->{ subject_id });
    }));
}

1;

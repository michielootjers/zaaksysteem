package Zaaksysteem::DB::Component::Logging::Subject::Create;

use Moose::Role;
use Data::Dumper;

sub onderwerp {
    my $self        = shift;

    return $self->get_column('onderwerp') unless $self->subject;

    sprintf('Betrokkene "%s" toegevoegd', $self->subject->naam);
}

sub _add_magic_attributes {
    shift->meta->add_attribute('subject' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->betrokkene_model->get(
            {},
            $self->data->{ subject_id }
        );
    }));
}

1;

package Zaaksysteem::DB::Component::Logging::Subject::Stufimport;

use Moose::Role;

sub onderwerp {
    my $self    = shift;

    return sprintf(
        'Succesfully imported subject from CMG (sleutel: %d): %s',
        $self->data->{system_of_record_id},
        $self->subject->naam
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('subject' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->betrokkene_model->get(
            {},
            $self->data->{ subject_id }
        );
    }));
}

1;

package Zaaksysteem::DB::Component::Logging::File::Remove;

use Moose;

sub onderwerp {
    sprintf('Document "%s" definitief verwijderd.', shift->data->{ file_name });
}

1;

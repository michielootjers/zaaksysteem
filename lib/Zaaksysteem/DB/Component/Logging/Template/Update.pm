package Zaaksysteem::DB::Component::Logging::Template::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp') unless $self->template;

    sprintf(
        'Sjabloon "%s" opgeslagen: %s',
        $self->template->naam,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('template' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('BibliotheekSjablonen')->find($self->data->{ template_id });
    }));
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut


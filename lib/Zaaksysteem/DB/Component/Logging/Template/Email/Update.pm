package Zaaksysteem::DB::Component::Logging::Template::Email::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp') unless $self->template;

    sprintf(
        'E-mailsjabloon "%s" opgeslagen: %s',
        $self->template->label,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('template' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('BibliotheekNotificaties')->find($self->data->{ template_id });
    }));
}

1;

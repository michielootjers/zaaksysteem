package Zaaksysteem::DB::Component::Logging::Template::Email::Create;

use Moose::Role;

has template => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('BibliotheekNotificaties')->find($self->data->{ template_id });
});

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp') unless $self->template;

    sprintf(
        'E-mailsjabloon "%s" toegevoegd: %s',
        $self->template->label,
        $self->data->{ reason }
    );
}

1;

package Zaaksysteem::DB::Component::BibliotheekKenmerken::Qmatic;

use Data::Dumper;
use DateTime::Format::Strptime;
use Moose::Role;
use DateTime::Format::DateParse;


=head2 format_as_string

A case field is updated, and this must be recorded in the logs and displayed as a file.
A QMatic value looks like "day;time;id", where day and time are ISO8601 datetimes.

=cut

sub format_as_string {
	my ($self, $values) = @_;

	return map { $self->format_qmatic($_) } @$values;
}


sub format_qmatic {
	my ($self, $value) = @_;

	my $qmatic_value = $self->parse_qmatic_value($value);

	return "Afspraak geboekt op: ". format_date($qmatic_value->{appDate}) .
		' om ' . format_time($qmatic_value->{appTime});
}


sub parse_qmatic_value {
	my ($self, $value) = @_;

	my ($appDate, $appTime, $appointmentId) = split /;/, $value;

	return {
		appDate 		=> $appDate,
		appTime 		=> $appTime,
		appointmentId 	=> $appointmentId
	};
}

=head2 format_date

parse something that looks like  '2013-12-26T12:00:00.000'

return '26 december 2013' - Dutch locale

=cut

sub format_date {
	my ($date) = @_;

	my $dt = DateTime::Format::DateParse->parse_datetime($date);

	$dt->set_locale('nl_NL');
	$dt->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam');

	return $dt->day . ' '. $dt->month_name . ' ' . $dt->year;
}

=head2 format_time

parse something that looks like : '1970-01-01T13:20:00.000'

return '14:20'

=cut

sub format_time {
	my ($time) = @_;

 	my $strp = DateTime::Format::Strptime->new(
	    pattern   => '%z',
	    time_zone => 'UTC',
	    locale    => 'nl_NL',
	);

	my $dt = DateTime::Format::DateParse->parse_datetime($time);

	$dt->set_locale('nl_NL');
	$dt->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam');

	return $dt->strftime('%H:%M');
}


=head2 reject_pip_change_request

From the QMatic documentation ('Q-MATIC Calendar Web Service API - Documenation V3.0.PDF'):
(misspelling of documentation is accurate)

Delete an existing appointment through the appointment ID
Input
	appointmentId Integer Unique appointment Id
Output
	An AppointmentStatusType element containing;
	appointmentId Integer This will always be '0'
	appointmentStatus Integer
		Integer representing an appointment status code. Status codes:
		1 = Appointment Deleted
		5 = An unknown error occurred.

=cut

sub reject_pip_change_request {
	my ($self, $parameters) = @_;

	my $qmatic_value = $self->parse_qmatic_value($parameters->{value});

	if ($qmatic_value->{appointmentId}) {

	    my $schema = $self->result_source->schema;

	    my $qmatic_interface = $schema->resultset('Interface')->search_active({ module => 'qmatic' })->first
		        or die "need qmatic interface";

		my $result = $qmatic_interface->process_trigger('deleteAppointment', {
			appointmentId => $qmatic_value->{appointmentId}
		});

		$schema->resultset('Logging')->trigger('case/pip/rejectupdate', {
            component => 'case',
            component_id  => $parameters->{case_id},
            zaak_id       => $parameters->{case_id},
            betrokkene_id => undef,
            data => {
            	reason => 'QMatic afspraak nummer ' . $qmatic_value->{appointmentId} . ' verwijderd'
            }
        });

		return $result && @$result && $result->[0]->{appointmentStatus} eq '1';
	}
}


1;

package Zaaksysteem::DB::Component::BagGeneral;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub TO_JSON {
    my $self                = shift;

    my $json                = $self->next::method(@_);

    # For lack of a better place to 'join' this information: look up the Object Subscription
    # here.
    $json->{object_subscription}    = undef;

    if ($self->can('subscription_id') && self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

1;

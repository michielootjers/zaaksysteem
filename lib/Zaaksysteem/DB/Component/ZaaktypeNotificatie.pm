package Zaaksysteem::DB::Component::ZaaktypeNotificatie;

use strict;
use warnings;

use Data::Dumper;
use base qw/DBIx::Class/;

#
# since 2.7.0 ZaaktypeNotificaties use a BibliotheekNotificatie for storage of 
# subject, label and message body. there's a conversion script in /bin, also
# with import of a zaaktype that is not aware, this conversion needs to be performed.
#
# look for a matching bibliotheek_notificatie in the database, enter it's id in
# bibliotheek_notificaties_id.
#
sub import_bibliotheek_notificatie {
    my ($self) = @_;

    # already set
    if(
        $self->bibliotheek_notificaties_id &&
        $self->bibliotheek_notificaties_id->id
    ) {
        warn "for " . $self->id . " the bibliotheek_notificaties_id is already set to " . $self->bibliotheek_notificaties_id->id;
        return;
    }

    my $bibliotheek_notificatie = $self->result_source->schema->resultset("BibliotheekNotificaties")->find({
        label       => $self->label,
        subject     => $self->onderwerp,
        message     => $self->bericht,
    });
    
    warn "zaaktype_notificatie id = " . $self->id;
    
    unless(
        $self->zaaktype_node_id &&
        $self->zaaktype_node_id->zaaktype_id &&
        $self->zaaktype_node_id->zaaktype_id->bibliotheek_categorie_id &&
        $self->zaaktype_node_id->zaaktype_id->bibliotheek_categorie_id->id
    ) {
        warn "bibliotheek_categorie not found";
        next;
    }

    my $bibliotheek_categorie_id = $self->zaaktype_node_id->zaaktype_id->bibliotheek_categorie_id->id;
    die "wtf" unless($bibliotheek_categorie_id);
    
    unless($bibliotheek_notificatie) { 
    
        $bibliotheek_notificatie = $self->result_source->schema->resultset("BibliotheekNotificaties")->create({
            label                       => $self->label,
            subject                     => $self->onderwerp,
            message                     => $self->bericht,
            bibliotheek_categorie_id    => $bibliotheek_categorie_id,
        });
    }
    
    die "wtf" unless ($bibliotheek_notificatie && $bibliotheek_notificatie->id);

    $self->bibliotheek_notificaties_id($bibliotheek_notificatie->id);
    $self->update();
}



=head2 case_document_attachments

a bibliotheek_notificatie can have attachments. these attachments are related to
file kenmerken - bibliotheek_kenmerken. these are useful only if these are included
in the current casetype.

so here's the procedure for listing these:
- see if the bibliotheek_notificatie has attachments
- see if the attachments's bibliotheek_kenmerken are used in this casetype


=cut

sub case_document_attachments {
    my ($self) = @_;

    my $notificatie_kenmerken = $self->bibliotheek_notificaties_id
                                     ->bibliotheek_notificatie_kenmerks
                                     ->search;

    my $rs = $self->zaaktype_node_id->zaaktype_kenmerkens->search({
        bibliotheek_kenmerken_id => {
            -in => $notificatie_kenmerken->get_column('bibliotheek_kenmerken_id')->as_query
        }
    },{
        prefetch => 'bibliotheek_kenmerken_id'
    });

    return [map {{
        selected          => 1,
        name              => $_->bibliotheek_kenmerken_id->naam,
        case_document_ids => $_->id  #zaaktype_kenmerken_id
    }} $rs->all];
}

1;

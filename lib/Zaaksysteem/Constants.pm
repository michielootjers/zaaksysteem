package Zaaksysteem::Constants;

use strict;
use warnings;

use DateTime;
use DateTime::Format::Strptime qw(strptime);
use Scalar::Util qw/blessed/;
use Data::Dumper;

use utf8;

require Exporter;
our @ISA        = qw/Exporter/;
our @EXPORT     = qw/
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_NAMING
    ZAAKSYSTEEM_OPTIONS
    ZAAKTYPE_DB_MAP

    ZAAKTYPE_PREFIX_SPEC_KENMERK
    ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE
    ZAAKTYPE_KENMERKEN_DYN_DEFINITIE

    GEGEVENSMAGAZIJN_GBA_PROFILE
    GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES

    GEGEVENSMAGAZIJN_KVK_PROFILE
    GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES

    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS

    SJABLONEN_EXPORT_FORMATS
    SJABLONEN_TARGET_FORMATS

    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR

    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE

    ZAAKSYSTEEM_STANDAARD_KENMERKEN
    ZAAKSYSTEEM_STANDAARD_KOLOMMEN
    ZAAKSYSTEEM_BETROKKENE_KENMERK
    ZAAKSYSTEEM_BETROKKENE_SUB

    ZAAKSYSTEEM_LOGGING_LEVELS
    LOGGING_COMPONENT_ZAAK
    LOGGING_COMPONENT_USER
    LOGGING_COMPONENT_ZAAKTYPE
    LOGGING_COMPONENT_CATEGORIE
    LOGGING_COMPONENT_NOTITIE
    LOGGING_COMPONENT_BETROKKENE
    LOGGING_COMPONENT_KENMERK
    LOGGING_COMPONENT_SJABLOON
    LOGGING_COMPONENT_NOTIFICATIE
    LOGGING_COMPONENT_DOCUMENT
    LOGGING_COMPONENT_KENNISBANK_VRAGEN
    LOGGING_COMPONENT_KENNISBANK_PRODUCTEN


    LDAP_DIV_MEDEWERKER

    STUF_VERSIONS
    STUF_XSD_PATH
    STUF_XML_URL

    DEFAULT_KENMERKEN_GROUP_DATA

    ZAKEN_STATUSSEN
    ZAKEN_STATUSSEN_DEFAULT

    SEARCH_QUERY_SESSION_VAR
    SEARCH_QUERY_TABLE_NAME

    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
    ZAAKSYSTEEM_CONTACTKANAAL_BALIE

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA

    ZAAK_CREATE_PROFILE
    ZAAKTYPE_DEPENDENCY_IDS
    ZAAKTYPE_DEPENDENCIES

    BETROKKENE_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION

    VERNIETIGINGS_REDENEN
    ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE
    ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE
    ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE

    PARAMS_PROFILE_MESSAGES_SUB

    ZAAKSYSTEEM_NAAM
    ZAAKSYSTEEM_OMSCHRIJVING
    ZAAKSYSTEEM_LEVERANCIER
    ZAAKSYSTEEM_STARTDATUM
    ZAAKSYSTEEM_LICENSE
    PARAMS_PROFILE_DEFAULT_MSGS

    DOCUMENTS_STORE_TYPE_NOTITIE
    DOCUMENTS_STORE_TYPE_FILE
    DOCUMENTS_STORE_TYPE_MAIL
    DOCUMENTS_STORE_TYPE_JOB

    FILE_STORE_LOCATION

    BETROKKENE_TYPE_BEDRIJF
    BETROKKENE_TYPE_NATUURLIJK_PERSOON
    BETROKKENE_TYPE_MEDEWERKER

    OBJECTSEARCH_TABLENAMES
    OBJECTSEARCH_TABLE_ORDER

    OBJECT_ACTIONS

    PROFILE_KENNISBANK_PRODUCT_BEWERKEN
    PROFILE_KENNISBANK_VRAAG_BEWERKEN

    EDE_PUBLICATION_STRINGS

    STATUS_LABELS
    MIMETYPES_ALLOWED

    ALLOW_NO_HTML
    ALLOW_ONLY_TRUSTED_HTML

    JOBS_INFORMATION_MAP

    STRONG_RELATED_COLUMNS
    ZAAK_EMPTY_COLUMNS

    EVENT_TYPE_GROUPS

    DENIED_BROWSERS
    FRIENDLY_BETROKKENE_MESSAGES

    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING

    SUBJECT_TYPE_EMPLOYEE

    CHILD_CASETYPE_OPTIONS

    ZAAKTYPE_NODE_TITLE
/;

our @EXPORT_OK = qw(
    BAG_TYPES
    PROFILE_BAG_TYPES_OK

    SERVICE_NORM_TYPES
    SERVICE_NORM_TYPES_OK

    ZAAKTYPE_ATTRIBUTEN

    ZAAKSYSTEEM_BOFH

    ZAAKTYPE_TRIGGER
    ZAAK_CREATE_PROFILE_BETROKKENE
);

### DO NOT FREAKING TOUCH ;)
### {
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID       => 'digid';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID   => 'bedrijfid';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA         => 'gba';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK         => 'kvk';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR => 'behandelaar';

use constant ZAAKTYPE_PREFIX_SPEC_KENMERK   => 'spec';

use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM      => 0.2;
use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH        => 0.1;
use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE        => 1;

use constant ZAAKSYSTEEM_LOGGING_LEVEL_DEBUG            => 'debug';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_INFO             => 'info';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_WARN             => 'warn';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_ERROR            => 'error';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_FATAL            => 'fatal';

use constant ZAAKSYSTEEM_LOGGING_LEVELS                 => {
    ZAAKSYSTEEM_LOGGING_LEVEL_DEBUG     => 1,
    ZAAKSYSTEEM_LOGGING_LEVEL_INFO      => 2,
    ZAAKSYSTEEM_LOGGING_LEVEL_WARN      => 3,
    ZAAKSYSTEEM_LOGGING_LEVEL_ERROR     => 4,
    ZAAKSYSTEEM_LOGGING_LEVEL_FATAL     => 5
};

use constant LOGGING_COMPONENT_USER         => 'user';
use constant LOGGING_COMPONENT_ZAAK         => 'zaak';
use constant LOGGING_COMPONENT_ZAAKTYPE     => 'zaaktype';
use constant LOGGING_COMPONENT_CATEGORIE    => 'bibliotheek_categorie';
use constant LOGGING_COMPONENT_NOTITIE      => 'notitie';

use constant LOGGING_COMPONENT_BETROKKENE   => 'betrokkene';
use constant LOGGING_COMPONENT_KENMERK      => 'kenmerk';
use constant LOGGING_COMPONENT_SJABLOON     => 'sjabloon';
use constant LOGGING_COMPONENT_NOTIFICATIE  => 'notificatie';
use constant LOGGING_COMPONENT_DOCUMENT     => 'document';
use constant LOGGING_COMPONENT_KENNISBANK_VRAGEN        => 'kennisbank_vragen';
use constant LOGGING_COMPONENT_KENNISBANK_PRODUCTEN     => 'kennisbank_producten';


use constant ZAKEN_STATUSSEN                => [ qw/new open stalled resolved deleted overdragen/ ];
use constant ZAKEN_STATUSSEN_DEFAULT        => 'new';

use constant ZAAKSYSTEEM_NAAM               => 'zaaksysteem.nl';
use constant ZAAKSYSTEEM_OMSCHRIJVING       => 'Het zaaksysteem.nl is een '
                                                .'complete oplossing '
                                                .'(all-in-one) voor '
                                                .'gemeenten om de '
                                                .'dienstverlening te '
                                                .'verbeteren.';
use constant ZAAKSYSTEEM_LEVERANCIER        => 'Mintlab B.V.';
use constant ZAAKSYSTEEM_STARTDATUM         => '01-10-2009';
use constant ZAAKSYSTEEM_LICENSE            => 'EUPL';

### } END DO NOT FREAKING TOUCH

use constant ZAAKSYSTEEM_NAMING     => {
    TRIGGER_EXTERN                              => 'extern',
    TRIGGER_INTERN                              => 'intern',
    AANVRAGER_TYPE_NATUURLIJK_PERSOON           => 'natuurlijk_persoon',
    AANVRAGER_TYPE_NATUURLIJK_PERSOON_NA        => 'natuurlijk_persoon_na',
    AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON      => 'niet_natuurlijk_persoon',
    AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON_NA   => 'niet_natuurlijk_persoon_na',
    AANVRAGER_TYPE_MEDEWERKER                   => 'medewerker',
    AANVRAGER_ADRES_TYPE_ADRES                  => 'aanvrager_adres',
    AANVRAGER_ADRES_TYPE_ANDERS                 => 'anders',
    WEBFORM_TOEGANG                             => 'webform_toegang',
    WEBFORM_AUTHENTICATIE_AAN                   => 'authenticatie',
    WEBFORM_AUTHENTICATIE_OPTIONEEL             => 'optie',
    HANDELINGSINITIATOR_AANGAAN                 => 'aangaan',
    HANDELINGSINITIATOR_AANGEVEN                => 'aangeven',
    HANDELINGSINITIATOR_AANMELDEN               => 'aanmelden',
    HANDELINGSINITIATOR_AANVRAGEN               => 'aanvragen',
    HANDELINGSINITIATOR_AFKOPEN                 => 'afkopen',
    HANDELINGSINITIATOR_AFMELDEN                => 'afmelden',
    HANDELINGSINITIATOR_INDIENEN                => 'indienen',
    HANDELINGSINITIATOR_INSCHRIJVEN             => 'inschrijven',
    HANDELINGSINITIATOR_MELDEN                  => 'melden',
    HANDELINGSINITIATOR_OPZEGGEN                => 'opzeggen',
    HANDELINGSINITIATOR_REGISTREREN             => 'registreren',
    HANDELINGSINITIATOR_RESERVEREN              => 'reserveren',
    HANDELINGSINITIATOR_STELLEN                 => 'stellen',
    HANDELINGSINITIATOR_VOORDRAGEN              => 'voordragen',
    HANDELINGSINITIATOR_VRAGEN                  => 'vragen',
    HANDELINGSINITIATOR_ONTVANGEN               => 'ontvangen',
    HANDELINGSINITIATOR_AANSCHRIJVEN            => 'aanschrijven',
    HANDELINGSINITIATOR_VASTSTELLEN             => 'vaststellen',
    HANDELINGSINITIATOR_VERSTUREN               => 'versturen',
    HANDELINGSINITIATOR_UITVOEREN               => 'uitvoeren',
    HANDELINGSINITIATOR_OPSTELLEN               => 'opstellen',
    HANDELINGSINITIATOR_STARTEN                 => 'starten',
    OPENBAARHEID_OPENBAAR                       => 'openbaar',
    OPENBAARHEID_GESLOTEN                       => 'gesloten',
    TERMS_TYPE_KALENDERDAGEN                    => 'kalenderdagen',
    TERMS_TYPE_WEKEN                            => 'weken',
    TERMS_TYPE_WERKDAGEN                        => 'werkdagen',
    TERMS_TYPE_EINDDATUM                        => 'einddatum',
    TERMS_TYPE_MINUTES                          => 'minuten', # minutes from now, for testing agendering
    TERMS_TYPE_MONTHS                           => 'maanden',
    RESULTAATINGANG_VERVALLEN                   => 'vervallen',
    RESULTAATINGANG_ONHERROEPELIJK              => 'onherroepelijk',
    RESULTAATINGANG_AFHANDELING                 => 'afhandeling',
    RESULTAATINGANG_VERWERKING                  => 'verwerking',
    RESULTAATINGANG_GEWEIGERD                   => 'geweigerd',
    RESULTAATINGANG_VERLEEND                    => 'verleend',
    RESULTAATINGANG_GEBOORTE                    => 'geboorte',
    RESULTAATINGANG_EINDE_DIENSTVERBAND         => 'einde-dienstverband',
    DOSSIERTYPE_DIGITAAL                        => 'digitaal',
    DOSSIERTYPE_FYSIEK                          => 'fysiek',
};


use constant ZAAKSYSTEEM_OPTIONS    => {
    'RESULTAATINGANGEN'   => [
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERVALLEN},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_ONHERROEPELIJK},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_AFHANDELING},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERWERKING},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_GEWEIGERD},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERLEEND},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_GEBOORTE},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_EINDE_DIENSTVERBAND},
    ],
    'DOSSIERTYPE'       => [
        ZAAKSYSTEEM_NAMING->{DOSSIERTYPE_DIGITAAL},
        ZAAKSYSTEEM_NAMING->{DOSSIERTYPE_FYSIEK}
    ],
    'BEWAARTERMIJN'     => {
        62      => '3 maanden',
        365     => '1 jaar',
        548     => '1,5 jaar',
        730     => '2 jaar',
        1095    => '3 jaar',
        1460    => '4 jaar',
        1825    => '5 jaar',
        2190    => '6 jaar',
        2555    => '7 jaar',
        2920    => '8 jaar',
        3285    => '9 jaar',
        3650    => '10 jaar',
        4015    => '11 jaar',
        4380    => '12 jaar',
        4745    => '13 jaar',
        5110    => '14 jaar',
        5475    => '15 jaar',
        7300    => '20 jaar',
        10950   => '30 jaar',
        14600   => '40 jaar',
        40150   => '110 jaar',
        99999   => 'Bewaren',
    },
    WEBFORM_AUTHENTICATIE   => [
        ZAAKSYSTEEM_NAMING->{WEBFORM_AUTHENTICATIE_AAN},
        ZAAKSYSTEEM_NAMING->{WEBFORM_AUTHENTICATIE_OPTIONEEL},
    ],
    TRIGGERS                => [
        ZAAKSYSTEEM_NAMING->{TRIGGER_EXTERN},
        ZAAKSYSTEEM_NAMING->{TRIGGER_INTERN},
    ],
    AANVRAGERS_INTERN       => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NATUURLIJK_PERSOON},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NATUURLIJK_PERSOON_NA},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON_NA},
    ],
    AANVRAGERS_EXTERN       => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_MEDEWERKER},
    ],
    AANVRAGER_ADRES_TYPEN   => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_ADRES_TYPE_ADRES},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_ADRES_TYPE_ANDERS},
    ],
    HANDELINGSINITIATORS    => [
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANGEVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANMELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANVRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AFKOPEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AFMELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_INDIENEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_INSCHRIJVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_MELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_RESERVEREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_STELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VOORDRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_ONTVANGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANSCHRIJVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VASTSTELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_UITVOEREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_OPSTELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_STARTEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_OPZEGGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANGAAN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_REGISTREREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VERSTUREN},
        ],
    OPENBAARHEDEN           => [
        ZAAKSYSTEEM_NAMING->{OPENBAARHEID_OPENBAAR},
        ZAAKSYSTEEM_NAMING->{OPENBAARHEID_GESLOTEN},
    ],
    ARCHIEFNOMINATIE_OPTIONS => {
        bewaren => 'Bewaren (B)',
        vernietigen => 'Vernietigen (V)',
        overbrengen => 'Overbrengen (O)'
    },
    RESULTAATTYPEN => [
        'aangehouden',
        'aangekocht',
        'aangesteld',
        'aanvaard',
        'afgeboekt',
        'afgebroken',
        'afgehandeld',
        'afgesloten',
        'afgewezen',
        'akkoord',
        'akkoord met wijzigingen',
        'behaald',
        'betaald',
        'buiten behandeling gesteld',
        'definitief toegekend',
        'geannuleerd',
        'gedeeltelijk verleend',
        'gedoogd',
        'gegrond',
        'gegund',
        'geweigerd',
        'gewijzigd',
        'geïnd',
        'ingeschreven',
        'ingesteld',
        'ingetrokken',
        'niet aangekocht',
        'niet aangesteld',
        'niet akkoord',
        'niet behaald',
        'niet betaald',
        'niet gegund',
        'niet gewijzigd',
        'niet geïnd',
        'niet ingesteld',
        'niet ingetrokken',
        'niet nodig',
        'niet ontvankelijk',
        'niet opgeleverd',
        'niet toegekend',
        'niet vastgesteld',
        'niet verleend',
        'niet verwerkt',
        'ongegrond',
        'ontvankelijk',
        'opgeheven',
        'opgeleverd',
        'opgelost',
        'opgezegd',
        'toegekend',
        'uitgevoerd',
        'vastgesteld',
        'verhuurd',
        'verkocht',
        'verleend',
        'vernietigd',
        'verstrekt',
        'verwerkt',
        'voorlopig toegekend',
        'voorlopig verleend',
    ],
};


use constant ZAAKTYPE_DB_MAP    => {
    'kenmerken'                     => {
        'id'            => 'id',
        'naam'          => 'key',
        'label'         => 'label',
        'type'          => 'value_type',
        'omschrijving'  => 'description',
        'help'          => 'help',
        #'value'         => 'value'             # Value of kenmerken_values
                                                # FOR: ztc
#        'magicstring'   => 'magicstring',
    },
    'kenmerken_values'              => {
        'value'         => 'value',
    },
};

use constant ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE   => [
    {
        'naam'          => 'zaaktype_id',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_nid',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_code',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'categorie_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'categorie_id',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'mogelijke_aanvragers',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'trigger',
        'in_node'       => 1,
    },
    {
        'naam'          => 'webform_authenticatie',
        'in_node'       => 1,
    },
    {
        'naam'          => 'adres_relatie',
        'in_node'       => 1,
    },
    {
        'naam'          => 'handelingsinitiator'
    },
    {
        'naam'          => 'grondslag',
    },
    {
        'naam'          => 'selectielijst',
    },
    {
        'naam'          => 'afhandeltermijn',
    },
    {
        'naam'          => 'afhandeltermijn_type',
    },
    {
        'naam'          => 'servicenorm',
    },
    {
        'naam'          => 'servicenorm_type',
    },
    {
        'naam'          => 'openbaarheid',
    },
    {
        'naam'          => 'procesbeschrijving',
    },
];

use constant ZAAKTYPE_KENMERKEN_DYN_DEFINITIE   => [
    {
        'naam'          => 'status',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'bag_items',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'streefafhandeldatum',
    },
    {
        'naam'          => 'contactkanaal',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'behandelaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaakeigenaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'org_eenheid',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_verificatie',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_geslachtsnaam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_telefoon',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_mobiel',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_email',
        'in_rt_only'    => 1,
    },
    { naam => 'gebruiker_naam', in_rt_only => 1 },
    {
        'naam'          => 'registratiedatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'afhandeldatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'vernietigingsdatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'besluit',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'bezwaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'locatie',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'depend_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'vroegtijdig_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'registratiedatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'urgentiedatum_high',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'urgentiedatum_medium',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'route_ou_role',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'opgeschort_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'resultaat',
        'in_rt_only'    => 1,
    },
];

use constant 'GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES'     => [qw/
    straatnaam
    huisnummer
    postcode
    huisnummertoevoeging
    huisletter
    woonplaats
    functie_adres
    gemeente_code
/];

use constant PARAMS_PROFILE_MESSAGES_SUB                => sub {
    my $dfv     = shift;
    my $rv      = {};

    for my $missing ($dfv->missing) {
        $rv->{$missing}  = 'Veld is verplicht.';
    }
    for my $missing ($dfv->invalid) {
        $rv->{$missing}  = 'Veld is niet correct ingevuld.';
    }

    return $rv;
};

use constant 'GEGEVENSMAGAZIJN_GBA_PROFILE'     => {
    'missing_optional_valid'  => 1,
    'required'      => [qw/
        straatnaam
        huisnummer
        postcode

        burgerservicenummer
        geslachtsnaam
        geslachtsaanduiding
        geboortedatum
    /],
    'optional'      => [qw/
        voornamen
        huisnummertoevoeging
        huisletter
        woonplaats
        geboorteplaats
        geboorteland
        a_nummer

        voorletters
        voorvoegsel
        nationaliteitscode1
        nationaliteitscode2
        nationaliteitscode3
        geboortegemeente_omschrijving
        geboorteregio
        aanhef_aanschrijving
        voorletters_aanschrijving
        voornamen_aanschrijving
        naam_aanschrijving
        voorvoegsel_aanschrijving
        burgerlijke_staat
        indicatie_gezag
        indicatie_curatele
        indicatie_geheim
        aanduiding_verblijfsrecht
        datum_aanvang_verblijfsrecht
        datum_einde_verblijfsrecht
        aanduiding_soort_vreemdeling
        land_vanwaar_ingeschreven
        land_waarnaar_vertrokken
        adres_buitenland1
        adres_buitenland2
        adres_buitenland3
        nnp_ts
        hash
        import_datum
        adres_id
        email
        telefoon
        geboortegemeente
        authenticated
        authenticatedby

        aanduiding_naamgebruik
        functie_adres
        onderzoek_persoon
        onderzoek_persoon_ingang
        onderzoek_persoon_einde
        onderzoek_persoon_onjuist
        onderzoek_huwelijk
        onderzoek_huwelijk_ingang
        onderzoek_huwelijk_einde
        onderzoek_huwelijk_onjuist
        onderzoek_overlijden
        onderzoek_overlijden_ingang
        onderzoek_overlijden_einde
        onderzoek_overlijden_onjuist
        onderzoek_verblijfplaats
        onderzoek_verblijfplaats_ingang
        onderzoek_verblijfplaats_einde
        onderzoek_verblijfplaats_onjuist

        datum_overlijden

        partner_a_nummer
        partner_voorvoegsel
        partner_geslachtsnaam
        partner_burgerservicenummer
        datum_huwelijk
        datum_huwelijk_ontbinding

        gemeente_code
        system_of_record_id
    /],
    constraint_methods => {
        'geslachtsaanduiding'   => qr/^[MV]$/,
    },
    field_filters     => {
        'burgerservicenummer'   => sub {
            my ($field) = @_;

            return $field if length($field) == 9;

            return sprintf("%09d", $field);
        },
        'postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'huisnummer'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^\d+$/;

            return $field;
        },
        'huisnummertoevoeging'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\w\d\s-]+$/;

            return $field;
        },
        'geboortedatum'             => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\d-]+$/;

            if ($field =~ /^\d{8}$/) {
                my ($year, $month, $day) = $field =~ /^(\d{4})(\d{2})(\d{2})$/;

                $month  = 1 if $month    < 1;
                $day    = 1 if $day      < 1;

                my $dt;

                eval {
                    $dt      = DateTime->new(
                        'year'          => $year,
                        'month'         => $month,
                        'day'           => $day,
                        #'time_zone'     => 'Europe/Amsterdam',
                    );
                };

                if ($@) {
                    $dt = undef;
                }

                return $dt;
            } elsif ($field =~ /^(\d{2})-(\d{2})-(\d{4})$/) {
                my ($day, $month, $year) = $field =~
                    /^(\d{2})-(\d{2})-(\d{4})$/;

                $month  = 1 if $month    < 1;
                $day    = 1 if $day      < 1;

                my $dt;

                eval {
                    $dt      = DateTime->new(
                        'year'          => $year,
                        'month'         => $month,
                        'day'           => $day,
                        #'time_zone'     => 'Europe/Amsterdam',
                    );
                };

                if ($@) {
                    $dt = undef;
                }

                return $dt;
            }

            return undef;
        },
        'geslachtsaanduiding'       => sub {
            my ($field) = @_;

            return $field unless $field =~ /^[mMvV]$/;

            return uc($field);
        },
        'partner_burgerservicenummer' => sub {
            my ($field) = @_;

            return '' if $field =~ /^[0 ]+$/;

            return $field;
        },
    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    },
};

use constant GEGEVENSMAGAZIJN_KVK_PROFILE   => {
    missing_optional_valid  => 1,
    required => [ qw/
        dossiernummer
        subdossiernummer

        hoofdvestiging_dossiernummer
        hoofdvestiging_subdossiernummer

        handelsnaam

        vestiging_adres
        vestiging_straatnaam
        vestiging_huisnummer
        vestiging_postcode
        vestiging_postcodewoonplaats
        vestiging_woonplaats

    /],
    optional => [ qw/
        fulldossiernummer
        vorig_dossiernummer
        vorig_subdossiernummer

        vestigingsnummer

        vestiging_huisnummertoevoeging

        rechtsvorm
        telefoonnummer
        surseance
        kamernummer

        correspondentie_adres
        correspondentie_straatnaam
        correspondentie_huisnummer
        correspondentie_huisnummertoevoeging
        correspondentie_postcodewoonplaats
        correspondentie_postcode
        correspondentie_woonplaats

        hoofdactiviteitencode
        nevenactiviteitencode1
        nevenactiviteitencode2

        werkzamepersonen

        contact_naam
        contact_aanspreektitel
        contact_voorletters
        contact_geslachtsnaam
        contact_voorvoegsel
        contact_geslachtsaanduiding

        email
        system_of_record_id
    /],
    constraint_methods => {
        'dossiernummer'                     => qr/^\d{8}$/,
        'subdossiernummer'                  => qr/^\d{4}$/,
        'vestigingsnummer'                  => qr/^\d{12}$/,
        'hoofdvestiging_dossiernummer'      => qr/^\d{8}$/,
        'hoofdvestiging_subdossiernummer'   => qr/^\d{4}$/,

        'handelsnaam'                       => qr/^.{0,45}$/,
        'rechtsvorm'                        => qr/^\d{0,3}$/,

        'hoofdactiviteitencode'             => qr/^\d{0,6}$/,
        'nevenactiviteitencode1'            => qr/^\d{0,6}$/,
        'nevenactiviteitencode2'            => qr/^\d{0,6}$/,

        'vestiging_adres'                   => qr/^.{0,30}$/,
        'vestiging_straatnaam'              => qr/^.{0,25}$/,
        'vestiging_huisnummer'              => qr/^\d{0,6}$/,
        'vestiging_huisnummertoevoeging'    => qr/^.{0,12}$/,
        'vestiging_postcodewoonplaats'      => qr/^.{0,30}$/,
        'vestiging_postcode'                => qr/^[\d]{4}[\w]{2}$/,
        'vestiging_woonplaats'              => qr/^.{0,24}$/,

        'correspondentie_adres'                 => qr/^.{0,30}$/,
        'correspondentie_straatnaam'            => qr/^.{0,25}$/,
        'correspondentie_huisnummer'            => qr/^.{0,6}$/,
        'correspondentie_huisnummertoevoeging'  => qr/^.{0,12}$/,
        'correspondentie_postcodewoonplaats'    => qr/^.{0,30}$/,
        'correspondentie_postcode'              => qr/^[\d]{4}[\w]{2}$/,
        'correspondentie_woonplaats'            => qr/^.{0,24}$/,
    },
    defaults => {
        vestiging_adres => sub {
            my ($dfv) = @_;

            return
                $dfv->get_filtered_data->{'vestiging_straatnaam'} . ' ' .
                $dfv->get_filtered_data->{'vestiging_huisnummer'} .
                ($dfv->get_filtered_data->{'vestiging_huisnummertoevoeging'}
                    ?  ' ' .  $dfv->get_filtered_data->{'vestiging_huisnummertoevoeging'}
                    : ''
                );
        },
        vestiging_postcodewoonplaats => sub {
            my ($dfv) = @_;

            return
                $dfv->get_filtered_data->{'vestiging_postcode'} . ' ' .
                $dfv->get_filtered_data->{'vestiging_woonplaats'};
        },
        correspondentie_adres => sub {
            my ($dfv) = @_;

            return undef unless    $dfv
                                ->get_filtered_data
                                ->{'correspondentie_straatnaam'};

            return
                $dfv->get_filtered_data->{'correspondentie_straatnaam'} . ' ' .
                $dfv->get_filtered_data->{'correspondentie_huisnummer'} .
                ($dfv->get_filtered_data->{'correspondentie_huisnummertoevoeging'}
                    ?  ' ' .  $dfv->get_filtered_data->{'correspondentie_huisnummertoevoeging'}
                    : ''
                );
        },
#        telefoonnummer => sub {
#            my ($dfv) = @_;
#
#            return
#                ($dfv->get_input_data->{'telefoonnummer_netnummer'} || '') . '-' .
#                ($dfv->get_input_data->{'telefoonnummer_nummer'} || '');
#        },
        fulldossiernummer => sub {
            my ($dfv) = @_;

            return unless $dfv->get_filtered_data->{'dossiernummer'};

            return
                ($dfv->get_filtered_data->{'dossiernummer'} || '') .
                ($dfv->get_filtered_data->{'subdossiernummer'} || '');
        },
        correspondentie_postcodewoonplaats => sub {
            my ($dfv) = @_;

            return unless (
                $dfv->get_filtered_data->{'correspondentie_postcode'} &&
                $dfv->get_filtered_data->{'correspondentie_woonplaats'}
            );

            return
                $dfv->get_filtered_data->{'correspondentie_postcode'} . ' ' .
                $dfv->get_filtered_data->{'correspondentie_woonplaats'};
        },
        subdossiernummer => '0001',
        hoofdvestiging_dossiernummer => sub {
            return $_[0]->get_filtered_data->{dossiernummer};
        },
        hoofdvestiging_subdossiernummer => sub {
            return $_[0]->get_filtered_data->{subdossiernummer} || '0001';
        },

    },
    field_filters     => {
        'werkzamepersonen'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'kamernummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'rechtsvorm'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'vestiging_huisnummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'surseance'  => sub {
            my ($field) = @_;

            if (lc($field) eq 'y') {
                return 1;
            } else {
                return 0;
            }

            return $field;
        },
        'telefoonnummer'  => sub {
            my ($field) = @_;

            $field =~ s/\-//;
            $field =~ s/ //;

            return substr($field, 0, 10);
        },
        'vestiging_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'correspondentie_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
    },
    msgs    => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES   => {
    '01'    => 'Eenmanszaak',
    '02'    => 'Eenmanszaak met meer dan één eigenaar',
    '03'    => 'N.V./B.V. in oprichting op A-formulier',
    '05'    => 'Rederij',
    '07'    => 'Maatschap',
    11    => 'Vennootschap onder firma',
    12    => 'N.V/B.V. in oprichting op B-formulier',
    21    => 'Commanditaire vennootschap met een beherend vennoot',
    22    => 'Commanditaire vennootschap met meer dan één beherende vennoot',
    23    => 'N.V./B.V. in oprichting op D-formulier',
    40    => 'Rechtspersoon in oprichting',
    41    => 'Besloten vennootschap met gewone structuur',
    42    => 'Besloten vennootschap blijkens statuten structuurvennootschap',
    51    => 'Naamloze vennootschap met gewone structuur',
    52    => 'Naamloze vennootschap blijkens statuten structuurvennootschap',
    53    => 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal',
    54    => 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap',
    55    => 'Europese naamloze vennootschap (SE) met gewone structuur',
    56    => 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap',
    61    => 'Coöperatie U.A. met gewone structuur',
    62    => 'Coöperatie U.A. blijkens statuten structuurcoöperatie',
    63    => 'Coöperatie W.A. met gewone structuur',
    64    => 'Coöperatie W.A. blijkens statuten structuurcoöperatie',
    65    => 'Coöperatie B.A. met gewone structuur',
    66    => 'Coöperatie B.A. blijkens statuten structuurcoöperatie',
    70    => 'Vereniging van eigenaars',
    71    => 'Vereniging met volledige rechtsbevoegdheid',
    72    => 'Vereniging met beperkte rechtsbevoegdheid',
    73    => 'Kerkgenootschap',
    74    => 'Stichting',
    81    => 'Onderlinge waarborgmaatschappij U.A. met gewone structuur',
    82    => 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge',
    83    => 'Onderlinge waarborgmaatschappij W.A. met gewone structuur',
    84    => 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge',
    85    => 'Onderlinge waarborgmaatschappij B.A. met gewone structuur',
    86    => 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge',
    88    => 'Publiekrechtelijke rechtspersoon',
    89    => 'Privaatrechtelijke rechtspersoon',
    91    => 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland',
    92    => 'Nevenvest. met hoofdvest. in buitenl.',
    93    => 'Europees economisch samenwerkingsverband',
    94    => 'Buitenl. EG-venn. met onderneming in Nederland',
    95    => 'Buitenl. EG-venn. met hoofdnederzetting in Nederland',
    96    => 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland',
    97    => 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland',

    ### BUSSUM SPECIFIC
    201    => 'Coöperatie',
    202    => 'Vereniging',
};

### Contactkanalen
use constant ZAAKSYSTEEM_CONTACTKANAAL_BALIE        => 'balie';
use constant ZAAKSYSTEEM_CONTACTKANAAL_TELEFOON     => 'telefoon';
use constant ZAAKSYSTEEM_CONTACTKANAAL_POST         => 'post';
use constant ZAAKSYSTEEM_CONTACTKANAAL_EMAIL        => 'email';
use constant ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM      => 'webformulier';
use constant ZAAKSYSTEEM_CONTACTKANAAL_BEHANDELAAR  => 'behandelaar';

### Hoofd en deelzaken
use constant ZAAKSYSTEEM_SUBZAKEN_DEELZAAK          => 'deelzaak';
use constant ZAAKSYSTEEM_SUBZAKEN_GERELATEERD       => 'gerelateerd';
use constant ZAAKSYSTEEM_SUBZAKEN_VERVOLGZAAK       => 'vervolgzaak';

use constant CASE_PAYMENT_STATUS_FAILED             => 'failed';
use constant CASE_PAYMENT_STATUS_SUCCESS            => 'success';
use constant CASE_PAYMENT_STATUS_PENDING            => 'pending';

use constant ZAAKSYSTEEM_CONSTANTS  => {
    'zaaksysteem_about' => [ qw/
        applicatie
        omschrijving
        leverancier
        versie
        licentie
        startdatum
    /],
    'mimetypes'         => {
        'default'                                   => 'icon-txt-32.gif',
        'dir'                                       => 'icon-folder-32.gif',
        'application/msword'                        => 'icon-doc-32.gif',
        'application/pdf'                           => 'icon-pdf-32.gif',
        'application/msexcel'                       => 'icon-xls-32.gif',
        'application/vnd.ms-excel'                  => 'icon-xls-32.gif',
        'application/vnd.ms-powerpoint'             => 'icon-ppt-32.gif',
        'text/email'                                => 'icon-email-32.gif',
        'image/jpeg'                                => 'icon-jpg-32.gif',
        'application/vnd.oasis.opendocument.text'   => 'icon-odt-32.gif',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'icon-doc-32.gif',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'icon-ppt-32.gif',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'icon-xls-32.gif',
    },
    'zaken_statussen'   => ZAKEN_STATUSSEN,
    'contactkanalen'    => [
        ZAAKSYSTEEM_CONTACTKANAAL_BEHANDELAAR,
        ZAAKSYSTEEM_CONTACTKANAAL_BALIE,
        ZAAKSYSTEEM_CONTACTKANAAL_TELEFOON,
        ZAAKSYSTEEM_CONTACTKANAAL_POST,
        ZAAKSYSTEEM_CONTACTKANAAL_EMAIL,
        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
    ],
    'payment_statuses'  => {
        CASE_PAYMENT_STATUS_FAILED() => 'Niet geslaagd',
        CASE_PAYMENT_STATUS_SUCCESS() => 'Geslaagd',
        CASE_PAYMENT_STATUS_PENDING() => 'Wachten op bevestiging'
    },
    'subzaken_deelzaak'         => ZAAKSYSTEEM_SUBZAKEN_DEELZAAK,
    'subzaken_gerelateerd'      => ZAAKSYSTEEM_SUBZAKEN_GERELATEERD,
    'subzaken_vervolgzaak'      => ZAAKSYSTEEM_SUBZAKEN_VERVOLGZAAK,
    'authenticatedby'   => {
        'digid'         => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID,
        'bedrijfid'     => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID,
        'behandelaar'   => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
        'gba'           => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA,
        'kvk'           => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK
    },
    'mail_rcpt' => {
        'aanvrager'     => {
            naam            => 'Aanvrager',
        },
        'coordinator'   => {
            naam            => 'Coordinator',
        },
        'behandelaar'   => {
            naam            => 'Behandelaar',
            in_status       => 1,
        },
        'overig'        => {
            naam            => 'Overig',
            in_status       => 1,
        },
    },
    'zaaktype'  => {
        'zt_trigger'    => {
            extern  => 'Extern',
            intern  => 'Intern',
        },
        'betrokkenen'   => {
            'niet_natuurlijk_persoon'       => 'Niet natuurlijk persoon',
            'niet_natuurlijk_persoon_na'    => 'Niet natuurlijk persoon (Ongeauthoriseerd)',
            'natuurlijk_persoon'            => 'Natuurlijk persoon',
            'natuurlijk_persoon_na'         => 'Natuurlijk persoon (Ongeauthoriseerd)',
            'medewerker'                    => 'Behandelaar',
            'org_eenheid'                   => 'Organisatorische eenheid',
        },
        'deelvervolg_eigenaar'  => {
            'behandelaar'   => {
                'label'     => 'Behandelaar van de huidige zaak',
            },
            'aanvrager'     => {
                'label'     => 'Aanvrager van de huidige zaak',
            },
        }
    },
    'veld_opties' => {
        'email' => {
            'label'              => 'E-mail',
            'object_search_type' => 'text',
        },
        'url' => {
            'label'               => 'Webadres',
            'allow_default_value' => 1,
            'object_search_type'  => 'text',
        },
        'text' => {
            'label'                    => 'Tekstveld',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'richtext' => {
            'label'                    => 'Rich text',
            'allow_multiple_instances' => 0,
            'object_search_type'  => 'text',
        },
        'image_from_url' => {
            'label'                    => 'Afbeelding (URL)',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'text_uc' => {
            'label'                    => 'Tekstveld (HOOFDLETTERS)',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'text',
        },
        'numeric' => {
            'label'                    => 'Numeriek',
            'rt'                       => 'Freeform-1',
            'constraint'               => qr/^\d*$/,
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
        },
        'valuta' => {
            'label'                    => 'Valuta',
            'rt'                       => 'Freeform-1',
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'valutain' => {
            'label'                    => 'Valuta (inclusief BTW)',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'valutaex' => {
            'label'                    => 'Valuta (exclusief BTW)',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'valutain6' => {
            'label'                    => 'Valuta (inclusief BTW (6))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'valutaex6' => {
            'label'                    => 'Valuta (exclusief BTW (6))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'valutain21' => {
            'label'                    => 'Valuta (inclusief BTW (21))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'valutaex21' => {
            'label'                    => 'Valuta (exclusief BTW (21))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'integer',
            'object_search_filter'     => \&_valuta_fix_filter,
        },
        'date' => {
            'label'                    => 'Datum',
            'rt'                       => 'Freeform-1',
            'type'                     => 'datetime',
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'timestamp',
            'object_search_filter'     => sub {
                my ($schema, $value) = @_;

                return if not $value;
                return strptime("%d-%m-%Y", $value);
            },
        },
        'googlemaps' => {
            'label'               => 'Adres (Google Maps)',
            'can_zaakadres'       => 1,
            'rt'                  => 'Freeform-1',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'textarea' => {
            'label'                    => 'Groot tekstveld',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'option' => {
            'multiple'            => 1,
            'label'               => 'Enkelvoudige keuze',
            'rt'                  => 'Freeform-1',
            'rtmultiple'          => 0,
            'allow_default_value' => 1,
            'object_search_type'  => 'text',
        },
        'select' => {
            'multiple'                 => 1,
            'label'                    => 'Keuzelijst',
            'rt'                       => 'Freeform-1',
            'rtmultiple'               => 0,
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'checkbox' => {
            'multiple'            => 1,
            'label'               => 'Meervoudige keuze',
            'rt'                  => 'Freeform-0',
            'rtmultiple'          => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'file' => {
            'label'               => 'Document',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'calendar' => {
            'label'               => 'Kalender afspraak',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'bag_straat_adres' => {
            'label'               => 'Adres (dmv straatnaam) (BAG)',
            'object_search_type'  => 'text',
            'rt'                  => 'Freeform-1',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                return $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_straat_adressen' => {
            'multiple'            => 1,
            'label'               => 'Adressen (dmv straatnaam) (BAG)',
            'rt'                  => 'Freeform-0',
            'rtmultiple'          => 1,
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                return $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_adres' => {
            'label'               => 'Adres (dmv postcode) (BAG)',
            'rt'                  => 'Freeform-1',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_adressen' => {
            'label'               => 'Adressen (dmv postcode) (BAG)',
            'rt'                  => 'Freeform-0',
            'multiple'            => 1,
            'rtmultiple'          => 1,
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_openbareruimte' => {
            'label'               => 'Straat (BAG)',
            'rt'                  => 'Freeform-1',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_openbareruimtes' => {
            'label'               => 'Straten (BAG)',
            'rt'                  => 'Freeform-0',
            'multiple'            => 1,
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
    },
    'document'      => {
        'categories'        => [qw/
            Advies
            Afbeelding
            Audio
            Begroting
            Behandelvoorstel
            Beleidsnota
            Besluit
            Bewijsstuk
            Brief
            Contract
            Document
            E-mail
            Envelop
            Factuur
            Formulier
            Foto
            Legitimatie
            Memo
            Offerte
            Presentatie
            Procesverbaal
            Product
            Projectplan
            Rapport
            Tekening
            Uittreksel
            Vergaderdocument
            Verslag
            Video
            Anders
        /],
        'types'             => {
            file        => {},
            mail        => {},
            dir         => {},
            sjabloon    => {},
        },
        'sjabloon'  => {
            'export_types'  => {
                'odt'   => {
                    mimetype    => 'application/vnd.oasis.opendocument.text',
                    label       => 'OpenDocument',
                },
                'pdf'   => {
                    mimetype    => 'application/pdf',
                    label       => 'PDF',
                },
                'doc'   => {
                    mimetype    => 'application/msword',
                    label       => 'MS Word',
                }
            },
        },
    },
    'kvk_rechtsvormen'          => GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES,
    'kvk_rechtsvormen_enabled'  => [qw/
        01
        07
        11
        21
        41
        51
        55
        70
        73
        74
        88
        201
        202
    /],
    # make a hash because checkbox widget expect that
    vertrouwelijkheidsaanduiding_options => {map { $_ => $_ } (
        '-',
        'Openbaar',
        'Beperkt openbaar',
        'Intern',
        'Zaakvertrouwelijk',
        'Vertrouwelijk',
        'Confidentieel',
        'Geheim',
        'Zeer geheim'
    )},
    # seemingly convoluted but useful for making sure the same options exist
    # in the rules and casetype admin screens
    casetype_boolean_property_options => {map {$_ => $_} ('Nee', 'Ja') },
    CASETYPE_RULE_PROPERTIES => [qw/
        vertrouwelijkheidsaanduiding
        beroep_mogelijk
        publicatie
        bag
        lex_silencio_positivo
        opschorten_mogelijk
        verlenging_mogelijk
        wet_dwangsom
        wkpb
    /],
    confidentiality => {
        public => 'Openbaar',
        internal => 'Intern',
        confidential => 'Vertrouwelijk'
    },
    CHILD_CASETYPE_OPTIONS => [
        {
            "value" => "betrokkenen",
            "label" => "Relaties"
        },
        {
            "value" => "actions",
            "label" => "Acties"
        },
        {
            "value" => "allocation",
            "label" => "Toewijzing (Fase Registreren)"
        },
        {
            "value" => "first_phase",
            "label" => "Fase Registreren"
        },
        {
            "value" => "last_phase",
            "label" => "Fase Afhandelen"
        },
        {
            "value" => "resultaten",
            "label" => "Resultaten"
        },
        {
            "value" => "authorisaties",
            "label" => "Rechten"
        },
    ]
};

use constant ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS => {
    'admin'                     => {
        'label'             => 'Administrator',
        'is_systeem_recht'  => 0,
    },
    'gebruiker'                 => {
        'label'             => 'Gebruiker',
        'is_systeem_recht'  => 0,
    },
    'dashboard'                 => {},
    'zaak_intake'               => {},
    'zaak_eigen'                => {},
    'zaak_afdeling'             => {},
    'search'                    => {},
    'plugin_mgmt'               => {},
    'contact_nieuw'             => {},
    'contact_search'            => {},
#    'beheer_kenmerken_admin'    => {},
#    'beheer_sjablonen_admin'    => {},
    'beheer_gegevens_admin'     => {},
    'beheer_basisregistratie'    => {},
#    'beheer_zaaktype_admin'     => {},
    'beheer_plugin_admin'       => {},
    'vernietigingslijst'        => {},
    'zaak_add'                  => {
        'label'             => 'Mag zaak aanmaken',
        'deprecated'        => 1,
    },
    'zaak_edit'                  => {
        'label'             => 'Mag zaken behandelen',
        'is_systeem_recht'  => 1,
    },
    'zaak_read'                  => {
        'label'             => 'Mag zaken raadplegen',
        'is_systeem_recht'  => 1,
    },
    'zaak_beheer'                  => {
        'label'             => 'Mag zaken beheren',
        'is_systeem_recht'  => 1,
    },
#    'zaak_edit'                 => {
#        'label'             => 'Mag zaken behandelen (wijzigen)',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_volgende_status'      => {
#        'label'             => 'Mag zaak naar volgende status zetten',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_vorige_status'        => {
#        'label'             => 'Mag zaak naar vorige status zetten',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_behandelaar_edit'     => {
#        'label'             => 'Mag behandelaar wijzigen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_coordinator_edit'     => {
#        'label'             => 'Mag coordinator wijzigen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_aanvrager_edit'       => {
#        'label'             => 'Mag aanvrager wijzigen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_verlengen'            => {
#        'label'             => 'Mag een zaak verlengen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_opschorten'           => {
#        'label'             => 'Mag een zaak opschorten/activeren',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_vroegtijdig_afhandelen' => {
#        'label'             => 'Mag een zaak vroegtijdig afhandelen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_relatie_edit'         => {
#        'label'             => 'Mag een relatie aanbrengen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_deelzaak_add'         => {
#        'label'             => 'Mag een deelzaak aanmaken',
#        'is_systeem_recht'  => 1,
#    },
};

use constant ZAAKSYSTEEM_STANDAARD_KOLOMMEN => {
    status                                      => sub {
        my  $zaak   = shift;

        return $zaak->status;
    },
    'me.id'                                     => sub {
        my  $zaak   = shift;

        return $zaak->id;
    },
    'me.days_perc'                              => sub {
        my  $zaak   = shift;

        return $zaak->get_column('days_perc');
    },
    'zaaktype_node_id.titel'                    => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->titel;
    },
    'me.onderwerp'                              => sub {
        my  $zaak   = shift;

        return $zaak->onderwerp;
    },
    'aanvrager'                                 => sub {
        my  $zaak   = shift;

        return $zaak->aanvrager->naam;
    },
    'dagen'                                     => sub {
        my  $zaak   = shift;

        return $zaak->get_column('days_left');
    }
};

use constant ZAAKSYSTEEM_BETROKKENE_KENMERK => {
    id                  => {
        'bedrijf'               => 'id',
        'medewerker'            => 'id',
        'natuurlijk_persoon'    => 'id'
    },
    naam                => {
        'bedrijf'               => 'handelsnaam',
        'medewerker'            => 'naam',
        'natuurlijk_persoon'    => 'naam'
    },
    handelsnaam         => {
        'bedrijf'               => 'handelsnaam',
    },
    display_name        => {
        'bedrijf'               => 'display_name',
        'medewerker'            => 'display_name',
        'natuurlijk_persoon'    => 'display_name'
    },
    kvknummer           => {
        'bedrijf'               => 'dossiernummer',
    },
    burgerservicenummer => {
        'natuurlijk_persoon'    => 'burgerservicenummer',
    },
    a_nummer => {
        'natuurlijk_persoon'    => 'a_nummer',
    },
    login     => {
        'bedrijf'               => 'login',
    },
    password  => {
        'bedrijf'               => 'password',
    },
    'achternaam' => {
        'medewerker'            => 'geslachtsnaam',
        'natuurlijk_persoon'    => 'achternaam'
    },
    'volledigenaam' => {
        'medewerker'            => 'display_name',
        'natuurlijk_persoon'    => 'volledige_naam'
    },
    'geslachtsnaam' => {
        'medewerker'            => 'geslachtsnaam',
        'natuurlijk_persoon'    => 'geslachtsnaam'
    },
    'voorvoegsel' => {
        'natuurlijk_persoon'    => 'voorvoegsel'
    },
    'voornamen'   => {
        'natuurlijk_persoon'    => 'voornamen',
        'medewerker'            => 'voornamen',
    },
    'geslacht'    => {
        'natuurlijk_persoon'    => 'geslacht'
    },
    'aanhef'      => {
        'natuurlijk_persoon'    => 'aanhef'
    },
    'aanhef1'     => {
        'natuurlijk_persoon'    => 'aanhef1'
    },
    'aanhef2'     => {
        'natuurlijk_persoon'    => 'aanhef2'
    },
    'straat'      => {
        'natuurlijk_persoon'    => 'straatnaam',
        'bedrijf'               => 'straatnaam',
        'medewerker'            => 'straatnaam'
    },
    'huisnummer'  => {
        'natuurlijk_persoon'    => 'volledig_huisnummer',
        'bedrijf'               => 'volledig_huisnummer',
        'medewerker'            => 'huisnummer'
    },
    'postcode'    => {
        'natuurlijk_persoon'    => 'postcode',
        'bedrijf'               => 'postcode',
        'medewerker'            => 'postcode'
    },
    'woonplaats'  => {
        'natuurlijk_persoon'    => 'woonplaats',
        'bedrijf'               => 'woonplaats',
        'medewerker'            => 'woonplaats'
    },
    'tel'           => {
        'natuurlijk_persoon'    => 'telefoonnummer',
        'bedrijf'               => 'telefoonnummer',
        'medewerker'            => 'telefoonnummer'
    },
    'mobiel'       => {
        'natuurlijk_persoon'    => 'mobiel',
        'bedrijf'               => 'mobiel',
    },
    'email'       => {
        'natuurlijk_persoon'    => 'email',
        'bedrijf'               => 'email',
        'medewerker'            => 'email'
    },
    'geboortedatum'             => {
        'natuurlijk_persoon'    => 'geboortedatum',
    },
    'geboorteplaats'             => {
        'natuurlijk_persoon'    => 'geboorteplaats',
    },
    'datum_huwelijk'            => {
        'natuurlijk_persoon'    => 'datum_huwelijk',
    },
    'datum_overlijden'            => {
        'natuurlijk_persoon'    => 'datum_overlijden',
    },
    'afdeling'              => {
        'medewerker'            => 'afdeling'
    },
    'type'        => {
        'natuurlijk_persoon'    => 'human_type',
        'bedrijf'               => 'human_type',
        'medewerker'            => 'human_type'
    }
};

use constant ZAAKSYSTEEM_BETROKKENE_SUB     => sub {
    my $zaak        = shift || return;
    my $betrokkene  = shift || return;
    my $attr        = shift || return;
    my ($config, $sub);

    unless (
        ($config    = ZAAKSYSTEEM_BETROKKENE_KENMERK->{$attr}) &&
        ($config    = $config->{ $betrokkene->btype }) &&
        ($sub       = $betrokkene->can( $config ))
    ) {
        return;
    }

    return $sub->($betrokkene);
};


use constant ZAAKSYSTEEM_STANDAARD_KENMERKEN => {
    'published' => sub {
        shift->published
    },
    'vertrouwelijkheid' => sub {
        my $zaak = shift;
        my $setting = $zaak->confidentiality;
        return ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$setting};
    },
    'zaak_bedrag' => sub {
        return shift->format_payment_amount;
    },
    'bedrag_web' => sub {
        return shift->format_payment_amount;
    },
    # same as bedrag_web, but with comma display
    'pdc_tarief' => sub {
        return shift->format_payment_amount;
    },
    'betaalstatus' => sub {
        return shift->format_payment_status;
    },
    'aanleiding' => sub {
        return shift->zaaktype_property('aanleiding');
    },
    'doel' => sub {
        return shift->zaaktype_property('doel');
    },
    'archiefclassicatiecode' => sub {
        return shift->zaaktype_property('archiefclassicatiecode');
    },
    'vertrouwelijkheidsaanduiding' => sub {
        return shift->zaaktype_property('vertrouwelijkheidsaanduiding');
    },
    'verantwoordelijke' => sub {
        return shift->zaaktype_property('verantwoordelijke');
    },
    'verantwoordingsrelatie' => sub {
        return shift->zaaktype_property('verantwoordingsrelatie');
    },
    'bezwaar_en_beroep_mogelijk' => sub {
        return shift->zaaktype_property('beroep_mogelijk');
    },
    'publicatie' => sub {
        return shift->zaaktype_property('publicatie');
    },
    'publicatietekst' => sub {
        return shift->zaaktype_property('publicatietekst');
    },
    'bag' => sub {
        return shift->zaaktype_property('bag');
    },
    'lex_silencio_positivo' => sub {
        return shift->zaaktype_property('lex_silencio_positivo');
    },
    'opschorten_mogelijk' => sub {
        return shift->zaaktype_property('opschorten_mogelijk');
    },
    'verlenging_mogelijk' => sub {
        return shift->zaaktype_property('verlenging_mogelijk');
    },
    'verlengingstermijn' => sub {
        return shift->zaaktype_property('verlengingstermijn');
    },
    'verdagingstermijn' => sub {
        return shift->zaaktype_property('verdagingstermijn');
    },
    'wet_dwangsom' => sub {
        return shift->zaaktype_property('wet_dwangsom');
    },
    'wkpb' => sub {
        return shift->zaaktype_property('wkpb');
    },
    'e_formulier' => sub {
        return shift->zaaktype_property('e_formulier');
    },
    'lokale_grondslag' => sub {
        return shift->zaaktype_property('lokale_grondslag');
    },
    'sjabloon_aanmaakdatum'                => sub {
        my  $zaak   = shift;

        my $dt = DateTime->now;
        return $dt->dmy;
    },
    'zaaknummer'                => sub {
        my  $zaak   = shift;

        return $zaak->id;
    },
    'zaaktype'                  => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->titel;
    },
    'zaaktype_versie'                   => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->version;
    },
    'zaaktype_versie_begindatum'        => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->created->dmy
            if $zaak->zaaktype_node_id->created;
    },
    'zaaktype_versie_einddatum'         => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->deleted->dmy
            if $zaak->zaaktype_node_id->deleted;
    },
    'trigger'                   => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->trigger;
    },
    'identificatie'       => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->code;
    },
    'procesbeschrijving'       => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->procesbeschrijving;
    },
    'trefwoorden'       => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_trefwoorden;
    },
    'omschrijving_of_toelichting'       => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_omschrijving;
    },
    'handelingsinitiator'       => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->handelingsinitiator;
    },
    'generieke_categorie'       => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_id->bibliotheek_categorie_id->naam
            if $zaak->zaaktype_id->bibliotheek_categorie_id;
    },
    'wettelijke_grondslag'                 => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->grondslag;
    },
    'selectielijst'             => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->selectielijst;
    },
    'doorlooptijd_wettelijk'           => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->servicenorm;
    },
    'doorlooptijd_service'           => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->afhandeltermijn;
    },
    'openbaarheid'              => sub {
        my  $zaak   = shift;

        return $zaak->zaaktype_node_id->zaaktype_definitie_id->openbaarheid;
    },
    'contactkanaal'             => sub {
        my  $zaak   = shift;

        return $zaak->contactkanaal;
    },
    'startdatum'                => sub {
        my  $zaak   = shift;

        return $zaak->registratiedatum->dmy
            if $zaak->registratiedatum;
    },
    'registratiedatum'                => sub {
        my  $zaak   = shift;

        return $zaak->registratiedatum->dmy
            if $zaak->registratiedatum;
    },
    'registratiedatum_volledig'       => sub {
        my  $zaak   = shift;

        return $zaak->registratiedatum->dmy
                . ' '
                . $zaak->registratiedatum->hms
            if $zaak->registratiedatum;
    },
    'afhandeldatum'                => sub {
        my  $zaak   = shift;

        return $zaak->afhandeldatum->dmy
            if $zaak->afhandeldatum;
    },
    'afhandeldatum_volledig'       => sub {
        my  $zaak   = shift;

        return $zaak->afhandeldatum->dmy
                . ' '
                . $zaak->afhandeldatum->hms
            if $zaak->afhandeldatum;
    },
    'opgeschort_tot' => sub {
        my $zaak = shift;
        return $zaak->stalled_until->dmy
            if $zaak->stalled_until;
    },
    'streefafhandeldatum'       => sub {
        my  $zaak   = shift;

        if ($zaak->status eq 'stalled') {
            return 'Opgeschort';
        }

        return $zaak->streefafhandeldatum->dmy
            if $zaak->streefafhandeldatum
    },
    'resultaat'                 => sub {
        my  $zaak   = shift;

        return $zaak->resultaat;
    },
    'uiterste_vernietigingsdatum' => sub {
        my  $zaak   = shift;

        return $zaak->vernietigingsdatum->dmy
            if $zaak->vernietigingsdatum;
    },
    'coordinator'               => sub {
        my  $zaak   = shift;

        return $zaak->coordinator_object->naam
            if $zaak->coordinator_object;
    },
    'coordinator_tel'           => sub {
        my  $zaak   = shift;

        return $zaak->coordinator_object->telefoonnummer
            if $zaak->coordinator_object;
    },
    'coordinator_email'         => sub {
        my  $zaak   = shift;

        return $zaak->coordinator_object->email
            if $zaak->coordinator_object;
    },
    'behandelaar'               => sub {
        my  $zaak   = shift;

        return $zaak->behandelaar_object->naam
            if $zaak->behandelaar_object;
    },
    'behandelaar_tel'           => sub {
        my  $zaak   = shift;

        return $zaak->behandelaar_object->telefoonnummer
            if $zaak->behandelaar_object;
    },
    'behandelaar_email'         => sub {
        my  $zaak   = shift;

        return $zaak->behandelaar_object->email
            if $zaak->behandelaar_object;
    },
    'behandelaar_afdeling'          => sub {
        my  $zaak   = shift;

        return unless $zaak->behandelaar_object;

        return $zaak->behandelaar_object->org_eenheid->naam
            if (
                $zaak->behandelaar_object->btype eq 'medewerker' &&
                $zaak->behandelaar_object->org_eenheid
            );

        return '';
    },
    'behandelaar_handtekening' => sub {
        my  $zaak   = shift;

        return unless $zaak->behandelaar_object;

        my $schema  = $zaak->result_source->schema;
        my $rs      = $schema->resultset('Subject');
        my $subject = $rs->find($zaak->behandelaar_object->id);

        my $current_id = $subject->settings->{signature_filestore_id};

        return '' unless $current_id;

        my $file = $schema->resultset('Filestore')->find($current_id);
        return $file->get_path;
    },
    'zaak_fase'                 => sub {
        my  $zaak   = shift;

        return $zaak->volgende_fase->fase
            if ($zaak->volgende_fase);
    },
    'zaak_mijlpaal'                 => sub {
        my  $zaak   = shift;

        return $zaak->huidige_fase->naam
            if ($zaak->huidige_fase);
    },
    'zaaknummer_hoofdzaak'          => sub {
        my  $zaak   = shift;

        return $zaak->id unless $zaak->pid;

        my $parent = $zaak->pid;
        while ($parent->pid) {
            $parent = $parent->pid;
        }

        return $parent->id;
    },
    gebruiker_naam => sub {
        return shift->result_source->resultset->current_user->naam;
    },
    'statusnummer'              => sub {
        my  $zaak   = shift;

        return $zaak->milestone;
    },
    ### 1. FASE
    ### 2. MIJLPAAL
    'aanvrager_anummer'         => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'a_nummer');
    },
    'aanvrager_naam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'naam');
    },
    'aanvrager_handelsnaam'     => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'handelsnaam');
    },
    'aanvrager_kvknummer'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'kvknummer');
    },
    'aanvrager_burgerservicenummer'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'burgerservicenummer');
    },
    'aanvrager_login'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'login');
    },
    'aanvrager_password'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'password');
    },
    'aanvrager_achternaam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'achternaam');
    },
    'aanvrager_volledigenaam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'volledigenaam');
    },
    'aanvrager_geslachtsnaam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geslachtsnaam');
    },
    'aanvrager_voorvoegsel'       => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'voorvoegsel');
    },
    'aanvrager_voornamen'       => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'voornamen');
    },
    'aanvrager_geslacht'        => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geslacht');
    },
    'aanvrager_aanhef'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'aanhef');
    },
    'aanvrager_aanhef1'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'aanhef1');
    },
    'aanvrager_aanhef2'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'aanhef2');
    },
    'aanvrager_straat'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'straat');
    },
    'aanvrager_huisnummer'              => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'huisnummer');
    },
    'aanvrager_postcode'        => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'postcode');
    },
    'aanvrager_woonplaats'      => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'woonplaats');
    },
    'aanvrager_tel'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'tel');
    },
    'aanvrager_mobiel'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'mobiel');
    },
    'aanvrager_email'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'email');
    },
    'aanvrager_afdeling'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'afdeling');
    },
    'aanvrager_type'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'type');
    },
    'aanvrager_geboortedatum'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        my $date = ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geboortedatum')
            or return;

        return $date->dmy;
    },
    'aanvrager_overlijdensdatum'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        my $date = ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'datum_overlijden')
            or return;

        return $date->dmy;
    },
    'aanvrager_huwelijksdatum'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        my $date = ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'datum_huwelijk')
            or return;

        return $date->dmy;
    },
    'aanvrager_geboorteplaats'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->aanvrager_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geboorteplaats')
    },
    'ontvanger'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'display_name');
    },
    'ontvanger_naam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'naam');
    },
    'ontvanger_handelsnaam'     => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'handelsnaam');
    },
    'ontvanger_kvknummer'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'kvknummer');
    },
    'ontvanger_burgerservicenummer'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'burgerservicenummer');
    },
    'ontvanger_achternaam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'achternaam');
    },
    'ontvanger_volledigenaam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'volledigenaam');
    },
    'ontvanger_geslachtsnaam'            => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geslachtsnaam');
    },
    'ontvanger_voorvoegsel'       => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'voorvoegsel');
    },
    'ontvanger_voornamen'       => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'voornamen');
    },
    'ontvanger_geslacht'        => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geslacht');
    },
    'ontvanger_aanhef'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'aanhef');
    },
    'ontvanger_aanhef1'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'aanhef1');
    },
    'ontvanger_aanhef2'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'aanhef2');
    },
    'ontvanger_straat'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'straat');
    },
    'ontvanger_huisnummer'              => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'huisnummer');
    },
    'ontvanger_postcode'        => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'postcode');
    },
    'ontvanger_woonplaats'      => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'woonplaats');
    },
    'ontvanger_tel'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'tel');
    },
    'ontvanger_mobiel'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'mobiel');
    },
    'ontvanger_email'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'email');
    },
    'ontvanger_afdeling'          => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'afdeling');
    },
    'ontvanger_type'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'type');
    },
    'ontvanger_geboortedatum'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        my $date = ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geboortedatum')
            or return;

        return $date->dmy;
    },
    'ontvanger_geboorteplaats'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'geboorteplaats')
    },
    'ontvanger_overlijdensdatum'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        my $date = ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'datum_overlijden')
            or return;

        return $date->dmy;
    },
    'ontvanger_huwelijksdatum'             => sub {
        my  $zaak       = shift;
        my  $betrokkene = $zaak->ontvanger_object;

        my $date = ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, 'datum_huwelijk')
            or return;

        return $date->dmy;
    },
    # Later toegevoegd ticket #1006
    'bedrijven_id'             => sub {
        my  $zaak   = shift;

        return unless $zaak->aanvrager_object;
        return unless $zaak->aanvrager_object->btype eq 'bedrijf';

        return $zaak->aanvrager_object->dossiernummer;
    },
    'dagen'             => sub {
        my  $zaak   = shift;

        return $zaak->get_column('days_left');
    },
    'voortgang'             => sub {
        my  $zaak   = shift;

        return $zaak->get_column('days_perc');
    },
    'afdeling'             => sub {
        my  $zaak   = shift;

        my $ou_object = $zaak->ou_object;

        return $ou_object->omschrijving
            if $ou_object;
    },
    'uname'                 => sub {
        my  $zaak   = shift;

        my $zaaksysteem_location    = $INC{'Zaaksysteem.pm'};

        return 'Zaaksysteem (unknown)' unless($zaaksysteem_location);

        my @file_information        = stat($zaaksysteem_location);

        my @uname   = (
            ZAAKSYSTEEM_NAAM,
            Zaaksysteem->config->{'SVN_VERSION'},
            ZAAKSYSTEEM_STARTDATUM,
            ZAAKSYSTEEM_LEVERANCIER,
            ZAAKSYSTEEM_LICENSE,
            'zaaksysteem.nl',
        );

        return join(', ', @uname);
    },
    'bewaartermijn' => sub {
        my $zaak = shift;

        if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
            my $bewaartermijn = $zaaktype_resultaat->bewaartermijn;
            return ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$bewaartermijn};
        }
    },
    'archiefnominatie' => sub {
        my $zaak        = shift;

        if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
            return $zaaktype_resultaat->archiefnominatie;
        }
    },
    'resultaat_omschrijving' => sub {
        my $zaak        = shift;

        if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
            return $zaaktype_resultaat->label;
        }
    },
    'resultaat_toelichting' => sub {
        my $zaak        = shift;

        if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
            return $zaaktype_resultaat->comments;
        }
    },
    'pid' => sub {
        my  $zaak   = shift;

        return $zaak->get_column('pid');
    },
    'vervolg_van' => sub {
        my $zaak = shift;

        return $zaak->get_column('vervolg_van');
    },
    'relates_to' => sub {
        my $zaak = shift;

        return $zaak->get_column('relates_to');
    },
    'alle_relaties' => sub {
        my  $zaak   = shift;

        my @relaties = ();
        if($zaak->get_column('pid')) {
            push @relaties, $zaak->get_column('pid');
        }
        if ($zaak->zaak_children->count) {
            my $children = $zaak->zaak_children->search;
            while (my $child = $children->next) {
                push @relaties, $child->id;
            }
        }
        if($zaak->get_column('vervolg_van')) {
            push @relaties, $zaak->get_column('vervolg_van');
        }
        if($zaak->get_column('relates_to')) {
            push @relaties, $zaak->get_column('relates_to');
        }

        push @relaties, map { $_->case_id }
            $zaak->result_source->schema->resultset('CaseRelation')->get_sorted($zaak->id);

        return join ", ", @relaties;
    },
    'deelzaken_afgehandeld' => sub {
        my $zaak = shift;

        return $zaak->zaak_children->search({
            'me.status' => { -not_in => ['resolved'] },
        })->count ? 'Nee' : 'Ja';
    },

};

use constant LDAP_DIV_MEDEWERKER             => 'Zaakbeheerder';

use constant ZAAKSYSTEEM_AUTHORIZATION_ROLES => {
    'admin'             => {
        'ldapname'          => 'Administrator',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'plugin_mgmt'               => 1,
                'contact_search'            => 1,
                'beheer'                    => 1,
#                'beheer_kenmerken_admin'    => 1,
#                'beheer_sjablonen_admin'    => 1,
                'beheer_gegevens_admin'     => 1,
#                'beheer_zaaktype_admin'     => 1,
                'beheer_plugin_admin'       => 1,
                'vernietigingslijst'        => 1,
                'owner_signatures'           => 1,
            }
        },
    },
    'beheerder'         => {
        'ldapname'          => 'Zaaksysteembeheerder',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'plugin_mgmt'               => 1,
                'contact_nieuw'             => 1,
                'contact_search'            => 1,
                'beheer'                    => 1,
#                'beheer_kenmerken_admin'    => 1,
#                'beheer_sjablonen_admin'    => 1,
                'beheer_zaaktype_admin'     => 1,
#                'beheer_gegevens_admin'     => 1,
                'beheer_plugin_admin'       => 1,
                'vernietigingslijst'        => 1,
                'owner_signatures'           => 1,
            }
        },
    },
    'zaaktypebeheerder' => {
        'ldapname'          => 'Zaaktypebeheerder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'beheer'                    => 1,
 #               'beheer_kenmerken_admin'    => 1, # TODO cleanup
 #               'beheer_sjablonen_admin'    => 1,
                'beheer_zaaktype_admin'     => 1,
            }
        },
    },
    'zaakbeheerder' => {
        'ldapname'          => 'Zaakbeheerder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'contact_search'            => 1,
            }
        },
    },
    'contactbeheerder' => {
        'ldapname'          => 'Contactbeheerder',
        'rechten'       => {
            'global'        => {
                'contact_nieuw'             => 1,
            }
        },
    },
    'basisregistratiebeheerder' => {
        'ldapname'          => 'Basisregistratiebeheerder',
        'rechten'       => {
            'global'        => {
                'woz_objects'              => 1,
                'beheer_gegevens_admin'    => 1,
            }
        },
    },
    'wethouder'    => {
        'ldapname'          => 'Wethouder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'directielid'    => {
        'ldapname'          => 'Directielid',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'afdelingshoofd'    => {
        'ldapname'          => 'Afdelingshoofd',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'div-medewerker'    => {
        'ldapname'          => LDAP_DIV_MEDEWERKER,
        'rechten'       => {
            'global'        => {
                'documenten_intake_all'     => 1,
                'documenten_intake_subject' => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_beheer'               => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'kcc-medewerker'    => {
        'ldapname'          => 'Kcc-medewerker',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_intake'               => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'zaakverdeler'      => {
        'ldapname'          => 'Zaakverdeler',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_intake'               => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'behandelaar'       => {
        'ldapname'          => 'Behandelaar',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'documenten_intake_subject' => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
            }
        },
    },
};

use constant PARAMS_PROFILE_DEFAULT_MSGS => PARAMS_PROFILE_MESSAGES_SUB;

use constant DEFAULT_KENMERKEN_GROUP_DATA => {
    help        => 'Vul de benodigde velden in voor uw zaak',
    label       => 'Benodigde gegevens',
};

use constant SEARCH_QUERY_SESSION_VAR => 'SearchQuery_search_query_id';
use constant SEARCH_QUERY_TABLE_NAME  => 'DB::SearchQuery';

use constant VALIDATION_CONTACT_DATA    => {
    optional    => [qw/
        npc-telefoonnummer
        npc-email
        npc-mobiel
    /],
    constraint_methods  => {
        'npc-email'                 => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
        'npc-telefoonnummer'        => qr/^[\d\+]{6,15}$/,
        'npc-mobiel'                => qr/^[\d\+]{6,15}$/,
    },
    msgs                => {
        'format'    => '%s',
        'missing'   => 'Veld is verplicht.',
        'invalid'   => 'Veld is niet correct ingevuld.',
        'constraints' => {
            '(?-xism:^\d{4}\w{2}$)' => 'Postcode zonder spatie (1000AA)',
            '(?-xism:^[\d\+]{6,15}$)' => 'Nummer zonder spatie (e.g: +312012345678)',
        }
    }, };

use constant ZAAKTYPE_TRIGGER => sub {
    my $trigger = pop;
    return grep { $trigger eq $_ } @{ZAAKSYSTEEM_OPTIONS->{TRIGGERS}};
};

use constant ZAAK_CONFIDENTIALITY => sub {
    my $c = pop;
    return grep { $c eq $_ } keys %{ZAAKSYSTEEM_CONSTANTS->{confidentiality}}
};

# TODO: Rename to ASSERT_BETROKKENE

use constant VALIDATION_EXTERNAL_CONTACT_DATA    => {
    required => [qw/
        npc-email
    /],
    optional    => [qw/
        npc-telefoonnummer
        npc-mobiel
    /],
    constraint_methods  => VALIDATION_CONTACT_DATA->{constraint_method},
    msgs                => VALIDATION_CONTACT_DATA->{msgs}
};

use constant ZAAK_CREATE_PROFILE_BETROKKENE => sub {
    my $val = pop;

    # TODO: Figure this out
    my $BETROKKENE_DEFAULT_HASH = {
        'betrokkene_type'   =>
            qr/^natuurlijk_persoon|medewerker|bedrijf|org_eenheid$/,
        'betrokkene_id'     => qr/^\d+$/,
        'betrokkene'        => qr/^[\w\d-]+$/,
        'verificatie'       => qr/^digid|medewerker$/,
    };

    my @betrokkenen;
    push(@betrokkenen, $val) if UNIVERSAL::isa($val, 'HASH');
    push(@betrokkenen, $val) if blessed($val);
    push(@betrokkenen, @{ $val }) if UNIVERSAL::isa($val, 'ARRAY');

    return 0 if !@betrokkenen;

    for my $betrokkene (@betrokkenen) {
        # Object? Assume betrokkene object
        # TODO: Be more strict in checking
        next if blessed($betrokkene);

        if (!$betrokkene->{betrokkene} && !$betrokkene->{create} and
            !$betrokkene->{betrokkene_id} || !$betrokkene->{betrokkene_type}) {
                return 0;
        }

        if ($betrokkene->{create} && !$betrokkene->{betrokkene_type}) {
            return 0;
        }

        if (!$betrokkene->{verificatie}) {
            return 0;
        }
    }
    return 1;
};

use constant ZAAK_CREATE_PROFILE        => {
    required        => [ qw/
        aanvraag_trigger

        aanvragers

        registratiedatum
        contactkanaal
    /],
    'optional'      => [ qw/
        status
        milestone

        onderwerp
        resultaat
        besluit

        route_ou
        route_role

        streefafhandeldatum
        afhandeldatum
        vernietigingsdatum

        coordinators
        behandelaars

        kenmerken

        created
        last_modified
        deleted

        id
        override_zaak_id

        locatie_zaak
        locatie_correspondentie

        relatie
        zaak

        actie_kopieren_kenmerken
        streefafhandeldatum_data

        ontvanger
        betrokkene_relaties
        bestemming

        uuid
        confidentiality

    /],
    'require_some'  => {
        'zaaktype_id_or_zaaktype_node_id'    => [
            1,
            'zaaktype_id',
            'zaaktype_node_id'
        ],
    },
    'constraint_methods'            => {
        'status'            => sub {
            my $val     = pop;

            return 1 unless $val;

            my $statussen = ZAKEN_STATUSSEN;

            return 1 if grep { $_ eq $val } @{ $statussen };
            return;
        },
        milestone         => qr/^\d+$/,
        onderwerp         => qr/^.{0,255}$/,
        contactkanaal     => qr/^\w{1,128}$/,
        aanvragers        => ZAAK_CREATE_PROFILE_BETROKKENE,
        coordinators      => ZAAK_CREATE_PROFILE_BETROKKENE,
        behandelaars      => ZAAK_CREATE_PROFILE_BETROKKENE,
        aanvraag_trigger  => ZAAKTYPE_TRIGGER,
        confidentiality   => ZAAK_CONFIDENTIALITY,
        ontvanger         => sub {
            my $val = pop;

            return 1 if $val =~ /-/;
            return;
        },
        'betrokkene_relaties' => sub {
            my $val = pop;

            return 1 if UNIVERSAL::isa($val, 'HASH');
            return;
        }
    },
    dependencies => {
        aanvraag_trigger => sub {
            my ($dfv, $val) = @_;

            if (   lc($val) eq 'intern'
                && lc($dfv->get_filtered_data->{'bestemming'}) eq 'extern') {
                return ['ontvanger'];
            }

            return [];
        },
    },
    dependency_groups => {
        zaak_and_relatie => ['relatie', 'zaak'],
    },
    defaults => {
        status          => ZAKEN_STATUSSEN_DEFAULT,
        milestone       => 1,
        confidentiality => 'public',
    },
#    typed => {
#        aanvraag_trigger => 'Str',
#        confidentiality  => 'Str',
#    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

#
# this is the configuration for importing zaaktypes from one system into another. the challenge
# is that configurations will differ, thus not every dependency is present on every system. at
# the same time, it is not helpful to just re-import any dependency that is missing, this will
# cause duplicate items. e.g. when importing a zaaktype it looks to re-link to all it's needed
# kenmerken. if one is not found, it will ask the user if the kenmerk must be imported, or that
# another kenmerk must be selected in it's place.
#
#
# when exporting, all dependencies are exported with the zaaktype.
#
# the 'match' subroutine tries to match these exported dependencies with items that are in the
# local database. it returns a filter that is used in a query.
#
#
use constant ZAAKTYPE_DEPENDENCY_IDS => {
    'zaaktype_id$'                      => 'Zaaktype',
    '_kenmerk$'                         => 'BibliotheekKenmerken',
    '^bibliotheek_kenmerken_id$'        => 'BibliotheekKenmerken',
    '^bibliotheek_sjablonen_id$'        => 'BibliotheekSjablonen',
    '^bibliotheek_categorie_id$'        => 'BibliotheekCategorie',
    '^bibliotheek_notificaties_id$'     => 'BibliotheekNotificaties',
    '^role_id$'                         => 'LdapRole',
    '^ou_id$'                           => 'LdapOu',
    '^filestore_id$'                    => 'Filestore',
    '^checklist_vraag_status_id$'       => 'ChecklistVraagStatus',
    '^actie_\d+_datum_bibliotheek_kenmerken_id$' => 'BibliotheekKenmerken',
};

use constant ZAAKTYPE_DEPENDENCIES => {
    ChecklistVraagStatus => {
        match => ['naam'],
        name  => 'naam',
        label => 'checklistvraag',
        title => 'Checklistvraag',
        letter_e => '',
    },
    Filestore => {
        match => ['md5'],
        name  => 'original_name',
        label => 'bestand',
        title => 'Bestand',
        letter_e => '',
    },
    Zaaktype => {
        match => [],
        name  => 'zaaktype_titel',
        label => 'zaaktype',
        title => 'Zaaktype',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekKenmerken => {
        match => [qw/naam deleted value_type value_mandatory value_length besluit/],
        name  => 'naam',
        label => 'kenmerk',
        title => 'Kenmerken',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekNotificaties => {
        match => [qw/label subject message/],
        name  => 'label',
        label => 'bericht',
        title => 'Berichten',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekSjablonen         => {
        match => [qw/naam/],
        name  => 'naam',
        label => 'sjabloon',
        letter_e => 'e',
        has_category => 1,
    },
    LdapRole => {
        match => [qw/short_name/],
        name  => 'short_name',
        label => 'rol',
        title => 'Rol',
        letter_e => 'e',
    },
    LdapOu => {
        match => [qw/ou/],
        name  => 'ou',
        label => 'organisatorische eenheid',
        letter_e => 'e',
    },
    BibliotheekCategorie        => {
        match => [qw/naam/],
        name  => 'naam',
        label => 'categorie',
        letter_e => 'e',
    },
};

use constant BETROKKENE_RELATEREN_PROFILE => {
    required    => [qw/
        betrokkene_identifier
        magic_string_prefix
        rol
    /],
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

use constant BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION => sub {
    my (@used_columns)              = @{ shift(@_) };
    my ($magic_string_prefix, $rol) = @_;

    my @ZAAKSYSTEEM_KENMERKEN   = (
        ZAAKSYSTEEM_STANDAARD_KOLOMMEN,
        ZAAKSYSTEEM_STANDAARD_KENMERKEN
    );

    push(
        @used_columns,
        keys %{ $_ }
    ) for @ZAAKSYSTEEM_KENMERKEN;

    ### make a suggestion or give back given string
    my $suggestion  = $magic_string_prefix || $rol;
    my $counter     = '';
    while (
        grep {
            $suggestion . $counter .'_naam' eq $_ ||
            $suggestion .  $counter eq $_
        } @used_columns
    ) {
        $counter = 0 if !$counter;
        $counter++;
    }

    $suggestion     .= $counter;

    return $suggestion;

};

use constant VERNIETIGINGS_REDENEN  => [
    'In belang van de aanvrager',
    'Uniek of bijzonder karakter voor de organisatie',
    'Bijzondere tijdsomstandigheid of gebeurtenis',
    'Beeldbepalend karakter',
    'Samenvatting van gegevens',
    'Betrokkene(n) is van bijzondere betekenis geweest',
    'Vervanging van stukken bij calamiteit',
    'Aanleiding van algemene regelgeving',
    'Verstoring van logische samenhang',
];



use constant ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE         => {
    required            => [qw/
    /],
    optional            => [qw/
        reden
        vernietigingsdatum_type
        vernietigingsdatum
    /],
    constraint_methods  => {
        vernietigingsdatum_type => sub {
            my ($dfv, $val) = @_;

            if($val eq 'termijn') {
                my $vernietigingsdatum = $dfv->{'__INPUT_DATA'}->{vernietigingsdatum};

                if(
                    !UNIVERSAL::isa($vernietigingsdatum, 'DateTime') &&
                    $vernietigingsdatum !~ /^\d{2}\-\d{2}\-\d{4}$/
                ) {
                    return;
                }
            }

            return 1;
        },
        vernietigingsdatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
        reden               => sub {
            my ($dfv, $value) = @_;

            my $redenen = VERNIETIGINGS_REDENEN;

            if (grep { $_ eq $value } @{ $redenen }) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        vernietigingsdatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs                => PARAMS_PROFILE_MESSAGES_SUB,
};



use constant ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE         => {
    required            => [qw/
        streefafhandeldatum
    /],
    constraint_methods  => {
        streefafhandeldatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        streefafhandeldatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs                => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE         => {
    required            => [qw/
        registratiedatum
    /],
    constraint_methods  => {
        registratiedatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        registratiedatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant DOCUMENTS_STORE_TYPE_NOTITIE       => 'notitie';
use constant DOCUMENTS_STORE_TYPE_FILE          => 'file';
use constant DOCUMENTS_STORE_TYPE_MAIL          => 'mail';
use constant DOCUMENTS_STORE_TYPE_JOB           => 'job';

use constant BETROKKENE_TYPE_BEDRIJF            => 'bedrijf';
use constant BETROKKENE_TYPE_NATUURLIJK_PERSOON => 'natuurlijk_persoon';
use constant BETROKKENE_TYPE_MEDEWERKER         => 'medewerker';

use constant OBJECTSEARCH_TABLE_ORDER => qw/
   contact zaak file
   kennisbank_producten kennisbank_vragen
/;
use constant OBJECTSEARCH_TABLENAMES => {
    bedrijf             => {
        tablename           => 'Bedrijf'
    },
    natuurlijk_persoon  => {
        tablename           => 'NatuurlijkPersoon',
    },
    zaak                => {
        tablename           => 'Zaak',
    },
    zaaktype            => {
        tablename           => 'Zaaktype',
    },
    file                => {
        tablename           =>  'File'
    },
    kennisbank_producten => {
        tablename           => 'KennisbankProducten'
    },
    kennisbank_vragen   => {
        tablename           => 'KennisbankVragen'
    },
    bibliotheek_kenmerken => {
        tablename           => 'BibliotheekKenmerken'
    },
    bibliotheek_sjablonen => {
        tablename           => 'BibliotheekSjablonen'
    },
    bibliotheek_categorie => {
        tablename           => 'BibliotheekCategorie'
    },
    bibliotheek_notificaties => {
        tablename           => 'BibliotheekNotificaties'
    },
};

use constant OBJECT_ACTIONS => {
    case => {
        allocate => {
            label => 'Toewijzing wijzigen',
            path => '/bulk/update/allocation'
        },
        acquire => {
            label => 'In behandeling nemen',
            path => '/bulk/update/owner'
        },
        suspend => {
            label => 'Opschorten',
            path => '/bulk/update/opschorten'
        },
        resume => {
            label => 'Hervatten',
            path => '/bulk/update/resume'
        },
        prolong => {
            label => 'Verlengen',
            path => '/bulk/update/verlengen'
        },
        relate => {
            label => 'Relateren',
            path => '/bulk/update/relatie'
        },
        manage => {
            label => 'Beheeracties',
            path => '/bulk/update/set_settings'
        },
        destroy => {
            label => 'Vernietigen',
            path => '/bulk/update/destroy'
        },
        publish => {
            label => 'Publiceren',
            path => '/bulk/publish'
        },
        export => {
            label => 'Exporteren',
            path => undef # implies current zql query 
        }
    }
};

use constant ALLOW_ONLY_TRUSTED_HTML    => sub {
    my $val     = shift;

    use HTML::TagFilter;

    my $tf      = new HTML::TagFilter;

    return $tf->filter($val);
};

use constant ALLOW_NO_HTML    => sub {
    my $val     = shift;

    use HTML::TagFilter;

    my $tf      = HTML::TagFilter->new(allow => {});

    return $tf->filter($val);
};

use constant PROFILE_KENNISBANK_PRODUCT_BEWERKEN => {
    required            => [qw/naam bibliotheek_categorie_id/],
    optional            => [qw/
        id
        omschrijving
        omschrijving_internal
        aanpak
        aanpak_internal
        kosten
        kosten_internal
        voorwaarden
        voorwaarden_internal
        producten
        zaaktypen
        vragen
        versie_id
        commit_message
        coupling
        external_id
        publication
        external
        internal
    /],
    field_filters       => {
        omschrijving            => ALLOW_ONLY_TRUSTED_HTML,
        omschrijving_internal   => ALLOW_ONLY_TRUSTED_HTML,
        aanpak                  => ALLOW_ONLY_TRUSTED_HTML,
        aanpak_internal         => ALLOW_ONLY_TRUSTED_HTML,
        voorwaarden             => ALLOW_ONLY_TRUSTED_HTML,
        voorwaarden_internal    => ALLOW_ONLY_TRUSTED_HTML,
        kosten                  => ALLOW_ONLY_TRUSTED_HTML,
        kosten_internal         => ALLOW_ONLY_TRUSTED_HTML,
    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

use constant PROFILE_KENNISBANK_VRAAG_BEWERKEN => {
    required            => [qw/naam bibliotheek_categorie_id/],
    optional            => [qw/
        id
        vraag
        antwoord
        producten
        zaaktypen
        vragen
        versie_id
        commit_message
    /],
    field_filters       => {
        vraag               => ALLOW_ONLY_TRUSTED_HTML,
        antwoord            => ALLOW_ONLY_TRUSTED_HTML,
    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};




use constant STATUS_LABELS => {
    new         => 'Nieuw',
    open        => 'In behandeling',
    stalled     => 'Opgeschort',
    deleted     => 'Vernietigd',
    resolved    => 'Afgehandeld',
    overdragen  => 'Overdragen',
    vernietigen => 'Te vernietigen',
};

use constant MIMETYPES_ALLOWED => {
    '.docx'    => {
        mimetype    => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.xls'     => {
        mimetype    => 'application/vnd.ms-excel',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/msword'],
    },
    '.xlsx'    => {
        mimetype    => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        conversion  => 'jodconvertor',
    },
    '.ppt'     => {
        mimetype    => 'application/vnd.ms-powerpoint',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/msword','application/vnd.openxmlformats-officedocument.presentationml.presentation'],
    },
    '.pptx'    => {
        mimetype    => 'application/vnd.ms-powerpoint',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/msword','application/vnd.openxmlformats-officedocument.presentationml.presentation'],
    },
    '.odp'     => {
        mimetype    => 'application/vnd.oasis.opendocument.presentation',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
    },
    '.pdf'     => {
        mimetype    => 'application/pdf',
        conversion  => 'none',
    },
    '.bmp'     => {
        mimetype    => 'image/bmp',
        conversion  => 'imagemagick',
    },
    '.gif'     => {
        mimetype    => 'image/gif',
        conversion  => 'imagemagick',
    },
    '.jpeg'    => {
        mimetype    => 'image/jpeg',
        conversion  => 'imagemagick',
    },
    '.jpg'     => {
        mimetype    => 'image/jpeg',
        conversion  => 'imagemagick',
    },
    '.png'     => {
        mimetype    => 'image/png',
        conversion  => 'imagemagick',
    },
    '.tiff'    => {
        mimetype    => 'image/tiff',
        conversion  => 'imagemagick',
    },
    '.tif'     => {
        mimetype    => 'image/tiff',
        conversion  => 'imagemagick',
    },
#    '.htm'     => {
#        mimetype    => 'text/html',
#    },
    '.xml'     => {
        mimetype    => 'text/xml',
    },
    '.rtf'     => {
        mimetype    => 'text/rtf', #application/rtf
        conversion  => 'jodconvertor',
    },
#    '.html'    => {
#        mimetype    => 'text/html',
#    },
    '.doc'     => {
        mimetype    => 'application/msword',
        conversion  => 'jodconvertor',
    },
    '.dot'     => {
        mimetype    => 'application/msword',
        conversion  => 'jodconvertor',
    },
    '.odt'     => {
        mimetype    => 'application/vnd.oasis.opendocument.text',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.ods'     => {
        mimetype    => 'application/vnd.oasis.opendocument.spreadsheet',
        conversion  => 'jodconvertor',
    },
    '.odf' => {
        mimetype => 'application/vnd.oasis.opendocument.text',
        conversion => 'jodconvertor'
    },
    '.svg'     => {
        mimetype    => 'image/svg+xml',
        conversion  => 'imagemagick',
    },
    '.csv'     => {
        mimetype    => 'text/csv',
        conversion  => 'jodconvertor',
    },
    '.xps'     => {
        mimetype    => 'application/vnd.ms-xpsdocument',
        conversion  => '',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.asc'     => {
        mimetype    => 'text/plain',
        conversion  => '',
    },
    '.txt'     => {
        mimetype    => 'text/plain',
        conversion  => 'jodconvertor',
    },
    '.gml'     => {
        mimetype    => 'application/xml',
        conversion  => '',
        alternate_mimetypes => ['application/gml'],
    },
    '.shp'     => {
        mimetype    => 'application/octet-stream',
        conversion  => '',
    },
    '.crt' => {
        mimetype => 'text/plain',
        conversion => '',
        alternate_mimetypes => ['application/x-pem-file']
    },
    '.pem' => {
        mimetype => 'text/plain',
        conversion => '',
        alternate_mimetypes => ['application/x-pem-file']
    },
    '.key' => {
        mimetype => 'text/plain',
        conversion => '',
        alternate_mimetypes => ['application/x-pem-file']
    },
    '.msg' => {
        mimetype => 'application/vnd.ms-outlook'
    },
    '.eml' => {
        mimetype => 'message/rfc822'
    }
};

use constant JOBS_INFORMATION_MAP   => {
    'zaak::update_kenmerk'    => {
        change_type => 'wijziging',
        label       => 'Wijzigen van kenmerk',
    },
};

use constant STRONG_RELATED_COLUMNS => [qw/
    aanvrager
    behandelaar
    coordinator
    locatie_correspondentie
    locatie_zaak
/];

use constant ZAAK_EMPTY_COLUMNS => [qw/
    aanvrager
    behandelaar
    coordinator
    locatie_correspondentie
    locatie_zaak
/];

=head2 Event type groupings

Ability to group events into categories for easier consumption.

=cut

use constant EVENT_TYPE_GROUPS => {
    document        => [qw[document case/document]],
    case            => [qw[case]],
    contactmoment   => [qw[subject/contact_moment]],
    note            => [qw[case/note subject/note]]
};

=head2 DENIED_BROWSERS

Deny browsers or certain browser versions, examples:

 ie         # Deny ie
 ie::<10    # Deny ie below version 10
 ie::>10    # Deny ie above version 10
 ie::7      # Deny ie 7

=cut

use constant DENIED_BROWSERS   => [qw/
    ie::<8
/];

use constant FILE_STORE_LOCATION =>
    '/home/zaaksysteem/dev/Zaaksysteem_Documentairly/files/storage';

use constant FRIENDLY_BETROKKENE_MESSAGES => {
    deceased    => 'Betrokkene is overleden',
    secret      => 'Betrokkene heeft een indicatie "Geheim"',
    moved       => 'Betrokkene is verhuisd',
    onderzoek   => 'Betrokkene staat in onderzoek',
    briefadres  => 'Betrokkene heeft een briefadres'
};

use constant EDE_PUBLICATION_STRINGS => {
    '_empty' => [],
    'Collegevergadering' => [],
    'Raadscommissie AZ&M' => [],
    'Raadscommissie RO' => [],
    'Raadscommissie MZ' => [],
    'Raadscommissie BFO' => [],
    'Raadsvergadering' => [],
    'College van B en W' => [],
    'Besluitvormende vergadering' => ['Opening en mededelingen van de voorzitter', 'Vaststelling agenda', 'Vragenuurtje'],
    'Oordeelvormende vergadering' => ['Opening en mededelingen van de voorzitter'],
    'Beeldvormende/informatieve ronde' => ['Opening en mededelingen van de voorzitter']
};

use constant SUBJECT_TYPE_EMPLOYEE => 'employee';

use constant SJABLONEN_TARGET_FORMATS => sub {
    my $format = shift;
    if ($format && $format =~ m/^(?:odt|pdf)$/) {
        return 1;
    }
    return 0;
};

use constant ZAAKTYPE_NODE_TITLE => qr/^[\w\d _]+$/;
use constant ZAAKTYPE_ATTRIBUTEN => {
    ztc_grondslag             => 'text',
    ztc_handelingsinitiator   => 'text',
    ztc_selectielijst         => 'text',
    ztc_afhandeltermijn       => 'text',
    ztc_afhandeltermijn_type  => 'text',
    ztc_servicenorm           => 'text',
    ztc_servicenorm_type      => 'text',
    ztc_escalatiegeel         => 'text',
    ztc_escalatieoranje       => 'text',
    ztc_escalatierood         => 'text',
    ztc_openbaarheid          => 'text',
    ztc_webform_toegang       => 'text',
    ztc_webform_authenticatie => 'text',
    pdc_meenemen              => 'text',
    pdc_description           => 'text',
    pdc_voorwaarden           => 'text',
    pdc_tarief                => 'text',
    ztc_procesbeschrijving    => 'text',
};


use constant STUF_VERSIONS => qr/^(?:0204|0301)$/;
use constant STUF_XSD_PATH => 'share/wsdl/stuf';
use constant STUF_XML_URL  => 'http://www.egem.nl/StUF';

=head2 BAG_TYPES

The types of Basis Administratie Gemeentes supported by Zaaksysteem

=cut

use constant BAG_TYPES => [qw/ligplaats nummeraanduiding openbareruimte pand standplaats verblijfsobject verblijfsobjectpand woonplaats/];

=head2 PROFILE_BAG_TYPES_OK

Helper function for parameter checking for BAG_TYPES

=cut

use constant PROFILE_BAG_TYPES_OK => sub {
    my $type = shift;
    if ($type) {
        $type = lc($type);
        if (grep { $type eq $_ } @{BAG_TYPES()}) {
            return $type;
        }
    }
    return undef;
};


use constant SERVICE_NORM_TYPES => [
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM},
];

use constant SERVICE_NORM_TYPES_OK => sub {
    my $type = shift;
    if ($type) {
        $type = lc($type);
        if (grep { $type eq lc($_) } @{SERVICE_NORM_TYPES()}) {
            return $type;
        }
    }
    return undef;
};

use constant ZAAKSYSTEEM_BOFH => 'betrokkene-medewerker-20000';

sub _bag_filter {
    # XXX Almost cloned from lib/Zaaksysteem/Attributes.pm
    # Probably needs to become part of B::O::A::BAG somehow
    my ($schema, $bag_id) = @_;
    return unless $bag_id;
    my $rs = $schema->resultset('BagNummeraanduiding');

    my $human_identifier = eval {
        $rs->get_human_identifier_by_source_identifier(
            $bag_id,
            {prefix_with_city => 1}
        ),
    };
    my $address_data = eval {
        $rs->get_address_data_by_source_identifier($bag_id),
    };

    return {
        bag_id           => $bag_id,
        human_identifier => $human_identifier,
        address_data     => $address_data,
    };
}

sub _valuta_fix_filter {
    my ($schema, $value) = @_;

    return if not $value;
    $value =~ s/,/./g;
    return $value;
}

1;

package Zaaksysteem::BR::Subject;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(UUID);
use Zaaksysteem::BR::Subject::Iterator;
use Zaaksysteem::Object::Types::Subject;

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::BR::Subject::Utils
    Zaaksysteem::BR::Subject::StUF::NP
    Zaaksysteem::BR::Subject::OverheidIO
/;

=head1 NAME

Zaaksysteem::BR::Subject - This is a bridge between new and old style creation of subjects.

=head1 DESCRIPTION

This module is an abstraction between old and new style creation. This module is in use by the 
JSON API for subject creation.

At this time using the old Betrokkene.pm infrastructure, but can be rewritten to use a new future
basis.

NOTE: This module is designed to be used outside of catalyst, make sure it stays this way...

=head1 SYNOPIS

    my $bridge     = Zaaksysteem::BR::Subject->new(
        schema          => $schema,
    );

    ###
    ### Retrieving multiple subjects
    ###

    my $subjects    = $bridge->search({
        subject_type => 'person',
        first_names  => 'Don',
    });

    my $subject     = $subjects->first->as_object;


    ###
    ### Retrieving a single subject
    ###

    my $subject     = $bridge->find('bde83160fea84b1397fc7f8256b40787');


    ###
    ### Updating a subject
    ###

    $subject->subject->first_names('Don Dana');
    my $subject     = $bridge->save($subject);


    ###
    ### Create a new subject from params:
    ###

    my %params = (
        subject_type    => 'person',
        subject         => {
            'personal_number'           => '123456789',
            'personal_number_a'         => '1987654321',
            'initials'                  => 'D.',
            'first_names'               => 'Don',
            'family_name'               => 'Fuego',
            'surname'                   => 'LaFuega',
            'prefix'                    => 'The',
            'gender'                    => 'M',
            'place_of_birth'            => 'Amsterdam',
            'date_of_birth'             => '1982-06-05',
            'use_of_name'               => 'E',
            'address_residence'         => {
                street          => 'Muiderstraat',
                number          => 42,
                number_letter   => 'a',
                number_suffix   => '521',
                city            => 'Amsterdam',
                zipcode         => '1011PZ',
                country         => {
                    label           => 'Netherlands',
                    dutch_code      => 6030,
                }
            }
        },
    );

    my $object      = $bridge->object_from_params(\%params);
    my $subject     = $bridge->save($object);

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object. Required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 remote_search

B<Type>: Str (ENUM, e.g.: stuf)

A str indicating we would like to search in a remote instance instead of locally.

=cut

has remote_search => (
    is       => 'ro',
    isa      => 'Str',
);

=head2 user

B<Type>: L<Zaaksysteem::Schema::Subject>

A Zaaksysteem subject,  used to log search requests.


=cut

has user => (
    is  => 'ro',
    isa => 'Zaaksysteem::Schema::Subject',
);

=head1 METHODS

=head2 search

    $bridge->search(
        {
            company         => 'Mintlab B.V.',
            coc_number      => '98765432',
        };
    );

B<Return value>: L<Zaaksysteem::Object::Types::Subject>

=cut

sub search {
    my $self        = shift;
    my $params      = shift;

    my $rs          = $self->search_rs($params, @_);

    my $iterator    = Zaaksysteem::BR::Subject::Iterator->new(
        rs                      => $rs,
        inflator                => sub {
            shift->as_object
        },
        cache_related_rows      => 1,
        cache_below_num_rows    => 100,
    );

    return wantarray ? $iterator->all : $iterator;
}

=head2 search_rs

    $bridge->search(
        {
            subject_type    => 'company'            ## Required
            subject         => {
                company         => 'Mintlab B.V.',
                coc_number      => '98765432',
            }
        };
    );

B<Return value>: L<Zaaksysteem::Model::DB::[NATUURLIJKPERSOON|BEDRIJF|SUBJECT]::ResultSet>

=cut

sub search_rs {
    my $self        = shift;
    my $params      = shift;

    throw('br/subject/search/missing_subject_type', 'Param: "subject_type" is mandatory')
        unless $params->{subject_type};

    ### Collect DBIx::Class data, like resultsource, table etc
    my %map             = (
        person             => 'NatuurlijkPersoon',
        company            => 'Bedrijf',
        employee           => 'Subject',
    );

    my $resultsource    = $map{ $params->{subject_type} };
    my $resultset       = $self->schema->resultset($resultsource);

    ### Determine the correct subroutine, search_from_bridge is preferred.
    return $self->_search_via_preferred($resultset, $params, @_);
}

=head2 find

    my $subject = $bridge->find('645cf805-6ef9-42e4-bf32-ede66aecc232');

    my $object  = $subject;

B<Return value>: L<Zaaksysteem::Object::Types::Subject>

Finds a subject by the given uuid

=cut

sub find {
    my $self        = shift;
    my $uuid        = shift;

    throw('br/subject/find/invalid_uuid', "Invalid UUID given: $uuid") unless UUID->check($uuid);

    my $row;
    for my $table (qw/NatuurlijkPersoon Bedrijf Subject/) {
        $row = $self->schema->resultset($table)->search({ uuid => $uuid })->first;
        return $row->as_object if $row;
    }
    return undef;
}

=head2 find_by_old_subject_identifier

Retrieves a subject (if one exists) using the old style subject identifier

    my $subject = $c->model('BR::Subject')->find_by_old_subject_identifier('betrokkene-medewerker-1');

=cut

sig find_by_old_subject_identifier => 'Str';

sub find_by_old_subject_identifier {
    my $self = shift;
    my $old_subject_identifier = shift;

    my %map             = (
        natuurlijk_persoon => 'NatuurlijkPersoon',
        bedrijf            => 'Bedrijf',
        medewerker         => 'Subject'
    );

    my (undef, $type, $id) = split m[\-], $old_subject_identifier;

    unless (exists $map{ $type }) {
        throw(
            'br/subject/find_by_old_subject_identifier/identifier_invalid',
            sprintf(
                '"%s" is not a valid old-style subject identifier',
                $old_subject_identifier
            )
        );
    }

    my $row = $self->schema->resultset($map{ $type })->find($id);

    return unless defined $row;
    return $row->as_object;
}

=head2 object_from_params

    my $object      = $bridge->object_from_params(\%params);

    print $object->subject->naam;

The object for a subject is little intense: there are relations to addresses, components,
countries, municipalities etc.

When retrieving a hash, this method turns it into a valid subject object for one of the
given types.

=cut

sub object_from_params {
    my $self        = shift;

    return Zaaksysteem::Object::Types::Subject->new_from_params(shift);
}

=head2 save

    ### New object
    my $subject = $bridge->save($bridge->object_from_params({subject_type=>'person', subject => {...}}));

    ### Existing object
    my $object  = $bridge->find('645cf805-6ef9-42e4-bf32-ede66aecc232');
    $object->subject->first_names('Don Klaas');
    my $subject = $bridge->save($object);

=cut

sub save {
    my $self        = shift;
    my $object      = shift;

    throw(
        'br/subject/save/invalid_object',
        'First parameter should be an object of type Zaaksysteem::Object::Types::Subject'
    ) unless blessed($object) && $object->isa('Zaaksysteem::Object::Types::Subject');

    return $object->save_to_tables(schema => $self->schema);
}

=head2 remote_import

    ### Params
    my $new_object = $self->remote_import({ subject_type => 'person', subject => { personal_number ... }});

    ### Object
    my $object = $self->find('8a9b8a9b-8ab9a8b-ab8ab-ab8ab89-abab')
    my $new_object = $self->remote_import($object);

Imports an entity in a way that is nearly identical to creating an entity. But this time, it
will take place remotely, and only works when remote_search is enabled.

Roles should completely "around" this method.

=cut

sub remote_import {
    my $self        = shift;

    throw(
        'br/subject/import/no_remote_search',
        'Bridge is in local-only mode, need remote_search != undef for this to work'
    ) unless $self->remote_search;
}

=head2 enable_subscription

Enable the object subscription for a subject.
Currently only supports L<Zaaksysteem::Object::Types::Person> objects

=cut

sub enable_subscription {
    my $self = shift;
    my $subject = shift;

    if ($subject->subject_type ne 'person'){
        return 0;
    }

    if (!$subject->has_external_subscription) {
        $self->log->warn("Unable to enable object subscription. The subject does not have one");
        return 0;
    }

    my $os = $subject->external_subscription;
    my $item  = $self->schema->resultset('Queue')->create_item(
        'enable_object_subscription',
        {
            label => "Enable object subscription",
            data  => {
                subscription_id => $os->internal_identifier,
                bsn             => $subject->subject->personal_number,
                a_nummer        => $subject->subject->personal_number_a,
            },
        }
    );
    push(@{ $self->schema->default_resultset_attributes->{queue_items} }, $item);
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::BR::Subject::Types::Authentication;

use Moose::Role;
use Zaaksysteem::Tools;


=head1 NAME

Zaaksysteem::BR::Subject::Types::Authentication - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=cut

=head2 find_or_generate_new_password

    $object->new_from_dbix(row => $row, password => 'blabla');

Will generate a new authentication object into the database, checking for a unique
loginname, a valid password etc.

=cut


define_profile find_or_generate_new_password => (
    required    => {
        row         => 'Zaaksysteem::Model::DB::Bedrijf',
    }
);

sub find_or_generate_new_password {
    my $self            = shift;
    my $params          = assert_profile({ @_ })->valid;
    my $row             = $params->{row};

    my $authrs          = $row->result_source->schema->resultset('BedrijfAuthenticatie');

    ### Existing row?
    if (my $existing = $authrs->search({gegevens_magazijn_id => $row->id })->first) {
        return $self->new_from_dbix($existing, password => $self->password);
    }

    throw(
        'object/types/authentication/invalid_coc_number',
        'Cannot create a login entry for a company without a proper "KVK-Nummer"'
    ) unless length($row->dossiernummer > 5);

    my $login           = int($row->dossiernummer);
    $login              .= '0' x (9 - length($login));

    ### Increment login by 1 when login exists
    $login += 1 while ($authrs->search({login => $login })->count);

    ### Hash the password
    my $csh = Crypt::SaltedHash->new(algorithm => 'SHA-1');
    $csh->add($self->password);

    my $hashed                  = $csh->generate();

    ### Create entry
    if (
        my $row = $authrs->create({
            password                => $hashed,
            login                   => $login,
            gegevens_magazijn_id    => $row->id,
        })
    ) {
        return $self->new_from_dbix($row);
    }
}

=head2 new_from_dbix

    $object->new_from_dbix($s->resultset('BedrijfAuthenticatie')->first);

Inflates an object from the BedrijfAuthenticatie schema

=cut

sig new_from_dbix => 'Zaaksysteem::Model::DB::BedrijfAuthenticatie';

sub new_from_dbix {
    my $class               = shift;
    my $row                 = shift;

    ### Existing row?
    my $password;
    if ($row->password =~ /^\{SSHA\}.+$/) {
        $password = $row->password;
    }

    return $class->new(@_, username => $row->login, entity_type => 'company', ($password ? (hashed_password => $password) : ()));
}

=head2 save_to_tables

=cut

define_profile save_to_tables => (
    required    => {
        gegevens_magazijn_id => 'Int',
        schema               => 'Zaaksysteem::Schema',
    },
);

sub save_to_tables {
    my $self        = shift;
    my $params      = assert_profile({ @_ })->valid;
    my $np_id       = $params->{gegevens_magazijn_id};
    my $schema      = $params->{schema};

    my $row         = $schema->resultset('Bedrijf')->find($np_id);

    return unless $row;

    return $self->find_or_generate_new_password(row => $row);
}

=head2 new_from_related_row

    $object->new_from_related_row($s->resultset('Bedrijf')->first);

Inflates an object from the Bedrijf schema

=cut

sig new_from_related_row => 'DBIx::Class::Row';

sub new_from_related_row {
    my $self        = shift;
    my $related_row = shift;

    return unless ($related_row && $related_row->result_source->name eq 'bedrijf');

    my $row;
    if ($related_row->cached_authentication) {
        $row = $related_row->cached_authentication->[0];
    } else {
        $row         = $related_row->result_source->schema->resultset('BedrijfAuthenticatie')->search(
            {
                gegevens_magazijn_id => $related_row->id,
            }
        )->first;
    }

    return unless $row;

    return $self->new_from_dbix($row);
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::JSON::DbAccessors;

use Moose::Role;
use Data::Dumper;


=head1 ATTRIBUTES

=head2 json_property_column

default: properties

The column to search for in table row containing JSON properties.

=cut

has 'json_property_column'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => 'properties'
);

=head2 json_property_list

defaul: EMPTY ARRAYREF

The list of columns to create accessors for

=cut

# has 'json_property_list'  => (
#     'is'        => 'ro',
#     'lazy'      => 1,
#     'default'   => sub { []; }
# );


=head1 METHODS

=head2 get_all_columns()

Return value: %hash_of_column_value_pairs

Returns a hash containing the columns from the json information in the raw
subject table added to the current subject table rows.

In other words: get_columns concatenated with DbAccessors from the json
property columns

=cut

sub get_all_columns {
    my $self            = shift;

    my %column_data     = $self->get_columns;

    for my $column (@{ $self->json_property_list }) {
        $column_data{ $column } = $self->json_data->{$column};
    }

    ### Drop json property column
    delete($column_data{ $self->json_property_column });

    return %column_data;
}

=head2 json_data

Return value: $perl_json_data

Returns the hashed version of the properties column of the subject row

=cut

has 'json_data'     => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        my $json_property_column = $self->json_property_column;

        return $self->$json_property_column || {};
    }
);


=head1 PRIVATE METHODS

=head2 _generate_jsonaccessors

Return value: $true of Error

Generates the accessors according to the XML definition, accessors will read
the data from the properties json column,

=cut

sub _generate_jsonaccessors {
    my $self        = shift;

    my $columns     = $self->json_property_list;

    for my $column (@{ $columns }) {
        next if $self->can($column);
        $self->_generate_jsonaccessor($column);
    }

    return 1;
}


=head2 _generate_jsonaccessor($accessor_name)

Return value: $true or Error

Generates an accessor for the name in the properties-json hash.
=cut

sub _generate_jsonaccessor {
    my $self        = shift;
    my $accessor    = shift;
    my $opts        = shift || {};

    $self->meta->add_attribute($accessor,
        'is'        => (defined($opts->{is}) ? $opts->{is} : 'rw'),
        'lazy'      => 1,
        'default'   => sub {
            my $self    = shift;

            return $self->json_data->{ $accessor };
        },
        'trigger'   => sub {
            my $self        = shift;
            my ($new, $old) = @_;

            $self->json_data->{ $accessor } = $new;

            $self->properties(
                $self->json_data
            );

            return $new;
        }
    );
}

sub _reload_json_properties {
    my $self        = shift;

    $self->json_data(
        $self->properties
    );

    for my $key (keys %{ $self->json_data }) {
        $self->{ $key } = $self->json_data->{ $key };
    }
}

1;

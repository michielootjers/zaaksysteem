package Zaaksysteem::JSON::Serializer;

use DateTime::Format::ISO8601;
use JSON qw//;

=head2 from_json($JSON_STRING)

Return value: $PERL_HASH

    Zaaksysteem::JSON::Serializer::from_json($JSON_STRING);

Returns a perl hash, where the following objects are deserialized

=over 4

=item DateTime

    __DateTime__: '2014-01-01T09:00:00'

    Objects serialized with the above key will be serialized back to
    DateTime objects

=back

=cut

sub from_json {
    my $class           = shift;
    my $val             = shift;

    JSON
      ->new
      ->filter_json_single_key_object (__DateTime__ => sub {
          return DateTime::Format::ISO8601->parse_datetime( shift );
      })
      ->decode ($val);
}

=head2 to_json($PERL_HASH)

Return value: $JSON_STRING

    Zaaksysteem::JSON::Serializer::to_json($PERL_HASH);

Returns a JSON formatted string. Objects defined in C<from_json> will be serialized.


=cut

sub to_json {
    my $class           = shift;
    my $val             = shift;

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub { { __DateTime__ => shift->iso8601 } };

    return JSON
        ->new
        ->utf8(1)
        ->allow_blessed(1)
        ->convert_blessed(1)
        ->encode($val);
}


1;

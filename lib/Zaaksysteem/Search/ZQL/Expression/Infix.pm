package Zaaksysteem::Search::ZQL::Expression::Infix;

use Moose;

use Zaaksysteem::Search::Conditional;

extends 'Zaaksysteem::Search::ZQL::Expression';

has operator => ( is => 'ro', required => 1 );
has lterm => ( is => 'ro', required => 1 );
has rterm => ( is => 'ro', required => 1 );

sub new_from_production {
    my $class = shift;
    my %item = @_;

    return $class->new(
        lterm => $item{ term },
        operator => $item{ infix_operator },
        rterm => $item{ 'infix_expression_tail(1)' }[0]
    );
}

sub dbixify {
    my $self = shift;
    my $cmd = shift;

    return Zaaksysteem::Search::Conditional->new(
        lterm => $self->lterm->dbixify($cmd),
        operator => $self->operator->dbixify($cmd),
        rterm => $self->rterm->dbixify($cmd)
    );
}

1;

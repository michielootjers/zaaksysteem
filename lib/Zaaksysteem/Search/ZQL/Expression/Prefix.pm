package Zaaksysteem::Search::ZQL::Expression::Prefix;

use Moose;

use Data::Dumper;

extends 'Zaaksysteem::Search::ZQL::Expression';

has operator => ( is => 'ro', required => 1 );
has term => ( is => 'ro', required => 1 );

sub new_from_production {
    my $class = shift;
    my %item = @_;

    return $class->new(
        operator => $item{ prefix_operator },
        term => $item{ term }
    );
}

sub dbixify {
    my $self = shift;
    my $cmd = shift;

    my $cond = $self->term->dbixify;

    unless (eval { $cond->isa('Zaaksysteem::Search::Conditional') }) {
        throw('search/zql', 'Term for logical inversion *must* be a conditional');
    }

    $cond->invert(1);

    return $cond;
}

1;

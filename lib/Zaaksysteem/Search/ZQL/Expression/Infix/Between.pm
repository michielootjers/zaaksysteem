package Zaaksysteem::Search::ZQL::Expression::Infix::Between;

use Moose;

use Zaaksysteem::Search::Conditional;
use Zaaksysteem::Search::Term::Literal;

extends 'Zaaksysteem::Search::ZQL::Expression';

has between_egt => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal' );
has between_elt => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal' );

has term => ( is => 'ro', required => 1 );

sub new_from_production {
    my $class = shift;
    my %item = @_;

    return $class->new(
        term => $item{ term },
        between_egt => $item{ infix_expression_between }[0],
        between_elt => $item{ infix_expression_between }[1],
    );
}

sub dbixify {
    my $self = shift;

    return Zaaksysteem::Search::Conditional->new(
        operator => 'and',
        
        lterm => Zaaksysteem::Search::Conditional->new(
            operator => '>=',
            lterm => Zaaksysteem::Search::Term::Literal->new(value => $self->term->value),
            rterm => Zaaksysteem::Search::Term::Literal->new(value => $self->between_egt->value)
        ),
        
        rterm => Zaaksysteem::Search::Conditional->new(
            operator => '<=',
            lterm => Zaaksysteem::Search::Term::Literal->new(value => $self->term->value),
            rterm => Zaaksysteem::Search::Term::Literal->new(value => $self->between_elt->value)
        )
    );
}

1;

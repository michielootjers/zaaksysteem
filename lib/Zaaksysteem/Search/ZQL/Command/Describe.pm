package Zaaksysteem::Search::ZQL::Command::Describe;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Command';

has object_type => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal::ObjectType' );

sub new_from_production {
    my $class = shift;
    my %param = @_;

    $class->new(
        object_type => $param{ object_type }
    );
}

1;

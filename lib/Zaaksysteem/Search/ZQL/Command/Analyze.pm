package Zaaksysteem::Search::ZQL::Command::Analyze;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Command';

has command => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Command' );

1;

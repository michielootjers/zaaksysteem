package Zaaksysteem::Search::ZQL::Command::Count;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Command';

=head1 NAME

Zaaksysteem::Search::ZQL::Command::Count

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 object_type

=cut

has object_type => (
    is => 'ro',
    isa => 'Zaaksysteem::Search::ZQL::Literal::ObjectType'
);

=head2 where

=cut

has where => (
    is => 'ro',
    isa => 'Maybe[Zaaksysteem::Search::ZQL::Expression]'
);

=head2 matching

=cut

has matching => (
    is => 'ro',
    isa => 'Maybe[Zaaksysteem::Search::ZQL::Literal::String]'
);

=head2 opts

=cut

has opts => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub { [] }
);

=head1 CONSTRUCTORS

=head2 new_from_production

=cut

sub new_from_production {
    my $class = shift;
    my %item = @_;

    $class->new(
        object_type => $item{ object_type },
        where => $item{ 'where(?)' }[0] || undef,
        matching => $item{ 'matching(?)' }[0] || undef,
        opts => $item{ 'query_opt(s?)' },
    );
}

=head1 METHODS

=head2 apply_to_resultset

=cut

sub apply_to_resultset {
    my $self = shift;
    my $rs = shift;

    $rs = $rs->search({ object_class => $self->object_type->value });

    if ($self->matching && length $self->matching->value) {
        $rs = $rs->search_text_vector($self->matching->value);
    }

    $rs = $rs->search_hstore(
        $self->dbixify_args,
        $self->dbixify_opts
    );

    return $rs;
}

=head2 dbixify_args

=cut

sub dbixify_args {
    my $self = shift;

    return {} unless $self->where;

    return $self->where->dbixify($self);
}

=head2 dbixify_opts

=cut

sub dbixify_opts {
    my $self = shift;

    return {
        map { $self->unroll_option($_) } @{ $self->opts }
    };
}

=head2 unroll_option

=cut

sub unroll_option {
    my $self = shift;
    my $option = shift;

    if(exists $option->{ limit }) {
        return rows => $option->{ limit }->value;
    }

    if(exists $option->{ group_by }) {
        return group_by => $option->{ group_by };
    }

    if(exists $option->{ order_by }) {
        my $column = $option->{ order_by };
        my $sort_style = $option->{ sort_style } || 'alphanumeric';

        return order_by => { $sort_style => [ $column ] };
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Search::ZQL::Command::Select;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Command';

has probes => ( is => 'ro', isa => 'ArrayRef' );
has from => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal::ObjectType' );
has where => ( is => 'ro', isa => 'Maybe[Zaaksysteem::Search::ZQL::Expression]' );
has matching => ( is => 'ro', isa => 'Maybe[Zaaksysteem::Search::ZQL::Literal::String]' );
has opts => ( is => 'ro', isa => 'ArrayRef', default => sub { [] } );
has distinct => ( is => 'ro' );
has describe => ( is => 'ro' );

sub new_from_production {
    my $class = shift;
    my %item = @_;

    $class->new(
        probes => $item{ 'probe(s)' },
        from => $item{ from },
        where => $item{ 'where(?)' }[0] || undef,
        matching => $item{ 'matching(?)' }[0] || undef,
        opts => $item{ 'query_opt(s?)' },
        distinct => (scalar @{ $item{ 'distinct(?)' } } > 0),
        describe => (scalar @{ $item{ 'describe(?)' } } > 0)
    );
}

sub apply_to_resultset {
    my $self = shift;
    my $rs = shift;

    $rs = $rs->search({ object_class => $self->object_type });

    if ($self->probes) {
        $rs->object_requested_attributes([
            map { $_->value }
            grep { $_->isa('Zaaksysteem::Search::ZQL::Literal::Column') } @{ $self->probes }
        ]);
    }

    if ($self->describe) {
        $rs->describe_rows(1);
    }

    if ($self->matching && length $self->matching->value) {
        $rs = $rs->search_text_vector($self->matching->value);
    }

    return $rs->search_hstore(
        $self->dbixify_args,
        $self->dbixify_opts
    );
}

sub object_type {
    return shift->from->value;
}

sub dbixify_args {
    my $self = shift;

    return {} unless $self->where;

    return $self->where->dbixify($self);
}

sub dbixify_opts {
    my $self = shift;

    my $ret = {
        map { $self->unroll_option($_) } @{ $self->opts }
    };

    my @columns = map { $_->value } @{ $self->probes };
    my @as;

    for my $probe (@{ $self->probes }) {
        my $val = $probe->value;

        $val =~ s/\./\$/g;

        push @as, $val;
    }

    if($self->distinct) {
        $ret->{ select } = \@columns;
        $ret->{ group_by } = \@columns;
        $ret->{ as } = \@as;
    }

    return $ret;
}

sub unroll_option {
    my $self = shift;
    my $option = shift;

    if(exists $option->{ limit }) {
        return rows => $option->{ limit }->value;
    }

    if(exists $option->{ group_by }) {
        return group_by => $option->{ group_by };
    }

    if(exists $option->{ order_by }) {
        my $column = $option->{ order_by };
        my $sort_style = $option->{ sort_style } || 'alphanumeric';

        return order_by => { $sort_style => [ $column ] };
    }
}

1;

package Zaaksysteem::Search::ZQL::Syntax;

use base 'Exporter';

use Zaaksysteem::Search::ZQL::Literal::String;
use Zaaksysteem::Search::ZQL::Literal::Number;
use Zaaksysteem::Search::ZQL::Literal::Null;
use Zaaksysteem::Search::ZQL::Literal::Set;
use Zaaksysteem::Search::ZQL::Literal::DateTime;
use Zaaksysteem::Search::ZQL::Literal::JPath;
use Zaaksysteem::Search::ZQL::Literal::Boolean;
use Zaaksysteem::Search::ZQL::Literal::Column;
use Zaaksysteem::Search::ZQL::Literal::ObjectType;
use Zaaksysteem::Search::ZQL::Literal::Function;

use Zaaksysteem::Search::ZQL::Expression::Infix;
use Zaaksysteem::Search::ZQL::Expression::Infix::Between;
use Zaaksysteem::Search::ZQL::Expression::Prefix;

use Zaaksysteem::Search::ZQL::Operator::Infix;
use Zaaksysteem::Search::ZQL::Operator::Prefix;

use Zaaksysteem::Search::ZQL::Command::Select;
use Zaaksysteem::Search::ZQL::Command::Describe;
use Zaaksysteem::Search::ZQL::Command::Analyze;

our @EXPORT = qw[zql_command zql_literal zql_op zql_expr];

sub zql_command {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Command::%s', shift);

    return $package->new_from_production(@_);
}

sub zql_literal {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Literal::%s', shift);

    return $package->new_from_production(@_);
}

sub zql_op {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Operator::%s', shift);

    return $package->new_from_production(@_);
}

sub zql_expr {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Expression::%s', shift);

    return $package->new_from_production(@_);
}

1;

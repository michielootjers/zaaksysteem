package Zaaksysteem::Search::ZQL::Operator::Infix;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Operator';

sub dbixify {
    my $self = shift;

    my %dispatch = (
        '~' => 'ILIKE',
    );

    if(exists $dispatch{ $self->operator }) {
        return $dispatch{ $self->operator };
    } else {
        return $self->operator;
    }
}

1;

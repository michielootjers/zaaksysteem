package Zaaksysteem::Search::ZQL::Literal::ObjectType;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'Str' );

1;

package Zaaksysteem::Search::ZQL::Literal::String;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => (
    isa => 'Str'
);

around new_from_production => sub {
    my $orig = shift;
    my $class = shift;

    my $string = substr shift, 1, -1;

    $string =~ s[\\][];

    $class->$orig($string);
};

1;

package Zaaksysteem::Search::ZQL::Literal::Number;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'Num' );

1;

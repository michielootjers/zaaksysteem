package Zaaksysteem::Search::ZQL::Literal::JPath;

use Moose;

use JSON::Path;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'JSON::Path' );

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %param = @_;

    $class->$orig(
        value => JSON::Path->new($param{ value })
    );
};

1;

package Zaaksysteem::Search::ZQL::Literal::Null;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has value => ( is => 'ro', isa => 'Undef' );

1;

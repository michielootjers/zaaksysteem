package Zaaksysteem::Search::ZQL::Literal::Boolean;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'Num' );

1;

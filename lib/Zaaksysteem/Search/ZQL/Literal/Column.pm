package Zaaksysteem::Search::ZQL::Literal::Column;

use Moose;

use Zaaksysteem::Search::Term::Column;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'Str' );

override dbixify => sub {
    my $self = shift;
    my $cmd = shift;

    return Zaaksysteem::Search::Term::Column->new(value => $self->value);
};

1;

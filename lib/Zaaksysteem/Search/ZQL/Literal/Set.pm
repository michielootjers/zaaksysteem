package Zaaksysteem::Search::ZQL::Literal::Set;

use Moose;

use Zaaksysteem::Search::Term::Set;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'ArrayRef' );

sub dbixify {
    return Zaaksysteem::Search::Term::Set->new(
        values => [ map { $_->dbixify } @{ shift->value } ]
    );
}

1;

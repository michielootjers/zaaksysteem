package Zaaksysteem::Search::ZQL::Literal::DateTime;

use Moose;

use DateTime::Format::Strptime;

use Zaaksysteem::Search::Term::Literal;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'DateTime' );

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %param = @_;

    my $string = $param{ value };

    # Strip the millisecond and Z components
    $string =~ s/(\.\d{1,3})?Z//;

    my $format = DateTime::Format::Strptime->new(
        pattern => '%FT%T', # 2001-01-01T12:34:56Z
        time_zone => 'UTC'   # UTC because of the Z part in the pattern
    );

    $class->$orig(
        value => $format->parse_datetime($string)
    );
};

sub dbixify {
    my $self = shift;

    return Zaaksysteem::Search::Term::Literal->new(value => $self->value);
}

1;

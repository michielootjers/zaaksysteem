package Zaaksysteem::Search::ZQL::Literal;

use Moose;

use Zaaksysteem::Search::Term::Literal;

has 'value' => ( is => 'ro', isa => 'Defined', required => 1 );

sub new_from_production {
    my $class = shift;

    return $class->new(value => shift);
}

sub dbixify {
    return Zaaksysteem::Search::Term::Literal->new(value => shift->value);
}

1;

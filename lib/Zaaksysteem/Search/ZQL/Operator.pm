package Zaaksysteem::Search::ZQL::Operator;

use Moose;

has 'operator' => ( is => 'ro', required => 1 );

sub new_from_production {
    my $class = shift;

    return $class->new(
        operator => shift
    );
}

sub dbixify {
    return shift->operator;
}

1;

package Zaaksysteem::Search::Term::Column;

use Moose;

extends 'Zaaksysteem::Search::Term';

use Moose::Util qw(ensure_all_roles);

=head1 NAME

Zaaksysteem::Search::Term::Column - "Column" term of a conditional

=head1 ATTRIBUTES

=head2 value

The columnname of this term.

=head1 METHODS

=head2 evaluate

Evaluate the term. Returns a list containing only the value as a string

=cut

has value => (
    is  => 'ro',
    isa => 'Str',
);

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    # This is a bit of a hack to make hstore queries faster.
    #
    # SELECT hstore_field @> "key = value"; is *way* faster than
    # SELECT hstore_field->'key' = 'value';
    if ($conditional->operator eq '=') {
        ensure_all_roles($self, 'Zaaksysteem::Search::Term::HStoreColumn');

        $self->column($self->value);
        $conditional->operator("@>");

        return ($resultset->hstore_column);
    }

    my $column = $resultset->map_hstore_key($self->value);

    if($conditional->rterm->isa('Zaaksysteem::Search::Term::Literal')) {
        my $rtype = $conditional->rterm->guess_type();

        if ($rtype ne 'TEXT') {
            $column = "CAST($column AS $rtype)";
        }
    }

    return ($column);
};

1;

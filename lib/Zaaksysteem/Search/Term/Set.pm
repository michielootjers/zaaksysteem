package Zaaksysteem::Search::Term::Set;
use Moose;

extends 'Zaaksysteem::Search::Term';

=head1 NAME

Zaaksysteem::Search::Term::Literal - "Literal" term of a conditional

=head1 ATTRIBUTES

=head2 value

The value of this term.

=head1 METHODS

=head2 evaluate

Evaluate the term. Returns an array:

    "?", $self->value

=cut

has values => (
    is  => 'ro',
    isa => 'ArrayRef[Zaaksysteem::Search::Term::Literal]',
);

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    my @args;
    my @sql_parts;

    for my $item (@{ $self->values }) {
        my @subargs = $item->evaluate($resultset, $conditional);

        push @sql_parts, shift @subargs;
        push @args, @subargs;
    }
    
    return (sprintf('( %s )', join(', ', @sql_parts)), @args);
};

__PACKAGE__->meta->make_immutable();

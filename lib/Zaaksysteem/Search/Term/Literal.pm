package Zaaksysteem::Search::Term::Literal;
use Moose;

extends 'Zaaksysteem::Search::Term';

use Encode qw(encode_utf8);
use Moose::Util::TypeConstraints qw(find_type_constraint);
use Pg::hstore;
use Scalar::Util qw(blessed);

=head1 NAME

Zaaksysteem::Search::Term::Literal - "Literal" term of a conditional

=head1 ATTRIBUTES

=head2 value

The value of this term.

=head1 METHODS

=head2 evaluate

Evaluate the term. Returns an array:

    "?", $self->value

=cut

has value => (
    is       => 'ro',
    required => 1,
);

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    my $value = $self->value;

    if ($conditional->lterm->does('Zaaksysteem::Search::Term::HStoreColumn')) {
        my $type = $self->guess_type($value);
        if ($type eq 'TIMESTAMP') {
            $value = $value->iso8601;
        }

        $value = Pg::hstore::encode({ encode_utf8($conditional->lterm->column) => $value });
    }

    return ("?", [{} => encode_utf8($value)]);
};

{
    my %types = (
        Int => find_type_constraint('Int'),
        Num => find_type_constraint('Num'),
    );

    sub guess_type {
        my $self = shift;

        if (blessed($self->value) && $self->value->isa('DateTime')) {
            return 'TIMESTAMP';
        }
        elsif ($types{Int}->check($self->value)) {
            return 'NUMERIC';
        }
        elsif ($types{Num}->check($self->value)) {
            return 'NUMERIC';
        }

        return 'TEXT';
    }
}

__PACKAGE__->meta->make_immutable();

package Zaaksysteem::Search::Term::HStoreColumn;
use Moose::Role;

=head1 NAME

Zaaksysteem::Search::Term::HStoreColumn - Specialization of "column" term

=head1 DESCRIPTION

This is a role a column can take on when it's being split up across a conditional:

From:

    SELECT foo FROM case WHERE foo = 'bar'

To:

    SELECT foo FROM case WHERE hstore_field @> '"foo" => "bar"'

=head1 ATTRIBUTES

=head2 column

The "hstore" field name which should be used by the right side of the
conditional to construct a HStore lookup.

=cut

has 'column' => (
    is => 'rw',
);

1;

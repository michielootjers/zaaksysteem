package Zaaksysteem::Search::HStoreResultSet;
use Moose::Role;
use namespace::autoclean;

use Encode qw(encode_utf8);
use Scalar::Util qw(blessed);
use Zaaksysteem::Exception;

requires ('search', 'hstore_column');

=head1 NAME

Zaaksysteem::Search::HStoreResultSet - Role to perform searches using an HSTORE column.

=head1 METHODS

=head2 search_hstore

A wrapper around "search()" that queries a column of type HSTORE on the table
(as defined by \$self->hstore_column).

=head3 Arguments

This method takes two positional arguments:

=over

=item * query

Shallow hash containing the hstore keys and values to search for.

=item * options

Query options. Passed to C<< DBIx::Class::ResultSet->search() >>.

=back

=head3 Returns

Returns a L<DBIx::Class::ResultSet>.

=cut

sub search_hstore {
    my $self = shift;
    my ($query, $options) = @_;

    my $hprop_query = blessed($query) ? $query->evaluate($self) : $query;
    my $hprop_options = $self->_parse_options($options);

    return $self->search(
        $hprop_query,
        $hprop_options,
    );
}

sub _parse_options {
    my $self = shift;
    my ($options) = @_;

    return if not defined $options;

    my %rv;
    for my $key (keys %$options) {
        if ($key eq 'order_by') {
            $rv{$key} = $self->_parse_option_order_by($options->{$key});
        }
        elsif ($key eq 'select') {
            $rv{$key} = $self->_parse_option_select($options->{$key});
        }
        elsif ($key eq 'group_by') {
            $rv{ $key } = $self->_parse_option_group_by($options->{ $key });
        }
        else {
            $rv{$key} = $options->{$key};
        }
    }

    # If the query uses a group_by, add the counts for rows grouped by default
    # unless 'no_count' is truthy. Map the column to name 'count'.
    if (!$rv{ no_count } && exists $rv{ group_by }) {
        $rv{ select } = [ @{ $rv{ select } // [] }, { count => $self->hstore_column } ];
        $rv{ as } = [ @{ $rv{ as } // [] }, 'count' ];
    }

    return \%rv;
}

sub _parse_option_select {
    my $self = shift;
    my ($select) = @_;

    if (ref($select) eq 'ARRAY') {
        my @rv;

        for my $select_element (@$select) {
            push @rv, $self->_parse_option_select($select_element);
        }

        return \@rv;
    }
    elsif (ref($select) eq 'HASH') {
        if(keys(%$select) != 1 || !exists($select->{distinct})) {
            throw(
                'hstore/select_distinct',
                "Could not parse 'select' option. Expected something like: "
                . "select => [ { distinct => 'col' } ]"
            );
        }

        return {
            distinct => $self->map_hstore_key($select->{distinct}),
        };
    }
    else {
        # Don't know what called passed to us, let DBIx::Class sort out the error.
        return $self->map_hstore_key($select);
    }

    return;
}

sub _parse_option_group_by {
    my $self = shift;
    my ($group_by) = @_;

    unless (ref $group_by eq 'ARRAY') {
        throw('hstore/wrong_group_by', 'Expected an ARRAY reference for group by ( [ $column1, $column2, ... ] )');
    }

    my @rv;

    for my $column (@{ $group_by }) {
        push @rv, $self->map_hstore_key($column);
    }

    return \@rv;
}

sub _parse_option_order_by {
    my $self = shift;
    my ($order_by) = @_;

    my @rv;

    unless (ref $order_by eq 'HASH') {
        throw('hstore/wrong_order_by', 'Expecting a HASH ref; { alphanumeric => [ $column, ... ] }');
    }

    for my $style (keys %{ $order_by }) {
        my $colspec = $order_by->{ $style };

        unless (ref $colspec eq 'ARRAY') {
            throw('hstore/wrong_order_by_column', 'Expecting nested ARRAY ref; [ $column, ... ]');
        }

        for my $subcolspec (@{ $colspec }) {
            for my $direction (keys %{ $subcolspec }) {
                my $column = $self->map_hstore_key($subcolspec->{ $direction });

                if ($style eq 'numeric') {
                    $column = \[ "(NULLIF(SUBSTRING($column FROM '([0-9]+(\\.[0-9]+)?)'), ''))::NUMERIC" ];
                }

                push @rv, {
                    $direction => $column
                };
            }
        }
    }

    return \@rv;
}

sub map_hstore_key {
    my $self = shift;
    my ($k) = @_;

    my $dbh = $self->result_source->storage->dbh;

    return sprintf("%s->%s", $self->hstore_column, encode_utf8($dbh->quote($k)));
}

1;

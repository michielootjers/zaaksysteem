package Zaaksysteem::Search::Conditional::IsNull;

use Moose::Role;

=head1 NAME

Zaaksysteem::Search::Conditional::IsNull - Special case for IS NULL conditions

=head1 METHODS

=head2 evaluate

Overriden evaluate behavior (see L<Zaaksysteem::Search::Conditional>).

Since defined-ness checks for HStore properties require special syntax, this
role provides an evaluate method that returns some special case SQL string.

    $is_null_cond->evaluate()
    => (defined(hstore_column, key) IS FALSE)

=cut

override evaluate => sub {
    my $self = shift;
    my $rs = shift;

    my $left = $rs->hstore_column;
    my $key = $rs->result_source->storage->dbh->quote($self->lterm->value);
    my $expect = $self->invert ? 'TRUE' : 'FALSE';

    return \[ "(defined($left, $key) IS $expect)" ];
};

1;

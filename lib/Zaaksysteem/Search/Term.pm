package Zaaksysteem::Search::Term;
use Moose;

=head1 NAME

Zaaksysteem::Search::Term - Interface-only role for search term classes

=head1 METHODS

This base class requires its subclasses to implement one method:

=over

=item * evaluate

A method that evaluates the term (if applicable) and returns the SQL
representation as:

    ($string_with_placeholders, @placeholder_values)

=back

=cut

sub evaluate {
    ...
}

__PACKAGE__->meta->make_immutable();

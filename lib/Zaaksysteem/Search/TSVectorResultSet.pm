package Zaaksysteem::Search::TSVectorResultSet;

use Moose::Role;
use namespace::autoclean;

use Zaaksysteem::Exception;

use Encode qw[encode_utf8];

requires qw[search text_vector_column];

=head1 NAME

Zaaksysteem::Search::TSVectorResultSet - Role to perform searches using a
TSVECTOR document-search style column.

=head1 SYNOPSIS

Usage of the TSVectorResultSet role is pretty straightforward. In order to
extend your resultset with the role you must implement a C<text_vector_column>
method so that the C<search_text_vector> method knows what column to select on.

    my ResultSet::Class

    use Moose;

    with Zaaksysteem::Search::TSVectorResultSet

    sub text_vector_column { 'text_vector' }

Now you can use an instance of your resultset like this

    $rs = $rs->search_text_vector(...);

=head1 METHODS

=head2 search_text_vector

This method is a simple frontend to the C<TSVECTOR> functionality Postgres
provides. It expects a single string scalar argument, which will be processed
into a C<TSQUERY> Postgres type.

    $rs = $rs->search_text_vector('query test');

The above resultset will expand into C<'query & test'::tsquery>. The string is
split on space-like characters, which are strung together implicly as individual
conditions to match on. So the above query will only match if query & test both
match against the C<TSVECTOR> column.

=cut

sub search_text_vector {
    my $self = shift;

    unless(scalar @_) { 
        return $self;
    }

    my @keywords;

    for my $keyword (map { split m[\s+] } @_) {
        $keyword =~ s/[^\w]//g;

        next unless length $keyword;

        push @keywords, sprintf('%s:*', $keyword);
    }

    unless(scalar @keywords) {
        return $self;
    }

    return $self->search({
        $self->text_vector_column => {
            '@@' => lc(join ' & ', @keywords)
        }
    });
}

1;

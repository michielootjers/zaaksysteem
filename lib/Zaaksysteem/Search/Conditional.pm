package Zaaksysteem::Search::Conditional;

use Moose;
use namespace::autoclean;

use Moose::Util qw[ensure_all_roles];

use Zaaksysteem::Search::Term;

=head1 NAME

Zaaksysteem::Search::Conditional - Map ZQL conditionals to SQL

=head1 DESCRIPTION

Class that maps ZQL conditionals to SQL

=head1 SYNOPSIS

    $left_term = Zaaksysteem::Search::Term->new(...);
    $right_term = Zaaksysteem::Search::Term->new(...);

    $cond = Zaaksysteem::Search::Conditional->new(
        lterm  => $left_term,
        rterm => $right_term,
        operator   => '=',
    );

=head1 ATTRIBUTES

=over

=item * lterm

Left side of the condition. Should be a L<Zaaksysteem::Search::Term>.

=item * rterm

Right side of the condition. Should be a L<Zaaksysteem::Search::Term>.

=item * operator

The operator (like "=" or ">", etc.)

=item * invert

State flag indicating if the condition should logically invert (NOT) the
condition.

=back

=cut

has lterm => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Search::Term | Zaaksysteem::Search::Conditional',
    required => 1,
);

has rterm => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Search::Term | Zaaksysteem::Search::Conditional',
    required => 1,
);

has operator => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has invert => (
    is => 'rw',
    isa => 'Bool'
);

sub BUILD {
    my $self = shift;

    if($self->is_is_null) {
        ensure_all_roles($self, 'Zaaksysteem::Search::Conditional::IsNull');
    }
}

=head1 METHODS

=head2 is_is_null

This method heuristically inspects the instance to check if it's a generic
"IS NULL" kind of construct. Since ZQL parses and constructs conditions like
C<case.number = NULL>, and Postgres's Hstore needs some funky syntax to make
defined-ness checks work we need some way of detecting the condition.

=cut

sub is_is_null {
    my $self = shift;

    return unless $self->operator eq '=';
    return unless $self->lterm->isa('Zaaksysteem::Search::Term::Column');
    return unless $self->rterm->isa('Zaaksysteem::Search::Term::Literal');
    return if defined $self->rterm->value;

    return 1;
}

=head2 evaluate

Evaluate the conditional.

This method will evaluate the left and right sides, and put the resulting query
part into an SQL fragment.

=cut

sub evaluate {
    my $self = shift;
    my ($resultset) = @_;

    my ($left, @largs)  = $self->lterm->evaluate($resultset, $self);
    my ($right, @rargs) = $self->rterm->evaluate($resultset, $self);
    my $op              = $self->operator;

    if (ref $left eq 'REF') {
        my @unpack = @{ $$left };

        $left = shift @unpack;
        push @largs, @unpack;
    }

    if (ref $right eq 'REF') {
        my @unpack = @{ $$right };

        $right = shift @unpack;
        push @rargs, @unpack;
    }

    if ($self->invert) {
        return \[ "(NOT ($left $op $right))", @largs, @rargs ];
    }

    return \[ "($left $op $right)", @largs, @rargs ];
}

__PACKAGE__->meta->make_immutable();

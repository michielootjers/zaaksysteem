package Zaaksysteem::SBUS::Types::StUF::VBO;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

sub _stuf_to_params {
    my ($self, $vbo_xml, $stuf_options) = @_;

    my $params = $self->_convert_stuf_to_hash(
        $vbo_xml,
        {
            brutoVloerOppervlak => 'verblijfsobject_oppervlakte',
        }
    );

    return $params;
}

sub _stuf_relaties {
    my ($self, $vbo_xml, $stuf_options, $create_options) = @_;

    my $address = $self->handle_adr_relations($vbo_xml, 'VBO');

    if ($vbo_xml->{extraElementen}) {
        my $extra = $self->_convert_stuf_extra_elementen_to_hash(
            $vbo_xml->{extraElementen}
        );

        my $mapping = STUF_VBO_MAPPING_EXTRA;

        for my $key (keys %{ $mapping }) {
            next unless defined($extra->{ $key });

            $create_options->{
                $mapping->{ $key }
            } = $extra->{ $key };
        }
    }

    $create_options->{ $_ } = $address->{ $_ }
        for keys %{ $address };

    if (
        defined($create_options->{verblijfsobject_gebruiksdoel}) &&
        $create_options->{verblijfsobject_gebruiksdoel}
    ) {
        $create_options->{verblijfsobject_gebruiksdoel} = [
            $create_options->{verblijfsobject_gebruiksdoel}
        ];
    }

    return $create_options;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

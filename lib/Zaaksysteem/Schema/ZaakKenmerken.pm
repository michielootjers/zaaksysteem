package Zaaksysteem::Schema::ZaakKenmerken;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaakKenmerken

=cut

__PACKAGE__->table("zaak_kenmerken");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_kenmerken_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 value_type

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 multiple

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_kenmerken_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "value_type",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "multiple",
  { data_type => "boolean", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });

=head2 zaak_kenmerken_values

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakKenmerkenValues>

=cut

__PACKAGE__->has_many(
  "zaak_kenmerken_values",
  "Zaaksysteem::Schema::ZaakKenmerkenValues",
  { "foreign.zaak_kenmerken_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-03 09:58:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wT53pu40c364hT3PWc4p7w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

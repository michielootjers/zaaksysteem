package Zaaksysteem::Schema::ZaaktypeKenmerken;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaaktypeKenmerken

=cut

__PACKAGE__->table("zaaktype_kenmerken");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_kenmerken_id_seq'

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 value_mandatory

  data_type: 'integer'
  is_nullable: 1

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaak_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 pip

  data_type: 'integer'
  is_nullable: 1

=head2 zaakinformatie_view

  data_type: 'integer'
  default_value: 1
  is_nullable: 1

=head2 bag_zaakadres

  data_type: 'integer'
  is_nullable: 1

=head2 is_group

  data_type: 'integer'
  is_nullable: 1

=head2 date_fromcurrentdate

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 value_default

  data_type: 'text'
  is_nullable: 1

=head2 pip_can_change

  data_type: 'boolean'
  is_nullable: 1

=head2 publish_public

  data_type: 'integer'
  is_nullable: 1

=head2 referential

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 is_systeemkenmerk

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 required_permissions

  data_type: 'text'
  is_nullable: 1

=head2 version

  data_type: 'integer'
  is_nullable: 1

=head2 help_extern

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_kenmerken_id_seq",
  },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "value_mandatory",
  { data_type => "integer", is_nullable => 1 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaak_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "pip",
  { data_type => "integer", is_nullable => 1 },
  "zaakinformatie_view",
  { data_type => "integer", default_value => 1, is_nullable => 1 },
  "bag_zaakadres",
  { data_type => "integer", is_nullable => 1 },
  "is_group",
  { data_type => "integer", is_nullable => 1 },
  "date_fromcurrentdate",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "value_default",
  { data_type => "text", is_nullable => 1 },
  "pip_can_change",
  { data_type => "boolean", is_nullable => 1 },
  "publish_public",
  { data_type => "integer", is_nullable => 1 },
  "referential",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "is_systeemkenmerk",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "required_permissions",
  { data_type => "text", is_nullable => 1 },
  "version",
  { data_type => "integer", is_nullable => 1 },
  "help_extern",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 file_case_documents

Type: has_many

Related object: L<Zaaksysteem::Schema::FileCaseDocument>

=cut

__PACKAGE__->has_many(
  "file_case_documents",
  "Zaaksysteem::Schema::FileCaseDocument",
  { "foreign.case_document_id" => "self.id" },
  {},
);

=head2 zaak_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaak_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaak_status_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-12-29 19:05:55
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1WDQ++wfSQcA6YiwqGUYBg

#__PACKAGE__->add_columns(value_default => { 
#    data_type => "text",
#    default_value => undef,
#    is_nullable => 1,
#    size => undef,
#    accessor => '_value_default', 
#});

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeKenmerken",
    "+DBIx::Class::Helper::Row::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
  { join_type   => 'left' },
);

__PACKAGE__->belongs_to(
  "library_attribute",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

__PACKAGE__->belongs_to(
  "zaaktype_sjablonen",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { 'zaaktype_node_id' => "zaaktype_node_id" },
);

__PACKAGE__->belongs_to(
  "zaaktype_node",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { 'id' => "zaaktype_node_id" },
);

__PACKAGE__->add_columns("+label", { is_serializable => 1});


# You can replace this text with custom content, and it will be preserved on regeneration
1;

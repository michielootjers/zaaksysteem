package Zaaksysteem::Schema::Subject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Subject

=cut

__PACKAGE__->table("subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'subject_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 subject_type

  data_type: 'text'
  is_nullable: 0

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 settings

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 username

  data_type: 'text'
  is_nullable: 0
  original: {data_type => "varchar"}

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "subject_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "subject_type",
  { data_type => "text", is_nullable => 0 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "settings",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "username",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("subject_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 user_entities

Type: has_many

Related object: L<Zaaksysteem::Schema::UserEntity>

=cut

__PACKAGE__->has_many(
  "user_entities",
  "Zaaksysteem::Schema::UserEntity",
  { "foreign.subject_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-12-10 10:28:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:TPG9+iyJycmUg7c/vXVtqQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Subject::ResultSet');

use JSON;

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Subject::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->inflate_column('properties', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) }
});

__PACKAGE__->inflate_column('settings', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) }
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

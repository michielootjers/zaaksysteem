package Zaaksysteem::Schema::BagNummeraanduiding;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::BagNummeraanduiding

=head1 DESCRIPTION

11.2 : een nummeraanduiding is een door de gemeenteraad als zodanig toegekende aanduiding van een adresseerbaar object.

=cut

__PACKAGE__->table("bag_nummeraanduiding");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

11.02 : de unieke aanduiding van een nummeraanduiding.

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

11.62 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een nummeraanduiding een wijziging hebben ondergaan.

=head2 einddatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

11.63 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een nummeraanduiding.

=head2 huisnummer

  data_type: 'integer'
  is_nullable: 0

11.20 : een door of namens het gemeentebestuur ten aanzien van een adresseerbaar object toegekende nummering.

=head2 officieel

  data_type: 'varchar'
  is_nullable: 1
  size: 1

11.21 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.

=head2 huisletter

  data_type: 'varchar'
  is_nullable: 1
  size: 1

11.30 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende toevoeging aan een huisnummer in de vorm van een alfanumeriek teken.

=head2 huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 4

11.40 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende nadere toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter.

=head2 postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

11.60 : de door tnt post vastgestelde code behorende bij een bepaalde combinatie van een straatnaam en een huisnummer.

=head2 woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 4

11.61 : unieke aanduiding van de woonplaats waarbinnen het object waaraan de nummeraanduiding is toegekend is gelegen.

=head2 inonderzoek

  data_type: 'varchar'
  is_nullable: 0
  size: 1

11.64 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.

=head2 openbareruimte

  data_type: 'varchar'
  is_nullable: 0
  size: 16

11.65 : de unieke aanduiding van een openbare ruimte waaraan een adresseerbaar object is gelegen.

=head2 type

  data_type: 'varchar'
  is_nullable: 0
  size: 20

11.66 : de aard van een als zodanig benoemde nummeraanduiding.

=head2 documentdatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

11.67 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden.

=head2 documentnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 20

11.68 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden, binnen een gemeente.

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 80

11.69 : de fase van de levenscyclus van een nummeraanduiding, waarin de betreffende nummeraanduiding zich bevindt.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_nummeraanduiding_id_seq'

=head2 gps_lat_lon

  data_type: 'point'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "einddatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "huisnummer",
  { data_type => "integer", is_nullable => 0 },
  "officieel",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "huisletter",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "inonderzoek",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "openbareruimte",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "type",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "documentdatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "documentnummer",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_nummeraanduiding_id_seq",
  },
  "gps_lat_lon",
  { data_type => "point", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 13:19:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:K1QhJY3086uujZXwZb2IOw

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::BagNummeraanduiding');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::BagNummeraanduiding",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
  "openbareruimte",
  "Zaaksysteem::Schema::BagOpenbareruimte",
  sub {
        my $args                    = shift;

        return {
            "$args->{foreign_alias}.identificatie"        => \"= $args->{self_alias}.openbareruimte",
        }
  },
  {
      join_type => 'LEFT'
  }
);

__PACKAGE__->has_many(
  "standplaatsen",
  "Zaaksysteem::Schema::BagStandplaats",
  { "foreign.hoofdadres" => "self.identificatie" },
  { is_foreign_key_constraint => 0 },
);

__PACKAGE__->has_many(
  "ligplaatsen",
  "Zaaksysteem::Schema::BagLigplaats",
  { "foreign.hoofdadres" => "self.identificatie" },
  { is_foreign_key_constraint => 0 },
);

__PACKAGE__->has_many(
  "verblijfsobjecten",
  "Zaaksysteem::Schema::BagVerblijfsobject",
  { "foreign.hoofdadres" => "self.identificatie" },
  { is_foreign_key_constraint => 0 },
);

__PACKAGE__->belongs_to(
    "subscription_id",
    "Zaaksysteem::Schema::ObjectSubscription",
    # {
    #     #'foreign.local_table' => 'self_resultsource.name',
    #     'foreign.local_id'    => 'self.id'
    # }

    sub {
        my $args                    = shift;

        return {
            "$args->{foreign_alias}.local_table"        => { '=' => 'BagNummeraanduiding' },
            "$args->{foreign_alias}.local_id::NUMERIC"  => \"= $args->{self_alias}.id",
        }
    },
    {
        join_type => 'LEFT'
    }
);





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


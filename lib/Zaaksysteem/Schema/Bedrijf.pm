package Zaaksysteem::Schema::Bedrijf;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Bedrijf

=cut

__PACKAGE__->table("bedrijf");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'bedrijf'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bedrijf_id_seq'

=head2 dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 hoofdvestiging_dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 hoofdvestiging_subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 vorig_dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 vorig_subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 handelsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 rechtsvorm

  data_type: 'smallint'
  is_nullable: 1

=head2 kamernummer

  data_type: 'smallint'
  is_nullable: 1

=head2 faillisement

  data_type: 'smallint'
  is_nullable: 1

=head2 surseance

  data_type: 'smallint'
  is_nullable: 1

=head2 telefoonnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 vestiging_adres

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 vestiging_straatnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 vestiging_huisnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 vestiging_huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 vestiging_postcodewoonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 vestiging_postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 vestiging_woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 24

=head2 correspondentie_adres

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 correspondentie_straatnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 correspondentie_huisnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 correspondentie_huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 correspondentie_postcodewoonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 correspondentie_postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 correspondentie_woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 24

=head2 hoofdactiviteitencode

  data_type: 'integer'
  is_nullable: 1

=head2 nevenactiviteitencode1

  data_type: 'integer'
  is_nullable: 1

=head2 nevenactiviteitencode2

  data_type: 'integer'
  is_nullable: 1

=head2 werkzamepersonen

  data_type: 'integer'
  is_nullable: 1

=head2 contact_naam

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 contact_aanspreektitel

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 contact_voorletters

  data_type: 'varchar'
  is_nullable: 1
  size: 19

=head2 contact_voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 contact_geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 95

=head2 contact_geslachtsaanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 1

=head2 authenticated

  data_type: 'smallint'
  is_nullable: 1

=head2 authenticatedby

  data_type: 'text'
  is_nullable: 1

=head2 fulldossiernummer

  data_type: 'text'
  is_nullable: 1

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1

=head2 verblijfsobject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 system_of_record

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 system_of_record_id

  data_type: 'bigint'
  is_nullable: 1

=head2 vestigingsnummer

  data_type: 'bigint'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "bedrijf",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bedrijf_id_seq",
  },
  "dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "hoofdvestiging_dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "hoofdvestiging_subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "vorig_dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "vorig_subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "handelsnaam",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "rechtsvorm",
  { data_type => "smallint", is_nullable => 1 },
  "kamernummer",
  { data_type => "smallint", is_nullable => 1 },
  "faillisement",
  { data_type => "smallint", is_nullable => 1 },
  "surseance",
  { data_type => "smallint", is_nullable => 1 },
  "telefoonnummer",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "vestiging_adres",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "vestiging_straatnaam",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "vestiging_huisnummer",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "vestiging_huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "vestiging_postcodewoonplaats",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "vestiging_postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "vestiging_woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 24 },
  "correspondentie_adres",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "correspondentie_straatnaam",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "correspondentie_huisnummer",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "correspondentie_huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "correspondentie_postcodewoonplaats",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "correspondentie_postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "correspondentie_woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 24 },
  "hoofdactiviteitencode",
  { data_type => "integer", is_nullable => 1 },
  "nevenactiviteitencode1",
  { data_type => "integer", is_nullable => 1 },
  "nevenactiviteitencode2",
  { data_type => "integer", is_nullable => 1 },
  "werkzamepersonen",
  { data_type => "integer", is_nullable => 1 },
  "contact_naam",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "contact_aanspreektitel",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "contact_voorletters",
  { data_type => "varchar", is_nullable => 1, size => 19 },
  "contact_voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "contact_geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 95 },
  "contact_geslachtsaanduiding",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "authenticated",
  { data_type => "smallint", is_nullable => 1 },
  "authenticatedby",
  { data_type => "text", is_nullable => 1 },
  "fulldossiernummer",
  { data_type => "text", is_nullable => 1 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1 },
  "verblijfsobject_id",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "system_of_record",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "system_of_record_id",
  { data_type => "bigint", is_nullable => 1 },
  "vestigingsnummer",
  { data_type => "bigint", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-04 15:46:55
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nCOJFgNlRCRPVPUfjm8CFw

__PACKAGE__->resultset_class('Zaaksysteem::SBUS::ResultSet::Bedrijf');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::Bedrijf",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
  "verblijfsobject_id",
  "Zaaksysteem::Schema::BagVerblijfsobject",
  { "identificatie" => "verblijfsobject_id" },
);

__PACKAGE__->belongs_to(
    "subscription_id",
    "Zaaksysteem::Schema::ObjectSubscription",
    # {
    #     #'foreign.local_table' => 'self_resultsource.name',
    #     'foreign.local_id'    => 'self.id'
    # }

    sub {
        my $args                    = shift;

        return {
            "$args->{foreign_alias}.local_table"        => { '=' => 'Bedrijf' },
            "$args->{foreign_alias}.local_id::NUMERIC"  => \"= $args->{self_alias}.id",
        }
    },
    { join_type => 'left' }

);




# You can replace this text with custom content, and it will be preserved on regeneration
1;

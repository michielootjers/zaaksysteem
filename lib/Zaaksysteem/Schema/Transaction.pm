package Zaaksysteem::Schema::Transaction;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Transaction

=cut

__PACKAGE__->table("transaction");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'transaction_id_seq'

=head2 interface_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 external_transaction_id

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 input_data

  data_type: 'text'
  is_nullable: 1

=head2 input_file

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 automated_retry_count

  data_type: 'integer'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 date_last_retry

  data_type: 'timestamp'
  is_nullable: 1

=head2 date_next_retry

  data_type: 'timestamp'
  is_nullable: 1

=head2 processed

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 error_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 direction

  data_type: 'varchar'
  default_value: 'incoming'
  is_nullable: 0
  size: 255

=head2 success_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 total_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 processor_params

  data_type: 'text'
  is_nullable: 1

=head2 error_fatal

  data_type: 'boolean'
  is_nullable: 1

=head2 preview_data

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "transaction_id_seq",
  },
  "interface_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "external_transaction_id",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "input_data",
  { data_type => "text", is_nullable => 1 },
  "input_file",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "automated_retry_count",
  { data_type => "integer", is_nullable => 1 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "date_last_retry",
  { data_type => "timestamp", is_nullable => 1 },
  "date_next_retry",
  { data_type => "timestamp", is_nullable => 1 },
  "processed",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "error_count",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "direction",
  {
    data_type => "varchar",
    default_value => "incoming",
    is_nullable => 0,
    size => 255,
  },
  "success_count",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "total_count",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "processor_params",
  { data_type => "text", is_nullable => 1 },
  "error_fatal",
  { data_type => "boolean", is_nullable => 1 },
  "preview_data",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 interface_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Interface>

=cut

__PACKAGE__->belongs_to(
  "interface_id",
  "Zaaksysteem::Schema::Interface",
  { id => "interface_id" },
);

=head2 input_file

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "input_file",
  "Zaaksysteem::Schema::Filestore",
  { id => "input_file" },
);

=head2 transaction_records

Type: has_many

Related object: L<Zaaksysteem::Schema::TransactionRecord>

=cut

__PACKAGE__->has_many(
  "transaction_records",
  "Zaaksysteem::Schema::TransactionRecord",
  { "foreign.transaction_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-04-24 08:53:13
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:K7liSXG1tRy1XFX1avDzbQ

use JSON;

__PACKAGE__->belongs_to(
  "interface",
  "Zaaksysteem::Schema::Interface",
  { id => "interface_id" },
  { join_type   => 'left' },
);

__PACKAGE__->belongs_to(
  "input_file",
  "Zaaksysteem::Schema::Filestore",
  { id => "input_file" },
  { join_type   => 'left' },
);

__PACKAGE__->has_many(
  "records",
  "Zaaksysteem::Schema::TransactionRecord",
  { 'foreign.transaction_id' => 'self.id' },
);

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Sysin::Transaction::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Sysin::Transaction::Component
    +DBIx::Class::Helper::Row::ToJSON
/);

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->inflate_column('processor_params', {
    inflate => sub { from_json(shift // '{}') },
    deflate => sub { to_json(shift // {}) },
});

__PACKAGE__->inflate_column('preview_data', {
    inflate => sub { from_json(shift // '{}') },
    deflate => sub { to_json(shift // {}) },
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

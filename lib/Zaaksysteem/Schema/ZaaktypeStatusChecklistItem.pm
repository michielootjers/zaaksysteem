package Zaaksysteem::Schema::ZaaktypeStatusChecklistItem;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaaktypeStatusChecklistItem

=cut

__PACKAGE__->table("zaaktype_status_checklist_item");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_status_checklist_item_id_seq'

=head2 casetype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 external_reference

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_status_checklist_item_id_seq",
  },
  "casetype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "external_reference",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 casetype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "casetype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "casetype_status_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-02-11 09:24:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/CzqPR5urhDzayHA40nOiQ

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeStatusChecklistItem');

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

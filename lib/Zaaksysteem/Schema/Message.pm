package Zaaksysteem::Schema::Message;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Message

=cut

__PACKAGE__->table("message");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'message_id_seq'

=head2 message

  data_type: 'text'
  is_nullable: 0

=head2 subject_id

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 logging_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 is_read

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "message_id_seq",
  },
  "message",
  { data_type => "text", is_nullable => 0 },
  "subject_id",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "logging_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "is_read",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 logging_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Logging>

=cut

__PACKAGE__->belongs_to(
  "logging_id",
  "Zaaksysteem::Schema::Logging",
  { id => "logging_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-10-22 09:34:35
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Nz/11xUAKIZZ0pLKzvKPAw

__PACKAGE__->belongs_to(
  "logging",
  "Zaaksysteem::Schema::Logging",
  { id => "logging_id" },
);

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Message::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Message::Component
    +DBIx::Class::Helper::Row::ToJSON
/);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

package Zaaksysteem::Schema::ZaaktypeSjablonen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ZaaktypeSjablonen

=cut

__PACKAGE__->table("zaaktype_sjablonen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_sjablonen_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 bibliotheek_sjablonen_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 zaak_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 automatisch_genereren

  data_type: 'integer'
  is_nullable: 1

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 target_format

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_sjablonen_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "bibliotheek_sjablonen_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "zaak_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "automatisch_genereren",
  { data_type => "integer", is_nullable => 1 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "target_format",
  { data_type => "varchar", is_nullable => 1, size => 5 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaak_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaak_status_id" },
);

=head2 bibliotheek_sjablonen_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonen>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_sjablonen_id",
  "Zaaksysteem::Schema::BibliotheekSjablonen",
  { id => "bibliotheek_sjablonen_id" },
);

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:02
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zCfz/OYC1eE6rDR11U18Cw
__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeSjablonen');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeSjablonen",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
    "bibliotheek_sjablonen",
    "Zaaksysteem::Schema::BibliotheekSjablonen",
    { id => "bibliotheek_sjablonen_id" },
    { join_type   => 'left' },
);


# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


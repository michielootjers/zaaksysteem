package Zaaksysteem::Schema::Config;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Config

=cut

__PACKAGE__->table("config");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'config_id_seq'

=head2 parameter

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 value

  data_type: 'text'
  is_nullable: 1

=head2 advanced

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "config_id_seq",
  },
  "parameter",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "value",
  { data_type => "text", is_nullable => 1 },
  "advanced",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("parameter_unique", ["parameter"]);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-11-26 17:07:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Ami9YjEQu1Q90T9P63AABQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Config::ResultSet');

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

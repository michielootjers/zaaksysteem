package Zaaksysteem::Schema::ObjectAclEntry;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ObjectAclEntry

=cut

__PACKAGE__->table("object_acl_entry");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 entity_type

  data_type: 'text'
  is_nullable: 0

=head2 entity_id

  data_type: 'text'
  is_nullable: 0

=head2 capability

  data_type: 'text'
  is_nullable: 0

=head2 verdict

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "entity_type",
  { data_type => "text", is_nullable => 0 },
  "entity_id",
  { data_type => "text", is_nullable => 0 },
  "capability",
  { data_type => "text", is_nullable => 0 },
  "verdict",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("uuid");

=head1 RELATIONS

=head2 object_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-03 21:12:47
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:VW+BEMC5L1GmW9CCNsziJA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

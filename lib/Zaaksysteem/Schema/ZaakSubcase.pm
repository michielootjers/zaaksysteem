package Zaaksysteem::Schema::ZaakSubcase;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaakSubcase

=cut

__PACKAGE__->table("zaak_subcase");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_subcase_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 relation_zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 required

  data_type: 'integer'
  is_nullable: 1

=head2 parent_advance_results

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_subcase_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "relation_zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "required",
  { data_type => "integer", is_nullable => 1 },
  "parent_advance_results",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });

=head2 relation_zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "relation_zaak_id",
  "Zaaksysteem::Schema::Zaak",
  { id => "relation_zaak_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-15 12:22:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gp8iJbwwpzC9KG8uf0ttyQ

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaakSubcase');





# You can replace this text with custom content, and it will be preserved on regeneration
1;

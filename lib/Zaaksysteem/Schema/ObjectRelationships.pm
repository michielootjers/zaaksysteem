package Zaaksysteem::Schema::ObjectRelationships;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ObjectRelationships

=cut

__PACKAGE__->table("object_relationships");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object1_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 object2_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 type1

  data_type: 'text'
  is_nullable: 0

=head2 type2

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object1_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "object2_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "type1",
  { data_type => "text", is_nullable => 0 },
  "type2",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("uuid");

=head1 RELATIONS

=head2 object2_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object2_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object2_uuid" },
);

=head2 object1_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object1_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object1_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-02-18 14:03:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:aUS1g/Xm9FXAL3Rocs/TYA

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Object::Relationships::ResultSet');
__PACKAGE__->load_components(qw[+Zaaksysteem::Backend::Object::Relationships::Component]);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

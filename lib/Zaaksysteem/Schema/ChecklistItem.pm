package Zaaksysteem::Schema::ChecklistItem;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ChecklistItem

=cut

__PACKAGE__->table("checklist_item");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'checklist_item_id_seq'

=head2 checklist_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 state

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 sequence

  accessor: 'column_sequence'
  data_type: 'integer'
  is_nullable: 1

=head2 user_defined

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 deprecated_answer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "checklist_item_id_seq",
  },
  "checklist_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "state",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "sequence",
  { accessor => "column_sequence", data_type => "integer", is_nullable => 1 },
  "user_defined",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "deprecated_answer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 checklist_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Checklist>

=cut

__PACKAGE__->belongs_to(
  "checklist_id",
  "Zaaksysteem::Schema::Checklist",
  { id => "checklist_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-02-11 11:14:25
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2XwfAtJjy6HXrEzg1XCUhQ

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::ChecklistItem',
    __PACKAGE__->load_components()
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

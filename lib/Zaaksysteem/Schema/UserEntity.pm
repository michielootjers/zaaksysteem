package Zaaksysteem::Schema::UserEntity;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::UserEntity

=cut

__PACKAGE__->table("user_entity");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'user_entity_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 source_interface_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 source_identifier

  data_type: 'text'
  is_nullable: 0

=head2 active

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 subject_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  is_nullable: 1

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "user_entity_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "source_interface_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "source_identifier",
  { data_type => "text", is_nullable => 0 },
  "active",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "subject_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "date_created",
  { data_type => "timestamp", is_nullable => 1 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 subject_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_id",
  "Zaaksysteem::Schema::Subject",
  { id => "subject_id" },
);

=head2 source_interface_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Interface>

=cut

__PACKAGE__->belongs_to(
  "source_interface_id",
  "Zaaksysteem::Schema::Interface",
  { id => "source_interface_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-12-10 15:31:47
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1ELVbxeroKyQuLH0KdTHwQ

__PACKAGE__->load_components(
    "+Zaaksysteem::Backend::Auth::UserEntity::Component",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

use JSON;

__PACKAGE__->inflate_column('properties', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) }
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

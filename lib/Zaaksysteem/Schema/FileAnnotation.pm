package Zaaksysteem::Schema::FileAnnotation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::FileAnnotation

=cut

__PACKAGE__->table("file_annotation");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 subject

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "subject",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "modified",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to("file_id", "Zaaksysteem::Schema::File", { id => "file_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-01-25 16:11:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:sqfPFELZzO9G9kbvzM3JdA

use JSON;

__PACKAGE__->inflate_column('properties', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) },
});


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

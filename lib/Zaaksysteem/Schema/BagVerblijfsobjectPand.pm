package Zaaksysteem::Schema::BagVerblijfsobjectPand;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::BagVerblijfsobjectPand - koppeltabel voor panden bij verblijfsobject

=cut

__PACKAGE__->table("bag_verblijfsobject_pand");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.01 : de unieke aanduiding van een verblijfsobject

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.

=head2 pand

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_verblijfsobject_pand_id_seq'

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "pand",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_verblijfsobject_pand_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 13:19:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Ih9GVzr9DV8+ezYhLoAtUg

__PACKAGE__->belongs_to(
  "pand",
  "Zaaksysteem::Schema::BagPand",
  { "identificatie" => "pand" },
  { is_foreign_key_constraint => 0},
);

__PACKAGE__->load_components(
    "+Zaaksysteem::Helper::ToJSON",
    "+Zaaksysteem::Backend::Component",
    __PACKAGE__->load_components()
);



# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


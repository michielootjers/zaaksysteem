package Zaaksysteem::Schema::CaseRelation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::CaseRelation

=cut

__PACKAGE__->table("case_relation");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'case_relation_id_seq'

=head2 case_id_a

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 case_id_b

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 order_seq_a

  data_type: 'integer'
  is_nullable: 1

=head2 order_seq_b

  data_type: 'integer'
  is_nullable: 1

=head2 type_a

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 type_b

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "case_relation_id_seq",
  },
  "case_id_a",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "case_id_b",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "order_seq_a",
  { data_type => "integer", is_nullable => 1 },
  "order_seq_b",
  { data_type => "integer", is_nullable => 1 },
  "type_a",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "type_b",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_id_a

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "case_id_a",
  "Zaaksysteem::Schema::Zaak",
  { id => "case_id_a" },
);

=head2 case_id_b

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "case_id_b",
  "Zaaksysteem::Schema::Zaak",
  { id => "case_id_b" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-01-16 14:35:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kwphBt5qQEEd9UCQvAjkag

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Case::Relation::ResultSet');
__PACKAGE__->load_components(qw[+Zaaksysteem::Backend::Case::Relation::Component]);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

package Zaaksysteem::Schema::Interface;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Interface

=cut

__PACKAGE__->table("interface");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'interface_id_seq'

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 active

  data_type: 'boolean'
  is_nullable: 0

=head2 case_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 max_retries

  data_type: 'integer'
  is_nullable: 0

=head2 interface_config

  data_type: 'text'
  is_nullable: 0

=head2 multiple

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 module

  data_type: 'text'
  is_nullable: 0

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "interface_id_seq",
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "active",
  { data_type => "boolean", is_nullable => 0 },
  "case_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "max_retries",
  { data_type => "integer", is_nullable => 0 },
  "interface_config",
  { data_type => "text", is_nullable => 0 },
  "multiple",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "module",
  { data_type => "text", is_nullable => 0 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_type_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "case_type_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "case_type_id" },
);

=head2 object_subscriptions

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectSubscription>

=cut

__PACKAGE__->has_many(
  "object_subscriptions",
  "Zaaksysteem::Schema::ObjectSubscription",
  { "foreign.interface_id" => "self.id" },
  {},
);

=head2 transactions

Type: has_many

Related object: L<Zaaksysteem::Schema::Transaction>

=cut

__PACKAGE__->has_many(
  "transactions",
  "Zaaksysteem::Schema::Transaction",
  { "foreign.interface_id" => "self.id" },
  {},
);

=head2 user_entities

Type: has_many

Related object: L<Zaaksysteem::Schema::UserEntity>

=cut

__PACKAGE__->has_many(
  "user_entities",
  "Zaaksysteem::Schema::UserEntity",
  { "foreign.source_interface_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-11-03 17:35:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:qstLaEusUQkwJoRbp5rmkg

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Sysin::Interface::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Sysin::Interface::Component
    +DBIx::Class::Helper::Row::ToJSON
/);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

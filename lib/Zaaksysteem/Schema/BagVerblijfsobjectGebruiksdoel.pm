package Zaaksysteem::Schema::BagVerblijfsobjectGebruiksdoel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagVerblijfsobjectGebruiksdoel - koppeltabel voor gebruiksdoelen bij verblijfsobject

=cut

__PACKAGE__->table("bag_verblijfsobject_gebruiksdoel");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.01 : de unieke aanduiding van een verblijfsobject

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.

=head2 gebruiksdoel

  data_type: 'varchar'
  is_nullable: 0
  size: 80

56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_verblijfsobject_gebruiksdoel_id_seq'

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "gebruiksdoel",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_verblijfsobject_gebruiksdoel_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-02-19 11:05:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4uwuyrE4nHimzOb7NbM3sA

__PACKAGE__->load_components(
    "+DBIx::Class::Helper::Row::ToJSON",
    "+Zaaksysteem::Backend::Component",
    __PACKAGE__->load_components()
);



# You can replace this text with custom content, and it will be preserved on regeneration
1;

package Zaaksysteem::Betrokkene::Object::Medewerker;

use strict;
use warnings;

use Net::LDAP;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw( LDAP_CONTROL_PAGED );

use Data::Dumper;
use Moose;

use Zaaksysteem::Exception;

use constant BOBJECT    => 'Zaaksysteem::Betrokkene::Object';
use constant BRSOBJECT  => 'Zaaksysteem::Betrokkene::ResultSet';

use Zaaksysteem::Constants;

extends BOBJECT;

my $CLONE_MAP = {
    'voorletters'   => 'initials',
    'voornamen'     => 'givenname',
    'email'         => 'mail',
    'geslachtsnaam' => 'sn',
    'display_name'  => 'displayname',
    'telefoonnummer' => 'telephonenumber',
    'username'      => 'cn',
};
my $UNIFORM = {
    'voorvoegsel'       => 0,
    'straatnaam'        => 'straatnaam',
    'huisnummer'        => 'huisnummer',
    'postcode'          => 'postcode',
    'geslachtsaanduiding'          => 0,
    'woonplaats'        => 'plaats',
};

my $SEARCH_MAP = {
    %{ $CLONE_MAP }
};

has 'intern'    => (
    'is'    => 'rw',
);

has 'ldap_rs'   => (
    'is'    => 'rw',
);

has 'is_active'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return shift->ldap_rs->is_active;
    }
);

has 'ldapid'   => (
    'is'    => 'rw',
);

### DUMMY:
has [qw/
    huisletter
    huisnummertoevoeging
    mobiel
/] => (
    'is'   => 'ro',
);

### Convenience method containing some sort of display_name
has 'naam' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->display_name;
    },
);

has 'afdeling' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->org_eenheid->naam
            if $self->org_eenheid;
    },
);

has 'org_eenheid' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        if ($self->ldap_rs) {
            # Get eenheid
            my $bclass = BOBJECT . '::OrgEenheid';

            return $bclass->get_by_dn(
                $self->_dispatch_options,
                $self->ldap_rs->dn,
            );
        }
    },
);

has 'roles'     => (
    'is'        => 'rw',
);

sub search {
    my $self                = shift;
    my $dispatch_options    = shift;
    my $opts                = shift;
    my ($searchr)           = @_;

    ### Will search our LDAP directory containing the asked users
    die('M::B::NP->search() only possible call = class based')
        unless !ref($self);

    my $search_options      = {};

    $search_options->{'me.properties'} = { 'ILIKE' => '%' . $searchr->{freetext} . '%' }
        if $searchr->{freetext};

    # my $subjects            = $c->model()
    my $page                = $dispatch_options->{stash}->{paging_total} || 25;


    # my ($cookie, $org_eenheid);
    # my $page = Net::LDAP::Control::Paged->new(size => 50);
    # my $ldap = $dispatch_options->{ ldap };

    # if (exists($searchr->{'org_eenheid'}) && $searchr->{'org_eenheid'}) {
    #     ### Find by cn
    #     my $org_search = $ldap->search(sprintf(
    #         '(&(objectClass=organizationalUnit)(l=%s))',
    #         $searchr->{ org_eenheid }
    #     ));

    #     $org_search->code && die("failed to search with filter: ", $org_search->error
    #         . ' / '
    #     );
    #     $org_eenheid = $org_search->entry(0);
    # }

    # my $searchfilter = '(&(objectClass=posixAccount)';

    # if($searchr->{freetext}) {
    #     if (
    #         my $append = $self->_search_freetext({
    #             query => $searchr->{freetext},
    #         })
    #     ) {
    #         $searchfilter .= $append;
    #     }
    # } else {
    #     if (
    #         my $append = $self->_search_specific({
    #             searchr     => $searchr,
    #             org_eenheid => $org_eenheid,
    #         })
    #     ) {
    #          $searchfilter .= $append;
    #     }
    # }

    # $searchfilter .= ')';

    # my $usersearch = $ldap->search($searchfilter,
    #     control => [ $page ],
    # );

    # $usersearch->code && die("failed to search with filter: ", $usersearch->error
    #     . ' / '
    # );

    # if (my ($resp) = $usersearch->control(LDAP_CONTROL_PAGED)) {
    #     $cookie = $resp->cookie;
    #     if ($cookie) {
    #         $page->cookie($cookie);
    #         $page->size(0);

    #         $ldap->ldap->search(control => [$page]);
    #     }
    # }


    # ### Do some searching over these entries depending on given values
    # my @results = $usersearch->entries;

    # #@results = sort { $a->get_value('sn') cmp $b->get_value('sn') } @results;
    # my @usernames = map { $_->get_value('cn') } @results;

    # return unless scalar(@results) > 0;
    ### Paging
    my $rowlimit    = $dispatch_options->{stash}->{paging_rows} =
        $dispatch_options->{stash}->{paging_rows} || 40;
    my $rowpage     = $dispatch_options->{stash}->{paging_page} =
        $dispatch_options->{stash}->{paging_page} || 1;
    my $roworder    = $dispatch_options->{stash}->{order} =
        $dispatch_options->{stash}->{order} || 'user_entities.source_identifier';
    my $roworderdir = $dispatch_options->{stash}->{order_direction} =
        $dispatch_options->{stash}->{order_direction} || 'asc';

    if($opts->{rows_per_page}) {
        $rowlimit = $opts->{rows_per_page};
    }

    $search_options->{subject_type} = SUBJECT_TYPE_EMPLOYEE; 

    my $resultset;
    if ($searchr->{inactive_search}) {
        $resultset = $dispatch_options->{dbic}->resultset('Subject')->search(
            {
                %{ $search_options },
                'user_entities.date_deleted'       => { 
                    '!=' => undef,
                },
            },
            {
                join        => 'user_entities',
                'page'      => $rowpage,
                'rows'      => $rowlimit,
                'order_by'  => { '-' . $roworderdir => $roworder }
            }
        );
    } else {
        $resultset = $dispatch_options->{dbic}->resultset('Subject')->search_active(
            $search_options,
            {
                join        => 'user_entities',
                'page'      => $rowpage,
                'rows'      => $rowlimit,
                'order_by'  => { '-' . $roworderdir => $roworder }
            }
        );        
    }

    ### Paging info
    $dispatch_options->{stash}->{paging_total}       = $resultset->pager->total_entries;
    $dispatch_options->{stash}->{paging_lastpage}    = $resultset->pager->last_page;

    return BRSOBJECT->new(
        'class'     => __PACKAGE__,
        'dbic_rs'   => $resultset,
        'opts'      => $opts,
        %{ $dispatch_options },
    );
}



Params::Profile->register_profile(
    method  => '_search_specific',
    profile => {
        required => [ qw/searchr/ ],
        optional => [ qw/org_eenheid/ ],
    }
);

sub _search_specific {
    my ($self, $params) = @_;
    my $filter = '';

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_specific" unless $dv->success;

    my $searchr     = $params->{searchr};
    my $org_eenheid = $params->{org_eenheid};

    foreach my $attr (keys %{ $SEARCH_MAP }) {
        # Usernames should be an exact match, all others can be wildcarded after the string.
        if (exists($searchr->{$attr}) &&
            $searchr->{$attr} &&
            $attr eq 'username' ) {
            $filter .= '(' . $SEARCH_MAP->{$attr} . '=' . $searchr->{$attr} . ')'; 

        }
        elsif (exists($searchr->{$attr}) &&
            $searchr->{$attr}
        ) {
             $filter .= '(' . $SEARCH_MAP->{$attr} . '=' . $searchr->{$attr} . '*)';
        }
    }

    return '(|' . $filter . ')' if $filter;
    return '';
}


Params::Profile->register_profile(
    method  => '_search_freetext',
    profile => {
        required => [ qw/query/ ]
    }
);
sub _search_freetext {
    my ($self, $params) = @_;
    my $filter = '';

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_freetext" unless $dv->success;

    my $query   = $params->{query};

    for my $value (values %{ $SEARCH_MAP }) {
        $filter .= '(' . $value . '=' . $query . '*)';
    }

    return '(|' . $filter . ')' if $filter;
    return '';
}



sub BUILD {
    my ($self) = @_;

    ### Nothing to do if we do not know which way we came in
    return unless ($self->trigger eq 'get' && $self->id);

    ### It depends on the 'intern' option, weather we retrieve
    ### our data from our our snapshot DB, or GM. When there is
    ### no intern defined, we will look at the id for a special string
    if ($self->id =~ /\-/) {
        $self->log->debug('XXX Found special string');

        ### Special string, no intern defined, go to intern default
        if (!defined($self->{intern})) {
            $self->log->debug('XXX Found internal request');
            $self->{intern} = 1;
        }

        my ($ldapid, $id) = $self->id =~ /^(\d+)\-(\d+)$/;

        $self->id($id);
        $self->ldapid($ldapid);
    }

    if (!$self->intern) {

        ### Get id is probably gmid, it is an external request, unless it is
        ### already set of course
        if (!$self->ldapid) {
            $self->ldapid($self->id);
            $self->id(undef);
        }
    }

    ### All set, let's rock and rolla. Depending on where we have to get the
    ### data from, fill in the blanks
    if ($self->{intern}) {
        $self->_load_intern or die('Failed loading M::B::NP Object');
    } else {
        $self->_load_extern or die('Failed loading M::B::NP Object: ' . $self->ldapid);
    }

    ### Some defaults, should move to Object
    $self->btype('medewerker');
}

sub _load_extern {
    my ($self) = @_;
    my (@entries, $ldapsearch);

    my $ldap = $self->_dispatch_options->{ ldap };

    my $subject;
    if ($self->record) {
        $subject = $self->record;
    } else {
        $subject         = $self->_dispatch_options->{dbic}->resultset('Subject')->find(
            $self->ldapid
        );
    }

    (
        $self->log->debug(
            'M::B::MW->load: Could not find Subject with ID: ' .
            $self->ldapid
        ),
        return
    ) unless $subject;

    $self->ldap_rs( $subject );

#    my $bo = $self->dbic->resultset('ZaakBetrokkenen')->find_or_create({
#        'betrokkene_type'           => 'medewerker',
#        'betrokkene_id'             => $self->ldapid,
#        'gegevens_magazijn_id'      => $self->ldapid,
#        'naam'                      => $self->ldap_rs->get_value('displayName'),
#    });

    $self->id( $self->ldap_rs->id );

    $self->identifier($self->ldapid . '-' . $self->id);

    ### We are loaded external, now let's set up some triggers and attributes
    $self->_load_attributes;

    return 1;
}

sub _load_intern {
    my ($self) = @_;
    my ($bo);

    (
        $self->log->debug(
            'M::B::MW->load: Could not find internal betrokkene by id ' . $self->id
        ),
        return
    ) unless $bo = $self->dbic->resultset('ZaakBetrokkenen')->find($self->id);

    #warn('Loading: ' . $bo->naam . ':' . $bo->id);
    ### TODO : NO idea yet if I really need this object
    $self->bo($bo);

    ### Retrieve data from internal GM
    return unless $bo->betrokkene_id;

    $self->ldapid($bo->betrokkene_id);

    ### Get external data
    $self->_load_extern or return;

    return 1;
}


=head2

Only used in the contact details page, there are duplicates of this.
Refactored in to lazy moose attribute because this borked up a code path
in test context. And it only seems to be eating resources in other contexts.

Constructive solution seems to be delegate this to Backend::LDAP routine.
Very low priority so I'm leaving this be.

=cut

has 'roles' => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my ($self)  = @_;

        my $ldap = $self->_dispatch_options->{ ldap };

        return unless $ldap && $self->ldap_rs;

        my $roles   = $ldap->search(sprintf(
            '(&(objectClass=posixGroup)(memberUid=%s))',
            $self->ldap_rs->dn
        ));

        my %roles;
        if ($roles->count) {
            for my $role ($roles->entries) {
                $roles{$role->dn} = $role;
            }
        }
        return \%roles;
    }
);

sub _load_attributes {
    my ($self) = @_;

    for my $meth (keys %{ $CLONE_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
#            'trigger'   => sub {
#                my ($self, $new, $old) = @_;
#
#                ## Do not update anything when new is the same
#                if ($new eq $old) { return $new; }
#
#                # And definetly do not update the adres_id
#                if ($meth eq 'adres_id') { return; }
#
#                ### Update object
#                $self->gm_np->$meth($new);
#                $self->gm_np->update;
#            },
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                my $key = $CLONE_MAP->{$meth};
                my $val = $self->ldap_rs->$key;
                utf8::decode($val);
                return $val;
            }
        );
    }

    ### Uniformiteit, attributes known to every object, but does not have
    ### a trigger :P
    for my $meth (keys %{ $UNIFORM }) {
        my $localmeth = $UNIFORM->{$meth};
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                return '' unless $localmeth;

                return $self->config->{gemeente}->{$localmeth};
            }
        );
    }
}

sub set {
    my ($self, $dispatch_options, $external_id) = @_;

    ### Here we get a medewerk uidNumber. We presume medewerkers will get
    ### deleted from the system, so we create a betrokkene.
    my $identifier = $external_id . '-';

    $dispatch_options->{log}->debug('M::B::MW->set called with identifier: ' . $identifier);

    my $subject = $dispatch_options->{dbic}->resultset('Subject')
                    ->find($external_id) or return;

    my $bo = $dispatch_options->{dbic}->resultset('ZaakBetrokkenen')->create({
        'betrokkene_type'           => 'medewerker',
        'betrokkene_id'             => $subject->id,
        'gegevens_magazijn_id'      => $subject->id,
        'naam'                      => $subject->displayname,
    });

    $identifier .= $bo->id;

    $dispatch_options->{log}->debug('M::B::MW->set create identifier ' . $identifier);
    return 'medewerker-' . $identifier;
}

sub security_identity {
    my $self = shift;

    return 'user', $self->ldap_rs->cn;
}

#sub set {
#    my ($self, $id) = @_;
#    my ($copy, $adrescopy) = {};
#
#    ### Found this id?
#    my $npo = $self->c->model('DBG::NatuurlijkPersoon')->find($id);
#    return unless $npo;
#
#    $copy->{ $_ } = $npo->$_ for @{ $CLONE_MAP };
#
#    my $npadreso = $npo->adres_id;
#
#    $adrescopy->{ $_ } = $npadreso->$_ for @{ $ADRES_CLONE_MAP };
#
#    my $npaoo = $self->c->model('DB::GMAdres')->create(
#        {
#            %$adrescopy
#        }
#    );
#
#    ### Copy this ID to our GM
#    my $npoo = $self->c->model('DB::GMNatuurlijkPersoon')->create(
#        {
#            'gegevens_magazijn_id'  => $id,
#            'adres_id' => $npaoo->id,
#            %$copy
#        }
#    );
#
#    #$self->log->debug('Gaatie? ');
#
#    ### Set this id
#    my $bo = $self->c->model('DB::Betrokkene')->create({
#        'btype'                     => 'natuurlijk_persoon',
#        'gm_natuurlijk_persoon_id'  => $npoo->id,
#        'naam'                      => $npoo->voornamen . ' ' . $npoo->geslachtsnaam,
#    });
#
#    return $bo->id;
#}
#
#sub _init {
#    my ($self, $c) = @_;
#
#
#}

# NEW. This subroutine will provide the Betrokkene class the information
# needed to get information from this class.. Strange eh :)

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


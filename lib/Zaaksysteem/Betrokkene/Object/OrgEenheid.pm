package Zaaksysteem::Betrokkene::Object::OrgEenheid;

use strict;
use warnings;

use Net::LDAP;
use Data::Dumper;
use Moose;

use constant BOBJECT    => 'Zaaksysteem::Betrokkene::Object';
use constant BRSOBJECT  => 'Zaaksysteem::Betrokkene::ResultSet';

extends BOBJECT;

my $CLONE_MAP = {
    'naam'          => 'description',
    'shortname'     => 'naam',
    'omschrijving'  => 'description',
};

my $SEARCH_MAP = {
    %{ $CLONE_MAP }
};

has 'intern'    => (
    'is'    => 'rw',
);

has 'ldap_rs'   => (
    'is'    => 'rw',
);

has 'ldapid'   => (
    'is'    => 'rw',
);

has [qw/huisletter huisnummertoevoeging/] => (
    'is'   => 'ro',
);

sub search {
    my $self        = shift;
    my $dispatch_options = shift;
    my $opts        = shift;
    my ($searchr)   = @_;

    ### Will search our LDAP directory containing the asked users
    die('M::B::NP->search() only possible call = class based')
        unless !ref($self);

    my $usersearch = $dispatch_options->{ ldap }->search(
        '(&(objectClass=organizationalUnit))',
    );

    ### Do some searching over these entries depending on given values
    my @results = ();
    foreach my $entry ($usersearch->entries) {
        my $searchfail = 0;
        foreach my $attr (keys %{ $SEARCH_MAP }) {
            if (
                exists($searchr->{$attr}) &&
                $searchr->{$attr}
            ) {
                my $se = $searchr->{$attr};
                unless ($entry->get_value($SEARCH_MAP->{$attr}) =~ /$se/i) {
                    $searchfail++;
                }
            }

        }

        push(@results, $entry) unless $searchfail;
    }

    return unless scalar(@results) > 0;

    return BRSOBJECT->new(
        'class'     => __PACKAGE__,
        'ldap_rs'   => \@results,
        'opts'      => $opts,
        %{ $dispatch_options },
    );
}


sub BUILD {
    my ($self) = @_;

    ### Nothing to do if we do not know which way we came in
    return unless ($self->trigger eq 'get' && $self->id);

    ### It depends on the 'intern' option, weather we retrieve
    ### our data from our our snapshot DB, or GM. When there is
    ### no intern defined, we will look at the id for a special string
    if ($self->id =~ /\-/) {
        $self->log->debug('XXX Found special string');

        ### Special string, no intern defined, go to intern default
        if (!defined($self->{intern})) {
            $self->{intern} = 1;
        }

        my ($ldapid, $id) = $self->id =~ /^(\d+)\-(\d+)$/;

        $self->id($id);
        $self->ldapid($ldapid);
    }

    if (!$self->intern) {

        ### Get id is probably gmid, it is an external request, unless it is
        ### already set of course
        if (!$self->ldapid) {
            $self->ldapid($self->id);
            $self->id(undef);
        }
    }

    ### All set, let's rock and rolla. Depending on where we have to get the
    ### data from, fill in the blanks
    if ($self->{intern}) {
        $self->_load_intern or die('Failed loading M::B::NP Object');
    } else {
        $self->_load_extern or die('Failed loading M::B::NP Object');
    }

    $self->btype('org_eenheid');

}

sub _load_extern {
    my ($self) = @_;
    my (@entries, $ldapsearch);

    my $ldap = $self->_dispatch_options->{ ldap };

    # use Carp;
    # confess('WUT?');
    (
        $self->log->debug(
            'M::B::OE->load: Could not search for ldapuid: ' .  $self->ldapid
        ),
        return
    ) unless $ldapsearch = $ldap->search(sprintf('(l=%s)', $self->ldapid));

    (
        $self->log->debug(
            'M::B::OE->load: Could not find external OE because LDAP '
            . ' returned an error: ' .  $ldapsearch->error_text
        ),
        return
    ) if $ldapsearch->is_error;

    (
        $self->log->debug(
            'M::B::OE->load: More than 1 org_eenheid found: ' .  $self->ldapid
        ),
        return
    ) if $ldapsearch->count != 1;

    $self->ldap_rs( $ldapsearch->pop_entry );
    $self->_load_attributes;

    return 1;
}

sub _load_intern {
    my ($self) = @_;
    my ($bo);

    die('DEPRECATED Betrokkene::OrgEenheid');

    (
        $self->log->debug(
            'M::B::OE->load: Could not find internal betrokkene by id ' . $self->id
        ),
        return
    ) unless $bo = $self->dbic->resultset('Betrokkene')->find($self->id);

    ### TODO : NO idea yet if I really need this object
    $self->bo($bo);

    ### Retrieve data from internal GM
    return unless $bo->org_eenheid_id;

    $self->ldapid($bo->org_eenheid_id);

    ### Get external data
    $self->_load_extern or return;

    return 1;
}

sub _load_attributes {
    my ($self) = @_;

    for my $meth (keys %{ $CLONE_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            'default'   => sub {
                my ($self) = @_;

                return $self->ldap_rs->get_value($CLONE_MAP->{$meth});
            }
        );
    }
}

sub set {
    my ($self, $dispatch_options, $external_id) = @_;

    die('DEPRECATED Betrokkene::OrgEenheid');

    ### Here we get a group gidNumber. We presume groups will get
    ### deleted from the system, so we create a betrokkene.
    my $identifier = $external_id . '-';

    $dispatch_options->{log}->debug('M::B::OE->set called with identifier: ' . $identifier);

    # Load external id
    my $mwo = __PACKAGE__->new(
        'trigger'       => 'get',
        'c'             => $dispatch_options,
        'id'            => $external_id,
        'intern'        => 0,
    );

    ### Let's do something EXTRA here. We do not want a new betrokkene for
    ### every time we create a medewerker. Medewerkers are 'static'

    ### Find betrokkene by id
    my $bo = $dispatch_options->{dbic}->resultset('Betrokkene')->search({
        'btype'                     => 'org_eenheid',
        'org_eenheid_id'             => $mwo->ldapid,
    });

    if (!$bo->count) {
        ### Create betrokkene
        $bo = $dispatch_options->{dbic}->resultset('Betrokkene')->create({
            'btype'                     => 'org_eenheid',
            'org_eenheid_id'            => $mwo->ldapid,
            'naam'                      => $mwo->naam,
        });
    } else {
        $bo = $bo->first;
    }

    return unless $bo;
    $identifier .= $bo->id;

    $dispatch_options->{log}->debug('M::B::OE->set create identifier ' . $identifier);
    return 'org_eenheid-' . $identifier;
}

sub get_by_dn {
    my ($self, $dispatch_options, $dn) = @_;

    ### No die's
    return unless $dispatch_options && $dn;

    my $ldap = $dispatch_options->{ ldap };

    my ($org_ou) = $dn =~ /ou=(.*?),/;

    my $ldapsearch = $ldap->search(sprintf(
        '(&(objectClass=organizationalUnit)(ou=%s))',
        $org_ou
    ));

    (
        $dispatch_options->{log}->debug(
            'M::B::OE->load: Could not find external OE because LDAP '
            . ' returned an error: ' .  $ldapsearch->error_text
        ),
        return
    ) if $ldapsearch->is_error;

    return if $ldapsearch->count < 1;

    my $entry = $ldapsearch->shift_entry;

    return __PACKAGE__->new(
            'trigger'       => 'get',
            'id'            => $entry->get_value('l'),
            'extern'        => 1,
            %{ $dispatch_options }
        );
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


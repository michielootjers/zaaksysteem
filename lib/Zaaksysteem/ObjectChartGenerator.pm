package Zaaksysteem::ObjectChartGenerator;

use Moose;
use namespace::autoclean;
use Data::Dumper;

=head1 NAME

Zaaksysteem::ObjectChartGenerator - Temporary clone of Zaaksysteem::ChartGenerator

=head1 DESCRIPTION

This is a temporary clone of L<Zaaksysteem::ChartGenerator> that uses the
C<ObjectData> resultset instead of the C<Zaak> resultset.

Once graphing features have been properly re-implemented using the new object
framework, this and the old L<Zaaksysteem::ChartGenerator> should be removed.

=head1 ATTRIBUTES

=head2 resultset

The resultset to use to generate graphs.

=head2 behandelaars_cache

(Internal) cache for behandelaars

=cut

has 'resultset' => (
    is => 'rw', 
    required => 1,
);

has 'behandelaars_cache' => (
    is => 'rw',
    default => sub { {} },
);

with qw{
    Zaaksysteem::ObjectChartGenerator::Roles::Status
    Zaaksysteem::ObjectChartGenerator::Roles::Handling_time
    Zaaksysteem::ObjectChartGenerator::Roles::Average_handling_time_per_month
    Zaaksysteem::ObjectChartGenerator::Roles::Cases_per_department
    Zaaksysteem::ObjectChartGenerator::Roles::Cases_per_owner_per_month
    Zaaksysteem::ObjectChartGenerator::Roles::Cases_per_department_per_month
    Zaaksysteem::ObjectChartGenerator::Roles::Cases_within_and_outside_term_per_month
    Zaaksysteem::ObjectChartGenerator::Roles::Cases_within_and_outside_term_per_month_per_department
    Zaaksysteem::ObjectChartGenerator::Roles::Average_handling_time_per_owner
};


=head1 METHODS

=head2 unsparsify

hash matrix is sparse, will look like this:
{
    'januari 2013' => {
        'department1' => 23,
        'department2' => 75,
    },
    'februari 2013' => {
        'department1' => 2,
        'department3' => 67,
    }
}

to make the chart work for highcharts, $hash_matrix needs
to written like this, with zeroes where the was no results
from the database. unsparsed.
[{
    'name' => 'department1',
    'data' => [23, 0, 2],
},{
    'name' => 'department2',
    'data' => [75, 0, 0],
},{
    'name' => 'department3',
    'data' => [0, 0, 67],
}]
=cut
sub unsparsify {
    my ($self, $arguments) = @_;

    my $unique_items = $arguments->{unique_items} or die "need unique_items";
    my $hash_matrix = $arguments->{hash_matrix} or die "need hash_matrix";
    my $categories = $arguments->{categories} or die "need categories";
    my $y_objects = $arguments->{y_objects}; # optional, points are represented as objects instead of simple values
    my $series = [];

    foreach my $unique_item (sort keys %$unique_items) {
        my $data = [];
        foreach my $category (@$categories) {
            my $values = $hash_matrix->{$category};

            if($y_objects) {
                my $object = $values->{$unique_item} || { y => int 0 };
                push @{ $data }, $object;
            } else {
                my $count = $values->{$unique_item} || 0;
                push @{ $data }, int $count;
            }

        }
        push @$series, { name => $unique_item, data => $data };
    }

    return $series;
}



=head2 generate

Generate a chart profile, looks for a given profile function and executes.
Main goal is to limit access.

=cut
sub generate {
    my ($self, $arguments) = @_;

    return {message => 'Geen resultaten'} unless $self->resultset->count;

    my $profile = $arguments->{profile} or die "need profile";

    # gatekeeper
    die "unknown profile $profile" unless grep { $_ eq $profile } qw/
        status
        handling_time
        average_handling_time_per_month
        average_handling_time_per_owner
        cases_per_department
        cases_per_department_per_month
        cases_per_owner_per_month
        cases_within_and_outside_term_per_month
        cases_within_and_outside_term_per_month_per_department
    /;

    return $self->$profile;
}

__PACKAGE__->meta->make_immutable;

1;

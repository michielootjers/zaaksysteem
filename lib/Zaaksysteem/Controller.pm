package Zaaksysteem::Controller;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Exception;

use Data::Dumper;

BEGIN { extends 'Catalyst::Controller'; }

# Stub 'begin' controller we can use to hook into in deriving classes
sub begin : Private {
    my ($self, $c) = @_;

    $c->forward('/page/begin');

    if(exists $c->action->attributes->{ Method }) {
        my ($method) = @{ $c->action->attributes->{ Method } };

        unless($c->req->method eq uc($method)) {
            throw('request/method', sprintf(
                "Invalid request method '%s', action '%s' requires '%s'.",
                $c->req->method,
                $c->action,
                $method,
            ));
        }
    }

    $c->forward('_xsrf_checks');
}

sub _xsrf_checks : Private {
    my ($self, $c) = @_;

    my @actions = $c->action->can('chain') ? @{ $c->action->chain } : ($c->action);

    for my $action (@actions) {
        if(exists $action->attributes->{ JSON }) {
            unless($c->req->header('X-XSRF-TOKEN')) {
                throw('security/xsrf', 'XSRF token validation failure. No token set.');
            }

            unless($c->session->{ _xsrf_token } eq $c->req->header('X-XSRF-TOKEN')) {
                throw('security/xsrf', 'XSRF token validation failure. Token invalid.');
            }

            $c->log->info('XSRF token validation successful');

            last;
        }
    }
}

1;

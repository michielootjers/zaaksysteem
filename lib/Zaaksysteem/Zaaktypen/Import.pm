package Zaaksysteem::Zaaktypen::Import;

use strict;
use warnings;

use Params::Profile;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Moose;
use namespace::autoclean;
use Clone qw(clone);
use Carp qw(cluck);


use Zaaksysteem::Zaaktypen::Dependency;

has [qw/prod log dbic groups session filepath problems export_zaaktype only_import_active_fieldvalues/] => (
    'is'    => 'rw',
);



{
    Params::Profile->register_profile(
        method  => 'initialize',
        profile => {
            required        => [ qw/
                session
                groups
                filepath
            /],
        }
    );

    sub initialize {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;
    
        $self->session($params->{session});
        $self->groups($params->{groups});
        $self->filepath($params->{filepath});
    }
}


sub imported_zaaktype {
    my ($self, $zaaktype) = @_;

    if($zaaktype) {
        $self->session->{imported_zaaktype} = $zaaktype;
    }
    
    return $self->session->{imported_zaaktype};
}



sub import_zaaktype {
    my ($self, $zaaktypen_model) = @_;

    $self->export_zaaktype(clone $self->imported_zaaktype);

    my $import_dependencies = $self->session->{import_dependencies};

    my $zaaktype_node;
    die "export zaaktype not defined, internal error" unless($self->export_zaaktype);
    my $zaaktype_id = $self->export_zaaktype->{zaaktype}->{id};

    eval {
        $self->dbic->txn_do(sub {

            # don't modify the session info
            #$zaaktype->{zaaktype}->{id} = $zaaktype->{node}->{zaaktype_id};
            delete $self->export_zaaktype->{zaaktype}->{zaaktype_titel};
            $self->export_zaaktype->{node}->{zaaktype_omschrijving} = 'Import ' . localtime();
            $self->execute_changes($import_dependencies);

            delete $self->export_zaaktype->{zaaktype}->{zaaktype_definitie_id};
            my $main_solution = $import_dependencies->{Zaaktype}->{$zaaktype_id}->solution;
            if($main_solution->{action} eq 'use_existing') {
                $self->export_zaaktype->{zaaktype}->{id} = $main_solution->{id};

                $self->set_node_version({
                    existing_zaaktype_id => $main_solution->{id}
                });

                delete $self->export_zaaktype->{zaaktype}->{bibliotheek_categorie_id};
            } elsif($main_solution->{action} eq 'add') { 
                delete $self->export_zaaktype->{zaaktype}->{id};
                $self->export_zaaktype->{node}->{titel} = $main_solution->{name};
                $self->export_zaaktype->{zaaktype}->{bibliotheek_categorie_id} = $main_solution->{bibliotheek_categorie_id};
                delete $self->export_zaaktype->{zaaktype}->{zaaktype_categorie_id};
            } else {
                die "incorrect action";
            }

#            $self->log->debug("export zt: " . Dumper $self->export_zaaktype);
            $zaaktype_node = $zaaktypen_model->commit_session(
                session         => $self->export_zaaktype,
                commit_message  => 'Import',
            );    

            $self->import_bibliotheek_notificaties();
        });
    };


    if ($@) {
        $self->log->error('Error: ' . $@);
        die("Import error: " . $@);
    } else {
        $self->log->info('Zaaktype geimportuleerd');
    }

    $self->flush;
    return $zaaktype_node;
}


# set version to current, commit_session will increment this
sub set_node_version {
    my ($self, $arguments) = @_;

    my $existing_zaaktype_id = $arguments->{existing_zaaktype_id} or die "need existing_zaaktype_id";

    my $version = $self->dbic->resultset('Zaaktype')->find($existing_zaaktype_id)->zaaktype_node_id->version;

    $self->export_zaaktype->{node}->{version} = $version;
}


# update bibliotheek_notificatie table. these are not present in some older exported
# zaaktypen. (built in in 2.6.x)
sub import_bibliotheek_notificaties {
    my ($self) = @_;

    # we could be more specific, only work on the currently imported zaaktype - but
    # it doesn't hurt to do some extra cleanup.
    $self->log->debug("importing bibliotheek_notificaties");
    my $resultset = $self->dbic->resultset("ZaaktypeNotificatie")->search({
        'zaaktype_node_id.deleted' => undef,
        'zaaktype_id.deleted' => undef,
    }, {
        join => {'zaaktype_node_id' => {'zaaktype_id' => 'bibliotheek_categorie_id'} }
    });

    while(my $zaaktype_notificatie = $resultset->next()) {
        $self->log->debug("importing zaaktype_notificatie id = " . $zaaktype_notificatie->id);
        $zaaktype_notificatie->import_bibliotheek_notificatie();
    }
    $self->log->debug("done importing zaaktype_notificaties");
}


sub execute_changes {
    my ($self, $import_dependencies) = @_;

    foreach my $table (sort keys %$import_dependencies) {

        my $table_items = $import_dependencies->{$table};
        foreach my $id (keys %$table_items) {
            my $dependency = $table_items->{$id};

            my $new_id = $self->execute_change($dependency, $table, $id);
            $dependency->local_id($new_id);
        }
    }
}


sub execute_change {
    my ($self, $dependency, $table, $id) = @_;

    my $sub_items = $dependency->sub_items($self->export_zaaktype);
    
    # implement solution
    my $solution = $dependency->{solution};
    return $solution->{id} unless ($sub_items);

    my $keyname = $dependency->{keyname};
    my $new_id;

    if($solution->{action} eq 'add') {
        $new_id = $self->execute_add_action($dependency, $table, $id);
    } elsif( $solution->{action} eq 'use_existing') {
        $new_id = $solution->{id};
        $self->import_field_options($dependency, $table, $id);
    } else {
        unless($table eq 'BibliotheekCategorie') {
            die "illegal action configured: " . Dumper $dependency;
        }
    }

    if($dependency->{main_zaaktype}) {
        $self->export_zaaktype->{zaaktype}->{id} = $new_id;
        delete $self->export_zaaktype->{zaaktype}->{bibliotheek_categorie_id};
    }

    foreach my $sub_item (@$sub_items) {
        my $item = $sub_item->{sub_item} or die Dumper $sub_items;
        my $key_name = $sub_item->{key_name} or die Dumper $sub_items;
        $item->{$key_name} = $new_id;
    } 
    
    return $new_id;
}


=head2 import_field_options

Although a field (kenmerk) may be present on the target system, options
may have been added. If this is the case, the user gets a choice to
import the options. This will enforce the source options, but leave the
other options in an inactive state.

e.g. source: butter, cheese, eggs (inactive)
target: butter, cheese (inactive), milk

after import target has: butter, cheese, eggs (inactive), milk (inactive)

=cut

sub import_field_options {
    my ($self, $dependency, $table, $id) = @_;

    my $solution = $dependency->{solution};
    return unless $table eq 'BibliotheekKenmerken' && $solution->{use_remote_field_options};

    my $local = $self->dbic->resultset('BibliotheekKenmerken')->find($solution->{id});
    return unless $local->uses_options;

    $local->bump_version;

    my $remote_record = $self->lookup_remote_record('BibliotheekKenmerken', $id)
        or throw('casetype/import', 'Afhankelijkheid mist in zaaktype bestand');

    my $field_options = $self->determine_field_options($remote_record, $solution->{id});

    $local->save_options({
        options => $field_options,
        reason => $self->kenmerk_reason($remote_record)
    });
}


=head2 determine_field_options

Filters out inactive remote options if requested
Merges remote and local options.

Marks remote options.

=cut

sub determine_field_options {
    my ($self, $remote_record, $bibliotheek_kenmerken_id) = @_;

    my $remote_options = $self->get_remote_options($remote_record);

    my $filtered = clone ($self->session->{only_import_active_fieldvalues} ?
        [grep { $_->{active} eq '1' } @$remote_options] :
        $remote_options);

    # used to distinguish these in the GUI
    $_->{remote} = 1 for @$filtered;

    return $self->merge_local_options({
        bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
        remote_options => $filtered
    });
}


=head2 get_remote_options

determine the options for this option-savvy field (option (=radio), checkbox, select)

remote_record is the record that was imported with the casetype. it represents
the state in the remote system.

=cut
sub get_remote_options {
    my ($self, $remote_record) = @_;

    # backwards compatibility: fallback on ->{options} if ->{extended_options}
    # are not available.
    return $remote_record->{extended_options} ||
        [map { {value => $_, active => '1'} } @{ $remote_record->{options} }] || [];
}


=head2 merge_local_options

Get local options that are not present in the remote options - to preserve these.

=cut
define_profile merge_local_options => (
    required => [qw/bibliotheek_kenmerken_id/],
    optional => [qw/remote_options/]
);
sub merge_local_options {
    my $self = shift;
    my $input = shift;

    # how can i make the profile assert that remote_options is present, but may
    # be an empty list?
    throw('casetype/import/merge_local_options', 'Opties niet gespecificeerd in import')
        unless exists $input->{remote_options};

    my $params = assert_profile($input)->valid;
    my $remote_options = $params->{remote_options};

    my $local = $self->dbic->resultset('BibliotheekKenmerken')->find(
        $params->{bibliotheek_kenmerken_id}
    );

    my @local_options = $local->bibliotheek_kenmerken_values->search({
        value => {
            'not in' => [map { $_->{value} } @$remote_options]
        }
    })->all;

    return [@$remote_options, map {{ value => $_->value, active => 0 }} @local_options];
}


=head2 kenmerk_reason

e.g. "Zaaktype import vanaf http://10.44.0.11/, kenmerk 'opplus import' versie: 2"

=cut

sub kenmerk_reason {
    my ($self, $remote_record) = @_;

    my $imported_zaaktype = $self->imported_zaaktype;
    my $reason = 'Zaaktype import';

    if ($imported_zaaktype->{origin}) {
        $reason .= " vanaf " . $imported_zaaktype->{origin};
    }
    $reason .= ", kenmerk '" . $remote_record->{naam} . "'";

    if ($remote_record->{version}) {
        $reason .= " versie " . $remote_record->{version};
    }

    return $reason;
}

sub execute_add_action {
    my ($self, $dependency, $table, $id) = @_;

    # create new item
    # first get the information on the new item from the db_dependencies repository, then
    # feed that to the database. receive the new id, put that in.
    my $remote_record = clone $self->lookup_remote_record($table, $id);
    
    foreach my $key (keys %$remote_record) {
        my $related_table = $self->tablename({id_field => $key});

        if($related_table && $related_table ne 'Zaaktype') {
            my $related_id = $remote_record->{$key};

            if($related_id) {
                my $related_dependency = $self->dependency_item({dependency_type => $related_table, id => $related_id});
        
                if($related_table && keys %$related_dependency) {
                    $self->log->debug("doing related item first $related_table $related_id");
                    my $new_id = $self->execute_change($related_dependency, $related_table, $related_id);
                    $remote_record->{$key} = $new_id;
                }
            } else {
                warn "empty id passed around, why oh why-oh";
            }
        }
    }

    my $dependency_config = ZAAKTYPE_DEPENDENCIES->{$table};
    my $solution = $dependency->solution;

    my $name_field = $dependency_config->{name};

    $remote_record->{$name_field} = $dependency->solution->{name};

    if($table eq 'Zaaktype') {
        # nop
    }
    
    else {
    
        # pay close scrutiny here
        my ($remote_options, $metadata);
        if($table eq 'BibliotheekKenmerken') {
            # since januari 2014 options have active and sort_order properties. this
            # code deals with backward compabitibility for that change.
            $remote_options = $self->get_remote_options($remote_record);
            delete $remote_record->{options};
            delete $remote_record->{extended_options};

            if ($remote_record->{metadata}) {
                $metadata       = { %{ $remote_record->{metadata} } };
                delete($metadata->{id});
                delete $remote_record->{metadata};
            }

            delete($remote_record->{file_metadata_id});
        }



        if($remote_record->{pid}) {
            delete $remote_record->{pid};
        }
        
        # categorie id is a special case, happens in non nice fashion. TODO 
        #
        if($solution->{bibliotheek_categorie_id} && exists $remote_record->{bibliotheek_categorie_id}) {
            $remote_record->{bibliotheek_categorie_id} = $solution->{bibliotheek_categorie_id};
        }
##
        die cluck "attempt to create categorie" if($table eq 'BibliotheekCategorie');


        if($table eq 'BibliotheekKenmerken') {
            if($solution->{magic_string}) {
                $remote_record->{magic_string} = $solution->{magic_string};
            }

            # the version is local only, since imports can come from different systems.
            # we could do this at the target, but it may come in handy to preserve
            # a little more information for the future.
            delete $remote_record->{version};
        }

        my $row;
        if($table eq 'Filestore') {
            my $archive     = $self->session->{upload};

            my $source      = $archive->extract_path . "/" . $id;
            my $filename    = $remote_record->{original_name};

            ### This is probably a tubular to documentairly import,
            ### no span: but we need to fix the extention and the filename,
            ### because tubular saves everything as odf.
            if (!$filename && $remote_record->{filename}) {
                $filename   = $remote_record->{filename};

                warn('CORRECTING TUBULAR');
                if ($filename =~ /\.odf$/i) {
                    warn('FOUND ODF');
                    if ($remote_record->{mimetype} =~ /oasis\.opendocument\.text/) {
                        warn('FOUND ODF: RENAME');
                        $filename =~ s/\.odf$/.odt/i;
                    }

                    if ($remote_record->{mimetype} =~ /oasis\.opendocument\.spreadsheet/) {
                        $filename =~ s/\.odf$/.ods/i;
                    }
                }
            }

            warn('Trying to add: ' . $filename);
            $row = $self->dbic->resultset($table)->filestore_create({
                original_name   => $filename,
                file_path       => $source,
            });
        } else {

            my $clean_record = $self->clean_record($table, $remote_record);

            $row = $self->dbic->resultset($table)->create($clean_record);
        }

        $self->log->debug("creating row $table $id, new local id: " . $row->id);
    
        if($table eq 'BibliotheekKenmerken') {
            $self->insert_options($row, $remote_options);

            ### Metatada
            if ($metadata) {
                my $metadata = $self->dbic->resultset("FileMetadata")->create(
                    $metadata
                );
                $row->file_metadata_id($metadata->id);
                $row->update;
            }         
        }

        $self->process_bibliotheek_notificatie($row, $remote_record)
            if $table eq 'BibliotheekNotificaties';

        return $row->id;
    }
}


=head2 insert_options

a notification can have attachments, which are stored in the linking
table 'bibliotheek_notificatie_kenmerk'. if we find these attachments,
add/update entries in this table

we are supplied with remote kenmerk_ids - so we need the local id's.

=cut

sub insert_options {
    my ($self, $row, $remote_options) = @_;

    my $rs = $self->dbic->resultset("BibliotheekKenmerkenValues");

    foreach my $option (@$remote_options) {

        my $record = {
            bibliotheek_kenmerken_id => $row->id,
            active                   => $option->{active},
            value                    => $option->{value}
        };

        # if not, let the auto increment do it's wonderful work
        # backwards compatibility happening here
        $record->{sort_order} = $option->{sort_order} if exists $option->{sort_order};

        $rs->create($record);
    }
}

=head2 process_bibliotheek_notificatie

this built on the assumption that dependencies are handled in alphabetical
order, notifications come after kenmerken so by the time we are working
notifications, we have already inserted kenmerken which means we can start
linking the real local id's.

by the way, this is currently only implemented for add - if we choose to
add something the notification it is added, otherwise it is up to the
administrator.

=cut

sub process_bibliotheek_notificatie {
    my ($self, $row, $remote_record) = @_;

    my $attachments = $remote_record->{attachment_bibliotheek_kenmerken_ids} || [];

    foreach my $kenmerk_id (@$attachments) {

        # first see if we know about this field - it is not necessarily
        # part of the casetype. if not, we ignore.
        my $dependency = $self->dependency_item({
            dependency_type => 'BibliotheekKenmerken',
            id => $kenmerk_id
        });

        next unless $dependency->id;

        # dependency_item is how the item will be imported in the database
        my $local_kenmerk_id = $dependency->local_id or die "need local kenmerk_id";

        # we need an entry in the db between $row->id (the locally present notification)
        # and local_kenmerk_id.
        $self->dbic->resultset('BibliotheekNotificatieKenmerk')->find_or_create({
            bibliotheek_kenmerken_id   => $local_kenmerk_id,
            bibliotheek_notificatie_id => $row->id,
        });
    }
}


sub check_dependencies {
    my ($self) = @_;

    my $imported_zaaktype = $self->imported_zaaktype;

    $self->problems(0);
    $self->traverse_zaaktype({ data=>$imported_zaaktype, ancestry=>[] });
}




{
    Params::Profile->register_profile(
        method  => 'traverse_zaaktype',
        profile => {
            'optional'      => [ qw/
                ancestry
                data
            /],
        }
    );
    sub traverse_zaaktype {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;
        
        my $ancestry = $params->{ancestry};
        my $data     = $params->{data};
        
        foreach my $key (sort keys %$data) {  
            next if($key eq 'BibliotheekCategorie');
            my $child_data = $data->{$key};
            next unless defined $child_data;
            
            if(ref $child_data && ref $child_data eq 'HASH') {
                my $new_ancestry = clone $ancestry;
                push @$new_ancestry, $key;
                $self->traverse_zaaktype({data=>$child_data, ancestry=>$new_ancestry});
    
            } elsif(ref $child_data && ref $child_data eq 'ARRAY') {
            } else {
                
                if(my $table = $self->tablename({id_field => $key})) {

                    if(
                        $data->{$key}                   && 
                        $data->{$key} =~ m|^\d+$|       && 
                        int($data->{$key}) > 0          &&
                        $table ne 'BibliotheekCategorie'
                    ) {
                        $self->check_dependency($table, $data, $key, $ancestry);
                    }
                }
            }
        }
    }


}






{
    Params::Profile->register_profile(
        method  => 'tablename',
        profile => {
            required      => [ qw/
                id_field
            /],
        }
    );
    
    sub tablename {
        my ($self, $params) = @_;
    
        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;
    
        my $id_field = $params->{id_field};

        my $config = ZAAKTYPE_DEPENDENCY_IDS;
        foreach my $regexp (keys %$config) {
            next unless($id_field =~ m|$regexp|);
    
            return $config->{$regexp};
        }
        
        return undef;
    }

}

#
# the export script has generously provided us with a lookup table containing
# full table rows of the referenced items. lookup the table row that goes
# with the id, then have the 'match' function check if it's a match. in that case
# all is well and dandy and we do nothing. if there's reasonable doubt, give the 
# user a choice. 
#
sub check_dependency {
    my ($self, $table, $data, $key, $ancestry) = @_;

    my $id = $data->{$key};
    
    my $dependency = $self->dependency_item({dependency_type => $table, id => $id});

    if($table eq 'Zaaktype' && $id eq $self->imported_zaaktype->{zaaktype}->{id}) {
        $dependency->{main_zaaktype} = 1;
    }
    # if the reference has already been looked up, just add the ancestry to the item. e.g. one
    # kenmerk may be used in different phases of the zaaktype. It only needs to be resolved once,
    # but we need to know where to replace it during the actual import.
    if($dependency->solution) {
        $dependency->add_ancestry($ancestry, $key);
        return;
    }

    my $remote_record;
    unless($remote_record = $self->lookup_remote_record($table, $id)) {
        $self->log->debug("table: $table" . Dumper( $data) . 'key: '. $key . Dumper $ancestry);
        my $message = 'Fout in zaaktype bestand. ' . " $key $table $id mist. ";
        if($table eq 'BibliotheekNotificaties') {
            $message .= "Exporteer het zaaktype bestand opnieuw met een recente versie (3.11 of hoger)."
        }
        die $message . "\n";
    }
    
    my $match_fields    = ZAAKTYPE_DEPENDENCIES->{$table}->{match};
    my $name_field      = ZAAKTYPE_DEPENDENCIES->{$table}->{name};
    my $condition       = {map { $_ => $remote_record->{$_} } @$match_fields};    

    # record the original name of the item
    $dependency->name($remote_record->{$name_field});
    $dependency->id($data->{$key});
    $dependency->add_ancestry($ancestry, $key);

    my $match = $self->find_match($table, $dependency, $condition, $data);

    # category stuff
    my $dependency_config = ZAAKTYPE_DEPENDENCIES->{$table};

    my $is_ldap = ($table eq 'LdapRole' || $table eq 'LdapOu');

    if($match->{count} == 0 && !$is_ldap) {
    
        # inform the user that the item has not been found and ask for permission to insert it
        my $solution = {
            action  => 'add',
            name    => $remote_record->{$name_field},
        };

        if($dependency_config->{has_category}) {
            $self->check_category($dependency, $solution, $remote_record);
        } else {
            $dependency->solution($solution);
        }

    } elsif($match->{count} == 1) {
        die "coding error, match should contain somethin (feed programmer more coffee)" unless($match->{id});
        
        my $name = $remote_record->{$name_field};

        unless($dependency->solution) {
            my $solution = {
                action => 'use_existing',
                id     => $match->{id},
                name   => $name,
            };

            if ($table eq 'BibliotheekKenmerken') {
                my $row = $self->find_kenmerk_cached($solution->{id});
                if ($row && $row->uses_options) {
                    $solution->{use_remote_field_options} = 'on';
                }
            }
            $dependency->solution($solution);
        }

        if($table eq 'Zaaktype') {
            $dependency->id($match->{id});
        }

    } else {
        unless($table eq 'BibliotheekCategorie') {
            $self->problems($self->problems + 1);
            $self->log->debug("no automatic resolution found for $key $table $id ($match->{count})");
        }
    }
}


sub find_kenmerk_cached {
    my ($self, $id) = @_;

    my $cache = $self->{_kenmerk_cache};
    return $cache->{$id} if exists $cache->{$id}; #can be undef
    return $cache->{$id} = $self->dbic->resultset('BibliotheekKenmerken')->find($id);
}


sub check_category {
    my ($self, $dependency, $solution, $remote_record) = @_;

    # if we don't already have it, search for it
    unless ($dependency->bibliotheek_categorie_id) {
        my $categorie_id = $self->find_category_match($remote_record->{bibliotheek_categorie_id});
        $dependency->bibliotheek_categorie_id($categorie_id);
    }

    # then, see do we have it now?
    if ($dependency->bibliotheek_categorie_id) {
        $solution->{bibliotheek_categorie_id} = $dependency->bibliotheek_categorie_id;
        $dependency->solution($solution);         
    } else {
        # nope, user will have to clear up the category thing before this can be inserted
        $dependency->remove_solution();
        $self->problems($self->problems + 1);
    }
}


=head2 find_category_match

find out if the given category is present on the receiving system. track it
back to the top. also, if this dependency has been cleared already, offer
that solution

first retrieve a list with subcategories.

traverse that list:
- look for the deepest level
- if not found, return undef
- if found, see if it's parent also matches up

the end result is a local category

=cut

sub find_category_match {
    my ($self, $bibliotheek_categorie_id) = @_;

    my $path = $self->remote_category_path($bibliotheek_categorie_id);

    return $self->find_category_path($path);
}


sub find_category_path {
    my ($self, $path, $pid) = @_;

    my ($top, @tail) = @$path;
# warn "top: " . Dumper $top;
# warn "path: " . Dumper $path;

    return $pid unless $top;

    my $row = $self->find_category_cached({
        naam => $top->{name},
        pid  => $pid
    });

    return unless $row;

    return $row->id unless @tail;

    return $self->find_category_path(\@tail, $row->pid);
}


=head2 

Since we're not creating categories during import, we can safely memoize these
values.

=cut

sub find_category_cached {
    my ($self, $where) = @_;

    my $cache = $self->{_category_cache} ||= {};

    my $cache_key = Dumper $where; # yes we can - to serialize potential undef values into a unique key

    return $cache->{$cache_key} if exists $cache->{$cache_key};

    return $cache->{$cache_key} = $self->dbic->resultset("BibliotheekCategorie")->find($where);
}

=head2 remote_category_path

using the supplied info of the remote system, find the path.

e.g.
/Primaire processen/Burgerzaken/Algemeen/

return ['Primaire processen', 'Burgerzaken', 'Algemeen'];

=cut

sub remote_category_path {
    my ($self, $id, $path) = @_;

    my $remote_record = $self->lookup_remote_record('BibliotheekCategorie', $id);

    $path ||= [];

    die "possible loop" if grep { $_->{id} eq $id } @$path;

    my $extended = [@$path, {
        id   => $id, 
        name => $remote_record->{naam}
    }];

    my $pid = $remote_record->{pid};

    return $pid ? $self->remote_category_path($pid, $extended) : $extended;
}


sub lookup_remote_record {
    my ($self, $table, $id) = @_;

    return $self->imported_zaaktype->{db_dependencies}->{$table}->{$id};
}




sub find_match {
    my ($self, $table, $dependency, $condition, $data) = @_;

    my $result = {count => 0};

    if($table eq 'LdapRole' || $table eq 'LdapOu') {
                
        my $ldap_match = $self->check_ldap_dependencies($table, $condition);

        if($ldap_match) {
            $result->{count} = 1;
            $result->{id} = $ldap_match->{id};
        }

    } elsif($table eq 'Zaaktype') {

        my $resultset = $self->dbic->resultset("ZaaktypeNode")->search({
                'me.titel' => $dependency->name,
                'zaaktypes.deleted' => undef,
                'me.deleted' => undef,
            },
            {
                join     => 'zaaktypes',
                distinct => 1,
                columns  => ['zaaktype_id'],
            }
        );
        $result->{count} = $resultset->count();
        if($result->{count} == 1) {
            my $row = $resultset->first();
            

            $dependency->id($row->zaaktype_id->id);
            $result->{id} = $row->zaaktype_id->id;
        }
    } else {
        my $cleaned = $self->clean_record($table, $condition);

        if(exists $cleaned->{naam}) {
            my $naam_filter = $cleaned->{naam};
            $cleaned->{naam} = {'ilike' => $naam_filter};
        }

        # since files are unique id-ed by their md5
        if($table eq 'Filestore') {
            my $resultset    = $self->dbic->resultset($table)->search($cleaned);
            $resultset = $resultset->search({}, { rows => 1});
            $result->{count} = $resultset->count();

            if($result->{count} == 1) {
                my $db_row = $resultset->first();
                $result->{id} = $db_row->id;
            }
            return $result;
        }

        return $self->find_match_cached($table, $cleaned);
    }

    return $result;
}



sub find_match_cached {
    my ($self, $table, $cleaned) = @_;

    my $cache = $self->{_dependency_cache} ||= {};

    my $cache_key = Dumper {
        table   => $table,
        cleaned => $cleaned
    };
    return $cache->{$cache_key} if exists $cache->{$cache_key};

    return $cache->{$cache_key} = $self->find_table_result($table, $cleaned);
}

sub find_table_result {
    my ($self, $table, $cleaned) = @_;

    my $rs = $self->dbic->resultset($table)->search($cleaned);
    my $count = $rs->count;

    my $result = {count => $count};
    $result->{id} = $rs->first->id if $count == 1;

    return $result;
}


sub check_ldap_dependencies {
    my ($self, $table, $condition) = @_;

    my $items = $table eq 'LdapOu' ? $self->groups->search_ou() : $self->groups->search();
    
    foreach my $item (@$items) {
        my $score = 0;
        foreach my $field (keys %$condition) {
            $score++ if($item->{$field} eq $condition->{$field});
        }
        return $item if($score == scalar keys %$condition);  # return the first match
    }
    return undef;
}


#
# these contain all the options for a dependency. e.g. in the case of 'kenmerken', this
# returns a list with all the kenmerken that can be linked.
#
sub dependency_options {
    my ($self, $dependency_type, $query) = @_;
    
    my $dependency_config = ZAAKTYPE_DEPENDENCIES->{$dependency_type}
        or return undef;

    my $table = $dependency_type;

#    unless($self->_session->{dependency_options}->{$table}) {
        my $name_field = $dependency_config->{name};
        my $options = {};

        my $resultset;        
        my @options = ();

        if($table eq 'Zaaktype') {
            $resultset = $self->dbic->resultset("Zaaktype")->search({
                'me.deleted' => undef,
            }, {
                prefetch => 'zaaktype_node_id',
                order_by => 'zaaktype_node_id.titel'
            });
            
            while (my $item = $resultset->next) {
                push @options, { 
                    id      => $item->id,
                    name    => $item->zaaktype_node_id->titel,
                };
            }
        } elsif($table eq 'LdapOu') {
            my $items = $self->groups->search_ou();
            foreach my $item (@$items) {
                push @options, { 
                    id      => $item->{id},
                    name      => $item->{ou},
                };
            }
        } elsif($table eq 'LdapRole') {
            my $items = $self->groups->search();
            foreach my $item (@$items) {
                push @options, { 
                    id      => $item->{id},
                    name    => $item->{short_name},
                };
            }
        } else {
            $options->{order_by} = $name_field;

            my $alternative_condition = {};
            $self->log->debug("table: $table");
            if($table eq 'BibliotheekKenmerken') {
                $alternative_condition->{system} = undef;
            }

            $resultset = $self->dbic->resultset($table)->search($alternative_condition, $options);

            while (my $item = $resultset->next) {
                push @options, { 
                    id      => $item->id,
                    name    => $item->$name_field,
                };
            }
        }

        
        $self->session->{dependency_options}->{$table} = \@options;
#    }
    
    if($query && $query->{id}) {
        foreach my $option (@{$self->session->{dependency_options}->{$table}}) {
            return $option if($option->{id} eq $query->{id});
        }
        return undef;
    }
    if($query && $query->{name}) {
        foreach my $option (@{$self->session->{dependency_options}->{$table}}) {
            return $option if($option->{name} eq $query->{name});
        }
        return undef;
    }

    return $self->session->{dependency_options}->{$table};
}




{
    Params::Profile->register_profile(
        method  => 'dependency_item',
        profile => {
            required        => [ qw/
                dependency_type
                id
            /],
        }
    );    
    sub dependency_item {
        my ($self, $params) = @_;
        
        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" .Dumper ($params) unless $dv->success;

        my $dependency_type = $params->{dependency_type};
        my $id              = $params->{id};

        unless(exists $self->session->{import_dependencies}->{$dependency_type}->{$id}) {
            $self->session->{import_dependencies}->{$dependency_type}->{$id} = 
                new Zaaksysteem::Zaaktypen::Dependency();
        }
        return $self->session->{import_dependencies}->{$dependency_type}->{$id};
    }
}


sub flush {
    my ($self) = @_;

    $self->log->debug('flush zaaktype import session');
    my $extract_path = $self->filepath();
    system("rm -rf ${extract_path}*");
    $self->session({});
}


=head2 clean_record

Only fields that appear in the imported record and the local database schema can be
imported. The others can be filtered out.

=cut

sub clean_record {
    my ($self, $table, $remote_record) = @_;

    my @columns = $self->dbic->source($table)->columns;

    return {
        map  { $_ =>  $remote_record->{$_} }
        grep { exists $remote_record->{$_} } @columns
    };
}


__PACKAGE__->meta->make_immutable;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


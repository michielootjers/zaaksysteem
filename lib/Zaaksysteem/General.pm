package Zaaksysteem::General;

use strict;
use warnings;
use Zaaksysteem::Constants;
use Data::Dumper;
use Scalar::Util qw/blessed/;
use Net::LDAP;
use Number::Format; 
use HTTP::BrowserDetect;

use base qw/
    Zaaksysteem::General::Authentication
    Zaaksysteem::General::Upgrade
    Zaaksysteem::General::Actions
/;



sub add_trail {
    my ($c, $opts) = @_;

    if (!$c->stash->{trail}) {
        $c->stash->{trail} = [];
        $c->stash->{trail}->[0] = {
            'uri'   => $c->uri_for('/'),
            'label' => 'Dashboard',
        };
    }

    push(@{ $c->stash->{trail} }, $opts);
}


sub zvalidate {
    my ($c, $dv, $opts) = @_;

    unless (defined($dv)) {
        $dv = $c->check(params => $c->req->params, method => [caller(1)]->[3]);
    }

    die('Definition not found for: ' . [caller(1)]->[3]) unless ref($dv);

    $c->stash->{last_validation} = $dv;

    if ($c->req->is_xhr &&
        (!defined($opts->{bypass_json}) || !$opts->{bypass_json})
    ) {
        # Do some JSON things
        my $json = {
            success     => $dv->success,
            missing     => [ $dv->missing ],
            invalid     => [ $dv->invalid ],
            unknown     => [ $dv->unknown ],
            valid       => [ $dv->valid ],
            msgs        => $dv->msgs,
        };

        $c->zcvalidate($json);
    }

    if ($dv->success) { return $dv; }

    ### Go log something
    my $errmsg = "Problems validating profile:";
    $errmsg .= "\n        Missing params:\n        * " .
                join("\n        * ", $dv->missing)
                if $dv->has_missing;
    $errmsg .= "\n        Invalid params:\n        * " .
                join("\n        * ", $dv->invalid)
                if $dv->has_invalid;
    $errmsg .= "\n        Unknown params:\n        * " .
                join("\n        * ", $dv->unknown)
                if $dv->has_unknown;
    $c->log->debug($errmsg);

    return;
}


sub zcvalidate {
    my ($c, $opts) = @_;
    my ($json);

    return unless $c->req->is_xhr;

    unless (
        $opts->{success} ||
        ($opts->{invalid} && UNIVERSAL::isa($opts->{invalid}, 'ARRAY'))
    ) {
        return;
    }
    $json->{invalid}    = $opts->{invalid};

    $json->{success}    = $opts->{success} || undef;
    $json->{missing}    = $opts->{missing} || [];
    $json->{unknown}    = $opts->{invalid} || [];
    $json->{valid}      = $opts->{valid}   || [];
    $json->{msgs}       = $opts->{msgs}    || [];


    $c->stash->{json} = $json;
    $c->log->debug('JSON response body', Dumper($c->stash->{ json }));;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

sub is_externe_aanvraag {
    my $c   = shift;

    return if $c->user_exists;

    return 1;
}

sub about {
    my $c   = shift;

    return {
        applicatie      => ZAAKSYSTEEM_NAAM,
        omschrijving    => ZAAKSYSTEEM_OMSCHRIJVING,
        leverancier     => ZAAKSYSTEEM_LEVERANCIER,
        versie          => $c->config->{SVN_VERSION},
        startdatum      => ZAAKSYSTEEM_STARTDATUM,
        licentie        => ZAAKSYSTEEM_LICENSE
    };
}

sub betrokkene_session {
    my $c           = shift;

    return unless $c->session->{betrokkene_session};

    return $c->stash->{_betrokkene_session} if ($c->stash->{_betrokkene_session});

    return ($c->stash->{_betrokkene_session} = $c->model('Betrokkene')->get(
            {},
            $c->session->{betrokkene_session}->{betrokkene_identifier}
        )
    );
}

sub betrokkene_session_enable {
    my $c           = shift;
    my $betrokkene  = shift;

    return unless ref($c);

    $c->log->info(
        'Starting betrokkene session for: '
        . $betrokkene->naam
    );

    $c->session->{betrokkene_session} = {
        betrokkene_identifier   => $betrokkene->betrokkene_identifier,
        betrokkene_naam         => $betrokkene->naam,
    };

    # Remove the cached "betrokkene_session", and re-cache.
    delete $c->stash->{_betrokkene_session};
    $c->betrokkene_session();
}

sub betrokkene_session_disable {
    my $c           = shift;
    my $betrokkene  = shift;

    return unless ref($c);

    return unless $c->session->{ betrokkene_session };

    $c->log->info(
        'Ending betrokkene session for: '
        . $c->betrokkene_session->naam
    );

    delete($c->session->{betrokkene_session});

    return 1;
}

sub is_allowed_browser {
    my ($c) = @_;

    my $bd          = HTTP::BrowserDetect->new($c->req->user_agent);

    my $browserlist = DENIED_BROWSERS;

    for my $browserentry (@{ $browserlist }) {
        my ($browser, $version) = split(/::/, $browserentry);

        ### Not in list
        next unless ($bd->can($browser) && $bd->$browser);

        ### Browser detected, but No version information, e.g. "ie"
        if (!$version) {
            $c->log->debug('Found browser: ' . $browser);
            return;
        }

        ## Version info
        if ($version =~ /^<\d+/) {
            ### browser is below version
            $version =~ s/^<//;

            if ($bd->version < $version) {
                return;
            }
        } elsif ($version =~ /^>\d+/) {
            ### browser is above version
            $version =~ s/^>//;

            if ($bd->version > $version) {
                return;
            }
        } elsif ($version =~ /\d+/) {
            ### browser is given version
            if ($bd->version == $version) {
                return;
            }
        }
    }

    ### No blacklist detected, return 1
    return 1;
}



1;



=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


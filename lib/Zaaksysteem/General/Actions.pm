package Zaaksysteem::General::Actions;

=head1 NAME

Zaaksysteem::General::Actions - Different actions to call on $c

=head1 SYNOPSIS

  $can_upload = $c->can_upload;

=head1 DESCRIPTION

When you would like to create a method in the L<Zaaksysteem> namespace, please DO NOT place
them directly in L<Zaaksysteem>. It prevents you from testing the method entirely. Instead, place
it in one of the classes subclassed in L<Zaaksysteem::General>, or, much easyer: in this class.

This way they will be all found together.

=head1 METHODS

=cut

### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;
    
    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if($case->status eq 'open' || $case->status eq 'stalled') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if ($case->status eq 'stalled') {
        $c->push_flash_message(sprintf(
            '<span class="flash-urgent">Zaak opgeschort: %s</span>',
            $case->reden_opschorten || ''
        ));
    }
}



=head2 $c->can_change

Arguments: none

Return value: true, when a person is allowed to change zaak

Determines wether the current user is allowed to make changes
on the current case.

=cut

sub can_change {
    my ($c, $opt) = @_;

    my $zaak = $c->stash->{zaak};
    return unless $zaak;

    # only once per request
    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
        $c->_can_change_messages;
    }

    return if $zaak->is_afgehandeld && !$opt->{ignore_afgehandeld};

    ### Zaak beheerders mogen wijzigingen aanbrengen ondanks dat ze geen
    ### behandelaar.
    return 1 if $c->check_any_zaak_permission('zaak_beheer');

    ### Override when we have the correct permissions, and we have a
    ### coordinator and behandelaar
    return 1 if (
        $zaak->behandelaar &&
        $zaak->coordinator &&
        $c->check_any_zaak_permission('zaak_beheer','zaak_edit')
    );

    if (
        !$zaak->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $zaak->behandelaar &&
            $zaak->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber &&
            (
                !$zaak->coordinator ||
                $zaak->coordinator->gegevens_magazijn_id ne $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        );

        return;
    }

    return 1;
}

=head2 $c->check_queue_coworker_changes

Arguments: TODO

Return value: TODO

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub check_queue_coworker_changes {
    my $c    = shift;
    my $zaak = $c->stash->{zaak};

    # admins always get what they want, this doesn't need to be queued
    return if $c->check_any_user_permission('admin');

    return unless $c->user;

    return $zaak->check_queue_coworker_changes($c->user->uidnumber);
}



# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#

=head2 $c->push_flash_message

Arguments: \%MSG | @MSGS

Return value: $flash_message

TODO: needs documenting

Pushes a flash message on the stack and returns the given message.

=cut

sub push_flash_message {
    my $c       = shift;
    my $message;

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result;

    $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
        };
    }
    
    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }
    
    $c->flash->{result} = $result;    
    
    return $result;
}



=head2 $c->can_upload

Arguments: none

Return value: true, when a person is allowed to upload in zaak

Will return C<true> when a user is able to upload in the current dossier. It differs from
C<can_change> in the way that people are able to add documents to the document queue when the
dossier is not yet resolved. Even when they are not allowed to alter the dossier.

In short: it will return C<true> when the case is open/stalled or new.

=cut

sub can_upload {
    my ($c) = @_;

    return unless $c->stash->{zaak};

    return 1 if $c->stash->{zaak}->status =~ /^open|stalled|new$/;

    return;
}

1;

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2013, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
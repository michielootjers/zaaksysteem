package Zaaksysteem::Exception;

use base 'Exporter';

use Data::Dumper;
use Params::Profile;
use Try::Tiny qw[try catch finally];

use Zaaksysteem::ZAPI::Error;

our @EXPORT = qw[throw try catch finally];

=head1 NAME

Zaaksysteem::Exception - Our personal exception class.

=head1 SYNOPSIS

    ### The simplest of versions version

    sub somewhere_in_space {
        throw('Houston, we have a problem');
    }

    ### Simple version
    #
    sub somewhere_in_space {
        ### Space? That can't be right

        throw('invalid/space', 'Got in space, did you mean world?');
    }

    try {
        somewhere_in_space();
    } catch {
        print "Got error: " . $_->error;
    }

    ### Extended version
    sub somewhere_in_hello {
        ### Hello? world!

        throw(
            {
                type        => 'invalid/hello',
                message     => 'Hello? World!',
            }
        );
    }

    try {
        somewhere_in_space();
    } catch {
        print "Got error: " . $_->error;
    }


=head1 DESCRIPTION

Our personal exception class, handling our exceptions as a charm. With
superduper Angular support.

=head1 METHODS

=head2 throw(\%NAMED_HASH || @LIST_OF_PARAMS);

Returns: Exception

Throws an Exception and makes sure this exception matches our "way" of
returning errors.

There are two ways of calling this method, by using a named key value pair,
or by giving it a ordered list of params.

B<Options in order>

=over 4

=item type [optional]

Type: String
Default: 'unknown'

A predefined error code. See below for a list of allowed error codes. This
is normally in a C<catagory/name> style.

=item message [optional]

Type: String
Default: 'No error message set'

A human readable error message, containing more descriptive information about
this single error

=item object [optional]

Type: HashRef

An object containing more information about the context of this error, this
could be, for example, a L<Data::FormValidator> object.

=back

=cut


sub throw {
    my $args = {};

    my $c = scalar(@_);

    if ($c == 1 && UNIVERSAL::isa($_[0], 'HASH')) {
        $args = shift;
    }
    elsif ($c == 1) {
        $args->{type}    = 'unknown';
        $args->{message} = shift;
    }
    else {
        $args->{type}    = shift || 'unknown';
        $args->{message} = shift || 'No error message set';
        $args->{object} = shift if $_[0];
    }

    if(exists $args->{error_message}) {
        $args->{message} = delete $args->{error_message}
    }

    return Zaaksysteem::Exception::Base->throw($args);
}

# Base exception should only be used from this file,
# no need for a seperate file

package Zaaksysteem::Exception::Base;

use Moose;
use Data::Dumper;

extends 'Throwable::Error';

has 'type'          => ( is => 'ro' );
has 'object'        => ( is => 'ro' );

has 'debug'         => (
    is      => 'ro',
    default => sub { return $ENV{CATALYST_DEBUG} }
);

sub get_ZAPI_error {
    my $self = shift;

    return Zaaksysteem::ZAPI::Error->new(
        type            => $self->type,
        messages        => [ $self->message ],
        data            => ($self->object || undef),
        stacktrace      => [ $self->trace_frames ],
        debug           => $self->debug,
    );
}

sub trace_frames {
    my $self = shift;

    return unless $self->debug;

    return map { $_->as_string } $self->stack_trace->frames;
}

sub TO_JSON {
    return shift->get_ZAPI_error->response;
}

sub TO_STRING {
    my $self = shift;

    return sprintf("%s: %s", $self->type, $self->message);
}

sub as_string {
    my $self = shift;
    return $self->TO_STRING . "\n";
}

=head1 PREDEFINED ERROR CODES

Although you do not have to use one of our predefined codes, there is a list of
most commonly used error codes. Below is a list and their capabilities.

=over 4

=item C<params/profile>

 $dv = Data::FormValidator->check({ subject => '' }, { required => ['subject']});

 throw(
    {
        error_code  => 'params/profile',
        message     => 'Validation failed',
        object      => $dv,
    }
 ) unless $dv->success;

When you attempt to throw an error, because a L<Data::FormValidator> object
does not pass the C<< ->success >> test, you can throw this error. Make sure
you supply the L<Data::FormValidator::Results> object in the data parameter
as C<dv>

It will make sure our JSON API receives a proper definition of what failed
during this parameter check, so Angular Forms can create a proper error.

=back

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

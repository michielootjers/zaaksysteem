package Zaaksysteem::CLI::Knife::Init;

use Moose;

use Getopt::Long qw/GetOptionsFromArray :config pass_through/;
use Zaaksysteem::CLI::Knife::Action qw/$actions/;
use Zaaksysteem::Tools;

extends "Zaaksysteem::CLI";

=head1 NAME

Zaaksysteem::CLI::Knife::Init - Initializes the ZS Knife system, and supplies helper functions

=head1 SYNOPSIS

See L<Zaaksysteem::CLI::Knife>

=head1 DESCRIPTION

This class contains the helper functions for the zsknife system, and supplies helper functions

=cut

## _actions
##
## returns L<Zaaksysteem::CLI::Knife::Action::$actions>
##

has '_actions' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return $actions;
    }
);

=head1 METHODS

=head2 get_knife_params

Returns the params from the commandline in a hashref.

=head3 SYNOPSIS

    ./bin/zsknife say hello world name=don age=24

    my $params = $self->get_knife_params;

    # $params is
    {
         name    => 'don',
         age     => 24,
    }

=head3 RETURNS

An hashref if parameters are found, otherwise undef

=cut

sub get_knife_params {
    my $self                            = shift;
    my ($knife, $cat, $action, @params) = $self->get_clean_argv;

    my %rv;
    for my $param (@params) {
        my ($key, $value) = $param =~ /^(\w+?)=(.*)$/;
        next unless defined $value;

        $rv{$key} = $value;
    }

    $self->log->trace("get_knife_params: " . dump_terse(\%rv));

    return %rv ? \%rv : undef;
}

=head2 assert_knife_params

=cut

sub assert_knife_params {
    my $self = shift;
    my %profile = @_;

    my $params = $self->get_knife_params;

    foreach (keys %profile) {
        if (!exists $params->{$_}) {
            throw("zsknife/params", "required param $_ missing");
        }
    }
    return $params;
}

=head2 get_clean_argv

    # zsknife --hostname 10.44.0.11 say hello world key1=value1 key2=value2

    my @argv = $self->get_clean_argv;

    # contains: ('say','hello','world','key1=value1','key2=value2')

Returns the argv without the program options

=cut

sub get_clean_argv {
    my $self                    = shift;
    my @cli_args                = @ARGV;

    GetOptionsFromArray(\@cli_args, {}, @{ $self->commandline_options });

    return @cli_args;
}

=head2 assert_actions

Return value: $TRUTH (or die)

    $self->assert_actions

Will check if the given commandline options are correct

=cut

sub assert_actions {
    my $self        = shift;

    my ($knife, $cat, $action, @params) = $self->get_clean_argv();

    unless ($action && $cat && $knife) {
        $self->show_usage_for_actions;
    }

    throw('zsknife/cat_not_implemented', "Category $cat not implemented") unless $self->_actions->{$knife}->{categories}->{$cat};
    throw('zsknife/action_not_implemented', "Action $action not implemented") unless $self->_actions->{$knife}->{categories}->{$cat}->{actions}->{$action};

    return 1;
}

=head2 run_action

Arguments: none

Return value: &SUB_RESULT

    $self->run_action;

Runs the requested action

=cut

sub run_action {
    my $self        = shift;

    my ($knife, $cat, $action, @params) = $self->get_clean_argv();

    return $self->_actions->{$knife}->{categories}->{$cat}->{actions}->{$action}->{code}->($self, @params);
}

=head2 show_usage_for_actions

Arguments: [ $STRING_CUSTOM_MESSAGE ]

    $self->show_usage_for_actions;

    $self->show_usage_for_actions('Error: please supply param x');

Shows zsknife usage, optionally started with a custom message

=cut

sub show_usage_for_actions {
    my $self                    = shift;
    my $custommsg               = shift;
    my ($knife, $cat, $action, @params) = $self->get_clean_argv;

    my $msg = '';
    $msg .= "$custommsg\n\n" if $custommsg;

    $msg .= "\nUSAGE: $0 knife category action [param1 [param2 [..]]]\n\n";
    $msg .= "Available subcommands, knife style:";
    for my $knife (keys %{ $self->_actions }) {
        $msg .= uc("\n\n** $knife COMMANDS **");
        for my $cat (keys %{ $self->_actions->{$knife}->{categories} }) {
            # $msg .= "\n $cat - " . $actions->{$knife}->{categories}->{$cat}->{description};
            for my $action (keys %{ $self->_actions->{$knife}->{categories}->{$cat}->{actions}}) {
                $msg .= sprintf("\nzsknife %-50s - ", "$knife $cat $action (options)") . $self->_actions->{$knife}->{categories}->{$cat}->{actions}->{$action}->{description};
            }
        }
    }

    die($msg . "\n");
}

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

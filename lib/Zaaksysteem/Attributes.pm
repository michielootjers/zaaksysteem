package Zaaksysteem::Attributes;
use warnings;
use strict;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_BETROKKENE_SUB
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_LEVERANCIER
    ZAAKSYSTEEM_LICENSE
    ZAAKSYSTEEM_NAAM
    ZAAKSYSTEEM_OPTIONS
    ZAAKSYSTEEM_STARTDATUM
/;

use Zaaksysteem::Backend::Object::Attribute;
use Zaaksysteem::Types::MappedString;

use Exporter 'import';
our @EXPORT     = qw/
    ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
/;

BEGIN {
    my @person_fields = (
        {
            bwcompat      => 'aanhef',
            name          => 'salutation',
            internal_name => 'aanhef',
        },
        {
            bwcompat      => 'aanhef1',
            name          => 'salutation1',
            internal_name => 'aanhef1',
        },
        {
            bwcompat      => 'aanhef2',
            name          => 'salutation2',
            internal_name => 'aanhef2',
        },
        {
            bwcompat      => 'afdeling',
            name          => 'department',
            internal_name => 'afdeling',
        },
        {
            bwcompat      => 'achternaam',
            name          => 'surname',
            internal_name => 'achternaam',
        },
        {
            bwcompat      => 'burgerservicenummer',
            name          => 'bsn',
            internal_name => 'burgerservicenummer',
        },
        {
            bwcompat      => 'email',
            name          => 'email',
            internal_name => 'email',
        },
        {
            bwcompat      => 'geboortedatum',
            name          => 'date_of_birth',
            internal_name => 'geboortedatum',
            type          => 'timestamp',
        },
        {
            bwcompat      => 'geboorteplaats',
            name          => 'place_of_birth',
            internal_name => 'geboorteplaats',
        },
        {
            bwcompat      => 'geslacht',
            name          => 'gender',
            internal_name => 'geslacht',
        },
        {
            bwcompat      => 'geslachtsnaam',
            name          => 'family_name',
            internal_name => 'geslachtsnaam',
        },
        {
            bwcompat      => 'handelsnaam',
            name          => 'trade_name',
            internal_name => 'handelsnaam',
        },
        {
            bwcompat      => 'huisnummer',
            name          => 'house_number',
            internal_name => 'huisnummer',
        },
        {
            bwcompat      => 'huwelijksdatum',
            name          => 'date_of_marriage',
            internal_name => 'datum_huwelijk',
            type          => 'timestamp',
        },
        {
            bwcompat      => 'kvknummer',
            name          => 'coc',
            internal_name => 'kvknummer',
        },
        {
            bwcompat      => 'mobiel',
            name          => 'mobile_number',
            internal_name => 'mobiel',
        },
        {
            bwcompat      => 'naam',
            name          => 'name',
            internal_name => 'naam',
        },
        {
            bwcompat      => 'overlijdensdatum',
            name          => 'date_of_death',
            internal_name => 'datum_overlijden',
            type          => 'timestamp',
        },
        {
            bwcompat      => 'postcode',
            name          => 'zipcode',
            internal_name => 'postcode',
        },
        {
            bwcompat      => 'straat',
            name          => 'street',
            internal_name => 'straat',
        },
        {
            bwcompat      => 'tel',
            name          => 'phone_number',
            internal_name => 'tel',
        },
        {
            bwcompat      => 'type',
            name          => 'type',
            internal_name => 'type',
        },
        {
            bwcompat      => 'volledigenaam',
            name          => 'full_name',
            internal_name => 'volledigenaam',
        },
        {
            bwcompat      => 'voornamen',
            name          => 'first_names',
            internal_name => 'voornamen',
        },
        {
            bwcompat      => 'voorvoegsel',
            name          => 'surname_prefix',
            internal_name => 'voorvoegsel',
        },
        {
            bwcompat      => 'woonplaats',
            name          => 'place_of_residence',
            internal_name => 'woonplaats',
        },
    );

    sub _get_aanvrager_attributes {
        my @attributes;

        my @aanvrager_fields = (
            @person_fields,
            {
                bwcompat      => 'anummer',
                name          => 'a_number',
                internal_name => 'a_nummer',
            },
            {
                bwcompat      => 'login',
                name          => 'login',
                internal_name => 'login',
            },
            {
                bwcompat      => 'password',
                name          => 'password',
                internal_name => 'password',
            },
        );

        for my $aanvrager_field (@aanvrager_fields) {
            push @attributes, Zaaksysteem::Backend::Object::Attribute->new(
                name                     => "requestor.$aanvrager_field->{name}",
                attribute_type           => $aanvrager_field->{type} || 'text',
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    my $betrokkene = $zaak->aanvrager_object;
                    return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, $aanvrager_field->{internal_name});
                },
                exists($aanvrager_field->{bwcompat})
                    ? (bwcompat_name => "aanvrager_$aanvrager_field->{bwcompat}")
                    : (),
            );
        }

        push @attributes, Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'requestor.id',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->aanvrager_object->betrokkene_identifier;
            },
        );

        return @attributes;
    }

    sub _get_ontvanger_attributes {
        my @attributes;

        my @ontvanger_fields = (
            @person_fields,
            {
                bwcompat      => '',
                name          => 'display_name',
                internal_name => 'display_name',
            },
        );
        for my $ontvanger_field (@ontvanger_fields) {
            push @attributes, Zaaksysteem::Backend::Object::Attribute->new(
                bwcompat_name            =>
                    $ontvanger_field->{bwcompat}
                        ? "ontvanger_$ontvanger_field->{bwcompat}"
                        : "ontvanger",
                name                     => "recipient.$ontvanger_field->{name}",
                attribute_type           => $ontvanger_field->{type} || 'text',
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    my $betrokkene = $zaak->ontvanger_object;
                    return ZAAKSYSTEEM_BETROKKENE_SUB->($zaak, $betrokkene, $ontvanger_field->{internal_name});
                }
            );
        }

        push @attributes, Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'recipient.id',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->ontvanger_object;
                return $zaak->ontvanger_object->betrokkene_identifier;
            },
        );

        return @attributes;
    }

    sub _get_zaaktype_attributes {
        my @attributes;

        my @properties = (
            {
                internal_name => 'aanleiding',
                name          => 'casetype.motivation',
            },
            {
                internal_name => 'archiefclassicatiecode',
                name          => 'casetype.archive_classification_code',
            },
            {
                internal_name => 'bag',
                name          => 'casetype.registration_bag',
            },
            {
                internal_name => 'beroep_mogelijk',
                bwcompat      => 'bezwaar_en_beroep_mogelijk',
                name          => 'casetype.objection_and_appeal',
            },
            {
                internal_name => 'doel',
                bwcompat      => 'doel',
                name          => 'casetype.goal',
            },
            {
                internal_name => 'e_formulier',
                name          => 'casetype.eform',
            },
            {
                internal_name => 'lex_silencio_positivo',
                name          => 'casetype.tactic_authorisation',
            },
            {
                internal_name => 'lokale_grondslag',
                name          => 'casetype.principle_national',
            },
            {
                internal_name => 'opschorten_mogelijk',
                name          => 'casetype.suspension',
            },
            {
                internal_name => 'publicatie',
                name          => 'casetype.publication',
            },
            {
                internal_name => 'publicatietekst',
                name          => 'casetype.text_for_publication',
            },
            {
                internal_name => 'verantwoordelijke',
                name          => 'casetype.supervisor',
            },
            {
                internal_name => 'verantwoordingsrelatie',
                name          => 'casetype.supervisor_relation',
            },
            {
                internal_name => 'verlenging_mogelijk',
                name          => 'casetype.extension',
            },
            {
                internal_name => 'verlengingstermijn',
                name          => 'casetype.extension_period',
            },
            {
                internal_name => 'verdagingstermijn',
                name          => 'casetype.adjourn_period',
            },
            {
                internal_name => 'vertrouwelijkheidsaanduiding',
                name          => 'casetype.designation_of_confidentiality',
            },
            {
                internal_name => 'wet_dwangsom',
                name          => 'casetype.penalty',
            },
            {
                internal_name => 'wkpb',
                name          => 'casetype.wkpb',
            },
        );

        for my $property (@properties) {
            push @attributes, Zaaksysteem::Backend::Object::Attribute->new(
                bwcompat_name            => $property->{bwcompat} || $property->{internal_name},
                name                     => $property->{name},
                attribute_type           => 'text',
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    return $zaak->zaaktype_property($property->{internal_name});
                }
            );
        }

        return @attributes;
    }

    sub _get_location_attributes {
        my @attributes;

        my %location_types = (
            locatie_zaak            => 'case_location',
            locatie_correspondentie => 'correspondence_location',
        );
        for my $location_type (keys %location_types) {
            for my $bag_type (qw(verblijfsobject openbareruimte nummeraanduiding pand standplaats ligplaats)) {
                my $bag_type_id = "${bag_type}_id";

                push @attributes, Zaaksysteem::Backend::Object::Attribute->new(
                    name                     => "case.$location_types{$location_type}.${bag_type}",
                    attribute_type           => 'bag',
                    is_systeemkenmerk        => 1,
                    systeemkenmerk_reference => sub {
                        my $zaak = shift;
                        return unless $zaak->$location_type;
                        return unless $zaak->$location_type->$bag_type_id;

                        my $rs = $zaak->result_source->schema->resultset('BagNummeraanduiding');
                        my $human_identifier = eval {
                            $rs->get_human_identifier_by_source_identifier(
                                $zaak->$location_type->$bag_type_id,
                                {prefix_with_city => 1}
                            ),
                        };
                        my $address_data = eval {
                            $rs->get_address_data_by_source_identifier(
                                $zaak->$location_type->$bag_type_id
                            ),
                        };

                        return {
                            bag_id => $zaak->$location_type->$bag_type_id,
                            human_identifier => $human_identifier,
                            address_data => $address_data,
                        };
                    },
                ),
            }
        }

        return @attributes;
    }
}

use constant ZAAKSYSTEEM_SYSTEM_ATTRIBUTES => sub {
    return [
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaaknummer',
            name                     => 'case.number',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->id;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaaktype',
            name                     => 'casetype.name',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->titel;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaak_fase',
            name                     => 'case.phase',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if ($zaak->volgende_fase) {
                    return $zaak->volgende_fase->fase;
                }
                return;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaak_mijlpaal',
            name                     => 'case.milestone',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if ($zaak->volgende_fase) {
                    return $zaak->huidige_fase->naam;
                }
                return;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'uname',
            name                     => 'system.uname',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my $zaaksysteem_location    = $INC{'Zaaksysteem.pm'};

                return 'Zaaksysteem (unknown)' unless($zaaksysteem_location);

                my @file_information        = stat($zaaksysteem_location);

                my @uname   = (
                    ZAAKSYSTEEM_NAAM,
                    Zaaksysteem->config->{'SVN_VERSION'},
                    ZAAKSYSTEEM_STARTDATUM,
                    ZAAKSYSTEEM_LEVERANCIER,
                    ZAAKSYSTEEM_LICENSE,
                    'zaaksysteem.nl',
                );

                return join(', ', @uname);
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaaktype_versie',
            name                     => 'casetype.version',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->version;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaaktype_versie_begindatum',
            name                     => 'casetype.version_date_of_creation',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->created;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaaktype_versie_einddatum',
            name                     => 'casetype.version_date_of_expiration',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->deleted;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'registratiedatum',
            name                     => 'case.date_of_registration',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'registratiedatum_volledig',
            name                     => 'case.date_of_registration_full',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            format                   => '%d-%m-%Y %H:%M:%S',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'afhandeldatum',
            name                     => 'case.date_of_completion',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->afhandeldatum;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'afdeling',
            name                     => 'casetype.department',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my $ou_object = $zaak->ou_object;

                return $ou_object->omschrijving
                    if $ou_object;
            }
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'afhandeldatum_volledig',
            name                     => 'case.date_of_completion_full',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            format                   => '%d-%m-%Y %H:%M:%S',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->afhandeldatum;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'uiterste_vernietigingsdatum',
            name                     => 'case.date_destruction',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->vernietigingsdatum;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'alle_relaties',
            name                     => 'case.relations',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my @relaties = ();
                if($zaak->get_column('pid')) {
                    push @relaties, $zaak->get_column('pid');
                }
                if ($zaak->zaak_children->count) {
                    my $children = $zaak->zaak_children->search;
                    while (my $child = $children->next) {
                        push @relaties, $child->id;
                    }
                }
                if($zaak->get_column('vervolg_van')) {
                    push @relaties, $zaak->get_column('vervolg_van');
                }
                if($zaak->get_column('relates_to')) {
                    push @relaties, $zaak->get_column('relates_to');
                }

                push @relaties, map { $_->case_id }
                    $zaak->result_source->schema->resultset('CaseRelation')->get_sorted($zaak->id);

                return join ", ", @relaties;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'archiefnominatie',
            name                     => 'case.type_of_archiving',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->archiefnominatie;
                }
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => ['zaak_bedrag', 'bedrag_web', 'pdc_tarief'],
            name                     => 'case.price',
            attribute_type           => 'integer', # XXX it's actually "numeric"
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->format_payment_amount();
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'betaalstatus',
            name                     => 'case.payment_status',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $ps = $zaak->payment_status;
                return unless $ps;

                return Zaaksysteem::Types::MappedString->new(
                    original => $ps,
                    mapped   => ZAAKSYSTEEM_CONSTANTS->{payment_statuses}->{$ps} || $ps,
                );
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.status',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->status();
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'statusnummer',
            name                     => 'case.number_status',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->milestone();
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'streefafhandeldatum',
            name                     => 'case.date_target',
            attribute_type           => 'timestamp_or_text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                
                # XXX 2 returntypes (timestamp en string)
                if ($zaak->status eq 'stalled') {
                    return 'Opgeschort';
                }
                return $zaak->streefafhandeldatum;
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'bewaartermijn',
            name                     => 'case.period_of_preservation',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    my $bewaartermijn = $zaaktype_resultaat->bewaartermijn;
                    return ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$bewaartermijn};
                }
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'contactkanaal',
            name                     => 'case.channel_of_contact',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->contactkanaal;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'pid',
            name                     => 'case.number_parent',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('pid');
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'vervolg_van',
            name                     => 'case.number_previous',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('vervolg_van');
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.progress_status',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->status_perc;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'zaaknummer_hoofdzaak',
            name                     => 'case.number_master',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->id unless $zaak->pid;

                my $parent = $zaak->pid;
                while ($parent->pid) {
                    $parent = $parent->pid;
                }

                return $parent->id;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'relates_to',
            name                     => 'case.number_relations',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('relates_to');
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'resultaat',
            name                     => 'case.result',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->resultaat;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'resultaat_omschrijving',
            name                     => 'case.result_description',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->label;
                }
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'resultaat_toelichting',
            name                     => 'case.result_explanation',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->comments;
                }
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'deelzaken_afgehandeld',
            name                     => 'case.relations_complete',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return $zaak->zaak_children->search({
                    'me.status' => { -not_in => ['resolved'] },
                })->count ? 'Nee' : 'Ja';
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'doorlooptijd_wettelijk',
            name                     => 'case.lead_time_legal',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->servicenorm;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'doorlooptijd_service',
            name                     => 'case.lead_time_service',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->afhandeltermijn;
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'trefwoorden',
            name                     => 'casetype.keywords',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_trefwoorden;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'trigger',
            name                     => 'casetype.initiator_source',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->trigger;
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'gebruiker_naam',
            name                     => 'user.name',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if ($zaak->result_source->resultset->current_user) {
                    return $zaak->result_source->resultset->current_user->naam;
                }
                return;
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'generieke_categorie',
            name                     => 'casetype.generic_category',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return $zaak->zaaktype_id->bibliotheek_categorie_id->naam
                    if $zaak->zaaktype_id->bibliotheek_categorie_id;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'handelingsinitiator',
            name                     => 'casetype.initiator_type',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->handelingsinitiator;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'casetype.id',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_id->id;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'identificatie',
            name                     => 'casetype.identification',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->code;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'omschrijving_of_toelichting',
            name                     => 'casetype.description',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_omschrijving;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'openbaarheid',
            name                     => 'casetype.publicity',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->openbaarheid;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'published',
            name                     => 'case.published',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->published;
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'opgeschort_tot',
            name                     => 'case.stalled_until',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->stalled_until;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'procesbeschrijving',
            name                     => 'casetype.process_description',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->procesbeschrijving;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'selectielijst',
            name                     => 'case.selection_list',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->selectielijst;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'wettelijke_grondslag',
            name                     => 'case.principle_local',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->grondslag;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'sjabloon_aanmaakdatum',
            name                     => 'case.date_current',
            attribute_type           => 'timestamp',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return DateTime->now();
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'vertrouwelijkheid',
            name                     => 'case.confidentiality',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $setting = $zaak->confidentiality;

                return Zaaksysteem::Types::MappedString->new(
                    original => $setting,
                    mapped   => ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$setting} || $setting,
                );
            },
        ),

        # "Behandelaar"
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'behandelaar',
            name                     => 'case.assignee',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->naam;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'behandelaar_afdeling',
            name                     => 'assignee.department',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;

                return $zaak->behandelaar_object->org_eenheid->naam
                    if (
                        $zaak->behandelaar_object->btype eq 'medewerker' &&
                        $zaak->behandelaar_object->org_eenheid
                    );

                return '';
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'behandelaar_email',
            name                     => 'assignee.email',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->email;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'behandelaar_tel',
            name                     => 'assignee.phone_number',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->telefoonnummer;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'assignee.id',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->ex_id;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'behandelaar_handtekening',
            name                     => 'assignee.signature',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my  $zaak   = shift;
                return unless $zaak->behandelaar_object;

                my $schema  = $zaak->result_source->schema;
                my $rs      = $schema->resultset('Subject');
                my $subject = $rs->find($zaak->behandelaar_object->id);

                my $current_id = $subject->settings->{signature_filestore_id};

                return '' unless $current_id;

                my $file = $schema->resultset('Filestore')->find($current_id);
                return $file->get_path;
            },
        ),

        # "Coordinator"
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'coordinator',
            name                     => 'case.coordinator',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->naam;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'coordinator_email',
            name                     => 'coordinator.email',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->email;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name            => 'coordinator_tel',
            name                     => 'coordinator.phone_number',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->telefoonnummer;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'coordinator.id',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->ex_id;
            },
        ),

        # Internal
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.route_ou',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->route_ou;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.route_role',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->route_role;
            },
        ),

        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.num_unaccepted_updates',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->unaccepted_pip_updates;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.num_unaccepted_files',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                # Force integerness
                return 0 + $zaak->files->search->unaccepted;
            },
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name                     => 'case.subject',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->onderwerp;
            },
        ),

        _get_aanvrager_attributes(),
        _get_ontvanger_attributes(),
        _get_zaaktype_attributes(),
        _get_location_attributes(),
    ];
};

1;

package Zaaksysteem::Auth::Bedrijfid;

use Moose;
use Zaaksysteem::Profile;


has [qw/authdbic log config session params c/] => (
    is      => 'rw',
);

has [qw/succes cancel error login success_endpoint/] => (
    is      => 'rw'
);

has 'prod' => (
    'is'        => 'rw',
);

sub BUILD {
    my $self = shift;

    ### Check for session
    if (exists($self->session->{_bedrijfid})) {
        $self->_load_session;
    }
}

=head2 authenticate(%opts)

See if there's a matching record in the bedrijf_authenticatie
table, manipulate the catalyst session, return given url to redirect
to. A number of things are stored in the session for later retrieval
through this module.

=cut

define_profile authenticate => (
    required => [qw/login password success_endpoint/]
);
sub authenticate {
    my $self = shift;
    my %opts = %{ assert_profile({@_})->valid };

    ### Remove digid session
    delete($self->session->{_bedrijfid});

    return unless (
        (
            $opts{login} &&
            $opts{login} =~ /^\d+$/
        ) &&
        $opts{password}
    );

    # Search
    my $users = $self->authdbic->search(
        {
            login       => $opts{login},
            password    => $opts{password}
        }
    );

    if ($users->count) {
        my $msg = 'Bedrijfid: authenticatie request geslaagd';
        $self->log->info($msg);
    } else {
        my $msg =
            'Bedrijfid: authenticatie request mislukt:'
            . ' Geen gebruiker gevonden met deze login en password';
        $self->log->error($msg);
        $self->error( $msg );

        return;
    }

    ### Succes
    $self->session->{_bedrijfid} = {};
    $self->session->{authenticated_by} = 'bedrijfid';

    ### Expire session
    #$self->c->session_expire_key('_bedrijfid' => '900');
    $self->session->{_bedrijfid}->{login} = $opts{login};
    $self->session->{_bedrijfid}->{success_endpoint} = $opts{success_endpoint};

    $self->_load_session;

    return $opts{success_endpoint};
}

sub logout {
    my ($self) = @_;

    ### Destroy session
    delete($self->session->{_bedrijfid});

    $self->$_(undef) for qw/
        succes
        success_endpoint
        login
    /;
}

sub _load_session {
    my ($self) = @_;

    return unless exists($self->session->{_bedrijfid}->{login});

    $self->succes(1);
    $self->success_endpoint(
        $self->session->{_bedrijfid}->{success_endpoint}
    );
    $self->login($self->session->{_bedrijfid}->{login});
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Auth::Store;

use Moose;

use Zaaksysteem::Auth::User;

use JSON;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use Data::Dumper;

has config => (is => 'ro');
has app => (is => 'ro');
has realm => (is => 'ro');

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(
        config => shift,
        app => shift,
        realm => shift
    );
};

define_profile find_user => (
    required => [qw[username source]],
);

sub find_user {
    my $self = shift;
    my $authinfo = assert_profile(shift)->valid;

    my $entity = shift->model('DB::UserEntity')->find({
        source_identifier   => $authinfo->{ username },
        source_interface_id => $authinfo->{ source }
    });

    return unless $entity;

    my $subject      = $entity->subject_id;
    $subject->login_entity($entity);

    $subject->result_source->schema->default_resultset_attributes->{ current_user }
        = $subject->result_source->schema->current_user($subject);

    return $subject;
}

sub for_session {
    my $self    = shift;
    my $c       = shift;
    my $user    = shift;

    return to_json($user->TO_JSON);
}

sub from_session {
    my $self                        = shift;
    my $c                           = shift;

    my $session_auth_params         = from_json(shift);

    my $entity                      = $c->model('DB::UserEntity')->find(
                                        $session_auth_params->{ login_entity_id }
                                    );

    my $subject                      = $c->model('DB::Subject')->find(
                                        $session_auth_params->{ id }
                                    );


    $subject->login_entity($entity);

    $subject->result_source->schema->default_resultset_attributes->{ current_user }
        = $subject->result_source->schema->current_user($subject);

    my $user = $subject;

    return $user;
}

1;

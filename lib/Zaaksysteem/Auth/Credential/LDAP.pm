package Zaaksysteem::Auth::Credential::LDAP;

use Moose;

use Net::LDAP;
use Net::LDAP::Constant qw[LDAP_INVALID_CREDENTIALS];

use Zaaksysteem::Auth::User;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;
use Zaaksysteem::Constants;

use Data::Dumper;

has _config => ( is => 'ro' );
has app => ( is => 'ro' );
has realm => ( is => 'ro' );
has interface => ( is => 'rw', isa => 'Zaaksysteem::Model::DB::Interface' );
has ldap => ( is => 'rw', isa => 'Maybe[Net::LDAP]' );

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig(
        _config => shift,
        app => shift,
        realm => shift
    );
};

around interface => sub {
    my $orig = shift;
    my $self = shift;
    my $interface = $_[0];

    my $retval = $self->$orig(@_);

    unless($interface) {
        return $retval;
    }

    my $config = $interface->result_source->schema->default_resultset_attributes->{config}->{LDAP};

    assert_profile($config, profile => {
        required => [qw[hostname port userfilter]],
        constraint_methods => {
            port => qr/^\d+$/,
            userfilter => qr[%s]
        }
    });

    my $ldap = Net::LDAP->new($config->{ hostname },
        port => $config->{ port }
    );

    unless($ldap) {
        throw('auth/ldap/configuration', sprintf(
            'Unable to connect to LDAP server at %s:%s',
            $config->{ hostname },
            $config->{ port }
        ));
    }

    $self->ldap($ldap);

    return $retval;
};

sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $ldap = $self->ldap;

    my $user = $realm->find_user(
        {
            username => $authinfo->{ username },
            source => $self->interface->id
        },
        $c
    );

    my $ldap_user = $self->search_user_dn($authinfo->{ username }) or return;

    my $bind_msg = $ldap->bind($ldap_user->dn,
        password => $authinfo->{ password }
    );

    if($bind_msg->is_error) {
        return if($bind_msg->code eq LDAP_INVALID_CREDENTIALS);

        throw('auth/ldap/bind', sprintf(
            'LDAP error while binding with user credentials for "%s" on %s:%s: %s',
            $authinfo->{ username },
            $self->config(qw[hostname port]),
            $bind_msg->error
        ));
    }

    if ($user) {
        ### User authenticated, make sure data in Subject is current
        $user->update_properties_from_ldap(
            $ldap_user,
            {
                interface_id        => $self->interface->id,
            }
        );

        ### User inactive? Return
        if (
            !$user->is_active
        ) {
            warn('User marked as deleted');
            return;
        }
    } else {
        if($self->config('autocreate_user') || 1) {
            $user         = $c->model('DB::Subject')->create_from_ldap(
                {
                    interface_id        => $self->interface->id,
                    ldap_user           => $ldap_user
                }
            );
        } else {
            $c->log->debug(sprintf(
                'No user found for credential "%s" and autocreate is disabled',
                $authinfo->{ username }
            ));

            return;
        }

        # my $event = $c->model('DB::Logging')->trigger('auth/login', {
        #     component   => 'auth',
        #     created_by  => 
        #     data => {
        #         subject_id      => $user->id,
        #         #transaction_id => $transaction->id,
        #     }
        # });       
    }


    # Not sure what to do with transactions yet, this is my best guess...
    # Michiel! Help!

    # my $transaction = $c->model('DB::Transaction')->transaction_create({
    #     interface_id => $self->interface->id,
    #     direction => 'incoming',
    #     input_data => $ldap_user->ldif,
    #     external_transaction_id => sprintf(
    #         'ldap_sync-%s-%s',
    #         $ldap_user->get_value('uidNumber'),
    #         DateTime->now->iso8601
    #     )
    # });

    return $user;
}

sub config {
    my $self = shift;

    my $interface = $self->interface;

    return unless $interface;

    my $config = $interface->get_interface_config || {};

    my @values = map { $config->{ $_ } } (scalar(@_) ? @_ : keys %{ $config });

    return wantarray ? @values : pop @values;
}

sub search_user_dn {
    my $self        = shift;
    my $username    = shift;

    my $catconfig   = $self
                    ->interface
                    ->result_source
                    ->schema
                    ->default_resultset_attributes
                    ->{config};

    my $userfilter  = $catconfig->{LDAP}->{userfilter};
    my $userbasedn  = $self
                    ->interface
                    ->result_source
                    ->schema
                    ->default_resultset_attributes
                    ->{ldap}
                    ->base_dn;

    throw(
        'auth/ldap/missing_required_config',
        'Missing required config userfilter or user_base_dn'
    ) unless ($userfilter && $userbasedn);

    my $filter      = sprintf(
        $userfilter,
        $username
    );

    my $ldap        = $self->ldap;

    my $bind_msg = $ldap->bind;

    if($bind_msg->code) {
        throw('auth/ldap/search_user_dn', sprintf(
            "Error while anonymously bind to LDAP server at %s:%s: %s",
            $self->config(qw[hostname port]),
            $bind_msg->error
        ));
    }

    my $search = $ldap->search(
        base    => $userbasedn,
        filter  => $filter
    );

    ### Error code, but not code "not found"
    if($search->code && $search->code != 32) {
        throw('auth/ldap/search_user_dn', $search->error . ':' . $filter . ':' . $search->code);
    }

    unless($search->count) {
        # User not found
        return;
    }

    if($search->count > 1) {
        throw('auth/ldap/search_user_dn', sprintf(
            'Multiple entries found in LDAP for user "%s": %s',
            $username,
            join(', ', map { sprintf '"%s"', $_ } map { $_->dn } $search->entries)
        ));
    }

    return $search->pop_entry;
}

1;


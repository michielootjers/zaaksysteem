package Zaaksysteem::Auth::User;

use Moose;

use Zaaksysteem::Exception;

BEGIN { extends 'Catalyst::Authentication::User' }

has person => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::Person',
    required => 1
);

has entity => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::UserEntity',
    required => 1
);

sub supported_features {
    return {
        session => 1
    };
}

sub get_object {
    return shift->person;
}

sub uidnumber {
    return shift->entity->source_identifier;
}

1;

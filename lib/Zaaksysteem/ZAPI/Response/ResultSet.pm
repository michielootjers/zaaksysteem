package Zaaksysteem::ZAPI::Response::ResultSet;

use Moose::Role;

sub from_resultset {
    my $self        = shift;
    my $resultset   = shift;

    ### Preperation
    die('Not a valid ResultSet: ' . ref($resultset))
        unless UNIVERSAL::isa($resultset, 'DBIx::Class::ResultSet');

    $self->_input_type('resultset');

    my $resultset_options = {};


    ### Ordering
    ### TODO: Create features in resultset (no_ordering)
    if ($self->order_by && $resultset->result_source->name ne 'object_data') {
        $self->order_by_direction('desc') unless $self->order_by_direction;

        $resultset_options->{order_by} = {
            '-' . $self->order_by_direction => $self->order_by,
        };
    }

    ### Paging
    $resultset_options->{page} = $self->page_current if $self->page_current;
    $resultset_options->{rows} = $self->page_size if $self->page_size;


    ### Generatin
    $resultset      = $resultset->search(
        {},
        $resultset_options
    );

    if ($self->no_pager) {
        my $count   = $resultset->pager->total_entries;
        $resultset  = $resultset->search(
            {},
            {
                rows    => $count,
            }
        );
    }

    $self->_generate_paging_attributes(
        $resultset->pager,
    );

    $self->result([ $resultset->all ]);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'DBIx::Class::ResultSet')
    ) {
        $self->from_resultset(@_);
    }

    $self->$orig( @_ );
};

1;

package Zaaksysteem::ZAPI::Response::Array;

use Data::Page;
use Moose::Role;

has 'pager' => (
    is      => 'rw',
);

sub from_array {
    my $self        = shift;
    my $array       = shift;

    die('Not a valid ARRAY: ' . ref($array))
        unless UNIVERSAL::isa($array, 'ARRAY');

    $self->_input_type('array');

    my $pager       = Data::Page->new();

    $pager->total_entries   (scalar(@{ $array }));

    if ($self->no_pager) {
        $pager->entries_per_page(scalar(@{ $array }) || 1);
    } else {
        $pager->entries_per_page($self->page_size);
    }

    $pager->current_page    ($self->page_current);

    $self->_generate_paging_attributes(
        $self->pager($pager),
    );

    $self->result([ $pager->splice($array) ]);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'ARRAY')
    ) {
        $self->from_array(@_);
    }

    $self->$orig( @_ );
};

1;

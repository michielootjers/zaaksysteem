package Zaaksysteem::ZAPI::Form::Action;

use Moose;

use Moose::Util::TypeConstraints qw[enum];

with 'Zaaksysteem::JSON::SerializerRole';

=head1 NAME

Zaaksysteem::ZAPI::Form::Action - Represent actions the frontend can enact on a form

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 name

=cut

has name => (
    traits => [qw[OA]],

    is => 'rw',
    isa => 'Str',
    required => 1
);

has type => (
    traits => [qw[OA]],

    is => 'rw',
    isa => enum([qw[submit reset popup]]),
    required => 1,
    default => 'submit'
);

has label => (
    traits => [qw[OA]],

    is => 'rw',
    isa => 'Str',
    lazy => 1,
    default => sub { ucfirst(shift->name) }
);

has importance => (
    traits => [qw[OA]],

    is => 'rw',
    isa => enum([qw[primary secondary tertiary]]),
    default => 'primary'
);

has when => (
    traits => [qw[OA]],

    is => 'rw',
    isa => 'Str'
);

has data => (
    traits => [qw[OA]],

    is => 'rw',
    isa => 'HashRef'
);

=head1 METHODS

=head2 TO_JSON

Return the JSON representation of the form action.

=cut

sub TO_JSON {
    my $self = shift;

    return $self->serialize;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


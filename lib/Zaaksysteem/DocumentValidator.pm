package Zaaksysteem::DocumentValidator;

#
# validate documents before they can be added to Zaaksysteem.
#
# In het kader van de nen2082 moet er een beperking worden ingevoerd 
# voor het aantal bestandsformaten. Uitgangspunt voor de formaten die 
# wel worden toegestaan is dat ze door ImageMagick of OpenOffice 
# geconverteerd kunnen worden. De formaten die worden geaccepteerd zijn:
# Office:
#   .rtf 
#   .doc 
#   .dot 
#   .docx 
#   .xls 
#   .xlsx 
#   .ppt 
#   .pptx 
#   .odt 
#   .ods 
#   .odp 
#   .pdf 
#
#   Afbeeldingen: 
#   .bmp 
#   .gif 
#   .jpeg 
#   .jpg 
#   .png 
#   .tiff 
#   .tif 
#
#   Overige: 
#   .htm 
#   .html 
#   .xml 
#

use strict;
use warnings;

use Data::Dumper;
use Params::Profile;
use Moose;
use File::Basename;
use File::MMagic;
use HTTP::Request::Common;
use LWP::UserAgent;
use Zaaksysteem::Constants qw/MIMETYPES_ALLOWED/;



Params::Profile->register_profile(
    method  => 'validate_path',
    profile => {
        required => [ qw/filepath/ ]
    }
);

sub validate_path {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for validate_path" unless $dv->success;

    my $filepath = $params->{filepath};

    my($filename, $directories, $suffix) = fileparse($filepath, qr/\.[^.]*/);
    my $suffix_lc = lc $suffix;

    my $mm       = new File::MMagic;
    my $mimetype = $mm->checktype_filename($filepath);

    my $determined_mimetype = $self->_mimetype_allowed({
            suffix_lc       => $suffix_lc,
            mimetype        => $mimetype,
            upload_mimetype => $mimetype,
    });

    return $suffix_lc && $mimetype && $determined_mimetype;
}


Params::Profile->register_profile(
    method  => 'validate_upload',
    profile => {
        required => [ qw/upload filename/ ]
    }
);

sub validate_upload {
	my ($self, $params) = @_;


    my $dv = Params::Profile->check(params => $params);
    die "invalid options for validate_upload" unless $dv->success;

    my $filename    = $params->{filename};
    my $upload      = $params->{upload};

    my $temp_filepath = $upload->tempname;
    my $mm      = new File::MMagic;

    my $fh = new FileHandle "< $temp_filepath" or die "could not read from $temp_filepath";
    my $mimetype = $mm->checktype_filehandle($fh);
    my $mimetype_2 = $mm->checktype_filename($temp_filepath);
  
    warn "Checking document: [$filename], mimetype: [$mimetype], mimetype filename: [$mimetype_2]";

    my($filename_parsed, $directories, $suffix) = fileparse($filename, qr/\.[^.]*/);
    my $suffix_lc = lc $suffix;

    warn "suffix: $suffix_lc, mimetype: $mimetype";

    my $determined_mimetype = $self->_mimetype_allowed({
        suffix_lc => $suffix_lc,
        mimetype  => $mimetype,
        upload_mimetype => $upload->type,
    });

    unless($suffix_lc && $mimetype && $determined_mimetype) {
        warn "invalid suffix or mimetype";
        return undef;
    }


    return $determined_mimetype;
}


Params::Profile->register_profile(
    method  => '_mimetype_allowed',
    profile => {
        required => [ qw/suffix_lc mimetype upload_mimetype/ ]
    }
);

sub _mimetype_allowed {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _mimetype_allowed" unless $dv->success;

    my $suffix_lc = $params->{suffix_lc};
    my $mimetype = $params->{mimetype};
    my $upload_mimetype = $params->{upload_mimetype};

    my $mimetypes_allowed = MIMETYPES_ALLOWED->{$suffix_lc};
    
    my %mimetypes = ($mimetypes_allowed->{mimetype} => 1);
    if(my $alternate_mimetypes = $mimetypes_allowed->{alternate_mimetypes}) {
        foreach my $mimetype (@$alternate_mimetypes) {
            $mimetypes{$mimetype} = 1;
        }
    }
       
    if (exists $mimetypes{$mimetype} || exists $mimetypes{$upload_mimetype}) {
        return $mimetypes_allowed->{mimetype};
    }
    return undef;
        
}


Params::Profile->register_profile(
    method  => '_check_pdf_conversion_possible',
    profile => {
        required => [ qw/filecontent mimetype/ ]
    }
);
sub _check_pdf_conversion_possible {
    my ($self, $params) = @_;
 
    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _check_pdf_conversion_possible" unless $dv->success;

    my $mimetype    = $params->{mimetype};
    my $filecontent = $params->{filecontent};

    my $ua = LWP::UserAgent->new;
    my $result = $ua->request(POST 'http://localhost:8080/converter/service', 
        Content => $filecontent,
        Content_Type => $mimetype,
        Accept => 'application/pdf',
    );
    
    return $result->code eq 200;

}

1;

__END__

#    open FILE, $tempfile or die "temp file not readable, problemo";
#    my $filecontent = join "", <FILE>;
#    close FILE;

#    unless($self->_check_pdf_conversion_possible({
#        filecontent => $filecontent, 
#        mimetype    => $mimetype
#    })) {
#        warn "jodconvertor could not work this file, rejected.";
#        return undef;
#    }




=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


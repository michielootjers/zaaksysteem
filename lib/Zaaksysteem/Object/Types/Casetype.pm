package Zaaksysteem::Object::Types::Casetype;

use Moose;
use namespace::autoclean;

use Moose::Meta::Attribute;

use List::Util qw[first];

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[Boolean];

extends 'Zaaksysteem::Object';

with qw[
    Zaaksysteem::Object::Roles::Security
    Zaaksysteem::Object::Roles::MetaType
];

=head1 NAME

Zaaksysteem::Object::Types::Casetype - Shim for "zaaktype"

=head1 DESCRIPTION

Automatically created (and kept in sync by) writing a Zaaktype entry.

=head1 ATTRIBUTES

=head2 casetype_id

The old-style (internal) id of the casetype row used to build th casetype
object.

=cut

has casetype_id => (
    is => 'rw',
    isa => 'Int',
    label => 'Zaaktype ID',
    traits => [qw[OA]],
    required => 1,
);

=head2 offline

Boolean value, indicating whether the casetype is currently "offline" or not.

If the case type is offline, it shouldn't be visible in most places.

=cut

has offline => (
    is => 'rw',
    isa => Boolean,
    label => 'Offline',
    traits => [qw[OA]],
    required => 0,
    default => 0,
);

=head2 casetype_node_id

The old-style (internal) id of the casetype node row used to build the
casetype object.

=cut

has casetype_node_id => (
    is => 'rw',
    isa => 'Int',
    label => 'Zaaktypenode ID',
    traits => [qw[OA]],
    required => 1,
);

=head2 version

Sequential integer number indicating the version of the 

=cut

has version => (
    is => 'rw',
    isa => 'Int',
    label => 'Versie',
    traits => [qw[OA]],
);

=head2 legal_basis

=cut

has legal_basis => (
    is => 'rw',
    isa => 'Str',
    label => 'Wettelijke grondslag',
    traits => [qw[OA]],
);

=head2 local_basis

=cut

has local_basis => (
    is => 'rw',
    isa => 'Str',
    label => 'Lokale grondslag',
    traits => [qw[OA]],
);

=head2 adjournment_period

=cut

has adjournment_period => (
    is => 'rw',
    isa => 'Int',
    label => 'Verdagingstermijn',
    traits => [qw[OA]],
);

=head2 process_description

=cut

has process_description => (
    is => 'rw',
    isa => 'Str',
    label => 'Procesbeschrijving',
    traits => [qw[OA]],
);

=head2 eform

=cut

has eform => (
    is => 'rw',
    isa => 'Str',
    label => 'e-Formulier',
    traits => [qw[OA]],
);

=head2 penalty

=cut

has penalty => (
    is => 'rw',
    isa => 'Str',
    label => 'Wet dwangsom',
    traits => [qw[OA]],
);

=head2 appealable

=cut

has appealable => (
    is => 'rw',
    isa => Boolean,
    label => 'Beroep mogelijk',
    coerce => 1,
    traits => [qw[OA]],
);

=head2 aggregation_scope

String identifier for the level on which metadata of this casetype applies to
inheriting object types (cases, documents).

Has a default value of 'Serie'

=cut

has aggregation_scope => (
    is => 'rw',
    isa => 'Str',
    label => 'Aggregatieniveau',
    traits => [qw[OA]],
    default => 'Serie'
);

=head2 lex_silencio_positivo

Boolean field that indicates whether a request is automatically approved if
the L</lead_time_legal> has passed.

=cut

has lex_silencio_positivo => (
    is => 'rw',
    isa => Boolean,
    label => 'Lex silencio positivo',
    coerce => 1,
    traits => [qw[OA]],
);

=head2 public_url_path

Contains a map of URLs that can be used for public webform deeplinking

=cut

has public_url_path => (
    is => 'rw',
    isa => 'HashRef[Str]',
    label => 'Publieke URLs',
    required => 0,
    traits => [qw[OA]],
);

=head2 wkpb

=cut

has wkpb => (
    is => 'rw',
    isa => Boolean,
    label => 'WKPB',
    coerce => 1,
    traits => [qw[OA]],
);

=head2 trigger

=cut

has trigger => (
    is => 'rw',
    isa => 'Str',
    label => 'Trigger',
    traits => [qw[OA]]
);

=head2 subject_types

=cut

has subject_types => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    label => 'Betrokkenetypen',
    traits => [qw[OA]]
);

=head2 sources

=cut

has sources => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    label => 'Contactkanalen',
    traits => [qw[OA]]
);

=head2 preset_client

=cut

has preset_client => (
    is => 'rw',
    type => 'subject',
    label => 'Vooringevulde aanvrager',
    traits => [qw[OR]],
    embed => 1,
    predicate => 'has_preset_client',
);

=head2 lock_registration_phase

=cut

has lock_registration_phase => (
    is => 'rw',
    isa => Boolean,
    coerce => 1,
    label => 'Vergrendel registratiefase',
    default => 0,
    traits => [qw[OA]]
);


=head2 queue_coworker_changes

Whether we allow updates from colleagues in a case.

=cut

has queue_coworker_changes => (
    is => 'rw',
    isa => Boolean,
    coerce => 1,
    label => 'Wijzigingen van collega\'s in wachtrij plaatsen',
    default => 0,
    traits => [qw[OA]]
);

=head2 instance_phases

Holds an array of phase definitions.

=cut

has instance_phases => (
    is => 'rw',
    type => 'casetype_phase',
    embed => 1,
    traits => [qw[OR]],
    isa_set => 1,
    default => sub { [] },
);

=head2 instance_results

Holds a collection of results available to case instances.

=cut

has instance_results => (
    is => 'rw',
    type => 'casetype_result',
    embed => 1,
    traits => [qw[OR]],
    isa_set => 1,
    default => sub { [] },
);

=head1 ATTRIBUTES (ImZTC 2.1)

The following attributes are specified by
L<GEMMA Online|http://gemmaonline.nl/index.php/Informatiemodel_Zaaktypen_%28ImZTC%29>.

=head2 name

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.formulier.naam>

=cut

has name => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Titel',
    required => 1,
    traits   => [qw(OA)],
);

=head2 identification

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.zaaktype-identificatie>

=cut

has identification => (
    is => 'rw',
    isa => 'Str',
    label => 'Identificatie',
    traits => [qw[OA]],
);

=head2 keywords

L<gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.trefwoord>

=cut

has keywords => (
    is => 'rw',
    isa => 'Str',
    label => 'Trefwoord',
    traits => [qw[OA]],
);

=head2 description

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.zaaktype-omschrijving>

=cut

has description => (
    is => 'rw',
    isa => 'Str',
    label => 'Omschrijving of toelichting',
    traits => [qw[OA]],
);

=head2 initiator_action

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.handeling_initiator>

=cut

has initiator_action => (
    is => 'rw',
    isa => 'Str',
    label => 'Handeling initiator',
    traits => [qw[OA]],
);

=head2 lead_time_service

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.servicenorm_behandeling>

=cut

has lead_time_service => (
    is => 'rw',
    isa => 'HashRef',
    label => 'Doorlooptijd service',
    traits => [qw[OA]],
);

=head2 lead_time_legal

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.doorlooptijd_behandeling>

=cut

has lead_time_legal => (
    is => 'rw',
    isa => 'HashRef',
    label => 'Doorlooptijd wettelijk',
    traits => [qw[OA]],
);

=head2 selection_list

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/resultaattype.selectielijstklasse>

=cut

has selection_list => (
    is => 'rw',
    isa => 'Str',
    label => 'Selectielijst',
    traits => [qw[OA]],
);

=head2 extendable

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.verlenging_mogelijk>

=cut

has extendable => (
    is => 'rw',
    isa => Boolean,
    label => 'Verlenging mogelijk',
    coerce => 1,
    traits => [qw[OA]],
);

=head2 extension_period

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.verlengingstermijn>

=cut

has extension_period => (
    is => 'rw',
    isa => 'Int',
    label => 'Verlengingstermijn',
    traits => [qw[OA]],
);

=head2 supervisor_relation

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.verantwoordingsrelatie>

=cut

has supervisor_relation => (
    is => 'rw',
    isa => 'Str',
    label => 'Verantwoordingsrelatie',
    traits => [qw[OA]],
);

=head2 publication_text

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.publicatietekst>

=cut

has publication_text => (
    is => 'rw',
    isa => 'Str',
    label => 'Publicatietekst',
    traits => [qw[OA]],
);

=head2 suspendable

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.opschorting_en_aanhouding_mogelijk>

=cut

has suspendable => (
    is => 'rw',
    isa => Boolean,
    label => 'Opschorting mogelijk',
    coerce => 1,
    traits => [qw[OA]],
);

=head2 publishable

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.publicatie-indicatie>

=cut

has publishable => (
    is => 'rw',
    isa => Boolean,
    label => 'Publicatie-indicatie',
    coerce => 1,
    traits => [qw[OA]],
);

=head2 confidentiality

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.vertrouwelijkheidaanduiding>

=cut

has confidentiality => (
    is => 'rw',
    isa => 'Str',
    label => 'Vertrouwelijkheidaanduiding',
    traits => [qw[OA]],
);

=head2 purpose

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.doel>

=cut

has purpose => (
    is => 'rw',
    isa => 'Str',
    label => 'Doel',
    traits => [qw[OA]],
);

=head2 records_classification

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.archiefclassificatiecode>

=cut

has records_classification => (
    is => 'rw',
    isa => 'Str',
    label => 'Archiefclassificatiecode',
    traits => [qw[OA]],
);

=head2 supervisor

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.verantwoordelijke>

=cut

has supervisor => (
    is => 'rw',
    isa => 'Str',
    label => 'Verantwoordelijke',
    traits => [qw[OA]],
);

=head2 motivation

L<http://gemmaonline.nl/index.php/Imztc_2.1/doc/attribuutsoort/zaaktype.aanleiding>

=cut

has motivation => (
    is => 'rw',
    isa => 'Str',
    label => 'Aanleiding',
    traits => [qw[OA]],
);

=head2 public_registration_form

Boolean field. Indicates whether this case type is requestable using a public
form.

=cut

has public_registration_form => (
    is => 'rw',
    isa => Boolean,
    label => 'Aanvragen met publiek webformulier',
    traits => [qw[OA]],
);

=head2 online_payment

Boolean field. Indicates whether cases of this type require online payment.

=cut

has online_payment => (
    is => 'rw',
    isa => Boolean,
    label => 'Internetkassa gebruiken',
    traits => [qw[OA]],
);


=head2 take_on_create

Boolean field. Indicates that the option to immediately take (assign to self) a case on creation.

=cut

has allow_take_on_create => (
    is => 'rw',
    isa => Boolean,
    label => 'Zaak automatisch in behandeling nemen',
    traits => [qw[OA]],
);

=head2 assign_on_create

Boolean field. Indicates that the option to assign a case on creation.

=cut

has allow_assign_on_create => (
    is => 'rw',
    isa => Boolean,
    label => 'Toewijzing tonen bij zaakintake',
    traits => [qw[OA]],
);


=head2 allow_subjects_on_create

Boolean field. If true, give user the ability to link other people (or
companies) to the case on create.

=cut

has allow_subjects_on_create => (
    is => 'rw',
    isa => Boolean,
    label => 'Toon vertrouwelijkheid',
    traits => [qw[OA]],
);
=head2 show_confidentiality_on_create

Boolean field. If true, show a "confidentiality" selector on the case creation page.

=cut

has show_confidentiality_on_create => (
    is => 'rw',
    isa => Boolean,
    label => 'Toon vertrouwelijkheid',
    traits => [qw[OA]],
);

=head2 intake_show_contact_info

Boolean field. If true, contact data should be shown in the case intake.

=cut

has intake_show_contact_info => (
    is => 'rw',
    isa => Boolean,
    label => 'Contactgegevens tonen bij intake',
    traits => [qw[OA]],
);

=head2 contact_info_email_required

=head2 contact_info_phone_required

=head2 contact_info_mobile_phone_required

Boolean fields, indicating whether these contact details are required.

=cut

has contact_info_email_required => (
    is => 'rw',
    isa => Boolean,
    label => 'E-mailadres verplicht bij webformulier',
    traits => [qw[OA]],
);

has contact_info_phone_required => (
    is => 'rw',
    isa => Boolean,
    label => 'Telefoonnummer verplicht bij webformulier',
    traits => [qw[OA]],
);

has contact_info_mobile_phone_required => (
    is => 'rw',
    isa => Boolean,
    label => 'Mobiel telefoonnummer verplicht bij webformulier',
    traits => [qw[OA]],
);

=head2 visible_on_pip

Boolean field. Cases with this flag set to true will be shown on the requestor's PIP.

=cut

has visible_on_pip => (
    is => 'rw',
    isa => Boolean,
    label => 'Zichtbaar op PIP',
    traits => [qw[OA]],
);

=head1 METHODS

=head2 instance_type

Implements interface required by L<Zaaksysteem::Object::Roles::MetaType>.

=cut

sub instance_type {
    return 'case';
}

=head2 instance_superclass

Implements interface required by L<Zaaksysteem::Object::Roles::MetaType>.

=cut

sub instance_superclass {
    return 'Zaaksysteem::Object::Types::Case';
}

=head2 instance_roles

Implements interface required by L<Zaaksysteem::Object::Roles::MetaType>.

=cut

sub instance_roles {
    return 'Zaaksysteem::Object::Roles::Type';
}

=head2 instance_attributes

Implements interface required by L<Zaaksysteem::Object::Roles::MetaType>.

=cut

sub instance_attributes {
    my $self = shift;

    return map {
        @{ $_->{ attribute_definitions } }
    } $self->all_instance_phases;
}

=head2 instance_relations

Implements interface required by L<Zaaksysteem::Object::Roles::MetaType>.

=cut

sub instance_relations {
}

=head2 TO_STRING

Stringification of the  Uses the "name" attribute.

=cut

override TO_STRING => sub {
    my $self = shift;

    return $self->name;
};

=head2 ordered_phases

Returns ordered instance_phases

=cut

sub ordered_phases  {
    my $self = shift;
    return [sort { $a->sequence cmp $b->sequence }
            @{ $self->instance_phases }];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

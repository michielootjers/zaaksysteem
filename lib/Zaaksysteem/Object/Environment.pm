package Zaaksysteem::Object::Environment;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Object::Constants qw[DEFAULT_ENVIRONMENT_VARIABLES];

=head1 NAME

Zaaksysteem::Object::Environment - Data-wrapper class for object processes

=head1 DESCRIPTION

This class implements a lisp-like environment instance, used by object
processes for argument passing.

=head1 ATTRIBUTES

=head2 parent

May hold a reference to a parent environment. If no parent environment is
provided the instance will be the implied root for whatever process uses it.

Provides C<has_parent>.

=cut

has parent => (
    is => 'ro',
    isa => 'Zaaksysteem::Object::Environment',
    required => 0,
    predicate => 'has_parent'
);

=head2 variables

Holds a HashRef containing the variable-value bindings for the environment
instance. Defaults to an empty hash.

Provides C<_get>, C<_set>, C<_exists>.

=cut

has variables => (
    is => 'ro',
    isa => 'HashRef',
    traits => [qw[Hash]],
    required => 1,
    default => sub { return DEFAULT_ENVIRONMENT_VARIABLES },
    handles => {
        _get => 'get',
        _set => 'set',
        _exists => 'exists'
    }
);

=head1 METHODS

=head2 get

Retrieves a value by it's variable name. If no local binding exists, and the
instance is not a root environment (!has_parent), will call C<get> on the
parent.

    my $value = $env->get('var');

=cut

sub get {
    my ($self, $var) = @_;

    return $self->_get($var) if $self->_exists($var);

    return $self->parent->get($var) if $self->has_parent;

    return;
}

=head2 set

Sets (bind) a value to a variable name. If the variable name is already in
local use an exception will be thrown with complaints about immutability.

The caller is allowed to mask variables bound in parent environments by
calling C<set> with a variable name that already exists in a parent
environment.

    $env->set('var', 'val');

=cut

sub set {
    my ($self, $var, $val) = @_;

    if ($self->_exists($var)) {
        throw(
            'object/env/variables_are_immutable',
            'Variables are immutable and cannot be re-set'
        );
    }

    $self->_set($var, $val);

    return $self;
}

=head2 exists

Checks that a variable name is bound to a value, deferring to the parent
environment if one is set.

    if($env->exists('var')) { ... }

=cut

sub exists {
    my ($self, $var) = @_;

    return 1 if $self->_exists($var);

    return $self->parent->exists($var) if $self->has_parent;

    return;
}

=head2 new_child

Instantiates a new child environment. Additional arguments to this method will
be passed to the environment constructor as a hashref for the C<variables>
attribute.

    my $env = ...;

    my $child = $env->new_child(a => 1, b => 2);

=cut

sub new_child {
    my ($self, %params) = @_;

    # Run it via the meta so if this class gets subclassed new_child returns
    # instances of the subclassed meta.
    return $self->meta->new_object(
        parent => $self,
        variables => \%params
    );
}

=head2 process

Convenience method for retrieving and evaluating a process.

    # This calling convention will re-use $env as the environment in which
    # the process is evaluated. If the subprocess mucks up the env it's on you
    my $retval = $env->process('my_proc');

    my $retval = $env->process('my_proc', { local => 'vars' });

    my $retval = $env->process('my_proc', $env->new_child(...));

=cut

sub process {
    my $self = shift;
    my $name = shift;
    my $target_env = shift || $self;

    if (ref $target_env eq 'HASH') {
        # Or should we unwrap the hash so we have scope-stability of it's
        # contents?
        $target_env = $self->meta->new_object(
            parent => $self,
            variables => $target_env
        );
    }

    unless (blessed $target_env && $target_env->isa(__PACKAGE__)) {
        throw(
            'object/environment/child_environment_invalid',
            'Unable to instantiate a child environment'
        );
    }

    my $process = $self->get($name);

    unless (blessed $process && $process->isa('Zaaksysteem::Object::Process')) {
        throw('object/environment/process_not_found', sprintf(
            'Unable to find process "%s"',
            $name
        ));
    }

    return $process->eval($target_env);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Object::Roles::Synchronizable;

use Moose::Role;

use Zaaksysteem::Tools;

requires qw[
    capabilities
    TO_JSON
];

=head1 NAME

Zaaksysteem::Object::Roles::Synchronizable - Expose object synchronization
functionality to frontend

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 synchronization_interfaces

Holds an HashRefs of interface_(id, label) tuples as an ArrayRef. This
attribute should be filled with interfaces that know how to synchronize
an instance implementing this role.

Built via C<build_synchronization_interfaces>, which should be provided via
the object type instance of the object this role is applied to.

=cut

has synchronization_interfaces => (
    is => 'ro',
    isa => 'ArrayRef[HashRef]',
    builder => 'build_synchronization_interfaces',
);

=head1 ACTIONS

=head2 synchronize

This action can be called via the L<Zaaksysteem::Object> action API, and
synchronizes the object using the provided interface.

The action basically wraps a call to the interface's
L<Zaaksysteem::Backend::Sysin::Interface::Component/process_trigger> method,
with the object instance as the argument.

On success a flash message is set and the user is redirect to the object
detail view (C</object/[UUID]>).

=head3 URL

C</api/object/[UUID]/action/synchronize>

=head3 Parameters

=over 4

=item interface_id

The primary key for the interface to be used for synchronizing this object

=back

=cut

define_profile synchronize => (
    required => {
        interface_id => 'Int'
    }
);

sub synchronize {
    my $self = shift;
    my $c = shift;

    my $params = assert_profile($c->req->params)->valid;

    my $interface = $c->model('DB::Interface')->find(
        $params->{ interface_id }
    );

    unless (defined $interface) {
        throw('object/synchronize/interface_not_found', sprintf(
            'Cannot find interface %d',
            $params->{ interface_id }
        ));
    }

    my $transaction = try {
        return $interface->process_trigger('update_object', {
            object => $self,
            object_model => $c->model('Object')
        });
    };

    my $message;

    if (defined $transaction && (not $transaction->error_fatal)) {
        $message = {
            type => 'info',
            message => sprintf(
                'Synchronisatie met <a href="%s">%s</a> voltooid',
                $c->uri_for('/beheer/sysin/transactions/' . $transaction->id),
                $interface->name
            )
        };
    } elsif(defined $transaction) {
        $message = {
            type => 'error',
            message => sprintf(
                'Synchronisatie met <a href="%s">%s</a> mislukt',
                $c->uri_for('/beheer/sysin/transactions/' . $transaction->id),
                $interface->name
            )
        };
    } else {
        $message = {
            type => 'error',
            message => sprintf(
                'Er is een fout opgetreden tijdens het initialiseren van de synchronisatie, controleer de <a href="%s">koppeling</a>.',
                $c->uri_for('/beheer/sysin/overview')
            )
        };
    }

    $c->push_flash_message($message);
    $c->res->redirect($c->uri_for('/object/' . $self->id));
    $c->detach;
}

=head1 METHODS

=cut

around capabilities => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_), qw[
        synchronize
    ];
};

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ synchronization_interfaces } = $self->synchronization_interfaces;

    return $data;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

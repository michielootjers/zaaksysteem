package Zaaksysteem::Object::Importer::Inav;

use Moose::Role;

use constant CASETYPE_OBJECT_TYPE => 'casetype';

sub handles {
    my $self = shift;
    my $object_type = shift;

    return $object_type eq CASETYPE_OBJECT_TYPE;
}

sub hydrate_from_files { }

sub redirect {
    '/beheer/import/inavigator/upload'
}

1;

package Zaaksysteem::Object::AttributeRole::CaseDestructable;
use Moose::Role;

use DateTime;
use Zaaksysteem::Exception;

with 'Zaaksysteem::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Object::AttributeRole::CaseDestructable - "Is this case destructable" - calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class => 'CaseDestructable',
        parent_object => $some_obj,
        ...
    );
    print "This is is" . ($attr->value ? '' : "n't") . " destructable";

=head1 METHODS

See L<Zaaksysteem::Object::AttributeRole> for the inherited wrapper around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $date_destruction = $self->_get_attribute('case.date_destruction')->value;

    # No date set
    if (!defined($date_destruction)) {
        return 0;
    }

    return 1 if $date_destruction < DateTime->now();
    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


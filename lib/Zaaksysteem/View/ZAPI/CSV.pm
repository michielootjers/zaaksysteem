package Zaaksysteem::View::ZAPI::CSV;

use Moose;

use Data::Dumper;
use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use Text::CSV;

BEGIN { extends 'Catalyst::View'; }

__PACKAGE__->config(
    'stash_key'             => 'csv',
    'quote_char'            => '"',
    'escape_char'           => '"',
    'sep_char'              => ',',
    'eol'                   => "\n",
    'binary'                => 1,
    'allow_loose_quotes'    => 1,
    'allow_loose_escapes'   => 1,
    'allow_whitespace'      => 1,
    'always_quote'          => 1
);

use constant EXPORT_FILETYPE_CONFIG => {
    xls         => { mimetype => 'application/vnd.ms-excel', extension => 'xls' },
    calc        => { mimetype => 'application/vnd.oasis.opendocument.spreadsheet', extension => 'ods' },
    csv         => { mimetype => 'text/csv', extension => 'csv' },
};

=head1 METHODS

=head2 $csv->process($c, \%OPTIONS)

Return value: $STRING_CONTENT

    $csv->process($c, { format => 'csv', zapi_object => [{beer => 'heineken'}] });

Called from catalyst when processing this view.

B<Options>

=over 4

=item zapi_object (required)

The ZAPI object to be converted to CSV

=item format (required)

The format of the output: instead of C<csv>, you could use the formats C<csv> or C<calc>

=back

=cut

define_profile 'process'    => (
    required            => [qw/zapi_object format/],
    constraint_methods  => {
        format      => qr/csv|xls|calc/,
    },
    field_filters       => {
        format      => ['lc'],
    }
);

sub process {
    my $self                = shift;
    my $c                   = shift;
    my $options             = assert_profile(shift || {})->valid;

    my $zapi_object         = $options->{zapi_object};
    my $format              = $options->{format};

    $c->stash->{csv}        = $self->_process_zapi_result(
        $zapi_object,
        $c->stash->{zapi}->{result}
    );

    my $content = $self->render( $c, undef, $zapi_object, $c->stash );

    $self->post_process_content($c, $format, $content);
}

=head2 $csv->post_process_content($c, $format, $string_content)

Return value: $STRING_CONTENT

    $csv->post_process_content($c, 'xls',"case.number,casetype.name\n1,Testzaaktype");

Post processor for C<process>. Will convert the default CSV format to the given format.

Will call jodconverter for C<xls> and C<calc>, also will set the correct content_type
and content-disposition.

B<Options>

=over 4

=item $format

The format of the output: instead of C<csv>, you could use the formats C<csv> or C<calc>

=back

=cut

sub post_process_content {
    my $self                = shift;
    my $c                   = shift;
    my $format              = shift;
    my $raw_content         = shift;

    my $content;
    if ($format eq 'csv') {
        $c->res->headers->header( "Content-Type" => "text/csv" )
            unless ( $c->res->headers->header("Content-Type") );

        $content    = $raw_content;
    } else {
        $c->res->headers->header( 'Content-Type'  => 'application/x-download' );
        $c->res->headers->header(
            'Content-Disposition'  =>
                "attachment;filename=zaaksysteem-" . time()
                . "." . EXPORT_FILETYPE_CONFIG->{ $format }->{extension}
        );

        $content    = $self->_invoke_jodconvertor($format, $raw_content);
    }

    $c->res->body($content);
}

=head2 $csv->_invoke_jodconvertor($format, $string_csv_data)

Return value: $STRING_CONTENT

    $csv->_invoke_jodconverter('xls',"case.number,casetype.name\n1,Testzaaktype");

Will call jodconverter to convert CSV data into formats like C<xls> and C<calc>

B<Options>

=over 4

=item $format

Either C<xls> or C<calc>

=item $string_csv_data

The CSV data to convert

=back

=cut

sub _invoke_jodconvertor {
    my ($self, $filetype, $csv) = @_;

    my $ua      = LWP::UserAgent->new;
    my $result  = $ua->post('http://localhost:8080/converter/service',
        Content         => $csv,
        Content_Type    => 'text/csv',
        Accept          => EXPORT_FILETYPE_CONFIG->{$filetype}->{'mimetype'},
    );

    if($result->code ne '200') {
        throw(
            'zapi/csv/converter/failure',
            'Jodconverter failure: ' . $result->code . ': ' . $result->content
        )
    }

    return $result->content;
}

sub _load_csv_header {
    my $self            = shift;
    my $zapi_object     = shift;
    my $result          = shift;


    if (
        $zapi_object->options &&
        $zapi_object->options->{csv}->{column_order}
    ) {
        return $zapi_object->options->{csv}->{column_order};
    }


    my ($first_row) = @$result;

    if (UNIVERSAL::isa($first_row, 'DBIx::Class')) {
        if ($first_row->can('csv_header')) {
            return $first_row->csv_header;
        } else {
            return [ keys $first_row->get_columns ];
        }
    }

    return;
}

=head2 $csv->_process_zapi_result($zapi_object, $result_from_zapi)

Return value: \@ROWS

    $csv->post_proce_process_zapi_result($zapi_object, $c->stash->{zapi}->{result});

Will turn the rows given in C<<$c->stash->{zapi}->{result} into a CSV format. By finding out
the header for the CSV, and places the output just below it.

B<Options>

=over 4

=item $zapi_object

Raw zapi_object. An instance of L<<Zaaksysteem::ZAPI::Response>>

=item $result_from_zapi

Because the ZAPI system can put bounds to our resultset, by setting pagination for instance,
this flag is ARRAYREF of rows to convert.

=back

=cut

sub _process_zapi_result {
    my $self            = shift;
    my $zapi_object     = shift;

    ### Do something with blessed references
    my $result          = shift;

    my $rows            = [];

    throw(
        'zaaksysteem/view/zapi/csv',
        'No zapi attribute found, cannot generate csv'
    ) unless $result;

    my $column_order    = $self->_load_csv_header($zapi_object, $result);

    unless (
        $zapi_object->options &&
        $zapi_object->options->{csv}{no_header}
    ) {
        push(@{ $rows }, $column_order) if $column_order;
    }

    # loop through results, use the column order
    foreach my $entry (@$result) {
        my $row;
        if (UNIVERSAL::isa($entry, 'DBIx::Class')) {
            my $rowdata = {};
            if ($entry->can('csv_data')) {
                $row    = $entry->csv_data;
            } else {
                my $rowdata    = { $entry->get_columns };
                $row    = [
                    map { $rowdata->{ $_ } } @$column_order
                ];               
            }
        } else {
            $row    = [
                map { $entry->{$_} } @$column_order
            ];
        }
        push @$rows, $row;
    }

    return $rows;
}


sub csv_options {
    my ($self, $arguments) = @_;

    my $options     = $arguments->{options}    or die "need options";

    my $config = $self->config;

    if(my $csv_options = $options->{csv}) {
        foreach my $key (keys %$config) {
            if(my $override = $csv_options->{$key}) {
                $config->{$key} = $override;
            }
        }
    }
    return $config;
}

=head1 METHODS

=head2 $csv->render($c, $template_name, $zapi_object, $params)

Return value: $STRING_CONTENT

    $csv->render($c, undef, $zapi_object, { 'csv' => $rows });

Called from catalyst when processing this view.

B<Options>

=over 4

=item $template_name

Because every view gets a template in the second argument, we make sure we still are
"interface correct". We do NOTHING with this var. Setting C<undef> has no effect.

=item $zapi_object (required)

The ZAPI object to be converted to CSV/ An instance of L<<Zaaksysteem::ZAPI::Response>>

=item params

Normally the stash, from where it can pick the C<stash_key> defined in our config. Defaults
to C<csv>.

=back

=cut

sub render {
    my $self = shift;
    my ( $c, $template, $zapi_object, $args ) = @_;

    my $config = $self->config;

    if(my $options = $zapi_object->options) {
        $config = $self->csv_options({
            options => $options,
        });
    }

    my $stash_key = $self->config->{'stash_key'};
    return '' unless $args->{$stash_key} && ref($args->{$stash_key}) =~ /ARRAY/;

    $config = { map { $_ => $config->{$_} } qw/
        quote_char
        escape_char
        sep_char
        eol
        binary 
        allow_loose_quotes
        allow_loose_escapes
        allow_whitespace
        always_quote/ 
    };

    my $csv = Text::CSV->new($config);

    my $content = '';
    foreach my $row ( @{ $args->{$stash_key} } ) {
        # Skip empty rows:
        next unless scalar @{ $row };

        ### Blessed objects need to be down translated
        my $status = $csv->combine( @{ $row } );
        Catalyst::Exception->throw(
            "Text::CSV->combine Error: " . $csv->error_diag() )
          if ( !$status );
        $content .= $csv->string();
    }

    return $content;
}

1;


package Zaaksysteem::View::ZAPI::JSON;

use Moose;
use JSON::XS (); # Empty list to prevent import of JSON::XS::encode_json

use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::ZAPI::Error;

use Data::Dumper;

BEGIN { extends 'Catalyst::View::JSON'; }

__PACKAGE__->config(
    expose_stash    => 'zapi',
);


# after http://www.gossamer-threads.com/lists/catalyst/users/29706
after 'process' => sub {
    my ($self, $c) = @_;

    if (my $content_type = $c->stash->{ json_content_type }) {
        $c->res->content_type($content_type);
    }
};


sub encode_json {
    my($self, $c, $data) = @_;

    my $encoder = JSON::XS->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed;

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub { shift->iso8601 };

    return $encoder->encode($data);
}

1;

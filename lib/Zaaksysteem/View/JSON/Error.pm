package Zaaksysteem::View::JSON::Error;

use Moose;
use Data::Dumper;
use JSON;
use Devel::StackTrace;

BEGIN { extends 'Zaaksysteem::View::JSON' };

=head1 Zaaksysteem::View::JSON::Error

This custom view is meant for providing requests in JSON context with a
standerdized error response, and is usually invoked by detaching to this
packagename when an errorcondition is found. It uses data in the
L<Catalyst> stash. The JSON body produced should be an implementation of the
structure described L<here|Zaaksysteem::Manual::API#ERROR_HANDLING>.

=head2 Used stash keys

=over 4

=item error

This key should contain a scalar string that signifies which error has been
detected. This should be in the format C<error[/suberror]*>. An example would
be C<parameter/invalid>.

=item data

This key is associated with a hashref that contains arbitrary data to be sent
over the line to the frontend / client. This data can also be used by the
individual error handlers defined in this package.

=back

=head2 Example invocation

    unless($c->req->param('case_id')) {
        $c->stash->{ error } = 'parameter/missing';
        $c->stash->{ data } = { parameter => 'case_id' };
        $c->detach('Zaaksysteem::View::JSON::Error');
    }

=head1 Error Categories

C<ERROR_CATEGORIES> is a constant hashref that contains the 'namespaced'
error handlers currently available. It can be arbitrarily deeply nested, but
the final reference should always be an anonymous sub that sets the correct
fields in the JSON body (the first argument) or the L<Catalyst::Response>
(second argument).

=head2 Example error category handler definition

    use constant ERROR_CATEGORIES => {
        'parameter' => {
            'missing' => sub {
                my $json = shift;

                $json->{ messages } = [ sprintf('Invalid parameters `%s`', $json->{ data }{ parameter }) ];
            }
        }
    };

=head2 Defined categories

=over 4

=item parameter/invalid

Invoked on invalid parameter, usually some hardwired validation scheme that
conditioned out. Uses C<parameter> key in the C<data> hashref.

=item parameter/missing

Invoked on missing request parameter. Uses C<parameter> key in the C<data>
hashref.

=item parameter/unresolvable

Invoked when a supplied parameter could not be logically resolved into, for
example, a row from the database. Uses C<parameter> key in the C<data> hashref.

=item request/method

Invoked on invalid request method for the URL requested. Sets the response
status to 405 for Method Not Allowed.

=item object/state

An object involved with the request has been found to be in a state which in
which the request cannot be executed. It could for example mean that a case has
been closed, and the client is trying to modify fields which it shouldn't.

Uses C<object_name> as an identifier for the object type, and C<reason> as 
a textual explanation of what's wrong.

=back

=cut

use constant ERROR_CATEGORIES => {
    parameter => {
        invalid => sub {
            my $json = shift;

            push @{ $json->{ messages } }, sprintf('Invalid parameter `%s`', $json->{ data }{ parameter });
        },

        missing => sub {
            my $json = shift;

            push @{ $json->{ messages } }, sprintf('Missing parameter `%s`', $json->{ data }{ parameter });
        },

        unresolvable => sub {
            my $json = shift;

            push @{ $json->{ messages } }, sprintf('Parameter `%s` did not resolve into whatever it\'s referring to', $json->{ data }{ parameter });
        },
    },

    request => {
        method => sub {
            my ($json, $res) = @_;

            $json->{ messages } = ['Invalid request method'];
            $json->{ error } = 4;

            $res->status(405);
        },
    },

    object => {
        state => sub {
            my ($json, $res) = @_;

            $json->{ messages } = [ sprintf('Inconsistant state for object `%s`, %s', $json->{ data }{ object_name }, $json->{ data }{ reason }) ];
        },
    },
};

sub process {
    my ($self, $c) = @_;

    # Categories are 'namespaced', unroll like a directory structure
    my @path = split m[/], $c->stash->{ error };
    my $node = ERROR_CATEGORIES;

    # Initialize stacktrace when in debugging mode, otherwise use a mock hashref
    my @frames;

    if($ENV{ CATALYST_DEBUG }) {
        @frames = Devel::StackTrace->new->frames;
    }

    my $error_body = {
        error => 1,
        data => $c->stash->{ data } // { },
        category => $c->stash->{ error },
        stacktrace => [ map { $self->stack_frame_as_json($_) } @frames ]
    };

    # Unless some error has a specific HTTP error code, iGiveUp
    $c->res->status(500);

    # Walk our tree
    1 while ref($node) eq 'HASH' && ($node = $node->{ shift @path });

    if(ref($node) eq 'CODE') {
        $node->($error_body, $c->res);
    }

    $c->res->content_type('application/json');
    $c->res->body(to_json($error_body, { pretty => 1}));
}

=head1 Stack trace unrolling

In C<CATALYST_DEBUG> mode, with every error a stacktrace is provided by this
JSON error handler. This method takes as argument a Devel::StackTrace::Frame
and returns a hashref containing the essential information contained in that
frame.

=cut

sub stack_frame_as_json {
    my ($self, $frame) = @_;

    return {
        package => $frame->package,
        filename => $frame->filename,
        line => $frame->line,
        subroutine => $frame->subroutine,
        args => [ map { ref || $_ } $frame->args ]
    };
}

1;

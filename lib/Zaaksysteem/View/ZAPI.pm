package Zaaksysteem::View::ZAPI;

use Moose;

use Data::Dumper;
use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::ZAPI::Error;

use Devel::StackTrace;

BEGIN { extends 'Catalyst::View'; }

use constant ZAPI_FORMATS   => {
    'csv'       => 'View::CSV',
    'xls'       => 'View::CSV',
    'calc'      => 'View::CSV',
};


=head2 process

Extends the default process sub

=cut

sub process {
    my $self    = shift;
    my $c       = shift;

    die('Error calling this View, missing "zapi" stash attribute')
        unless (
            exists($c->stash->{zapi}) &&
            $c->stash->{zapi}
        );

    # my $trace = Devel::StackTrace->new;

    # $c->log->debug('Processing:' . Dumper( $trace->as_string));

    my $zapi_object;
    if (
        UNIVERSAL::isa(
            $c->stash->{zapi},
            'Zaaksysteem::ZAPI::Response'
        ) ||
        UNIVERSAL::isa(
            $c->stash->{zapi},
            'Zaaksysteem::ZAPI::Error'
        )
    ) {
        $zapi_object        = $c->stash->{zapi};
        $c->stash->{zapi}   = $c->stash->{zapi}->response;
        $c->res->status($c->stash->{zapi}->{status_code});
    } elsif (
        $c->stash->{zapi} &&
        !(
            UNIVERSAL::isa($c->stash->{zapi}, 'HASH') &&
            $c->stash->{zapi}->{num_rows}
        )
    ) {
        my $zapi_no_pager = 0;

        # If the controller specifies it's a "no pager" controller, AND the
        # user specifies a zapi_no_pager parameter, the parameter wins.
        $zapi_no_pager = $c->stash->{zapi_no_pager}
            if (defined $c->stash->{zapi_no_pager});
        $zapi_no_pager = $c->req->params->{zapi_no_pager}
            if (defined $c->req->params->{zapi_no_pager});

        ### Some kind of loop?
        $zapi_object     = Zaaksysteem::ZAPI::Response->new(
            unknown         => $c->stash->{zapi},
            uri_prefix      => $c->uri_for(
                $c->action,
                $c->req->captures,
                @{ $c->req->args },
                $c->req->params
            ),
            no_pager        => $zapi_no_pager,
            page_current    => $c->req->params->{   zapi_page     } || 1,
            page_size       => $c->req->params->{   zapi_num_rows } || 10,

            order_by        => $c->req->params->{   zapi_order_by   },

            order_by_direction => $c->req->params->{zapi_order_by_direction},
        );

        $c->stash->{zapi} = $zapi_object->response;
        
        $c->res->status($c->stash->{zapi}->{status_code});
    }

    my $format  = $c->req->params->{zapi_format};

    if ($c->res->status ne '200') {
        $format = 'json';
    }

    $self->_process_format($c, $format, $zapi_object, @_);
};

sub _process_format {
    my $self        = shift;
    my $c           = shift;
    my $format      = shift;
    my $zapi_object = shift;


    if ($format && ZAPI_FORMATS->{$format}) {
        return $c->view('ZAPI::CSV')->process(
            $c,
            {
                zapi_object => $zapi_object,
                format      => $format
            },
            @_
        );
    }

    ### Default JSON
    return $c->view('ZAPI::JSON')->process($c, $zapi_object, @_);
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


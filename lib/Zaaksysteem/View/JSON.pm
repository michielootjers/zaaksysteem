package Zaaksysteem::View::JSON;

use Moose;

use JSON::XS ();

use Data::Dumper;

BEGIN { extends 'Catalyst::View::JSON'; }

has 'url_navigation' => (
    is => 'rw',
    isa => 'HashRef[Maybe[Str]]',
    lazy => 1,
    clearer => '_clear_nav',
    default => sub {
        { next => undef, prev => undef };
    }
);

=head2 process

Extends the default process sub to extract some pager information

This featurette was created to inject into JSON responses for ResultSets
or other paged data URLs that the frontend can directly use to navigate
the resource. Should probably be extended further (with stuff like
C<latest_results_url> for the timeline, or perhaps refresh URLs for datasets
that are prone to change while being viewed).

=cut

around 'process' => sub {
    my $orig = shift;
    my $self = shift;
    my $c = shift;

    # Extract the required navigation URLs and store them as local state
    # > yeah, maybe not such a great idea...
    # > also implicitly relies on the value under *_url is an URI object.
    if($c->stash->{ next_url }) {
        $self->url_navigation->{ next } = $c->stash->{ next_url }->as_string;
    }

    if($c->stash->{ prev_url }) {
        $self->url_navigation->{ prev } = $c->stash->{ prev_url }->as_string;
    }

    my $retval = $self->$orig($c, @_);

    # This little gem is a workaround that fixes a bug where the View object
    # holds on to links from a previous request. Naive implementation, View
    # objects are persistant across requests. TODO: Remove the custom state,
    # keep a reference to $c->stash->{ *_url } so that encode_json can pick
    # up on it.
    $self->_clear_nav;

    if ($c->stash->{ json_content_type }) { 
        $c->res->headers->header(
            'Content-Type' => $c->stash->{ json_content_type }
        );
    }

    # http://docs.angularjs.org/api/ng.$http @ JSON Vulnerability Protection
    #$c->res->body(sprintf(")]}',\n%s", $c->res->body));

    return $retval;
};

=head2 encode_json

Extends the default encode_json to add some default wrapping for DBIx::Class.
objects.

=cut

sub encode_json {
    my($self, $c, $data) = @_;

    if(UNIVERSAL::isa($data, 'DBIx::Class::ResultSet')) {
        $data = $self->prepare_json_resultset($data);
    }
    # Handle single DBIx::Class rows
    elsif (UNIVERSAL::isa($data, 'DBIx::Class::Row')) {
        $data = $self->prepare_json_row($data);
    }
    # Check if arrays contain DB-rows. Does not check the entire array. Mixing 
    # rows with other stuff is rather silly.
    elsif (UNIVERSAL::isa($data, 'ARRAY')) {
        $data = $self->prepare_json_rows($data);
    }
    
    my $encoder = JSON::XS->new->pretty->allow_nonref->allow_blessed->convert_blessed;

    $encoder->encode($data);
}

=head2 prepare_json_resultset

Function to prepare DBIx::Class ResultSets for JSON.

=cut

sub prepare_json_resultset {
    my ($self, $resultset) = @_;

    ### TODO - build in paging
    return {
        at       => undef,
        rows     => undef,
        num_rows => $resultset->count,
        result   => [$resultset->all],
        next     => $self->url_navigation->{ next },
        prev     => $self->url_navigation->{ prev },
    }
}

=head2 prepare_json_row

Function to prepare a single DBIx::Class Row for JSON.

=cut

sub prepare_json_row {
    my ($self, $row) = @_;

    return {
        at       => undef,
        rows     => undef,
        num_rows => 1,
        result   => [$row],
        next     => $self->url_navigation->{ next },
        prev     => $self->url_navigation->{ prev },
        comment  => 'This is a single DBIx::Class::Row, it has no paging.'
    }
}

=head2 prepare_json_rows

Function to prepare DBIx::Class Rows for JSON.

=cut

sub prepare_json_rows {
    my ($self, @rows) = @_;

    return {
        at       => undef,
        rows     => undef,
        num_rows => scalar @rows,
        result   => @rows,
        next     => $self->url_navigation->{ next },
        prev     => $self->url_navigation->{ prev },
        comment  => 'This is not a DBIx::Class::ResultSet and does NOT support paging.'
    }
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Template::Plugin::ValutaFormattingFilter;
use Template::Plugin::Filter;
use base qw( Template::Plugin::Filter );

use Zaaksysteem::Constants ();

=head2 filter

Format valuta field, by removing all thousands separators (","), and using "."
as a decimal separator.

=cut

sub filter {
    my ($self, $text) = @_;

    return Zaaksysteem::Constants::_numeric_fix_filter(undef, $text);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Template::Plugin::DateFormattingFilter;

use Template::Plugin::Filter;
use base qw( Template::Plugin::Filter );

=head2 filter

 Enable TT templates to format Dutch dates: 20110212 => 12 februari 2011

=cut

sub filter {
    my ($self, $text) = @_;

    my @months = qw/januari februari maart april mei juni juli augustus september oktober november december/;
        
    if(my ($year, $month, $day) = $text =~ m|^(\d{4})(\d{2})(\d{2})$|) {
	    return int($day) . ' ' . $months[int($month)-1] . ' ' . int($year);
    }

    return $text;
}


1;

package Zaaksysteem::Template::Plugin::JSON;

use Moose;

BEGIN { extends 'Template::Plugin'; }

has context => ( is => 'ro', isa => 'Template::Context' );

sub load { shift; }

sub BUILDARGS {
    my $orig = shift;
    my $self = shift;

    my $context = shift;

    return $self->$orig({ context => $context });
}

sub encode {
    my $self = shift;

    my $scalar;

    if(scalar(@_) > 1) { $scalar = \@_; }
    else { $scalar = shift; }

    return JSON->new->canonical->allow_nonref->allow_blessed->convert_blessed->encode($scalar);
}

1;

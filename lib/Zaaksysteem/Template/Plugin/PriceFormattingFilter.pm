package Zaaksysteem::Template::Plugin::PriceFormattingFilter;

use Template::Plugin::Filter;
use base qw( Template::Plugin::Filter );
use Number::Format;

=head2 filter

Format price: 1234567 => 1.234.567

=cut

sub filter {
    my ($self, $text) = @_;

    if($text && $text =~ m|^\d+$|) {
        
        my $number_formatter = new Number::Format(-thousands_sep => '.', -decimal_point => ',', -int_curr_symbol => 'DEM'); 
    
        return $number_formatter->format_number(int($text));
    }

    return $text;
}

1;

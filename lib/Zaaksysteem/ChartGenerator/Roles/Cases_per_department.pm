package Zaaksysteem::ChartGenerator::Roles::Cases_per_department;

use Moose::Role;
use Data::Dumper;


=head2 cases_per_department

Returns a chart profile
 
=cut

sub cases_per_department {
    my ($self) = @_;

    my $rs = $self->resultset->search({}, {
        select => [
            'route_ou',
            {
                count => 'me.id',
                -as => 'count'
            }
        ],
        group_by => [ 'route_ou'],
        order_by => [ 'route_ou']
    });

    $rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my @results = $rs->all();

    my @categories = map { 
        $_->{route_ou} 
    } @results;

    my @average_handling_times = map { int $_->{count} } @results;

    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => 'Zaken per afdeling',
        },
        xAxis => {
            categories => \@categories
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Aantal zaken'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>{point.y:.f} zaken</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                pointPadding => 0.2,
                borderWidth => '0'
            }
        },
        series => [{
            name => 'Afdeling',
            data => \@average_handling_times
        }]
    };

    return $profile;
}


1;

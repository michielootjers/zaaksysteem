package Zaaksysteem::ChartGenerator::Roles::Cases_per_owner_per_month;

use Moose::Role;
use Data::Dumper;


=head2 search_cases_per_owner

Within a given resultset, find all the different owners and do a count per owner.
Then lookup the name for each found behandelaar.

=cut

sub search_cases_per_owner {
    my ($self, $rs) = @_;

    my $resultset = $rs->search({}, {
        select => [
            'behandelaar_gm_id',
            {
                count => "me.id",
                -as => 'count'
            },
        ],
        group_by => [ 'behandelaar_gm_id'],
        order_by => [ 'behandelaar_gm_id']
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my @results = $resultset->all();


    foreach my $result (@results) {
        my $behandelaar_gm_id = $result->{behandelaar_gm_id};

        $result->{behandelaar} = $behandelaar_gm_id ? $self->lookup_cached_behandelaar($behandelaar_gm_id) : 'geen'
    }

    return @results;
}


=head2 cases_per_owner_per_month

Generate a chart profile

=cut

sub cases_per_owner_per_month {
    my ($self) = @_;

    my @results = $self->search_months_with_cases($self->resultset);
    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my $categories    = [];
    my $hash_matrix   = {};
    my $unique_owners = {};

    # Loop through the months
    foreach my $result (@results) {

        my $month = $month_names[$result->{month}-1] . ' '. $result->{year};

        push @$categories, $month;

        my $owner_rs = $self->search_month($self->resultset, $result);

        my @owner_results = $self->search_cases_per_owner($owner_rs);

        # foreach department, add the found count to a matrix
        # we will only know all the relevant departments at the end
        my $owner_averages = {};
        foreach my $owner_row (@owner_results) {
            warn Dumper $owner_row;
            my $behandelaar_gm_id = $owner_row->{behandelaar};
            my $count = $owner_row->{count};

            $owner_averages->{$behandelaar_gm_id} = $count;

            $unique_owners->{$behandelaar_gm_id} += 1;
        }

        $hash_matrix->{$month} = $owner_averages;
    }

    my $series = $self->unsparsify({
        unique_items => $unique_owners,
        hash_matrix => $hash_matrix,
        categories => $categories
    });

    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => "Zaken per behandelaar per maand (%)"
        },
        xAxis => {
            categories => $categories
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Percentage zaken'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>{point.y:.f} zaken</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                stacking => 'percent'
            }
        },
        series => $series
    };

    return $profile;
}



1;

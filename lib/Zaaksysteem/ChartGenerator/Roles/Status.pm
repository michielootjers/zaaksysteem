package Zaaksysteem::ChartGenerator::Roles::Status;

use Moose::Role;
use Data::Dumper;


=head2 status

Generate chart profile

=cut

sub status {
    my ($self) = @_;

    my $profile = {
        chart => {
            defaultSeriesType => 'line',
        },
        title => { text => 'Geregistreerd/afgehandeld' },
        yAxis => { title => { text => 'Aantal zaken' }, min => 0 },
    };

    my ($rsr,$rsa,$axis)   = $self->resultset->group_geregistreerd();

    die 'Could not find resultset for chart' unless $rsa;

    my $data = [];
    while (my $rsrrow = $rsr->next) {

        my $rsarow      = $rsa->next;
        my $axis_label  = shift(@{ $axis->{x} });

        push(@{ $data },
            {
                day => $axis_label,
                geregistreerd => int($rsrrow->zaken),
                afgehandeld   => int($rsarow->zaken)
            }
        );
    }
    if(@$data) {
        $profile->{xAxis} = {'categories' => [ map {$_->{day}} @$data ]    }; 

        my $afgehandeld    = { name => 'Afgehandeld', data => [ map {$_->{afgehandeld}} @$data ]};
        my $geregistreerd  = { name => 'Geregistreerd', data => [ map {$_->{geregistreerd}} @$data ]};
        $profile->{series} = [$afgehandeld, $geregistreerd];
    }

    return $profile;
}


1;

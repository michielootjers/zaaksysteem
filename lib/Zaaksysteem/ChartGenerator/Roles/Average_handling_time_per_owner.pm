package Zaaksysteem::ChartGenerator::Roles::Average_handling_time_per_owner;

use Moose::Role;
use Data::Dumper;



=head2 search_average_handling_time_per_owner

Returns a list with the the owners in the given resultset.
For every owner the average handling time is calculated.
 
=cut

sub search_average_handling_time_per_owner {
    my ($self, $rs) = @_;

    #use Time::HiRes qw/gettimeofday tv_interval/;
    #my $t0 = [gettimeofday];

    my $resultset = $rs->search({
        afhandeldatum => {
            -not => undef,
        }
    },{
        select => [
            'behandelaar_gm_id',
            {
                date_part => {
                    "'days'," => {
                        justify_hours => {
                            avg => "afhandeldatum - registratiedatum"
                        },
                    }
                },
                -as => 'average_handling_time'
            }
        ],
        group_by => [ 'behandelaar_gm_id'],
        order_by => [ 'behandelaar_gm_id']
    });
    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my @results = $resultset->all();

    my @behandelaars = map { $_->{behandelaar_gm_id} } @results;

    foreach my $result (@results) {
        my $behandelaar_gm_id = $result->{behandelaar_gm_id};

        $result->{behandelaar} = $behandelaar_gm_id ? $self->lookup_cached_behandelaar($behandelaar_gm_id) : 'geen'
    }
    #warn "time taken: " . tv_interval($t0);

    return @results;
}


=head2 lookup_cached_behandelaar

Given a behandelaar_gm_id looks for the name.
Since the zaak_betrokkene table contains multiple snapshots, the latest snapshot is selected
and put in the cache.
 
=cut

sub lookup_cached_behandelaar {
    my ($self, $behandelaar_gm_id) = @_;

    die "need behandelaar_gm_id" unless $behandelaar_gm_id;
    unless($self->behandelaars_cache->{$behandelaar_gm_id}) {

        my $zaak_betrokkenen = $self->resultset->result_source->schema->resultset('ZaakBetrokkenen');

        my $rs = $zaak_betrokkenen->search({ 
            gegevens_magazijn_id    => $behandelaar_gm_id,
            betrokkene_type         => 'medewerker',
        },{
            columns => [qw/gegevens_magazijn_id naam/],
            order_by => { '-desc' => 'id' },
            rows => 1,
        });
        my $row = $rs->first();

        $self->behandelaars_cache->{$behandelaar_gm_id} = defined($row) ? $row->naam : undef;
    }
    return $self->behandelaars_cache->{$behandelaar_gm_id};
}


=head2 average_handling_time_per_owner

Returns a chart profile
 
=cut

sub average_handling_time_per_owner {
    my ($self) = @_;

    my @results = $self->search_average_handling_time_per_owner($self->resultset);

    my @behandelaars = map { $_->{behandelaar} } @results;
    my $average_handling_times = [];
    foreach my $result (@results) {
        push @$average_handling_times, int ($result->{average_handling_time} || 0);
    }
    
    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => 'Gemiddelde behandeltijd per behandelaar',
        },
        xAxis => {
            categories => \@behandelaars
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Gem. behandeltijd (dagen)'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>{point.y:.f} dagen</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                pointPadding => 0.2,
                borderWidth => '0'
            }
        },
        series => [{
            name => 'Behandelaar',
            data => $average_handling_times
        }]
    };

    return $profile;
}


1;

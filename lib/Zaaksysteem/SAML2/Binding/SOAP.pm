package Zaaksysteem::SAML2::Binding::SOAP;

use Moose;

BEGIN { extends 'Net::SAML2::Binding::SOAP'; }

use Zaaksysteem::Exception;

use Zaaksysteem::SAML2::XML::Sig;
use XML::Generator;
use HTTP::Request::Common;
use LWP::UserAgent;

has xmlsig => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    Zaaksysteem::SAML2::XML::Sig->new({
        x509 => 1,
        key => $self->key,
        cert => $self->cert
    });
});

has xmlgen => ( is => 'ro', lazy => 1, default => sub {
    XML::Generator->new(':std');
});

has ua => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    LWP::UserAgent->new(
        agent => 'Zaaksysteem SAML+SOAP Agent',
        ssl_opts => {
            SSL_cert_file => $self->cert,
            SSL_key_file  => $self->key
        }
    );
});

has soap_action => ( is => 'ro', default => '"http://www.oasis-open.org/committees/security"' );

sub request {
    my $self            = shift;

    my $request_body    = $self->create_soap_envelope(shift);

    my $req             = POST $self->url, Content => $request_body;

    $req->header('SOAPAction' => $self->soap_action);
    $req->header('Content-Type' => 'text/xml');
    $req->header('Content-Length' => length $request_body);
    $req->header('Connection' => 'Keep-Alive');

    my $res             = $self->ua->request($req);

    unless($res->header('Content-Length')) {
        throw('saml2/soap/response', 'SOAP response came back empty');
    }

    return $res->content;
}

sub create_soap_envelope {
    my $self = shift;

    my $signed_message = $self->xmlsig->sign(shift);

    unless($self->xmlsig->verify($signed_message)) {
        #throw('saml2/soap/signature', 'Unable to verify XML Signature in outgoing SOAP message.');
    }

    return sprintf(
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n  <soapenv:Body>\n    %s\n  </soapenv:Body>\n</soapenv:Envelope>",
        $signed_message
    );
}

sub handle_response {
    my ($self, $response) = @_;

        # Zaaksysteem commented out
    # # verify the response
    # my $x       = $self->xmlsig;
    # my $ret = $x->verify($response);
    # die "bad SOAP response" unless $ret;

    # # verify the signing certificate
    # my $cert    = $x->signer_cert;
    # my $ca      = Crypt::OpenSSL::VerifyX509->new($self->cacert);
    # my $ret     = $ca->verify($cert);
    # die "bad signer cert" unless $ret;

    # my $subject = sprintf("%s (verified)", $cert->subject);

    # parse the SOAP response and return the payload
    my $parser = XML::XPath->new( xml => $response );
    $parser->set_namespace('soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
    $parser->set_namespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

    my $subject;
    my $saml = $parser->findnodes_as_string('/soap-env:Envelope/soap-env:Body/*');
    return ($subject, $saml);
}

1;

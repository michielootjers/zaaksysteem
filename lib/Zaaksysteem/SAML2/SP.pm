package Zaaksysteem::SAML2::SP;

use Moose;

BEGIN { extends 'Net::SAML2::SP'; }

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

# Bloody assumptions by Net::SAML2. This stuff isn't SAML spec compliant
for my $attr (qw[name display_name contact]) {
    has sprintf('org_%s', $attr) => ( is => 'ro', isa => 'Undef' );
}

has id => ( is => 'rw', isa => 'Str', required => 1 );

has interface => ( is => 'rw', isa => 'Zaaksysteem::Model::DB::Interface' );

=head1 SAML2::SP

This class wraps L<Net::SAML2::SP> for a bit nicer integration with
L<Zaaksysteem>.

=head1 Example usage

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        $my_interface_object
    );

    my $metadata = $sp->metadata;

=head1 Methods

=head2 new_from_interface

This methods wraps the logic of building a SP object from an interface
definition. Works like the normal C<new> function, except that it requires
an interface object be supplied as the first argument. Remaining parameters
are interpreted as a hash passed directly to C<new>.

=head3 interface

An L<Zaaksysteem::Backend::Sysin::Interface::Component> instance.

=cut

define_profile new_from_interface => (
    required => [qw[interface]],
    typed => {
        interface => 'Zaaksysteem::Model::DB::Interface'
    }
);

sub new_from_interface {
    my ($class, %params) = @_;

    my $interface = assert_profile(\%params)->valid->{ interface };
    my $schema = $interface->result_source->schema;

    my $cert = $schema->resultset('Filestore')->find($interface->jpath('$.sp_cert[0].id'));

    unless($cert) {
        throw('saml2/sp/certificate', 'Certificate configured for SP not found in filestore.');
    }

    return $class->new(
        id => $interface->jpath('$.sp_webservice'),
        url => $interface->jpath('$.sp_webservice'),
        interface => $interface,
        cert => $cert->get_path,
        cacert => ''
    );
}

1;

package Zaaksysteem::SAML2::Protocol::ArtifactResolve;

use Moose;

BEGIN { extends 'Net::SAML2::Protocol::ArtifactResolve'; }

use XML::Generator;

has xmlgen => ( is => 'ro', lazy => 1, default => sub {
    XML::Generator->new(':pretty');
});

sub as_xml {
    my ($self) = @_;

    my $saml  = ['saml'     => 'urn:oasis:names:tc:SAML:2.0:assertion'];
    my $samlp = ['samlp'    => 'urn:oasis:names:tc:SAML:2.0:protocol'];

    $self->xmlgen->ArtifactResolve(
        $samlp,

        {
            ID => $self->mangled_id,
            IssueInstant => $self->issue_instant,
            Destination => $self->destination,
            Version => '2.0'
        },

        $self->xmlgen->Issuer(
            $saml,
            $self->issuer,
        ),

        $self->xmlgen->Artifact(
            $samlp,
            $self->artifact,
        ),
    );
}

sub mangled_id {
    my $self = shift;

    return '_' . unpack 'H*', Crypt::OpenSSL::Random::random_pseudo_bytes(20);
}

1;

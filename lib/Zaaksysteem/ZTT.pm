package Zaaksysteem::ZTT;

use Moose;
use Scalar::Util;

use Scalar::Util qw(blessed);

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;
use Zaaksysteem::Constants;

use Zaaksysteem::ZTT::Element;
use Zaaksysteem::ZTT::Template;

=head1 Attributes

=head2 string_fetchers

This attribute contains all the stringfetchers the ZTT instance can use.

String fetchers are coderefs that reference functions that take as their first
argument a ZTT::Tag object, and return a ZTT::Element object containing the
value to be injected in the document.

=cut

has string_fetchers => (
    is => 'rw',
    isa => 'ArrayRef[CodeRef]',
    default => sub { [] }
);

has iterators => (
    is => 'ro',
    isa => 'HashRef[CodeRef]',
    default => sub { {} }
);

sub add_context {
    my $self = shift;
    my $context = shift;

    if (blessed $context) {
        unless($context->can('get_string_fetchers')) {
            throw('ztt/add_context', sprintf(
                "Provided a %s instance, but package does not provide string fetchers",
                blessed($context)
            ));
        }

        if ($context->can('get_context_iterators')) {
            my $iterators = $context->get_context_iterators;

            map { $self->add_iterator($_, $iterators->{ $_ }) } keys %{ $iterators };
        }

        return map
            { $self->add_string_fetcher($_) }
            $context->get_string_fetchers;
    }

    if (ref $context eq 'HASH') {
        return $self->add_string_fetcher(sub {
            my $tag = shift;
            my $value = $context->{ $tag->name };

            return unless $value;
            return Zaaksysteem::ZTT::Element->new(value => $value);
        });
    }

    if (ref $context eq 'ARRAY') {
        return $self->add_string_fetcher(sub {
            my $value = shift @{ $context };

            return unless $value;
            return Zaaksysteem::ZTT::Element->new(value => $value);
        });
    }

    throw('ztt/add_context', 'Unable to interpret context, not a blessed object, not a HASHREF or ARRAY, giving up man.');
}

sub fetch_string {
    my $self = shift;
    my $tag = shift;

    for my $fetcher (@{ $self->string_fetchers }) {
        my $interpolation = $fetcher->($tag);

        return $interpolation if $interpolation;
    }

    # Return an empty element by default, if it cannot be found by string fetchers
    # the tag should be removed from the document still
    return Zaaksysteem::ZTT::Element->new(value => '');
}

sub process_template {
    my $self = shift;
    my $template = shift;

    unless (eval { $template->isa('Zaaksysteem::ZTT::Template'); }) {
        $template = Zaaksysteem::ZTT::Template->new_from_thing($template);
    }

    my $recurse_limit = 10;

    while($recurse_limit--) {
        my @mods = $template->modifications;

        last unless scalar(@mods);

        for my $mod (@mods) {
            $self->apply($mod, $template);
        }
    }

    return $template;
}

sub apply {
    my ($self, $mod, $template) = @_;
    
    my %dispatch = (
        replace => sub {
            $template->replace($mod->selection, $self->fetch_string($mod->selection->tag), $self->string_fetchers);
        },

        iterate => sub {
            my $iterator_type = $mod->selection->iterate;

            unless (exists $self->iterators->{ $iterator_type }) {
                throw('ztt/template/iterator', 'Unable to find iterator for type ' . $iterator_type);
            }

            $template->iterate(
                $self->iterators->{ $iterator_type },
                $mod->selection
            );
        },

        iterate_inline => sub {
            my $iterator_type = $mod->selection->iterate;

            unless (exists $self->iterators->{ $iterator_type }) {
                throw('ztt/tempalte/inline_iterator', 'Unable to find iterator for type ' . $iterator_type);
            }

            $template->iterate_inline(
                $self->iterators->{ $iterator_type },
                $mod->selection
            );
        },
    );

    unless (exists $dispatch{ $mod->type }) {
        throw('ztt/template/apply', "Don't know how to handle modifications of '$mod->type' type.");
    }

    $dispatch{ $mod->type }->($mod, $template);
}

=head2 add_string_fetcher

With this method you can add additional string fetchers to the ZTT object.
The string fetchers are represented as an chronologically ordered array, first
in, first hit.

=cut

sub add_string_fetcher {
    my $self = shift;

    push @{ $self->string_fetchers }, shift;

    return $self;
}

sub add_iterator {
    my $self = shift;
    my $name = shift;
    my $items = shift;

    $self->iterators->{ $name } = $items;
}

1;

package Zaaksysteem::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:R4RHNOmRFCrCmyTFSe08tA

use Moose;
use Zaaksysteem::Profile;

use constant CLEAR_PER_REQUEST => [qw/
    users
    current_user
    ldap
    cache
    betrokkene_pager
    catalyst_config
    customer_instance
    current_user_ou_ancestry
/];

sub _clear_schema {
    my $self            = shift;

    for my $attr (@{ CLEAR_PER_REQUEST() }) {
        $self->$attr(undef);
    }
}

sub betrokkene_model {
    my $self            = shift;

    my $dbic            = $self->clone;

    my $stash           = {};

    if ($self->betrokkene_pager) {
        for my $key (keys %{ $self->betrokkene_pager }) {
            $stash->{ $key } = $self->betrokkene_pager->{ $key };
        }
    }

    $dbic->cache($self->cache);

    return Zaaksysteem::Betrokkene->new(
        dbic            => $dbic,
        stash           => $stash,
        log             => $self->log,
        config          => $self->catalyst_config,
        customer        => $self->customer_instance,
        ldap            => $self->ldap,
    );
}

# has 'betrokkene_model' => (
#     is => 'rw',
#     lazy => 1,
#     default => sub {
#         my $self            = shift;

#         my $dbic            = $self->clone;

#         my $stash           = {};

#         if ($self->betrokkene_pager) {
#             for my $key (keys %{ $self->betrokkene_pager }) {
#                 $stash->{ $key } = $self->betrokkene_pager->{ $key };
#             }
#         }

#         return Zaaksysteem::Betrokkene->new(
#             dbic            => $dbic,
#             stash           => $stash,
#             log             => $self->log,
#             config          => $self->catalyst_config,
#             customer        => $self->customer_instance,
#             ldap            => $self->ldap,
#         );
#     }
# );

has 'current_user_ou_ancestry'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;

        my $users       = $self->users;

        my $ou_id       = $users->get_user_ou_id($self->current_user);
        my $ou_entry    = $users->get_ou_by_id($ou_id);

        return $users->get_parent_ou_ids($ou_entry);
    }
);

has [qw/users current_user ldap/] => (
    'is'        => 'rw',
    'weak_ref'  => 1,
);

has [qw/cache/] => (
    'is'        => 'rw',
);

has 'log'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return Catalyst::Log->new }
);

has [qw/betrokkene_pager catalyst_config customer_instance/]  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return {} }
);

=head2 search_zql($zql)

Search the Zaaksysteem database using ZQL

=cut

define_profile search_zql => (
    required => [qw(zql)],
    typed => {
        zql => 'Zaaksysteem::Search::ZQL',
    },
);

sub search_zql {
    my $self = shift;
    my $zql = shift;
}

1;

package Zaaksysteem::Users;

use strict;
use warnings;

use Params::Profile;
use Data::Dumper;
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Net::LDAP;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw( LDAP_CONTROL_PAGED );

use Digest::SHA1;
use MIME::Base64;
use Encode qw(decode);

use Moose;
use namespace::autoclean;

use constant    DEFAULT_IMPORT_OU   => 'Import';
use constant    INTERNAL_USERS      => [qw/admin/];
use constant    OBJECT_CLASSES      => [qw/
    inetOrgPerson
    person
    posixAccount
    shadowAccount
    top
/];

use constant    OBJECT_CLASSES_POSIXGROUP   => [qw/
    posixGroup
    top
/];

use constant    OBJECT_CLASSES_OU   => [qw/
    organizationalUnit
    top
/];

use constant    LDAP_COLUMNS    => [qw/
    cn
    sn
    displayName
    givenName
    mail
    telephoneNumber

    homeDirectory
    userPassword
    uid
    uidNumber
    gidNumber
    loginShell
    initials
/];

use constant    LDAP_TRANSLATE_DEPARTMENTS  => {
    'Directie'                  => 'Directieteam',
    'Sociale zaken'             => 'Sociale_Zaken',
    'SoZa'                      => 'Sociale_Zaken',
    'Vergunningen & Handhaving' => 'V&H',
    'Ruimtelijke Inrichting'    => 'RI',
    'Ruimte'                    => 'RI',
    'Sociale Zaken'             => 'Sociale_Zaken',
    'Burgemeester & Wethouders' => 'College',
    'Soziale Zaken'             => 'Sociale_Zaken',
    'Planvoorbereiding en Groenbeheer'  => 'Planvoorbereiding en Groenbeheer',
    'Facilitaire zaken, Receptie en ICT beheer' => 'Facilitaire zaken, Receptie en ICT beheer',
    'DIV en communicatie en WenB werken'   => 'DIV en communicatie en WenB werken',
};

has 'customer' => (
    is       => 'rw',
    isa      => 'HashRef',
    required => 1,
);

has 'config' => (
    is       => 'rw',
    isa      => 'HashRef',
    required => 1,
);

has 'prod' => (
    is => 'rw',
);

has 'log' => (
    required => 1,
    is       => 'rw',
    isa      => 'Zaaksysteem::Log::CallingLogger',
);

has 'ldap' => (
    is => 'rw',
);

has '_ldaph' => (
    is => 'rw',
    isa => 'Net::LDAP',
);

has 'uidnumber' => (
    is => 'rw',
);

has 'schema' => (
    is       => 'rw',
    weak_ref => 1,
);

has 'interface_id' => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->schema->resultset('Interface')
          ->find_by_module_name('authldap');
    });

has 'components' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return [qw/sync_users sync_password/];
    },
);

has 'uidnumber_start' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return 55100;
    },
);

has 'uidnumber' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return shift->uidnumber_start;
    },
);

has 'ldapbase' => (
    is        => 'rw',
    lazy      => 1,
    required  => 1,
    isa       => 'Str',
    'default' => sub {
        return shift->customer->{start_config}{LDAP}{basedn};
    },
);

has 'import_ou' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return (shift->customer->{start_config}{LDAP}{import_ou}
              || DEFAULT_IMPORT_OU);
    },
);

has 'departments_map'       => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return (
            shift->customer->{start_config}{LDAP}{departments_mapping} ||
            LDAP_TRANSLATE_DEPARTMENTS
        );
    }
);

has 'ldap_system_roles'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $roles = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

        my $ldaproles = {};
        for my $role (keys %{ $roles }) {
            my $data    = $roles->{ $role };

            $ldaproles->{ $data->{ldapname} } = $data;
            $ldaproles->{ $data->{ldapname} }->{
                'internalname'
            } = $role;
        }

        return $ldaproles;
    }
);

# In case LDAP will be a performance bottleneck, this will tell us
# where it hurts the most
# my $total_elapsed = 0;
# my $_sendmesg = \&Net::LDAP::_sendmesg;

# *Net::LDAP::_sendmesg = sub {
#     my ($ldap, $mesg) = @_;
#     warn "sendmesg" . Dumper $mesg;

#     use Time::HiRes qw/tv_interval gettimeofday/;
#     my $t0 = [gettimeofday];

#     my $result = &$_sendmesg(@_);

#     my $elapsed = tv_interval($t0);
#     $total_elapsed += $elapsed;
#     warn "time taken: " . $elapsed;
#     warn "total elapsed: ". $total_elapsed;

#     return $result;
# };


sub ldaph {
    my $self    = shift;

    return $self->_ldaph if $self->_ldaph;

    my $ldap_server = $self->config->{LDAP}->{hostname};
    my $ldap_port   = $self->config->{LDAP}->{port} || 389;

    my $ldap = Net::LDAP->new(
        "$ldap_server:$ldap_port",
        version => 3
      )
      or throw(
        '/Zaaksysteem/Users/ldaph',
        sprintf(
            "Unable to connect to LDAP server %s:%d", $ldap_server, $ldap_port
        ));

    my $msg = $ldap->bind(
        $self->config->{LDAP}->{admin},
        password => $self->config->{LDAP}->{password});

    if ($msg->code) {
        throw(
            '/Zaaksysteem/Users/ldaph',
            sprintf("LDAP error: %s [%s]", $msg->error, $msg->code),
        );
    }

    return $self->_ldaph($ldap);
}

sub sync_users {
    my $self    = shift;
    my $users   = shift;
    $self->schema(shift);

    $self->log->info('Start synchronizing AD users');

    my $ldap            = $self->ldaph;
    my $local_users     = $self->_retrieve_local_users($ldap);

    my $compared_users = $self->_compare_users(
        local_users  => $local_users,
        remote_users => $users->{Bussum},
    );

    $self->_load_ldap(
        compared_users => $compared_users,
        local_users    => $local_users,
    );

    return 1;
}

=head2 _load_ldap

my $ok = _load_ldap(
    $ldap_handle,
    $compared_users,
    $local_users,
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item ldaphandle not used

=item compared_users [hashref]

=item local_users [hashref]

=back

=head3 RETURNS

True

=cut

define_profile _load_ldap => (
    required => [qw(compared_users local_users)],
    typed => {
        compared_users => 'HashRef',
        local_users    => 'HashRef',
    },
);

sub _load_ldap {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my ($compared_users, $local_users) = ($opts->{compared_users}, $opts->{local_users});

    if (keys %{$compared_users->{modify}}) {
        $self->log->debug('Users: NUM USERS: ' . scalar(keys %{ $compared_users->{modify} }));
        $self->_modify_ldap(
            users       => $compared_users->{modify},
            users_in_zs => $local_users,
        );
    }

    if (keys %{$compared_users->{delete}}) {
        my $force = defined $self->customer->{start_config}{LDAP}{skip_removal_after_sync} ? 0 : 1;
        $self->_delete_ldap(
            users => $compared_users->{delete},
            force => $force,
        );
    }

    if (keys %{$compared_users->{create}}) {
        $self->_create_ldap(users => $compared_users->{create});
    }

    return 1;
}

=head2 _create_ldap

my $ok = $self->_create_ldap_user(
    user    => $username,
    details => $details,
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item user

=item details

=back

=head3 RETURNS

True

=cut

define_profile _create_ldap_user => (
    required => [qw(username details)],
    typed    => {
        username => 'Str',
        details  => 'HashRef',
    },
);

sub _create_ldap_user {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;
    my $ldap = $self->ldaph;

    my @must_exist = qw(cn userPassword displayName);
    if (grep { !defined $opts->{details}{$_} || !length($opts->{details}{$_}) } @must_exist) {
        throw('/Zaaksysteem/User/create_ldap_user', "Minimal LDAP data not found!");
    }

    my $ldap_departments    = $self->departments_map;
    my $departments         = {};

    ### Define DN, check for existence
    my $ou = $self->import_ou;

    my ($dn, $entity);
    if ($entity = $self->_get_user_entity_from_username($opts->{username})) {
        $dn = $entity->subject_id->dn;
    }

    if ($dn) {
        $dn =~ s/^cn=(.*?),//;
    }
    else {
        $dn = 'ou=' . $ou . ',' . $self->ldapbase;
    }

    if (!$entity && $opts->{details}{department}) {
        my $department = $opts->{details}{department};

        if ($ldap_departments->{$department}) {
            $department = $ldap_departments->{$department}
        }

        my $sh = $ldap->search(
            base   => $self->ldapbase,
            filter => sprintf("(&(objectClass=organizationalUnit)(ou=%s))", $department),
        );

        if ($sh->count) {
            $self->log->debug('- Found department: ' . $department);
            $dn = $sh->entry(0)->dn;
        }
        else {
            $self->log->debug("Department NOT found: \"$department\", falling back to Import");
        }
    }

    my %add;
    for my $column (keys %{$self->_return_clean_user($opts->{details})}) {
        $add{$column} = $opts->{details}{$column};
    }

    if ($opts->{details}{force_uidNumber}) {
        $add{uidNumber} = $add{gidNumber} = $opts->{details}{force_uidNumber};
    }
    else {
        $add{uidNumber} = $add{gidNumber} = $self->uidnumber($self->uidnumber + 1);
    }

    $dn = "cn=$opts->{username},$dn";
    $self->log->info('- ADD USER: ' . $dn);
    my $result = $ldap->add(
        $dn,
        attr => [
            %add,
            objectclass => OBJECT_CLASSES
        ],
    );

    if ($result->code) {
        {
            local $Data::Dumper::Terse      = 1;
            local $Data::Dumper::Indent     = 0;
            local $Data::Dumper::Varname    = "";
            local $Data::Dumper::Sparseseen = 0;
            local $Data::Dumper::Quotekeys  = 0;
            throw(
                '/Zaaksysteem/User/_create_ldap_user',
                sprintf(
                    "failed to add entry '%s': %s / %s",
                    $dn, $result->error, Dumper(\%add)));
        }
    }

    if ($entity) {
        $self->_update_properties_from_ldap_user($dn);
    }
    $self->_handle_role($ldap, $dn);
    return $result;

}

=head2 _create_ldap

my $ok = $self->_create_ldap(
    add_users => $users_to_add
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item ldaphandle not used

=item compared_users [hashref]

=item local_users [hashref]

=back

=head3 RETURNS

True

=cut

define_profile _create_ldap => (
    required => [qw(users)],
    typed => {
        users => 'HashRef',
    },
);

sub _create_ldap {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;
    my ($ldap, $compared_users) = ($self->ldaph, $opts->{users});

    for my $username (keys %{$compared_users}) {
        # The function dies in case of an LDAP error (handy for testing).
        # But we don't want to fail the complete sync because of it
        eval {
            $self->_create_ldap_user(
                username => $username,
                details  => $compared_users->{$username},
            );
        };
    }

}

sub _handle_role {
    my ($self, $ldap, $dn)  = @_;

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(cn=Behandelaar))",
    );

    return unless $sh->count;

    my $behandelaar = $sh->entry(0);

    $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(cn=Behandelaar)(memberUid=$dn))",
    );

    return 1 if $sh->count;

    $self->log->debug('- Roles: Adding ' . $dn . ' to Behandelaar');

    my $result = $ldap->modify(
        $behandelaar->dn,
        changes => [
            add     => [
                memberUid   => $dn
            ],
        ]
    );
    $result->code && $self->log->error("failed to add role: $dn : ", $result->error);
}

=head2 _delete_ldap

my $ok = $self->_delete_ldap(
    rm_users => $users_to_delete
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item compared_users [hashref]

=item local_users [hashref]

=back

=head3 RETURNS

True

=cut

define_profile _delete_ldap => (
    required => [qw(users)],
    optional => [qw(force)],
    typed => {
        users => 'HashRef',
        force => 'Bool',
    },
    defaults => {
        force => 0,
    },
);

sub _delete_ldap {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    for my $username (keys %{$opts->{users}}) {
        # The function dies in case of an LDAP error (handy for testing).
        # But we don't want to fail the complete sync because of it
        eval { 
            $self->_delete_ldap_user(
                username => $username,
                dn       => $opts->{users}{$username}{dn},
                force    => $opts->{force},
            );
        }

    }
}

define_profile _delete_ldap_user => (
    required => [qw(username dn)],
    optional => [qw(force)],
    typed    => {
        username => 'Str',
        dn       => 'Str',
        force    => 'Bool',
    },
    defaults => {
        force => 0,
    },
);

sub _delete_ldap_user {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $dn = $opts->{dn};

    if ($opts->{username} =~ /^admin$/i) {
        throw('/Zaaksysteem/Users/delete_ldap', "Refusing to delete admin user!");
    }

    if (!$opts->{force}) {
        return 0;
    }

    $self->log->debug("- REMOVE LDAP: $dn");
    my $result = $self->ldaph->delete($dn);
    if ($result->code) {
        throw(
            '/Zaaksysteem/User/_create_ldap_user',
            sprintf(
                "Failed to remote user '%s': %s / %s",
                $opts->{username}, $dn, $result->error
            ));
    }
    $self->_update_properties_from_ldap_user($dn);
    return 1;
}

sub _return_clean_user {
    my ($self, $userdata) = @_;

    my %rv;
    my @columns = @{LDAP_COLUMNS()};
    for my $column (@columns) {
        next unless $userdata->{$column};

        if ( $column eq 'telephoneNumber'
            && length($userdata->{$column})
            && (   $userdata->{$column} =~ /^\s+$/
                || $userdata->{$column} !~ /^[\+0-9\s-]+$/)
          ) {
            next;
        }
        $rv{$column} = $userdata->{$column};
    }
    return \%rv if (keys %rv);
    throw('/Zaaksysteem/User/clean_user', "No userdata found!");
}

=head2 _get_user_entity_from_username

my $ue = $self->_get_user_entity_from_username($username);

Get the UserEntity for the user

=head3 ARGUMENTS

=over

=item username

=back

=head3 RETURNS

=cut

sub _get_user_entity_from_username {
    my ($self, $username) = @_;

    my $users = $self->schema->resultset('UserEntity')->search({
        source_interface_id => $self->interface_id->id,
        source_identifier   => $username,
    });

    return $users->first;
}

sub _update_properties_from_ldap_user {
    my $self                = shift;
    my $ldap_user           = shift;
    my $dn;

    if (!ref($ldap_user)) {
        $dn         = $ldap_user;
        $ldap_user  = $self->find($ldap_user);
    } else {
        $dn         = $ldap_user->dn;
    }

    if (!$ldap_user) {
        my ($username)  = $dn =~ /^cn=(.*?),/;
        ### User is probably deleted, set as deleted in LDAP
        my $users = $self->schema->resultset('UserEntity')->search({
                source_interface_id => $self->interface_id->id,
                source_identifier   => $username,
                date_deleted        => undef,
            },
        );

        while (my $user = $users->next) {
            $user->date_deleted(DateTime->now());
            $user->update;
        }

        return;
    }

    $self->schema->resultset('Subject')->update_or_create_from_ldap({
            interface_id => $self->interface_id->id,
            ldap_user    => $ldap_user,
        },
    );
}

define_profile _modify_ldap => (
    required => [qw(users users_in_zs)],
    typed    => {
        users       => 'HashRef',
        users_is_zs => 'HashRef',
    },
);

sub _modify_ldap {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;
    for my $username (keys %{$opts->{users}}) {
        # The function dies in case of an LDAP error (handy for testing).
        # But we don't want to fail the complete sync because of it
        eval {
            $self->_modify_ldap_user(
                username        => $username,
                details         => $opts->{users}{$username},
                current_details => $opts->{users_in_zs}{$username},
            );
        };

        if ($@) {
            $self->log->error('User modify error for ' . $username . ': ' . $@);
        }
    }

}

define_profile _modify_ldap_user => (
    required => [qw(username details current_details)],
    typed    => {
        username        => 'Str',
        details         => 'HashRef',
        current_details => 'HashRef',
    },
);

sub _modify_ldap_user {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $username = $opts->{username};
    my $dn       = $opts->{current_details}{dn};

    ### Check for Temp as ou, then check the department
    my ($ou) = $dn =~ /ou=(.*?),/;

    if ($ou eq 'Temp' && $opts->{details}{department}) {
        $self->log->info(
'Found user in Temp, try to create it in correct OU by removing this user and creating a new one'
        );

        $self->_delete_ldap_user(
            username => $username,
            dn       => $dn,
            force    => 1,
        );

        $opts->{details}{force_uidNumber} = $opts->{current_details}{uidNumber};

        my $result = $self->_create_ldap_user(
            username => $username,
            details  => $opts->{details},
        );
        return $result;
    }

    my %modify;
    for my $column (keys %{$self->_return_clean_user($opts->{details})}) {
        next if ($column =~ m/^(?:(?:gid|uid)Number|dn)$/);

        my $changetype = 'add';
        if (defined($opts->{current_details}{$column})) {
            # We need to compare it better because Pi\x{f6} is the same as decode('UTF-8', Piö)
            if (decode('UTF-8', $opts->{current_details}{$column}) eq $opts->{details}{$column}) {
                next;
            }
            $changetype = 'modify';
        }
        push(@{$modify{$changetype}}, $column => $opts->{details}{$column});
    }

    if (!keys %modify) {
        return undef;
    }

    my $dmod;
    {
        local $Data::Dumper::Terse      = 1;
        local $Data::Dumper::Indent     = 0;
        local $Data::Dumper::Varname    = "";
        local $Data::Dumper::Sparseseen = 0;
        local $Data::Dumper::Quotekeys  = 0;
        $dmod = Dumper \%modify;
    }

    my $result = $self->ldaph->modify($dn,
        changes => [
            replace => $modify{modify},
            add     => $modify{add},
        ]
    );

    if ($result->code) {
        throw(
            '/Zaaksysteem/User/_modify_ldap_user',
            sprintf(
                "Failed to modify entry '%s': %s / %s",
                $dn, $result->error, $dmod)
        );
    }

    if ($self->_get_user_entity_from_username($username)) {
        $self->_update_properties_from_ldap_user($dn);
    }
    $self->_handle_role($self->ldaph, $dn);

    $self->log->info(sprintf("Modified user: '%s', because: %s", $dn, $dmod));
    return $result;
}

=head2 _compare_users(@args);

my $res = $self->_compare_users(
    ldap_handle,
    local_users,
    users,
);

Compares users with something ... from Bussum.

=head3 ARGUMENTS

=over

=item $ldap_handle [undef, not used!]

=item $local_users

=item $users

=back

=head3 RETURNS

{
    'modify'    => {},
    'create'    => {},
    'delete'    => {},
};

=cut

define_profile _compare_users => (
    required => [qw(local_users remote_users)],
    #typed => {
    #    local_users  => 'HashRef',
    #    remote_users => 'HashRef',
    #},
);

sub _compare_users {
    my $self        = shift;
    my $opts        = assert_profile({@_})->valid;
    my $local_users = $opts->{local_users};
    my $users       = $opts->{remote_users};

    my $rv = {
        'modify'    => {},
        'create'    => {},
        'delete'    => {},
    };

    for my $username (keys %{$users}) {
        $username       = lc($username);
        my $userdata    = $users->{$username};

        ### Required
        next unless $userdata->{sn};

        $userdata->{homeDirectory} = '/home/' . $username;
        $userdata->{loginShell}    = '/bin/bash';
        $userdata->{uid}           = $username;
        $userdata->{initials}      = $userdata->{givenName} || '';
        $userdata->{initials}      =~ s/( ?[a-zA-Z])\w+/$1./g;

        if (
            defined($local_users->{$username}) &&
            $local_users->{$username}->{userPassword} &&
            !$userdata->{userPassword}
        ) {
            $userdata->{userPassword} = $local_users->{$username}->{userPassword};
        }

        if (!$local_users->{$username}) {
            $rv->{create}->{$username}  = $userdata;
            next;
        }

        ### Edit
        $rv->{modify}->{$username}      = $userdata;
    }

    for my $username (keys %{$local_users}) {
        if (!$users->{$username}) {
            $rv->{delete}{$username}  = $local_users->{$username};
        }
    }

    return $rv;
}

=head2 _retreive_local_users($ldap_handle)

Retreives local users from LDAP.

=head3 ARGUMENTS

=over

=item LDAP handle

=back

=head3 RETURNS

An hashref with user information:

{
    'admin' => {
        'cn'          => 'admin',
        'displayName' => 'A. Admin',
        'dn' => 'cn=admin,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '50001',
        'givenName'       => 'Anton',
        'homeDirectory'   => '/home/admin',
        'initials'        => 'A',
        'loginShell'      => '/nologin',
        'mail'            => 'demo-gebruiker@zaaksysteem.nl',
        'sn'              => 'Admin',
        'telephoneNumber' => '0612345678',
        'uid'             => 'admin',
        'uidNumber'       => '500001',
        'userPassword'    => '{SSHA}iwbtUYAf4wwRPgUW221L6BGrUrB4iWP+CgPBRA=='
    },
    'gebruiker' => {
        'cn'          => 'gebruiker',
        'displayName' => 'G. Bruiker',
        'dn' => 'cn=gebruiker,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '55101',
        'givenName'       => 'Bruiker',
        'homeDirectory'   => '/home/gebruiker',
        'initials'        => 'G.',
        'loginShell'      => '/nologin',
        'mail'            => 'demo-gebruiker@zaaksysteem.nl',
        'sn'              => 'Bruiker',
        'telephoneNumber' => '0612345678',
        'uid'             => 'Ge',
        'uidNumber'       => '55101',
        'userPassword'    => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA=='
    },
};

=cut

sub _retrieve_local_users {
    my $self    = shift;
    my $ldap    = $self->ldaph;

    $self->log->info('- Retrieve all users for comparison');

    my $page = Net::LDAP::Control::Paged->new(size => 500);
    my $cookie;

    my $local_users = {};
    my @entries;
    while (1) {
        my $msg = $ldap->search(
            base    => $self->ldapbase,
            filter  => "(&(objectClass=posixAccount))",
            control => [$page],
        );

        if ($msg->code) {
            throw(
                '/Zaaksysteem/Users/rlu',
                sprintf("LDAP error: %s [%s]", $msg->error, $msg->code),
            );
        }
        push(@entries, $msg->entries());

        my ($resp) = $msg->control(LDAP_CONTROL_PAGED) or last;
        $cookie = $resp->cookie or last;

        # Paging Control
        $page->cookie($cookie);
    }

    if (!@entries) {
        return $local_users;
    }

    my @columns = @{LDAP_COLUMNS()};
    my $uidNumber   = 0;
    for my $entry (@entries) {
        my $rv = {};

        foreach (@columns) {
            if ($entry->exists($_)) {
                $rv->{$_} = $entry->get_value($_)
            }
        }
        $rv->{dn}   = $entry->dn;

        my $uid = $entry->get_value('uidNumber');
        if ($uid > $uidNumber) {
            $uidNumber = $uid
        }

        $local_users->{ lc($entry->get_value('cn')) } = $rv;
    }

    if ($uidNumber > $self->uidnumber_start) {
        $self->uidnumber($uidNumber);
    }

    return $local_users;
}

sub sync_user {
    my $self    = shift;
}

sub get_all_roles {
    my $self    = shift;
    my $ldap    = $self->ldaph;
    my $rv      = [];

    my $sh      = $ldap->search(
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup))"
    );

    return $rv unless $sh->count;

    for my $entry ($sh->entries) {
        my ($ou)    = $entry->dn =~ /ou=(.*?),/;

        my $role    = {
            dn  => $entry->dn,
            cn  => $entry->get_value('cn'),
        };

        $role->{ou} = $ou if $ou;

        push(@$rv, $role);
    }

    return $rv;
}

# my $tree = [
#   {
#       name        => Username || Afdeling
#       type        => PosixAccount
#       entry       => {}
#       children    => [
#           {
#               name        => Username || Afdeling,
#               entry       => {},
#           },
#           {
#               name        => Username || Afdeling,
#               entry       => {},
#               children    => blabla
#           }
#       ]
#   }
# ];

sub move_to_ou {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts && $opts->{entry_dn} && $opts->{ou_dn});

    my $username    = $opts->{entry_dn};
    my $ou          = $opts->{ou_dn};

    my $ldap    = $self->ldaph;

    my $sh      = $ldap->search( # perform a search
        base   => $username,
        scope   => 'base',
        filter => '(objectClass=*)'
    );

    return unless $sh->count;

    ### Get roles
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(memberUid=$username))",
    );
    my @user_roles;
    if ($roles->count) {
        for my $role ($roles->entries) {
            push(@user_roles, $role->dn);
        }
    }

    my $newdn   = $username;
    $newdn      =~ s/(cn=.*?),.*/$1,$ou/;

    my $userpart = $username;
    $userpart   =~ s/(cn=.*?),.*/$1/;

    my $result = $ldap->moddn(
        dn              => $username,
        newsuperior     => $ou,
        newrdn          => $userpart,
    );

    $result->code && $self->log->error("failed to user to ou: " .
        $username . " : ", $result->error) && return;

    for my $user_role (@user_roles) {
        $result = $ldap->modify(
            $user_role,
            add  => {
                memberUid   => $newdn
            }
        );

        $result->code && $self->log->error("failed to add role to: " .
            $newdn . " : ", $result->error);

        $result = $ldap->modify(
            $user_role,
            delete  => {
                memberUid   => $username,
            }
        );

        $result->code && $self->log->error("failed to remove role from: " .
            $username . " : ", $result->error);
    }

    return $newdn;
}

sub delete_role_from {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts && $opts->{entry_dn} && $opts->{role_dn});

    my $username    = $opts->{entry_dn};
    my $role        = $opts->{role_dn};

    my $ldap    = $self->ldaph;

    my $roles   = $ldap->search( # perform a search
        base    => $role,
        scope   => 'base',
        filter => '(objectClass=*)'
    );

    unless ($roles->count) {
        $self->log->error(
            'Cannot remove role "'
            . $role .'"'
            . ', role not found'
        );

        return;
    }

    $self->log->info('Search for ' . $role . ': '. $roles->count);

    my $result = $ldap->modify(
        $role,
        delete  => {
            memberUid   => $username,
        }
    );

    $result->code && $self->log->error("failed to remove memberUid: " .
        $username . " from role: " .
        $role . ' : ' . $result->error) && return;

    return 1;
}
sub add_role_to {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts && $opts->{entry_dn} && $opts->{role_dn});

    my $username    = $opts->{entry_dn};
    my $role        = $opts->{role_dn};

    my $ldap    = $self->ldaph;

    my $sh      = $ldap->search( # perform a search
        base   => $username,
        scope   => 'base',
        filter => '(objectClass=*)'
    );
    return unless $sh->count;

    ### First, remove all memberships
    my $roles   = $ldap->search( # perform a search
        base    => $role,
        scope   => 'base',
        filter => '(objectClass=*)'
    );

    return unless $roles->count;
    warn('Search for ' . $role . ': '. $roles->count);

    my $result = $ldap->modify(
        $role,
        add  => {
            memberUid   => $username,
        }
    );

    $result->code && $self->log->error("failed to add memberUid: " .
        $username . " : ", $result->error) && return;

    return 1;
}


=head2 change_password

change the password on the found user

=cut

sub change_password {
    my ($self, $opts) = @_;

    die 'Invalid options' unless $opts->{username} && $opts->{new_password};


    my $ldap = $self->ldaph;

    my @entries = $ldap->search(
        base    => $self->ldapbase,
        filter  => '(&(objectClass=posixAccount)(cn=' . $opts->{username} . '))',
    )->entries;

    die "user not found (or multiple - that'd be worse)" unless @entries == 1;

    my ($entry) = @entries;

    my $result = $ldap->modify(
        $entry,
        replace  => {
            userPassword => $self->encrypt_password($opts->{new_password})
        }
    );

    # result code = 0 would be good news
    return $result && !$result->code;
}


sub encrypt_password {
    my ($self, $password) = @_;

    my $salt = join '', ('a'..'z')[rand 26,rand 26,rand 26,rand 26,rand 26,rand 26,rand 26,rand 26];

    my $ctx = Digest::SHA1->new;

    $ctx->add($password);
    $ctx->add($salt);

    return '{SSHA}' . encode_base64($ctx->digest . $salt, '');
}



sub get_list_of_entries {
    my $self    = shift;
    my $opts    = shift || {};

    my $ldap    = $self->ldaph;
    my $rv      = [];

    my @objectClass = (
        $opts->{objectClass}
            ? @{ $opts->{objectClass } }
            : ('posixAccount','organizationalUnit','posixGroup')
        );

    my $filter = '(|(objectClass='
            . join(')(objectClass=', @objectClass)
            . '))';

    my $sh      = $ldap->search( # perform a search
        base        => $self->ldapbase,
        scope       => 'sub',
        filter      => $filter
    );

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    my @results;
    for my $entry (@entries) {
        my $layout = $self->_load_tree_entry($entry);

        push(@results, $layout);
    }

    return \@results;
}

sub get_tree_view {
    my $self    = shift;
    my $opts    = shift || {};
    my $parent  = shift;
    my $ldap    = $self->ldaph;
    my $rv      = [];

    my @objectClass = (
        $opts->{objectClass}
            ? @{ $opts->{objectClass } }
            : ('organization', 'posixAccount','organizationalUnit','posixGroup')
        );

    my $filter = '(|(objectClass='
            . join(')(objectClass=', @objectClass)
            . '))';

    my $sh      = $ldap->search( # perform a search
        base        => ($parent || $self->ldapbase),
        scope       => ($parent ? 'one' : 'base'),
        filter      => $filter
    );

    #$self->log->debug('COUNT: ' . $sh->count . ' / ' . ($parent ||
    #        $self->ldapbase) . ' / ' . $filter
    #);

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    my @results;
    for my $entry (@entries) {
        my $layout = $self->_load_tree_entry($entry, $opts);

        push(@results, $layout);
    }

    my @sorted_results = sort {
        $a->{type} cmp $b->{type} ||
        lc($a->{name}) cmp lc($b->{name})
    } @results;

    return \@sorted_results;
}

sub get_array_of_roles {
    my $self        = shift;
    my $opts        = shift;

    my $ldap    = $self->ldaph;

    my @results;

    if ($opts->{parent_ou}) {
        my $filter = '(&(objectClass=organizationalUnit)'
            . '(l=' . $opts->{parent_ou} . '))';

        my $sh      = $ldap->search( # perform a search
            base        => $self->ldapbase,
            scope       => 'sub',
            filter      => $filter
        );

        if ($sh->count) {
            my $entry = $sh->entry(0);

            my $roles = $self->get_tree_view(
                {
                    'objectClass'   => ['posixGroup','organizationalUnit']
                },
                $entry->dn
            );

            $roles = [
                grep {
                    $_->{type} eq 'posixGroup'
                } @{ $roles }
            ];

            push(@results, @{ $roles });

            if (scalar(@results)) {
                push(@results, 'split');
            }
        }
    }

    # allow caching, with the new Angular widgets all the roles are prefetched
    # causing a delay > 10sec. This brings it down quite a bit.
    my $primary_roles = $opts->{cache} ?
        ($opts->{cache}->{primary_roles} ||= $self->primary_roles) :
        $self->primary_roles;

    return [@results, @$primary_roles];
}


sub primary_roles {
    my $self = shift;

    my $primary_roles = $self->get_tree_view({
        objectClass => ['organization', 'posixGroup','organizationalUnit']
    }, $self->ldapbase);

    return [grep {$_->{type} eq 'posixGroup'} @{ $primary_roles }];
}


sub _load_tree_entry {
    my ($self, $entry, $opts) = @_;

    my $layout = {
        name    => '',
        entry   => {},
        dn      => $entry->dn,
        has_children => ($self->has_children($entry->dn) || 0),
        system  => 0
    };

    unless ($opts->{counter}) {
        $opts->{counter} = 0;
    }

    $layout->{counter}      = ++$opts->{counter};

    my @object_classes = $entry->get_value('objectClass');
    if (grep { $_ eq 'organization' } @object_classes) {
        $layout->{entry}        = $self->get_organization($entry);
        $layout->{name}         = $layout->{entry}->{o};
        $layout->{type}         = 'organization';
        $layout->{internal_id}  = 0;
        $layout->{children}     = $self->get_tree_view($opts, $entry->dn);
    } elsif (grep { $_ eq 'posixAccount' } @object_classes) {
        $layout->{entry}        = $self->get_medewerker($entry);
        $layout->{name}         = $layout->{entry}->{cn};
        $layout->{type}         = 'posixAccount';
        $layout->{internal_id}  = $layout->{entry}->{uidNumber};
        $layout->{children}     = [];
    } elsif (grep { $_ eq 'organizationalUnit' } @object_classes) {
        $layout->{entry}        = $self->get_organisational_unit($entry);
        $layout->{name}         = $layout->{entry}->{ou};
        $layout->{type}         = 'organizationalUnit';
        $layout->{children}     = $self->get_tree_view($opts, $entry->dn);
        $layout->{internal_id}  = $layout->{entry}->{l};
    } elsif (grep { $_ eq 'posixGroup' } @object_classes) {
        $layout->{entry}        = $self->get_posix_group($entry);
        $layout->{name}         = $layout->{entry}->{cn};
        $layout->{type}         = 'posixGroup';
        $layout->{members}      = $layout->{entry}->{memberUid};
        $layout->{has_children} = $self->has_children($entry->dn);
        $layout->{internal_id}  = $layout->{entry}->{gidNumber};
        $layout->{children}     = [];
    }

    if ($layout->{type} eq 'posixGroup') {
        #$self->log->debug(
        #    'dn: ' . $entry->dn . ' / ' . $layout->{name}
        #);
    }

    if (
        $layout->{type} eq 'posixGroup' &&
        $entry->dn !~ /ou/ &&
        $self->ldap_system_roles->{ $layout->{name} }
    ) {
        $layout->{system} = 1;
    }

    return $layout;
}

sub add_role {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts->{role});

    die('Invalid role name') unless $opts->{role} =~ /^[\w\d\s-]+$/;

    warn('Options: ' . Dumper($opts));

    my $ldap    = $self->ldaph;
    my $dn      = 'cn=' . $opts->{role} . ',' . (
        $opts->{parent}
            ? $opts->{parent}
            : $self->ldapbase
    );


    ### Role exists?
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(&(objectClass=posixGroup))"
    );

    return if $sh->count;

    my $newid   = $self->_retrieve_new_id(
        {
            objectClass => 'posixGroup',
            id          => 'gidNumber',
        }
    );

    my $result = $ldap->add(
        $dn,
        attr        => [
            cn          => $opts->{role},
            gidNumber   => $newid,
            description => $opts->{role},
            objectclass => OBJECT_CLASSES_POSIXGROUP
        ],
    );

    $result->code && $self->log->error("failed to add role: " .
        $dn . " : ", $result->error) && return;

    $self->log->error("Succesful added role: " .  $dn);

    return 1;
}

sub has_children {
    my ($self, $dn) = @_;
    my $ldap    = $self->ldaph;

    ### Check if there are entries below this object
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'one',
        filter  => "(&(objectClass=*))"
    );

    return unless $sh->count;

    return $sh->count;
}

sub delete_entry {
    my ($self, $dn) = @_;

    die('No dn given') unless $dn;

    $self->log->info('Trying to delete entry: ' . $dn);

    my $ldap    = $self->ldaph;

    return if $self->has_children($dn);

    $self->log->info('No children found, safe to delete: ' . $dn);

    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(&(objectClass=*))"
    );

    return unless $sh->count;

    my $result = $self->ldaph->delete($dn);

    $result->code && $self->log->error("failed to remove entry: $dn : ",
        $result->error) && return;

    return 1;
}

sub add_ou {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts->{ou});

    die('Invalid ou name') unless $opts->{ou} =~ /^[\w\d\s-]+$/;

    warn('Options: ' . Dumper($opts));

    my $ldap    = $self->ldaph;
    my $dn      = 'ou=' . $opts->{ou} . ',' . (
        $opts->{parent}
            ? $opts->{parent}
            : $self->ldapbase
    );


    ### ou exists?
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(&(objectClass=organizationalUnit))"
    );

    return if $sh->count;

    my $newid   = $self->_retrieve_new_id(
        {
            objectClass => 'organizationalUnit',
            id          => 'l',
        }
    );

    my $result = $ldap->add(
        $dn,
        attr        => [
            ou          => $opts->{ou},
            l           => $newid,
            description => $opts->{ou},
            objectclass => OBJECT_CLASSES_OU
        ],
    );

    $result->code && $self->log->error("failed to add ou: " .
        $dn . " : ", $result->error) && return;

    $self->log->error("Succesful added ou: " .  $dn);

    return 1;
}

sub _retrieve_new_id {
    my ($self, $opts) = @_;

    die('Invalid options') unless (
        $opts &&
        $opts->{objectClass} &&
        $opts->{id}
    );

    my $ldap    = $self->ldaph;

    ### Check for new id
    my $sh         = $ldap->search( # perform a search
        base    => $self->ldapbase,
        scope   => 'sub',
        filter  => "(&(objectClass=" . $opts->{objectClass} . "))"
    );

    my @entries = $sh->entries;

    my $maxid = 0;
    for my $entry (@entries) {
        if ($entry->get_value($opts->{id}) > $maxid) {
            $maxid = $entry->get_value($opts->{id});
        }
    }

    return ($maxid + 1);
}

sub get_all_medewerkers {
    my $self    = shift;
    my $ldap    = $self->ldaph;
    my $rv      = [];

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixAccount))"
    );

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    for my $entry (@entries) {
        my $mw = $self->get_medewerker($entry);

        push(@{ $rv }, $mw);
    }

    return $rv;
}

sub get_organization {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    return {
        o   => $entry->get_value('o')
    };
}

sub get_organisational_unit {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    my $ou      = {};
    my $dn      = $entry->dn;

    $ou->{ $_ } = $entry->get_value($_) for
        qw/ou description l/;

    return $ou;
}

sub get_posix_group {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    my $group      = {};
    my $dn      = $entry->dn;

    $group->{ $_ } = $entry->get_value($_) for
        qw/cn description gidNumber memberUid/;

    $group->{memberUid} = [ $entry->get_value('memberUid') ];

    return $group;
}

sub get_medewerker {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    my $mw      = {};
    my $dn      = $entry->dn;

    $mw->{ $_ } = $entry->get_value($_) for
        qw/cn displayName uid uidNumber/;

    ### Get OU
    my ($ou)    = $dn =~ /ou=(.*?),/;

    $mw->{ou}   = $ou;

    ### Get roles
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(memberUid=$dn))",
    );

    my %roles;
    if ($roles->count) {
        for my $role ($roles->entries) {
            $roles{$role->dn} = 1;
        }
    }

    $mw->{roles}    = \%roles;

    return $mw;
}

sub deploy_user_in_roles {
    my ($self, $username, $userroles) = @_;

    my $ldap    = $self->ldaph;

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixAccount)(cn=$username))"
    );

    return unless $sh->count;

    my $user    = $sh->entry(0);
    my $dn      = $user->dn;

    ### First, remove all memberships
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(memberUid=$dn))",
    );

    for my $role ($roles->entries) {
        my $result = $ldap->modify(
            $role->dn,
            delete  => {
                memberUid   => $user->dn,
            }
        );

        $result->code && $self->log->error("failed to delete memberUid: " .
            $user->dn . " : ", $result->error);
    }

    ### Add entries
    for my $role (@{ $userroles }) {
        my $result = $ldap->modify(
            $role,
            add  => {
                memberUid   => $user->dn,
            }
        );

        $result->code && $self->log->error("failed to add memberUid: " .
            $user->dn . " : ", $result->error);
    }
}

sub get_role_by_id {
    my ($self, $role_id) = @_;

    my $ldap    = $self->ldaph;
    my $roles   = $ldap->search(
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(gidNumber=$role_id))",
    );

    return unless $roles->count;

    return $roles->entry(0);
}

sub get_ou_by_id {
    my ($self, $ou_id) = @_;

    my $ldap    = $self->ldaph;
    my $ous   = $ldap->search(
        base   => $self->ldapbase,
        filter => "(&(objectClass=organizationalUnit)(l=$ou_id))",
    );

    return unless $ous->count;

    return $ous->entry(0);
}

sub find {
    my ($self, $dn) = @_;

    my $roles   = $self->ldaph->search(
        base    => $dn,
        scope   => 'base',
        filter  => "(objectClass=*)",
    );

    return unless $roles->count;
    return $roles->entry(0);
}


sub get_parent_ou_ids {
    my ($self, $ou_entry, $parent_ou_ids) = @_;

    # chop the first ou=.. part off, that should give us the parent ou.
    # 'ou=Sub vice management,ou=Vice management,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl'
    my ($parent_dn) = $ou_entry->dn =~ m|.*?,(.*)|;

    $parent_ou_ids ||= [];

    push @$parent_ou_ids, {
        ou_id => $ou_entry->get_value('l'),
        name  => $ou_entry->get_value('ou')
    };

    if ($parent_dn =~ m|^ou=|) {
        return $self->get_parent_ou_ids($self->find($parent_dn), $parent_ou_ids);
    }

    return $parent_ou_ids;
}


=head2 get_user_ou_id

Given a user's ldap entry, discover in which organisational unit
the user is placed.

=cut

sub get_user_ou_id {
    my ($self, $entry) = @_;

    # chop off anything that's not 'ou=' from the head
    # 'cn=gebruiker,ou=Sub vice management,ou=Vice management,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl'
    my ($user_part, $ou_dn) = $entry->dn =~ m|(.*?,)?(ou=.*)$|;

    my $ou_entry = $self->find($ou_dn);
    return $ou_entry->get_value('l');
}

__PACKAGE__->meta->make_immutable;



=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::OutlookMailParser;

use Moose;
use Data::Dumper;
use Email::Outlook::Message;
use MIME::Parser;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;


has 'tmpDir' => (
    'is'        => 'ro',
    'required'  => 1
);

has 'parts'    => (
    'is'        => 'rw',
    'default'	=> sub { [] }
);

has 'subject' => (
    'is'        => 'rw',
    'default'   => '(geen onderwerp)'
);

has 'recipient' => (
    'is'        => 'rw',
    'default'   => '-'
);

has 'sender' => (
    'is'        => 'rw',
    'default'   => '-'
);

has 'body'    => (
    'is'        => 'rw'
);

has 'attachments'    => (
    'is'        => 'rw',
    'default'   => sub { [] }
);

has 'bodyCandidates'    => (
    'is'        => 'rw',
    'default'   => sub { {} }
);

=head2 dissectEntity

i tried to rewrite this without the recursing parts but for some
reason the module seems to like it, maybe something like wantarray
this generates a list of parts in $self->parts

=cut

define_profile dissectEntity => (
    required    => [qw[entity]]
);
sub dissectEntity {
	my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $entity = $arguments->{entity};

    if (my $recipient = $entity->head->get('to')) {
        $self->recipient($recipient);
    }

    if (my $sender = $entity->head->get('from')) {
        $self->sender($sender);
    }

    if (my $subject = $entity->head->get('subject', 0)) {
        chomp $subject;
        $self->subject($subject);
    }
    my @parts = $entity->parts;

    if (@parts) {
		map { $self->dissectEntity({entity => $_}) } @parts;
    }
    else {
        my $path = $entity->bodyhandle->path;
        my ($extension) = $path =~ m|\.(\w+)$|;

        my $attachment = $entity->head->recommended_filename;

        my $part = {
            name => $attachment || $self->subject . ".$extension",
            path => $entity->bodyhandle->path,
            mime => $entity->head->mime_type
        };

        if ($attachment) {
            push $self->attachments, $part;
        } else {
            $self->bodyCandidates->{$extension} = $part;
        }
    }
}


=head2 parseMSG

Also called E621.

MSG is a binary format, this converts it to a text MIME format.

=cut 
define_profile parseMSG => (
    required    => [qw[path original_name]]
);
sub parseMSG {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $path = $arguments->{path};

    die "file $path does not exist" unless -e $path;

    my $original_name = $arguments->{original_name};

    my $mimeMessage;

    if ($original_name =~ m|\.msg$|i) {
        $mimeMessage = new Email::Outlook::Message($path)->to_email_mime->as_string;
    }
    elsif ($original_name =~ m|\.eml$|i) {
        open FILE, $path or die "couldn't open $path for reading: $!";
        $mimeMessage = join "", <FILE>;
        close FILE;
    }

    return unless $mimeMessage;

    return $self->parseMIME({mimeMessage => $mimeMessage});
}


=head2 parseMIME

MIME is a text format, extract parts like text and html representations,
attachments.

=cut

define_profile parseMIME => (
    required    => [qw[mimeMessage]]
);
sub parseMIME {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $mimeMessage = $arguments->{mimeMessage};

    my $parser = new MIME::Parser;
    $parser->extract_uuencode(1);
    $parser->output_under($self->tmpDir);
    my $entity = $parser->parse_data($mimeMessage);
 
    $self->dissectEntity({entity => $entity});

    # there can be different representations of the body.
    # select one and store it in $self->body
    return $self->body($self->selectBody());
}


sub selectBody {
    my ($self) = @_;

    if (my $textCandidate = $self->bodyCandidates->{txt}) {

        open my $input, "<", $textCandidate->{path} or die "file not found: $!";
        my $content = join "", <$input>;
        close $input;

        open my $output, ">", $textCandidate->{path} or die "file not found: $!";
        # upon a next overhaul, use $self->format.
        print $output "Van: " . $self->sender .
                    "Aan: " . $self->recipient .
                    "Onderwerp: " . $self->subject . "\n\n" . $content;
        close $output;

        return $textCandidate;
    }

    # no mail info in RTF yet -- we'd need to edit the rtf from perl,
    # bit out of scope and probably very rare edge case.
    return $self->bodyCandidates->{rtf} or die "could not find a suitable candidate";
}


define_profile format => (
    required => [qw/from to subject body/],
    optional => [qw/attachments/],
);
sub format {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $attachments = $params->{attachments} ?
        join "\n", map { $_->{filename} } @{ $params->{attachments} }:
        'geen';

    return "Van: " . $params->{from} . "\n" .
        "Aan: " . $params->{to} . "\n" .
        "Onderwerp: " . $params->{subject} . "\n\n" .
        "Bijlagen: ". $attachments . "\n\n" .
        $params->{body};
}

1;

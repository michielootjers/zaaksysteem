package Zaaksysteem::ObjectChartGenerator::Roles::Cases_per_department_per_month;

use Moose::Role;
use Data::Dumper;




=head2 search_months_with_cases

Get a list of months in which we have cases
this will simply yield 2013-01, 2013-02, 2013-03

=cut
sub search_months_with_cases {
    my ($self, $rs) = @_;

    # Get a list of months in which we have cases
    # this will simply yield 2013-01, 2013-02, 2013-03
    my $resultset = $rs->search({
            "CAST(index_hstore->'case.date_of_completion' AS timestamp)" => { '-not' => undef }
        }, {
        select => [
            {
                date_part => "'month', CAST(index_hstore->'case.date_of_completion' AS timestamp)",
                -as => 'month'
            },
            {
                date_part => "'year', CAST(index_hstore->'case.date_of_completion' AS timestamp)",
                -as => 'year',
            },
        ],
        group_by => [ 'year', 'month'],
        order_by => [ 'year', 'month']
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    return $resultset->all();
}


=head2 search_month

Filter a given resultset by a given month.

So, out of the given resultset, only give me the resultset for which the 
afhandeldatum is within month x in year y.

=cut
sub search_month {
    my ($self, $rs, $arguments) = @_;

    my $month = $arguments->{month} or die "need month";
    my $year  = $arguments->{year}  or die "need year";

    # could be combined to one query?
    my $min_rs = $self->resultset->search({}, {
        select => {
            min => "CAST(index_hstore->'case.date_of_completion' AS timestamp)",
        },
        "as" => 'min',
    });
    $min_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my $earliest_date_in_search_results = $min_rs->first->{min};

    my $max_rs = $self->resultset->search({}, {
        select => {
            max => "CAST(index_hstore->'case.date_of_completion' AS timestamp)",
        },
        "as" => 'max',
    });
    $max_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my $latest_date_in_search_results = $max_rs->first->{max};

    return $rs->search({
        "date_part('month', CAST(index_hstore->'case.date_of_completion' AS timestamp))" => $month,
        "date_part('year', CAST(index_hstore->'case.date_of_completion' AS timestamp))"  => $year,
        "CAST(index_hstore->'case.date_of_completion' AS timestamp)" => {
            '>=' => $earliest_date_in_search_results,
            "<=" => $latest_date_in_search_results
        }
    });
}



=head2 search_department_count_per_month

Given a resultset and a month, group by department.

So: Here's a resultset, first only get the results in month x of year y, 
then find all the different departments and give me a count for each.
 
Return a list like [
    { department_A: 34   },
    { department_B: 3434 },
    { department_c: 0    }
]

=cut
sub search_department_count_per_month {
    my ($self, $rs, $arguments) = @_;

    $rs = $self->search_month($rs, $arguments);

    my $department_rs = $rs->search({
        -and => [
            \[ "defined(index_hstore, 'case.route_ou')" ]
        ],
    }, {
        select => [
            "index_hstore->'case.route_ou'",
            {count => 'me.object_id'},
        ],
        as => [ 'route_ou', 'count' ],
        group_by => [ 1 ],
        order_by => [ 1 ]
    });

    $department_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    return $department_rs->all();
}

=head2 cases_per_department_per_month

Returns a chart profile
 
=cut
sub cases_per_department_per_month {
    my ($self) = @_;

    my @results = $self->search_months_with_cases($self->resultset);

    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my $categories         = [];
    my $hash_matrix        = {};
    my $unique_departments = {};

    # Loop through the months
    foreach my $result (@results) {

        my $month = $month_names[$result->{month}-1] . ' '. $result->{year};

        push @$categories, $month;

        my @department_results = $self->search_department_count_per_month(
            $self->resultset,
            $result
        );

        # foreach department, add the found count to a matrix
        # we will only know all the relevant departments at the end
        my $department_counts = {};
        foreach my $department_row (@department_results) {
            my $route_ou = $department_row->{route_ou};
            my $count = $department_row->{count};

            $department_counts->{$route_ou} = $count;

            $unique_departments->{$route_ou} += 1;
        }

        $hash_matrix->{$month} = $department_counts;
    }

    my $series = $self->unsparsify({
        unique_items => $unique_departments,
        hash_matrix => $hash_matrix,
        categories => $categories,
    });


    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => "Zaken per afdeling per maand"
        },
        xAxis => {
            categories => $categories
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Aantal zaken'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>{point.y:.f} zaken</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                pointPadding => 0.2,
                borderWidth => 0
            }
        },
        series => $series
    };

    return $profile;
}


1;

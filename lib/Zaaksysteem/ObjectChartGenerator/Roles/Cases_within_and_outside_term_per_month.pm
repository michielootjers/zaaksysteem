package Zaaksysteem::ObjectChartGenerator::Roles::Cases_within_and_outside_term_per_month;

use Moose::Role;
use Data::Dumper;


=head2 cases_within_and_outside_term_per_month

Generate chart profile

=cut
sub cases_within_and_outside_term_per_month {
    my ($self) = @_;

    my @results = $self->search_months_with_cases($self->resultset);

    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my $categories = [];
    my $within     = [];
    my $outside    = [];

    # Loop through the months
    foreach my $result (@results) {

        my $month = $month_names[$result->{month}-1] . ' '. $result->{year};

        push @$categories, $month;

        my $within_and_outside_rs = $self->search_month($self->resultset, $result);

        my $within_and_outside = $self->within_and_outside_term(
           $within_and_outside_rs
        );

        push @$within, $within_and_outside->{within};
        push @$outside, $within_and_outside->{outside};
    }

    my $series = [{
        name => 'Binnen',
        data => $within,
        color => 'green'
    }, {
        name => 'Buiten',
        data => $outside,
        color => 'red'
    }];

    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => "Zaken binnen/buiten termijn per maand (%)"
        },
        xAxis => {
            categories => $categories
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Percentage zaken'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
#            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
#                '<td style="padding:0"><b>{point.y:.f} zaken</b></td></tr>',
            pointFormat => '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.0f}%</b><br/>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                stacking => 'percent'
            }
        },
        series => $series
    };

    return $profile;
}


1;

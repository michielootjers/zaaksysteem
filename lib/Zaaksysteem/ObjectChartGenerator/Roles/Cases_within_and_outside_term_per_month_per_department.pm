package Zaaksysteem::ObjectChartGenerator::Roles::Cases_within_and_outside_term_per_month_per_department;

use Moose::Role;
use Data::Dumper;


sub search_within_and_outside {
    my ($self, $arguments) = @_;

    # month includes the year
    my $month    = $arguments->{month}    or die "need month";
    my $route_ou = $arguments->{route_ou};
    die "need route_ou" unless defined $arguments->{route_ou};

    # would this perform?
    my $within_and_outside_rs = $self->resultset->result_source->schema->resultset('ObjectData');

    $within_and_outside_rs = $self->search_month($within_and_outside_rs, $month);
    $within_and_outside_rs = $within_and_outside_rs->search(
        {
            "index_hstore->'case.route_ou'" => $route_ou
        }
    );

    return $self->within_and_outside_term(
        $within_and_outside_rs
    );
}


=head2 cases_within_and_outside_term_per_month_per_department

Generate chart profile

=cut
sub cases_within_and_outside_term_per_month_per_department {
    my ($self) = @_;

    my @results = $self->search_months_with_cases($self->resultset);

    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my $categories = [];
    # my $within     = [];
    # my $outside    = [];

    my $hash_matrix        = {};
    my $unique_departments = {};

    my $within_and_outside_table = {};

    # Loop through the months
    foreach my $result (@results) {

        my $month = $month_names[$result->{month}-1] . ' '. $result->{year};

        push @$categories, $month;

        my @department_results = $self->search_department_count_per_month(
            $self->resultset,
            $result
        );

        # foreach department, add the found count to a matrix
        # we will only know all the relevant departments at the end
        my $department_counts = {};
        foreach my $department_row (@department_results) {
            my $route_ou = $department_row->{route_ou};
            my $count = $department_row->{count};

            $unique_departments->{$route_ou} += 1;

            my $within_and_outside = $self->search_within_and_outside({
                month => $result,
                route_ou => $route_ou,
            });

            $department_counts->{$route_ou} = {
                y  => int($within_and_outside->{within} || 0),
                outside => int($within_and_outside->{outside} || 0),
            };

        }

        $hash_matrix->{$month} = $department_counts;

    }

    my $series = $self->unsparsify({
        unique_items => $unique_departments,
        hash_matrix => $hash_matrix,
        categories => $categories,
        y_objects => 1,
    });


    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => "Zaken binnen/buiten termijn per maand per afdeling (%)"
        },
        xAxis => {
            categories => $categories
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Percentage zaken afgehandeld binnen termijn'
            },
            max => 100
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>Binnen: {point.y:.f}%, Buiten: {point.outside}%</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                pointPadding => 0.2,
                borderWidth => 0
            }
        },
        series => $series
    };

    return $profile;
}


1;

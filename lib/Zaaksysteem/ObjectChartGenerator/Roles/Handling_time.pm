package Zaaksysteem::ObjectChartGenerator::Roles::Handling_time;

use Moose::Role;
use Data::Dumper;



=head2 within_and_outside_term

Calculate how many of the cases within the given resultset
where finished within the rqequired term.

=cut
sub within_and_outside_term {
    my ($self, $rs) = @_;

    my $row = $self->group_binnen_buiten_termijn($rs)->first()
        or return;

    my $binnen = $row->get_column('binnen');
    my $buiten = $row->get_column('buiten');
    my $total = $binnen + $buiten;

    ### Prevent division by zero
    if ($total) {
        $binnen = int(0.5 + (100* $binnen / $total));
        $buiten = int(0.5 + (100* $buiten / $total));
    } else {
        $binnen = 0;
        $buiten = 0;
    }

    return {
        'within'  => $binnen,
        'outside' => $buiten
    };
}

=head2 within_and_outside_term

Generate chart profile

=cut
sub handling_time {
    my ($self) = @_;

    my $result = $self->within_and_outside_term($self->resultset) 
        or return {};

    my $profile = {
        title => { 
            text => 'Binnen afhandeltermijn/Buiten afhandeltermijn'
        },
        plotOptions => {
            pie => {
                allowPointSelect => 1,
                cursor => 'pointer',
                dataLabels => {
                    enabled => 1,
                    color => '#000000',
                    connectorColor => '#000000',
                    format => '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        tooltip => {
            pointFormat => '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        series => [{
            type => 'pie',
            name => 'Afhandeling',
            data => [
                {name => 'Binnen', color => 'green', 'y' => $result->{within}},
                {name=> 'Buiten', color => 'red', 'y' => $result->{outside}},
            ]
        }]
    };

    return $profile;
}

sub group_binnen_buiten_termijn {
    my $self = shift;
    my $rs = shift;

    delete($rs->{attrs}->{'+as'});
    delete($rs->{attrs}->{'+select'});
    delete($rs->{attrs}->{'prefetch'});
    delete($rs->{attrs}->{'order_by'});

    ## Clean up attributes
    return $rs->search({}, {
        select      => [
            {
                sum => q{CASE WHEN (
                    defined(me.index_hstore, 'case.date_of_completion') AND
                    CAST(me.index_hstore->'case.date_of_completion' AS TIMESTAMP) < CAST(me.index_hstore->'case.date_target' AS TIMESTAMP)
                ) THEN 1 ELSE 0 END},
                -as => 'binnen',
            },
            {
                sum => q{CASE WHEN (
                    (
                        NOW() > CAST(me.index_hstore->'case.date_target' AS TIMESTAMP) AND NOT defined(me.index_hstore, 'case.date_of_completion')
                    ) OR CAST(me.index_hstore->'case.date_of_completion' AS TIMESTAMP) > CAST(me.index_hstore->'case.date_target' AS TIMESTAMP)
                ) THEN 1 ELSE 0 END},
                '-as' => 'buiten'
            },
        ],
        'as'      => [qw/binnen buiten/],
    });

    return $rs;
}

1;

package Zaaksysteem::StUF::NNP;

use Moose::Role;
use Zaaksysteem::Constants;

use constant ORGANIZATION_MAP                       => {
    'handelsRegisternummer'         => 'dossiernummer',
    'indicatieFaillisement'         => 'faillisement',
    'indicatieSurseanceVanBetaling' => 'surseance',
    'rechtsvorm'                    => 'rechtsvorm',
};

use constant ORGANIZATION_ADDRESS_MAP => {
    'huisnummer'            => 'huisnummer',
    'huisnummertoevoeging'  => 'huisnummertoevoeging',
    'postcode'              => 'postcode',
    'straatnaam'            => 'straatnaam',
    'woonplaatsnaam'        => 'woonplaats',
};

=head2 METHODS

=head2 get_params_for_organization

Gets a set of params for manipulating NNP (Bedrijf)

=cut

sub get_params_for_organization {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{NNP};

    for my $key (keys %{ ORGANIZATION_MAP() }) {
        next unless exists($object_params->{$key});

        $params->{ORGANIZATION_MAP->{$key}} = $object_params->{ $key };
    }

    if (exists($object_params->{extraElementen}->{handelsRegisterVolgnummer})) {
        $params->{subdossiernummer} = $object_params->{extraElementen}->{handelsRegisterVolgnummer};
    }

    if (exists($object_params->{extraElementen}->{vestigingsnummer})) {
        $params->{vestigingsnummer} = $object_params->{extraElementen}->{vestigingsnummer};
    }

    # Get correct naam of organisation
    $params->{handelsnaam} = $self->_get_handelsnaam($object_params);

    my $address_params  = $self->get_params_for_organization_adres;

    $params             = { %{ $params }, %{ $address_params } };

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_KVK_PROFILE
    )->valid;

    #warn(Data::Dumper::Dumper($valid_params));
    return $valid_params;
}

sub _get_handelsnaam {
    my $self                        = shift;
    my $object_params               = shift;

    ### Handelsnaam preferred, otherwise: zaaknaam.
    return $object_params->{handelsnaam} || $object_params->{zaaknaam};
}

=head2 get_params_for_natuurlijk_persoon_adres

Gets a set of params for manipulating the adres entry for a PRS.

=cut

sub get_params_for_organization_adres {
    my $self                        = shift;
    my ($params, $address)          = ({}, {});

    my $object_params               = $self->as_params->{NNP};

    return $params unless (
        exists($object_params->{NNPADRVBL}) ||
        exists($object_params->{NNPADRCOR})
    );

    my $address_prefix = '';
    if (exists($object_params->{NNPADRVBL})) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{NNPADRVBL} }
        );

        if ($error) {
            throw(
                'stuf/nnp/get_params_for_organization_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $address_prefix = 'vestiging_';
    } elsif (
        exists($object_params->{NNPADRCOR})
    ) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{NNPADRCOR} }
        );

        if ($error) {
            throw(
                'stuf/nnp/get_params_for_organization_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $address_prefix = 'correspondentie_';
    }

    return $params unless $address;

    for my $key (keys %{ ORGANIZATION_ADDRESS_MAP() }) {
        next unless exists($address->{ADR}->{$key});

        $params->{$address_prefix . ORGANIZATION_ADDRESS_MAP->{$key}} = $address->{ADR}->{ $key };
    }

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_KVK_PROFILE
    )->valid;

    return $valid_params;
}

1;
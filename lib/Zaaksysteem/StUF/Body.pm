package Zaaksysteem::StUF::Body;

use Moose;

use Zaaksysteem::StUF::Body::Field;
use Zaaksysteem::StUF::Body::Relationship;


has '_body_params'  => (
    is          => 'rw',
    isa         => 'HashRef',
);

has 'is_active'     => (
    is          => 'rw',
    isa         => 'Bool',
    lazy        => 1,
    default     => 1,
);

has 'sleutelGegevensbeheer'   => (
    is          => 'rw',
    predicate   => 'has_sleutelGegevensbeheer',
);

has 'soortEntiteit'   => (
    is          => 'rw',
    lazy        => 1,
    default     => 'F'
);

has 'verwerkingssoort'   => (
    is          => 'rw',
    predicate   => 'has_verwerkingssoort',
);

has 'extraElementen'    => (
    is          => 'rw',
    lazy        => 1,
    default     => sub { return {}; },
);

has '_object_extra_params' => (
    is          => 'rw',
    lazy        => 1,
    default     => sub { return []; },
);

has 'is_description' => (
    is          => 'rw',
);

has 'datumSluiting' => (
    is          => 'rw',
);

sub BUILD {
    my $self        = shift;

    $self->_load_body_params;
}

sub _load_body_params {
    my $self                = shift;

    return unless $self->_body_params;

    for my $value (@{ $self->_object_params }) {
        my $name        = $value->{name};

        unless (exists($self->_body_params->{ $name })) {
            ### Clear it
            my $clear   = 'clear_' . $name;
            $self->$clear;
            next;
        }

        $self->_load_value($name, $value);
    }

    if (
        exists($self->_body_params->{'extraElementen'})
    ) {
        my @extra_elementen;

        if ($self->_body_params->{'extraElementen'}->{seq_extraElement}) {
            push(
                @extra_elementen,
                @{ $self->_body_params->{'extraElementen'}->{seq_extraElement} }
            );
        } elsif ($self->_body_params->{'extraElementen'}->{extraElement}) {
            for my $element (@{ $self->_body_params->{'extraElementen'}->{extraElement} }) {
                push(
                    @extra_elementen,
                    { 'extraElement' => $element }
                );
            }
        }

        for my $elem (@extra_elementen) {
            ### Check special cases:
            my $name        = $elem->{extraElement}->{naam};

            my $value_as_field = Zaaksysteem::StUF::Body::Field->new(
                _value  => $elem->{extraElement},
                name    => $name,
            );

            next unless $value_as_field->exists;

            if (exists($self->extraElementen->{$name})) {
                if (!UNIVERSAL::isa($self->extraElementen->{$name}, 'ARRAY')) {
                    $self->extraElementen->{$name} = [
                        $self->extraElementen->{$name}
                    ];
                }

                push(
                    @{ $self->extraElementen->{$name} },
                    $value_as_field
                );
            } else {
                $self->extraElementen->{$name} = $value_as_field;
            }
        }
    }

    $self->sleutelGegevensbeheer($self->_body_params->{sleutelVerzendend});
    $self->verwerkingssoort($self->_body_params->{verwerkingssoort});
    $self->_manage_tijdvak_params();
}


sub _manage_tijdvak_params {
    my $self                = shift;


    if (exists($self->_body_params->{tijdvakGeldigheid})) {
        $self->is_active(
            $self->_check_active(
                $self->_body_params
                            ->{tijdvakGeldigheid}
                            ->{begindatumTijdvakGeldigheid},
                $self->_body_params
                            ->{tijdvakGeldigheid}
                            ->{einddatumTijdvakGeldigheid}
            )
        );
    }
}

sub _load_value {
    my $self                = shift;
    my ($name, $value)      = @_;


    ### Related object, dispatch
    if ($value->{related}) {
        return $self->_load_related_object(@_);
    }

    my $value_as_field = Zaaksysteem::StUF::Body::Field->new(
        _value  => $self->_body_params->{ $name },
        name    => $name,
    );

    ### Check special cases:
    if (!$value_as_field->exists) {
        my $clear   = 'clear_' . $name;
        $self->$clear;
        return;
    }

    # if (my $conv = $value->{convenient}) {
    #     $self->$conv($value_as_field);
    # }

    $self->$name($value_as_field);
}

sub _load_related_object {
    my $self                = shift;
    my ($name, $value)      = @_;

    $self->$name(
        Zaaksysteem::StUF::Body::Relationship->new(
            _value          => $self->_body_params->{ $name },
            name            => $name,
            definition      => $value,
        )
    );
}

sub _check_active {
    my $self                = shift;
    my ($newdate, $olddate) = @_;

    my $startdate   = Zaaksysteem::StUF::Body::Field->new(
        _value  => $newdate
    )->value;
    my $enddate     = Zaaksysteem::StUF::Body::Field->new(
        _value  => $olddate
    )->value;

    my $currentdate = DateTime->now->strftime('%Y%m%d');

    if ($startdate && $startdate =~ /^\d+$/ && $startdate <= $currentdate) {
        if (!$enddate || $enddate !~ /^\d+$/ || $enddate > $currentdate) {
            $self->is_active(1);
        }
    }
}

sub TO_STUF {
    my $self        = shift;
    my $rv          = {};

    for my $value (@{ $self->_object_params }) {
        my $name        = $value->{name};
        my $hasattr     = 'has_' . $name;

        next unless ($self->is_description || $self->$hasattr);

        if ($value->{related} && $self->$name) {
            $rv->{ $name } = $self->$name->TO_STUF;
            next;
        }

        if ($self->is_description) {
            # Make sure no-value-items are Field objects so they do not get discarded later on.
            if (!$self->$name && $value->{isa} eq 'Maybe[Zaaksysteem::StUF::Body::Field]') {
                $rv->{$name} = Zaaksysteem::StUF::Body::Field->new(
                    value    => $self->$name,
                    no_value => 1
                );
                next;
            }
            elsif (!$self->$name && $value->{isa} eq 'Maybe[Zaaksysteem::StUF::Body::Relationship]') {
                $rv->{$name} = Zaaksysteem::StUF::Body::Relationship->new({
                    is_description => $self->is_description,
                    no_value       => 1,
                    definition => {
                        related  => $value->{related},
                        name     => $value->{name},
                    },
                });
                next;
            }
        }

        $rv->{ $name }              = $self->$name;
    }

    $rv->{sleutelGegevensbeheer}    = $self->sleutelGegevensbeheer
        if ($self->has_sleutelGegevensbeheer);

    $rv->{verwerkingssoort}         = $self->verwerkingssoort
        if ($self->has_verwerkingssoort);

    # $rv->{soortEntiteit}            = 'F';

    return $rv;
}

sub _convert_to_plain_value {
    my $self            = shift;
    my $value           = shift;

    if (
        UNIVERSAL::isa($value, 'Zaaksysteem::StUF::Body::Field') &&
        $value->value
    ) {
        return $value->value;
    } elsif (
        UNIVERSAL::isa($value, 'Zaaksysteem::StUF::Body::Relationship') &&
        $value->value
    ) {
        #warn('Fixing: ' . $value->name . "\n" . Data::Dumper::Dumper($value));

        return $value->as_params;
    }

    return undef;
}

sub as_params {
    my $self            = shift;
    my $rv              = {};

    for my $obj_value (@{ $self->_object_params }) {
        my $name        = $obj_value->{name};
        my $hasattr     = 'has_' . $name;
        my $value       = $self->$name;

        next unless $self->$hasattr;

        $rv->{$name}     = $self->_convert_to_plain_value($value);

        if (my $conv = $obj_value->{convenient}) {
            $rv->{$conv} = $rv->{$name};
        }
    }

    if (scalar( keys %{ $self->extraElementen })) {
        my $plain_values = {};
        for my $obj_key (keys %{ $self->extraElementen }) {
            if (UNIVERSAL::isa($self->extraElementen->{$obj_key}, 'ARRAY')) {
                my @values;
                for my $value (@{ $self->extraElementen->{$obj_key} }) {
                    push(
                        @values,
                        $self->_convert_to_plain_value(
                            $value
                        )
                    );
                }

                $plain_values->{ $obj_key } = \@values;
            } else {
                $plain_values->{ $obj_key } = $self->_convert_to_plain_value(
                    $self->extraElementen->{$obj_key}
                );
            }
        }

        $rv->{extraElementen} = $plain_values;
    }

    my $as_params   = {
        $self->_object_name     => $rv,
        is_active               => $self->is_active,
        sleutelGegevensbeheer   => $self->sleutelGegevensbeheer,
        verwerkingssoort        => $self->verwerkingssoort,
    };

    $as_params->{datumSluiting} = $self->datumSluiting;

    return $as_params;
}

__PACKAGE__->meta->make_immutable;
package Zaaksysteem::StUF::Body::AOA;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'AOA';

use constant    OBJECT_VALUES   => [
    {
        name        => 'identificatie',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'typering',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'wpl.identificatie',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'wpl.woonplaatsNaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'opr.identificatie',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'gor.openbareRuimteNaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'huisnummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'huisletter',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'huisnummertoevoeging',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'postcode',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'gor.straatnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'num.status',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'inOnderzoek',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'ingangsdatumObject',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    ### Relationships
    {
        name        => 'isVan',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'TGO',
        via         => 'gerelateerde'
    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);

has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;
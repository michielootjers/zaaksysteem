package Zaaksysteem::StUF::Body::OPR;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'OPR';

use constant    OBJECT_VALUES   => [
    {
        name        => 'identificatie',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'wpl.identificatie',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'wpl.woonplaatsNaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'gor.openbareRuimteNaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'gor.geconstateerd',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'gor.type',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'gor.status',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'inOnderzoek',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'ingangsdatumObject',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    }
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);


has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;
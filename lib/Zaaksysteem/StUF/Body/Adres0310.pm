package Zaaksysteem::StUF::Body::Adres0310;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'verblijfsadres';

use constant    OBJECT_VALUES   => [
    {
        name        => 'aoa.postcode',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'wpl.woonplaatsNaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'gor.straatnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aoa.huisnummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aoa.huisletter',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aoa.huisnummertoevoeging',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aoa.aanduidingBijHuisnummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aoa.ingangsdatum',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    # {
    #     name        => 'ingangsdatum',
    #     isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    # },
    # {
    #     name        => 'einddatum',
    #     isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    # },
    {
        name        => 'aoa.gemeentecode',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
        convenient  => 'gemeentecode',

    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
    # has $value->{convenient} => ( is => 'rw') if $value->{convenient};
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);

has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;
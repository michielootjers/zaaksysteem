package Zaaksysteem::StUF::Parser::0204;

use File::Spec::Functions qw(catfile);
use XML::Compile::Util;
use Moose;

use Zaaksysteem::Constants qw(STUF_XSD_PATH);
use Zaaksysteem::Exception;

extends 'Zaaksysteem::StUF::Parser';

use Zaaksysteem::XML::Compile;

has 'schema'    => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class('Zaaksysteem::StUF::0204::Instance')->stuf0204->schema;
    }
);
    
has 'xml_definitions'   => (
    'is'    => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return [
            catfile($self->home, STUF_XSD_PATH, '0204/stuf0204.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bgstuf0204.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bg0204.xsd'),
        ];
    }
);

sub process {
    my $self = shift;

    if (!$self->xml) {
        throw('stuf/parser/parse_xml/no_xml', 'No XML given');
    }

    my $stufxml = $self->_get_body_as_element();

    my $elem    = pack_type $stufxml->namespaceURI, $stufxml->localName;

    my $reader = $self->schema->reader($elem);
    $self->namespace($stufxml->namespaceURI);
    $self->element($stufxml->localName);
    $self->data($reader->($stufxml));
}

__PACKAGE__->meta->make_immutable();

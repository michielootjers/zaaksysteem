package Zaaksysteem::StUF::0204::Instance;

use File::Spec::Functions qw(catfile);
use Moose;

use Zaaksysteem::Constants qw(STUF_XSD_PATH STUF_XML_URL);

with 'Zaaksysteem::XML::Compile::Instance';

use constant STUF_ELEMENTS => [
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0204}kennisgevingsBericht',
        compile  => 'RW',
        method   => 'kennisgevingsbericht',
    },
    {
        element  => '{http://www.egem.nl/StUF/StUF0204}bevestigingsBericht',
        compile  => 'RW',
        method   => 'bevestigingsbericht',
    },
    {
        element  => '{http://www.egem.nl/StUF/StUF0204}foutBericht',
        compile  => 'RW',
        method   => 'foutbericht',
    }
];

=head1 NAME

Zaaksysteem::StUF::0204::Instance - StUF0204 instance object for L<Zaaksysteem::XML::Compile>

=head1 SYNOPSIS

    ### Within a different module
    my $backend     = Zaaksysteem::XML::Compile->xml_compile;

    $backend->add_class('Zaaksysteem::StUF::0204::Instance');

    ### Load StUF 0204 instance
    my $stuf0204    = $backend->stuf0204;

    $stuf0204->kennisgevingsbericht('WRITER', { stuurgegevens => {...}});
    $stuf0204->kennisgevingsbericht('READER', $xml);


=head1 DESCRIPTION

=head1 REQUIRED ATTRIBUTES

See L<Zaaksysteem::XML::Compile::Instance> for a description of Required attributes

=cut

has 'name'  => (
    'is'        => 'ro',
    'default'   => 'stuf0204'
);

has 'elements'  => (
    'is'        => 'ro',
    'default'   => sub {
        return STUF_ELEMENTS;
    }
);


has 'schemas'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return [
            catfile($self->home, STUF_XSD_PATH, '0204/stuf0204.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bgstuf0204.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bg0204.xsd'),
        ];
    }
);

has '_reader_config' => (
    'is'        => 'rw',
    'default'   => sub {
        my $self = shift;

        my $egem = $self->egem;

        my $res  = {
            sloppy_integers                => 1,
            interpret_nillable_as_optional => 1,
            check_values                   => 0,
            'hooks'                        => [{
                    before => sub {
                        my ($node, $value, $path) = @_;

                        ### StUF Patch, somehow we do not know how to parse PRSADRINS messages. Also,
                        ### we ignore the PRSADRINS information, so this information is not needed
                        ### for zaaksysteem. Therefore, we remove the PRSADRINS blocks to keep the sysin
                        ### from halting
                        if ($node->localName =~ /^PRS$/) {
                            my @children = $node->childNodes();
                            my $found    = {};
                            for my $child (@children) {
                                if (   $child->localName
                                    && $child->localName =~ /^PRSADRINS$/) {
                                    if ($found->{$child->nodeName}) {
                                        $node->removeChild($child);
                                    }

                                    $found->{$child->nodeName} = 1;
                                }
                            }
                        }

                        _apply_vicrea_patches($egem, @_);

                        return $node;
                    },
                    after => sub {
                        my ($node, $value, $path) = @_;

                        my @find = qw(
                          sleutelGegevensbeheer
                          verwerkingssoort
                          sleutelVerzendend
                        );

                        foreach (@find) {
                            _parse_node_atribute(
                                $egem,
                                node     => $node,
                                atribute => $_,
                                value    => $value
                            );
                        }

                        if ($node->getAttributeNS($egem, 'noValue')) {
                            $value->{'noValue'} =
                              $node->getAttributeNS($egem, 'noValue');
                        }

                        return $value;
                      }
                }]
        };
    }
);

=head1 METHODS

=head2 egem

Arguments: none

Return Value: $URL_OF_STUF_STANDARD

    print $self->egem;

    # http://www.egem.nl/StUF/StUF0204
    
=cut

sub egem {
    my $self            = shift;

    return join('/', STUF_XML_URL, 'StUF0204');
}

=head1 INTERNAL METHODS

=head2 _apply_vicrea_patches

Applies vicrea patches, supplied for C<_reader_config> of this class.

=cut


sub _apply_vicrea_patches {
    my ($egem, $node, $value, $path)   = @_;

    if ($node->localName =~ /^PRSPRS.+$/) {
        my @children = $node->childNodes();

        for my $child (@children) {
            next unless (
                $child->localName &&
                $child->localName eq 'PRS'
            );

            my @prs_elements = $child->childNodes();

            for my $prs_element (@prs_elements) {
                if (
                    $prs_element->localName &&
                    !grep({ lc($prs_element->localName) eq lc($_) }
                        qw/
                            a-nummer
                            bsn-nummer
                            voornamen
                            voorletters
                            voorvoegselGeslachtsnaam
                            geslachtsnaam
                            geboortedatum
                            geboorteplaats
                            codeGeboorteland
                            geslachtsaanduiding
                            adellijkeTitelPredikaat
                        /
                    )
                ) {
                    $child->removeChild($prs_element);
                }
            }
        }
    }

    ### Vicrea Patch, add nillable when we get a noValue
    ### attribute
    if (
        $node->hasAttributeNS($egem, 'noValue')
        && !$node->hasAttributeNS(
            'http://www.w3.org/2001/XMLSchema-instance', 'nil'
        )
      ) {
        $node->setAttributeNS(
            'http://www.w3.org/2001/XMLSchema-instance',
            'nil',
            'true'
        );
    }
}

=head2 _parse_node_attribute

Parses an attribute of a C<$node>, supplied for C<_reader_config> of this class.

=cut

sub _parse_node_atribute {
    my $egem = shift;
    my $args = {@_};
    if ($args->{node}->hasAttributeNS($egem, $args->{atribute})) {
        $args->{value}{$args->{atribute}} = $args->{node}->getAttributeNS($egem, $args->{atribute});
    }
}

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2013, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
package Zaaksysteem::StUF::TGO;

use Moose::Role;

use constant ADDRESS_MAP            => {
    'identificatie'                                 => 'verblijfsobject_identificatie',
    'gbo.gebruiksdoel'                              => 'verblijfsobject_gebruiksdoel',
};

=head2 METHODS

=head2 get_params_for_r02

Gets a set of params for manipulating natuurlijk_persoon

=cut


sub get_params_for_tgo {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{OPR};

    for my $key (keys %{ ADDRESS_MAP() }) {
        my $object_key  = $key;
        my $object_value;

        if ($object_key =~ /^extra\./) {
            $object_key =~ s/^extra\.//;

            next unless exists($object_params->{ 'extraElementen' }-> { $object_key });
            $object_value = $object_params->{ 'extraElementen' }-> { $object_key };
        } else {
            next unless exists($object_params->{ $object_key });

            $object_value = $object_params->{ $object_key };
        }

        $params->{ADDRESS_MAP->{$key}} = $object_value;
    }

    if ($params->{openbareruimte_begindatum} && $params->{openbareruimte_begindatum} =~ /^\d{8}$/) {
        $params->{openbareruimte_begindatum} =~ s/^(\d{4})(\d{2})(\d{2})$/$1-$2-$3/;
    }

    return $params;
}

1;
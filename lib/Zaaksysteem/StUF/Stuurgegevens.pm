package Zaaksysteem::StUF::Stuurgegevens;

use Moose;
use Moose::Util qw[apply_all_roles];

use Zaaksysteem::Exception;

use Zaaksysteem::StUF::Parser;

use Data::Dumper;

use constant STUF_STUURGEGEVENS_FIELDS  => [qw/
    entiteittype
    berichtsoort
    sectormodel
    versieStUF
    versieSectormodel

    zender
    ontvanger
    referentienummer
    tijdstipBericht

    kennisgeving
    vraag
    antwoord
    bevestiging
    fout

    berichtcode
    crossRefnummer
/];

has [ @{ STUF_STUURGEGEVENS_FIELDS() }]     => (
    is          => 'rw',
);

has '+zender'       => (
    required    => 1,
);

has '+ontvanger'    => (
    required    => 1,
);

has '+versieStUF'   => (
    lazy        => 1,
    default     => '0204',
);

has '+versieSectormodel'   => (
    lazy        => 1,
    default     => '0204',
);

has '+sectormodel'   => (
    lazy        => 1,
    default     => 'BG',
);

has '+berichtcode'   => (
    trigger     => sub {
        my $self            = shift;

        $self->berichtsoort(shift);
    }
);

sub TO_STUF {
    my $self                = shift;

    return {
        map {
            $_  => $self->$_
        } grep { defined($self->$_) } @{ STUF_STUURGEGEVENS_FIELDS() }
    };
}

__PACKAGE__->meta->make_immutable();

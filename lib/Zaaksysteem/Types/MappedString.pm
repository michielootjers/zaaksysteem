package Zaaksysteem::Types::MappedString;
use Moose;
use Moose::Util::TypeConstraints;

=head1 NAME

Zaaksysteem::Types::MappedString - "Multivalue" strings

=head1 DESCRIPTION

These are "multi-value" strings, that 

=head1 SYNOPSIS

    my $mapped = Zaaksysteem::Types::MappedString->new(
        original => 'foo',
        mapped   => 'bar',
    );

    print "$mapped"; # prints "bar"

=head1 ATTRIBUTES

=head2 original

The "internal" string.

=head2 mapped

The "external" version of the string.

=cut

use overload 
  q("") => \&_as_string;

coerce 'Str',
    from 'Zaaksysteem::Types::MappedString',
    via { "$_" };

has original => (
    is       => 'ro',
    required => 1,
    isa      => 'Str',
);

has mapped => (
    is      => 'ro',
    isa     => 'Str',
    default => sub { shift->original },
);

sub _as_string {
    return shift->mapped;
}

sub TO_JSON {
    my $self = shift;

    return {
        original => $self->original,
        mapped   => $self->mapped,
    };
}

# Because namespace::autoclean removes overloads.
no Moose;

__PACKAGE__->meta->make_immutable();

1;

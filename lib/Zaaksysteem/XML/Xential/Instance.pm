package Zaaksysteem::XML::Xential::Instance;
use Moose;

=head1 NAME

Zaaksysteem::XML::Xential::Instance - Zaaksysteem's implementation of Xential

=head1 SYNOPSIS

=cut

use File::Spec::Functions qw(catfile);
use Zaaksysteem::Tools;

with 'Zaaksysteem::XML::Compile::Instance';

has name => (
    is      => 'ro',
    default => 'xential',
);

has wsdl => (
    is   => 'ro',
    default   => sub {
        my $self    = shift;

        my $basedir = catfile($self->home, qw(share wsdl xential));
        my @wsdl;
        foreach (qw(infoservice contextservice buildservice)) {
            push(@wsdl, catfile($basedir, "$_.wsdl"));
        }
        return \@wsdl;
    }
);


has schemas => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my $basedir = catfile($self->home, qw(share xsd xential));

        my @schemas;
        foreach (qw(infoservice contextservice buildservice jaxb.dev.java.net.array)) {
            push(@schemas, catfile($basedir, "$_.xsd"));
        }
        return \@schemas;
    }
);

has '+schema' => (
    is      => 'ro',
    isa     => 'XML::Compile::WSDL11',
    lazy    => 1,
    builder => 'get_schema',
);

has elements => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return [
            ### Info service
            {
                element => '{http://services.factory.xential.interactionnext.nl/}getVersion',
                compile => 'RW',
                method  => 'get_version',
                soapcall => {
                    call     => 'getVersion',
                    service  => 'InfoService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}getVersionInfo',
                compile => 'RW',
                method  => 'get_version_info',
                soapcall => {
                    call     => 'getVersionInfo',
                    service  => 'InfoService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}getTemplates',
                compile => 'RW',
                method  => 'get_templates',
                soapcall => {
                    call     => 'getTemplates',
                    service  => 'InfoService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}getRegistrationInfo',
                compile => 'RW',
                method  => 'get_registration_info',
                soapcall => {
                    call     => 'getRegistrationInfo',
                    service  => 'InfoService',
                },
            },

            ### Context service
            {
                element => '{http://services.factory.xential.interactionnext.nl/}listBases',
                method  => 'list_bases',
                compile => 'RW',
                soapcall => {
                    call     => 'listBases',
                    service  => 'ContextService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}createContext',
                method  => 'create_context',
                compile => 'RW',
                soapcall => {
                    call     => 'createContext',
                    service  => 'ContextService',
                },
            },

            ### Build service
            {
                element => '{http://services.factory.xential.interactionnext.nl/}preparePreregisteredWithOptions',
                method  => 'prepare_preregistered_with_options',
                compile => 'RW',
                soapcall => {
                    call     => 'preparePreregisteredWithOptions',
                    service  => 'BuildService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}startDocument',
                method  => 'start_document',
                compile => 'RW',
                soapcall => {
                    call     => 'startDocument',
                    service  => 'BuildService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}buildDocument',
                compile => 'RW',
                method  => 'build_document',
                soapcall => {
                    call     => 'buildDocument',
                    service  => 'BuildService',
                },
            },
            {
                element => '{http://services.factory.xential.interactionnext.nl/}getBuildStatus',
                compile => 'RW',
                method  => 'get_build_status',
                soapcall => {
                    call     => 'getBuildStatus',
                    service  => 'BuildService',
                },
            },
        ];
    },
);


has _reader_config => (is => 'ro', default => sub { });

=head2 get_schema

Get the correct L<XML::Compile> schema for this instance

=cut

sub get_schema {
    my $self = shift;

    my $wsdl = XML::Compile::WSDL11->new();

    $wsdl->addWSDL($_) for @{ $self->wsdl };

    $wsdl->importDefinitions($self->schemas);

    $wsdl->compileAll;

    return $wsdl;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

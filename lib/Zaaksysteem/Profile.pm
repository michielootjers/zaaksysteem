package Zaaksysteem::Profile;

use base 'Exporter';

use Params::Profile;
use Moose::Util::TypeConstraints qw[find_type_constraint];

use Zaaksysteem::Exception qw[throw];
use Zaaksysteem::Constants;

our @EXPORT = qw[define_profile assert_profile];
our @EXPORT_OK = qw[build_profile];

=head1 Zaaksysteem L<Params::Profile> helper

This module exports a single sub, C<define_profile> that helps with
defining L<Param::Profile> profiles.

=head2 define_profile

This function is nothing but some syntactic sugar for registering a
L<Params::Profile> profile.

    use Zaaksysteem::Profile;

    define_profile method_name => (
        required => [qw/required parameters/]],
        optional => [qw/optional params/]],
        constraint_methods => {
            required => qr[true|false]
        }
    );

Or, using the extended C<typed> profile syntax:

    define_profile method_name => (
        required => [qw/object string/],
        typed => {
            object => 'Package::Name', # Works, isa is called first
            string => 'Str'            # Also works, uses Moose to validate
        }
    );

Alternatively, you can combine the C<required> and C<optional> lists with the
C<typed> specification for a bit better terseness:

    define_profile method_name => (
        required => {
            id => 'Int',
            name => 'Str'
        },
        optional => {
            version => 'Int'
        }
    );

=cut

sub define_profile ($%) {
    my $profile_name = shift;
    my $profile = build_profile(@_);

    my ($calling_class) = caller 0;

    Params::Profile->register_profile(
        method => $profile_name,
        caller => $calling_class,
        profile => $profile
    );
}

=head2 build_profile

This function implements the logic to implement C<typed> profiles. It expects
a profile defintion as a hash, and returns a hashref representing a profile
defintion that L<Data::FormValidator> will accept.

=cut

sub build_profile (%) {
    my %profile = @_;

    for my $key (qw[required optional]) {
        if (exists $profile{ $key } && ref $profile{ $key } eq 'HASH') {
            $profile{ typed } = {
                %{ exists $profile{ typed } ? $profile{ typed } : {} },
                %{ $profile{ $key } }
            };
            
            $profile{ $key } = [ keys %{ $profile{ $key } } ];
        }
    }

    my $typed = delete $profile{ typed };

    if($typed) {
        $profile{ constraint_methods } = inject_typed_constraints(
            $typed,
            $profile{ constraint_methods }
        );
    }

    unless ($profile{ msgs }) {
        $profile{ msgs } = PARAMS_PROFILE_DEFAULT_MSGS;
    }

    return \%profile;
}

=head2 inject_typed_constraints

This is a bit of Moose integration for L<Params::Profile>. This method
produces a hashref suitable for L<Data::FormValidator>'s constraint_methods
key.

It basically boils down to an implicit validator-chain built from combining
the C<typed> key in the validator profile and the C<constraint_methods> in a
subref that checks first if the supplied value L<UNIVERSAL::isa> specific type,
and tries to apply Moose's typesystem as a fallback.

There is one major caveat though. L<Data::FormValidator> constraint_methods
can be more than just a coderef or regexp, this code will unelegantly fail
catastrophically in that case. Deal with it.

=cut

sub inject_typed_constraints {
    my $types = shift;
    my $constraints = shift || {};

    for my $fieldname (keys %{$types}) {
        my $type = find_type_constraint($types->{$fieldname})
          || $types->{$fieldname};

        my $existing_constraint = delete $constraints->{$fieldname};

        my $ref = ref $existing_constraint;
        if ($ref && $ref ne 'CODE' && $ref ne 'Regexp') {
            throw(
                'params/profile/validation_chain',
                sprintf(
'Could not inject wrapped validator for "%s", existing validator not a subref or regexp: %s',
                    $fieldname, $ref
                ));
        }

        $constraints->{$fieldname} = sub {
            my $dv    = shift;
            my $value = pop;

            # Here's the magic bit. This tries to run UNIVERSAL::isa on the
            # value to see if it's an instance of $type, otherwise uses the
            # Moose type for whatever you supplied
            if (eval {$type->isa('Moose::Meta::TypeConstraint')}) {
                if (!$type->check($value)) {
                    return 0;
                }
                if (!$ref) {
                    return 1;
                }
            }
            else {
                my $isa_ok = eval {$value->isa($type);};
                if (!$isa_ok) {
                    return 0;
                }
                elsif ($isa_ok && !$ref) {
                    return 1;
                }
            }

            if ($ref eq 'CODE') {
                return $existing_constraint->($value);
            }
            elsif ($ref eq 'Regexp') {
                return $value =~ $existing_constraint;
            }
            return 0;
        };
    }

    return $constraints;
}

=head2 assert_profile(\%params, $string_method || \%profile )

Return: Exception C<params/profile>

Assert a given parameter profile by validating it and throwing an exception on
failure.

B<Options>

=over 4

=item params [required]

The parameters to test, you will probably use C<< $c->req->params >>.

=item method OR profile [optional]

When given a method name, it will find out the profile for you by using the
caller and finding the profile registered by register_profile.

When the second parameters is a HashRef, it will use it as the profile.

=back

=cut

sub assert_profile {
    my ($data, %params) = @_;

    unless(ref $data eq 'HASH') {
        throw('params/profile/input', 'Input not a HashRef, unable to assert profile');
    }

    my $dv;

    if (exists $params{ profile }) {
        unless(ref $params{ profile } eq 'HASH') {
            throw('params/profile/config', 'Parameter "profile" expected to be a HASH ref.');
        }

        $dv = Data::FormValidator->check($data, $params{ profile });
    } elsif (exists $params{ method }) {
        $dv = Params::Profile->check(
            params => $data,
            method => "$params{ method }"
        );
    } else {
        $dv = Params::Profile->check(
            params => $data,
            method => (caller 1)[3]
        );
    }

    unless(UNIVERSAL::isa($dv, 'Data::FormValidator::Results')) {
        throw('params/profile/lumbergh', 'Trying to assert validity of a non-Data::FormValidator::Results profile. Yeahhhhh, if you could just... rewrite your profile... that\'d be greeeaaaaaat.'
        );
    }

    return $dv if $dv->success;

    my $message = format_dv($dv);

    if (exists $params{ error_wrapper }) {
        unless(ref $params{ error_wrapper} eq 'CODE') {
            throw('params/profile/config', 'Parameter "error_wrapper" expected to be a CODE ref.');
        }

        $message = $params{ error_wrapper }->($message);
    }

    throw({
        type            => 'params/profile',
        message         => $message,
        object          => $dv
    });
}

sub format_dv {
    my $dv = shift;

    my @message_parts = sprintf("Validation of profile failed at %s:%d", (caller(1))[1, 2]);

    if ($dv->has_invalid) {
        push @message_parts, sprintf("  invalid: %s", join(', ', $dv->invalid));
    }

    if ($dv->has_missing) {
        push @message_parts, sprintf("  missing: %s", join(', ', $dv->missing));
    }

    return join ",\n", @message_parts;
}

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

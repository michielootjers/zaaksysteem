package Zaaksysteem::SOAP;

use Moose;
use Data::Dumper;
use XML::Tidy;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use FindBin qw/$Bin/;

#use Zaaksysteem::SBUS::Dispatcher::Soap::TestTransport;

use FindBin qw/$Bin/;

use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Translate::Reader;
use XML::Compile::Util;



has 'wsdl_file'     => (
    'is'    => 'rw',
    'isa'   => 'Str',
);

has 'home'      => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return $Bin . '/..';
    }
);

has 'soap_endpoint'     => (
    'is'        => 'rw',
);

has 'soap_port'         => (
    'is'        => 'rw',
);

has 'soap_service'      => (
    'is'        => 'rw',
);

has 'soap_action'       => (
    'is'        => 'rw',
);

has 'is_test'      => (
    'is'        => 'rw',
);

=head2 test_responses (HASHREF)

Use this to stock the SOAP client with preset xml responses for given
calls. The SOAP server will not talk to the actual server, but will
pretend the server has returned the provided XML. Useful for regression
testing.

Works on a per-call basis, so for every call this hash will be consulted,
if there's an answer that is used, otherwise, business as usual.

    my $soap = new Zaaksysteem::SOAP({
        test_responses => {
            call1 => '<?xml><bla>1</bla>',
            call2 => '<?xml><bla>2</bla>'
        }
    });

=cut

has test_responses => (
    is => 'rw',
    default => sub { {} }
);

has 'reader_writer_config'  => (
    'is'        => 'rw',
    'default'   => sub { return {}; }
);

has 'xml_definitions'       => (
    'is'        => 'rw',
    'default'   => sub { return []; }
);

has 'soap_ssl_key' => (
    'is' => 'rw',
);

has 'soap_ssl_crt' => (
    'is' => 'rw',
);


has wsdl => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        my %opts    = ();
        $opts{opts_readers}     = $self->reader_writer_config->{READER}
            if $self->reader_writer_config->{READER};
        $opts{opts_writers}     = $self->reader_writer_config->{WRITER}
            if $self->reader_writer_config->{WRITER};

        my $wsdl = XML::Compile::WSDL11->new(
            (
                $self->wsdl_file =~ /^\//
                    ? $self->wsdl_file
                    : $self->home . '/share/wsdl/' . $self->wsdl_file
            ),
            %opts
        );

        for my $def (@{ $self->xml_definitions }) {
            $wsdl->importDefinitions(
                $def
            );
        }

        return $wsdl;
    }
);

sub transport {
    my $self         = shift;
    my $call         = shift;

    my $transport   = XML::Compile::Transport::SOAPHTTP->new(
        'address'   => $self->soap_endpoint,
        'timeout'   => 25,
    );

    if ($self->soap_ssl_key && $self->soap_ssl_crt) {
        $transport->userAgent->ssl_opts(
            'SSL_key_file' =>
                $self->soap_ssl_key
        );

        $transport->userAgent->ssl_opts(
            'SSL_cert_file' =>
                $self->soap_ssl_crt
        );

        $transport->userAgent->ssl_opts(
            'verify_hostname' => 0
        );
    }

    $transport->userAgent->default_header('SOAPAction', $self->soap_action);

    return $transport;
}


sub get_dispatch_xml {
    my $self                        = shift;
    my ($call, $params)             = @_;

    my $client_options              = {
        transport_hook  => sub {
            my $xml         = shift;

            return $xml;
        }
    };

    my ($answer, $trace)            = $self->_dispatch_call(
        {
            call                => $call,
            params              => $params,
            client_options      => $client_options,
        }
    );

    return $trace->request->content;
}


sub dispatch {
    my $self                        = shift;
    my ($call, $params)             = @_;

    my $client_options              = {
        transport       => $self->transport($call, $self->soap_port),
    };


    # when creating this object, you can pass a list of test responses.
    # if one of these hardcoded responses is requested, it is returned
    # instead of contacting the remote server. the purpose is to be able
    # to test a large part of the chain without invoking the actual remote
    # server, because that may be both impolite (spammy) and change data
    # because some requests trigger write operations.
    if (my $fake = $self->test_responses->{$call}) {
        $client_options->{transport_hook} = sub {
            return HTTP::Response->new(200, 'answer manually created',
                [ 'Content-Type' => 'text/xml' ], $fake
            );
        }
    }

    my ($answer, $trace)            = $self->_dispatch_call(
        {
            call                => $call,
            params              => $params,
            client_options      => $client_options,
        }
    );

    if (!$answer) {
        throw(
            'stuf/soap/no_answer',
            'No answer received from other party, that cannot be good',
            $trace,
        );
    }

    return ($answer, $trace);
}

define_profile _dispatch_call => (
    required        => [qw/call params client_options/],
);

sub _dispatch_call {
    my $self                        = shift;
    my $options                     = assert_profile(shift)->valid;

    my %soap_options                = ();
    $soap_options{service}          = $self->soap_service   if $self->soap_service;
    $soap_options{port}             = $self->soap_port      if $self->soap_port;
    $soap_options{operation}        = $options->{call};
    $soap_options{transport}        = $options->{client_options}{transport}->compileClient(
        hook => $options->{client_options}{transport_hook}
    );

    my $client = $self->wsdl->compileClient(%soap_options);

    # pending testresults for key2verhuizing, leaving this in just in case
    # my $operation                   = $self->wsdl->operation(
    #     $options->{call},
    #     %soap_options
    # );

    # my $client                      = $operation->compileClient(
    #     %{ $options->{client_options} }
    # );

    my ($answer, $trace)            = $client->($options->{params});

    return ($answer, $trace);
}

=head2 $soap->from_xml($XML)

Return value: $HASH_REF

    my $perl = $soap->from_xml('<xml><bla>test</bla>');

    # returns: { bla => 1 }

Convert an xml string into a perl datastructure. The idea is that when
you send a SOAP request and it's not received succesfully, you want to
fire the exact same request at a later time. Xml is a clean way to serialize,
so the xml is stored in the transaction table and revived a later time.
Since the SOAP transporter works from a Perl structure, this converts back.
Wouldn't it be possible to send the XML directly without further back-and-
forth conversion.

B<Return HASHREF>

=over 4

=item content

The perl structure compiled from xml

=item name

The xml node name

=back

=cut

sub from_xml {
    my $self    = shift;
    my $xml     = shift;

    die('xml needs to be set to be able to transform xml')
        unless $xml;

    my $doc     = XML::LibXML->load_xml(string => $xml);

    my ($node)  = $doc->findnodes('//soap:Envelope/soap:Body/*');

    my $elem    = pack_type $node->namespaceURI, $node->localName;

    my $reader  = $self->wsdl->compile(
        READER => $elem,
        %{ $self->reader_writer_config->{READER} }
    );

    # {kennisgeving} => {}
    return {
        name    => $node->localName,
        content => $reader->($node)
    };
}

1;

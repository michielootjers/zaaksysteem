package Zaaksysteem::Controller::Kennisbank::Vraag;

use Moose;

BEGIN {extends 'Catalyst::Controller' }

use Zaaksysteem::Constants;
use Data::Dumper;

use constant ENTRY_TYPE     => 'vraag';
use constant KENNISBANK     => 'kennisbank';
use constant TABLE_NAME     => 'DB::KennisbankVragen';
use constant ENTRY_CONFIG   => {
    components  => [
        {
            name     => 'vraag',
            content => [
                {
                    column  => 'vraag'
                },
            ]
        },
        {
            name     => 'antwoord',
            content => [
                {
                    column  => 'antwoord'
                },
            ]
        },
    ],
    relaties    => [
        {name => 'zaaktypen',   url => '/zaaktype/search' },
        {name => 'producten',   url => '/kennisbank/search/producten' },
    ]
};
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

=head1 METHODS

=head2 base

Chain: /kennisbank/vraag

=cut

sub base : Chained('/kennisbank/base') : PathPart('vraag') : CaptureArgs(2) {
    my ($self, $c, $bibliotheek_categorie_id, $id) = @_;

    $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
        {
            'system'    => { 'is' => undef },
            'pid'       => undef,
        },
        {
            order_by    => ['pid','naam']
        }
    );

    $c->stash->{entry_type}     = ENTRY_TYPE;
    $c->stash->{entry_config}   = ENTRY_CONFIG;
    $c->stash->{bibliotheek_categorie_id} = $bibliotheek_categorie_id;

    if($bibliotheek_categorie_id) {
        my $categorie = $c->model('Bibliotheek::Categorie');

        my $crumbs = $categorie->get_crumb_path({
            bibliotheek_categorie_id => $bibliotheek_categorie_id, 
        });

        $c->stash->{crumbs} = $crumbs;
    }

    unless (length($id) && $id =~ /^\d+/) {
        $c->log->error(
            'C::Kennisbank::Vraag->find: invalid input data'
        );
        $c->res->redirect($c->uri_for('/'));
        $c->detach;
    }

    unless (
        !$id ||
        (
            $c->stash->{entry} = $c->model(TABLE_NAME)->find_active($id)
        )
    ) {
        $c->log->error(
            'C::Kennisbank::Vraag->find: cannot find id specified'
        );
        $c->res->redirect($c->uri_for('/'));
        $c->detach;
    }
}

=head2 vraag

End Chain: /kennisbank/vraag/ID/some_name_of_vraag

Publieke informatie voor /kennisbank/vraag/ID/naam

=cut

sub kennisbank : Chained('base') : PathPart('') : Args(1) {
    my ($self, $c, $naam_not_used_internally ) = @_;

    unless ($c->stash->{entry}) {
        $c->log->error(
            'C::Kennisbank::Vraag->view: no entry to view'
        );
        $c->res->redirect($c->uri_for('/'));
        $c->detach;
    }

    # only trigger event if we have an active subject
    if($c->session->{ betrokkene_session }) {
        $c->model('DB::Logging')->trigger('kb/question/ask', {
            component => 'kb',
            created_for => $c->session->{ betrokkene_session }{ betrokkene_identifier },
            data => {
                subject_id => $c->session->{ betrokkene_session }{ betrokkene_identifier },
                question_id => $c->stash->{ entry }->id
            }
        });
    }

    $c->stash->{template} = KENNISBANK . '/index.tt';
}

=head2 bewerken

End Chain: /kennisbank/vraag/ID/bewerken

Publieke informatie voor /kennisbank/vraag/ID/naam

=cut

{
    Zaaksysteem->register_profile(
        method  => 'bewerken',
        profile => PROFILE_KENNISBANK_VRAAG_BEWERKEN
    );

    sub bewerken : Chained('base') : PathPart('bewerken') : Args() {
        my ($self, $c, $naam_not_used_internally ) = @_;

        $c->stash->{bewerken}           = 1;

        my $params = $c->req->params;

        warn "params: " . Dumper $params;
        my $dv = Params::Profile->check(
            params  => $params,
        );

        my $opts    = $dv->valid;

        ### Depending on xml request (do_validation) or create, we detach
        if ($c->req->is_xhr && $params->{do_validation}) {
            $c->zvalidate($dv);
            $c->detach;
        }


        if ($params->{confirm}) {
            $c->stash->{nowrapper} = 1;
            $c->stash->{template} = KENNISBANK . '/confirm.tt';
            $c->detach;
        } elsif ($params->{confirmed}) {

            if ($dv->success) {
            

                if ((my $entry = $c->model(TABLE_NAME)->bewerk_entry($opts))) {
                    $c->log->info(
                        'Kennisbank::Vraag->bewerken: Created entry: '
                        . Dumper [$entry->get_columns]
                    );

                    $c->res->redirect($c->uri_for(
                        '/' .  KENNISBANK . '/' . ENTRY_TYPE .
                        '/' . $params->{bibliotheek_categorie_id} .
                        '/' . $entry->id . '/' . $entry->titel_url
                    ));
                    $c->detach;
                }
            } else {
                $c->log->error('Invalid fields: ' . Dumper($dv));

            }
        }

        $c->stash->{dagobert_editor}    = 1;

        $c->stash->{template} = KENNISBANK . '/index.tt';
    }
}


sub verwijderen : Chained('base'): PathPart('verwijderen') {
    my ($self, $c)   = @_;

    my $params = $c->req->params;

    unless($params->{confirmed}) {
        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze vraag wilt verwijderen?';
    
        $c->stash->{confirmation}->{commit_message}       = 1;

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/kennisbank/vraag/' . 
                $c->stash->{entry}->bibliotheek_categorie_id->id . '/'.
                $c->stash->{entry}->id
                .'/verwijderen'
            );
    
        $c->forward('/page/confirmation');
        $c->detach;
    } 

    my $event = $c->model('DB::Logging')->trigger('kb/question/remove', {
        component => LOGGING_COMPONENT_KENNISBANK_VRAGEN,
        component_id => $c->stash->{ entry }->id,
        data => {
            question_id => $c->stash->{ entry }->id,
            reason => $params->{ commit_message }
        }
    });

    $c->push_flash_message($event->onderwerp);

    $c->stash->{entry}->deleted(DateTime->now());
    $c->stash->{entry}->update;
    
    $c->res->redirect($c->uri_for('/beheer/bibliotheek/'));

    $c->detach;
}


=head2 index

=cut

sub index : Chained('/') : PathPart('vraag') : Args(0) : ZAPI {
    my ($self, $c, $id) = @_;

    $c->stash->{zapi}  = $c
                        ->model(TABLE_NAME)
                        ->search_external();
}

sub zapi_base : Chained('/') : PathPart('vraag') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{entry}  = $c
                        ->model(TABLE_NAME)
                        ->find_external($id);
}

sub read : Chained('zapi_base') : PathPart('') : Args(0): ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->stash->{entry} || [];
}



1;

=head1 INTERNAL

=head2 PATH STRUCTUUR

De structuur van de publieke paden is als volgt

=over 4

=item /kennisbank

Publiek overzicht

=item /kennisbank/vraag/44/energiebelasting_met_vrijstelling_leverancier/

Vraag overzicht

=item /kennisbank/vraag/31/sluiten_wij_homo_huwelijken/

Vraag overzicht

=back

=cut


package Zaaksysteem::Controller::ObjectSubscription;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Zaaksysteem::ZAPI::CRUD::Interface;
use Zaaksysteem::ZAPI::CRUD::Interface::Column;
use Zaaksysteem::ZAPI::CRUD::Interface::Filter;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

use constant ZAPI_CRUD => {
    'read' => Zaaksysteem::ZAPI::CRUD::Interface->new(
        options     => {
            select      => 'multi'
        },
        actions => [
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => "object-subscription-delete",
                type    => "delete",
                label   => "Verwijderen",
                data    => {
                    url => '/object_subscription/delete'
                }
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => 'object-subscription-download',
                type    => 'download',
                label   => 'Exporteer als CSV',
                data    => {
                    url => '/object_subscription/?zapi_format=csv&zapi_no_pager=1'
                }
            )
        ],
        filters => [
            Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                name    => 'interface_id',
                type    => 'select',
                data    => {
                    'options' => '<[getInterfaceOptions()]>'
                },
                value   => '',
                label   => ''
            ),
        ],
        columns         => [
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'local_id',
                label       => 'Tabel ID',
                template    => '<a href="/beheer/object/search/<[item.local_table]>/<[item.local_id]>"><[item.local_id]></a>'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'local_table',
                label       => 'Tabel',
                resolve     => 'local_table',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'object_preview',
                label       => 'Voorbeeld',
                resolve     => 'object_preview',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'interface_id',
                label       => 'Interface',
                resolve     => 'interface_id.name',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'external_id',
                label       => 'Extern ID',
                resolve     => 'external_id',
            ),
        ],
    ),
};

=head1 NAME

Zaaksysteem::Controller::ObjectSubscription - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::ObjectSubscription>

=head1 DESCRIPTION

Zaaksysteem API Controller for ObjectSubscription.

=head1 METHODS

=head2 /object_subscription [GET READ]

 /object_subscription?zapi_crud=1

Returns a list of object_subscriptions.

B<Special Query Params>

=over 4

=item C<zapi_crud=1>

Use the special query parameter C<zapi_crud=1> for getting a technical CRUD
description.

=back

=cut

sub index
    : Chained('/')
    : PathPart('object_subscription')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    if (exists $c->req->params->{zapi_crud}) {
        $c->stash->{zapi} = [ ZAPI_CRUD->{read}->from_catalyst($c) ];
        $c->detach;
    }

    if (exists $c->req->params->{is_error}) {
        $c->req->params
    }

    $c->stash->{zapi}   = $c->model('DB::ObjectSubscription')->search_filtered(
        $c->req->params
    );
}


sub base
    : Chained('/')
    : PathPart('object_subscription')
    : CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{entry}  = $c->model('DB::ObjectSubscription')->find($id);
}

=head2 /object_subscription/ID [GET READ]

Reads information from ObjectSubscription by ID

B<Options>: none

=cut

sub read
    : Chained('base')
    : PathPart('')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->stash->{entry} || [];
}


=head2 /object_subscription/delete [POST DELETE]

Deletes object subscriptions from our database. This will mark these entries as deleted.

=cut

sub delete
    : Chained('/')
    : PathPart('object_subscription/delete')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    # Get resultset and run the trigger
    my $rs      = $c->model('DB::ObjectSubscription')->search_filtered(
        $c->req->params
    );
    $rs->action_for_selection(
        'object_subscription_delete',
        $c->req->params,
    );

    $c->stash->{zapi} = [];
}

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut 



package Zaaksysteem::Controller::File;

use Moose;

use File::stat;
use File::Temp qw(tempfile);
use MIME::Lite;
use Try::Tiny;
use Data::Dumper;

use Zaaksysteem::Backend::Tools::FilestoreMetadata qw(get_document_categories);
use Zaaksysteem::Exception;

BEGIN { extends 'Zaaksysteem::Controller'; }

=head1 NAME

Zaaksysteem::Controller::File - Frontend library for the Zaaksysteem
file structure.

=cut

=head2 create

Create a new file.

=head3 Arguments

=over

=item case_id [optional]

Assign this file to a case. When this parameter is ommitted, the file
will show up in the global file queue for later processing.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item return_content_type

Option to override the content-type this call returns. Mostly for IE-compatibility.

=back

=head3 Location

POST: /file/create

=head3 Returns

A JSON structure containing file properties.

=cut

sub create : Local {
    my ($self, $c) = @_;

    $c->{stash}->{json_content_type} = $c->req->params->{return_content_type};

    try {
        my %optional;
        if ($c->req->params->{case_id}) {
            $optional{case_id} = $c->req->params->{case_id};
        }
        my $upload  = $c->req->upload('file');
        my $subject = _get_subject($c)->betrokkene_identifier;

        # Create the DB-entry
        my $result = $c->model('DB::File')->file_create({
            db_params => {
                
                created_by   => $subject,
                %optional,
            },
            name              => $upload->filename,
            file_path         => $upload->tempname,
        });
        
        $c->{stash}->{json} = $result->discard_changes;
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');
    }
}

=head2 file_create_sjabloon

Create a new file based on a sjabloon and add it to the case accept queue.

=head3 Arguments

=over

=item name [required]

The name this file will get.

=item case_id [required]

The case_id this document will be assigned to.

=item zaaktype_sjabloon_id [required]

The sjabloon_id of the sjabloon that will be used.

=item target_format [optional]

Override for zaaktype_sjablonen.target_format (which is given with
zaaktype_sjabloon_id). Defaults to what is set in 
zaaktype_sjablonen.target_format or .odt if it isn't set at all.

=back

=head3 Returns

A JSON structure containing file properties.

=cut

sub file_create_sjabloon : JSON : Local {
    my ($self, $c) = @_;

    my $params     = $c->req->params;

    try {
        my $subject = _get_subject($c)->betrokkene_identifier;

        my $case = $c->model('DB::Zaak')->find($params->{case_id});
        if (!$case) {
            throw(
                '/file/file_create_sjabloon/case_not_found',
                sprintf("Case with ID %d not found", $params->{case_id}),
            );
        }
        my $zaaktype_sjabloon = $c->model('DB::ZaaktypeSjablonen')
            ->search({id => $params->{zaaktype_sjabloon_id}})->single;

        if (!$zaaktype_sjabloon) {
            throw(
                '/file/file_create_sjabloon/zaaktype_sjabloon_not_found',
                "Zaaktype-sjabloon not found",
            );
        }

        my %optional;
        if (defined $params->{case_document_ids}) {
            $optional{case_document_ids} = [$params->{case_document_ids}];
        }

        my $result = $zaaktype_sjabloon->bibliotheek_sjablonen_id->file_create({
            name    => $params->{name},
            case    => $case,
            subject => $subject,
            target_format => $params->{target_format} || $zaaktype_sjabloon->target_format,
            %optional,
        });

        $c->{stash}->{json} = $result;
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');
    }
}

=head2 update

Update properties of a file or replace the file itself.

=head3 Arguments

=over

=item file_id [required]

=item name [optional]

=item case_id [optional]

=item directory_id [optional]

Setting this to (JSON/JS unquoted) null will reset it to the root.

=item case_document_ids [optional]

=item case_document_clear_old [optional with case_document_id]

When this boolean is set to true, a case_document which is already assigned
to another document gets cleared.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item accepted [optional]

Boolean. When set to false, it requires a rejection_reason. This results in
the document being placed back in the global document queue. No longer bound to
the case.

=item rejection_reason [required with accepted]

A string detailing why the document was rejected. See the accepted parameter.

=item metadata [optional]

A hash containing any of the following metadata columns:
 description
 document_category_parent
 document_category_child
 trust_level -- Valid: 'Openbaar', 'Beperkt openbaar', 'Intern', 'Zaakvertrouwelijk', 'Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim'
 origin -- Valid: 'Inkomend', 'Uitgaand', 'Intern'

All of these columns are optional.

=item deleted

Sets the document to deleted. Note: files are never truly removed, just disabled.

Important: as this is called from a javascript environment it NEEDS 'true' or 'false'.

=back

=head3 Location

POST: /file/update

=head3 Returns

A JSON structure containing the updated file properties.

=cut

sub update : JSON : Local {
    my ($self, $c) = @_;
    my $params     = $c->req->params;

    try {
        $params->{subject} = _get_subject($c)->betrokkene_identifier;
    
        my $file_id = $params->{file_id};
        my ($file) = $c->model('DB::File')->search({
            'me.id' => $file_id,
        });

        if (!$file) {
            throw(
                '/file/update/file_not_found',
                "File with ID $file_id not found",
            );
        }

        # destroying is only allowed when the user has zaak_beheer
        # permissions. since the permissions engine is dependant on
        # the controller layer, determine permissions here.
        # Since this call is somewhat expensive (potential LDAP traffic,
        # db lookups) only do it if necessary.
        # BTW, the idea is that backend layer should be able to tell
        # permissions like $schema->current_user->check_any_zaak_permission('zaak_beheer'),
        # which will make this patch obsolete.
        if ($params->{destroyed}) {
            $params->{user_has_zaak_beheer_permissions} =
                $c->check_any_given_zaak_permission($file->case_id, 'zaak_beheer');
        }

        my $result = $file->update_properties($params);

        $c->{stash}->{json} = $result;
        $c->forward('Zaaksysteem::View::JSON');        
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');
    }
}

=head2 move_case_document

Moves a case document assignment from one file to another file.

=head3 Arguments

=over

=item from [required

=item to [required]

=item case_document_id [required]

=back

=head3 Location

POST: /file/move_case_document

=head3 Returns

The file it was assigned to.

=cut

sub move_case_document : JSON : Local {
    my ($self, $c) = @_;
    my $params     = $c->req->params;

    try {
        my $subject = _get_subject($c)->betrokkene_identifier;
    
        my $file_id = $params->{to};
        my ($file) = $c->model('DB::File')->search({
            id => $file_id,
        });

        if (!$file) {
            throw_controller_exception({
                error_code    => '/file/update/file_not_found',
                error_message => "File with ID $file_id not found",
            });
        }

        my $result = $file->move_case_document({
            from             => $params->{from},
            subject          => $subject,
            case_document_id => $params->{case_document_id},
        });

        $c->{stash}->{json} = $result;
        $c->forward('Zaaksysteem::View::JSON');        
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');
    }
}


=head2 search_queue

Returns all files currently in the document queue. (Accepted false)

=head3 Arguments

=over

=item visibility [required]

Restricts the output to files belonging to the subject or simply returning all files.

=item subject_id [optional]

Limit the search to a certain subject.

=item case_id [optional]

Get all files related to the given case id.

=back

=head3 Location

/file/search_queue/(subject_id|case_id)/1234

=head3 Returns

A list containing one or more JSON structures containing the file
properties.
 
=cut

sub search_queue : JSON : Chained('/') : PathPart('file/search_queue') : Args {
    my ($self, $c) = @_;

    my $params  = $c->req->params;
    my $visibility = $params->{visibility};

    if (!$visibility) {
        throw_controller_exception(
            code  => '/file/search_queue/visibility_required',
            error => 'Parameter for visibility needs to be set'
        );
    }

    try {
        # Only queued documents + default to no case_id/subject_id/date_deleted
        my %search_params;
        $search_params{accepted}     = 0;
        $search_params{case_id}      = undef;
        $search_params{subject_id}   = undef;
        $search_params{date_deleted} = undef;

        # Restrict context
        if ($params->{case_id}) {
            $search_params{case_id} = $params->{case_id};
        }
        elsif ($params->{subject_id}) {
            $search_params{subject_id} = $params->{subject_id};
        }

        # Visibility 'all' is a default by not restricting the intake_owner. With subject it needs to be set explicitly.
        if ($visibility eq 'subject' && $c->check_any_user_permission('documenten_intake_subject')) {
            $search_params{intake_owner} = _get_subject($c)->betrokkene_identifier;
        }
        # When 'all' is set, make sure the user executing this call is allowed to. No other parameters are required.
        elsif ($visibility eq 'all' && !$c->check_any_user_permission('documenten_intake_all')) {
            throw_controller_exception(
                code => '/file/search_queue/not_allowed',
                error => 'This user is not allowed to view all document intake files'
            );
        }

        $c->{stash}->{json} = $c->model('DB::File')->search({%search_params});
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');
    }
}

=head2 verify

Checks if the database's md5sum matches the filesystem md5sum.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/file/verify/file_id/1234

=head3 Returns

Returns a JSON hash with 1 upon succes, 0 for a failure.

=cut

sub verify : JSON : Regex('^file/verify/file_id/(\d+)') {
    my ($self, $c) = @_;
    try {
        my ($file_id) = @{$c->req->captures};

        my ($file) = $c->model('DB::File')->search({id => $file_id});
        if (!$file) {
            throw_controller_exception({
                error_code    => '/file/verify/file_not_found',
                error_message => "File with ID $file_id not found",
            });
        }
        my $result = $file->filestore->verify_integrity;

        $c->{stash}->{json} = { result => $result };
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');   
    }

}


=head2 get_thumbnail

Returns the thumbnail.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/file/thumbnail/file_id/1234

=head3 Returns

Returns the thumbnail data.

=cut

sub get_thumbnail : Regex('^file/thumbnail/file_id/(\d+)') {
    my ($self, $c) = @_;

    try {
        my ($file_id) = @{$c->req->captures};

        my ($file) = $c->model('DB::File')->search({id => $file_id});
        if (!$file) {
            throw_controller_exception({
                error_code    => '/file/get_thumbnail/file_not_found',
                error_message => "File with ID $file_id not found",
            });
        }

        # Generate thumbnail if it doesn't have one yet.
        if (!$file->filestore->thumbnail_uuid) {
            $file->filestore->generate_thumbnail;
        }

        my $thumbnail_path = $file->filestore->get_thumbnail_path;

        $c->serve_static_file($thumbnail_path);
        $c->res->headers->content_length(stat($thumbnail_path)->size);
        $c->res->headers->content_type('image/jpeg');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');   
    }
}

=head2 update_file

Replace a file. This results in a new file being created with the properties
of the current (/soon to be 'old') file. The only difference is that it will
have a higher version and updated file properties (md5, size, mimetype, etc.).

=head3 Arguments

=over

=item file_id [required]

=item file [required - mutex with existing_file_id]

The newly uploaded file.

=item existing_file_id [required - mutex with file]

An existing file entry can replace a file instead of uploading a new one.

The new file's data.

=item return_content_type

Option to override the content-type this call returns. Mostly for IE-compatibility.

=back

=head3 Location

POST: /file/update_file

=head3 Returns

A JSON structure containing the new file properties.

=cut

sub update_file : Local {
    my ($self, $c) = @_;

    $c->{stash}->{json_content_type} = $c->req->params->{return_content_type};

    try {
        my %params  = %{ $c->req->params };
        my $subject = _get_subject($c)->betrokkene_identifier;
        my $file_id = $params{file_id};

        my ($file) = $c->model('DB::File')->search({'me.id' => $file_id});
        if (!$file) {
            throw_controller_exception({
                error_code    => '/file/update_file/file_not_found',
                error_message => "File with ID $file_id not found",
            });
        }

        my $result;
        if ($params{existing_file_id}) {
            $result = $file->update_existing({
                subject          => $subject,
                existing_file_id => $params{existing_file_id}
            });
        }
        elsif ($params{file_id}) {
            my $upload  = $c->req->upload('file');

            $result  = $file->update_file({
                subject       => $subject,
                original_name => $upload->filename,
                new_file_path => $upload->tempname,
            });
        }

        $c->{stash}->{json} = $result;
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');   
    }
}

=head2 version_info

Get a version history tree for this file.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/file/version_info/file_id/1234

=head3 Returns

Returns a JSON hash keyed on version IDs.

=cut

sub version_info : JSON : Regex('^file/version_info/file_id/(\d+)') {
    my ($self, $c) = @_;

    try {
        my ($file_id) = @{$c->req->captures};

        my ($file) = $c->model('DB::File')->search({id => $file_id});
        if (!$file) {
            throw_controller_exception({
                error_code    => '/file/version_info/file_not_found',
                error_message => "File with ID $file_id not found",
            });
        }
        my $root_file = $file->get_root_file;
        my @files     = $root_file->files;

        # Add the starter file to the results. The files method only lists files
        # pointing to it.
        push @files, $root_file;

        my @loglines = $c->model('DB::Logging')->search({
            component    => 'document',
            component_id => [ map ({ $_->id } @files) ]
        });

        my @result;
        for my $f (@files) {
           push @result, {
                file_id         => $f->id,
                case_id         => $f->get_column('case_id'),
                version         => $f->version,
                name            => $f->name,
                extension       => $f->extension,
                creation_reason => $f->creation_reason,
                created_by      => $f->created_by,
                log             => [ grep { $f->id eq $_->component_id } @loglines ],

                # any reason why not use the TO_JSON pattern?
                date_created    => $f->date_created->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam')->iso8601,
                date_modified   => $f->date_modified->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam')->iso8601,
            }
        }

        $c->{stash}->{json} = {result => \@result};
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');   
    }
}

=head2 revert_to

Revert to a previous version of a file. This sets the version you
revert to as the last version.

Example:

File.jpg has 5 versions. The last 2 versions had a fault in them and
the user wishes to revert to version 3. Version_id is set to 3 and 
in the return value you will find the restored file with version 6.

=head3 Arguments

=over

=item file_id [required]

The file you wish to revert to.

=back

=head3 Location

POST: /file/revert_to/

=head3 Returns

The reverted file as a JSON hash.

=cut

sub revert_to : JSON : Local {
    my ($self, $c) = @_;

    try {
        my %params  = %{ $c->req->params };
        my $file_id = $params{file_id};
        my ($file) = $c->model('DB::File')->search({id => $file_id});
        if (!$file) {
            throw_controller_exception({
                error_code    => '/file/revert_to/file_not_found',
                error_message => "File with ID $file_id not found",
            });
        }

        my $subject = _get_subject($c)->betrokkene_identifier;
        my $result = $file->make_leading($subject);
        
        $c->{stash}->{json} = $result;
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');   
    } 
}

=head2 document_categories

Get a list of document categories and their allowed values.

=head3 Location

/file/document_categories

=head3 Returns

The document categories in a list of hashes.

=cut

sub document_categories : JSON : Local {
    my ($self, $c) = @_;

    my @categories = get_document_categories();
    $c->{stash}->{json} = \@categories;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 help

Returns a HTMLified version of the POD in this library.

=cut

sub help : Local {
    my ($self, $c) = @_;

    require Pod::Simple::HTML;
    my $p = Pod::Simple::HTML->new;
    $p->output_string(\$c->stash->{pod_output});
    $p->parse_file('lib/Zaaksysteem/Controller/File.pm');
    $c->stash->{template} = 'pod.tt';
}

sub _get_subject {
    my ($c) = @_;
    return $c->model('Betrokkene')->get(
        {
            intern  => 0,
            type    => 'medewerker',
        },
        $c->user->uidnumber,
    );
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Controller::Medewerker;

use strict;
use warnings;
use parent 'Catalyst::Controller';
use Data::Dumper;

use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;


sub prepare_page : Private {
    my ($self, $c) = @_;

    ### Define the basics
    $c->stash->{org_eenheden} = $c->model('Betrokkene')->search(
        {
            type    => 'org_eenheid',
            intern  => 0,
        },
        {}
    );
}


sub behandelaar : Global {
    my ($self, $c, $zaaktype_node_id) = @_;
    my ( $aanvrager_keuze );

    $c->stash->{template}               = 'form/list.tt';
    $c->session->{zaaksysteem}->{mode}  = $c->stash->{layout_type} = 'simple';


    $c->stash->{behandelaar_form}       = $c->session->{behandelaar_form} = 1;

    ### When id given, redirect to first step
    if ($zaaktype_node_id) {
        $c->res->redirect(
            $c->uri_for(
                '/zaak/create',
                {
                    mode                => 'behandelaar',
                    zaaktype            => $zaaktype_node_id,
                    create              => 1,
                    ztc_trigger         => 'intern',
                    betrokkene_type     => 'medewerker',
                    ztc_contactkanaal   => 'post',
                    ztc_aanvrager_id    => 'betrokkene-medewerker-' .  $c->user->uidnumber
                }
            )
        );
        $c->detach;
    }

    ### Let's get a list of zaaktypen
    $c->stash->{zaaktypen}  = $c->model('Zaaktype')->list(
        {
            'zaaktype_trigger'  => 'intern',
        },
    );
}

sub base : Chained('/') : PathPart('medewerker') : CaptureArgs(1) {
    my ($self, $c, $userid)  = @_;

}

sub index : Chained('/') : PathPart('medewerker') : Args(0) {
    my ($self, $c)      = @_;

    $c->assert_any_user_permission('admin');

    if ($c->req->params->{parent_dn}) {

        $c->stash->{people_entries}        = $c->model('Users')->get_tree_view(
            {
                'objectClass'   => ['posixAccount']
            },
            $c->req->params->{parent_dn}
        );
    } else {
        $c->stash->{ou_entries}        = $c->model('Users')->get_tree_view({
            'objectClass'   => ['organization', 'organizationalUnit']
        });
    }

    $c->stash->{rollen}        = $c->model('Users')->get_tree_view({
        'objectClass'   => ['organization', 'posixGroup','organizationalUnit']
    });

$c->log->debug(Dumper($c->stash->{rollen}));

    $c->stash->{template}       = 'medewerker/index.tt';
}

sub move_to_ou : Chained('/') : PathPart('medewerker/move_to_ou') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    my $result = $c->model('Users')->move_to_ou(
        {
            ou_dn     => $c->req->params->{ou_dn},
            entry_dn    => $c->req->params->{entry_dn}
        }
    );

    if ($result) {
        $c->stash->{json} = {
            'newdn'     => $result,
            'succes'    => 1,
            'bericht'   => 'Medewerker succesvol toegevoegd aan OU',
        };

        $c->model('DB::Logging')->trigger('user/update/ou', {
            component => LOGGING_COMPONENT_USER,
            data => {
                user_dn => $c->req->param('entry_dn'),
                ou_dn => $c->req->param('ou_dn')
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Medewerker kan niet toegevoegd worden aan ou.'
        };

    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

my $ADD_MAP = {
    'ou'    => {
        'required'  => [qw/ou/],
        'optional'  => [qw/parent/],
        'constraint_methods' => {
            'ou'    => qr/^[\w\d\s-]+$/,
        }
    },
    'role'  => {
        'required'  => [qw/role/],
        'optional'  => [qw/parent/],
        'constraint_methods' => {
            'role'    => qr/^[\w\d\s-]+$/,
        }
    }
};

sub add : Chained('/') : PathPart('medewerker/add') : Args(1) {
    my ($self, $c, $type ) = @_;

    unless ($type && $ADD_MAP->{$type}) {
        $c->log->error('Ongeldig type voor add');
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    }

    if ($type eq 'role' || $type eq 'ou') {
        $c->stash->{tree}        = $c->model('Users')->get_tree_view({
            'objectClass'   => ['organization', 'organizationalUnit']
        });
    }

    $c->stash->{type} = $type;

    if ($c->req->params->{confirmed}) {
        Params::Profile->register_profile(
            method  => 'add',
            profile => $ADD_MAP->{$type}
        );

        my $dv = Params::Profile->check(
            params  => $c->req->params,
        );

        my $opts    = $dv->valid;

        if ($dv->success) {
            my $addmethod = 'add_' . $type;

            if ((my $entry = $c->model('Users')->$addmethod($opts))) {
                $c->log->info(
                    'Users->add: Created entry'
                );

                $c->push_flash_message('Succesvol aangemaakt');

                my $msg;
                if ($dv->valid('role')) {
                    $msg    = 'Rol ' . $dv->valid('role') .
                            ' succesvol toegevoegd';
                } else {
                    $msg    = 'Organisatorische eenheid ' . $dv->valid('ou') .
                            ' succesvol toegevoegd';
                }

                $c->model('DB::Logging')->trigger($type . '/create', {
                    component => LOGGING_COMPONENT_USER,
                    data => $opts
                })
            }
        } else {
            $c->log->error('Invalid fields: ' . Dumper($dv));
            $c->push_flash_message('Aanmaken mislukt, naam van '
            . $type . ' niet correct');
        } 
    } else {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'medewerker/add.tt';
        $c->detach;
    }

    $c->res->redirect($c->uri_for('/medewerker'));
}

sub delete : Chained('/') : PathPart('medewerker/delete') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->params->{confirmed}) {
        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze entry wilt verwijderen?';
    
        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{params}     = {
            dn  => $c->req->params->{dn}
        };
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/medewerker/delete'
            );
    
        $c->forward('/page/confirmation');
        $c->detach;
    } 

    my $message;
    if ($c->req->params->{dn}) {
        if ($c->model('Users')->delete_entry($c->req->params->{dn})) {
            my $event = $c->model('DB::Logging')->trigger('role/remove', {
                component => LOGGING_COMPONENT_USER,
                data => { dn => $c->req->param('dn') }
            });

            $c->push_flash_message($event->onderwerp);
        } else {
            $c->push_flash_message('Entry met DN: ' . $c->req->params->{dn}
                . ' kan helaas niet verwijderd worden');
        }

    }

    $c->res->redirect($c->uri_for('/medewerker'));
}

sub delete_role_from : Chained('/') : PathPart('medewerker/delete_role_from') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    my $result = $c->model('Users')->delete_role_from(
        {
            role_dn     => $c->req->params->{role_dn},
            entry_dn    => $c->req->params->{entry_dn}
        }
    );

    if ($result) {
        $c->stash->{json} = {
            'succes'    => 1,
            'bericht'   => 'Rol succesvol verwijderd van medewerker.',
        };

        $c->model('DB::Logging')->trigger('user/role/remove', {
            component => LOGGING_COMPONENT_USER,
            data => {
                role_dn => $c->req->param('role_dn'),
                user_dn => $c->req->param('entry_dn')
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol kan niet verwijderd worden van medewerker.'
        };

    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub add_role_to : Chained('/') : PathPart('medewerker/add_role_to') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    my $result = $c->model('Users')->add_role_to(
        {
            role_dn     => $c->req->params->{role_dn},
            entry_dn    => $c->req->params->{entry_dn}
        }
    );

    if ($result) {
        $c->stash->{json} = {
            'succes'    => 1,
            'bericht'   => 'Rol succesvol toegevoegd aan medewerker',
        };

        $c->model('DB::Logging')->trigger('user/role/add', {
            component => LOGGING_COMPONENT_USER,
            data => {
                role_dn => $c->req->param('role_dn'),
                entry_dn => $c->req->param('entry_dn')
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol kan niet toegevoegd worden aan medewerker, bestaat het al?'
        };

    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}




sub update : Chained('/') : PathPart('medewerker/update_rol') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{medewerkers}    = $c->model('Users')->get_all_medewerkers;

    my $rawroles                   = {
        map {
            my $key = $_;
            $key    =~ s/zsmw_//;
            $key    => $c->req->params->{$_}
        } grep(/^zsmw_/, keys %{ $c->req->params })
    };

    my $roles;
    for my $role (keys %{ $rawroles }) {
        if (UNIVERSAL::isa($rawroles->{$role}, 'ARRAY')) {
            $roles->{$role} = $rawroles->{$role};
        } else {
            $roles->{$role} = [ $rawroles->{$role} ];
        }
    }

    #### Run it
    while (my ($username, $roles) = each %{ $roles }) {
        $c->log->debug('USERNAME: ' . $username . ' / Roles: ' . Dumper($roles));
        $c->model('Users')->deploy_user_in_roles(
            $username, $roles
        );
    }

    $c->res->redirect($c->uri_for('/medewerker'));

    $c->detach;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


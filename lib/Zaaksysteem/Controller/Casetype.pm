package Zaaksysteem::Controller::Casetype;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Casetype - ZAPI Controller

=head1 SYNOPSIS

 # /casetype

=head1 DESCRIPTION

Zaaksysteem API Controller for Casetypes

=head1 ACTIONS

=head2 base

Reserves the C</casetype> URI namespace.

=cut

sub base : Chained('/') : PathPart('casetype') : CaptureArgs(0) { }

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $casetype_id) = @_;

    if ($casetype_id && $casetype_id =~ /^[0-9]+$/) {
        $c->stash->{zaaktype} = $c->model('DB::Zaaktype')->find($casetype_id)
            || $c->error('Invalid zaaktype id given');
    }
}

=head2 zaaktype_search

Returns a resultset of L<Zaaksysteem::Schema::Zaaktype> rows, optionally
filtered by the C<query> GET parameter.

=head3 URL

C</casetype>

=head3 Query parameters

=over 4

=item query

Type: STRING

 # /casetype?query=test

Filters the results with the given query string

=back

=cut

sub zaaktype_search : Chained('base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';

    my $offline = $c->req->params->{inactive} ? 1 : 0;

    my $search = {
            "me.deleted" => undef,
            "me.active" => (
                $offline
                    ? ([0, 1])
                    : (1)
            ),
        };
    my $query = $c->req->params->{query} || '';
    if ($query) {
        $search->{search_term} = { 'ilike' => '%' . lc($query) . '%' },
    }

    $c->stash->{zapi} = $c->model('DB::Zaaktype')->search($search);
}

=head2 casetype_search

Returns a resultset of L<Zaaksysteem::Object::Types::Casetype> instances,
optionally filtered by the C<query> GET parameter.

=head3 URL

C</casetype/object>

=head3 Query parameters

=over 4

=item query

Filters the results with the given string

=back

=cut

define_profile casetype_search => (
    optional => {
        query => 'Str'
    }
);

sub casetype_search : Chained('base') : PathPart('object') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;
    my $rs = $c->model('Object')->rs->search({ object_class => 'casetype' });

    if ($params->{ query }) {
        $rs = $rs->search_text_vector($params->{ query });
    }

    $c->stash->{ zapi } = $rs;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

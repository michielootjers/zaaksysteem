package Zaaksysteem::Controller::Beheer::Configuration;

use Moose;
use Data::Dumper;

BEGIN { extends 'Catalyst::Controller'; }


sub configuration : Path {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my $advanced = $c->req->param('advanced') ? 1 : 0;

    $c->stash->{notifications} = [$c->model('DB::BibliotheekNotificaties')->search({}, {
        order_by => 'label'
    })->all];

    $c->stash->{config} = $c->model('DB::Config')->get_all($advanced);
    $c->stash->{ template } = 'beheer/configuration.tt';
}


sub save : Local {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my $advanced = $c->req->param('advanced') ? 1 : 0;

    $c->model('DB::Config')->save($c->req->params, $advanced);

    $c->push_flash_message('Instellingen opgeslagen');
    $c->forward('configuration');
}


1;
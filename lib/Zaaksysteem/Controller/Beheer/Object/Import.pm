package Zaaksysteem::Controller::Beheer::Object::Import;

use Moose;

use Params::Profile;
use Data::Dumper;

use Zaaksysteem::Object::Importer;
use Zaaksysteem::Object::ImportState;

use constant OBJECT_TYPES => [
    {
        type => 'casetype',
        label => 'Zaaktypen',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' },
            { type => 'inav', label => 'I-Navigator (versie 1.5.02)' },
            { type => 'icon', label => 'I-Controller' },
            { type => 'ztc2', label => 'KING ZTC2.0 (Referentiezaaktypen)' }
        ]
    },

    {
        type => 'case',
        label => 'Zaken',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' }
        ]
    },

    {
        type => 'product',
        label => 'Producten en diensten',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' },
            { type => 'sim', label => 'Kluwer (SIM)' },
            { type => 'sdu', label => 'SDU' }
        ]
    },

    {
        type => 'qa',
        label => 'Vragen en antwoorden',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' }
        ]
    }
];

BEGIN { extends 'Catalyst::Controller'; }

sub base : Chained('/') : PathPart('beheer/object/import') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ page_title } = 'Objecten importeren';
    $c->stash->{ object_types } = OBJECT_TYPES;
    $c->stash->{ state } = Zaaksysteem::Object::ImportState->new(
        %{ $c->session->{ _import_state } }
    );
}

sub index : Chained('base') : PathPart('') : Args(1) {
    my ($self, $c, $library_id) = @_;

    $c->stash->{ library_id } = $library_id;

    if($c->stash->{ state }->import_fileref) {
        $c->stash->{ fileref } = $c->model('DB::Filestore')->find_by_uuid(
            $c->stash->{ state }->import_fileref
        );
    }

    $c->stash->{ nowrapper } = $c->req->is_xhr;

    $c->stash->{ template } = $c->req->is_xhr ? 
        'beheer/object/import_config_form.tt' :
        'beheer/object/import.tt';
}

Params::Profile->register_profile(
    method => 'validate',
    profile => {
        required => [qw[object_type file_format filestore_uuid library_id]]
    }
);

sub validate : Chained('base') : PathPart('validate') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->method eq 'POST') {
        $c->res->redirect($c->uri_for('/beheer/object/import/' . $c->req->param('library_id')));
        $c->detach;
    }

    my $params = $c->req->params();
    my $filestore_uuid = $params->{ filestore_uuid };

    unless($filestore_uuid) {
        $c->forward('/upload/upload');
        $params->{filestore_uuid} = $c->stash->{uuid};
    }

    
    my $dv = Params::Profile->check(params => $params);

    if($dv->has_missing) {
        die('You can has all of my nopes, missing field(s)');
    }

    if($dv->has_invalid) {
        die('You can has all of my nopes, invalid field(s)');
    }

    my $fileref = $c->model('DB::Filestore')->find_by_uuid($dv->valid->{ filestore_uuid });

    unless($fileref) {
        die('Filestore did not contain referenced file.');
    }

    my $importer;
    my $state = Zaaksysteem::Object::ImportState->new(
        library_id => $dv->valid->{ library_id }
    );

    eval {
        $importer = Zaaksysteem::Object::Importer->new(
            format => $dv->valid->{ file_format },
            object_type => $dv->valid->{ object_type },
            schema => $fileref->result_source->schema,
            state => $state
        );

        $importer->hydrate_from_files($fileref);
    };

    if($@) {
        $c->log->debug('Unable to initialize importer, ' . $@);
        
        $c->push_flash_message('Importbestand niet geldig');

        $c->res->redirect($c->uri_for('/beheer/object/import/' . $params->{ library_id }));
        $c->detach;
    }

    $c->session->{ _import_state } = $state->get_state;

    $c->forward($importer->redirect) if($importer->redirect);

    $c->res->redirect($c->uri_for('/beheer/object/import/configure_items'));
    $c->detach;
}

sub configure_items : Chained('base') : PathPart('configure_items') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ fileref } = $c->model('DB::Filestore')->find_by_uuid($c->stash->{ state }->import_fileref);

    $c->stash->{ bib_cat } = $c->model('DB::BibliotheekCategorie')->search(
        { system => undef, pid => undef },
        { order_by => ['pid', 'naam'] }
    );

    $c->stash->{ template } = 'beheer/object/import_items_options.tt';
}

Params::Profile->register_profile(
    method => 'run',
    profile => {
        required => [qw[object_type file_format filestore_uuid]]
    }
);

sub run : Chained('base') : PathPart('run') : Args(0) {
    my ($self, $c) = @_;

    my @unfinished = $c->stash->{ state }->find_by('selected_option', '');

    if(scalar(@unfinished)) {
        $c->push_flash_message('Er zijn nog items zonder geconfigureerde actie');
        $c->res->redirect($c->uri_for('/beheer/object/import/configure_items'));
        $c->detach;
    }

    my $importer = Zaaksysteem::Object::Importer->new( 
        schema => $c->model('DB::Filestore')->search({})->result_source->schema,
        state => $c->stash->{ state },
        format => $c->stash->{ state }->format,
        object_type => $c->stash->{ state }->object_type
    );

    $importer->execute;

    $c->session->{ _import_state } = $c->stash->{ state }->get_state;
    $c->res->redirect($c->uri_for('/beheer/object/import/finish'));
    $c->detach;
}

Params::Profile->register_profile(
    method => 'save_option',
    profile => {
        required => [qw[item_id option]]
    }
);

sub save_option : Chained('base') : PathPart('save_option') : Args(0) {
    my ($self, $c) = @_;

    if($c->req->method eq 'POST') {
        my $item = $c->stash->{ state }->get_by(
            'id',
            $c->req->param('item_id')
        );

        my $selected = $c->req->param('option');

        my %config;

        for my $key (grep { $_ =~ m[^${selected}\.] } keys %{ $c->req->params }) {
            my ($config_key) = $key =~ m|^${selected}\.(.*)$|;

            $config{ $config_key } = $c->req->params->{ $key };
        }

        $item->select_option($selected, \%config);

        # Save modified state back to the session
        $c->session->{ _import_state } = $c->stash->{ state }->get_state;
    }

    $c->res->status(200);
    $c->res->body('');
}

sub finish : Chained('base') : PathPart('finish') : Args(0) {
    my ($self, $c) = @_;

    if($c->stash->{ state }->import_fileref) {
        $c->stash->{ fileref } = $c->model('DB::Filestore')->find_by_uuid(
            $c->stash->{ state }->import_fileref
        );
    }

    $c->session->{ _import_state } = { };
    $c->stash->{ template } = 'beheer/object/import_finish.tt';
}

1;

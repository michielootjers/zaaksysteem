package Zaaksysteem::Controller::Beheer::Sysin;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub base : Chained('/') : PathPart('beheer/sysin') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
}

sub overview : Chained('base') : PathPart('overview') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/overview.tt'
}

sub transactions : Chained('base') : PathPart('transactions') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/transactions.tt'
}

sub transaction_base : Chained('base') : PathPart('transactions') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{transaction}    = $c->model('DB::Transaction')->find($id);
    $c->stash->{transaction_id} = $id;
}


sub transaction : Chained('transaction_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/transaction-single.tt'
}

sub record_base : Chained('transaction_base') : PathPart('records') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{record}    = $c->model('DB::TransactionRecord')->find($id);
    $c->stash->{record_id} = $id;
}

sub record_mutation : Chained('record_base') : PathPart('mutation') {
    my ($self, $c, $id) = @_;

    $c->stash->{ mutation_id } = $id;
    $c->stash->{ template } = 'beheer/sysin/mutation-single.tt'
}

sub record : Chained('record_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/record-single.tt'
}

1;

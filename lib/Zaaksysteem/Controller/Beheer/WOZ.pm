package Zaaksysteem::Controller::Beheer::WOZ;
use Moose;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Catalyst::Controller'; }

sub index :Path :Args(0) {
    my ( $self, $c, $updated ) = @_;
  
    $c->assert_any_user_permission('woz_objects');

    unless($c->customer_instance->{start_config}->{woz_info_on_pip}) {
        $c->res->redirect("/");
        $c->detach();
    }  

    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    foreach my $setting (@$settings) {
        $c->stash->{    
            $setting->key
        } = $setting->value;
    }

    $c->stash->{dagobert_editor} = 1;
    $c->stash->{template}   = 'beheer/import/woz/admin.tt';
}


sub updated : Local {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('woz_objects');

    unless($c->customer_instance->{start_config}->{woz_info_on_pip}) {
        $c->res->redirect("/");
        $c->detach();
    }  
    
    $c->stash->{updated} = 1;

    $c->forward('index');    
}


sub update : Local {
    my ($self, $c) = @_;    

    $c->assert_any_user_permission('woz_objects');

    unless($c->customer_instance->{start_config}->{woz_info_on_pip}) {
        $c->res->redirect("/");
        $c->detach();
    }  

    my $params = $c->req->params();

    # dirty
    $params->{settings_woz_zaaktype_id} = $params->{zaaktype_id};

    # checkbox, need to explicify
    $params->{settings_woz_pip_comments} = !!$params->{settings_woz_pip_comments};

    if($params->{filestore_uuid}) {
        my $filestore_uuid = $params->{filestore_uuid};
        
        my $filestore_rs = $c->model("DB::Filestore") or die "unable to get filestore resultset";

        my $file = $filestore_rs->find({
            uuid => $filestore_uuid,
        });
        
        die "can't find uploaded file, aborting" unless $file;
        die "uuid ID for file is not set" unless $file->uuid;

        my $path = $file->get_path;
        die "can find path for file" unless $path;

        my $tmp_location = $c->model('DB::Config')->get_value('tmp_location');

        eval {
            $c->model('DB::WozObjects')->import_woz({
                file_path   => $path,
                tmp_dir     => $tmp_location . "/"
            });
    
            $c->model('DB::Logging')->trigger('woz/import/upload', {
                component => 'woz_objects',
                data => {
                    file_id => $file->id,
                    file_uuid => $filestore_uuid
                }
            });
        };

        if($@) {
            $c->stash->{error} = "Error during WOZ import: " . $@;
            $c->stash->{template}   = 'beheer/import/woz/admin.tt';
            $c->detach();
        }
    }

    my $settings_rs = $c->model('DB::Settings');
    foreach my $param (keys %$params) {
        if($param =~ m|^settings_(.*)|) {
            my $key = $1;
                            
            $settings_rs->store({
                key     => $key, 
                value   => $params->{$param}
            });
        }
    }

    $c->model('DB::Logging')->trigger('woz/update', {
        component => 'woz_objects'
    });

    $c->res->redirect('/beheer/woz/updated');
    $c->detach();
}

sub view : Local: Args() {
    my ($self, $c, $owner, $id) = @_;

    $c->assert_any_user_permission('woz_objects');

    if($owner) {
        $c->stash->{owner} = $owner;
        $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search({owner => $owner});
        if($id) {
            $c->stash->{woz_objects} = $c->stash->{woz_objects}->search({'me.id' => $id});
        }
        $c->stash->{template} = "beheer/import/woz/ownerobjects.tt";
    } else {
        $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search({}, {
            select => [ 'owner', { count => 'me.id' } ],
            as => [qw/owner count_per_owner/],
            group_by => 'owner',
            rows => 900
        });
        $c->stash->{template} = "beheer/import/woz/list.tt";
    }
}

sub retrieve_object : Private {
    my ($self, $c, $id) = @_;

    my $params = $c->req->params;
    die "need owner and object_id" unless $params->{owner} && $params->{object_id};

    $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search({
        id => $id,
        owner => $params->{owner},
        object_id => $params->{object_id}
    });

}


sub object : Local: Args() {
    my ($self, $c, $id) = @_;

    $c->forward('retrieve_object', [$id]);
    $c->stash->{template} = "beheer/import/woz/ownerobjects.tt";
}


sub report : Local : Args() {
    my ($self, $c, $id) = @_;

    $c->forward('retrieve_object', [$id]);

    $c->stash->{extra_body_class} = ' woz-body';
    $c->stash->{layout_type} = 'simple';
    $c->stash->{template} = "plugins/woz/report.tt";
}

__PACKAGE__->meta->make_immutable;


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


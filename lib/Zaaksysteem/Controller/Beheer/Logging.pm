package Zaaksysteem::Controller::Beheer::Logging;

use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );
use Data::Dumper;
use Zaaksysteem::Constants;

BEGIN {extends 'Catalyst::Controller'; }


sub list : Chained('/') : PathPart('beheer/logging') : Args() {
	my ($self, $c) = @_;

    $c->assert_any_user_permission('beheer');

	my $params = $c->req->params();

    my $page       = $params->{'page'} || 1;
	my $textfilter = $params->{'textfilter'} || '';

    # In geval er een verschil in textfilter is de page op 1 zetten;
    # zodat we de results zien (indien de page niet op 1 stond)!
    my $session_textfilter = $c->session->{'textfilter'} || '';
    if ($session_textfilter ne $textfilter) {
        $page = 1;

        # Voor de template pager2.tt
        $c->req->params->{'page'} = $page;
    }
    $c->session->{'textfilter'} = $textfilter; 

# de sort_direction moet omdraaien als een zelfde sorteer_selectie voor de tweede keer wordt opgevraagd
# als je een component kiest moet daar uiteraard niet op worden gesorteerd, alle records hebben dat compoentn
# het gekozen component wordt opgeslagen in de sessie.

    my $previous_component      = $c->session->{logging_component} || '';
    my $previous_sort_direction = $c->session->{logging_sort_direction} || '';
    my $current_component       = $c->session->{logging_component} = $params->{component} || '';

    my $sort_direction = '-desc';
    my $sort_field = 'created';

#    $c->log->debug("current_copmonent: " . $current_component . ", prev: " . $previous_component);

    if($current_component ne $previous_component) {
        delete $c->session->{logging_sort_direction};
    } else {
        my $new_sort_field      = $params->{sort_field};
        my $previous_sort_field = $c->session->{logging_sort_field};

        if($new_sort_field) {
            $sort_field = $new_sort_field;

            if(
                $new_sort_field eq $previous_sort_field &&
                $previous_sort_direction eq '-desc'
            ) {
                $sort_direction = '-asc';
            }
        }
    }

    $c->session->{logging_sort_direction} = $sort_direction;
    $c->session->{logging_sort_field} = $sort_field;
    
    $c->stash->{results_per_page} = $params->{results_per_page} || 10;

#    $c->log->debug("sort_field: " . $sort_field . ", sort_direction: " . $sort_direction);

    $c->stash->{results} = $c->model('DB::Logging')->search({
        component => { '-not' => undef}
    }
    , {
        rows        => $c->stash->{results_per_page},
        order_by    => {$sort_direction => $sort_field },
    });

# todo: include logging in objectsearch.
# create a resultset for searchable
# move search routines there
    if($textfilter) {
        my $query = {
            '-or' => [
                { 'lower(onderwerp)' => { ilike    => '%' . lc($textfilter) . '%'} },
                { 'cast(zaak_id as text)'   => { ilike    => '%' . lc($textfilter) . '%'} },
            ],
        };

        $c->stash->{results} = $c->stash->{results}->search($query);
    }


    if($params->{component}) {
        $c->stash->{results} = $c->stash->{results}->search({component => $params->{component}});
        $c->stash->{current_component} = $params->{component};
    }

    if($params->{view} && $params->{view} eq 'csv') {
        $c->forward('export_as_csv');
        $c->detach();
    }
    


    # wait with paging till after we've branched for csv export
    $c->stash->{results}  = $c->stash->{results}->search({}, {page=>$page});

    my $components = $c->model('DB::Logging')->search({
        component => {'-not'=> undef} 
    }, {
        select => [ {count => 'me.component'}, 'component' ],
        as => ['count', 'component'],
        group_by => 'me.component',
    });

    $components->result_class(
        'DBIx::Class::ResultClass::HashRefInflator');

    my @components = $components->all;
    $c->stash->{components} = \@components;
	$c->stash->{template} = 'beheer/logging.tt';

}


sub export_as_csv : Chained('/') : PathPart('beheer/logging/export_as_csv') : Args() {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('beheer');

    my $resultset   = $c->stash->{results};

    ### Make sure we get every row, by setting page size to maximum
    $resultset      = $resultset->search({}, {rows => undef});

    my @results = ();

    $resultset->result_class(
        'DBIx::Class::ResultClass::HashRefInflator');
  

    my @header_row = qw/id onderwerp component created betrokkene_id/;
    push @results, \@header_row;

    while(my $row = $resultset->next) {
        my @csv_row = (
            $row->{id},
            $row->{onderwerp},
            $row->{component},
            $row->{created},
            $row->{betrokkene_id},
        );
        push @results, \@csv_row;
    }

    $c->stash->{csv} = { data => \@results };
    my $csv = $c->view('CSV')->render($c, $c->stash);

    use Encode qw(encode);
    $csv = encode('UTF-8', $csv);

    $c->res->headers->header( 'Content-Type'  => 'application/x-download' );
    $c->res->headers->header(
        'Content-Disposition'  =>
            "attachment;filename=zaaksysteem-logging-" . time()
            . ".csv\n\n"
    );

	$c->res->body($csv);
}


    
sub _search_table {
    my ($self, $params) = @_;

    my $c          = $params->{c};
    my $query      = $params->{query};
    my $textfilter = $params->{textfilter};
    my $tablename  = $params->{tablename};
    my $bibliotheek_categorie_id  = $params->{bibliotheek_categorie_id};

    if($textfilter) {
        $query->{'lower(search_term)'} = {
            like    => '%' . lc($textfilter) . '%'
        };

        my $category_field = $tablename eq 'BibliotheekCategorie' ? 'pid' : 
            'bibliotheek_categorie_id';

        delete $query->{$category_field};

        if($bibliotheek_categorie_id) {
            
            my $child_category_ids = $self->_list_child_categories({
                c           => $c, 
                bibliotheek_categorie_id => $bibliotheek_categorie_id,
            });

            $query->{$category_field} = {
                '-in' => $child_category_ids
            };
        }
    }

    my $resultset = $c->model('DB::'.$tablename)->search($query, {
            select      => ['id', 'object_type', 'search_term'],
            order_by    => 'search_term',
    })->as_subselect_rs->search(undef, {
            select      => ['id', 'object_type', 'search_term'],
    });

    $resultset->result_class(
        'DBIx::Class::ResultClass::HashRefInflator');
        
    return $resultset;
} 








Params::Profile->register_profile(
    method  => '_filter',
    profile => {
        required => [ qw/textfilter items/ ]
    }
);


sub _filter {
    my ($self, $params) = @_;
    
    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _filter" unless $dv->success;

    my $textfilter  = $params->{textfilter};
    my $items       = $params->{items};

	my @results = ();
	foreach my $item (@$items) {
		if($item->{naam} =~ m|$textfilter|) {
			push @results, $item;
		}
	}
	return @results;
}



__PACKAGE__->meta->make_immutable;

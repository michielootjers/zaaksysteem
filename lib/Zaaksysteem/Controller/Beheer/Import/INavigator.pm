package Zaaksysteem::Controller::Beheer::Import::INavigator;

use Moose;
use Data::Dumper;

use Zaaksysteem::Zaaktypen::INavigator qw/GENERAL_FIELDS DOCUMENT_FIELDS RESULT_FIELDS CHECKLIST_FIELDS/;

BEGIN { extends 'Catalyst::Controller' }


=head2 view

Display an HTML page with a list of I-Navigator casetypes, passing
the complete settings structure from the parsed XML. The GUI gets
the mess with the settings, and then submit changes to the backend.

=cut

sub view : Path {
    my ($self, $c) = @_;

    $c->detach('/beheer/bibliotheek/list') unless $c->session->{inavigator_import_uuid};

    my $filename = $c->model('DB::Filestore')->find({
        uuid => $c->session->{inavigator_import_uuid},
    })->get_path;

    $c->stash->{casetypes} = $c->model('Zaaktypen')->inavigator_xml_to_json({
        filename => $filename
    });

    $c->forward('/api/casetype/inavigator/existing_casetypes');

    $c->stash->{GENERAL_FIELDS} = [GENERAL_FIELDS];
    $c->stash->{RESULT_FIELDS} = [RESULT_FIELDS];
    $c->stash->{DOCUMENT_FIELDS} = [DOCUMENT_FIELDS];
    $c->stash->{CHECKLIST_FIELDS} = [CHECKLIST_FIELDS];

    $c->stash->{template}  = 'beheer/import/inavigator/import.tt';
}


=head2 upload

Receive an uploaded file. Unzip this file in a temporary directory and store the
zip information in the session.

For IE, the uploaded file is send to this controller, for spanky browsers we receive
a UUID with which a Filestore obj can be obtained.

=cut

sub upload : Local {
    my ($self, $c) = @_;

    delete $c->session->{inavigator_import_uuid};

    my $params = $c->req->params;

    my $uuid = $params->{filestore_uuid};
    unless ($uuid) {
        # IE
        $c->forward('/upload/index');

        $uuid = $c->stash->{uuid};
    }

    throw ("inavigator/upload/uuid_missing", "uuid for uploaded file missing") unless $uuid;

    $c->session->{inavigator_import_uuid} = $uuid;

    $c->res->redirect($c->uri_for('/beheer/import/inavigator'));
    $c->detach();
}





__PACKAGE__->meta->make_immutable;


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Controller::Beheer::Bibliotheek;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Controller::Beheer::Bibliotheek - Controller for our catalogue

=head1 METHODS

=cut

use Data::Dumper;
use Hash::Merge::Simple qw( clone_merge );
use Zaaksysteem::Constants;
use Zaaksysteem::XML::Zaaksysteem::Serializer;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/bibliotheek'): CaptureArgs(0) {
    my ( $self, $c ) = @_;

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

}


=head2 export_to_xml

Export all catalogue attributes to XML.

=cut

sub export_to_xml : Chained('/') : PathPart('beheer/bibliotheek/export/xml'): Args() {
    my ($self, $c) = @_;

    my $s = Zaaksysteem::XML::Zaaksysteem::Serializer->new(
        schema => $c->model('DB')->schema,
    );

    $c->res->headers->header(
        'Content-Disposition',
        sprintf('attachment; filename="%s"', sprintf("zs-%s-%s.%s", 'catalogue', DateTime->now->iso8601, 'xml')),
    );
    $c->res->content_type('text/xml');
    $c->res->body($s->catalogue_to_xml());

    return 1;
}

sub list : Chained('/') : PathPart('beheer/bibliotheek') : Args() {
    my ($self, $c, $bibliotheek_categorie_id) = @_;

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    if($bibliotheek_categorie_id) {
        $c->stash->{bibliotheek_categorie_id} = $bibliotheek_categorie_id;

        my $parent_categorie = $c->model('DB::BibliotheekCategorie')->
            find($bibliotheek_categorie_id);

        unless($parent_categorie) {
            $c->res->redirect($c->uri_for('/beheer/bibliotheek'));
            $c->detach;
        }

        if($parent_categorie && $parent_categorie->pid) {
            $c->stash->{parent_categorie_id} = $parent_categorie->pid->id;
        }
    }


    my $params = $c->req->params();

    my $page = $params->{'page'} || 1;

    $c->stash->{results_per_page} = $c->req->params->{results_per_page} || 10;

    my $textfilter = $params->{'textfilter'} // '';

    # In geval er een verschil in textfilter is de page op 1 zetten;
    # zodat we de results zien (indien de page niet op 1 stond)!
    my $session_textfilter = $c->session->{'textfilter'} // '';

    if ($session_textfilter ne $textfilter) {
        $page = 1;

        # Voor de template pager2.tt
        $c->req->params->{'page'} = $page;
    }

    $c->session->{'textfilter'} = $textfilter;

    my $tables = {
        BibliotheekCategorie => {
            pid => $bibliotheek_categorie_id
        },
        BibliotheekKenmerken => {
            bibliotheek_categorie_id    => $bibliotheek_categorie_id,
            deleted                     => undef,
        },
        BibliotheekSjablonen => {
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
            deleted                     => undef,
        },
        Zaaktype => {
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
            deleted => undef,
        },
        BibliotheekNotificaties => {
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
            deleted => undef,
        },
        ObjectBibliotheekEntry => {
            bibliotheek_categorie_id => $bibliotheek_categorie_id
        }
    };


    my $resultsets = {};
    foreach my $tablename (keys %$tables) {

        $resultsets->{$tablename} = $self->_search_table({
            query => $tables->{$tablename},
            textfilter => $textfilter,
            tablename => $tablename,
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
            c =>$c,
        });
    }


    $c->stash->{results} = $resultsets->{BibliotheekCategorie}->union_all([
        $resultsets->{Zaaktype},
        $resultsets->{BibliotheekSjablonen},
        $resultsets->{BibliotheekKenmerken},
        $resultsets->{BibliotheekNotificaties},
        $resultsets->{ObjectBibliotheekEntry}
    ])->search(undef, {
        page => $page,
        rows => $c->stash->{results_per_page},
    });


    $self->_get_descriptions($c);

    if($bibliotheek_categorie_id) {
        my $categorie = $c->model('Bibliotheek::Categorie');

        my $crumbs = $categorie->get_crumb_path({
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
        });
        $c->stash->{crumbs} = $crumbs;
    }

    $c->stash->{get_element_count} = sub { $self->_get_element_count(@_) };
    $c->stash->{template} = 'beheer/catalogue.tt';
}


# TODO make params profile
sub _get_element_count {
    my ($self, $params) = @_;

    my $c = $params->{c};
    my $bibliotheek_categorie_id = $params->{bibliotheek_categorie_id};

    my $child_category_ids = $self->_list_child_categories({
        c           => $c,
        bibliotheek_categorie_id => $bibliotheek_categorie_id,
    });


    my $total = 0;
    foreach my $table (qw/BibliotheekKenmerken BibliotheekSjablonen BibliotheekNotificaties Zaaktype/) {
        my $count = $c->model('DB::' .$table)->search({
            bibliotheek_categorie_id    => { '-in' => $child_category_ids },
            deleted                     => undef,
        })->count();

        $total += $count;
    }

    # Special case for ObjectBibliotheekEntry table, it has no notion of 'deleted'

    $total += $c->model('DB::ObjectBibliotheekEntry')->search({
        bibliotheek_categorie_id => { -in => $child_category_ids },
    });

    return $total;
}



sub _search_table {
    my ($self, $params) = @_;

    my $c          = $params->{c};
    my $query      = $params->{query};
    my $textfilter = $params->{textfilter};
    my $tablename  = $params->{tablename};
    my $bibliotheek_categorie_id  = $params->{bibliotheek_categorie_id};

    if($textfilter) {
        $query->{'lower(search_term)'} = {
            like    => '%' . lc($textfilter) . '%'
        };

        my $category_field = $tablename eq 'BibliotheekCategorie' ? 'pid' :
            'bibliotheek_categorie_id';

        delete $query->{$category_field};

        if($bibliotheek_categorie_id) {

            my $child_category_ids = $self->_list_child_categories({
                c           => $c,
                bibliotheek_categorie_id => $bibliotheek_categorie_id,
            });

            $query->{$category_field} = {
                '-in' => $child_category_ids
            };
        }
    }

    my $resultset = $c->model('DB::'.$tablename)->search($query, {
            select      => ['id', 'object_type', 'search_term'],
            order_by    => 'search_term',
    })->as_subselect_rs->search(undef, {
            select      => ['id', 'object_type', 'search_term'],
    });

    $resultset->result_class(
        'DBIx::Class::ResultClass::HashRefInflator');

    return $resultset;
}


sub categoriebase : Chained('/') :
    PathPart('beheer/bibliotheek/categorie'):
    CaptureArgs(1)
{
    my ( $self, $c, $id ) = @_;

    $c->stash->{bibliotheek_categorie_id} = $id;
    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);
}

sub categories : JSON : Chained('/') : PathPart('beheer/bibliotheek/categories') {
    my ($self, $c) = @_;

    $c->stash->{json} = $c->model('DB::BibliotheekCategorie')->search({});
    $c->forward('Zaaksysteem::View::JSON');
}


Zaaksysteem->register_profile(
    method  => 'categorie_bewerken',
    profile => {
        'required'      => [],
        'optional'      => [],
    }
);

sub categorie_bewerken : Chained('categoriebase'): PathPart('bewerken'): Args() {
    my ($self, $c, $pid)   = @_;

    $c->stash->{pid}        = $pid;

    my ($categorie);

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'beheer/bibliotheek/categorie/edit.tt';

    my $params = $c->req->params;

    if ($params->{update}) {

        $self->_update_category({
            naam => $params->{naam},
            pid  => $pid,
            c => $c,
        });

    } elsif ($c->stash->{bibliotheek_categorie_id}) {
        $c->stash->{categorie} = $c->model('DB::BibliotheekCategorie')->find(
            $c->stash->{bibliotheek_categorie_id}
        );
    }
}




Params::Profile->register_profile(
    method  => '_update_category',
    profile => {
        required => [ qw/c naam/ ]
    }
);


sub _update_category : Private {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _update_category" unless $dv->success;

    my $c = $params->{c};
    my $categorie;


    my $args = {
        naam => $params->{naam}
    };
    if ($params->{pid}) {
        $args->{pid} = $params->{pid};
    }


    my $bibliotheek_categorie_id = $c->stash->{bibliotheek_categorie_id};

    if ($bibliotheek_categorie_id) {
        $categorie = $c->model('DB::BibliotheekCategorie')->find($bibliotheek_categorie_id);
    }

    if (
        (!$categorie || $categorie->naam ne $args->{naam}) &&
        $c->model('DB::BibliotheekCategorie')->search(
            $args
        )->count
    ) {
        $c->zcvalidate(
            {
                invalid => ['naam'],
                msgs    => {
                    naam    => 'Categorie bestaat al'
                }
            }
        );

        $c->log->error(
            'Bibliotheek->categorie_bewerken: Categorie "'
            . $args->{naam} . '" bestaat al.'
        );
        $c->detach;
    } elsif ($c->req->is_xhr &&
        exists($c->req->params->{do_validation})
    ) {
        $c->zcvalidate({success => 1 });
        $c->detach;
    }


    if (!$categorie) {
        $categorie = $c->model('DB::BibliotheekCategorie')->create(
            $args
        );
    } else {
        $categorie->naam($params->{naam});
        $categorie->update;
    }

    $c->push_flash_message('Bibliotheek categorie ' . $categorie->naam
            . ' succesvol bijgewerkt');
    $c->res->redirect(
        $c->uri_for(
            '/beheer/bibliotheek/' .  $categorie->id
        )
    );
    $c->detach;
}




Zaaksysteem->register_profile(
    method  => 'categorie_verwijderen',
    profile => {
        'required'      => [],
        'optional'      => [],
    }
);

sub categorie_verwijderen : Chained('categoriebase'): PathPart('verwijderen'): Args() {
    my ($self, $c)   = @_;

    my $params = $c->req->params;

    unless($params->{confirmed}) {

        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze categorie wilt verwijderen?';

#        $c->stash->{confirmation}->{commit_message}    =1;

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/beheer/bibliotheek/categorie/' . $c->stash->{bibliotheek_categorie_id}
                .'/verwijderen'
            );

        $c->forward('/page/confirmation');
        $c->detach;

    }


    # confirmed
    my $categorie = $c->model("DB::BibliotheekCategorie")->find(
        { id => $c->stash->{bibliotheek_categorie_id}}
    );

    my $pid = $categorie && $categorie->pid ? $categorie->pid->id : '';
    $c->response->redirect($c->uri_for('/beheer/bibliotheek/' . $pid));

    if (!$categorie) {
        $c->push_flash_message('ERROR: Helaas, kan de categorie niet vinden');
        $c->detach;
    }

    if (
        $categorie->bibliotheek_kenmerkens->count ||
        $categorie->bibliotheek_sjablonens->count
    ) {
        $c->push_flash_message(
            'ERROR: Helaas, kan de categorie niet verwijderen,'
            .' bepaalde onderdelen uit de bibliotheek maken hier'
            .' gebruik van.'
        );

        $c->detach;
    }

    if ($categorie->bibliotheek_categories->count) {
        $c->push_flash_message(
            'ERROR: Helaas, kan de categorie niet verwijderen,'
            .' deze is niet leeg.'
        );

        $c->detach;
    }

    if ($categorie->delete) {
        $c->push_flash_message(
            'Categorie ' . $categorie->naam . ' succesvol verwijderd'
        );

# Add logging
#         $c->model('DB::Logging')->add(
#             {
#                 component       => LOGGING_COMPONENT_CATEGORIE,
#                 component_id    => $categorie->id,
#                 onderwerp       => 'Categorie ' . $categorie->id . ' (' .
#                     $categorie->naam
#                      . ') verwijderd, opmerking: ' . $c->req->params->{commit_message}
#             },
#         );

    }

    $c->detach;
}






#-------------------------------  private -------------------------



Params::Profile->register_profile(
    method  => '_list_child_categories',
    profile => {
        required => [ qw/c/ ],
        optional => [ qw/child_category_ids bibliotheek_categorie_id/ ],
    }
);

sub _list_child_categories {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _list_child_categories". Dumper $dv unless $dv->success;

    my $bibliotheek_categorie_id = $params->{bibliotheek_categorie_id};
    my $c = $params->{c};
    my $child_category_ids = [];

# disabled, no worky
#    if(
#        $bibliotheek_categorie_id &&
#        exists $c->stash->{child_category_ids}->{$bibliotheek_categorie_id}
#    ) {
#        warn "returning here";
#       # return $c->stash->{child_category_ids}->{$bibliotheek_categorie_id};
#    }


    my $child_categories = $c->model('DB::BibliotheekCategorie')->search({
        pid => $bibliotheek_categorie_id
    }, {
        columns => [qw/id/],
    });
    $child_categories->result_class(
        'DBIx::Class::ResultClass::HashRefInflator');


    while(my $row = $child_categories->next()) {
        my $children = $self->_list_child_categories({
            c                        => $c,
            bibliotheek_categorie_id => $row->{id},
        });
        push @$child_category_ids, @$children;
    }

    if($bibliotheek_categorie_id) {
        $c->stash->{child_category_ids}->{$bibliotheek_categorie_id} = $child_category_ids;
    }

    return [$bibliotheek_categorie_id, @$child_category_ids];
}




Params::Profile->register_profile(
    method  => '_filter',
    profile => {
        required => [ qw/textfilter items/ ]
    }
);


sub _filter {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _filter" unless $dv->success;

    my $textfilter  = $params->{textfilter};
    my $items       = $params->{items};

    my @results = ();
    foreach my $item (@$items) {
        if($item->{naam} =~ m|$textfilter|) {
            push @results, $item;
        }
    }
    return @results;
}


sub _get_descriptions {
    my ($self, $c) = @_;

    $c->stash->{kenmerk_zaaktypen} = $c->model('DB::ZaaktypeKenmerken')->search({
        'zaaktype_node_id.deleted' => undef,
    },
    {
        join => 'zaaktype_node_id',
        #select => [ 'zaaktype_node_id.titel' ],            ### MEMORY LEAK!!
        distinct => 1,
    });

    $c->stash->{sjabloon_zaaktypen} = $c->model('DB::ZaaktypeSjablonen')->search({
        'zaaktype_node_id.deleted' => undef,
    },
    {
        join => 'zaaktype_node_id',
        #select => [ 'zaaktype_node_id.titel' ],            ### MEMORY LEAK!!
        distinct => 1,
    });
    $c->stash->{notificatie_zaaktypen} = $c->model('DB::ZaaktypeNotificatie')->search({
        'zaaktype_node_id.deleted' => undef,
    },
    {
        join => 'zaaktype_node_id',
        #select => [ 'zaaktype_node_id.titel' ],            ### MEMORY LEAK!!
        distinct => 1,
    });
}


__PACKAGE__->meta->make_immutable;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 categorie_bewerken

TODO: Fix the POD

=cut

=head2 categorie_verwijderen

TODO: Fix the POD

=cut

=head2 categoriebase

TODO: Fix the POD

=cut

=head2 categories

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut


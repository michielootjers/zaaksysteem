package Zaaksysteem::Controller::Search;

use strict;
use warnings;
use Data::Dumper;
use Moose::Util qw/apply_all_roles/;
use Time::HiRes qw/gettimeofday tv_interval/;

use parent 'Catalyst::Controller';
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;


use Moose;

has 'page' => (
	is => 'rw',
);



#
# June 2011 - new search functionality, TRAC #168. 
#


############################ begin URL dispatching #####################################

#/search
#/search/345
#/search/results
#/search/433/results
#/search/presentation
#/search/456/presentation



# match /search
sub base :Chained("/") :PathPart("search") :CaptureArgs(0) {
	my ($self, $c) = @_;

	$c->stash->{user_uidnumber} = $c->user->uidnumber if $c->user_exists;
}


# match /search$
sub filters :Chained("base") :PathPart("") :Args(0) {
	my ($self, $c) = @_;
	
	$self->show_filters($c);
}

sub filters_nw :Chained("base") :PathPart("nw") :Args(0) {
    my ($self, $c) = @_;
    
    $self->show_filters($c);
    $self->show_filters_nw($c);
}

sub presentation :Chained("base") :PathPart("presentation") :Args(0) {
	my ($self, $c) = @_;
	
	$self->show_presentation($c);
}



sub results :Chained("base") :PathPart("results") :Args(0) {
	my ($self, $c) = @_;
	
	$self->show_results($c);
}




#
# read the search_query_id from the url
#
sub id :Chained("base") :PathPart("") :CaptureArgs(1) {
	my ($self, $c, $search_query_id) = @_;
	
    my $record = $c->stash->{query_record} = $c->model('DB::SearchQuery')->find($search_query_id);
	$c->stash->{SEARCH_QUERY_SESSION_VAR()} = $search_query_id;


    ### When you have asked for a shared search query, we load the contents in our
    ### session, and leave this id, so we can save our modified query when we would like to
    if (
        $c->user_exists && $record->ldap_id ne $c->user->uidnumber
    ) {
        my $search_query                            = $self->_search_query($c);

        $search_query->tmp_name($record->name);

        $c->session->{SEARCH_QUERY_SESSION_VAR()}   = $search_query
                                                    ->serialize();

        my $path = $c->req->path;
        $path    =~ s/(search)\/\d+(.*)/$1$2/;

        $c->res->redirect($c->uri_for('/' . $path));
        $c->detach;
    }
}


#
# match /search/* (end of chain)
#
sub filters_by_id :Chained("id") :PathPart("") :Args(0) {
	my ($self, $c) = @_;
	
	$self->show_filters($c);
}

#
# match /search/*/edit (end of chain)
#
sub presentation_by_id :Chained("id") :PathPart("presentation") :Args(0) {
	my ($self, $c) = @_;
	
#	$c->log->debug('presentation_by_id: '.$c->stash->{SEARCH_QUERY_SESSION_VAR()}.'/edit');
	$self->show_presentation($c);
}

#
# match /search/*/edit (end of chain)
#
sub results_by_id :Chained("id") :PathPart("results") :Args(0) {
	my ($self, $c) = @_;
	
#	$c->log->debug('results_by_id/'.$c->stash->{SEARCH_QUERY_SESSION_VAR()}.'/edit');
    $self->show_results($c);
}



sub chart :Chained("base") :PathPart("results/chart") :Args(0) {
	my ($self, $c) = @_;

	my $params = $c->req->params;

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);

	my $chart_profile = $params->{'chart_profile'};
	if($chart_profile) {
		$c->stash->{json} = $self->_chart_profile($c, $chart_profile);
		$c->forward('Zaaksysteem::View::JSONlegacy');
		$c->detach;
	}

    $c->stash->{total_results} = $search_query->results({
        c           => $c, 
        page        => 1,
        get_total   => 'GET_TOTAL'
    })->pager->total_entries;
    $c->stash->{grouping_field_options} = $search_query->grouping_field_options;

	$c->stash->{'template'} = 'search/chart.tt';
}


sub chart_by_id :Chained("id") :PathPart("results/chart") :Args(0) {
	my ($self, $c) = @_;
	
    $self->chart($c);
}

sub map :Chained("base") :PathPart("results/map") :Args(0) {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $search_query = $self->_search_query($c);
    $self->_process_input($c, $search_query);

#    my $chart_profile = $params->{'chart_profile'};
#    if($chart_profile) {
#        $c->stash->{json} = $self->_chart_profile($c, $chart_profile);
#        $c->forward('Zaaksysteem::View::JSON');
#        $c->detach;
#    }

    $c->stash->{total_results} = $search_query->results({
        c           => $c, 
        page        => 1,
        get_total   => 'GET_TOTAL'
    })->pager->total_entries;

    $c->stash->{'json_entries'} =
        $c->forward(
            '/plugins/maps/_get_locations_from_case_query',
            [ $search_query ]
        );

    $c->stash->{grouping_field_options} = $search_query->grouping_field_options;

    $c->stash->{'template'} = 'search/map.tt';
}

sub map_by_id :Chained("id") :PathPart("results/map") :Args(0) {
	my ($self, $c) = @_;
	
    $self->map($c);
}

sub public_map_by_id : Chained("id") : PathPart("results/public_map") : Args(0) {
    my ($self, $c) = @_;

    ### Permission check
    throw(
        'search/public_map/query_not_found',
        'Cannot find query by id'
    ) unless $c->stash->{query_record};

    my $search_query = $c->model('SearchQuery');
    $search_query->unserialize($c->stash->{query_record}->settings);

    throw(
        'search/public_map/publish_denied',
        'Query is not publicly published, denied'
    ) unless (
        $search_query->access eq 'public'
    );

    $self->map($c);
    $c->stash->{layout_type} = 'simple';

    # Allow this controller to be displayed elsewhere, clickjacking doesn't
    # matter, this page is merely a Google Maps thing, nothing to redress.
    $self->res->header('X-Frame-Options', '');
}



#
# supply the popup with the settings for a filter - works purely on cgi params, no id handling necessary
#
sub search_edit_filter : Chained('base'): PathPart('filter/edit'): Args() {
    my ($self, $c) = @_;

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);

	my $params = $c->req->params();
	
	$c->stash->{filter_data} = $self->_search_query($c)->edit_filter(
		$params->{filter_type}, 
		$params->{filter_value},
	);

	$c->stash->{SEARCH_QUERY_SESSION_VAR()} = $params->{SEARCH_QUERY_SESSION_VAR()};
    $c->stash->{nowrapper} = 1;

    $c->stash->{org_eenheden} = $c->model('Betrokkene')->search(
        {
            type    => 'org_eenheid',
            intern  => 0,
        },
        {}
    );

    $c->stash->{template} = 'search/edit_filter.tt';
}


sub save_settings : Chained('base'): PathPart('save'): Args() {
    my ($self, $c) = @_;

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);
	$c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'search/save.tt';
    #TODO
}

sub save_settings_id : Chained('id'): PathPart('save'): Args() {
    my ($self, $c) = @_;

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);
	$c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'search/save.tt';
    #TODO
}


sub reset : Chained('base'): PathPart('reset'): Args() {
    my ($self, $c) = @_;

	delete $c->session->{SEARCH_QUERY_SESSION_VAR()};

	$c->response->redirect($c->uri_for('/search'));
	$c->detach();
}


sub dashboard: Chained('base'): PathPart('dashboard'): Args() {
    my ($self, $c) = @_;
	
	$self->_handle_dashboard_updates($c);

    my $params = $c->req->params();
    my $action = $params->{action} || '';
    if($action eq 'update_sort_order') {
   		$c->stash->{json} = {'result' => 1 };
		$c->forward('Zaaksysteem::View::JSONlegacy');
		$c->detach;
    }

    my $options = {
        order_by    => {'-asc' => 'sort_index'},
        page        => 1,
        rows        => 10000,
        join        => [ 'search_query_delens' ],
        distinct    => 1,
    };
    
    if(
        $c->stash->{show_more_search_queries} ||
        $c->req->params->{show_more_search_queries}
    ) {
        $options->{page} = 1;
        $options->{rows} = 10;
    }

    ### We need to explain this one.
    ### The below tests true WHEN:
    ### ou_id AND role_id matches the logged in user
    ###   OR (
    ###     This query is shared (search_query_delens.id is set)
    ###       AND
    ###     There is either a ou_id OR a role_id set, NOT BOTH
    ###       AND
    ###         (
    ###         the role_id matches the logged in USER
    ###           OR
    ###         the ou_id matches the logged in USER
    ###         )

    my $groups = [
        '-and'  => {
                'search_query_delens.ou_id'     => $c->user_ou_id,
                'search_query_delens.role_id'   => {
                    '-in'   => [ $c->user_roles_ids ]
                },
        },
        '-and'  => [
            'search_query_delens.id'        => { '!=' => undef },
            '-or'   => [
                'search_query_delens.ou_id'     => undef,
                'search_query_delens.role_id'   => undef
            ],
            '-or'   => [
                '-and' => [
                    # it was shared to the unit i'm in
                    'search_query_delens.ou_id'     => $c->user_ou_id,
                    # or it was shared to all units
                    'search_query_delens.role_id'     => undef,
                ],  # and..
                '-and' => [
                    # it was shared to one of my roles
                    'search_query_delens.role_id'   => {
                        '-in'   => [ $c->user_roles_ids ]
                    },
                    # or it was shared to all roles
                    'search_query_delens.ou_id'   => undef,
                ],
            ],
        ]
    ];

    # Where ldap_id is set OR the ou_id or role_id is set and matches
    my $where = {
        '-or'   => [
            'ldap_id'   => $c->user->uidnumber, # i am the owner, or:
            '-or'      => $groups                # the owner shared it
        ]
    };

	$c->stash->{'search_queries'} = $c->model('DB::SearchQuery')->search(
	    $where,
	    $options,
	);
		
    $c->stash->{template} = 'search/dashboard.tt';	
}

sub opties : Chained('id'): PathPart('opties'): Args() {
    my ($self, $c) = @_;

    my $search_query = $self->_search_query($c);

    if (
        $c->stash->{query_record} &&
        $c->stash->{query_record}->ldap_id ne $c->user->uidnumber
    ) {
        die('YOU ARE NOT OWNER. FAIL');
    }

    ### Template
    $c->stash->{template}   = 'search/widgets/opties.tt';
    $c->stash->{access}     = $search_query->access;


    ### DB settings
    my $delen   = $c->stash->{query_record}
        ->search_query_delens
        ->search({}, { order_by => 'id' });

    my $counter = 0;
    while (my $deel = $delen->next) {
        $c->stash->{delen}->{++$counter} = $deel;
    }

    if (uc($c->req->method) eq 'POST' && $c->req->params->{update}) {


        $c->res->redirect($c->uri_for(
            '/search/' .
            $c->stash->{query_record}->id
        ));

        my @rollen  = grep { /^delen/ } keys %{ $c->req->params };

        my $rv      = {};
        for my $rol (@rollen) {
            my ($id)    = $rol =~ /(\d+)$/;
            next unless $id;

            $rv->{$id}  = {
                role_id     => $c->req->params->{'delen.role_id.' . $id} || undef,
                ou_id       => $c->req->params->{'delen.ou_id.' . $id} || undef,
            }
        }

        ### update
        my @ids     = sort(keys %{ $rv });

        $c->stash->{query_record}->search_query_delens->delete;
        for my $id (@ids) {
            $c->stash->{query_record}->search_query_delens->create(
                $rv->{$id}
            )
        }

        ### Zoekopdracht beveiligen
        $search_query->access($c->req->params->{access});

        $self->_save($c, $search_query);
    }
}



sub get : Chained('/'): PathPart('search/get'): Args(0) {
    my ($self, $c) = @_;

    (
        $c->res->redirect('/'),
        return
    ) unless (
        exists($c->req->params->{searchstring}) &&
        $c->req->params->{searchstring}
    );

    my $sstring = $c->req->params->{searchstring};

    if ($c->req->params->{component} eq 'zaak') {
        if (
            $sstring =~ /^\d+$/
        ) {
            $c->res->redirect('/zaak/' . $sstring);
            $c->detach;
        }
    } elsif ($c->req->params->{component} eq 'natuurlijk_persoon') {
        $c->res->redirect(
            $c->uri_for(
                '/betrokkene/search',
                {
                    'np-geslachtsnaam'  => $sstring,
                    'betrokkene_type'   => 'natuurlijk_persoon',
                    'search'            => 1,
                }
            )
        );
        $c->detach;
    } elsif ($c->req->params->{component} eq 'bedrijf') {
        $c->res->redirect(
            $c->uri_for(
                '/betrokkene/search',
                {
                    'handelsnaam'       => $sstring,
                    'betrokkene_type'   => 'bedrijf',
                    'search'            => 1,
                }
            )
        );
        $c->detach;
    }

    $c->res->redirect('/');
}


############################ end of URL dispatching #####################################


sub _handle_dashboard_updates {
	my ($self, $c) = @_;

	my $params = $c->req->params();
	my $action = $params->{'action'} || '';

	if($action eq 'delete_search_query') {
		my $search_query_id = $params->{'search_query_id'};
		die "need search query id" unless($search_query_id);
		
		my $model = $c->model(SEARCH_QUERY_TABLE_NAME);
		my $record = $model->find($search_query_id);
		
		return unless($record);

        if ($record->search_query_delens->count) {
            $record->search_query_delens->delete;
        }

		if($record->ldap_id eq $c->user->uidnumber) {
			$record->delete;
		}
	} elsif($action eq 'update_sort_order') {
	    my @id_strings = grep /^search_query_id_/, keys %$params;
	    my $sort_index = 0;
        my $model = $c->model(SEARCH_QUERY_TABLE_NAME);

        @id_strings = sort { $params->{$a} <=> $params->{$b} } @id_strings; 
	    foreach my $id_string (@id_strings) {
	        my ($search_query_id) = $id_string =~ m|(\d+)|;
	        $c->log->debug('search_query_id: ' . $search_query_id);

            my $record = $model->find($search_query_id);
	        if($record) {
	            $c->log->debug('record found');
	            $record->sort_index($sort_index);
	            $record->update();
	        }
	        $sort_index++;
	    }
	}
}



sub show_filters {
    my ($self, $c) = @_;

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);
	
	my $params = $c->req->params();

	if($params->{ nowrapper } && $params->{ nowrapper } == 1) {
		$c->stash->{ nowrapper } = 1;
	}

	$c->stash->{ filter_type } = $params->{ filter_type };
	$c->stash->{ active_filtertype } = $params->{ filter_type };
	$c->stash->{ filters } = $search_query->get_filters($c);
	$c->stash->{ kenmerken } = $search_query->get_kenmerken($c);

    $c->stash->{ grouping_field_options } = $search_query->grouping_field_options;
    $c->stash->{ attribute_query_style } = $search_query->attribute_query_style;

    $c->stash->{ total_results } = $search_query->results({
        c => $c, 
        page => 1, 
        get_total => 'GET_TOTAL'
    })->pager->total_entries;
    
    $c->stash->{ is_searchquery } = 1;
    $c->stash->{ filtertypes } = $search_query->filter_options;
    $c->stash->{ template } = 'search/filters.tt';
}

sub show_filters_nw {
    my ($self, $c) = @_;
    
    $c->stash->{ template } = 'search/widgets/search-widget.tt';
    $c->stash->{ object_type } = 'case';   
}



sub show_presentation {
    my ($self, $c) = @_;

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);

	my $display_fields = $search_query->edit_display_fields();
	$c->log->debug("display fields: " . scalar(@$display_fields));	
	$c->stash->{display_fields} = $display_fields;

	$c->stash->{grouping_field} = $search_query->grouping_field;
	$c->stash->{grouping_field_options} = $search_query->grouping_field_options;

    $c->stash->{total_results} = $search_query->results({
        c => $c, 
        page => 1, 
        get_total => 'GET_TOTAL'
    })->pager->total_entries;
	
    $c->stash->{template} = 'search/presentation.tt';
}



#
# display the results
#
sub show_results {
    my ($self, $c) = @_;

    my $t0 = [gettimeofday];
	my $params = $c->req->params();

    if($c->req->params->{statusfilter}) {
        $params->{filter} = $c->req->params->{statusfilter};
    }

	# default page, to be overridden somewhere down the line
	$self->page(1);

	my $search_query = $self->_search_query($c);
	$self->_process_input($c, $search_query);

	my $resultset;

    if (!$search_query->sort_field) {
        $search_query->sort_field('me.id');
    }

    $search_query->sort_direction($params->{sort_direction} || 'DESC');
    

    my $grouping_field = $c->stash->{'grouping_field'} = $params->{'grouping_field'};

    $search_query->grouping_field($grouping_field);
    if($grouping_field && !$params->{grouping_choice}) {
        $search_query->sort_field(undef);
    }

    my $grouping_choice = $c->stash->{'grouping_choice'} = $params->{'grouping_choice'};
    if($grouping_choice) {
   		$search_query->grouping_choice($grouping_choice);
    } else {
   		$search_query->grouping_choice(undef);
    }

    $resultset = $search_query->results({
        c                   => $c, 
        page                => $self->page,
        show_future_cases   => 1,
    })->with_progress();

	# use the central filter code to handle the dropdown and textfilter limiting filter options    
	$resultset = $c->model('Zaken')->filter({
		resultset 	   => $resultset,
		textfilter     => $params->{'textfilter'},
		dropdown	   => $params->{'filter'},
	});

    $c->stash->{total_results} = $search_query->results({
        c           => $c, 
        page        => 1, 
        get_total   => 'GET_TOTAL'
    })->pager->total_entries;

    if($params->{results_per_page} ||= 20) {
        $c->stash->{results_per_page} = $params->{results_per_page};
        $resultset = $resultset->search(undef, {
            rows => $params->{results_per_page},
        });
    }

    $c->stash->{results} = $resultset;
    $c->stash->{grouping_field_options} = $search_query->grouping_field_options;
        
    if($grouping_field) {
        $c->stash->{total_results} = $search_query->results({
            c           => $c, 
            page        => 1, 
            get_total   =>'GET_TOTAL'
        })->pager->total_entries;
    }

    if($grouping_field && !$grouping_choice) {
        $c->stash->{status_labels} = $search_query->status_labels;
        $c->stash->{template} = 'search/grouping.tt';
    } else {
        $c->stash->{listactions} = 1;
        $c->stash->{sort_field} = $search_query->sort_field;
        $c->stash->{sort_direction} = $search_query->sort_direction;	
        $c->stash->{'page'} = $self->page();
        $c->stash->{display_fields} = $search_query->get_display_fields;
        $c->stash->{template} = 'search/results.tt';
    }
    
    if($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;
    }
    $c->log->debug("Time taken: " . tv_interval($t0));
}




# sub update_sort_field {
# 	my ($self, $sort_field) = @_;
# 	
# 	my $current_sort_field = $self->sort_field || '';
# 	
# 	if($current_sort_field eq $sort_field) {
# 		if($self->sort_direction eq 'ASC') {
# 			$self->sort_direction('DESC');
# 		} else {
# 			$self->sort_direction('ASC');
# 		}
# 	} else {
# 		$self->sort_direction('ASC');
# 	}
# 	$self->sort_field($sort_field);
# }


# ------------------------------- only friends can see private parts ---------------------------#

#
# process changes to the state of the application
#
sub _process_input {
	my ($self, $c, $search_query) = @_;

	my $params = $c->req->params();
	
	$self->_handle_updates($c, $params, $search_query);
	
	my $search_query_id = $c->stash->{SEARCH_QUERY_SESSION_VAR()} || '';
	my $search_query_id_path = $search_query_id ? '/'. $search_query_id : '';

#	$c->log->debug("action: $action, sqidp: $search_query_id_path");

    my $destination = $params->{'destination'} || '';
	if($destination eq 'filters' && $params->{'current_search_page'} ne 'filters') {
		$c->response->redirect($c->uri_for('/search'. $search_query_id_path));
		$c->detach();
	} elsif($destination eq 'presentation' && $params->{'current_search_page'} ne 'presentation') {
		$c->response->redirect($c->uri_for('/search'. $search_query_id_path ."/presentation"));
		$c->detach();
	} elsif($destination eq 'results' && $params->{'current_search_page'} ne 'results') {
        my $additional_params = {};
        if($params->{grouping_field}){
            $additional_params->{grouping_field} = $params->{grouping_field};
        }
		$c->response->redirect($c->uri_for('/search'. $search_query_id_path ."/results", $additional_params));
		$c->detach();			
	} elsif($destination eq 'map' && $params->{'current_search_page'} ne 'map') {
		$c->response->redirect($c->uri_for('/search'. $search_query_id_path ."/results/map"));
		$c->detach();			
	} elsif($destination eq 'chart' && $params->{'current_search_page'} ne 'chart') {
		$c->response->redirect($c->uri_for('/search'. $search_query_id_path ."/results/chart"));
		$c->detach();			
	}
	
	$c->stash->{'current_filter_type'} = $c->session->{'current_filter_type'};
}


sub _handle_updates {
	my ($self, $c, $params, $search_query) = @_;
	
	my $action = $params->{'action'} || '';

	if($action eq 'show_kenmerken_filters') {
		$c->stash->{'show_kenmerken_filters'} = 1;
	}

	if($action eq 'reset') {
		delete $c->session->{SEARCH_QUERY_SESSION_VAR()};
		$c->response->redirect($c->uri_for('/search'));
		$c->detach();
	}

	if($action eq 'update_presentation') {
        my $search_fields = $self->_make_list($c, $params->{search_fields});
        if($params->{'additional_kenmerk'}) {
	        push @$search_fields, $params->{'additional_kenmerk'};    
	    }
		$search_query->set_display_fields($search_fields);
	} 
	elsif($action eq 'update_filter') {
		# make sure all kenmerk updates and deletions are taken care of
		$search_query->update_filter($params);
		$c->session->{'current_filter_type'} = $params->{'filter_type'};
	}
	elsif($action eq 'update_filters') {
		$self->_update_filters($c, $search_query, $params);
	}
	elsif($action eq 'update_results') {
        $self->page($params->{'page'} || 1);
        ### Do not change sort direction on page change
        if(!$params->{pager_request} && $params->{'sort_field'}) {
            $search_query->update_sort_field($params->{'sort_field'});
        }
	}
	elsif($action eq 'add_kenmerk_filter') {
	    my $kenmerk = { id => $params->{bibliotheek_kenmerken_id} };
	    my $kenmerken = $search_query->kenmerken();
	    push @$kenmerken, $kenmerk;
	    $search_query->kenmerken($kenmerken);
	}

    $search_query->attribute_query_style($c->req->param('attribute_query_style'));

	$self->_save($c, $search_query);
}


sub _update_filters {
	my ($self, $c, $search_query, $params) = @_;

	my $filter_type  = $self->_make_list($c, $params->{filter_type});
	my $filter_value = $self->_make_list($c, $params->{filter_value});

	$c->session->{'current_filter_type'} = $params->{'current_filter_type'};
	
	my $kenmerken = [];
	my @kenmerk_indexes = map { $_ =~ m|(\d+)| } grep /^k-kenmerk_id_\d+/, keys %$params;

	foreach my $index_key (sort {$a <=> $b } @kenmerk_indexes) {
		my $index_value         = $params->{'k-kenmerk_id_'.$index_key};
		my $search_value        = $params->{'k-kenmerk_search_'.$index_key} || '';
		my $operator_value      = $params->{'k-kenmerk_operator_'.$index_key} || '';
        my $show_inactive_value = $params->{'k-kenmerk_show_inactive_'.$index_key} || '';

		if($index_value) {
			push @$kenmerken, {
				'id'            => $index_value, 
				'data'          => $search_value, 
				'operator'      => $operator_value,
                'show_inactive' => $show_inactive_value,
			};
			$c->session->{'current_filter_type'} = 'kenmerk';
		}
	}

	$search_query->update_filters($filter_type, $filter_value, $kenmerken);
}


#
# save the new search settings
#
sub _save {
	my ($self, $c, $search_query) = @_;

	my $params = $c->req->params();
    
	my $serialized = $search_query->serialize();
	my $search_query_id = $c->stash->{SEARCH_QUERY_SESSION_VAR()} || '';

	unless($params->{'search_query_name_hidden'} || $search_query_id) {	
		$c->session->{SEARCH_QUERY_SESSION_VAR()} = $serialized;
		return;
	}

	my $model = $c->model(SEARCH_QUERY_TABLE_NAME);
	my $record;
	
	if($search_query_id) {
		$record = $model->find($search_query_id);
	}
	
	my $search_query_name = $params->{'search_query_name_hidden'};

    # Create in place
    if(!$record) {
    	$record = $model->create({
    		name     => $search_query_name || $record->name,
			ldap_id  => $c->user->uidnumber,
			settings => $serialized,
		});
		my $path = $c->req->path();
		$c->log->debug('path: ' . $path);
		$path =~ s|search|'/search/'.$record->id|eis;
		$c->log->debug('path: ' . $path);
		$c->response->redirect($path);
		$c->detach();
    } else {
        # so there was an existing record.
        # if it's the users own, or it's a public record, update it.
    	if($c->user_exists && $record->ldap_id eq $c->user->uidnumber) {
            if($search_query_name) {
                $record->name($search_query_name);
                $c->stash->{'search_query_name'} = $record->name;
            }
            $record->settings($serialized);
            $record->update;
            $c->stash->{'search_query_owner'} = 1;
        } elsif ($search_query->access() eq 'public') {
            $c->stash->{'search_query_owner'} = 0;
        } else { 
            # so it's a private record of somebody else. don't update, just show results.
            $c->stash->{'search_query_read_only'} = 1;
        }
    }
}

sub _search_query {
	my ($self, $c) = @_;
	die "need c" unless($c);

	my $search_query = $c->model('SearchQuery');
	my $search_query_id = $c->stash->{SEARCH_QUERY_SESSION_VAR()};
	my $model = $c->model(SEARCH_QUERY_TABLE_NAME);
	my $record;
	
	if($search_query_id) {
		my $record = $model->find($search_query_id);
		if($record) {
			$search_query->unserialize($record->settings);
			$c->stash->{'search_query_name'} = $record->name;
			$c->stash->{'record_ldap_id'} = $record->ldap_id;
			$c->stash->{'search_query_access'} = $search_query->access;
            $c->stash->{ attribute_query_style } = $search_query->attribute_query_style;

			return $search_query;
		} else {
			$c->response->redirect($c->uri_for('/search'));
			$c->detach();
		}
	} else {
		$c->stash->{'search_query_name'} = $search_query->tmp_name || 'Naamloze zoekopdracht';
		$c->stash->{'search_query_access'} = 'private';
	}

	if($c->session->{SEARCH_QUERY_SESSION_VAR()}) {
		$search_query->unserialize($c->session->{SEARCH_QUERY_SESSION_VAR()});
        $c->stash->{'search_query_name'} = $search_query->tmp_name if $search_query->tmp_name;
	}
	
	return $search_query;
}

sub _chart_profile {
	my ($self, $c, $profile) = @_;
	
	die "need profile" unless($profile);

    my $search_query = $self->_search_query($c);
    my $resultset = $search_query->results({c => $c});

    use Zaaksysteem::ChartGenerator;
    my $chartgenerator = Zaaksysteem::ChartGenerator->new({ resultset => $resultset });
    my $chart_profile = $chartgenerator->generate({ profile => $profile });

    my $lookup = $self->departments_lookup($c);

    if($profile eq 'cases_per_department') {

        my $categories = [
            map { 
                $lookup->{$_} || 'Afdeling '. $_
            } @{ 
                $chart_profile->{xAxis}->{categories}
            } 
        ];
        $chart_profile->{xAxis}->{categories} = $categories;
    } elsif (
        $profile eq 'cases_per_department_per_month' ||
        $profile eq 'cases_within_and_outside_term_per_month_per_department'
    ) {
        my $series = $chart_profile->{series};
        foreach my $part (@$series) {
            my $department = $part->{name};
            $part->{name} = $lookup->{$department} || 'Afdeling ' . $department;
        }
    }

    return $chart_profile;
}


sub departments_lookup {
    my ($self, $c) = @_;

    my $ous = $c->model('Groups')->search_ou;

    return { map { $_->{id} => $_->{ou} } @$ous };
}


sub _make_list {
	my ($self, $c, $input) = @_;
	
	return [] unless($input);

	return [$input] unless(ref $input);
	
	return $input if(ref $input eq 'ARRAY');
	
	die "incorrect input: " . Dumper $input;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


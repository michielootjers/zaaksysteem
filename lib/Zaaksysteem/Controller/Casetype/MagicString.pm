package Zaaksysteem::Controller::Casetype::MagicString;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

use Zaaksysteem::Constants;


=head1 NAME

Zaaksysteem::Controller::Casetype::MagicString - ZAPI Controller

=head1 SYNOPSIS

 # /casetype/magicstring

=head1 DESCRIPTION

Zaaksysteem API Controller for magicstrings related to Casetypes.

=head1 METHODS

=head2 /casetype/CASETYPE_ID/magicstring [GET READ]

Returns a resultset of all possible magicstrings related to a casetype,
which also includes system strings like 'aanvrager_naam' when C<include_system>
is given.

B<Query Parameters>

=over 4

=item query

Type: STRING

 # /casetype/1/magicstring?query=test
 
Filters the results with the given query string

=item include_system

 # /casetype/1/magicstring?query=test&include_system=1

=back

=cut

sub index
    : Chained('/casetype/base')
    : PathPart('magicstring')
    : Args(0)
    : ZAPI
{
    my ($self, $c)      = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';


    my $query           = lc($c->req->params->{query});

    ### TODO MOVE THIS TO MODEL!! :(
    my $rs_kenmerken    = $c->model('DB::BibliotheekKenmerken')
                        ->search_freeform(
                            $query,
                        );

    if ($c->stash->{zaaktype}) {
        $rs_kenmerken = $self->scope_to_zaaktype_id($c, $rs_kenmerken);
    }

    my @rv;
    while (my $kenmerk = $rs_kenmerken->next) {
        push(
            @rv,
            {
                'searchable_object_id'          => $kenmerk->magic_string,
                'searchable_object_label'       => $kenmerk->magic_string,
                'searchable_object_type'        => 'magicstring',
            },
        );
    }

    if ($c->req->params->{include_system}) {
        for my $magic_string (
            (keys %{ ZAAKSYSTEEM_STANDAARD_KENMERKEN() }),
            (keys %{ ZAAKSYSTEEM_BETROKKENE_KENMERK() }),
        ) {
            next unless !$query || $magic_string =~ /$query/;

            push(
                @rv,
                {
                    'searchable_object_id'          => $magic_string,
                    'searchable_object_label'       => $magic_string,
                    'searchable_object_type'        => 'magicstring',
                },
            );    

        }
    }

    $c->stash->{zapi} = \@rv;
}


sub scope_to_zaaktype_id {
    my ($self, $c, $rs_kenmerken) = @_;

    $rs_kenmerken       = $rs_kenmerken->search(
        {
            'zaaktype_kenmerkens.zaaktype_node_id' => $c->stash
                                                        ->{zaaktype}
                                                        ->zaaktype_node_id
                                                        ->id,
        },
        {
            'join'  => 'zaaktype_kenmerkens',
        }
    );
    return $rs_kenmerken;
}


=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;
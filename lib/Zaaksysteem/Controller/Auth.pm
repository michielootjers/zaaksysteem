package Zaaksysteem::Controller::Auth;

use Moose;
use Zaaksysteem::Access;
use Zaaksysteem::Exception;

use Data::Dumper;

use Digest::SHA qw[sha256_base64];
use DateTime;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub login : Local : Access(*) {
    my ($self, $c) = @_;

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    if ($c->flash->{referer}) {
        $c->stash->{'referer'} = $c->flash->{referer};
    } elsif ($c->req->params->{referer}) {
        $c->stash->{'referer'} = $c->req->params->{referer};
    }

    if ($c->user_exists) {
        $c->res->redirect(
            ($c->stash->{'referer'} ||
            $c->uri_for( '/')),
        );
        $c->detach;
    } else {
        $c->stash->{template} = 'auth/login.tt';
    }


    if (exists($c->req->params->{'username'})) {

        ### Some input checking
        if ($c->req->params->{'username'} !~ /^[\w\-\d\.]+$/) {
            $c->detach;
        }

        if (
            $c->authenticate(
                {
                    'username'  => lc($c->req->params->{'username'}),
                    'password'  => $c->req->params->{'password'},
                }
            )
        ) {
            $c->statsd->increment('employee.login.ok', 1);
            $c->session->{ _xsrf_token } = sha256_base64($c->user->uidnumber . $c->config->{ xsrf_salt } . DateTime->now->datetime);
            $c->res->cookies->{ 'XSRF-TOKEN' } = {
                value => $c->session->{ _xsrf_token },
                secure => ($c->req->uri->scheme eq 'https' ? 1 : 0) # HAAAAACK
            };

            if ($c->req->params->{referer}) {
                $c->response->redirect($c->req->params->{referer});
            } else {
                $c->response->redirect($c->uri_for('/'));
            }
            $c->detach;
            return;
        } else {
            $c->statsd->increment('employee.login.failed', 1);
            $c->push_flash_message('Invalid login.');
        }
    }

}

sub logout : Local : Access(*) {
    my ($self, $c) = @_;

    $c->logout();
    $c->delete_session();

    $c->stash->{message} = 'You have been logged out';
    $c->response->redirect($c->uri_for('/auth/login'));
}


sub retrieve_roles : Local {
    my ($self, $c, $ouid) = @_;

    return unless $c->req->is_xhr;
    
    my $roles = $c->model('Users')->get_array_of_roles({
        'parent_ou' => $ouid
    });

    if($c->req->params->{select_complete_afdeling}) {
        unshift @$roles, 'split';
        unshift @$roles, {
            children        => [], 
            name            => 'Gehele afdeling', 
            internal_id     => ''
        };
    }

    my $json = {
        'roles' => $roles
    };

    $c->stash->{json} = $json;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}



sub prepare_page : Private {
    my ($self, $c) = @_;

    if ($c->user_exists) {
        $c->forward('/page/add_menu_item', [
            {
                'quick' => [
                    {
                        'name'  => 'Uitloggen',
                        'href'  => $c->uri_for('/auth/logout'),
                    },
                    {
                        'name'  => 'Ingelogd als "' . $c->user->displayname . '"',
                        'href'  => $c->uri_for(
                            '/betrokkene/' . $c->user->uidnumber, {
                                gm => 1,
                                type => 'medewerker'
                            }
                        ),
                    },
                ],
            }
        ]);
    } else {
        $c->forward('/page/add_menu_item', [
            {
                'quick' => [
                    {
                        'name'  => 'Inloggen',
                        'href'  => $c->uri_for('/auth/login'),
                    }
                ],
            }
        ]);

    }
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


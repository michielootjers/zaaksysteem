package Zaaksysteem::Controller::Betrokkene;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

use JSON;
use Data::Dumper;

use 5.010;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
    VALIDATION_CONTACT_DATA
    ZAAKSYSTEEM_CONSTANTS
    LOGGING_COMPONENT_BETROKKENE
    DOCUMENTS_STORE_TYPE_NOTITIE
/;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Betrokkene in Betrokkene.');
}

sub base : Chained('/') : PathPart('betrokkene'): CaptureArgs(0) {
    my ($self, $c) = @_;

    ## Zaakid?
    if ($c->req->params->{'zaak'}) {
        $c->stash->{zaak} = $c->model('DB::Zaak')->find($c->req->params->{'zaak'});
    }
}

{
    Zaaksysteem->register_profile(
        method  => 'create',
        profile => {
            required => [ qw/
                betrokkene_type
                np-geslachtsnaam
                np-huisnummer
                np-postcode
                np-straatnaam
                np-voornamen
                np-woonplaats
                np-geslachtsaanduiding
            /],
            optional => [ qw/
                create
                np-burgerservicenummer
                np-huisnummertoevoeging
                np-voorvoegsel
                np-geboortedatum
                npc-telefoonnummer
                npc-email
                npc-mobiel
            /],
            constraint_methods  => {
                'np-burgerservicenummer'    => qr/^\d{1,9}$/,
                'np-geboortedatum'          => qr/[\d-]+/,
                'np-geslachtsnaam'          => qr/.+/,
                'np-huisnummer'             => qr/[\d]+/,
                'np-postcode'               => qr/^\d{4}\w{2}$/,
                'np-straatnaam'             => qr/.+/,
                'np-voorletters'            => qr/[\w.]+/,
                'np-voornamen'              => qr/.+/,
                'np-woonplaats'             => qr/.+/,
                'npc-email'                 => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
                'npc-telefoonnummer'        => qr/^[\d\+]{6,15}$/,
                'npc-mobiel'                => qr/^[\d\+]{6,15}$/,
            },
            msgs                => {
                'format'    => '%s',
                'missing'   => 'Veld is verplicht.',
                'invalid'   => 'Veld is niet correct ingevuld.',
                'constraints' => {
                    '(?-xism:^\d{4}\w{2}$)' => 'Postcode zonder spatie (1000AA)',
                    '(?-xism:^[\d\+]{6,15}$)' => 'Nummer zonder spatie (e.g: +312012345678)',
                }
            },
        }
    );

    sub create : Chained('/') : PathPart('betrokkene/create'): Args(0) {
        my ($self, $c) = @_;

        if ($c->req->is_xhr) {
            $c->zvalidate;
            $c->detach;
        }

        ### Default: view
        $c->stash->{template}   = 'betrokkene/create.tt';

        if ($c->req->method eq 'POST') {
            # Validate information
            my $params = $c->req->params;
            return unless $c->zvalidate && $params->{create};


            ### Create person

            # Convert postcode
            $params->{'np-postcode'} = uc($params->{'np-postcode'});

            my $id = $c->model('Betrokkene')->create(
                'natuurlijk_persoon',
                {
                    %$params,
                    authenticatedby =>
                        ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
                }
            );

            $c->model('DB::Logging')->trigger('subject/create', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $id,
                created_for => $id,
                data => {
                    subject_id => $id
                }
            });
            
            if ($id) {
                $c->push_flash_message('Natuurlijk persoon aangemaakt');
                $c->res->redirect(
                    $c->uri_for(
                        '/betrokkene/' . $id,
                        { gm => 1, type => 'natuurlijk_persoon' }
                    )
                );
            }
        }

    }
}


sub view_base : Chained('base'): PathPart('') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    die "view_base: id not set, nothing to do here" unless $id =~ /^\d+$/;

    my $betrokkene_type = $c->req->params->{type};

    $c->stash->{requested_bid} = $id;

    if ($c->req->params->{gm}) {
        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $id
        );

        $c->stash->{'betrokkene_edit'} = 1 unless
            !$c->req->params->{edit} ||
            $c->stash->{'betrokkene'}->authenticated
    } else {
        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {},
            $id
        );

    }

    $c->forward('include_woz_tab');
    $c->forward('signature_settings');

    $c->detach unless $c->stash->{betrokkene};

    if($betrokkene_type eq 'bedrijf' && $c->check_any_user_permission(qw/contact_nieuw contact_search/) && $c->stash->{ betrokkene }->authenticated) {
        $c->stash->{ user_can_set_authentication } = 1;

        my $other_auth = $c->model('DB::BedrijfAuthenticatie')->search({
            login => $c->stash->{ betrokkene }->dossiernummer,
            gegevens_magazijn_id => { '!=' => $id }
        })->first;

        if($other_auth) {
            $c->stash->{ other_bedrijf_auth } = $c->uri_for(
                '/betrokkene/' . $other_auth->gegevens_magazijn_id,
                { gm => 1, type => 'bedrijf' }
            );
        }
    }

    if ($betrokkene_type eq 'medewerker') {
        $c->stash->{ user_can_change_password } =
            $c->model('DB::Config')->get('users_can_change_password') &&
            $self->looking_at_self($c);

        $c->stash->{ user_can_change_extension } = $self->looking_at_self($c);
    }

    $c->forward('handle_betrokkene_session');
}


=head2 signature_settings

Determines wether the signature settings need to be shown.

There are two options:
1) Behandelaars can change their own signature (default)
This means that a behandelaar will see the settings on their own page.

2) Zaaksysteembeheerders and Administrators can change signatures for behandelaars
This means that the role must be checked.

Signature are always applied to the user that's currently being viewed.

=cut

sub signature_settings : Private {
    my ($self, $c) = @_;

    my $role = $c->model('DB::Config')->get('signature_upload_role') || '';

    if ($role eq 'behandelaar') {
        # see if behandelaar is looking at own page
        $c->stash->{show_signature_settings} = $self->looking_at_self($c);

    } elsif ($role eq 'zaaksysteembeheerder') {

        $c->stash->{show_signature_settings} = $c->check_any_user_permission('owner_signatures');
    }
}


=head2 looking_at_self

See if we have an employee looking at own page. Compare the uuid of the
logged in user to the user on the stash.

ldap_rs is a legacy misnomer, it refers to the subject table row.
$c->user also maps to a row in the subject table.

=cut

sub looking_at_self {
    my ($self, $c) = @_;

    return $c->stash->{betrokkene} &&
        $c->stash->{betrokkene}->type eq 'medewerker' &&
        $c->stash->{betrokkene}->can('ldap_rs') &&
        $c->stash->{betrokkene}->ldap_rs &&
        $c->user &&
        $c->stash->{betrokkene}->ldap_rs->uuid eq $c->user->uuid;
}


sub include_woz_tab : Private {
    my ($self, $c) = @_;

    if ($c->check_any_user_permission(qw/woz_objects/) &&
        $c->stash->{ betrokkene }->can('burgerservicenummer') &&
        $c->stash->{betrokkene}->burgerservicenummer
    ) {
        my $owner = 
            $c->stash->{betrokkene}->btype . '-' .
            $c->stash->{betrokkene}->burgerservicenummer;

        my $arguments = [$owner];
        if (my $woz_id = $c->req->params->{woz_id}) {
            push @$arguments, $woz_id;
        }

        $c->forward('/beheer/woz/view', $arguments);
    }
}

sub woz_object : Chained('view_base'): PathPart('woz_object') {
    my ($self, $c) = @_;

    warn "woz_object: ". Dumper $c->req->params;
}


sub handle_betrokkene_session : Private {
    my ($self, $c) = @_;

    if ($c->req->params->{enable_betrokkene_session}) {
        $c->betrokkene_session_enable($c->stash->{betrokkene});
        $self->prepare_page($c);

        if ($c->req->is_xhr) {
            $c->stash->{json} = {
                succes  => 1,
                naam    => $c->betrokkene_session->naam,
                url     => $c->uri_for(
                    '/betrokkene/' . $c->betrokkene_session->ex_id,
                    {
                        gm  => 1,
                        type => $c->betrokkene_session->btype

                    }
                )->as_string
            };

            $c->detach('Zaaksysteem::View::JSONlegacy');
        }
    }
}

=head1 Enable company subject authentication

This action creates the C<BedrijfAuthentication> row associated to this subject,
allowing the subject to login via /pip or /form.

=head2 URL construction

B</betrokkene/enable_company_auth?id=[subject_id]>

=cut

define_profile enable_company_auth => (
    required => {
        id => 'Int'
    }
);

sub enable_company_auth : Chained('/') : PathPart('betrokkene/enable_company_auth') : Args(0) {
    my ($self, $c) = @_;

    my $id = assert_profile($c->req->params)->valid->{ id };

    unless($c->check_any_user_permission(qw/contact_nieuw contact_search/)) {
        throw('subject/authorization', 'You do not have permission to make this change.');
    }

    my $subject = $c->model('Betrokkene')->get(
        { type => 'bedrijf', intern => 0 }, $id
    );

    unless ($subject) {
        throw('subject/id', 'ID not valid');
    }

    $subject->create_auth;

    $c->push_flash_message('Login voor betrokkene ingeschakeld');
    $c->res->redirect($c->uri_for('/betrokkene/' . $id, {
        type => 'bedrijf',
        gm   => 1
    }));
}

=head1 Disable company subject authentication

Same as above, but disables the C<BedrijfAuthentication> row, such that
companies can no longer login via MuncipalityID or eHerkenning.

=head2 URL construction

B</betrokkene/disable_company_auth?id=[subject_id]>

=cut

define_profile disable_company_auth => (
    required => {
        id => 'Int'
    }
);

sub disable_company_auth : Chained('/') : PathPart('betrokkene/disable_company_auth') : Args(0) {
    my ($self, $c) = @_;

    my $id = assert_profile($c->req->params)->valid->{ id };

    unless($c->check_any_user_permission(qw/contact_nieuw contact_search/)) {
        throw('subject/authorization', 'You do not have permission to make this change.');
    }

    my $subject = $c->model('Betrokkene')->get(
        { type => 'bedrijf', intern => 0 }, $id
    );

    unless ($subject) {
        throw('subject/id', 'ID not valid');
    }

    $subject->auth_rs->delete_all;

    $c->push_flash_message('Login voor betrokkene uitgeschakeld.');
    $c->res->redirect($c->uri_for('/betrokkene/' . $id, {
        type => 'bedrijf',
        gm   => 1
    }));
}

=head1 Disable subject session

ZAPI (POST) callable for removing / disabling the current active subject session, if any.

=head2 URL Construction

B</betrokkene/disable_session>

=cut

sub disable_betrokkene_session : POST : Chained('/') : PathPart('betrokkene/disable_session') : Args(0) {
    my ($self, $c) = @_;

    if(!$c->betrokkene_session_disable) {
        throw('betrokkene/session_disable_failed', 'Kon actieve betrokkene niet uitzetten.');
    }

    $c->stash->{zapi} = [];
    $c->detach('Zaaksysteem::View::ZAPI');
}

=head1 Enable a subject session

ZAPI (POST) callable for enabling the subject session for a specific subject.

=head2 URL Construction

B</betrokkene/enable_session>

=head2 POST variables

=over

=item * identifier

The identifier of the subject to set in the session.

=back

=cut

define_profile enable_betrokkene_session => (
    required => [qw(identifier)],
);

sub enable_betrokkene_session : Chained('/') : JSON : PathPart('betrokkene/enable_session') : Args(0) {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $betrokkene = $c->model('Betrokkene')->get({}, $opts->{identifier})
        or throw("betrokkene/not_found", "Betrokkene niet gevonden.");

    $c->betrokkene_session_enable($betrokkene);
    $self->prepare_page($c);

    $c->stash->{zapi} = [$betrokkene->as_hashref];

    return $c->detach('Zaaksysteem::View::ZAPI');
}

=head2 prepare_page

Retrieves the currently active subject session details, if any are set, and
puts them in the stash for the template to pick up.

=cut

sub prepare_page {
    my ($self, $c) = @_;

    if (!$c->user_exists) {
        return;
    }

    my $subject = $c->betrokkene_session;
    my $subject_data;
    if($subject) {
        $subject_data = $subject->as_hashref();
    }

    $c->stash->{betrokkene_session} = encode_json({subject => $subject_data});

    return;
}

sub _betrokkene_zaken : Private {
    my ($self, $c, $opts)   = @_;

    $c->stash->{results_per_page} = (
        $c->req->params->{results_per_page} ||
        $opts->{rows} ||
        10
    );

    my $resultset = $c->model('Zaken')->zaken_pip(
        {
            page                    => ($opts->{page} || $c->req->params->{'page'} || 1),
            rows                    => ($c->req->params->{results_per_page} || $opts->{rows} || 10),
            betrokkene_type         => $opts->{betrokkene}->btype,
            gegevens_magazijn_id    => $opts->{betrokkene}->ex_id,
            type_zaken              => $opts->{type_zaken},
            'sort_direction'        => $c->req->params->{sort_direction},
            'sort_field'            => $c->req->params->{sort_field},
        }
    );

    return $c->model('Zaken')->filter({
        resultset      => $resultset,
        textfilter     => $opts->{textfilter},
        dropdown       => (
            $opts->{statusfilter} ||
            $c->req->params->{'statusfilter'},
        )
    });
}

sub view : Chained('view_base'): PathPart('') : Args() {
    my ($self, $c) = @_;

    $c->stash->{template}   = 'betrokkene/view.tt';

    if ($c->check_any_user_permission(qw/contact_nieuw contact_search/)) {
        $c->stash->{can_betrokkene_edit} = 1;
    }


    if ($c->user_exists && $c->stash->{'betrokkene'}) {
        $c->stash->{'betrokkene'}->log_view(
            'betrokkene-medewerker-' . $c->user->uidnumber
        );
    }

    $c->stash->{force_result_finish} = 1;

    $c->stash->{zaken}  = $c->forward('_betrokkene_zaken', [
        {
            rows                    => 10,
            betrokkene              => $c->stash->{betrokkene},
            type_zaken              => ['resolved', 'overdragen', 'new', 'open'],
        }
    ]);
    $c->stash->{'zaken_display_fields'} = $c->model('SearchQuery')->get_display_fields();

    $c->stash->{open_zaken}  = $c->forward('_betrokkene_zaken', [
        {
            rows                    => 10,
            betrokkene              => $c->stash->{betrokkene},
            type_zaken              => ['new', 'open', 'stalled'],
            textfilter              => $c->req->params->{open_textfilter}
        }
    ]);

    # status
    {
        my $open_display_fields = {
            map { $_->{fieldname} => $_ }
            @{ $c->model('SearchQuery')->get_display_fields() }
        };

        $open_display_fields->{'days_left'}->{label} = 'Dagen';

        $c->stash->{'open_display_fields'} = [];
        push(
            @{ $c->stash->{'open_display_fields'} },
            $open_display_fields->{ $_ }
        ) for qw/status me.id zaaktype_node_id.titel
        me.onderwerp days_left/;
    }



    if ($c->stash->{betrokkene}->verblijfsobject) {
        $c->stash->{adres_zaken}        = $c->model('Zaken')->adres_zaken(
            {
                page                    => ($c->req->params->{'page'} || 1), 
                rows                    => 10,
                nummeraanduiding        => $c->stash->{betrokkene}
                                                ->verblijfsobject
                                                ->hoofdadres,
                'sort_direction'        => $c->req->params->{sort_direction},
                'sort_field'            => $c->req->params->{sort_field},
            }
        );

        $c->stash->{'adres_display_fields'} = $c->model('SearchQuery')->get_display_fields();
    }
    
    my $betrokkene_obj = $c->stash->{betrokkene};
    if (
        $betrokkene_obj &&
        $betrokkene_obj->can('messages') &&
        $betrokkene_obj->messages &&
        scalar(keys %{ $betrokkene_obj->messages })
    ) {
        $c->push_flash_message(
            'Let op: '
                . join(', ',
                    map(
                        { ucfirst($_) }
                        values %{ $betrokkene_obj->messages }
                    )
                ),
        );
    }

    ### Meldingen
    $c->stash->{ $_ } = $c->req->params->{ $_ } for qw/sort_direction sort_field/;
}

sub search : Chained('base'): PathPart('search') {
    my ($self, $c) = @_;

    $c->stash->{ $_ } = $c->req->params->{ $_ } for (
        keys (%{ $c->req->params })
    );

    my $stufconfig = $c->model('DB::Interface')
                ->find_by_module_name('stufconfig');

    if ($stufconfig && $stufconfig->active) {
        my $stuf_params = $stufconfig->get_interface_config;

        if ($stuf_params && $stuf_params->{bidirectional} && $stuf_params->{makelaar_search}) {
            $c->stash->{search_stuf} = 1;
        }
    }

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;

        $c->stash->{betrokkene_type} = $c->req->params->{betrokkene_type} ||
            $c->req->params->{jstype};

        if (exists($c->req->params->{search})) {
            my %sparams = ();

            for my $key (keys %{ $c->req->params }) {
                if ($c->req->params->{$key}) {
                    my $rawkey = $key;
                    $key =~ s/np-//g;
                    $sparams{$key} = $c->req->params->{$rawkey};
                }
            }

            # Geboortedatum...
            if ($sparams{'geboortedatum-dag'}) {
                $sparams{'geboortedatum'} =
                    sprintf('%02d', $sparams{'geboortedatum-jaar'}) . '-'
                    . sprintf('%02d', $sparams{'geboortedatum-maand'}) . '-'
                    .$sparams{'geboortedatum-dag'};
            } elsif ($sparams{'geboortedatum'}) {
                $sparams{'geboortedatum'} =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
            }


            my $rows_per_page = $c->req->param('rows_per_page') || 40;
            delete($sparams{$_}) for qw/import_datum url method jscontext jsversion jsfill submit search jstype rows_per_page/;

            my $type    = $c->req->params->{jstype};
            if ($c->req->params->{jsversion} == 3) {
                $c->log->debug('Betrokkene server VERSION 3');
                delete($sparams{$_}) for grep { /^ezra_client_info/ } keys %{
                    $c->req->params
                };
                $type   = $c->req->params->{betrokkene_type};
            }

            $c->stash->{betrokkene_type} = $type;

            delete($sparams{betrokkene_type});


            $c->stash->{template} = 'betrokkene/popup/search_resultrows.tt';

            if (
                $c->stash->{search_stuf} &&
                $c->req->params->{external_search}
            ) {
                my $stufprs     = $c->model('DB::Interface')
                                ->find_by_module_name('stufprs');

                if ($sparams{geboortedatum}) {
                    $sparams{geboortedatum} =~ s/-//g;
                }

                $c->stash->{results} = $stufprs->process_trigger(
                    'search',
                    \%sparams
                );

                $c->log->debug('makelaar subjects' . Dumper(
                        $c->stash->{results}
                ));
            } else {
                if ($c->req->params->{inactive_search}) {
                    $sparams{inactive_search} = 1;
                }

                my $betrokkenen = $c->model('Betrokkene')->search(
                    {
                        type    => $type,
                        intern  => 0,
                        rows_per_page => $rows_per_page,
                    },
                    \%sparams
                );

                $c->stash->{results} = [];

                if ($betrokkenen) {
                    while (my $bet = $betrokkenen->next) {
                        push (@{ $c->stash->{results} }, $bet);
                    }
                }

            }

            $c->detach;
        }

        $c->stash->{template} = 'betrokkene/popup/search.tt';
    } else {
        $c->stash->{template} = 'betrokkene/search.tt';

        ## Paging
        $c->stash->{ $_ } = $c->req->params->{ $_ }
            for grep {
                $c->req->params->{ $_ } &&
                $c->req->params->{ $_ } =~ /^\d+/
            } qw/paging_page paging_rows/;

        my %sparams = ();
        my ($startsearch, $betrokkene_type);

        if (exists($c->req->params->{search})) {
            for my $key (keys %{ $c->req->params }) {
                if ($c->req->params->{betrokkene_type} eq 'natuurlijk_persoon') {
                    if ($c->req->params->{$key} && $key =~ /^np-/) {
                        my $rawkey = $key;
                        $key =~ s/np-//g;
                        $sparams{$key} = $c->req->params->{$rawkey};
                    }
                } elsif ($c->req->params->{betrokkene_type} eq 'bedrijf') {
                    my $rawkey = $key;
                    next if (
                        lc($rawkey) eq 'search' ||
                        lc($rawkey) eq 'betrokkene_type'
                    );
                    $sparams{$key} = $c->req->params->{$rawkey};
                } elsif ($c->req->params->{betrokkene_type} eq 'medewerker') {
                    my $rawkey = $key;
                    next if (
                        lc($rawkey) eq 'search' ||
                        lc($rawkey) eq 'betrokkene_type'
                    );
                    $sparams{$key} = $c->req->params->{$rawkey};
                }

            }
            $betrokkene_type = $c->req->params->{'betrokkene_type'};

            $startsearch++;
        } elsif (
            (
                $c->stash->{paging_page} ||
                $c->req->params->{order}
            ) && $c->session->{betrokkene_search_data}
        ) {
            %sparams            = %{ $c->session->{betrokkene_search_data} };
            $betrokkene_type    = $c->session->{betrokkene_type};
            $startsearch++;
        } else {
            delete($c->session->{betrokkene_search_data});
        }

        if ($startsearch) {
            $c->session->{betrokkene_search_data} = \%sparams;
            $c->session->{betrokkene_type} = $betrokkene_type;

            $c->stash->{template} = 'betrokkene/search_results.tt';

            $c->log->debug('Search for betrokkene with params' .
                Dumper(\%sparams));


            # Geboortedatum...
            if ($sparams{'geboortedatum-dag'}) {
                $sparams{'geboortedatum'} =
                    sprintf('%02d', $sparams{'geboortedatum-jaar'}) . '-'
                    . sprintf('%02d', $sparams{'geboortedatum-maand'}) . '-'
                    .$sparams{'geboortedatum-dag'};
            } elsif ($sparams{'geboortedatum'}) {
                $sparams{'geboortedatum'} =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
            }

$c->log->debug("contact search params: " . Dumper \%sparams);
            $c->stash->{betrokkenen} = $c->model('Betrokkene')->search(
                {
                    type    => $betrokkene_type,
                    intern  => 0,
                },
                \%sparams
            );

            $c->stash->{betrokkene_type} = $betrokkene_type;
        }

    }
}

sub external_import : Chained('/'): PathPart('betrokkene/external_import'): Args(0) {
    my ($self, $c)  = @_;

    if ($c->req->params->{system_of_record_id}) {
        my $stufprs     = $c->model('DB::Interface')
                        ->find_by_module_name('stufprs');

        my $transaction;
        eval {
            $transaction = $stufprs->process_trigger(
                'import',
                {
                    sleutelGegevensbeheer   => $c->req->params->{system_of_record_id},
                }
            );
        };

        if ($@ || !$transaction || !$transaction->success_count) {
            $c->stash->{json} = $c->format_error($@);
        } else {
            my $record      = $transaction->records->first;

            my $npm         = $record->transaction_record_to_objects->search(
                {
                    'local_table'           => 'NatuurlijkPersoon',
                    'transaction_record_id' => $record->id,
                }
            )->first;

            $c->stash->{json} = $c->view('JSON')->prepare_json_row(
                {
                    subject_source_identifier   => 'betrokkene-natuurlijk_persoon-' . $npm->local_id
                }
            );
        }
    }

    $c->forward('View::JSON');
}



sub snapshot : Chained('base'): PathPart('snapshot'): Args(2) {
    my ($self, $c, $betrokkene_type, $id) = @_;


    $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get(
        {
            intern  => 1,
            type    => $betrokkene_type,
        },
        $id
    );

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'betrokkene/popup/get.tt';
}


sub get : Chained('base'): PathPart('get'): Args(1) {
    my ($self, $c, $id) = @_;
    
    die "betrokkene_id not set" unless($id);

    my $params = $c->req->params;

    if ($params->{betrokkene_type}) {
        $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => $params->{betrokkene_type},
            },
            $id
        ) or return;
    } else {
        $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get({}, $id)
            or return;
    }

    if ($c->user_exists && $c->stash->{'betrokkene'}) {
        $c->stash->{'betrokkene'}->log_view(
            'betrokkene-medewerker-' . $c->user->uidnumber
        );
    }

    if ($params->{actueel} && $params->{actueel} =~ /^\d+$/) {
        if ($c->stash->{'betrokkene'}->gm_extern_np) {
            my $gegevens_magazijn_id =
                $c->stash->{'betrokkene'}->gm_extern_np->id;

            $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
                {
                    intern  => 0,
                    type    => $c->stash->{betrokkene}->btype,
                },
                $gegevens_magazijn_id
            );

            $c->log->debug('Externe betrokkene vraag');
        }
    }

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'betrokkene/popup/get.tt';
    }
}



{
    sub _load_update_profile {
        my ($self, $c) = @_;

        if ($c->req->params->{betrokkene_type} eq 'bedrijf') {
            ### Get profile from Model
            my $profile = $c->get_profile(
                'method'=> 'create',
                'caller' => 'Zaaksysteem::Betrokkene::Object::Bedrijf'
            ) or return;

            my @required_fields = grep {
                $_ ne 'vestiging_postcodewoonplaats' ||
                $_ ne 'vestiging_adres'
            } @{ $profile->{required} };

            push(@required_fields, 'rechtsvorm');

            $profile->{required} = \@required_fields;

            $c->register_profile(
                method => 'update',
                profile => $profile,
            );
        } else {
            $c->register_profile(
                method => 'update',
                profile => 'Zaaksysteem::Controller::Betrokkene::create'
            );
        }
    }

    my $BETROKKENE_MAP = {
        bedrijf => 2,
        natuurlijk_persoon => 1,
    };

    Zaaksysteem->register_profile(
        method => 'update',
        profile => VALIDATION_CONTACT_DATA
    );

    sub update : Chained('base'): PathPart('info/update'): Args(1) {
        my ($self, $c, $gmid) = @_;

        return unless $c->check_any_user_permission(qw/contact_nieuw contact_search/);

        my $params = $c->req->params();

        ### Betrokkene edit only 
        if ($params->{betrokkene_edit}) {
            $self->_load_update_profile($c)
                if $params->{betrokkene_edit};
        } else {
            $c->register_profile(
                'method'    => 'update',
                profile     => VALIDATION_CONTACT_DATA,
            );
        }

        if ($c->req->is_xhr) {
            $c->zvalidate;
            $c->detach;
        }

        ### END Betrokkene edit only

        my $contact_data = $c->model('DB::ContactData')->search({
            gegevens_magazijn_id  => $gmid,
            betrokkene_type         => $BETROKKENE_MAP->{
                $params->{betrokkene_type}
            },
        });

        my $betrokkene_ident = sprintf(
            'betrokkene-%s-%d',
            $params->{ betrokkene_type },
            $gmid
        );

        if ($contact_data->count) {
            $contact_data = $contact_data->first;
        } else {
            $contact_data = $c->model('DB::ContactData')->create({
                    gegevens_magazijn_id    => $gmid,
                    betrokkene_type         => $BETROKKENE_MAP->{
                        $params->{betrokkene_type}
                    },
            });
        }

        # Update niet authentieke gegevens
        if ($params->{betrokkene_edit} && (my $dv = $c->zvalidate)) {
            my $gmbetrokkene = $c->model('Betrokkene')->get(
                {
                    type    => $params->{betrokkene_type},
                    intern  => 0,
                },
                $gmid
            );

            unless ($gmbetrokkene->authenticated) {
                if ($params->{betrokkene_type} eq 'bedrijf') {

                    if($c->req->param('set_can_login')) {
                        unless($gmbetrokkene->can_login) {
                            $gmbetrokkene->create_auth;
                        }
                    } else {
                        if($gmbetrokkene->can_login) {
                            $gmbetrokkene->auth_rs->delete;

                            $c->log->debug(sprintf(
                                'Removed BedrijfAuthenticatie for %s',
                                $gmbetrokkene->dossiernummer
                            ));
                        }
                    }

                    my $params = $dv->valid;
                    for my $dbkey (keys %{ $params }) {

                        $gmbetrokkene->$dbkey($params->{$dbkey})
                            if $gmbetrokkene->can($dbkey);
                    }
                } else {
                    for my $pkey (grep(/^np-/, keys %{ $params })) {
                        my $dbkey = $pkey;
                        $dbkey =~ s/^np-//g;

                        warn('UPDATING:' . $dbkey);

                        $gmbetrokkene->$dbkey($params->{$pkey})
                            if $gmbetrokkene->can($dbkey);
                    }
                }
            }
        }

        # Update contactgegevens
        if ($c->zvalidate) {
            $contact_data->mobiel($params->{'npc-mobiel'});
            $contact_data->telefoonnummer($params->{'npc-telefoonnummer'});
            $contact_data->email($params->{'npc-email'});
            $contact_data->update;

            $c->model('DB::Logging')->trigger('subject/update', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $gmid,
                created_for => $betrokkene_ident,
                data => {
                    subject_id => $betrokkene_ident,
                    parameters => $params
                }
            });
        }

        if ($c->stash->{zaak}) {
            $c->res->redirect($c->uri_for('/zaak/' . $c->stash->{zaak}->nr));
        } else {
            # Remove edit on post
            my $referer = $c->req->referer;
            $referer =~ s/[&\?]?edit=1//;

            $c->res->redirect($referer);
        }
    }
}

{
    sub verwijder : Chained('base'): PathPart('verwijder'): Args(2) {
        my ($self, $c, $betrokkene_type, $gmid) = @_;

        return unless $c->check_any_user_permission(qw/contact_nieuw contact_search/);

        return unless $gmid;

        my $gmbetrokkene = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $gmid
        );
        
        my $params = $c->req->params();

        # Update niet authentieke gegevens
        if (%$params && $params->{confirmed}) {
            $c->response->redirect(
                $c->uri_for(
                    '/betrokkene/search'
                )
            );

            do {
                $c->push_flash_message('Deze betrokkene kan niet'
                    . ' worden verwijderd');
                $c->detach;
            } unless $gmbetrokkene->can_verwijderen;

            if ($gmbetrokkene->verwijder) {
                my $event = $c->model('DB::Logging')->trigger('subject/remove', {
                    component => LOGGING_COMPONENT_BETROKKENE,
                    component_id => $gmid,
                    data => {
                        subject_name => $gmbetrokkene->naam,
                        subject_id => $gmid
                    }
                });

                $c->push_flash_message($event->onderwerp);
            }
        }

        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u betrokkene "'
            . $gmbetrokkene->naam . '" wilt verwijderen?';

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/betrokkene/verwijder/' . $betrokkene_type . '/' . $gmid
            );


        $c->forward('/page/confirmation');
        $c->detach;
    }
}

sub betrokkene : Chained('/') : PathPart('betrokkene'): CaptureArgs(1) {
    my ($self, $c, $betrokkene_identifier) = @_;

    if ($betrokkene_identifier) {
        my ($betrokkene_type, $betrokkene_id)
            = $betrokkene_identifier =~ /^betrokkene-(.*?)-(\d+)$/;

        unless ($betrokkene_type && $betrokkene_id) {
            $c->res->redirect($c->uri_for('/'));
            $c->detach;
        }

        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {
                'type'      => $betrokkene_type,
                'intern'    => 0,
            },
            $betrokkene_id
        );
    }
}

my $rechtsvormen = [];
{
    my $kvkrechtsvormen_enabled = ZAAKSYSTEEM_CONSTANTS
                                    ->{kvk_rechtsvormen_enabled};

    for my $code (@{ $kvkrechtsvormen_enabled }) {
        if (ZAAKSYSTEEM_CONSTANTS->{kvk_rechtsvormen}->{ $code }) {
            push(@{ $rechtsvormen },
                {
                    value   => $code,
                    label   => ZAAKSYSTEEM_CONSTANTS
                        ->{kvk_rechtsvormen}
                        ->{ $code }
                }
            );
        }
    }
}

my $BETROKKENE_TEMPLATE = {
    natuurlijk_persoon  => [
        {
            label   => 'BSN',
            name    => 'np-burgerservicenummer',
            classes => ['input_medium'],
        },
        {
            label   => 'Voornamen',
            name    => 'np-voornamen',
            classes => ['input_medium'],
        },
        {
            label   => 'Tussenvoegsel',
            name    => 'np-voorvoegsel',
            classes => ['input_mini'],
        },
        {
            label   => 'Achternaam',
            name    => 'np-geslachtsnaam',
            classes => ['input_medium'],
        },
        {
            label       => 'Geslacht',
            name        => 'np-geslachtsaanduiding',
            type        => 'radio',
            options     => [
                {
                    label   => 'Man',
                    name    => 'np-geslachtsaanduiding',
                    value   => 'M',
                },
                {
                    label   => 'Vrouw',
                    name    => 'np-geslachtsaanduiding',
                    value   => 'V',
                }
            ],
        },
        {
            label   => 'Straat',
            name    => 'np-straatnaam',
            classes => ['input_medium'],
        },
        {
            label   => 'Huisnummer',
            name    => 'np-huisnummer',
            classes => ['input_mini'],
        },
        {
            label   => 'Huisnummer toevoeging',
            name    => 'np-huisnummertoevoeging',
            classes => ['input_mini'],
        },
        {
            label       => 'Postcode',
            name        => 'np-postcode',
            classes     => ['input_mini'],
            post_label  => '1234AZ',
        },
        {
            label   => 'Woonplaats',
            name    => 'np-woonplaats',
            classes => ['input_medium'],
        },
    ],
    bedrijf     => [
        {
            label   => 'Inloggen met eHerkenning',
            name    => 'set_can_login',
            type    => 'checkbox',
            checked => 1
        },
        {
            label   => 'Rechtsvorm',
            name    => 'rechtsvorm',
            type    => 'select',
            options => $rechtsvormen,
        },
        {
            label   => 'KVK-nummer',
            name    => 'dossiernummer',
            classes => ['input_medium'],
        },
        {
            label   => 'Vestigingsnummer',
            name    => 'vestigingsnummer',
            classes => ['input_medium'],
        },
        {
            label   => 'Handelsnaam',
            name    => 'handelsnaam',
            classes => ['input_medium'],
        },
        {
            label   => 'Vestiging straat',
            name    => 'vestiging_straatnaam',
            classes => ['input_medium'],
        },
        {
            label   => 'Vestiging huisnummer',
            name    => 'vestiging_huisnummer',
            classes => ['input_mini'],
        },
        {
            label   => 'Vestiging toevoeging',
            name    => 'vestiging_huisnummertoevoeging',
            classes => ['input_mini'],
        },
        {
            label   => 'Vestiging postcode',
            name    => 'vestiging_postcode',
            classes => ['input_mini'],
        },
        {
            label   => 'Vestiging woonplaats',
            name    => 'vestiging_woonplaats',
            classes => ['input_medium'],
        },
    ],
};

sub bewerken : Chained('betrokkene') : PathPart('bewerken'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{betrokkene_type}    = (
        $c->req->params->{betrokkene_type}
        || 'natuurlijk_persoon'
    );

    $c->stash->{betrokkene_type}    = 'natuurlijk_persoon'
        unless defined($BETROKKENE_TEMPLATE->{
                $c->stash->{betrokkene_type}
        });

    $c->stash->{betrokkene_template} = $BETROKKENE_TEMPLATE->{
        $c->stash->{betrokkene_type}
    };

    my $profile;
    if ($c->stash->{betrokkene_type} eq 'bedrijf') {
        $profile                    = $c->forward(
            '/betrokkene/bedrijf/bedrijven_profile'
        );
    } else {
        $profile                    = Zaaksysteem->get_profile(
            method  => __PACKAGE__ . '::create'
        );
    }

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie           => $profile,
            user_permissions    => [qw/contact_nieuw/],
            template            => 'widgets/betrokkene/bewerken.tt',
        }])
    ) {
        my $params  = $dv->valid;

        if (
            my $id = $c->forward(
                '_create_betrokkene',
                [
                    $c->stash->{betrokkene_type},
                    $params
                ],
            )
        ) {
            my $betrokkene = $c->model('Betrokkene')->get(
                {},
                'betrokkene-' . $c->stash->{betrokkene_type} . '-' . $id
            );

            if($c->stash->{ betrokkene_type } eq 'bedrijf' && $c->req->param('set_can_login')) {
                if ($betrokkene->create_auth) {

                    $c->log->debug(sprintf(
                        'Created BedrijfAuthenticatie for %s',
                        $betrokkene->dossiernummer
                    ));
                }
            }

            # Add logging            
            my $logging_description = join ", ", map { $_ . ': ' . $params->{$_} } sort keys %$params;

            $c->model('DB::Logging')->trigger('subject/create', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $id,
                data => {
                    subject_id => $betrokkene->betrokkene_identifier,
                    parameters => $params
                }
            });

            # TODO what the actual fuck guys...
            $c->stash->{json} = {
                'succes'    => 1,
                'bericht'   => 'Betrokkene aangemaakt: '
                    .'<a href="' . $c->uri_for(
                        '/betrokkene/' . $id,
                        {
                            gm      => 1,
                            type    => $c->stash->{betrokkene_type}
                        }
                    ) . '">' .  $betrokkene->display_name . '</a>'
                    . ' (<a href="' . $c->uri_for(
                        '/zaak/create',
                        {
                            aanvraag_trigger    => 'extern',
                            betrokkene_naam     => $betrokkene->display_name,
                            betrokkene_id       =>
                                $betrokkene->betrokkene_identifier,
                            betrokkene_type     => $betrokkene->btype,
                        },
                    ) . '" class="ezra_nieuwe_zaak_tooltip-show">'
                    . 'Zaak aanmaken</a>)'
            }
        } else {
            $c->stash->{json} = {
                'succes'    => 0,
                'bericht'   => 'Fout bij aanmaken betrokkene',
            }
        }

        $c->forward('Zaaksysteem::View::JSONlegacy');
    }
}

sub _create_betrokkene : Private {
    my ($self, $c, $betrokkene_type, $opts)  = @_;

    $opts->{'np-postcode'} = uc($opts->{'np-postcode'})
        if defined($opts->{'np-postcode'});

    my $id = $c->model('Betrokkene')->create(
        $betrokkene_type,
        {
            %{ $opts },
            authenticatedby =>
                ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
        }
    );

    return $id;
}



1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


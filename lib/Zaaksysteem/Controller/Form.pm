package Zaaksysteem::Controller::Form;

use Moose;
use HTML::TagFilter;
use HTML::Entities;
use JSON;
use Clone qw/clone/;
use Try::Tiny;
use Scalar::Util qw/blessed/;
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;
use Data::Dumper;

BEGIN { extends 'Catalyst::Controller'; }
with 'Catalyst::TraitFor::Controller::reCAPTCHA';

use constant RELATIE_TABLE_CONFIG => {
    'header'    => [
        {
            label   => 'Relatietype',
            mapping => 'type',
        },
        {
            label   => 'Naam',
            mapping => 'betrokkene_naam',
        },
        {
            label   => 'Rol',
            mapping => 'rol'
        },
    ],
    'options'   => {
        data_source         => '/form/register_relaties',
        search_action       => '/form/register_relaties/add',
        row_identifier      => 'betrokkene_identifier',
        has_delete_button   => 1,
        init                => 1,
        add                 => {
            label   => 'Toevoegen',
            popup   => 1,
        },
    }
};


sub form : Chained('/'): PathPart('form'): Args(0) {
    my ($self, $c) = @_;

    $c->detach('list');
}

sub form_with_id : Chained('/'): PathPart('form'): Args() {
    my ($self, $c, $id) = @_;

    $c->res->redirect($c->uri_for(
        '/zaak/create/webformulier/',
        { zaaktype => $id, sessreset => 1 }
    ));

    $c->detach;
}

sub cancel : Local {
    my ($self, $c) = @_;

    delete($c->session->{ _zaak_create });

    $c->res->redirect($c->config->{ gemeente }{ gemeente_portal });
    $c->detach;
}

sub casetype_offline : Local {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'form/casetype_offline.tt';
}


sub form_by_zaaktype_afronden : Chained('/') : PathPart('aanvraag'): Args(3) {
    my ($self, $c, $zaaktype_naam, $type_aanvrager, $afronden) = @_;

    $c->stash->{ afronden } = $afronden;

    $c->forward('form_by_zaaktype', [ $zaaktype_naam, $type_aanvrager ]);
}

=head2 form_by_zaaktype_id

Convert a human-readable url into a redirect, linking to the first step
of the case creation process.

I changed the preample to aanvragen - it's similar enough to aanvraag to not
disrupt, and otherwise I'd need to check for casetype names with only digits.

Arguments:
    casetype_id: numeric
    subject_type: persoon|organisatie|onbekend

=cut

sub form_by_zaaktype_id : Chained('/') : PathPart('aanvragen') : Args() {
    my ($self, $c, $casetype_id, $subject_type) = @_;

    my $retro_aanvrager_type = {
        persoon => 'natuurlijk_persoon',
        organisatie => 'niet_natuurlijk_persoon',
        onbekend => 'unknown'
    };

    throw('controller/form_by_zaaktype_id/invalid_subject_type',
        'Ongeldig aanvrager type: ' . $subject_type)
        unless $subject_type && exists $retro_aanvrager_type->{$subject_type};

    my $zaaktype = $c->model('DB::Zaaktype')->find($casetype_id)
        or throw('controller/form_by_zaaktype_id/casetype_not_found',
            "Zaaktype $casetype_id niet gevonden");

    $c->forward('form_by_zaaktype_create', [
        $zaaktype,
        $retro_aanvrager_type->{$subject_type}
    ]);
}


=head2

Legacy support for previously published urls.

=cut

sub form_by_zaaktype : Chained('/') : PathPart('aanvraag'): Args(2) {
    my ($self, $c, $zaaktype_naam, $type_aanvrager) = @_;

    $zaaktype_naam =~ s/-/ /g;

    my $zaaktype = $c->model('DB::Zaaktype')->find_by_lowercase_title($zaaktype_naam);

    $c->forward('form_by_zaaktype_create', [$zaaktype, $type_aanvrager]);
}


sub form_by_zaaktype_create : Private {
    my ($self, $c, $zaaktype, $type_aanvrager) = @_;

    $c->session->{ _zaak_create }{ ztc_aanvrager_type } = $type_aanvrager;

    my $afronden = $c->stash->{ afronden } ? 1 : 0;

    unless($zaaktype->active) {
        $c->stash->{ casetype_node } = $zaaktype->zaaktype_node_id;
        $c->detach('casetype_offline');
    }

    my $args = {
        ztc_aanvrager_type    => $type_aanvrager,
        sessreset             => '1',
        zaaktype_id           => $zaaktype->id,
        afronden              => $afronden
    };

    if($type_aanvrager eq 'natuurlijk_persoon') {
        $args->{authenticatie_methode} = 'digid';
    } elsif($type_aanvrager eq 'niet_natuurlijk_persoon') {
        $args->{authenticatie_methode} = 'bedrijfid';
    } elsif($type_aanvrager eq 'unknown') {
        my $preset_client = $zaaktype->zaaktype_node_id->zaaktype_definitie_id->preset_client;

        die "need preset_client" unless($preset_client);

        $args->{aanvrager} = $preset_client;

        # Clear SAML login attributes
        delete $c->session->{ _saml };
        delete $c->session->{ pip };

        $c->model('Plugins::Bedrijfid')->logout;

    }

    $c->res->redirect(
        $c->uri_for(
            '/zaak/create/webformulier', 
            $args
        )
    );
}

sub list : Private {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen}  = $c->model('DB::Zaaktype')->search(
        {
            'me.deleted'                        => undef,
            'zaaktype_node_id.trigger'          => [
                'extern', 'internextern'
            ],
            'zaaktype_node_id.webform_toegang'  => 1,
            'me.active'                         => 1,
        },
        {
            'prefetch'      => 'zaaktype_node_id',
        }
    );

    $c->stash->{template}   = 'form/list.tt';
}


sub aanvrager_type : Private {
    my ($self, $c)          = @_;

    $c->stash->{template}   = 'form/aanvrager_type.tt';
}


sub aanvrager : Private {
    my ($self, $c)      = @_;
    my (%betrokkene_opts, %searchopts, $searchcolumn);

    $c->stash->{template} = 'form/aanvrager.tt';

    my $verified = $c->session->{_zaak_create}->{extern}->{verified} || '';
    $c->log->debug("verified: " . $verified);
    
    my $betrokkene;
    
    if ($verified eq 'digid') {
        $searchcolumn = 'gm_natuurlijk_persoon_id';
        $searchopts{ burgerservicenummer } = $c->session->{_zaak_create}->{extern}->{id};

        $c->log->debug("burgerservicenummer: " . $c->session->{_zaak_create}->{extern}->{id});

        %betrokkene_opts = (
            type    => 'natuurlijk_persoon',
            intern  => 0,
        );

        my $brs = $c->model('Betrokkene')->search(\%betrokkene_opts, \%searchopts);

        $betrokkene = $brs->next if $brs;
    } elsif($verified eq 'preset_client') { 

        $searchopts{id} = $c->session->{_zaak_create}->{extern}->{id};

        %betrokkene_opts = (
            type    => $c->session->{_zaak_create}->{extern}->{aanvrager_type},
            intern  => 0,
        );
        
        $betrokkene = $c->model('Betrokkene')->get(
            {},
            'betrokkene-' . 
                $c->session->{_zaak_create}->{extern}->{aanvrager_type} . '-'.
                $c->session->{_zaak_create}->{extern}->{id}
        );
    
    }else {
        $searchopts{dossiernummer}    = $c->session
            ->{_zaak_create}->{extern}->{id};

        %betrokkene_opts                    = (
            type    => 'bedrijf',
            intern  => 0,
        );

        $searchcolumn   = 'gm_bedrijf_id';
        my $betrokkene_resultset = $c->model('Betrokkene')
                ->search(\%betrokkene_opts, \%searchopts);

        $betrokkene = $betrokkene_resultset->next 
            if $betrokkene_resultset;
    }

    my $authentication_method = $c->session->{form}->{authenticatie_methode} || '';
    ### LOGGING
    if (
        $authentication_method eq
        ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ) {
        $c->log->debug('Checking burgerservicenumber: ' . $c->session->{ _saml }{ uid });
    } elsif (
        $authentication_method eq
            ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID
    ) {
        $c->log->debug('Checking dossiernummer: ' .
            $c->model('Plugins::Bedrijfid')->login || $c->session->{ _saml }{ uid }
        );
    }

    ### Aanvrager update AND aanvrager has put in his correct credentials:
    if (
        $c->session->{_zaak_create}->{aanvrager_update} &&
        $c->req->param('skip_ahead') != 1 &&
        scalar(keys(%{
            $c->session->{_zaak_create}->{aanvrager_update}
        }))
    ) {
        $c->forward('webform');
    }

    if($verified eq 'preset_client') {
        $c->stash->{aanvrager}  = $betrokkene;
    
        ### Set ztc_aanvrager_id
        $c->session->{_zaak_create}->{ztc_aanvrager_id} =
            $betrokkene->betrokkene_identifier;
        $c->session->{_zaak_create}->{aanvraag_trigger} = 'extern';
        $c->forward($c->session->{ _zaak_create }{ ztc_aanvrager_type } eq 'unknown' ? 'webform' : 'zaakcontrole');
        $c->detach();
    }

    ### person not found? Well...show form, it's impossible
    ### for bedrijven to not be found, they wouldn't have a login
    ### and password anyway.
    $self->_check_mogelijke_aanvragers($c, $betrokkene);

    if (
        !$betrokkene ||
        (
            $betrokkene->btype eq 'natuurlijk_persoon' &&
            !$betrokkene->authenticated && $betrokkene->authenticated_by ne
                ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
        )
    ) {
        # Redirect to broker loader template as these requests can take a very long time. (And
        # giving some feedback to the user is good.)
        if (_search_stuf_allowed($c) && !$c->req->param('ran_stuf')) {
            $c->stash->{load_person_from_broker} = 1;
        }

        # If a betrokkene can't be found, default to displaying a form that can be filled out.
        if (!$betrokkene) {
            $c->stash->{aanvrager_bsn} = $c->session->{ _saml }{ uid };
            $c->stash->{aanvrager_edit} = 1;
            $c->forward('_generate_navigation');
            $c->detach;
        }
    }

    $c->stash->{aanvrager}  = $betrokkene;

    if (
        $betrokkene->can('messages') &&
        $betrokkene->messages
    ) {
        my @errors = qw/onderzoek deceased/;

        for (@errors) {
            $c->stash->{aanvrager_error} = $betrokkene->messages if
                $betrokkene->messages->{ $_ };
        }

    }

    ### Set ztc_aanvrager_id
    $c->session->{_zaak_create}->{ztc_aanvrager_id} =
        $betrokkene->betrokkene_identifier;

    ### Zoek laatste zaak
     my $params = $c->req->params;
    
    $c->stash->{ _nav_position } = 'aanvrager';

    if ($params->{aanvrager_update}) {
        $c->forward('update_aanvrager_contact_data');

        if($params->{ skip_ahead }) {
            delete $c->stash->{ _nav_position };

            $c->detach('webform');
        }

        $c->forward('zaakcontrole');
        $c->stash->{ _nav_position } = 'zaakcontrole';
    }

    $c->forward('_generate_navigation');

    if($c->req->param('afronden')) {
        
        $c->forward('webform');
    }

}

my $STUF_MAP = {
    voorletters                 => 'voorletters',
    voornamen                   => 'voornamen',
    voorvoegsel                 => 'tussenvoegsel',
    geslachtsnaam               => 'geslachtsnaam',
    geslachtsaanduiding         => 'geslachtsaanduiding',
    straatnaam                  => 'straatnaam',
    huisnummer                  => 'huisnummer',
    huisnummertoevoeging        => 'huisnummertoevoeging',
    huisletter                  => 'huisletter',
    postcode                    => 'postcode',
    woonplaats                  => 'woonplaats',
};

sub _load_external_requestor : Private {
    my ($self, $c)  = @_;

    my $stufprs = $c->model('DB::Interface')->find_by_module_name('stufprs');

    $c->stash->{results} = $stufprs->process_trigger(
        'search',
        {
                burgerservicenummer => $c->stash->{aanvrager_bsn},
        }
    );

    $c->log->debug('makelaar subjects' . Dumper(
            $c->stash->{results}
    ));

    if (@{ $c->stash->{results}} > 1) {
        die 'Found more than one person for single BSN';
    }

    for my $prs (@{ $c->stash->{results}}) {
        if (!$prs->{sleutelGegevensbeheer}) {
            die "No sleutelGegevensbeheer present in return data for person";
        }

        my $transaction = $stufprs->process_trigger(
            'import',
            {
                sleutelGegevensbeheer   => $prs->{sleutelGegevensbeheer}
            }
        );
        return $transaction;
    }
}

sub _parse_stuf_value {
    my $self    = shift;
    my $value   = shift;

    if (UNIVERSAL::isa($value, 'HASH') && defined($value->{_})) {
        $value = $value->{_};
    }

    if ($value eq 'NIL' || $value eq 'NIL:geenWaarde') {
        $value = '';
    }

    return $value;
}


sub update_aanvrager_contact_data : Private {
    my ($self, $c)      = @_;

    my $params          = $c->req->params;

    my %contact_params;
    for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
        my $value;
        if (defined($params->{ $_ })) {
            $value = $c->req->params->{ $_ };
        } elsif (defined($params->{ $_ })) {
            $value = $params->{$_};
        } else {
            next;
        }

        $contact_params{$_} = $value;
    }

    my $dv = Data::FormValidator->check(
        \%contact_params, VALIDATION_CONTACT_DATA
    );

    my $valid_params = $dv->valid;

    for my $raw_key (keys %{ $valid_params }) {
        my $key = $raw_key;
        $key =~ s/^npc-//g;

        $c->log->debug('Add aanvrager: ' . $key . ':' . $valid_params->{$raw_key});
        $c->stash->{aanvrager}->$key($valid_params->{$raw_key})
    }
}

sub _check_mogelijke_aanvragers {
    my $self                = shift;
    my $c                   = shift;
    my $betrokkene                 = shift;

    my $ztc_aanvrager_type  = $c->stash->{ aanvrager_type } // $c->session->{ _zaak_create }{ ztc_aanvrager_type };

    if ($ztc_aanvrager_type eq 'preset_client') {
        $c->log->debug("res: ".  $betrokkene);
    }
    elsif ($ztc_aanvrager_type =~ /^natuurlijk_persoon/) {
        $self->_check_mogelijke_aanvragers_personen($c, $betrokkene);
    } else {
        $self->_check_mogelijke_aanvragers_bedrijven($c, $betrokkene);
    }
}

sub _check_mogelijke_aanvragers_bedrijven {
    my $self                = shift;
    my $c                   = shift;
    my $betrokkene                 = shift;

    if (
        (
            !$betrokkene ||
            !$betrokkene->authenticated
        ) &&
        !grep(
         /^niet_natuurlijk_persoon_na$/,
         @{ $c->stash->{type_aanvragers} }
        )
    ) {
        $c->log->debug('U bent niet gevonden in onze KVK. Deze aanvraag is niet op u van toepassing.');
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }

    if (
        (
         $betrokkene &&
         $betrokkene->authenticated
        ) &&
        !grep(
         /^niet_natuurlijk_persoon$/,
         @{ $c->stash->{type_aanvragers} }
        )
    ) {
        $c->log->debug('U bent gevonden in de KVK binnen de gemeente. Deze aanvraag is
            alleen van toepassing op bedrijven buiten onze gemeente.');
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }
}

sub _check_mogelijke_aanvragers_personen {
    my $self                = shift;
    my $c                   = shift;
    my $betrokkene                 = shift;

    if (
        (
            !$betrokkene ||
            !$betrokkene->authenticated
        ) &&
        !grep(
         /^natuurlijk_persoon_na$/,
         @{ $c->stash->{type_aanvragers} }
        )
    ) {
        $c->log->debug('U bent geen inwoner van de gemeente. Deze aanvraag is niet op u van toepassing.');
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }

    if (
        (
         $betrokkene &&
         $betrokkene->authenticated
        ) &&
        !grep(
         /^natuurlijk_persoon$/,
         @{ $c->stash->{type_aanvragers} }
        )
    ) {
        $c->log->debug('U bent inwoner van de gemeente. Deze aanvraag is
            alleen van toepassing op personen buiten de gemeente.');
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }
}




sub _should_zaakcontrole : Private {
    my ($self, $c) = @_;

    ### Geen controle bij aanvrager onbekend
    return unless $c->stash->{aanvrager};

    # Check of de sessie op afronden staat
    return if $c->session->{afronden};

    ### Of geen zaakcontrole
    return unless $c->stash->{zaaktype}->aanvrager_hergebruik;

    ### Check zaken
    my $vorige_zaak = $self->get_vorige_zaak($c);
    
    return unless $vorige_zaak;
    return if $c->session->{ _zaak_create }{ ztc_aanvrager_type } eq 'unknown';

    return 1;
}

sub get_vorige_zaak {
    my ($self, $c) = @_;

    ### Check zaken
    return $c->model('DB::Zaak')->search_extended({
        'me.aanvrager_gm_id' => $c->stash->{ aanvrager }->ex_id,
        'me.zaaktype_id'     => $c->stash->{ zaaktype }->zaaktype_id->id,
        'me.deleted'         => undef
    },
    {
        order_by    => { -desc => 'me.id' },
        rows        => 1,
    })->first;
}

sub zaakcontrole : Private {
    my ($self, $c) = @_;

    $c->detach('webform') unless $c->forward('_should_zaakcontrole');

    $c->stash->{ ztc_aanvrager_type } = $c->session->{ _zaak_create }{ ztc_aanvrager_type };

    my $vorige_zaak = $self->get_vorige_zaak($c);

    my $params = $c->req->params();

    if($params->{copy_gegevens} && $params->{copy_gegevens} eq '1') {

        my $registratie_fase = $vorige_zaak->registratie_fase;
        my $field_values = $vorige_zaak
            ->field_values({ 
                fase => $registratie_fase->status 
            });
        $c->session->{_zaak_create}->{form}->{kenmerken} = $field_values;
        $c->detach('webform');

    } elsif($vorige_zaak && !exists $params->{copy_gegevens}) {

        $c->stash->{vorige_zaak}    = $vorige_zaak;
        $c->stash->{template}       = 'form/zaakcontrole.tt';
        $c->stash->{ _nav_position } = 'zaakcontrole';
        $c->forward('_generate_navigation');

    } else {

        $c->detach('webform');

    }
}


=head2 submit_to_pip

Save the case creation blob to the database, ready for continuation
when the user decides to do so.

=cut

sub submit_to_pip : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $process_step_index = $params->{process_step_index} || 0;
    my $steps = $c->stash->{kenmerken_groups_keep_sort};

    throw("form/submit_to_pip", "illegal step index")
        unless $process_step_index < scalar @$steps && $process_step_index >= 0;

    my $zaak_create = $c->session->{_zaak_create};
    $zaak_create->{afronden_goto_step} = $process_step_index;

    my $encoder = JSON::XS->new->allow_nonref->allow_blessed->convert_blessed;

    $c->model('DB::ZaakOnafgerond')->update_or_create({
        zaaktype_id     => $zaak_create->{zaaktype_id},
        betrokkene      => $zaak_create->{ztc_aanvrager_id},
        json_string     => $encoder->encode($zaak_create),
        afronden        => 0,
        create_unixtime => time()
    });

    $c->stash->{template} = 'form/boodschap_onafgeronde_zaak.tt';
}


sub resume_from_pip : Private {
    my ($self, $c) = @_;

    my $zaak_create = $c->session->{_zaak_create};

    my $onafgeronde_zaak = $c->model('DB::ZaakOnafgerond')->search({
        zaaktype_id => $zaak_create->{zaaktype_id},
        betrokkene  => $zaak_create->{ztc_aanvrager_id}
    })->first;

    if ($onafgeronde_zaak) {
        $c->session->{_zaak_create} = JSON->new->decode($onafgeronde_zaak->json_string);
        $c->stash->{afronden_goto_step} = $c->session->{_zaak_create}->{afronden_goto_step};
    }
}


sub webform : Private {
    my ($self, $c) = @_;

    $c->forward('/form/resume_from_pip') if $c->req->params->{afronden};

    $c->stash->{ table_config } = RELATIE_TABLE_CONFIG;
    $c->stash->{ table_config }{ rows } = $c->session->{ _zaak_create }{ betrokkene_relaties };

    $c->stash->{ template } = $c->forward('_preprocess_webform');
    $c->stash->{ ztc_aanvrager_type } = $c->session->{ _zaak_create }{ ztc_aanvrager_type };

    $c->forward('_process_stap');

    my $status = $c->req->param('fase') || 1;
    my $kenmerken = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

    my $betrokkene = (
        $c->session->{_zaak_create}->{ztc_aanvrager_id} ||
        $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene}
    );

    my ($betrokkene_type, $betrokkene_id) = $betrokkene =~ m|betrokkene-(\w+)-(\d+)|;
    my $aanvrager_type = 'Niet natuurlijk persoon';

    if($betrokkene_type eq 'natuurlijk_persoon') {
        $aanvrager_type = 'Natuurlijk persoon';
    }

    my $betrokkene_obj = $c->model('Betrokkene')->get(
        { type => $betrokkene_type },
        $betrokkene_id
    ); 

    if (
        $betrokkene_obj &&
        $betrokkene_obj->can('messages') &&
        $betrokkene_obj->messages &&
        scalar(keys %{ $betrokkene_obj->messages })
    ) {
        $c->push_flash_message(
            'Let op: '
                . join(', ',
                    map(
                        { ucfirst($_) }
                        values %{ $betrokkene_obj->messages }
                    )
                ),
        );
    }

    my $rules = $c->stash->{zaaktype}->rules({ 
        status => $status
    });

    my $rules_result = $c->stash->{regels_result} = $rules->execute({
        kenmerken               => $kenmerken,
        casetype                => $c->stash->{zaaktype},
        aanvrager               => $betrokkene_obj,
        contactchannel          => $c->session->{_zaak_create}->{contactkanaal},
        payment_status          => undef, # satisfy interface,
    });

    if(exists $rules_result->{price}->{value}) {
        my $price = $rules_result->{price}->{value};
        $c->stash->{online_betaling_kosten} = $price;
        $price =~ s|,|.|gis;
        $c->stash->{rules_payment_amount} = $price;
    }

    $c->session->{_zaak_create}->{streefafhandeldatum_data} = 
        $rules_result->{wijzig_afhandeltermijn};  
          
    $c->session->{regel_sjablonen} = 
        $rules_result->{templates};

    if(my $toewijzing = $rules_result->{toewijzing}) {
        $c->session->{create_case_allocation_rule} = $toewijzing;
    }

    unless ($c->stash->{template}) {
        $c->stash->{template} = 'foutmelding.tt';
    }

    # last step
    my $steps = $c->stash->{kenmerken_groups_keep_sort};

    $c->detach('submit_to_pip') if $c->req->params->{submit_to_pip};


    delete $c->stash->{ _nav_position };
    $c->forward('_generate_navigation');

    if($c->req->is_xhr && $c->req->params->{update_kenmerken}) {
        $c->stash->{template} = "form/form_inner.tt";
    }

    if($c->stash->{ process }{ step } == ($c->stash->{ process }{ total_step_count } - 1)) {
        if(!$c->user_exists && $c->session->{ _zaak_create }{ ztc_aanvrager_type } eq 'unknown' && $c->stash->{ zaaktype }->zaaktype_definitie_id->preset_client) {
            $c->forward('captcha_get');

            # Because setting the language in the recaptcha options variable is merely a 'suggestion' for
            # the google servers, this little trick overrides that completely and forces a locale that's defined
            # by us, instead of Google's interpretation of the locale of the user.
            my $key = $c->config->{ recaptcha }{ pub_key };
            my $lang = $c->config->{ recaptcha }{ options }{ lang } || 'nl';

            $c->stash->{ recaptcha } =~ s/$key/$key&amp;lang=$lang/;
        }
    }

    # Bij de laatste stap zet publish_zaak
    if ($c->req->params->{update_kenmerken} && 
        ($c->req->param('process_step_index')+1) == @$steps && 
        $c->req->param('submit_to_next')) {

        $c->stash->{ publish_zaak } = 1;
    } else {
        $c->detach();
    }
}

sub _process_stap : Private {
    my ($self, $c)          = @_;

    $c->forward('_process_stap_wizard');
    $c->forward('_process_stap_load_values');
    $c->forward('_process_stap_handle_post');


    $c->stash->{form} = $c->session->{_zaak_create}->{form};

    $c->forward('/form/upload/display_uploads');

    $self->_get_default_values($c);

	# allow cheat - in certain situations, required fields may be bypassed. inform
	# browser that the cheat option may be presented.

	my $zaak_create = $c->session->{_zaak_create};
	my $extra_auth_info = {
		aangevraagd_via => $zaak_create->{aangevraagd_via},
	};

	if(
#		$zaak_create->{contactkanaal} eq 'post' &&
		$zaak_create->{milestone} eq '1' &&
		$c->check_any_user_permission('zaak_beheer')
#		&& $self->required_fields_in_fase($c)
	) {
	    $c->stash->{allow_cheat} = 1;
	}
}



sub _get_default_values {
    my ($self, $c) = @_;

    # only once please
    return if $c->session->{_zaak_create}->{default_values_set}++;

    my $zaaktype_id             = $c->session->{_zaak_create}->{zaaktype_id};

    my $zaaktype_kenmerkens;
    if ($c->stash->{zaaktype}) {
        $zaaktype_kenmerkens    = $c->stash->{zaaktype}
                                ->zaaktype_kenmerkens;
    } else {
        my $zaaktype_node_id    = $c->model("DB::Zaaktype")
                                ->find($zaaktype_id)
                                ->zaaktype_node_id->id;

        $zaaktype_kenmerkens  = $c->model('DB::ZaaktypeNode')
                                ->find($zaaktype_node_id)
                                ->zaaktype_kenmerkens;
    }

    ### Given a hash of parameters, mangle_defaults will combine the defaults
    ### given in our library with our parameters. Since we have none yet, we
    ### just get all the default parameters by giving an empty hash
    ### (perldoc Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken)
    $c->session->{_zaak_create}->{form}->{kenmerken} =
        $zaaktype_kenmerkens->mangle_defaults(
            (
                defined($c->session->{_zaak_create}->{form}->{kenmerken}) &&
                $c->session->{_zaak_create}->{form}->{kenmerken}
            ) ? $c->session->{_zaak_create}->{form}->{kenmerken} : {}
        );
}

sub _generate_navigation : Private {
    my ($self, $c) = @_;

    my @steps = $c->stash->{ zaaktype }->get_steps(1);
    my $cur_step = $c->req->param('process_step_index') // 0;

    if($c->req->param('afronden') && $c->session->{_zaak_create}->{afronden_goto_step}) {
        $cur_step = $c->session->{_zaak_create}->{afronden_goto_step};
    }

    my $origin = $c->session->{ _zaak_create }{ aangevraagd_via };
    my $iter_adjust = 0;

    if($c->forward('_should_zaakcontrole')) {
        unshift @steps, {
            label => 'Zaakcontrole',
            disabled => 1
        };

        $iter_adjust++;
    }

    if($origin ne 'balie' && $c->session->{ _zaak_create }{ ztc_aanvrager_type } ne 'unknown') {
        my $url = $c->uri_for(
            '/zaak/create/webformulier/aanvrager',
            { skip_ahead => $c->stash->{ _nav_position} ne 'zaakcontrole' }
        );

        unshift @steps, {
            label => 'Contactgegevens',
            url => $url
        };

        $c->stash->{ navigation_links_start } = $url;

        $iter_adjust++;
    }

    if(exists $c->stash->{ _nav_position }) {
        if($c->stash->{ _nav_position } eq 'aanvrager') {
            $cur_step = 0;
        } elsif ($c->stash->{ _nav_position } eq 'zaakcontrole') {
            $cur_step = $iter_adjust - 1;
        }
    } else {
        $cur_step += $iter_adjust;
    }

    if($c->req->param('submit_to_next')) {
        $cur_step++;
    }

    if($c->req->param('submit_to_previous')) {
        $cur_step--;
    }

    if($origin eq 'webformulier') {
        push(@steps, { label => 'Controleren en verzenden' });
    } elsif($c->stash->{ zaaktype }->contact_info_intake && $c->session->{ _zaak_create }{ aanvraag_trigger } ne 'intern') {
        push(@steps, { label => 'Contactgegevens' });
    }

    if($origin eq 'webformulier' && $c->stash->{ zaaktype }->online_betaling) {
        push(@steps, { label => 'Online betaling' });
    }

    my $iter = 0;
    for my $step (@steps) {
        $step->{ classes } = join(' ', (
            'stap',
            $iter < 1 ? 'first' : (),
            $iter < $cur_step ? 'done' : (),
            $iter == $cur_step ? 'active' : (),
            $iter == ($cur_step + 1) ? 'next-to-active': ()
        ));

        if($iter < $cur_step && !$step->{ disabled } && !$step->{ url }) {
            $step->{ url } = $c->req->uri_with({
                ztc_aanvrager_type => $c->session->{ _zaak_create }{ ztc_aanvrager_type },
                process_step_index => $iter - $iter_adjust,
                submit_to_next => undef,
                submit_to_previous => undef
            });
        }

        $iter++;
    }
    $c->stash->{ navigation_links } = scalar(@steps) ? \@steps : undef;
}

sub _process_stap_handle_post : Private {
    my ($self, $c)          = @_;

    return 1 unless $c->req->params->{update_kenmerken};

    ### Validation
    my $registratie_fase;
    if ($c->stash->{zaak_status}) {
        $registratie_fase    = $c->stash->{zaak_status};
    } else {
        $registratie_fase    = $c->stash->{zaaktype}
            ->zaaktype_statussen
            ->search({status => 1 });

        die('WUT? Geen registratiefase?') unless (
            $registratie_fase = $registratie_fase->first
        );
    }

    my $params = $c->req->params();

# put files in the params just before validating, otherwise the validator doesn't know 
# files have been uploaded.
    my $session_uploads = $c->session->{_zaak_create}->{uploads} || {};
    
    foreach my $upload_kenmerk_id (keys %$session_uploads) {
        my $kenmerk = 'kenmerk_id_' . $upload_kenmerk_id;
        ### When use hits 'create zaak later', documents cannot be saved
        if (
            defined $session_uploads->{$upload_kenmerk_id}
        ) {
            for my $file (@{$session_uploads->{$upload_kenmerk_id}}) {
                next if !$file->{upload};

                unless($params->{ $kenmerk }) {
                    $params->{ $kenmerk } = [];
                }

                unless($params->{ $kenmerk } eq 'ARRAY') {
                    $params->{ $kenmerk } = [];
                }

                push @{$params->{ $kenmerk }}, $file->{upload}->filename;
            }
            
        } else {
            delete($params->{$kenmerk});
        }
    }
    

# hack - to make checkboxes and options defined, to enable search for required fields
    my @defined_kenmerken = ($c->req->param('defined_kenmerk'));
    foreach my $defined_kenmerk (@defined_kenmerken) {
        $params->{$defined_kenmerk} ||= '';
    }

    {
        my $dv = $registratie_fase->validate_kenmerken(
            $params,
            {
                ignore_undefined => 1,
                with_prefix      => 1,
            }
        );

        my $process = $c->stash->{ process };
        my $ztc_aanvrager_type = $c->session->{ _zaak_create }{ ztc_aanvrager_type } // $params->{ztc_aanvrager_type} || '';

        if(
            $registratie_fase->zaaktype_node_id->zaaktype_definitie_id->preset_client &&
            $c->stash->{logged_in_by} && 
            $c->stash->{logged_in_by} eq 'preset_client' &&
            $process->{step} == $process->{total_step_count} &&
            !$registratie_fase->zaaktype_node_id->properties->{no_captcha} &&
            !$c->user_exists &&
            (
                !defined($c->session->{_zaak_create}->{_captcha_validated}) ||
                !$c->session->{_zaak_create}->{_captcha_validated}
            )
        ) {
            my $captcha_result = $c->forward('captcha_check');

            $c->log->debug(
                'Captcha check result: '
                . (
                    $captcha_result
                        ? 'VALID'
                        : 'INVALID'
                    )
            );

            ### Save the result if captcha is already valid

            if ($captcha_result) {
                $c->session->{_zaak_create}->{_captcha_validated} = 1;
            } else {
                $dv->{ invalid }{ recaptchatable } = [ $c->req->param('recaptcha_response_field') ];
            }

        }

        if ($c->req->is_xhr && $c->req->params->{do_validation}) {
            $c->zvalidate($dv);
            $c->detach;
        }
    }

    $self->uploadfile($c);

    my $session_kenmerken = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

    my %req_kenmerken   = map {
            my $key = $_;
            $key    =~ s/kenmerk_id_//g;
            $key    => $c->req->params->{ $_ }
        } grep(/^kenmerk_id_(\d+)$/, keys %{ $c->req->params });

    for my $kenmerk (keys %req_kenmerken) {
        
        if (UNIVERSAL::isa($req_kenmerken{$kenmerk}, 'ARRAY')) {
            $session_kenmerken->{ $kenmerk } = [];

            foreach my $value (@{ $req_kenmerken{$kenmerk} }) {
                push @{$session_kenmerken->{ $kenmerk }}, $self->_make_value_secure($value);
            }
        } else {
            $session_kenmerken->{ $kenmerk } = $self->_make_value_secure($req_kenmerken{$kenmerk});
        }
    }

    # remove kenmerken that are in the current step but not in cgi params 
    # - to get rid of the last checkbox
    # first find out which step the post is about - which data are we replacing here
    my $submitted_step_index = $c->req->param('process_step_index');
    my $steps = $c->stash->{kenmerken_groups_keep_sort};
    my $submitted_step = $steps->[$submitted_step_index];

    # then get a list of kenmerken for this submitted step. for each of them, if there's
    # no information for one of them, delete it.
    my $current_stap_kenmerken = $c->stash->{kenmerken_groups}->{$submitted_step};
    foreach my $current_stap_kenmerk (@$current_stap_kenmerken) {
        my $kenmerk_id = $current_stap_kenmerk->bibliotheek_kenmerken_id->id;
        unless(exists $req_kenmerken{$kenmerk_id}) {
            delete $session_kenmerken->{$kenmerk_id};
        }        
    }
}

sub _make_value_secure {
    my ($self, $value) = @_;


    my $tf  = HTML::TagFilter->new(allow => {
        p   => { all => [] },
        h1  => { all => [] },
        h2  => { all => [] },
        h3  => { all => [] },
        strong => { all => [] },
        em   => { all => [] },
        ul  => { all => [] },
        li  => { all => [] },
        ol  => { all => [] },
        a   => { 'href' => ['any'], 'title'=> ['any'] },
        img => { 'src' => ['any'],  'alt'  => ['any'],  'title' => ['any'],},
    });

    ### HTML::TagFilter has the annoying problem that it returns en empty
    ### string when given a 0. So we make sure we do not run tagfilter when
    ### string is empty or contains a 0 (when it tests 'false').
    ### Also, we recode entities to Unicode
    unless (defined($value) && !$value) {
        $value = $tf->filter($value);
        decode_entities($value);
    }

    return $value;
}

sub uploadfile {
    my ($self, $c) = @_;

    my $uploaded_files = {};
    foreach my $upload_param (keys %{$c->req->uploads}) {
        my $upload = $c->req->upload($upload_param);

        my $options = {
          'filename' => $upload->filename,
          'id'       => '0',
          'naam'     => $upload->filename,
        };

        my $file_id = $c->req->param('file_id');

        # For oldschool uploading (IE)        
        unless($file_id) {
            ($file_id) = $upload->headers()->header('content-disposition') =~ m|name="(.*?)"|;
        }

        die "need file id" unless($file_id);

        my ($kenmerk) = $file_id =~ m|(\d+)$|;

        my $params = {
            uploads => {
                $kenmerk => {'upload' => $upload}
            }
        };

        my $filestore = $c->model('DB::Filestore')->filestore_create({
            original_name => $upload->filename,
            file_path     => $upload->tempname,
        });
        
        push @{$c->session->{_zaak_create}->{uploads}->{$kenmerk}}, {
            upload       => $c->req->upload($upload_param),
            filestore_id => $filestore->id,
        };
        $uploaded_files->{$filestore->id} = $upload;
    }

    return $uploaded_files; #obsolete
}

sub _process_stap_load_values : Private {
    my ($self, $c)          = @_;


    return 1 if (
        $c->session->{_zaak_create}->{form} &&
        scalar(keys %{ $c->session->{_zaak_create}->{form} })
    );

    $c->session->{_zaak_create}->{form} = {
        kenmerken   => {}
    };

}

#
# determine the screenflow of the webform.
# the webform can be submitted through a submit button - in which case there's a variable
# present in $c->req->params(), or through AJAX. default behaviour is to stay on the same 
# step
#
# input:
# - current step index (process_step_index)
# - CGI param submit to next
# - CGI param submit to prev
#
# output:
# - new current step
#
sub _process_stap_wizard : Private {
    my ($self, $c)          = @_;
    $c->stash->{process}    = {};

    if ($c->user_exists && $c->stash->{aanvraag_trigger} eq 'extern' && $c->stash->{ zaaktype_node }->contact_info_intake) {
        push(@{ $c->stash->{kenmerken_groups_keep_sort} }, 'contactgegevens');
    }

    if($c->session->{ _zaak_create }{ aangevraagd_via } eq 'webformulier') {
        push(@{ $c->stash->{ kenmerken_groups_keep_sort } }, 'verify');
    }

    my $steps = $c->stash->{kenmerken_groups_keep_sort};
    my $process_step_index = $c->req->param('process_step_index') || 0;

    my $s = scalar @$steps;
    if (!$s) {
        throw(
            '/Controller/Form/_process_stap_wizard',
            'Deze zaak heeft geen kenmerken!'
        );
    }

    do {
        throw(
            '/Controller/Form/_process_stap_wizard',
            sprintf(
                'Illegal step index (tried step %d of %d)', $process_step_index,
                $s
            ));

    } unless ($process_step_index < $s && $process_step_index >= 0);
    
    if($c->req->param('submit_to_previous')) {
        $process_step_index--;
    } elsif($c->req->param('submit_to_next')) {
        $process_step_index++;
    }

    if ($c->stash->{afronden_goto_step}) {
        $process_step_index = $c->stash->{afronden_goto_step};
        $c->stash->{afronden_goto_step} = undef;
    }

    $c->stash->{ process_step_index } = $process_step_index;

    my $process = {
        current_stap => $steps->[$process_step_index],        
        previous_stap => $process_step_index - 1,
        step         => $process_step_index,
    };

    if(scalar @$steps > $process_step_index + 1) {
        $process->{'next_stap'} = $process_step_index + 1;
    }

    $process->{ total_step_count } = scalar @$steps;
    $c->stash->{process} = $process; 
}



sub finish : Private {
    my ($self, $c) = @_;

    ### Delete form
    delete($c->session->{form});

    ### Set finish template
    $c->stash->{template} = 'form/finish.tt';
}

sub _preprocess_webform : Private {
    my ($self, $c) = @_;

    ### Form fields
    {
        $c->stash->{kenmerken_groups}           = {};
        $c->stash->{kenmerken_groups_keep_sort} = [];
        $c->stash->{kenmerken_groups_only}      = {};
        my $fields                              = $c->stash->{fields};

        my $current_group;
        $fields->reset;
        while (my $kenmerk = $fields->next) {
            if ($kenmerk->is_group) {
                $current_group = $kenmerk->label;
                push(
                    @{ $c->stash->{kenmerken_groups_keep_sort} },
                    $kenmerk->label
                );

                $c->stash->{kenmerken_groups_only}->{$kenmerk->label} = $kenmerk;
                next;
            } else {
                ### Geen group, show default
                if (!scalar(@{ $c->stash->{kenmerken_groups_keep_sort} })) {
                    $current_group = 'Benodigde gegevens';
                    $c->stash->{kenmerken_groups_keep_sort}->[0]
                        = 'Benodigde gegevens';
                    $c->stash->{kenmerken_groups_only}->{'Benodigde gegevens'}
                        = {
                            label   => 'Benodigde gegevens',
                            help    => undef,
                        };
                }
            }

            $c->stash->{kenmerken_groups}->{$current_group} ||= [];

            push(
                @{ $c->stash->{kenmerken_groups}->{$current_group} },
                $kenmerk
            );
        }
    }

    return 'form/intake.tt';
}




sub register_relaties_in_session_suggestion : Chained('/') : PathPart('form/register_relaties/suggestion'): Args(0) {
    my ($self, $c) = @_;

    my @columns;
    my $suggestion = BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(
        \@columns,
        $c->req->params->{magic_string_prefix},
        $c->req->params->{rol}
    );

    unless ($suggestion) {
        $c->res->body('NOK');
        return;
    }

    $c->res->body($suggestion);
}

Params::Profile->register_profile(
    'method'    => 'register_relaties_in_session',
    'profile'   => BETROKKENE_RELATEREN_PROFILE,
);

sub register_relaties_in_session : Chained('/') : PathPart('form/register_relaties'): Args() {
    my ($self, $c, $action)          = @_;


    my $params = $c->req->params();

    $c->stash->{nowrapper}  = 1;

    if ($c->req->is_xhr) {
        if ($params->{do_validation}) {
            $c->zvalidate;
        }
    }

    if ($action && $action eq 'add') {
        $c->stash->{template}   = 'widgets/betrokkene/create_relatie.tt';
        $c->detach;
    }

    my $relaties = $c->session->{_zaak_create}->{betrokkene_relaties} ||= [];

    if ($params->{action} && $params->{action} eq 'remove') {

        my $remove_id = $params->{remove_id};
        my @new = ();
        foreach my $relatie (@$relaties) {
            unless($relatie->{id} eq $remove_id) {
                push @new, $relatie;
            }
        }
        $relaties = \@new;
    }

    $c->stash->{table_config} = RELATIE_TABLE_CONFIG;
    $c->stash->{template}       = 'widgets/general/simple_table.tt';

    if (uc($c->req->method) eq 'POST') {

        my $relatie_profile     = BETROKKENE_RELATEREN_PROFILE;

        my $relatie = {};
        $relatie->{ $_ } = $params->{ $_ } for (
            qw/
                type
                betrokkene_naam
            /,
            @{ $relatie_profile->{required} },
            @{ $relatie_profile->{optional} }
        );

        $relatie->{id} = $self->_get_new_insert_id($relaties);
        
        push @$relaties, $relatie;
    };

    $c->stash->{table_config}->{rows} = $c->session->{_zaak_create}->{betrokkene_relaties} = $relaties;
}


sub _get_new_insert_id {
    my ($self, $object_list) = @_;
    
    my $max_id = 1;
    
    foreach my $object (@$object_list) {
        if($object->{id} && $object->{id} > $max_id) {
            $max_id = $object->{id};
        }
    }
    
    return $max_id + 1;
}

=head2 _search_stuf_allowed

Simple helper to check whether or not broker searching is enabled or not.

=cut

sub _search_stuf_allowed {
    my ($c) = shift;

    my $stufconfig = $c->model('DB::Interface')
        ->find_by_module_name('stufconfig');

    if ($stufconfig && $stufconfig->active) {
        my $stuf_params = $stufconfig->get_interface_config;

        if ($stuf_params && $stuf_params->{bidirectional} && $stuf_params->{makelaar_search}) {
            return 1;
        }
    }
}

=head2 stuf_integrale_zoekvraag_import

Voert een integrale zoekvraag uit om een gebruiker uit een makelaar te importeren. Redirect vervolgens naar
een ingevuld formulier.

=cut

sub stuf_integrale_zoekvraag_import : Chained('/') : PathPart('form/integrale_zoekvraag_import') {
    my ($self, $c) = @_;

    if (!$c->req->params->{url}) {
        die 'Need URL to redirect to after completion';
    }

    my $url = sprintf "%s&ran_stuf=1", $c->req->params->{url};

    $c->stash->{aanvrager_bsn} = $c->session->{ _saml }{ uid };

    my $result = $c->forward('_load_external_requestor');

    $c->stash->{json} = $c->view('JSON')->prepare_json_row($url);
    $c->forward('View::JSON');
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


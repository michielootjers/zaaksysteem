package Zaaksysteem::Controller::ObjectSearch;

use Moose;

BEGIN { extends 'Catalyst::Controller'; }

use Data::Dumper;

use HTML::TreeBuilder;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use Zaaksysteem::Constants qw/
    OBJECTSEARCH_TABLENAMES
    OBJECTSEARCH_TABLE_ORDER
    ZAAKSYSTEEM_OPTIONS
    STATUS_LABELS
/;

use Zaaksysteem::Attributes qw[ZAAKSYSTEEM_SYSTEM_ATTRIBUTES];

=head1 NAME

Zaaksysteem::Controller::ObjectSearch

=head1 SUMMARY

This controller defines several actions that can be used to search for specific
objects within Zaaksysteem. What you should imagine is meant by that is things
like employees, addresses, contacts etc.

These actions are mainly used by the autocomplete fields you find throughout
the Zaaksysteem frontend (Spot-enlighter anyone?)

=head1 ACTIONS

=cut

define_profile objectsearch_base => (
    required => [],
    require_some => {
        query_or_all => [ 1, qw[query all] ],
    },
    typed => {
        query => 'Str'
    }
);

=head2 objectsearch_base

This action implements a base for all objectsearch actions. It does nought but
verify a C<query> parameter is present/valid and defines the main URI path
for this domain (C<objectsearch>).

=head3 Parameters

=over 4

=item query

This is the main interface to ObjectSearch queries, every action based on this
expects a string query to be present. It is stored in the C<< $c->stash >> hash.

=item all

This parameter overrides the default check that query is filled with data,
allowing subcontrollers to return all items in a collection as a search result.

=back

=cut

sub objectsearch_base : Chained('/') : PathPart('objectsearch') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # return unless($c->req->is_xhr);

    my $params = assert_profile($c->req->params)->valid;

    $c->stash->{ query } = $params->{ query };
    $c->stash->{ show_all } = exists $params->{ all };
}

=head2 objectsearch_contact_base

This actions implements a base for all contact-related object search actions.
It merely defines the URI path to be that of the C<objectsearch_base> base
action with C<contact> slapped after it.

=cut

sub objectsearch_contact_base : Chained('objectsearch_base') : PathPart('contact') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

=head2 objectsearch

=head3 URL

C</objectsearch>

=head3 Parameters

=over 4

=item object_type

=item rows

=item trigger

=item betrokkene_type

=back

=cut

define_profile objectsearch => (
    optional => [qw[object_type rows trigger betrokkene_type active]],
    typed => {
        object_type => 'Str',
        rows => 'Int',
        trigger => 'Str',
        betrokkene_type => 'Str',
        active => 'Bool'
    }
);

sub objectsearch : Chained('objectsearch_base') : PathPart('') : Args(0) {
	my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $object_type = $opts->{ object_type };

    my $results;

    if($object_type) {
        my $extra_params = {};

        if($object_type eq 'zaaktype') {
            $extra_params->{ trigger } = $opts->{ trigger };

            if($opts->{ trigger } eq 'intern') {
                $extra_params->{ betrokkene_type } = 'medewerker';
            } else {
                if($opts->{ betrokkene_type } ne 'natuurlijk_persoon') {
                    $extra_params->{ betrokkene_type } = [
                        qw[niet_natuurlijk_persoon niet_natuurlijk_persoon_na]
                    ];
                } else {
                    $extra_params->{ betrokkene_type } = 'natuurlijk_persoon';
                }
            }
        }

        if($opts->{ rows }) {
            $extra_params->{ rows } = $opts->{ rows };
        }

        if ($opts->{active}) {
            $extra_params->{active} = $opts->{active};
        }

        $results = $self->_search_searchable_object_type({
            c            => $c,
            query        => $c->stash->{ query },
            object_type  => $object_type,
            extra_params => $extra_params,
        });
    } else {
        $results = $self->_search_searchable({
            c       => $c,
            query   => $c->stash->{ query },
        });
    }

    my @entries;

    if($results) {
        my $object_types_seen = {};

        foreach my $result (@$results) {
            next unless $result->{ searchable_id };

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => $result->{ object_type },
                searchable_id   => $result->{ searchable_id }
            });

            next unless $object;

            push @entries, {
                id          => $result->{ searchable_id },
                label       => $result->{ search_term },
                object_type => $result->{ object_type },
                object      => $object,
            };
        }
    }

    $c->stash->{json} = { entries => \@entries };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 address

=head3 URL

C</objectsearch/address>

=cut

sub address : Chained('objectsearch_base') : PathPart('address') {
	my ($self, $c) = @_;

    my $query = $c->stash->{ query };

    my $entries;

    if($query =~ m|^\w+.*\d+\,|) {
        $entries = $self->_search_huisnummer({ c => $c, query => $query });
    } else {
        $entries = $self->_search_openbareruimte({ c => $c, query => $query });
    }

    $c->stash->{json} = { entries => $entries };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 case_results

=head3 URL

C</objectsearch/case/results>

=cut

sub case_results : Chained('objectsearch_base') : PathPart('case/results') {
    my ($self, $c) = @_;

    my $types = ZAAKSYSTEEM_OPTIONS->{ RESULTAATTYPEN };

    my @entries;
    my $query = $c->stash->{ query };

    for my $type (grep { $_ =~ m[$query]i } @{ $types }) {
        warn $type;

        push @entries, {
            id => $type,
            label => ucfirst($type),
            object_type => 'case-result',
            object => {}
        };
    }

    $c->stash->{ json } = { entries => \@entries };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 attributes

This action implements an autocomplete backend for system/db/whatever
attributes. At this point it will freeform query the
L<Zaaksysteem::Schema::BibliotheekKenmerken> table, followed by an
iteration over all attributes defined in L<Zaaksysteem::Attributes>.

=head3 URL

C</objectsearch/attributes>

=head3 Output

The returned objects look something like this:

    {
        "id" : 463,
        "label" : "Groot tekstveld voorinvulling test",
        "object_type" : "attribute",
        "object" : {
            "value_type" : "textarea",
            "source" : "db-attr",
            "column_name" : "groot_tekstveld_voorinvulling_test"
        }
    },
    .
    .
    .

=cut

sub attributes : Chained('objectsearch_base') : PathPart('attributes') {
    my ($self, $c) = @_;

    my @entries = $c->model('Attributes')->search($c->stash->{ query });

    $c->stash->{ json } = {
        entries => [ sort { uc($a->{ label }) cmp uc($b->{ label }) } @entries ]
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bag

=head3 URL

C</objectsearch/bag>

=cut

sub bag_search : Chained('objectsearch_base') : PathPart('bag') {
    my ($self, $c) = @_;

    my $query = $c->stash->{ query };

    my ($postal_code, $rest) = $query =~ m[(\d{4}\s?[a-zA-Z]{2})\s?(.*)];

    if($postal_code) {
        $c->detach('_bag_postal', [ $postal_code, $rest ]);
    }

    my ($number, $letter, $suffix) = _parse_street_number($query);
    my $city = '';

    my @street_parts = (substr $query, 0, index($query, $number // length($query)) - 1);

    unless($number) {
        @street_parts = grep { length >= 3 } split m[\s+], $query;

        if(scalar @street_parts > 1) {
            $city = pop @street_parts;

            unless($c->model('DB::BagWoonplaats')->search({ naam => { ilike => $city } })->count) {
                unshift @street_parts, $city;
                $city = undef;
            }
        }
    }

    unless(scalar(@street_parts)) {
        $c->stash->{ json } = { entries => [] };
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    my $entries = $c->model('DB::BagNummeraanduiding')->search(
        { -and => [ map { { 'openbareruimte.naam' => { ilike => sprintf('%%%s%%', $_) } } } @street_parts ] },
        {
            order_by => [
                qw[woonplaats.naam openbareruimte.naam huisnummer],
                \'huisletter NULLS FIRST',
                \'huisnummertoevoeging NULLS FIRST',
            ],
            join => { openbareruimte => 'woonplaats' },
            rows => 20
        }
    );

    if($city) {
        $entries = $entries->search({ 'woonplaats.naam' => $city });
    }

    if($number) {
        $entries = $entries->search({ 'me.huisnummer' => $number });

        if($letter) { $entries = $entries->search({ 'me.huisletter' => uc($letter) }); }
        if($suffix) { $entries = $entries->search({ 'me.huisnummertoevoeging' => $suffix }); }
    }

    $c->stash->{ json } = { entries => $self->_build_entry_set($entries) };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bag_street

=head3 URL

C</objectsearch/bag-street>

=cut

sub bag_search_street : Chained('objectsearch_base') : PathPart('bag-street') {
    my ($self, $c) = @_;

    my $query = $c->stash->{ query };

    my ($postal_code, $rest) = $query =~ m[(\d{4}\s?[a-zA-Z]{2})\s?(.*)];

    if($postal_code) {
        $c->detach('_bag_postal', [ $postal_code ]);
    }

    my @query_parts = grep { length $_ >= 3 } split m[\s], $query;
    my ($city, $street);

    if(scalar(@query_parts) > 1) {
        $city = pop @query_parts;
    }

    unless(scalar(@query_parts)) {
        $c->stash->{ json } = { entries => [] };

        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    my $entries = $c->model('DB::BagOpenbareruimte')->search(
        { -and => [ map { { 'me.naam' => { ilike => sprintf('%%%s%%', $_) } } } @query_parts ] },
        { join => [ 'woonplaats' ], rows => 20, order_by => [qw[woonplaats.naam me.naam]] }
    );

    if($city) {
        $entries = $entries->search({ 'woonplaats.naam' => { ilike => sprintf('%%%s%%', $city) } });
    }

    $c->stash->{ json } = { entries => $self->_build_entry_set($entries) };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 casetype_document

=head3 URL

C</objectsearch/case_type_document>

=cut

sub casetype_document : Chained('objectsearch_base') : PathPart('case_type_document') {
    my ($self, $c) = @_;

    my $entries = $self->_search_casetype_document({
        c       => $c,
        query   => $c->stash->{ query }
    });

    $c->stash->{json} = {
        entries => $entries,
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 natuurlijk_persoon

=head3 URL

C</objectsearch/contact/natuurlijk_persoon>

=cut

sub natuurlijk_persoon : Chained('objectsearch_contact_base') : PathPart('natuurlijk_persoon') {
	my ($self, $c) = @_;

    $c->stash->{ json } = {
        entries => $self->_search_natuurlijk_persoon({
            c => $c,
            query => $c->stash->{ query }
        })
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bedrijf

=head3 URL

C</objectsearch/contact/bedrijf>

=cut

sub bedrijf : Chained('objectsearch_contact_base') : PathPart('bedrijf') {
	my ($self, $c) = @_;

    my $entries = $self->_search_bedrijf({
        c       => $c,
        query   => $c->stash->{ query }
    });

    $c->stash->{json} = {
        entries	=> $entries,
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 medewerker

=head3 URL

C</objectsearch/contact/medewerker>

=cut

sub medewerker : Chained('objectsearch_contact_base') : PathPart('medewerker') {
	my ($self, $c) = @_;

    my $search_args = {
        intern => 0,
        type => 'medewerker',
        rows_per_page => '',
    };

    my $entries = $c->model('Betrokkene')->search($search_args, {
        freetext => $c->stash->{ query }
    });

    my @hashresults;

    if($entries) {
        while(my $entry = $entries->next()) {
            push @hashresults, {
                id          => $entry->ex_id,
                object_type => 'medewerker',
                label       => $entry->naam,
                object      => {
                    id          => $entry->ex_id,
                    object_type => 'medewerker',
                    naam        => $entry->naam,
                }
            };
        }
    }

    $c->stash->{json} = { entries => \@hashresults };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head1 PRIVATE ACTIONS

=head2 _bag_postal

=cut

sub _bag_postal : Private {
    my ($self, $c, $postal_code, $rest) = @_;

    $postal_code =~ s/\s//;
    $postal_code = uc($postal_code);

    my $entries;

    if(defined $rest) {
        $entries = $c->model('DB::BagNummeraanduiding')->search(
            { 'me.postcode' => $postal_code },
            {
                order_by => [
                    qw[woonplaats.naam openbareruimte.naam postcode huisnummer],
                    \'huisletter NULLS FIRST',
                    \'huisnummertoevoeging NULLS FIRST'
                ],
                join => { openbareruimte => 'woonplaats' },
                rows => 20
            }
        );

        if($rest =~ m[^\d]) {
            my ($number, $letter, $suffix) = _parse_street_number($rest);

            $c->log->debug('BAG Postal parsed street number',
                sprintf('No.: %d', $number),
                sprintf('Letter: %s', $letter),
                sprintf('Suffix: %s', $suffix)
            );

            if($number) { $entries = $entries->search({ 'me.huisnummer' => $number }); }
            if($letter) { $entries = $entries->search({ 'me.huisletter' => $letter }); }
            if($suffix) { $entries = $entries->search({ 'me.huisnummertoevoeging' => $suffix }); }
        }
    } else {
        $entries = $c->model('DB::BagOpenbareruimte')->search(
            { 'hoofdadressen.postcode' => $postal_code },
            {
                join => [qw[hoofdadressen woonplaats]],
                order_by => [
                    qw[woonplaats.naam me.naam hoofdadressen.postcode hoofdadressen.huisnummer],
                    \'hoofdadressen.huisletter NULLS FIRST',
                    \'hoofdadressen.huisnummertoevoeging NULLS FIRST'
                ],
                group_by => [qw[me.identificatie me.begindatum me.einddatum me.naam me.officieel me.woonplaats me.type me.inonderzoek me.documentdatum me.documentnummer me.status me.correctie]],
                rows => 20
            }
        );
    }

    $c->stash->{ json } = { entries => $self->_build_entry_set($entries) };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head1 METHODS

=head2 _search_natuurlijk_persoon

=cut

define_profile _search_natuurlijk_persoon => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_natuurlijk_persoon {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

	my @entries;

    my $resultset;

	if(length $query > 1 && length $query <= 3) {
		$resultset = $c->model("DB::NatuurlijkPersoon")->search(
			{
				'lower(geslachtsnaam)' => {
					like => lc($query) . '%',
				}
			},
			{
				rows => 5,
				page => 1,
    			order_by => 'me.search_term',
				prefetch => 'adres_id',
			}
		);

	} elsif(length $query > 3) {

        my $query_parts = _parse_query({
            query => $query
        });

        my @whereClause;

        foreach my $query_part (@$query_parts) {
            push @whereClause, { 'lower(search_term)' => {
                    like => sprintf('%%%s%%', lc($query_part))
            } };
        }

        $resultset = $c->model("DB::NatuurlijkPersoon")->search({ -and => \@whereClause }, {
            rows => 5,
            page => 1,
            order_by => 'me.search_term',
            prefetch => 'adres_id',
        });
    }

	if($resultset) {
	    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

		while (my $result = $resultset->next) {

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'natuurlijk_persoon',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push @entries, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => 'natuurlijk_persoon',
                object      => $object,
            };
		}
	}

	return \@entries;
}

=head2 _search_casetype_document

=cut

define_profile _search_casetype_document => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_casetype_document {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

    my @entries;

    my $resultset;

    if(length $query >= 1) {
        $resultset = $c->model('DB::BibliotheekKenmerken')->search({
            value_type => 'file',
            naam       => { '~*' => $query },
        });
    }

    if($resultset) {
        $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

        while (my $result = $resultset->next) {

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'bibliotheek_kenmerken',
                searchable_id   => $result->{ searchable_id }
            });

            next unless $object;

            push @entries, {
                id          => $result->{ searchable_id },
                label       => $result->{ search_term },
                object_type => 'bibliotheek_kenmerken',
                object      => $object,
            };
        }
    }

    return \@entries;
}

=head2 _search_searchable_object_type

=cut

define_profile _search_searchable_object_type => (
    required => [qw[c query object_type]],
    optional => [qw[extra_params]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str',
        object_type => 'Str',
        extra_params => 'HashRef'
    }
);

sub _search_searchable_object_type {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };
    my $object_type = $opts->{ object_type };
    my $extra_params = $opts->{ extra_params };

    return undef if(length $query < 1);

    my @results = $self->_build_resultset({
        object_type     => $object_type,
        query           => $query,
        c               => $c,
        extra_params    => $extra_params,
        max_items       => 50,
    });

    return \@results;
}

=head2 _search_searchable

=cut

define_profile _search_searchable => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_searchable {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

    return undef if(length $query < 1);

    my $whereClause = $self->_search_parts({
        query   => $query,
    });

    my $resultset_distinct = $c->model("DB::Searchable")->search($whereClause, {
        select => ['object_type'],
        distinct => 1,
    });

    $resultset_distinct->result_class('DBIx::Class::ResultClass::HashRefInflator');

    # we want the output ordered. order comes from the constant OBJECTSEARCH_TABLE_ORDER
    # a little perl poetry to accomplish this.
    my $grouped_results = [];

    foreach my $object_type (OBJECTSEARCH_TABLE_ORDER) {

        my @results = $self->_build_resultset({
            object_type => $object_type,
            query => $query,
            c => $c,
        });

        push @$grouped_results, @results;
    }

    return $grouped_results;
}

=head2 _search_contacts

=cut

define_profile _search_contacts => (
    required => [qw[whereClause c limit]],
    typed => {
        c => 'Zaaksysteem',
        limit => 'Int'
    }
);

sub _search_contacts {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $whereClause = $opts->{ whereClause };
    my $c = $opts->{ c };
    my $limit = $opts->{ limit };

    my $resultset = $c->model('DB::Searchable')->search({
        -and        =>	$whereClause,
        object_type => { -in => [ qw/natuurlijk_persoon bedrijf/] },
    },
    {
        rows        => $limit,
        order_by    => 'me.search_term',
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

    return $resultset->all;
}

=head2 _search_parts

=cut

define_profile _search_parts => (
    required => [qw[query]],
    typed => {
        query => 'Str'
    }
);

sub _search_parts {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $query = $opts->{ query };

    my $query_parts = _parse_query({
        query => $query
    });

    my @whereClause;

    foreach my $query_part (@$query_parts) {
        push @whereClause, { 'lower(me.search_term)' => {
            like => sprintf('%%%s%%', lc($query_part))
        } };
    }

    return { -and => \@whereClause };
}

=head2 _search_bedrijf

=cut

define_profile _search_bedrijf => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_bedrijf {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

	my @entries;

	my $resultset;

	if(length $query > 1 && length $query <= 3) {
		$resultset = $c->model('DB::Bedrijf')->search(
			{
				'lower(handelsnaam)' => {
					like => lc($query) . '%',
				}
			},
			{
				rows => 5,
				page => 1,
			}
		);

	} elsif(length $query > 3) {
		my $query_parts = _parse_query({
		    query => $query
		});

        my @whereClause;

		foreach my $query_part (@$query_parts) {
			push @whereClause, {
				'lower(search_term)' => {
					like => sprintf('%%%s%%', lc($query_part))
				},
			};
		}

		$resultset = $c->model('DB::Bedrijf')->search({ -and => \@whereClause }, {
			rows => 5,
			order_by => 'me.handelsnaam',
			page => 1,
		});
	}

	if($resultset) {
	    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

		while (my $result = $resultset->next) {
            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'bedrijf',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push @entries, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => 'bedrijf',
                object      => $object,
            };
		}
	}

	return \@entries;
}

=head2 _search_openbareruimte

=cut

define_profile _search_openbareruimte => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_openbareruimte : Private {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

	return [] unless $query;

	$query =~ s/.+> //;
	$query =~ s|\,.*||;

	my $resultset = $c->model('DB::BagOpenbareruimte')->search({
        'lower(naam)' => {
            like    => lc($query) . '%'
        }
    });

    my @entries;

	while (my $result = $resultset->next) {
		push @entries, {
            id 		=> $result->id,
            label 	=> $result->naam,
            type 	=> 'straatnaam',
            woonplaats => $result->woonplaats->naam,
		};
	}

	return \@entries;
}

=head2 _search_huisnummer

=cut

define_profile _search_huisnummer => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_huisnummer : Private {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

	$query =~ s|\,.*||is;

	my ($straatnaam, $huisnummer) = $query =~ m|^(.*)\s+(\d+)|;

	$straatnaam =~ s/.+> //;

	my $straten = $c->model('DB::BagOpenbareruimte')->search({
        'lower(naam)' => {
            like => sprintf('%s%%', lc($straatnaam))
        }
    });

    my @entries;

	if($straten->count) {
		my $straat  = $straten->first;

		my $nas     = $straat->hoofdadressen->search({
            'CAST(me.huisnummer as text)' => {
                like => sprintf('%s%%', $huisnummer)
            }
        }, { order_by => 'huisnummer' });

		while (my $na = $nas->next) {
			push @entries, {
                label 		=> sprintf('%s %s', $straat->naam, $na->nummeraanduiding),
                woonplaats 	=> $straat->woonplaats->naam,
                id    		=> sprintf('nummeraanduiding-%s', $na->identificatie)
            };
		}
	}

	return \@entries;
}

=head1 HELPER METHODS

=head2 _build_entry_set

=cut

sub _build_entry_set {
    my $self = shift;
    my $rs = shift;

    my @results;

    for my $entry ($rs->all) {
        my $prefix = 'openbareruimte';
        my $type = 'bag_street';

        if(eval { $entry->isa('BagNummeraanduiding') }) {
            $prefix = 'nummeraanduiding';
            $type = 'bag_address';
        }

        push @results, {
            id => sprintf('%s-%s', $prefix, $entry->identificatie),
            object_type => $type,
            label => $entry->to_string,
            object => $entry
        };
    }

    return \@results;
}

=head2 _build_resultset

=cut

define_profile _build_resultset => (
    required => [qw[object_type query c]],
    optional => [qw[extra_params max_items]],
    typed => {
        object_type => 'Str',
        query => 'Str',
        c => 'Zaaksysteem',
        extra_params => 'HashRef'
    }
);

sub _build_resultset {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $object_type  = $params->{ object_type };
    my $query        = $params->{ query };
    my $c            = $params->{ c };
    my $extra_params = $params->{ extra_params };
    my $max_items    = $params->{ max_items } || 3;

    my $whereClause = $self->_search_parts({ query => $query });

    if($object_type eq 'contact') {
        return $self->_search_contacts({
            whereClause => $whereClause,
            c => $c,
            limit => $max_items,
        });
    }

    my $model = OBJECTSEARCH_TABLENAMES->{ $object_type }{ tablename };
    my $resultset;

    my $where = { -and => $whereClause };
    my $options = {
        rows        => $max_items,
        order_by    => 'me.search_term',
    };

    # don't include documents that belong to a deleted zaak
    if($object_type eq 'file') {
        $options->{join} = 'case_id';

        delete $whereClause->{ 'case_id.deleted' };
        delete $whereClause->{ 'date_deleted' };
    }

    if($object_type eq 'kennisbank_producten' || $object_type eq 'kennisbank_vragen') {
        delete $whereClause->{ 'me.deleted' };
    }

    if($object_type eq 'zaaktype') {
        $resultset = $c->model('DB::Zaaktype')->search_with_options({
            trigger         => $extra_params->{ trigger },
            betrokkene_type => $extra_params->{ betrokkene_type },
            term            => $query,
        });

        $resultset = $resultset->search({ 'me.active' => 1 }, {
            order_by    => 'me.id',
        });
    }

    if($model eq 'Zaak') {
        $resultset = $c->model(sprintf('DB::%s', $model))->search_extended($where, $options)->search({
            'me.deleted' => undef
        });
    } elsif($object_type ne 'zaaktype') {
        $resultset = $c->model(sprintf('DB::%s', $model))->search($where, $options);
    }

    if ($object_type eq 'zaak') {
        if ($extra_params->{active}) {
            $resultset = $resultset->search({
                'me.status' => [qw/new open stalled/]
            });
        }
    }

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

    my @results = $resultset->all;

    if($object_type eq 'file') {
        return _filter_permitted_files($c, @results);
    }

    ### Last minute safety checks
    if ($object_type eq 'documents') {
        for (my $i = 0; $i < scalar(@results); $i++) {
            my $value = $results[$i];

            $c->stash->{zaak} = $c->model('DB::Zaak')->find($value->{zaak_id});

            unless ($c->check_any_zaak_permission(qw[zaak_read zaak_beheer zaak_edit])) {
                $c->log->debug(sprintf(
                    'No permission for case: %s',
                    $c->stash->{ zaak }->id
                ));

                delete($results[$i]);
            }
        }
    }

    return @results;
}

=head2 _retrieve_object

=cut

define_profile _retrieve_object => (
    required => [qw[c object_type searchable_id]],
    typed => {
        c => 'Zaaksysteem',
        object_type => 'Str',
        searchable_id => 'Int'
    }
);

sub _retrieve_object {
	my ($self, $params) = @_;

    my $opts = assert_profile($params)->valid;

    my $c = $opts->{ c };
    my $object_type = $opts->{ object_type };
    my $searchable_id = $opts->{ searchable_id };

    my $tablename = OBJECTSEARCH_TABLENAMES->{ $object_type }{ tablename };

    my $options = {};

    if($object_type eq 'natuurlijk_persoon') {
        $options->{ prefetch } = [ 'adres_id' ];
    } elsif($object_type eq 'zaaktype') {
        $options->{ prefetch } = [ 'zaaktype_node_id', {
            zaaktype_node_id => 'zaaktype_definitie_id'
        } ];
    }

    $options->{result_class} = 'DBIx::Class::ResultClass::HashRefInflator';

    my $resultset = $c->model("DB::" . $tablename)->search(
        { searchable_id => $searchable_id },
        $options
    );

    ### Deleted of store, ignore
    if($object_type eq 'natuurlijk_persoon' || $object_type eq 'bedrijf') {
        return if $resultset->first->{deleted_on};
    }

    # searchable_id is unique, so it's safe to return first. there ain't gonna
    # be more rows.
    return $self->_preprocess_object($c, $resultset->first, $object_type);
}

=head2 _preprocess_object

=cut

sub _preprocess_object {
    my ($self, $c, $object, $object_type) = @_;

    if($object_type eq 'natuurlijk_persoon') {
        if($object->{voornamen}) {
            $object->{voorletters} = _get_voorletters({
                voornamen => $object->{voornamen}
            });
        }

        if($object->{geboortedatum}) {
            my ($year, $month, $day) = $object->{geboortedatum} =~ m|(\d+)-(\d+)-(\d+)|;
            $object->{geboortedatum} = "$day-$month-$year";
        }

    } elsif($object_type eq 'kennisbank_vragen') {
        $object->{vraag_plaintext} = _limit_text(
            _strip_html($object->{vraag} || ''),
            40
        );
        $object->{antwoord_plaintext} = _limit_text(
            _strip_html($object->{antwoord} || ''),
            40
        );
    } elsif($object_type eq 'kennisbank_producten') {
        $object->{omschrijving_plaintext} = _limit_text(
            _strip_html($object->{omschrijving}),
            40
        );
    } elsif($object_type eq 'zaak') {
        my $status = $object->{status};

        $object->{status_formatted} = STATUS_LABELS->{$status};

        my $case = $c->model('DB::Zaak')->find($object->{ aanvrager }->{ zaak_id });

        if($case) {
            $object->{ description } = $case->onderwerp;
            $object->{ fase } = $case->volgende_fase ? $case->volgende_fase->fase : $case->huidige_fase->fase;
        }
    } elsif($object_type eq 'zaaktype') {

        if(my $preset_client = $object->{zaaktype_node_id}->{zaaktype_definitie_id}->{preset_client}) {
            my ($betrokkene_type, $betrokkene_id) = $preset_client =~ m|betrokkene-(\w+)-(\d+)|;

            if($betrokkene_type && $betrokkene_id) {
                my $betrokkene = $c->model('Betrokkene')->get({ type=> $betrokkene_type}, $betrokkene_id);

                if($betrokkene) {
                    $object->{preset_client} = {
                        naam        => $betrokkene->display_name,
                        id          => $preset_client,
                    };
                }
            }
        }
    } elsif($object_type eq 'bag') {

    }

    return $object;
}

=head1 SUBROUTINES

=head2 _get_voorletters

=cut

define_profile _get_voorletters => (
    required => [qw[voornamen]],
    typed => {
        voornamen => 'Str'
    }
);

sub _get_voorletters {
    my $voornamen = assert_profile(shift)->valid->{ voornamen };

	my ($firstchar) = $voornamen =~ /^(\w{1})/;
	my @other_chars = $voornamen =~ / (\w{1})/g;

	return join(". ", $firstchar, @other_chars) . (
		($firstchar || @other_chars) ?
		'.' : ''
	);
}

=head2 _parse_query

=cut

define_profile _parse_query => (
    required => [qw[query]],
    typed => {
        query => 'Str'
    }
);

sub _parse_query {
    my $query = assert_profile(shift)->valid->{ query };

	my $seen = {};

    # Remove all literal '.' chars
	$query =~ s|\.||gis;

    # Sort by length, the parts of the query not already seen,
    # by splitting the query on whitespace
	my @query_parts = sort { length $b <=> length $a }
		grep { !$seen->{ $_ }++ }
		split /\s/, $query;

	return \@query_parts;
}

=head2 _filter_permitted_files

Users may only find files they have permissions on - otherwise
they will denied anyway which is annoying. if documents have no
case_id, they are not useful for anything, so don't show them.

kinda of shame to create case objects here - but the performance
penalty doesn't seem to be extreme.

=cut

sub _filter_permitted_files {
    my ($c, @results) = @_;

    my $check_file = sub {
        my $zaak = $c->model('DB::Zaak')->find(shift);

        return $c->check_any_given_zaak_permission($zaak, qw[
            zaal_read zaak_beheer zaak_edit
        ]);
    };

    return grep { $_->{ case_id } && $check_file->($_->{ case_id }) } @results;
}

=head2 _limit_text

=cut

sub _limit_text {
    my ($text, $maxlength) = @_;

    my @words = split /\b/, $text;

    my $limited = '';

    foreach my $word (@words) {
        if((length ($limited) + length ($word)) > $maxlength) {

            my $difference = $maxlength - length($limited);
            if($difference > 10) {
                $limited .= substr($word, 0, 8) . '...';
            }
            last;
        }
        $limited .= $word;
    }

    return $limited;
}

=head2 _parse_street_number

This helper method attempts to parse a street number string into it's
constituent components

Returns a list of components in the order C<number>, C<letter>, and C<suffix>

=head3 Example

    _parse_street_number('7-521');

=cut

sub _parse_street_number {
    return shift =~ m[(\d+)([a-zA-Z])?[\s\-]?(\w+)?];
}

=head2 _strip_html

This helper method strips the provided string of HTML content by building an
HTML tree, returning the flattened plain text content of the tree.

=head3 Example

    my $text = _strip_html($potentially_html_content);

=cut

sub _strip_html {
    my $tree = HTML::TreeBuilder->new;

    $tree->parse(shift);
    $tree->eof;

    return $tree->as_text;
}

1;

package Zaaksysteem::Controller::Sysin::Transaction;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Zaaksysteem::ZAPI::CRUD::Interface;
use Zaaksysteem::ZAPI::CRUD::Interface::Column;
use Zaaksysteem::ZAPI::CRUD::Interface::Action;
use Zaaksysteem::ZAPI::CRUD::Interface::Filter;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

use constant ZAPI_CRUD => {
    'records' => Zaaksysteem::ZAPI::CRUD::Interface->new(
        options     => {
            select      => 'none',
        },
        actions => [
        ],
        columns => [
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'id',
                label       => 'Record ID',
                resolve     => 'id',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'preview_string',
                label       => 'Voorbeeld',
                template    => '<a href="/beheer/sysin/transactions/<[item.transaction_id]>/records/<[item.id]>"><[item.preview_string||(\'#\' + item.id)]></a>'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'is_error',
                label       => 'Resultaat',
                template     => "<span class=\"record-item-status icon-font-awesome\" data-ng-class=\"{ 'icon-ok': !item.is_error, 'icon-remove': !!item.is_error}\"></span>",
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'date_time',
                label       => 'Uitgevoerd',
                resolve     => 'date_executed',
                filter      => "date:'dd-MM-yyyy hh:mm'"
            ),
        ],
    ),
    'read'  => Zaaksysteem::ZAPI::CRUD::Interface->new(
        options     => {
            select      => 'multi',
        },
        style       => {
            classes     =>  {
                'transaction-item-row-success'  => '!result.is_error',
                'transaction-item-row-error'    => '!!result.is_error'
            }
        },
        filters => [
            Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                name    => 'interface_id',
                type    => 'select',
                data    => {
                    'options' => '<[getInterfaceOptions()]>'
                },
                value   => '',
                label   => ''
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                name    => 'records.is_error',
                type    => 'checkbox',
                data    => {
                    'checkboxlabel' => 'Alleen met fout',
                    'trueValue'     => '1',
                    'falseValue'    => '<[undefined]>'
                },
                value   => 0,
                label   => ''
            ),
        ],
        actions         => [
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => "transaction-retry",
                type    => "update",
                label   => "Opnieuw",
                data    => {
                    url => '/sysin/transaction/retry/'
                }
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => "transaction-delete",
                type    => "delete",
                label   => "Verwijderen",
                data    => {
                    url => '/sysin/transaction/delete'
                }
            ),
        ],
        columns         => [
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'success',
                label       => 'Status',
                template    => "<i class=\"transaction-item-status icon-font-awesome\" data-ng-class=\"{ 'icon-ok': (item.state == 'success'), 'icon-remove': (item.state == 'error'), 'icon-time': (item.state == 'pending')}\"></span>",
                resolve     => 'success_count'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'name',
                label       => 'Naam (Voorbeeld)',
                resolve     => 'interface.name',
                template    => '<a class="transaction-item-name" data-ng-href="/beheer/sysin/transactions/<[item.id]>"><[item.interface_id.name]></a><span data-ng-show="getPreviewItems(item).length"> (<span class="transaction-record-link" data-ng-repeat="preview in getPreviewItems(item) | limitTo:2"><a href="/beheer/sysin/transactions/<[preview.transaction_id]>/records/<[preview.id]>"><[preview.preview_string||(\'#\' + preview.id)]></a><span data-ng-show="!$last">, </span></span><span data-ng-show="getPreviewItems(item).length>2"> en <[getPreviewItems(item).length-2]> meer</span>)</span>'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'direction',
                label       => 'Richting',
                template    => "<span class=\"transaction-item-direction\" data-ng-class=\"{ 'transaction-item-incoming': item.direction=='incoming', 'transaction-item-outgoing': item.direction!='incoming'}\"><i class=\"icon-font-awesome <[item.direction=='incoming'&&'icon-arrow-down'||'icon-arrow-up']>\"></i> <[item.direction=='incoming'&&'Inkomend'||'Uitgaand']></span>",
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'external_transaction_id',
                label       => 'Extern ID',
                resolve     => 'external_transaction_id',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'date_time',
                label       => 'Volgende poging',
                resolve     => 'date_next_retry',
                filter      => "date:'dd-MM-yyyy hh:mm'"
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'date_created',
                label       => 'Aangemaakt',
                resolve     => 'date_created',
                filter      => "date:'dd-MM-yyyy hh:mm'"
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'records',
                label       => 'Records',
                resolve     => 'total_count'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'errors',
                label       => 'Fouten',
                resolve     => 'error_count'
            )
        ],
    ),
};

=head1 NAME

Zaaksysteem::Controller::Sysin::Transaction - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for System Integration module Transaction.

=head1 METHODS

=head2 /sysin/transaction [GET READ]

 /sysin/transaction?zapi_crud=1
 
 /sysin/transaction


Returns a list of transactions.

B<Special Query Params>

=over 4

=item C<zapi_crud=1>

Use the special query parameter C<zapi_crud=1> for getting a technical CRUD
description.

=back

=cut

sub index
    : Chained('/')
    : PathPart('sysin/transaction')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    if (exists $c->req->params->{zapi_crud}) {
        $c->stash->{zapi} = [ ZAPI_CRUD->{read}->from_catalyst($c) ];
        $c->detach;
    }

    if (exists $c->req->params->{is_error}) {
        $c->req->params
    }

    $c->stash->{zapi}   = $c->model('DB::Transaction')->search_filtered(
        $c->req->params
    );
}


sub base
    : Chained('/')
    : PathPart('sysin/transaction')
    : CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{entry}  = $c->model('DB::Transaction')->find($id);
}

=head2 /sysin/transaction/ID [GET READ]

Reads interface information from Sysin by ID

B<Options>: none

=cut

sub read
    : Chained('base')
    : PathPart('')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->stash->{entry} || [];
}

=head2 /sysin/transaction/$ID/records [GET READ]

 /sysin/transaction?zapi_crud=1
 
 /sysin/transaction

Returns a list of transaction_records.

B<Special Query Params>

=over 4

=item C<zapi_crud=1>

Use the special query parameter C<zapi_crud=1> for getting a technical CRUD
description.

=back

=cut

sub records : Regex('^sysin/transaction/(\d+)/records') : Args(0) : ZAPI {
    my ($self, $c) = @_;
    my ($id)       = @{$c->req->captures};

    $c->assert_any_user_permission('admin');

    if (exists $c->req->params->{zapi_crud}) {
        $c->stash->{zapi} = [ ZAPI_CRUD->{records}->from_catalyst($c) ];
        $c->detach;
    }

    $c->stash->{zapi} = $c->model('DB::TransactionRecord')->search({
        transaction_id => $id,
    }, $c->req->params);
}

=head2 /sysin/transaction/delete [POST DELETE]

Deletes transactions from our database. This will mark these entries as deleted.

=cut

sub delete
    : Chained('/')
    : PathPart('sysin/transaction/delete')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');    

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    ### In case we call this function for the whole resultset, make sure
    ### the search filter is still intact. Get a filtered resultset
    my $rs      = $c->model('DB::Transaction')->search_filtered(
        $c->req->params
    );

    ### Run the trigger "delete" over this resultset    
    $rs->action_for_selection(
        'transaction_delete',
        $c->req->params,
    );

    ### If the above trigger did not throw an error, it succeeded. Return
    ### an empty result to let the frontend know everything worked out the way
    ### we wanted.
    ### Yes: we only supply errors, succeeded rows won't be returned.
    $c->stash->{zapi} = [];
}

=head2 /sysin/transaction/record/delete [POST DELETE]

Deletes transaction_records from our database. This will mark these entries as deleted.

=cut

sub delete_record
    : Chained('/')
    : PathPart('sysin/transaction/record/delete')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    # Get resultset and run the trigger
    my $rs      = $c->model('DB::TransactionRecord')->search(
        $c->req->params
    );
    $rs->action_for_selection(
        'transaction_record_delete',
        $c->req->params,
    );

    ### If the above trigger did not throw an error, it succeeded. Return
    ### an empty result to let the frontend know everything worked out the way
    ### we wanted.
    $c->stash->{zapi} = [];
}

=head2 /sysin/transaction/retry [POST UPDATE]

Retry transactions.

=cut

sub retry
    : Chained('/')
    : PathPart('sysin/transaction/retry')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    ### In case we call this function for the whole resultset, make sure
    ### the search filter is still intact. Get a filtered resultset
    my $rs      = $c->model('DB::Transaction')->search_filtered(
        $c->req->params
    );

    ### Run the trigger "delete" over this resultset    
    my $result  = $rs->action_for_selection(
        'transaction_retry',
        $c->req->params,
    );

    ### If the above trigger did not throw an error, it succeeded. Return
    ### an empty result to let the frontend know everything worked out the way
    ### we wanted.
    ### Yes: we only supply errors, succeeded rows won't be returned.
    $c->stash->{zapi} = $result;
}

=head2 record

Fetches all the information of a given transaction_record.id.

=cut

sub record : Local {
    my ($self, $c, $id) = @_;

    $c->assert_any_user_permission('admin');

    my ($result) = $c->model('DB::TransactionRecord')->find($id);
    if (!$result) {
        throw('/sysin/transaction/record/notfound', "Record with ID $id not found");
    }

    $c->stash->{json}  = $result;
    $c->forward('Zaaksysteem::View::JSON');
}

sub run : Local {
    my ($self, $c)          = @_;

    $c->model('DB::Transaction')->process_pending();
}

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut 



package Zaaksysteem::Controller::Sysin::Interface;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Zaaksysteem::Backend::Sysin::Modules;

use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Sysin::Interface - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for System Integration module Interface.

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

You can use the C<zapi_form=1> query parameter to retrieve more information
from a function regarding the needed form parameters.

=head1 METHODS

=head2 /sysin/interface [GET READ]

Returns a resultset of loaded interfaces

=cut

sub index
    : Chained('/')
    : PathPart('sysin/interface')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{zapi}   = $c->model('DB::Interface')->search_active();
}

=head2 /sysin/modules [GET READ]

    {
       "next" : null,
       "status_code" : "200",
       "prev" : null,
       "num_rows" : 1,
       "rows" : 1,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "manual_type" : "file",
             "is_casetype_interface" : 0,
             "name" : "bagcsv",
             "has_attributes" : 1,
             "direction" : "outgoing",
             "max_retries" : 10,
             "is_manual" : 1,
             "allow_multiple_configurations" : 1,
             "retry_interval" : [
                300,
                300,
                1800,
                14400,
                14400,
                14400,
                86400,
                86400,
                604800
             ],
             "is_multiple" : 1
          },
       ]
    }

Returns a list of installed interface modules

=cut

sub modules
    : Chained('/')
    : PathPart('sysin/interface/modules')
    : Args(1)
    : ZAPI
{
    my ($self, $c, $type)  = @_;

    $c->assert_any_user_permission('admin');

    if ($type eq 'available') {
        $c->stash->{zapi}   = [
            Zaaksysteem::Backend::Sysin::Modules
                ->list_of_available_modules(
                    $c->model('DB')
                )
        ];
    } elsif ($type eq 'all') {
        $c->stash->{zapi}   = [
            Zaaksysteem::Backend::Sysin::Modules
                ->list_of_modules(
                    $c->model('DB')
                )
        ];
    }
}

=head2 /sysin/interface/create [POST CREATE]

Creates a new interface on Sysin.

See L<Zaaksysteem::Backend::Sysin::Interface::ResultSet#interface_create> for more
details.

=cut

sub create
    : Chained('/')
    : PathPart('sysin/interface/create')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{zapi} = 
        $c->model('DB::Interface')->interface_create($c->req->params);
}

sub base
    : Chained('/')
    : PathPart('sysin/interface')
    : CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    ### Unfortunatly, I needed this check in the last hours before OKT release,
    ### need a better solution for preventing a forbidden at the soap gateway.
    unless ($c->req->action eq '/sysin/interface/soap/enter') {
        $c->assert_any_user_permission('admin');
    }

    $c->stash->{entry}  = $c->model('DB::Interface')->find_active({ id => $id });

    throw(
        'interface/entry_not_found',
        'Interface entry with ID: ' . $id . ' not found'
    ) unless $c->stash->{entry};
}



sub base_no_auth
    : Chained('/')
    : PathPart('sysin/interface')
    : CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    $c->stash->{entry}  = $c->model('DB::Interface')->find_active({ id => $id });

    throw(
        'interface/entry_not_found',
        'Interface entry with ID: ' . $id . ' not found'
    ) unless $c->stash->{entry};
}


=head2 /sysin/interface/ID [GET READ]

Reads interface information from Sysin by ID

B<Options>: none

=cut

sub read
    : Chained('base')
    : PathPart('')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->stash->{entry} || [];
}

=head2 /sysin/interface/ID/update [POST]

Updates an interface on Sysin. Use C<zapi_form> for form information

=cut

sub update
    : Chained('base')
    : PathPart('update')
    : Args(0)
    : ZAPI 
{
    my ($self, $c) = @_;

    if (exists $c->req->params->{zapi_form}) {
        my $form = $c->stash->{entry}->get_interface_form;

        $form->actions->[1]->{data}->{url} = $c->uri_for(
            '/sysin/interface/' . $c->stash->{entry}->id . '/update'
        )->as_string;

        $c->stash->{zapi} = [
            $form
        ];

        $c->detach('View::ZAPI');
    }

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    my $params          = $self->_prepare_interface_params($c->req->params);

    $c->log->debug('Sending params: ' . Dumper($params));
    
    $c->stash->{zapi}   = $c->stash->{entry}->interface_update($params);
}

=head2 /sysin/interface/ID/manual_process [POST]

Return value: ZAPI object containing Transaction Record

    # /sysin/interface/ID/manual_process?input_file=2b3fe-2bfe8bfe-eefbbe-bebfbe-efef

    # /sysin/interface/ID/manual_process?input_data=ParseThisJSONString

Manually processes data on this interface by giving an input file or input data.

B<Options>

=over 4

=item input_file

A UUID pointing to an entry in our filestore

=item input_data

A bulk of text containing the data. Beware that not every interface supports this
feature. Be save, and use input_file

=back

=cut

sub manual_process
    : Chained('base')
    : PathPart('manual_process')
    : Args(0)
    : ZAPI 
{
    my ($self, $c)  = @_;

    my $params      = $c->req->params;

    $c->log->debug(Dumper($params));

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    throw('sysin/interface/manual_proccess/no_input', 'No input given', [])
      unless ($params->{input_file} || $params->{input_data});

    if ($params->{input_data}) {
        $c->stash->{zapi} = $c->stash->{entry}->process(
            {
                input_data                => $params->{input_data},
            }
        ) || [];
    } elsif ($params->{input_file}) {
        warn('Process filestore ID');
        $c->stash->{zapi} = $c->stash->{entry}->process(
            {
                input_filestore_uuid      => $params->{input_file},
            }
        ) || [];
    }
}

### XXX DELETE IT
sub manual_process_soap
    : Chained('base')
    : PathPart('manual_process_soap')
    : Args(0)
    : ZAPI 
{
    my ($self, $c)  = @_;

    my $params      = $c->req->params;

    open(my $fh, "<:encoding(UTF-8)", 'share/stuf/prs/101-prs-create-tinus.xml') or
        die('Cannot find test file');

    my $xml     = '';
    while (<$fh>) {
        $xml    .= $_;
    }

    close($fh);

    $c->stash->{zapi} = $c->stash->{entry}->process(
        {
            input_data                => $xml,
        }
    ) || [];
}

=head2 /sysin/interface/ID/delete [POST]

Deletes an interface from the interface list

=cut

sub delete
    : Chained('base')
    : PathPart('delete')
    : Args(0)
    : ZAPI 
{
    my ($self, $c) = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    if ($c->stash->{entry}->interface_delete) {
        $c->stash->{zapi}   = [];
    }
}

=head2 /sysin/interface/ID/trigger/TRIGGER_ACTION [POST/GET depending on interface]

Triggers an action on the module interface.

=cut

sub trigger
    : Chained('base_no_auth')
    : PathPart('trigger')
    : Args(1)
    : ZAPI 
{
    my ($self, $c, $action) = @_;

    my $definition  = $c->stash->{entry}->get_trigger_definition($action);

    # If an interface isn't set as public a user needs to admin to be able to run a trigger.
    if (!$definition->{public} && !$c->check_any_user_permission('admin')) {
        throw "sysin/interface/ID/trigger/action",
            "Action $action is not public and no user with valid permissions was found";
    }

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        (!$definition->{update} || lc($c->req->method) eq 'post');

    $c->stash->{zapi}   = $c->stash->{entry}->process_trigger(
        $action,
        $c->req->params
    ) || [];
}

=head2 /sysin/interface/ID/mapping [GET]

    {
      'attributes' => [
        {
          'checked' => 0,
          'external_name' => 'woonplaats_identificatie',
          'internal_name' => 'identificatie',
          'optional' => 0
        },
        {
          'checked' => 0,
          'external_name' => 'woonplaats_begindatum',
          'internal_name' => 'begindatum',
          'optional' => 0
        },
        {
          'checked' => 0,
          'external_name' => 'woonplaats_einddatum',
          'internal_name' => 'einddatum',
          'optional' => 1
        },
        {
          'checked' => 0,
          'external_name' => 'woonplaats_naam',
          'internal_name' => 'naam',
          'optional' => 0
        },
        {
          'checked' => 0,
          'external_name' => 'openbareruimte_identificatie',
          'internal_name' => 'identificatie',
          'optional' => 0
        },
      ],
      'attributes_type' => 'defined',
      'zaaktype_id' => undef
    }

Reads attribute mapping

=cut

sub mapping_index
    : Chained('base')
    : PathPart('mapping')
    : Args(0)
    : ZAPI 
{
    my ($self, $c) = @_;

    $c->stash->{zapi}   = [ $c
                        ->stash
                        ->{entry}
                        ->get_attribute_mapping() ];
}


=head2 /sysin/interface/ID/mapping/update [POST]

    {
      'attributes' => [
        {
          'checked' => 0,
          'external_name' => 'woonplaats_identificatie',
          'internal_name' => 'identificatie',
          'optional' => 0
        },
        {
          'checked' => 0,
          'external_name' => 'woonplaats_begindatum',
          'internal_name' => 'begindatum',
          'optional' => 0
        },
        { /* CHANGED CHECKED TO (PERL) BOOLEAN TRUE */
          'checked' => 1,
          'external_name' => 'woonplaats_einddatum',
          'internal_name' => 'einddatum',
          'optional' => 1
        },
        {
          'checked' => 0,
          'external_name' => 'woonplaats_naam',
          'internal_name' => 'naam',
          'optional' => 0
        },
        {
          'checked' => 0,
          'external_name' => 'openbareruimte_identificatie',
          'internal_name' => 'identificatie',
          'optional' => 0
        },
      ],
      'attributes_type' => 'defined',
      'zaaktype_id' => undef
    }

Updates a attribute mapping on Sysin. When no options are given, on
=cut

sub test
  : Chained('base')
  : PathPart('test')
  : Args()
  : ZAPI
{
    my ($self, $c, $test_id) = @_;

    throw(
        'sysin/modules/test/not_found',
        'No tests defined for this interface'
    ) unless $c->stash->{entry}->module_object->test_interface;

    if (!$test_id) {
        $c->stash->{zapi} = [$c->stash->{entry}->module_object->test_definition];
        $c->detach;
    }
    my $result = $c->stash->{entry}->run_test($test_id);

    $c->stash->{zapi} = [$result];
}

sub mapping_update
    : Chained('base')
    : PathPart('mapping/update')
    : Args(0)
    : ZAPI 
{
    my ($self, $c) = @_;

    if (exists $c->req->params->{zapi_form}) {
        my $form = $c->stash->{entry}->get_interface_form;

        $form->actions->[1]->{data}->{url} = $c->uri_for(
            '/sysin/interface/' . $c->stash->{entry}->id . '/update'
        )->as_string;

        $c->stash->{zapi} = [
            $form
        ];

        $c->detach('View::ZAPI');
    }

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    $c->stash->{zapi}   = [
        $c->stash->{entry}->set_attribute_mapping($c->req->params)
    ];
}

sub _prepare_interface_params {
    my $self            = shift;
    my $request_params  = shift;

    my $params          = {};

    for my $param (keys %{ $request_params }) {
        my $value   = $request_params->{$param};

        if ($param  =~ /^interface_/) {
            $params->{interface_config} = {} unless UNIVERSAL::isa(
                $params->{interface_config}, 'HASH'
            );

            $param  =~ s/^interface_//;

            $params->{interface_config}->{$param} = $value;
            next;
        }

        $params->{ $param } = $value;
    }

    return $params;
}

=head1 AUTHOR

vagrant,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;



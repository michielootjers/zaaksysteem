package Zaaksysteem::Controller::Datastore;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Datastore - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for Datastore (Gegevensmagazijn).

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

You can use the C<zapi_form=1> query parameter to retrieve more information
from a function regarding the needed form parameters.

=head1 METHODS

=head2 /datastore [GET READ]

Returns a resultset of loaded interfaces

=cut

use constant DATASTORE_CLASSES => [qw/
    NatuurlijkPersoon
    Bedrijf
    BagLigplaats
    BagNummeraanduiding
    BagOpenbareruimte
    BagPand
    BagStandplaats
    BagVerblijfsobject 
    BagWoonplaats
/];

use constant NATUURLIJK_PERSOON_DATE_FIELDS => qw/
    geboortedatum
    datum_aanvang_verblijfsrecht
    datum_einde_verblijfsrecht
    import_datum
    deleted_on
    datum_overlijden
    onderzoek_persoon_ingang
    onderzoek_persoon_einde
    onderzoek_huwelijk_ingang
    onderzoek_huewelijk_einde
    onderzoek_overlijden_ingang
    onderzoek_overlijden_einde
    onderzoek_verblijfsplaats_ingang
    onderzoek_verblijfsplaats_einde
    datum_huwelijk
    datum_huwelijk_ontbinding
/;

use constant BEDRIJF_DATE_FIELDS => qw/
    import_datum
    deleted_on
/;

use constant BAG_DATE_FIELDS => qw/
    begindatum
    einddatum
    documentdatum
/;


# prep here so with large exports we save a few cpu cycles
has DATE_FIELDS_CONFIG => (
    is => 'ro',
    lazy => 1,
    default => sub {
        # expects a Perl datetime object or falsy
        my $date_formatter = sub {
            my $value = shift;
            return $value && $value->strftime('%d-%m-%Y %H:%M:%S');
        };

        # expects something like 20101012000000, 2009-07-01. 20070203 or falsy
        my $bag_date_formatter = sub {
            my $value = shift;

            # if the value is truthy and matches one of these three formats,
            # return a DateTime object
            if ($value && (
                    $value =~ m|^(\d{4})(\d{2})(\d{2})\d{6}$| ||
                    $value =~ m|^(\d{4})-(\d{2})-(\d{2})$| ||
                    $value =~ m|^(\d{4})(\d{2})(\d{2})$|
                )
            ) {
                return DateTime->new(
                    year   => $1,
                    month  => $2,
                    day    => $3
                )->strftime('%d-%m-%Y');
            }

            # fall through - if somehow no match, just return input
            return $value;
        };

        return {
            NatuurlijkPersoon => {
                map { $_ => $date_formatter } (NATUURLIJK_PERSOON_DATE_FIELDS)
            },
            Bedrijf => {
                map { $_ => $date_formatter } (BEDRIJF_DATE_FIELDS)
            },
            Bag => {
                # disabled Bag date formatting. Above code is functional, so for the sake
                # of future gains, and because this setup has been tested already
                # I'm leaving it intact.
                #map { $_ => $bag_date_formatter } (BAG_DATE_FIELDS)
            },
        }
    }
);


sub index
    : Chained('/')
    : PathPart('datastore')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{zapi}   = [];
}

sub base
    : Chained('/')
    : PathPart('datastore/search')
    : Args(1)
{
    my ($self, $c, $class) = @_;

    my $classes = DATASTORE_CLASSES;

    # Module/auth allowed check
    $c->assert_any_user_permission('admin');
    if (!grep{$_ eq $class} @{ $classes }) {
        throw 'datastore/class_not_allowed', "Class $class is not allowed for datastore viewing/exporting";
    }

    if (exists $c->req->params->{zapi_crud}) {
        my @columns = _get_columns($c, $class);
        $c->stash->{zapi} = [
            Zaaksysteem::ZAPI::CRUD::Interface->new(
                options     => {
                    select      => 'multi',
                },
                actions => [],
                columns => \@columns,
                url     => '<[cl]>'
            )
        ];
        $c->detach();
    }

    $c->stash->{zapi} = _get_data_for_class($c, $class);
}

sub classes
    : Chained('/')
    : PathPart('datastore/classes')
{
    my ($self, $c) = @_;
    my $classes = DATASTORE_CLASSES;
    $c->stash->{json} = \@$classes;
    $c->forward('Zaaksysteem::View::JSON');
}

sub csv
    : Chained('/')
    : PathPart('datastore/csv')
    : Args(1)
{
    my ($self, $c, $class) = @_;
    my @records = _get_data_for_class($c, $class)->all;
    my @columns = $c->model("DB::$class")->result_source->columns;

    my @active_object_subscription_ids =
        map {
            $_->id
        } $c->model("DB::ObjectSubscription")->search({
            local_table  => $class,
            date_deleted => undef
        })->all();

    my @csv;
    push @csv, \@columns;

    for my $r (@records) {
        my @line = $self->format_fields($class, $r, \@columns);

        my ($has_object_subscription) = grep {$_ eq $r->id} @active_object_subscription_ids;
        if ($has_object_subscription) {
            push @line, 'ja';
        }

        push @csv, \@line;
    }
    push @columns, 'afnemerindicatie';

    $c->stash->{csv} = { data => \@csv };

    my $csv = $c->view('CSV')->render($c, $c->stash);

    $c->res->headers->header( 'Content-Type'  => 'application/x-download' );
    $c->res->headers->header(
        'Content-Disposition'  =>
            sprintf "attachment;filename=%s-%s.csv", ($class, time())
    );
    $c->res->body($csv);
}

sub _get_data_for_class {
    my ($c, $class) = @_;
    my $search_opts = {
        prefetch    => []
    };

    my $search      = {};



    if ($class eq 'NatuurlijkPersoon') {
        ### Join address
        push(@{ $search_opts->{prefetch} }, 'adres_id');
        $search->{'me.deleted_on'} = undef;
    } elsif ($class eq 'Bedrijf') {
        $search->{'me.deleted_on'} = undef;
    } elsif ($class eq 'BagNummeraanduiding') {
        push(@{ $search_opts->{prefetch} }, { openbareruimte => 'woonplaats'});
    } elsif ($class eq 'BagOpenbareruimte') {
        push(@{ $search_opts->{prefetch} }, 'woonplaats');
    }

    if ($c->model("DB::$class")->result_source->has_relationship('subscription_id')) {
        push(@{ $search_opts->{prefetch} }, 'subscription_id');
    }


    return $c->model("DB::$class")->search(
        {
            %{ $search },
            'me.id' => {
                # Make sure that any objects that are deleted in the database are 
                # excluded from the resultset. This NEEDS to be done beforehand, all
                # alternatives would mean breaking the resultset later on. (And ruining
                # paging, easy exporting.)
                'not in ' => $c->model("DB::ObjectSubscription")->search({
                    local_table  => $class,
                    date_deleted => {'!=', undef}
                })->get_column('id')->as_query 
            }
        },
        $search_opts
    );
}

sub _get_columns {
    my ($c, $class) = @_;

    my @columns = grep {
        $_ ne 'search_index' &&
        $_ ne 'search_term' &&
        $_ ne 'searchable_id' &&
        $_ ne 'object_type' &&
        $_ ne 'adres_id'            ### Special natuurlijkpersoon case
    } $c->model("DB::$class")->result_source->columns;

    ### Skip some internal columns

    # Object subscription isn't an actual column
    my @generic_columns;
    push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id       => 'object_subscription',
        label    => 'afnemerindicatie',
        template => '<[item.object_subscription.external_id]>',
        when     => 'item.object_subscription'
    );

    # Process columns
    for my $col (@columns) {
        push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id          => 'me.' . $col,
            label       => $col,
            resolve     => $col,
        );
    }

    ### Special NatuurlijkPersoon case
    if ($class eq 'NatuurlijkPersoon') {
        my @adr_columns = grep {
            $_ ne 'search_index' &&
            $_ ne 'search_term' &&
            $_ ne 'searchable_id' &&
            $_ ne 'object_type'
        } $c->model("DB::Adres")->result_source->columns;

        for my $col (@adr_columns) {
            push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'adres_id.' . $col,
                label       => $col,
                resolve     => 'adres_id.' . $col,
            );
        }
    }
    return @generic_columns;
}


=head2 format_fields

If a field is a designated date field, format as date. Otherwise return string
value.

=cut

sub format_fields {
    my ($self, $class, $row, $columns) = @_;

    # Bag* all share same config
    my $config_class = $class =~ m|^Bag| ? 'Bag' : $class;
    my $formatters = $self->DATE_FIELDS_CONFIG->{$config_class};

    return map {
        # if a specific formatter is defined for this field, apply it.
        $formatters->{$_} ?
            $formatters->{$_}->($row->$_) :
            # otherwise fall back on default DBIx::Class behaviour
            $row->get_column($_)
    } @$columns;
}

=head1 AUTHOR

vagrant,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;



package Zaaksysteem::Controller::Test;

use Moose;

use Zaaksysteem::Exception;
use Data::Dumper;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('test') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    unless($c->debug) {
        throw('test/mode', 'Unable to enter testmode in non-test environment');
    }
}

sub upload_global : Chained('base') : PathPart('upload_global') : Args(0) {
    my ($self, $c) = @_;

    unless (lc($c->req->method) eq 'post') {
        $c->stash->{ template } = 'test/upload_global.tt';
        $c->detach;
    }

    my $file = $c->req->upload('file');

    unless ($file) {
        throw('test/upload_global', 'Failed to get file, did you upload?');
    }

    $c->model('DB::File')->file_create({
        db_params => {
        },
        name => $file->filename,
        file_path => $file->tempname
    });

    $c->res->body('Done. <a href="">Reload</a>?');
}

1;

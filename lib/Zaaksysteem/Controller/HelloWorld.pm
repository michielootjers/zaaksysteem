package Zaaksysteem::Controller::HelloWorld;

use Moose;

use namespace::autoclean;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::HelloWorld - Catalyst Controller

=head1 SYNOPSIS

 # Will give a description of this API, and makes it possible to test the API
 /helloworld/help

=head1 DESCRIPTION

Zaaksysteem API Controller.

=head1 METHODS

=head2 help

Gives the API Help page describing this API and a test interface.

=cut

define_profile index => (
    required => [qw[test subject]]
);

sub index : Chained('/') : PathPart('helloworld') : Args(0) : ZAPI {
    assert_profile({});
}

sub create : Chained('/') : PathPart('helloworld/create') : Args(0) : ZAPI :
Profile('create') : AssertProfile {
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->model('DB::Product')->find(1);
}

sub base : Chained('/') : PathPart('helloworld') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;
}

sub read : Chained('base') : PathPart('') : Args(0): ZAPI {
    my ($self, $c) = @_;

    $c->stash->{json}   = {
        data    => 'bla'
    };
}

sub update : Chained('base') : PathPart('update') : Args(0) : Profile('update'): ZAPI {}

sub delete : Chained('base') : PathPart('delete') : Args(0) : Profile('delete'): ZAPI {}

=head1 AUTHOR

michiel,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

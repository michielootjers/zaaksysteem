package Zaaksysteem::Controller::Man;

use strict;
use warnings;
use Data::Dumper;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::POD;

use parent 'Catalyst::Controller';

sub index :Path :Args() {
    my ( $self, $c, $manual ) = @_;

    my $p = Zaaksysteem::POD->new;

    $p->html_css('/css/documentation.css');
    $p->index(1);
    $p->output_string(\$c->stash->{pod_output});

    # TODO:
    # Perhaps this logic should move to Zaaksysteem::POD
    my $file = $manual;
    $file =~ s|::|/|g;

    my $libdir = $c->path_to('lib');
    my $devel  = $c->path_to('t/inc');

    my $pod = _get_file($libdir, $file);

    if (!$pod && $devel) {
        $pod = _get_file($devel, $file);
    }

    # TODO:
    # Generate a listing of all the .pod/pm/pl files so we can have a nice clicky clicky webinterface
    $pod ||= catfile($libdir, 'Zaaksysteem.pm');

    $p->parse_file($pod);
    $c->res->body($c->stash->{pod_output});
}

=begin private

=head2 _get_file

Retreives the POD file for a specific module

=head3 Synopsis

my $pod = _get_file($libdir, $filename);

=head3 Arguments

=over

=item libdir

=item filename

=back

=head3 Returns

Returns the full path to the file containing the pod

=cut

sub _get_file {
    my ($libdir, $file) = @_;

    $file = catfile($libdir, $file);

    return $file if (-f $file);
    foreach (qw(pm pod)) {
        my $p = "$file.$_";
        return $p if (-f $p);
    }
    return;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


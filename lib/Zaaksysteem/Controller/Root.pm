package Zaaksysteem::Controller::Root;

use strict;
use warnings;
use Data::Dumper;
use parent 'Catalyst::Controller';

use Scalar::Util qw/blessed/;

use Zaaksysteem::Constants qw/
    ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE
    ZAAKTYPE_KENMERKEN_DYN_DEFINITIE
/;

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config->{namespace} = '';


sub begin : Private {
    my ($self, $c) = @_;

    ### C::P
    if (!$c->forward('/page/begin')) { return; }
}


sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->forward('/zaak/list');
}


sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

sub forbidden : Private {
    my ($self, $c) = @_;

    $c->stash->{template} = 'forbidden.tt';
    $c->detach;
}

sub end : ActionClass('RenderView') {
    my ($self, $c) = @_;

    my @errors = @{ $c->error };

    $c->forward('touch_delayed_cases');

    if(scalar(@errors)) {
        my $err = shift @errors;

        $c->clear_errors;

        if(UNIVERSAL::isa($err, 'Zaaksysteem::Exception::Base')) {

            if ($c->req->is_xhr || $c->stash->{current_view} eq 'ZAPI') {
                $c->stash->{ zapi } = $err->get_ZAPI_error;

                $c->log->error(sprintf('Caught exception: %s', $err->TO_STRING));

                $c->detach($c->view('ZAPI'));
            }

            $c->error($err->TO_STRING);
            $c->log->debug("Dumping stacktrace", '=================================', $err->trace_frames);
        } else {
            if($c->req->is_xhr || $c->stash->{current_view} eq 'ZAPI') {
                my $logerror = sprintf("Caught exception: %s", $err);
                $c->stash->{zapi}   = Zaaksysteem::ZAPI::Error->new(
                    type        => 'unknown',
                    messages    => $err,
                );

                $c->log->error($logerror);
                $c->detach($c->view('ZAPI'));
            }

            my $errstring = $err;

            if (blessed($errstring) && $errstring->can('as_string')) {
                $errstring = $errstring->as_string;
            }

            $c->error(sprintf("Caught exception: %s", $errstring));
        }
    }
}

sub touch_delayed_cases : Private {
    my ($self, $c)                  = @_;

    my $delayed_touch               = $c->model('DB')->schema->default_resultset_attributes->{delayed_touch};

    $c->log->debug('Touching cases: ' . join(', ', @{ $delayed_touch->cases }));

    $delayed_touch->execute($c->model('DB')->schema);
}


sub monitor : Global {
    my ($self, $c)  = @_;

    if ($c->req->params->{false}) {
        $c->res->body('CHECKFALSE (forcefalse: geforceerd afgebroken)');
        $c->detach;
    }

    my $logging = $c->model('DB::Logging')->search_events('monitor')->search({
        created => { '>', DateTime->now(time_zone => 'local')->subtract(minutes => 2) }
    });

    for my $event ($logging->all) {
        next unless $event->data->{ ip_address } eq $c->req->address;

        $c->res->body('CHECKOK');
        $c->detach;
    }

    my %errs;

    for my $check (qw/database/) {
        my $routine = '_monitor_' . $check;

        unless ((my $msg = $c->forward($routine)) eq 1) {
            $c->log->debug(sprintf(
                "Something's up, an error occurred while executing %s checks: %s",
                $check,
                $msg
            ));

            $errs{ $check } = $msg;
        }
    }

    unless(scalar(keys %errs)) {
        $c->res->body('CHECKOK');
        $c->detach;
    }

    $c->res->body(sprintf(
        'CHECKFALSE (%s)',
        join(',', map { sprintf('%s: %s', $_, $errs{ $_ }) } keys %errs)
    ));

    $c->detach;
}

sub _monitor_database : Private {
    my ($self, $c) = @_;

    my $event = $c->model('DB::Logging')->trigger('monitor/check', { component => 'monitor', data => {
        ip_address => $c->req->address
    }});

    unless($event) {
        return 'Could not write to database';
    }

    unless($c->model('DB::Logging')->search({}, { rows => 1 })->count) {
        return 'Could not read from database (or no logging found)';
    }

    return 1;
}

sub http_error : Private {
    my ( $self, $c, %opt ) = @_;
    my @valid_types = qw/404 403 500/;

    ### Defaults to 404 handling
    $opt{type} = 404 unless $opt{type};

    ### Some security awareness in place
    $opt{type} = 500 unless grep({$opt{type} eq $_} @valid_types);

    ### Set response status
    $c->res->status($opt{type});

    ### Error handling, send template error information and set view
    $c->stash->{error} = \%opt;
    $c->stash->{template} = 'error/' . $opt{type} . '.tt';

    return $opt{type};
}

sub logout : Local {
    my ($self, $c) = @_;

    $c->logout();
    $c->delete_session();

    my $redirect = $c->uri_for('/pip');
    if ($c->req->params->{redirect}) {
        $c->log->debug('REDIRECTING: ' . $c->req->params->{redirect});
        if ($c->req->params->{redirect} eq 'gemeente_portal') {
            $redirect = $c->config->{gemeente}->{gemeente_portal};
        }
    }

    $c->response->redirect($redirect);
    $c->detach;
}



1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


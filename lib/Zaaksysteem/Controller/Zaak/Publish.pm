package Zaaksysteem::Controller::Zaak::Publish;

use strict;
use warnings;
use Data::Dumper;
use Zaaksysteem::Constants;

use parent 'Catalyst::Controller';


Zaaksysteem->register_profile(
    method  => 'publish',
    profile => {
        required => [ qw/selection profile/],
    },
);
sub bulk_publish : Chained('/') : PathPart('bulk/publish') {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    my $selection = $params->{selection};
    
# TODO: When checkboxes are used to make a selection, the display fields
# of the search query are not used. This can be fixed by changing the javascript
# to look for an active search query, passing on the parameter and getting the
# fields from that SearchQuery object.

    # default display fields
    my $search_query = $c->model('SearchQuery');
    my $display_fields = $search_query->get_display_fields();
        
    if($selection eq 'selected_cases') {
        my @case_ids = split /,/, $params->{selected_case_ids};

        $c->stash->{cases} = $c->model('DB::Zaak')->search_extended({
            'me.id' => { 
                '-in' => \@case_ids 
            }
        });

    } elsif($selection eq 'search_results') {
        if($params->{search_query_id}) {
            my $record = $c->model(SEARCH_QUERY_TABLE_NAME)->find($params->{search_query_id});
            $search_query->unserialize($record->settings);
        } elsif($c->session->{SEARCH_QUERY_SESSION_VAR()}) {
		    $search_query->unserialize($c->session->{SEARCH_QUERY_SESSION_VAR()});
		} else {
		    die "no search query active";
		}

        $c->stash->{cases} = $search_query->results({c => $c});
        $display_fields = $search_query->get_display_fields();
    } else {
        die "unknown selection: $selection";
    }

    for (keys %$params) {
        $c->stash->{$_} = $params->{$_};
    }

    my %publish_related_ids;

    foreach my $related_case_id ($c->req->param('related_case_id')) {
        $publish_related_ids{ $related_case_id } = 1;
    }

    my $published_file_ids = {};
    foreach my $file_id_combi ($c->req->param('file_id')) {
        # namespacing ot allow the same files be used differently in different cases
        my ($case_id, $file_id) = split /-/, $file_id_combi;

        next unless $c->model('DB::File')->search({
            id => $file_id,
            case_id => { -in => [keys %publish_related_ids] }
        })->count;

       $published_file_ids->{ $file_id_combi } = 1;
    }


    if($params->{commit}) {
        my $profile = $params->{profile};

        eval {
            my $result = $c->stash->{cases}->publish({
                root_dir        => $c->config->{root},
                files_dir       => $c->model('DB::Config')->get_value('tmp_location'),
                hostname        => $c->req->uri->host,
                publish_script  => $c->config->{home} . '/bin/publish.pl',
                profile         => $profile,
                config          => $c->config->{publish}->{$profile},
                display_fields  => $display_fields,
                published_file_ids => $published_file_ids,
                published_related_ids => \%publish_related_ids,
                dry_run         => $params->{dry_run}
            });

            $c->stash->{publish_result} = $result->{result};
            $c->stash->{publish_logging} = $result->{logging};
        };
        
        if($@) {
            $c->stash->{publish_result} = 1;
            $c->stash->{publish_logging} .= "Server fout: " . $@;
        }
        
    }
    $c->stash->{cases} = $c->stash->{cases}->search({}, {rows=>10});
    $c->stash->{template} = 'zaak/publish.tt';    
    $c->stash->{nowrapper} = 1;
}


sub testpublish : Chained('/') : PathPart('testpublish') : Args() {
    my ($self, $c, $id, $publish) = @_;

    my $zaak = $c->model('DB::Zaak')->find($id);

    $c->res->content_type('text/xml');
    $c->res->body(
        $zaak->ede_vergadering_document({
            cases => {},
            files => {},
            publish => $publish
        })->toStringHTML()
    );
    $c->detach; 
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Controller::Zaak::Upload;

use Data::Dumper;
use Moose;
use HTML::TagFilter;
use Try::Tiny;
use File::stat;

BEGIN { extends 'Catalyst::Controller' }


=head2 upload

handle one or more file uploads by delegating

=cut

sub upload : Chained('/zaak/base') : PathPart('upload') {
    my ($self, $c) = @_;

    $c->stash->{uploaded_files} = [];

    foreach my $upload_param (keys %{$c->req->uploads}) {
        $c->forward('handle_single_file_upload', [$upload_param]);
    }

    $c->stash->{template}  = 'uploadresponse.tt';
    $c->stash->{nowrapper} = 1;
}


=head2 handle_single_file_upload

Given a single file upload parameter, handle upload

=cut

sub handle_single_file_upload : Private {
    my ($self, $c, $upload_param) = @_;

    my $upload = $c->req->upload($upload_param);
    my $tf = HTML::TagFilter->new;
    my $filename = $tf->filter($upload->filename);

    my ($kenmerk_id) = $c->req->params->{file_id} =~ m|(\d+)$|;

    # IE stuff
    unless ($kenmerk_id) {
        ($kenmerk_id) = $upload_param =~ m|(\d+)$|;
    }
    $c->stash->{fieldname} = $upload_param;
    $c->stash->{filename} = $filename;
    # end IE

    throw('case/upload', 'Unable to process upload, no attribute id') unless $kenmerk_id;

    my $params = {
        uploads => {
            $kenmerk_id => [{'upload' => $upload}]
        }
    };

    $c->forward("/zaak/_create_zaak_handle_uploads", [$params]);

    if($params->{upload_error}) {
        my $upload_error = $params->{upload_error};
        if (ref $upload_error && ref $upload_error eq 'Zaaksysteem::Exception::Base') {
            $upload_error = $upload_error->{message};
        } elsif(ref $upload_error && ref $upload_error eq 'ARRAY') {
            $upload_error = join ", ", @$upload_error;
        }
        $c->stash->{upload_error} = $upload_error;
    } else {
        my $display = {
            filename => $upload->filename,
            mimetype => $upload->type,
            file_id  => $c->stash->{upload_result_file}->id,
            accepted => $c->stash->{upload_result_file}->accepted
        };
        my $files = $c->stash->{uploaded_files};
        push @$files, $display;
    }
}


sub remove_upload : Chained('/zaak/base') : PathPart('upload/remove_upload') {
    my ($self, $c) = @_;

    my ($bibliotheek_kenmerken_id) = $c->req->params->{kenmerk_id} =~ m|(\d+)$|;

    die "need bibliotheek_kenmerken_id" unless $bibliotheek_kenmerken_id;

    my $zaak = $c->stash->{zaak};

    my $zaaktype_kenmerk = $c->model('DB::ZaaktypeKenmerken')->search({
        bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
        zaaktype_node_id => $zaak->get_column('zaaktype_node_id')
    })->first;

    die "zaaktype_kenmerk not found" unless $zaaktype_kenmerk;

    $zaak->files->remove_unaccepted_case_documents({zaaktype_kenmerken_id => $zaaktype_kenmerk->id});

    $c->stash->{veldoptie_type}  = $zaaktype_kenmerk->type;
    $c->stash->{veldoptie_value} = $zaak->file_field_documents($bibliotheek_kenmerken_id);

    $c->stash->{template} = 'widgets/general/veldoptie_view.tt';
    $c->stash->{nowrapper} = 1;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


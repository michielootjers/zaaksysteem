package Zaaksysteem::Controller::Zaak::Destroy;

use strict;
use warnings;
use Data::Dumper;
use DateTime;

use Scalar::Util qw/blessed/;
use parent 'Catalyst::Controller';

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

=head2 bulk_update_destroy

there are three outs in this sub. it's tricky to separate these in different subs
and still take advantage of the listaction js and validation system.
1: validation. returns JSON
2: destroy. return JSON
3: show form + warnings/errors. Returns HTML

=cut

define_profile bulk_update_destroy => (
    required => [qw/selection selected_case_ids/],
    optional => [qw/commit confirm_destroy do_validation/],
    constraint_methods => {
        selection => qr/^selected_cases$/,
        selected_case_ids => sub {
            my $value = shift;
            my @case_ids = split /,/, $value;
            my $fault = grep { $_ !~ m/^\d+$/ } @case_ids;
            return !$fault;
        }
    },
);
sub bulk_update_destroy : Chained('/') : PathPart('bulk/update/destroy') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin', 'beheerder');

    my $params = assert_profile($c->req->params)->valid;

    my @case_ids = sort { $a <=> $b } split /,/, $params->{selected_case_ids};
    my $resultset = $c->model('DB::Zaak')->search({ 'me.id' => \@case_ids });

    # $params will contain the user response made in the dialog. the
    # warnings need to be answered with a confirm.
    # a confirm will result in confirm_destroy=on in the $params hashref:
    my $destroy_report = $resultset->check_destroy($params);
    $c->stash->{destroy_report} = $destroy_report;

    if ($destroy_report->has_errors) {
        $c->stash->{message} = 'Selectie kan niet worden vernietigd omdat er fouten zijn geconstateerd.';
    } elsif ($destroy_report->has_warnings && !$params->{confirm_destroy}) {
        $c->stash->{need_confirmation} = 1;
        $c->stash->{message} .= 'Er zijn waarschuwingen, bevestig de wijzigingen om door te gaan.'
    }

    if ($params->{do_validation}) {
        if (my $dv = $c->zvalidate) {

            my @valid = grep { $_ ne 'selection' } $dv->valid;
            my $json = {
                success     => $dv->success,
                missing     => [ $dv->missing ],
                invalid     => [ $dv->invalid ],
                unknown     => [ $dv->unknown ],
                valid       => [ @valid ],
                msgs        => $dv->msgs,
            };

            if ($c->stash->{message}) {
                $json->{msgs}->{selection_extra} = $c->stash->{message};
                push @{$json->{invalid}}, 'error';
                $json->{success} = 0;
                $json->{msgs}->{error} = $c->stash->{message};
            }
            $c->zcvalidate($json);
        }
        $c->detach;
    }

    $c->detach('destroy', [$resultset]) if !$c->stash->{message} && $params->{commit};

    $c->stash->{case_ids} = \@case_ids;
    $c->stash->{action} = 'destroy';
    $c->stash->{selection} = $params->{selection};
    $c->stash->{selected_case_ids} = join ",", @case_ids;
    $c->stash->{template} = 'zaak/widgets/management.tt';
    $c->stash->{nowrapper} = 1;
}


sub destroy : Private {
    my ($self, $c, $resultset) = @_;

    # actual deletion is permitted if there are no errors,
    # and no warnings unless they are confirmed.
    $resultset->destroy_cases;

    $c->stash->{ json }{ success } = 1;
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Controller::Zaak::Mail;

use strict;
use warnings;
use Data::Dumper;
use parent 'Catalyst::Controller';

use Email::MIME;
use Email::Valid;
use IO::All;

use Zaaksysteem::ZTT;
use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
/;


=head2 send_automatic_create_notifications

Send notifications that are on automatic send or
that are selected using the 'schedule_mail' rule.

Notifications are identified by their index in the phase. 
It would be preferably to identify them by a systemwide unique id, 
but this will require tricky changes in ZaaktypeBeheer rule management.

So first a list with the notifications and their settings for this phase is obtained:

Index   |   Notification  | Automatic   |  Schedule_by_rule  |  Action
1               Object         1                   0            Send now
2               Object         1                   1            Schedule
3               Object         0                   1            Schedule
4               Object         0                   0            -
..              ..              ..

Then the rules for the phase are executed and the results are added to this table
Then based on the settings for 'Automatic' and 'Schedule_by_rule' are compared and a 
decision is made. A rule takes precedence over Automatic behaviour. It is not recommended
to use these settings simultaneously, however it will take considerable work in the 
Zaaktypebeheer interface to inform the user about this pitfall.

=cut

sub send_automatic_create_notifications : Private {
    my ($self, $c) = @_;

    my $zaak = $c->stash->{zaak};

    my $registration_phase = $zaak->zaaktype_node_id->zaaktype_statussen->search({
        status => 1
    })->first;

    my $notifications = $registration_phase->notifications();

    my $scheduled_mails = $c->stash->{zaak}->reschedule_phase_notifications({
        status          => 1,
        notifications   => $notifications,
    });

    my $rules = $zaak->execute_rules({ status => 1 });

    my %transition_notifications;

    if(exists $rules->{ schedule_mail }) {
        for my $index (keys %{ $rules->{ schedule_mail } }) {
            next unless($rules->{ schedule_mail }{ $index }{ datum_bibliotheek_kenmerken_id } eq 'phase_transition');

            $transition_notifications{ $index }++;
        }
    }

    my $notification_index = 0;

    # this query has already been ran above
    $notifications->reset();

    while(my $notification = $notifications->next()) {
        $notification_index++;

        next if $scheduled_mails->{$notification_index};
        if($notification->automatic || $transition_notifications{ $notification_index }) {
            my $prepared_notification = $c->stash->{zaak}->prepare_notification({
                recipient_type  => $notification->rcpt,
                behandelaar     => $notification->behandelaar, 
                email           => $notification->email,
                context         => $c,
                body            => $notification->bibliotheek_notificaties_id->message,
                subject         => $notification->bibliotheek_notificaties_id->subject,
                # attachments     => $notification->bibliotheek_notificaties_id->email_files,
            });

            $c->forward("send", [$prepared_notification]);
        }
    }
}




sub send : Private {
    my ($self, $c, $notification, $opts) = @_;
    $opts ||= {};

    # 'to' can be undefined, as long as one of these is set as the type
    if (!$notification->{to} && $notification->{recipient_type} =~ qr/(behandelaar|coordinator|aanvrager)/) {
        my $obj_str = sprintf "%s_object", $notification->{recipient_type};
        $notification->{to} = $c->stash->{zaak}->$obj_str->email;
    }

    # die "need body"     unless $notification->{body};
    # die "need subject"  unless $notification->{subject};
    # die "need to"       unless $notification->{to};

    my $body    = $self->parse_special_vars($c, $notification->{body});
    my $subject = $self->parse_special_vars($c, $notification->{subject});

    my $cd_attachments = $notification->{case_document_attachments};

    $c->stash->{email} = {
        from    => $notification->{from} || $c->config->{gemeente}->{zaak_email},
        to      => $notification->{to},
        subject => $subject,
        body    => $body,
    };

    $c->log->debug('mailstash: ' . Dumper($c->stash->{email}));

    if ($body && $subject) {
        $c->model('DB')->txn_do(sub {

            if (scalar keys @{ $c->error }) {
                die(
                    'Cannot unset error for sending mail: '
                    . Dumper($c->error)
                );
            }

            $c->error(0);

            my %add_args = (
                zaak_id      => $c->stash->{zaak}->id,
                zaakstatus   => $c->stash->{'zaak'}->milestone,
                filename     => $notification->{to},
                documenttype => 'mail',
                category     => '',
                subject      => $subject,
                message      => $body,
                rcpt         => $notification->{to},
            );

            if ($c->user_exists) {
                $add_args{betrokkene_id} = 'betrokkene-medewerker-'
                    . $c->user->uidnumber;
            } else {
                $add_args{betrokkene_id} =
                    $c->stash->{zaak}->aanvrager_object->rt_setup_identifier;
            }

            $c->log->debug('Going to add mail as document to zaak: '
                . Dumper(\%add_args)
            );

            # Handle any possible case document attachments by splitting the mail up in parts 
            if ($cd_attachments) {
                # When using parts in emails, the original body is ignored, we place it in the first part 
                push @{ $c->{stash}->{email}{parts} }, Email::MIME->create(
                    attributes => {
                        charset => 'utf-8',
                    },
                    body => $c->stash->{email}->{body},
                );
            }

            my @contactmoment_attachments;
            for my $a (@{ $cd_attachments }) {
                next if (!$a->{selected});
                
                my $files = $c->model('DB::File')->search(
                    {
                        'case_documents.case_document_id' => $a->{case_document_ids},
                        'me.case_id' => $c->stash->{zaak}->id,
                    },
                    {
                        join => {case_documents =>'file_id'},
                    },
                )->active_rs;

                while (my $f = $files->next) {
                    # Create Email::MIME parts 
                    my $part = Email::MIME->create( 
                        attributes => { 
                            filename     => $f->name . $f->extension,
                            content_type => $f->filestore->mimetype, 
                            encoding     => 'base64', 
                            disposition  => 'attachment', 
                            name         => $f->name . $f->extension,
                        }, 
                        body => io($f->filestore->get_path), 
                    );
                                  
                    push @{ $c->{stash}->{email}{parts} }, $part; 
                    push @contactmoment_attachments, {
                        filename => $f->filename,
                        file_id  => $f->id,
                        email_filename => $a->{name}.$f->extension,
                    };
                }
            }

            $c->forward( $c->view('Email') );

            if ( scalar( @{ $c->error } ) ) {
                my $msg = 'Errors while sending e-mail notifications, no e-mail address?';

                unless ($opts->{no_messages}) {
                    $c->push_flash_message($msg);
                }

                $c->log->error($msg . join(',', @{ $c->error }));

                $c->error(0);
            } else {
                my $result = $c->model('DB::Contactmoment')->contactmoment_create({
                    type       => 'email',
                    subject_id => $add_args{betrokkene_id},
                    created_by => $add_args{betrokkene_id},
                    medium     => 'balie',
                    case_id    => $c->stash->{zaak}->id,
                    email      => {
                       %{ $c->stash->{email} },
                       recipient  => $notification->{to},
                       subject    => $subject,
                       attachments => \@contactmoment_attachments,
                    }
                });
            }
        });
    } else {
        $c->log->error(
            'C:Z:Mail: Body or Onderwerp empty?' .
            (
                $c->stash->{zaak}
                    ? ' Zaak: ' . $c->stash->{zaak}->nr
                    : ''
            )
        );
    }

    if ($@) {
        warn 'Could not send email: ' . $@;
        return;
    }

    return 1;
}

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaak::Mail in Zaak::Mail.');
}

sub preview : Private {
    my ($self, $c, $zaak) = @_;

    $c->stash->{mailconcept} = $c->view('TT')->render(
        $c,
        'tpl/zaak_v1/nl_NL/email/status_next.tt',
        {
            nowrapper => 1,
            %{ $c->stash }
        }
    );
}

sub registratie : Private {
    my ($self, $c) = @_;

    eval {
        $c->stash->{mailconcept} = $c->view('TT')->render(
            $c,
            'tpl/zaak_v1/nl_NL/email/registratie.tt',
            {
                nowrapper => 1,
                %{ $c->stash }
            }
        );

        $c->forward('aanvrager', [
            'Uw zaak is geregistreerd bij de ' .  $c->config->{gemeente}->{naam_lang}
        ]);
    };

    if ($@) {
        $c->log->debug('Something went wrong by sending this email: '
            . $@
        );
    }
}



sub aanvrager : Local {
    my ( $self, $c, $subject ) = @_;

    my $body    = $c->stash->{mailconcept} || $c->req->params->{'mailconcept'};
    if (!$subject) {
        $subject = $c->req->params->{'mailsubject'} || 'Status gewijzigd';
    }

    return unless (
        $body &&
        $c->stash->{zaak} &&
        $c->stash->{zaak}->kenmerk->aanvrager_email
    );

    eval {

        $body = $self->parse_special_vars($c, $body);

        $c->stash->{email} = {
            from    => $c->config->{gemeente}->{zaak_email},
            to      => $c->stash->{zaak}->kenmerk->aanvrager_email,
            subject => '[' . uc( $c->config->{gemeente}->{naam_kort}) . ' Zaak #' . $c->stash->{zaak}->nr . '] ' . $subject ,
            body    => $body,
        };

        $c->forward( $c->view('Email') );
    };

    if (!$@) {
        # Record email
        my %add_args = (
            zaakstatus   => $c->stash->{'zaak'}->kenmerk->status,
            filename     => $c->stash->{zaak}->kenmerk->aanvrager_email,
            documenttype => 'mail',
            category     => '',
            subject      => '[' . uc( $c->config->{gemeente}->{naam_kort}) . ' Zaak #' . $c->stash->{zaak}->nr . '] ' . $subject,
            message      => $body,
            rcpt         => $c->stash->{zaak}->kenmerk->aanvrager_email,
        );

        $c->stash->{zaak}->documents->add(
            \%add_args
        )
   } else {
       $c->log->debug(
           'Something went wrong sending email: ' . $@
        );
    }
}

sub parse_special_vars {
    my ( $self, $c, $body ) = @_;

    # Why this?
    if ((ref($body) eq 'ARRAY' && !@$body) || !$body) {
        return $body;
    }

    # Make sure to add the magic_strings hash for PIP feedback. (Amongst things)
    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context($c->stash->{ magic_strings }) if exists $c->stash->{ magic_strings };
    $ztt->add_context($c->stash->{ zaak });

    return $ztt->process_template($body)->string;
}


sub document : Private {
    my ( $self, $c, $body, $onderwerp ) = @_;

    $c->stash->{email} = {
        from    => $c->config->{gemeente}->{zaak_email},
        to      => $self->parse_special_vars($c, $c->stash->{rcpt}),
        subject => $onderwerp,
        body    => $body,
    };

    $c->forward( $c->view('Email') );
}


sub prepare_email {
    my ($self, $c, $params) = @_;

    my $arguments = {
        recipient_type  => $params->{recipient_type},
        #behandelaar     => $params->{notificatie_ztc_aanvrager_id}, 
        behandelaar     => $params->{medewerker_betrokkene_id}, 
        email           => $params->{notificatie_email},
        context         => $c,
        body            => $params->{notificatie_bericht},
        subject         => $params->{notificatie_onderwerp},
    };

    if(
        $params->{zaaktype_notificatie_id} && 
        $params->{mailtype} eq 'bibliotheek_notificatie'
    ) {
        my $zaaktype_notificatie = $c->model('DB::ZaaktypeNotificatie')->find($params->{zaaktype_notificatie_id});
        $arguments->{body}        = $zaaktype_notificatie->bibliotheek_notificaties_id->message;
        $arguments->{subject}     = $zaaktype_notificatie->bibliotheek_notificaties_id->subject;

        $arguments->{case_document_attachments} = $zaaktype_notificatie->case_document_attachments;
    }

    return $c->stash->{zaak}->prepare_notification($arguments);
}

sub set_error_missing {
    my ($self, $dv, $field) = @_;

    $dv->{success} = 0;
    $dv->{invalid} //= [];
    push @{ $dv->{missing} }, $field;
}

sub set_error_invalid {
    my ($self, $dv, $field) = @_;

    $dv->{success} = 0;
    $dv->{invalid} //= [];
    push @{ $dv->{invalid} }, $field;
}

=head2 send_email

From zaakbehandeling, send email

=cut 

sub send_email : Chained('/zaak/base') : PathPart('send_email') : Args() {
    my ($self, $c, $zaaktype_notificatie_id) = @_;

    my $params = $c->req->params;

    $c->stash->{nowrapper} = 1;

    if($params->{do_validation}) {
        my $dv = {success => 1, msgs => {
            notificatie_email => 'Geef een valide e-mailadres of een magic string die daarnaar verwijst'
        }};

        if($params->{recipient_type} eq 'overig') {
            if($params->{notificatie_email}) {
                eval {
                    my $prepared_notification = $self->prepare_email($c, $params);
                    my $to = $prepared_notification->{to};
                    foreach my $partial (split /,|;/, $to) {
                        die "invalid e-mail address: $partial" unless Email::Valid->address($partial);
                    }
                };

                if($@) {
                    if ($@ =~ /body/i) {
                        $self->set_error_invalid($dv, 'notificatie_body');
                        $dv->{msgs}->{notificatie_body} = 'E-mailsjabloon is leeg';
                    }

                    if ($@ =~ /subject/i) {
                        $self->set_error_invalid($dv, 'notificatie_subject');
                    }

                    if ($@ =~ /invalid e-mail/) {
                        $self->set_error_invalid($dv, 'notificatie_email');
                    }

                    $c->log->error('Error e-mail:' . $@);
                }
            } else {
                $self->set_error_missing($dv, 'notificatie_email');
            }

        } elsif($params->{recipient_type} eq 'medewerker') {
            $self->set_error_missing($dv, 'medewerker_betrokkene_id') unless $params->{medewerker_betrokkene_id};
        }
        if($params->{mailtype} eq 'specific_mail') {
            $self->set_error_missing($dv, 'notificatie_onderwerp') unless $params->{notificatie_onderwerp};
            $self->set_error_missing($dv, 'notificatie_bericht')   unless $params->{notificatie_bericht};
        }
        $c->zcvalidate($dv);
        $c->detach();

    } elsif($params->{update}) {

        my $prepared_notification = $self->prepare_email($c, $params);

        if (
            $c->forward("send", [$prepared_notification])
        ) {
            $c->push_flash_message('E-mail verstuurd naar ' . $prepared_notification->{to});
        } else {
            $c->push_flash_message(
                'Er is iets mis gegaan bij het versturen'
                . ' van e-mail naar ' . $prepared_notification->{to}
            );
        }

        $c->res->redirect(
            $c->uri_for("/zaak/" . $c->stash->{zaak}->id)
        );

    } else {

        if($zaaktype_notificatie_id) {
            $c->stash->{zaaktype_notificatie} = $c->model('DB::ZaaktypeNotificatie')->find($zaaktype_notificatie_id);
            $c->stash->{template}  = 'zaak/send_casetype_email.tt';
        } else {
            $c->stash->{template}  = 'zaak/send_email.tt';
        }
    }

}


sub download_email_pdf : Chained('/zaak/base') : PathPart('download_email_pdf') : Args(1) {
    my ($self, $c, $event_id) = @_;

    my $event = $c->model('DB::Logging')->find($event_id);
    my $event_data = JSON::from_json($event->event_data);

    my $name = $c->stash->{zaak}->id . " " . $event_data->{subject} . ".pdf";

    my $pdf = $event->export_as_pdf;

    $c->response->body($pdf);
    $c->res->headers->content_length(length $pdf);
    $c->res->headers->content_type('application/octet-stream');
    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');
    $c->res->header('Content-Disposition' => sprintf('attachment; filename="%s"', $name));
}



1;


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


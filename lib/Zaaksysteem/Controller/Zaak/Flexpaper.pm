package Zaaksysteem::Controller::Zaak::Flexpaper;

use Moose;
use Zaaksysteem::Exception;

BEGIN { extends 'Zaaksysteem::Controller'; }

=head2 flexpaper

Display flexpaper PDF reader

=cut

sub flexpaper : Chained('/zaak/base') : PathPart('flexpaper') : Args() {
    my ($self, $c, $file_id) = @_;

    # find the requested file, check if it's active
	$c->stash->{file} = $c->stash->{zaak}->files->search({
		id => $file_id
	})->active->first or throw('api/case/file', "file $file_id not available");

	$c->forward('annotations');
}


sub annotations : Private {
	my ($self, $c) = @_;

	my $file_id = $c->stash->{file}->id;

	my $rs = $c->model('DB::FileAnnotation')->search({file_id => $file_id});

	my @annotations = $c->model('DB::Config')->get('pdf_annotations_public') ?
		$rs->all :
		$rs->search({subject => 'betrokkene-medewerker-' . $c->user->uidnumber})->all;

    $c->stash->{file_id} = $file_id;
    $c->stash->{annotations} = [map { $_->properties } @annotations];
    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'flexpaper.tt';
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE
The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

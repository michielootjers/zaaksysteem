package Zaaksysteem::Controller::Zaak::Status;

use strict;
use warnings;
use Data::Dumper;
use parent 'Catalyst::Controller';

use Zaaksysteem::Exception;
use Try::Tiny;

sub index :Chained('/zaak/base') : PathPart('status'): Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/elements/status.tt';
}


sub _status_check_possibility : Private {
    my ( $self, $c ) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if (!$c->can_change) {
        $c->res->redirect(
            $c->uri_for(
                '/zaak/' . $c->stash->{zaak}->id
            )
        );
        $c->detach;
    }

    if ($c->stash->{zaak}->is_afgehandeld) {
        my $errmsg = 'Deze zaak is afgehandeld, '
            .'extra wijzigingen zijn niet meer mogelijk';

        $c->log->warn($errmsg);
        $c->push_flash_message($errmsg);

        $c->response->redirect(
            $c->uri_for('/zaak/' . $c->stash->{zaak}->nr)
        );

        $c->detach;
    }
}

sub status_base_old : Chained('/zaak/base') : PathPart('statusold'): CaptureArgs(0) {
    my ( $self, $c ) = @_;

    ### Little bit dirty, but we always check first status too when we check
    ### the seconde fase. Because we are able to skip required kenmerken in
    ### the first fase
    my ($regels_result, $incompleet_fase);
    if ($c->stash->{zaak}->milestone == 1) {
        $incompleet_fase = $c->stash->{zaak}->huidige_fase->status;
        $regels_result = $c->stash->{zaak}->phase_fields_complete({
            phase => $c->stash->{zaak}->huidige_fase,
        });
    }

    my $case = $c->stash->{zaak};
    ### Als de registratiefase niet van toepassing is OF deze had een succes
    ### status, check de huidige fase
    if (!$regels_result || $regels_result->{succes}) {
        $incompleet_fase = $case->volgende_fase->status
            if $case->volgende_fase;

            if($case->volgende_fase) {
            $regels_result = $case->phase_fields_complete({
                phase   => $case->volgende_fase
            });
        }
    }

    unless ($regels_result && $regels_result->{succes}) {
        my $errmsg = 'Volgende fase niet mogelijk: ';

        if ($regels_result->{pauze}) {
            $errmsg .= 'aanvraag gepauzeerd "'
                . $regels_result->{pauze} . '"';
        } elsif ($regels_result->{required}) {
            $errmsg .= 'niet alle verplichte kenmerken zijn ingevuld';
        }

        $c->stash->{status_kenmerken_incompleet}        = $errmsg;
        $c->stash->{status_kenmerken_fase_incompleet}   = $incompleet_fase;
        $c->stash->{status_next_stop}                   = 1;
    }

    $c->forward('_status_check_possibility');
}

sub status_base : Chained('/zaak/base') : PathPart('status'): CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->forward('_status_check_possibility');
}


sub can_advance : Chained('status_base') : PathPart('can_advance') {
    my ($self, $c) = @_;

    my $case = $c->stash->{zaak};

    my $can_advance = $case->can_volgende_fase->can_advance();
    $c->stash->{json} = $can_advance;
    $c->detach('Zaaksysteem::View::JSONlegacy');
}



sub next :Chained('status_base') : PathPart('next'): Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->is_xhr &&
        $c->req->params->{do_validation}
    ) {
        $c->zcvalidate({ success => 1});
        $c->detach;
    }

    $c->forward('load_zaaktype_kenmerken');

    my $case = $c->stash->{zaak};

    $c->stash->{can_parent_advance} = $case->pid &&
        $case->pid->zaak_subcases->find({
            relation_zaak_id        => $case->id,
            parent_advance_results  => $case->resultaat,
        });

    ### We need regels_result, how else would we know the rules for berichten?!
    if (!defined($c->stash->{regels_result})) {
        $c->stash->{regels_result}  = $c
            ->stash
            ->{zaak}
            ->execute_rules(
                {
                    status => ($c->stash->{zaak}->volgende_fase->status)
                }
        );
    }

    $c->stash->{nowrapper} = $c->req->params->{nowrapper};
    $c->stash->{template} = 'zaak/status/next.tt';

    ### NO Pos
    $c->detach unless $c->req->params->{update};

    ### CHECKS
    unless (
        $c->stash->{zaak}->can_volgende_fase->can_advance()
    ) {
        my $errmsg = 'Checklist, documenten of kenmerken niet compleet, '
            .'kan status niet verhogen.';

        $c->log->warn           ( $errmsg );
        $c->push_flash_message  ( $errmsg );
        $c->response->redirect  ( $c->uri_for('/' . $c->req->path));
        $c->detach;
    }

    $c->model("DB")->txn_do(sub {

        $c->forward('advance_old');

        # advance parent case
        if(
            $c->req->params->{parent_advance} &&
            $case->is_afgehandeld &&
            $case->pid &&
            $case->pid->zaak_subcases->find({
                relation_zaak_id        => $case->id,
                parent_advance_results  => $case->resultaat,
            })
        ) {

            $case->pid->advance({
                context => $c
            });
        }
    });
    if($@) {
        die "problem: $@";
    }

    $c->response->redirect(
        $c->stash->{response_uri}
    );

    return;

}


=head2 advance


=cut

sub advance :Chained('status_base') : PathPart('advance'): Args(0) {
    my ($self, $c) = @_;

    my $case = $c->stash->{zaak};

    unless(
        ($case->behandelaar && $case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) ||
        ($case->coordinator && $case->coordinator->gegevens_magazijn_id eq $c->user->uidnumber) ||
        $c->check_any_zaak_permission('zaak_beheer')
    ) {
        $c->push_flash_message('U heeft geen rechten om de fase van de zaak aan te passen.');

        $c->stash->{ zapi } = [{
            success => 0,
            redirect => $c->uri_for('/zaak/' . $c->stash->{ zaak }->id)->as_string,
            advance_result => {
                not_allowed => 1
            }
        }];

        $c->detach($c->view('ZAPI'));
    }

    try {
        # stuff goes awry, we wanna have a plan B
        $c->model('DB')->txn_do(sub {
            my $result = $case->advance({ context => $c });

            map { $c->push_flash_message($_) } @{ $result->{ flash_messages } };

            my $redirect;
            if($result->{ redirect_to_dashboard }) {
                $redirect = $c->uri_for('/');
            } else {
                $redirect = $c->uri_for('/zaak/' . $c->stash->{zaak}->nr);
            }

            $c->stash->{zapi} = [
                {
                    'success'           => 1,
                    'redirect'          => $redirect->as_string,
                    'advance_result'    => ''
                }
            ];
        });
    } catch {
        $c->stash->{zapi} = [
            {
                'success'           => 0,
                'redirect'          => '',
                'advance_result'    => {
                    type    => $_->type,
                    message => $_->message,
                    object  => $_->object
                }
            }
        ];
    };

    $c->detach($c->view('ZAPI'));
}

sub advance_waiting :Chained('/') : PathPart('zaak/status/advance_waiting'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{template} = 'zaak/status/advancewaiting.tt';
    $c->stash->{nowrapper} = 1;
}

sub advance_result_error :Chained('status_base') : PathPart('advance_error'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{advance_result}     = $c->req->params;
    $c->stash->{template} = 'zaak/status/advanceresult.tt';
    $c->stash->{nowrapper} = 1;
}


sub advance_old : Private {
    my ($self, $c) = @_;

    ### SET FASE
    if (!$c->stash->{zaak}->set_volgende_fase) {
        my $errmsg = 'Fase werd niet verhoogd, contact systeembeheer.';

        $c->log->error          ( $errmsg );
        $c->push_flash_message  ( $errmsg );
        $c->response->redirect  (
            $c->uri_for('/zaak/' . $c->stash->{zaak}->nr)
        );
        $c->detach;
    }

    ## Start vervolg/deelzaken
    my $open_zaak = $self->start_subzaken($c);


    $c->forward('_handle_toewijzing');

    # voor regels uit
    my $milestone = $c->stash->{zaak}->{milestone};

    my $rules_result = $c->stash->{regels_result} = $c->stash->{zaak}->execute_rules({status => $milestone});

    if(my $toewijzing = $rules_result->{toewijzing}) {
        $c->stash->{zaak}->wijzig_route({
            route_ou    => $toewijzing->{ou_id},
            route_role  => $toewijzing->{role_id},
        });
    }

    ### XXX TODO ZS2 kenmerk resultaat
    if (defined($c->req->params->{system_kenmerk_resultaat})) {
        $c->stash->{zaak}->kenmerk->resultaat(
            $c->req->params->{system_kenmerk_resultaat}
        );
    }

    $c->push_flash_message('Fase van zaak succesvol verhoogd');


    $c->forward('notificatie_send');
    $c->forward('/zaak/_create_zaak_handle_sjablonen');

    my $response_uri;
    if (exists($c->stash->{behandelaar_changed})) {
        $c->push_flash_message('Fase van zaak succesvol verhoogd en behandelaar gewijzigd.');
        $response_uri = $c->uri_for('/');
    } elsif (
        $c->stash->{zaak}->is_afhandel_fase
    ) {
        if ($open_zaak) {
            $c->push_flash_message('Fase van zaak succesvol afgehandeld'
                . ' en vervolgzaak geopend.'
            );
            ### Override any of the above when we want to start a subzaak
            ### immediatly
            $response_uri = '/zaak/' . $open_zaak->nr;
        } else {
            $c->push_flash_message('Fase van zaak succesvol afgehandeld.');
            $response_uri = $c->uri_for('/');
        }
    } else {
        if ($c->stash->{zaak}->behandelaar) {
            $response_uri = $c->uri_for('/zaak/' . (
                    $open_zaak ? $open_zaak->nr :
                    $c->stash->{zaak}->nr
                )
            );
        } else {
            $response_uri = $c->uri_for('/');
        }
    }
    $c->stash->{response_uri} = $response_uri;

    $c->model('Bibliotheek::Sjablonen')->touch_zaak($c->stash->{zaak});
}


sub _handle_toewijzing : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    
    my $toewijzing_type = $params->{toewijzing_type} || '';

    if ($toewijzing_type eq 'route') {
        $c->log->debug('Wijzig toewijzing naar: ' .
            $params->{ou_id} . ':' . $params->{role_id}
        );
        $c->stash->{zaak}->wijzig_route({
            route_ou    => $params->{ou_id},
            route_role  => $params->{role_id},
        });
    } elsif ($toewijzing_type eq 'behandelaar') {
        if ($params->{ztc_behandelaar_id}) {
            $c->stash->{zaak}->set_behandelaar(
                $params->{ztc_behandelaar_id}
            );

            $c->stash->{zaak}->wijzig_status({status => 'new'});

            $c->stash->{behandelaar_changed} = 1;
        }
    }

    return 1;
}

sub notificatie_send : Private {
    my ($self, $c) = @_;

    ### Make sure mail is fully loaded
    $c->forward('load_notificatie_definitie', [ { huidige_fase => 1 } ]);   
    #$c->forward('notificatie_definitie');

    my $params = $c->req->params();

    $c->log->debug(
        'Z::S->notificatie_send: sending notifications, params: '.
        #Dumper ($params) . 
        'session: ' . Dumper $self->_status_tmp($c)->{notificaties}
    );

    ### Now have fun
    while (
        my ($uniqueidr, $notificatie) = each %{
            $self->_status_tmp($c)->{notificaties}
        }
    ) {
        my ($notificatie_index) = $uniqueidr =~ m|(\d+)$|;

        $uniqueidr =~ s/_id_/_run_/g;

        # look if checkbox is checked
        next unless $params->{$uniqueidr};

        $c->log->debug(
            'Z::S->notificatie_send: sending onderwerp: '
            . $notificatie->{onderwerp}
        );

        if(my $send_date = $params->{'status_notificatie_send_date_' . $notificatie_index}) {

            my $send_date_dt;
            if($params->{'status_notificatie_schedule_test_' . $notificatie_index}) {
                # tis epoch
                $send_date_dt = DateTime->from_epoch(epoch => $send_date);

            } else {
                my ($day, $month, $year) = $send_date =~ m|(\d+)-(\d+)-(\d+)|is;

                $send_date_dt = DateTime->new(
                    year    => $year,
                    month   => $month,
                    day     => $day,
                    hour    => 9
                );
            }

            $c->log->debug("Zaak::Status: queuing email for future delivery:" . Dumper $notificatie);

            $c->model('DB::ScheduledJobs')->create_zaak_notificatie({
                bibliotheek_notificaties_id => $notificatie->{bibliotheek_notificaties_id},
                scheduled_for   => $send_date_dt,
                recipient_type  => $notificatie->{rcpt},
                behandelaar     => $notificatie->{ztc_aanvrager_id},
                email           => $notificatie->{email},
                zaak_id         => $c->stash->{zaak}->id,
            });

        } else {

            my $prepared_notification = $c->stash->{zaak}->prepare_notification({
                recipient_type  => $notificatie->{rcpt},
                behandelaar     => $notificatie->{ztc_aanvrager_id}, 
                email           => $notificatie->{email},
                context         => $c,
                body            => $notificatie->{bericht},
                subject         => $notificatie->{onderwerp},
            });
            $c->log->debug("Zaak::Status: sending mail directly:" . Dumper $prepared_notification);

            $c->forward("/zaak/mail/send", [$prepared_notification]);
        }
    }
}




sub start_subzaken : Private {
    my ($self, $c) = @_;
    my (%subzaken_args, $open_zaak);

    ### Of course some authentication
    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    ### And make sure we got all the information we need
    $c->forward('load_zaaktype_kenmerken', [ { huidige_fase => 1 } ]);

    $c->log->debug('1. Start subzaak');

    $subzaken_args{ $_ } = $c->req->params->{ $_ } for
        grep(/status_zaaktype_.*?_\d+$/, keys %{ $c->req->params });

    $c->log->debug('1. Start subzaak, args:' . Dumper \%subzaken_args);

    my @counts = ();
    foreach my $key (keys %subzaken_args) {
        if($key =~ m|status_zaaktype_id_(\d+)|) {
            my $count = $1;
            push @counts, $count;
        }
    }

    foreach my $count (sort {$a <=> $b} @counts) {

        if (!$c->req->params->{'status_zaaktype_id_' . $count}) { next; }

        ### Is vinkje 'Starten' aangevinkt?
        if (!$c->req->params->{'status_zaaktype_run_' . $count}) { next; }

        ### Now let's go...
        my $aanvrager_type = $self->_status_tmp($c)->{zaaktype_kenmerken}->{
            'status_zaaktype_id_' . $count
        }->{eigenaar_type};

        die "somehow we can still get here";
        my %zaakopts = (
            'zaaktype_id'  =>
                $c->req->params->{'status_zaaktype_id_' . $count},
            'add_days'          =>
                $c->req->params->{'status_zaaktype_start_' . $count},
            'actie_kopieren_kenmerken' =>
                (
                    $c->req->params->{
                        'status_zaaktype_kopieren_kenmerken_' .  $count
                    } ||
                    $self->_status_tmp($c)->{zaaktype_kenmerken}->{
                        'status_zaaktype_id_' . $count
                    }->{kopieren_kenmerken}
                ),
            'role_id' => (
                    $self->_status_tmp($c)->{zaaktype_kenmerken}->{
                        'status_zaaktype_id_' . $count
                    }->{role_id}
                ),
            'ou_id' => (
                    $self->_status_tmp($c)->{zaaktype_kenmerken}->{
                        'status_zaaktype_id_' . $count
                    }->{ou_id}
                    
                ),
            'aanvrager_type'    => ($aanvrager_type ?
                $aanvrager_type : 'aanvrager'
            ),
            'actie_automatisch_behandelen'    => (
                $self->_status_tmp($c)->{zaaktype_kenmerken}->{
                    'status_zaaktype_id_' . $count
                }->{automatisch_behandelen}
            ),

        );

        $zaakopts{type_zaak} =
            $c->req->params->{'status_zaaktype_deelrelatie_' . $count};

        # Logging for subzaak:
        $c->log->debug(
            'C::Zaak::Status->start_subzaken: ' .
            Dumper(\%zaakopts)
        );

        $c->log->debug('5. Daadwerkelijk starten' .  Dumper \%zaakopts);

        my $extra_zaak = $c->model('DB::Zaak')->create_relatie(
            $c->stash->{zaak},
            %zaakopts
        );

        $c->log->debug(
            'start_subzaken req: ' . $c->req->params->{'status_zaaktype_open'} . "\n" .
            'zaaktype_id: ' . $zaakopts{zaaktype_id} .
            'extra epoch: ' . $extra_zaak->registratiedatum->epoch .
            'nu epoch: ' . DateTime->now()->epoch
        );

        if (
            $c->req->params->{'status_zaaktype_open'} eq
                $zaakopts{zaaktype_id} &&
            $extra_zaak->registratiedatum->epoch < DateTime->now()->epoch
        ) {
            $c->log->debug('OOOPEN: ' . $extra_zaak->id);
            $open_zaak = $extra_zaak;
        }

        ### Make sure we do not have a 'create_zaak' session open
        my $current_zaak    = $c->stash->{zaak};
        $c->stash->{zaak}   = $extra_zaak;

        delete($c->session->{_zaak_create});
        $c->forward('/zaak/handle_fase_acties');

        $c->stash->{zaak}   = $current_zaak;
    }

    return $open_zaak;
}

sub load_zaaktype_kenmerken : Private {
    my ($self, $c, $opt) = @_;

    my $relaties;
    if ($opt && UNIVERSAL::isa($opt, 'HASH') && $opt->{huidige_fase}) {
        $relaties =
            $c->stash->{zaak}->huidige_fase->zaaktype_relaties;
    } else {
        $relaties =
            $c->stash->{zaak}->volgende_fase->zaaktype_relaties;
    }

    $self->_status_tmp($c, {});
    $self->_status_tmp($c)->{zaaktype_kenmerken} = {};

    my $count = 0;
    while (my $relatie = $relaties->next) {
        $self->_status_tmp($c)->{zaaktype_kenmerken}->{
            'status_zaaktype_id_' . ++$count
        } = { 
            $relatie->get_columns
        };
    }

}


sub zaaktype_kenmerken : Chained('/zaak/base'): PathPart('status/next/zaaktype_kenmerken'): Args(0) {
    my ($self, $c) = @_;

    my $params      = $c->req->params();
    my $destination = $params->{destination};


    if ($params->{update}) {
    
        $self->_status_tmp($c)->{zaaktype_kenmerken}
            ->{ $destination } = {
                'eigenaar'               => $params->{relaties_eigenaar},
                'eigenaar_type'          => $params->{relaties_eigenaar_type},
                'kopieren_kenmerken'     => $params->{relaties_kopieren_kenmerken},
                'status'                 => $params->{relaties_status},
                'ou_id'                  => $params->{relaties_ou_id},
                'role_id'                => $params->{relaties_role_id},
                'automatisch_behandelen' => $params->{relaties_automatisch_behandelen},
                'required'               => $params->{relaties_required}
            };

        delete $params->{update};

        if($params->{json}) {
            $c->stash->{json} = 1;
            $c->detach('Zaaksysteem::View::JSONlegacy');
        } else {
            $c->res->redirect(
                $c->uri_for(
                    '/zaak/'.$c->stash->{zaak}->id. '/status/next/',
                    $params
                )
            );
        }
        $c->detach();
    }


    if (
        $destination &&
        $self->_status_tmp($c)->{zaaktype_kenmerken}->{ 
            $destination 
        }
    ) {
        $c->stash->{params} = $self->_status_tmp($c)
            ->{zaaktype_kenmerken}
            ->{
                $destination 
            };
    }

    $c->stash->{action}         = '/zaak/'. $c->stash->{zaak}->id . '/status/next/zaaktype_kenmerken';
    $c->stash->{destination}    = $destination;
    $c->stash->{class}          = 'ezra_zaaktype_json';
    $c->stash->{nowrapper}      = 1;
    $c->stash->{nextstatus}     = 1;
    $c->stash->{template}       = 'beheer/zaaktypen/milestones/edit_relatie.tt';
}

sub load_notificatie_definitie : Private {
    my ($self, $c, $opt) = @_;

    unless ($self->_status_tmp($c)->{notificaties}) {

        my $notificaties;
        if ($opt && UNIVERSAL::isa($opt, 'HASH') && $opt->{huidige_fase}) {
            $notificaties = $c->stash->{zaak}
                ->huidige_fase
                ->zaaktype_notificaties
                ->search;
        } else {
            $notificaties = $c->stash->{zaak}
                ->volgende_fase
                ->zaaktype_notificaties
                ->search;
        }

        $self->_status_tmp($c)->{notificaties}       = {};

        my $count = 0;
        while (my $notificatie = $notificaties->next) {

            $count++;
            unless(
                $notificatie->bibliotheek_notificaties_id &&
                $notificatie->bibliotheek_notificaties_id->id
            ) {
                die "need bibliotheek_notificatie here. run import script";
            }
            $c->log->debug("notificatie columns" . Dumper {$notificatie->get_columns()});
            my $status_notificatie = {
                rcpt            => $notificatie->rcpt,
                onderwerp       => $notificatie->bibliotheek_notificaties_id->subject,
                bericht         => $notificatie->bibliotheek_notificaties_id->message,
                bibliotheek_notificaties_id => $notificatie->bibliotheek_notificaties_id->id,
            };
    
            if ($notificatie->rcpt eq 'behandelaar') {
                $status_notificatie->{ztc_aanvrager_id} = $notificatie->behandelaar;
            } elsif ($notificatie->rcpt eq 'overig') {
                $status_notificatie->{email}        = $notificatie->email;
            }
            
            $self->_status_tmp($c)->{notificaties}->{'status_notificatie_id_' . $count} = 
                $status_notificatie;
        }
    }
}

sub notificatie_definitie : Chained('/zaak/base'): PathPart('status/next/notificatie_definitie'): Args() {
    my ($self, $c, $statusid) = @_;

    my $params = $c->req->params;    

    if ($params->{update}) {
        if($params->{send_now}) {

            foreach my $key (keys %$params) {
                my $value = $params->{$key};
                if($key =~ m|^notificatie_|) {
                    delete $params->{$key};
                    $key =~ s|^notificatie_||is;                
                    $params->{$key} = $value;
                }
            }
            $params->{rcpt_type}    = $params->{rcpt};
            $params->{rcpt}         = $params->{email};
            $params->{message}      = $params->{bericht};
            $params->{subject}      = $params->{onderwerp};
            $params->{documenttype} = 'mail'; 
            $c->res->redirect(
                $c->uri_for(
                    "/zaak/" . $c->stash->{zaak}->id . "/documents/0/add_mail",
                    $params
                )
            );
            $c->detach();        
        }

        $self->_status_tmp($c)->{notificaties}
            ->{ $c->req->params->{uniqueidr} } = {
                map {
                    my $label   = $_;
                    $label      =~ s/^notificatie_//g;
                    $label      => $c->req->params->{ $_ }
                } grep(/^notificatie_/, keys %{ $c->req->params })
            };

        $c->log->debug("session updated: " . Dumper $self->_status_tmp($c)->{notificaties});

        $c->res->body('OK');
        return;
    }
$c->log->debug("send_now3");

    $c->forward('load_notificatie_definitie');
    if (
        $self->_status_tmp($c)->{notificaties}
            ->{ $c->req->params->{uniqueidr} }
    ) {
        ### History should overwrite edit parameters
        $c->stash->{history} =
                $self->_status_tmp($c)->{notificaties}
                    ->{ $c->req->params->{uniqueidr} };
    }
$c->log->debug("send_now4");

    ### Behandelaar of overig
    if ($c->stash->{history}->{rcpt} eq 'behandelaar') {
        $self->_status_tmp($c)->{notificaties}
            ->{ $c->req->params->{uniqueidr} }->{ztc_aanvrager_id}
            = $self->_status_tmp($c)->{notificaties}
                ->{ $c->req->params->{uniqueidr} }->{email};

        if (
            ! $self->_status_tmp($c)->{notificaties}
                ->{ $c->req->params->{uniqueidr} }->{ztc_aanvrager} &&
            $self->_status_tmp($c)->{notificaties}
                ->{ $c->req->params->{uniqueidr} }->{ztc_aanvrager_id}
        ) {
            my $beh = $c->model('Betrokkene')->get(
                {},
                $self->_status_tmp($c)->{notificaties}
                    ->{ $c->req->params->{uniqueidr} }->{ztc_aanvrager_id}
            );

            if ($beh) {
                $self->_status_tmp($c)->{notificaties}
                    ->{ $c->req->params->{uniqueidr} }->{ztc_aanvrager}
                        = $beh->naam;
            }
        }
    }

    $c->stash->{popupaction} = $c->uri_for(
        '/zaak/' . $c->stash->{zaak}->nr .
        '/status/next/notificatie_definitie'
    );
    $c->stash->{nowrapper} = 1;
    $c->stash->{ZAAKSTATUS} = 1;
    $c->stash->{template}  = 'zaaktype/status/notificatie_definitie.tt';
}



sub _status_tmp {
    my ($self, $c, $new_value) = @_;

    my $zaak_id = $c->stash->{zaak}->id;

    return $c->session->{status_tmp}->{$zaak_id} = $new_value
        if($new_value);

    return $c->session->{status_tmp}->{$zaak_id} ||= {};
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


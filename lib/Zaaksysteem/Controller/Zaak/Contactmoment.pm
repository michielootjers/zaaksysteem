package Zaaksysteem::Controller::Zaak::Contactmoment;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Data::Dumper;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub base : JSON : Chained('/zaak/base') : PathPart('contactmoment') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

=head2 create

Create a new contact moment related to a case

=head3 URL

C</zaak/B<[zaak_id]>/contactmoment/create>

=head3 Request method

The request method must be POST for this call.

=head3 Parameters

=over 4 

=item contactkanaal

One of C<behandelaar>, C<balie>, C<telefoon>, C<post>, C<email> or C<webformulier>

=item content 

Plaintext note to store

=item ztc_aanvrager_id

Standard subject identifier in the format C<betrokkene-B<[type]>-B<[gmid]>>.

=back

=head3 Response

A single JSON hydrated contact moment instance

=head3 Events

=over 4

=item C<subject/contactmoment/create>

This event reflects the fact that a contact moment has been created on this case

=back

=cut

define_profile create => (
    required => [ qw[contactkanaal ztc_aanvrager_id content] ],
);

sub create : Chained('base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    unless($c->req->method eq 'POST') {
        throw('request/method', "Invalid request method, should be POST.");
    }

    $c->model('DB::Contactmoment')->contactmoment_create({
        type => 'note',
        subject_id => $c->req->param('ztc_aanvrager_id'),
        case_id => $c->stash->{ zaak }->id,
        created_by => $c->model('DB::Zaak')->current_user->betrokkene_identifier,
        medium => $c->req->param('contactkanaal'),
        message => $c->req->param('content')
    });

    $c->stash->{ json } = $c->stash->{ zaak }->logging->trigger('subject/contactmoment/create', {
        component => 'zaak',
        created_for => $c->req->param('ztc_aanvrager_id'),
        data => {
            case_id => $c->stash->{ zaak }->id,
            content => $c->req->param('content'),
            subject_id => $c->req->param('ztc_aanvrager_id'),
            contact_channel => $c->req->param('contactkanaal'),
        }
    });

    $c->detach('Zaaksysteem::View::JSON');
}

1;

package Zaaksysteem::Controller::Zaak::Acties;

use strict;
use warnings;
use Data::Dumper;
use DateTime;

use Scalar::Util qw/blessed/;
use parent 'Catalyst::Controller';

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;
use Zaaksysteem::Profile;

use constant BULK_UPDATE_SIMPLE_TABLE_CONFIG         => {
    'header'    => [
        {
            label   => 'Kenmerk',
            mapping => 'kenmerk_naam',
        },
        {
            label   => 'Waarde',
            mapping => 'kenmerk_value',
        },
    ],
    'options'   => {
        data_source         => '/bulk/update/kenmerken',
        row_identifier      => 'kenmerk_identifier',
        has_delete_button   => 1,
        add_class           => 'ezra_bulk_update_kenmerken_add',
        search_action       => '/beheer/bibliotheek/kenmerken/search',
        init                => 1,
        add                 => {
            label   => 'Kenmerk toevoegen',
            popup   => 1,
        },
        add_class           => 'ezra_bulk_update_kenmerken_add',
        dialog              => '#searchdialog',
    }
};



sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaak::Acties in Zaak::Acties.');
}

#### ACTIES VOOR OVERZICHT
{
    Zaaksysteem->register_profile(
        method  => 'verplaats',
        profile => {
            required => [ qw/
                betrokkene_type
                ztc_behandelaar_id
            /],
        }
    );

    sub verplaats : Chained('/zaak/base'): PathPart('actie/verplaats'): Args(0) {
        my ($self, $c) = @_;

        die('HIGHLY DEPRECATED, REMOVE AFTER 2.0 RELEASE');
        return unless $c->req->params->{betrokkene_type};

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

        ### Validation
        if (
            $c->req->is_xhr &&
            $c->req->params->{do_validation}
        ) {
            $c->zvalidate;
            $c->detach;
        }

        ### Post
        if (
            %{ $c->req->params } &&
            $c->req->params->{'ztc_behandelaar_id'}
        ) {
            $c->res->redirect(
                $c->uri_for('/')
            );

            my $dv;
            return unless ($dv = $c->zvalidate);


            $c->stash->{zaak}->set_behandelaar(
                $c->req->params->{'ztc_behandelaar_id'}
            );

            $c->detach;
        }

        if ($c->req->params->{betrokkene_type} eq 'medewerker') {
            $c->stash->{betrokkene_type} = 'medewerker';
            $c->stash->{template} = 'zaak/widgets/management.tt';
        } else {
            $c->stash->{betrokkene_type} = 'org_eenheid';
            $c->stash->{template} = 'zaak/widgets/set_org_eenheid.tt';
        }
    }
}

#### ACTIES VOOR OVERZICHT
{
    Zaaksysteem->register_profile(
        method  => 'weiger',
        profile => {
        }
    );

    sub weiger : Chained('/zaak/base'): PathPart('actie/weiger'): Args(0) {
        my ($self, $c) = @_;

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');


        ### VAlidation
        if ($c->req->is_xhr &&
            $c->req->params->{do_validation}
        ) {
            $c->zvalidate;
            $c->detach;
        }

        ### Post
        if (
            %{ $c->req->params } &&
            $c->req->params->{confirmed}
        ) {
            $c->res->redirect(
                $c->uri_for('/')
            );

            ### Confirmed
            #my $dv;
            #return unless $dv = $c->zvalidate;

            if ($c->stash->{zaak}->behandelaar) {
                $c->log->info(
                    'Zaak::Acties->weiger ['
                    . $c->stash->{zaak}->nr . ']: behandelaar removed'
                );

                if (
                    $c->stash->{zaak}->coordinator &&
                    $c->stash->{zaak}->coordinator->gegevens_magazijn_id eq
                        $c->stash->{zaak}->behandelaar->gegevens_magazijn_id
                ) {
                    $c->stash->{zaak}->coordinator(undef);
                    $c->stash->{zaak}->coordinator_gm_id(undef);
                }

                $c->stash->{zaak}->behandelaar_gm_id(undef);
                $c->stash->{zaak}->behandelaar(undef);
                $c->stash->{zaak}->update;
            }

            my $start_config = { %{ $c->customer_instance->{start_config} } };
            if (
                defined $start_config->{deny_zaak} &&
                $start_config->{deny_zaak}->{organizationalUnit} &&
                $start_config->{deny_zaak}->{posixGroup}
            ) {
                $c->log->debug(
                    'Change route_ou and route_role because of deny_zaak'
                );

                my $route_ou    = $c->model('Users')->find(
                    $start_config->{deny_zaak}->{organizationalUnit}
                );
                my $route_role  = $c->model('Users')->find(
                    $start_config->{deny_zaak}->{posixGroup}
                );

                if ($route_ou && $route_role) {
                    $c->stash->{zaak}->route_ou($route_ou->get_value('l'));
                    $c->stash->{zaak}->route_role($route_role->get_value('gidNumber'));
                    $c->stash->{zaak}->update;
                }

                $c->log->info(
                    'Zaak::Acties->weiger ['
                    . $c->stash->{zaak}->nr . ']: route_ou_role removed'
                );
            }

            $c->detach;
        }

        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze zaak wilt weigeren?';

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/zaak/'
                . $c->stash->{zaak}->nr
                . '/actie/weiger'
            );


        $c->forward('/page/confirmation');
        $c->detach;
    }
}


#### ACTIES 
{
    Zaaksysteem->register_profile(
        method  => 'wijzig_route',
        profile => {
        }
    );

    sub wijzig_route : Chained('/zaak/base'): PathPart('update/afdeling'): Args(0) {
        my ($self, $c) = @_;

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

        if (
            $c->req->params->{'ou_id'} &&
            $c->req->params->{'role_id'} &&
            $c->req->params->{confirmed} &&
            $c->model('Users')
                ->get_role_by_id(
                    $c->req->params->{'role_id'}
                ) &&
            $c->model('Users')
                ->get_ou_by_id(
                    $c->req->params->{'ou_id'}
                )
        ) {
            my $route = $c->stash->{zaak}->wijzig_route(
                $c->req->params->{ou_id},
                $c->req->params->{role_id}
            );

            if ($route) {
                my $ou = $c->model('Users')->get_ou_by_id($c->req->param('ou_id'));
                my $role = $c->model('Users')->get_role_by_id($c->req->param('role_id'));

                my $event = $c->stash->{ zaak }->logging->trigger('case/update/department', {
                    component => 'zaak',
                    data => {
                        case_id => $c->stash->{ zaak }->id,
                        ou_dn => $ou->get_value('description'),
                        role_dn => $role->get_value('cn')
                    }
                });

                $c->push_flash_message($event->onderwerp);
                $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr);
            }
            $c->detach;
        } else {
            $c->stash->{nowrapper} = 1;
            $c->stash->{template} = 'zaak/widgets/wijzig_route.tt';
            $c->detach;
        }
    }
}

{
    sub set_betrokkene_suggestion : Chained('/zaak/base'): PathPart('update/betrokkene/suggestion'): Args(0) {
        my ($self, $c) = @_;

        my $suggestion = $c->stash
            ->{zaak}
            ->betrokkenen_relateren_magic_string_suggestion(
                $c->req->params
            );

        unless ($suggestion) {
            $c->res->body('NOK');
            return;
        }

        $c->res->body($suggestion);
    }


    sub set_betrokkene : Chained('/zaak/base'): PathPart('update/betrokkene'): Args(0) {
        my ($self, $c) = @_;

        if (
            my $dv = $c->forward('/page/dialog', [{
                validatie       => BETROKKENE_RELATEREN_PROFILE,
                permissions     => [qw/zaak_beheer zaak_edit/],
                template        => 'widgets/betrokkene/create_relatie.tt',
                complete_url    => $c->uri_for('/zaak/'. $c->stash->{zaak}->id)
            }])
        ) {
            my $params  = $dv->valid;

            if (
                $c->stash->{zaak}->betrokkene_relateren(
                    $params
                )
            ) {
                $c->push_flash_message("Betrokkene toegevoegd");
            }
        }
    }
}

sub wijzig_vernietigingsdatum : Chained('/zaak/base'): PathPart('update/vernietigingsdatum'): Args(0) {
    my ($self, $c) = @_;

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie       => ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE,
            permissions     => [qw/zaak_beheer/],
            template        => 'zaak/widgets/wijzig_vernietigingsdatum.tt',
            complete_url    => $c->uri_for('/zaak/'. $c->stash->{zaak}->id)
        }])
    ) {
        my $params  = $dv->valid;
        
        if (
            $c->stash->{zaak}->wijzig_vernietigingsdatum(
                $params
            )
        ) {
            my $event = $c->stash->{ zaak }->logging->trigger('case/update/purge_date', {
                component => 'zaak',
                data => {
                    case_id => $c->stash->{ zaak }->id,
                    purge_date => $params->{ vernietigingsdatum }->dmy,
                    reason => $params->{ reden }
                }
            });

            $c->push_flash_message($event->onderwerp);

            $c->log->info(
                'Zaak[' . $c->stash->{zaak}->id . ']: ' . $event->onderwerp
            );
        }
    }
}


{
    Zaaksysteem->register_profile(
        method  => 'wijzig_zaaktype',
        profile => {
        }
    );

    sub wijzig_zaaktype : Chained('/zaak/base'): PathPart('update/zaaktype'): Args(0) {
        my ($self, $c) = @_;

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

        if ($c->req->params->{zaaktype_id} && $c->req->params->{confirmed}) {
            my $zaak = $c->stash->{zaak}->wijzig_zaaktype({
                zaaktype_id    => $c->req->params->{zaaktype_id}
            });
            if ($zaak) {
                $c->push_flash_message(sprintf('Zaaktype %s succesvol gewijzigd, nieuw zaaknummer: %d',
                    $c->stash->{ zaak }->nr,
                    $zaak->id
                ));

                $c->res->redirect('/zaak/' . $zaak->nr);
            } else {
                $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr);
            }
            $c->detach;
        } else {
            $c->stash->{nowrapper} = 1;
            $c->stash->{template} = 'zaak/widgets/wijzig_zaaktype.tt';
            $c->detach;
        }
    }
}


Zaaksysteem->register_profile(
    method  => 'set_allocation',
    profile => {
        required => [qw[selection selected_case_ids]],
        optional => [qw[notify]],
        dependencies => {
            change_allocation   => sub {
                my $dfv     = shift;
                my $type    = shift;

                if ($type eq 'behandelaar') {
                    return [ 'betrokkene_id' ];
                } else {
                    return [ 'ou_id' , 'role_id' ];
                }
            }

        }
    },
);
sub set_allocation : Chained('/zaak/base'): PathPart('update/allocation'): Args(0) {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    

    $c->stash->{ action }    = 'allocation';
    $c->stash->{ selection } = 'one_case';
    $c->stash->{ context } = $c->req->param('context') // 'index';
    $c->stash->{ template_available } = $c->model('DB::Config')->get('allocation_notification_template_id') ? 1 : 0;
    $c->stash->{ context } = $c->req->param('context') // 'index';

    $c->forward('bulk_update');
}
 

Zaaksysteem->register_profile(
    method  => 'bulk_update_allocation',
    profile => {
        required => [ qw/selection/],
        require_some => {
            toewijzing => [1, qw/betrokkene_id role_id ou_id/],
        }
    },
);
sub bulk_update_allocation : Chained('/') : PathPart('bulk/update/allocation') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    
    $c->stash->{action} = 'allocation';
    $c->forward('bulk_update');
}


Zaaksysteem->register_profile(
    method  => 'bulk_update_set_settings',
    profile => PROFILE_BULK_UPDATE_SET_SETTINGS,
);
sub bulk_update_set_settings : Chained('/') : PathPart('bulk/update/set_settings') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    
    
    $c->session->{bulk_update_kenmerken} = [];
    $c->stash->{table_config} = BULK_UPDATE_SIMPLE_TABLE_CONFIG;

    $c->stash->{action} = 'set_settings';
    $c->forward('bulk_update');
}



Zaaksysteem->register_profile(
    method  => 'bulk_update_verlengen',
    profile => {
        required => [ qw/reden datum selection/],
    },
);
sub bulk_update_verlengen: Chained('/') : PathPart('bulk/update/verlengen') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    
    
    $c->stash->{action} = 'verlengen';    

    $c->forward('bulk_update');
}

Zaaksysteem->register_profile(
    method  => 'bulk_update_owner',
    profile => {
        required => [ qw/selection/],
    },
);
sub bulk_update_owner: Chained('/') : PathPart('bulk/update/owner') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    
    
    $c->stash->{action} = 'owner';    

    $c->forward('bulk_update');
}

Zaaksysteem->register_profile(
    method  => 'verlengen',
    profile => {
        required => [ qw/reden datum/],
#        constraint_methods => {
#            datum  => sub {
#                my ($dfv) = @_;
#
#                my ($datum_dag, $datum_maand, $datum_jaar) = split (/-/,
#                    $dfv->get_filtered_data->{datum}); 
#                my $givendate = DateTime->new(
#                    year => $datum_jaar,
#                    month => $datum_maand,
#                    day => $datum_dag,
#                );
#
#                if ($givendate > DateTime->now()) {
#                    return 1;
#                }
#
#                return;
#            },
#        },
#        msgs => {
#            'invalid' => 'Datum mag niet in het verleden liggen',
#        },
    }
);
sub verlengen: Chained('/zaak/base'): PathPart('update/verlengen'): Args(0) {
    my ($self, $c) = @_;


    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    

    $c->stash->{action}    = 'verlengen';    
    $c->stash->{selection} = 'one_case';

    $c->forward('bulk_update');
}




#
# these changes can apply to 3 different sets:
# - a single case
# - a selection of cases
# - all cases in the search results
#
sub bulk_update : Private {
    my ($self, $c) = @_;

#    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my $action = $c->stash->{action} || '';
    
    my $permitted_bulk_actions = {
        map { $_ => 1 } 
        qw/allocation verlengen opschorten resume relatie set_settings destroy owner/
    };
    
    unless($permitted_bulk_actions->{$action}) {
        die "action can only be in " . Dumper $permitted_bulk_actions;
    }

 #   die "only acceptable through xmlhttprequest" unless($c->req->is_xhr);
    
    my $params = $c->req->params();

    $c->stash->{action} = $action;
    my $selection = $c->stash->{selection} ||= $params->{selection};

    if($params->{commit}) {
        if($selection eq 'selected_cases') {
            my @case_ids = sort {$b <=> $a} split /,/, $params->{selected_case_ids};
            foreach my $case_id (@case_ids) {
                $self->_execute_action($c, {case_id => $case_id, action=>$action});
            }
        } elsif($selection eq 'search_results') {

            $c->stash->{search_query_id} = $params->{search_query_id};
            my $search_query = $self->_search_query($c);
            my $resultset = $search_query->results({
                c => $c
            })->search({}, {order_by => {'-desc' => 'me.id'}});
           
            while(my $case = $resultset->next()) {
                $self->_execute_action($c, {case_id => $case->id, action=>$action});
            }

        } elsif($selection eq 'one_case') {
            
            my $case_id = $params->{selected_case_ids};
            $self->_execute_action($c, {case_id => $case_id, action=>$action});

            if($action eq 'behandelaar') {
                my ($betrokkene_type, $uidnumber) = $params->{betrokkene_id} =~ /betrokkene-(\w+)-(\d+)$/;
                
                if (
                    $betrokkene_type eq 'medewerker' &&
                    $uidnumber eq $c->user->uidnumber
                ) {
                    $c->stash->{json}->{redirect} = '/zaak/' . $case_id;    
                } else {
                    $c->stash->{json}->{redirect} = '/';
                }
            } elsif($action =~ m/^(verlengen|opschorten|resume|relatie|set_settings)$/) {
                $c->stash->{ json }{ redirect } = sprintf('/zaak/%d', $case_id);
            }
        }

        if($c->req->param('no_redirect')) {
            delete $c->stash->{ json }{ redirect };
            delete $c->stash->{ json }{ reload };
        }

        $c->stash->{ json }{ success } = 1;
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }


    if($selection eq 'selected_cases') {
        $c->stash->{selected_case_ids} = $params->{selected_case_ids};
    } elsif($selection eq 'search_results') {        
        $c->stash->{search_query_id} = $params->{search_query_id};
        my $search_query = $self->_search_query($c);
        my $resultset = $search_query->results({
            c => $c
        })->with_progress({}, {page=>1});
        $c->stash->{search_query} = $resultset;

    } else {
        die "need zaak_id" unless $c->stash->{zaak}->id;
        $c->stash->{selected_case_ids} = $c->stash->{zaak}->id;
    }

    $c->stash->{template} = 'zaak/widgets/management.tt';    
    $c->stash->{nowrapper} = 1;
}




sub _execute_action {
    my ($self, $c, $args) = @_;
    
    my $case_id = $args->{case_id}  or die "need case id";
    my $action  = $args->{action}   or die "need action";

    $c->log->debug("executing action $action on zaak $case_id");

    my $params  = $c->req->params;
    my $case    = $c->model('DB::Zaak')->find($case_id);
    
    $c->stash->{zaak} = $case;

    return unless $c->check_any_zaak_permission('zaak_beheer','zaak_edit');

    if($action eq 'allocation') {
        
        my $allocation_message = 'Toewijzing voor zaak '. $case->id . ' is gewijzigd: ';

        if($case->is_afgehandeld) {
            $c->push_flash_message('Zaak is reeds afgehandeld, kan toewijzing niet wijzigen');
            $c->log->debug("case id:  " . $case->id . " is afgehandeld");
        } else {
            if($params->{change_allocation} eq 'behandelaar') {

                $allocation_message     .= "behandelaar is " . $params->{betrokkene_naam};

                $case->set_behandelaar(
                    $params->{betrokkene_id}
                );

                # this can be influenced with a checkbox
                if ($params->{change_department}) {
                    my $route_ou = $case->behandelaar_object->org_eenheid->ldapid;
                    $case->wijzig_route({
                        route_ou                 => $route_ou,
                        route_role               => $case->route_role,
                        change_only_route_fields => 1,
                    });
                    $allocation_message .= ", afdeling is $route_ou";
                }

                my $current_user        = 'betrokkene-medewerker-'. $c->user->uidnumber;

                if($params->{betrokkene_id} eq $current_user) {
                    $case->wijzig_status({status => 'open'});

                    if($params->{selection} eq 'one_case') {
                        $c->stash->{json}->{redirect} = '/zaak/' . $case->id;
                    }
                } else {
                    $case->wijzig_status({status => 'new'});

                    if($params->{selection} eq 'one_case') {
                        $c->stash->{json}->{redirect} = '/';
                    }
                }
            }


            if(
                $params->{change_allocation} eq 'group' &&
                $params->{ou_id} && $params->{role_id}
            ) {
                $case->wijzig_route(
                    {
                        route_ou    => $params->{ou_id},
                        route_role  => $params->{role_id}
                    }
                );

                $allocation_message .= "afdeling is " . $params->{ou_id};
                $allocation_message .= ', ';
                $allocation_message .= 'rol is ' . $params->{role_id};

                if($c->stash->{ context } eq 'case') {
                    $c->stash->{ json }{ redirect } = '/';
                } else {
                    $c->stash->{ json }{ reload } = 1;
                }

            } elsif(!$params->{betrokkene_id}) {
                $allocation_message .= 'zaak is niet toegewezen.';
            }

            $c->push_flash_message($allocation_message);
        }

        unless($c->stash->{json}->{redirect}) {
            $c->stash->{json}->{reload} = 1;
        }

        $case->update();

        # Send notification after update, in case the save doesn't pan out.

        if($params->{ notify }) {
            unless($params->{ betrokkene_id }) {
                $c->stash->{ json }{ reload } = 1;

                return;
            }

            my $template_id = $c->model('DB::Config')->get('allocation_notification_template_id');

            unless($template_id) {
                $c->push_flash_message('Kon geen e-mail notificatie template vinden, geen notificatie verstuurd');
                $c->stash->{ json }{ reload } = 1;

                return;
            }

            # TODO remove hardwired ID
            my $template = $c->model('DB::BibliotheekNotificaties')->find($template_id);

            unless($template) {
                $c->push_flash_message('Notificatie template kon niet gevonden worden, geen e-mail notificatie verstuurd');
                $c->stash->{ json }{ reload } = 1;

                return;
            }

            unless($case->behandelaar_object->email) {
                $c->push_flash_message(sprintf(
                    'Geen e-mail adres gevonden voor behandelaar "%s", geen notificatie verstuurd',
                    $params->{ betrokkene_naam }
                ));

                $c->stash->{ json }{ reload } = 1;

                return;
            }

            $c->forward('/zaak/mail/send', [{
                to => $case->behandelaar_object->email,
                body => $template->message,
                subject => $template->subject
            }]);
        }

        unless($c->stash->{json}->{redirect}) {
            $c->stash->{json}->{reload} = 1;
        }

    } elsif($action eq 'owner') {

        $case->wijzig_status({status => 'open'});
    
        my $current_user = $case->result_source
            ->schema
            ->resultset('Zaak')
            ->current_user;

        $case->logging->trigger('case/accept', { component => 'zaak', data => {
            case_id => $case->id,
            acceptee_name => $current_user->naam
        }});
    
        unless ($case->behandelaar) {
            $case->set_behandelaar($current_user->betrokkene_identifier);
        }

    } elsif($action eq 'verlengen') {   

        $self->_verleng($c, {
            datum       => $params->{datum},
            reden       => $params->{reden},
            case_id     => $case_id
        });
    } elsif($action eq 'opschorten') {
        unless(
            $case->status eq 'resolved' || 
            $case->status eq 'overdragen'
        ) {
            my $options = {
                status => 'stalled',
                reason => $params->{reden},
            };
            if ($params->{schedule_resume}) {
                $options->{suspension_term_amount} = $params->{suspension_term_amount};
                $options->{suspension_term_type}   = $params->{suspension_term_type};
            }
            $case->wijzig_status($options);
            $case->update();
        }
    } elsif($action eq 'resume') {
        $case->wijzig_status(
            {
                status => 'open',
                reason => $params->{reden},
            }
        );
        $case->update();
    } elsif($action eq 'relatie') {
        $self->_relate_case($c, {
            zaaknr  => $params->{zaaknr}, 
            case    => $case
        });                
    } elsif($action eq 'set_settings') {
        $self->_set_settings($c, {
            case => $case
        });
    } elsif($action eq 'destroy') {
        $self->_destroy($c, { 
            case => $case,
        });
    }
}



# TODO could use some rework - this code exists on multiple places. should be a model
sub _search_query {
	my ($self, $c) = @_;
	die "need c" unless($c);

	my $search_query = $c->model('SearchQuery');
	my $search_query_id = $c->req->params->{search_query_id};
	my $model = $c->model(SEARCH_QUERY_TABLE_NAME);
	my $record;
	$c->log->debug("dsdfsdfdfdf: " . $search_query_id);
	if($search_query_id) {
		my $record = $model->find($search_query_id);
		if($record) {
			$search_query->unserialize($record->settings);
			$c->stash->{'search_query_name'} = $record->name;
			$c->stash->{'record_ldap_id'} = $record->ldap_id;
			$c->stash->{'search_query_access'} = $search_query->access();
			return $search_query;
		} else {
		    die "wtf object not found";
		}
	} else {
		$c->stash->{'search_query_name'} = 'Naamloze zoekopdracht';
		$c->stash->{'search_query_access'} = 'private';
	}

	if($c->session->{SEARCH_QUERY_SESSION_VAR()}) {
		$search_query->unserialize($c->session->{SEARCH_QUERY_SESSION_VAR()});
	}
	
	return $search_query;
}



Params::Profile->register_profile(
    method  => '_relate_case',
    profile => {
        required => [qw/case zaaknr/],
    }
);

sub _relate_case {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _relate_case" unless $dv->success;

    my $case    = $params->{case};
    my $zaaknr  = $params->{zaaknr};
    my $relatie = $params->{relatie};
    
    my $related_case = $c->model('DB::Zaak')->find($params->{zaaknr});
    
    my $error = 0;
    if(!$related_case) {
        $c->push_flash_message(sprintf('Er is geen zaak met nummer %d.', $params->{ zaaknr }));

        $error = 1;
    }

    if ($params->{zaaknr} eq $case->id) {
        $c->push_flash_message('Kan geen relatie aanmaken met hetzelfde zaaknummer als huidige zaak.');

        $error = 1;
    }

    unless($error) { 
        $case->set_relatie({
            relatie => 'gerelateerd',
            relatie_zaak => $related_case
        });
    }

    $c->push_flash_message(
        'Zaak %d (%s) gerelateerd',
        $related_case->id,
        $related_case->zaaktype_node_id->titel
    );
}



Params::Profile->register_profile(
    method  => '_verleng',
    profile => {
        required => [qw/datum reden case_id/],
    }
);
sub _verleng : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _get_session_zaaktype_status" unless $dv->success;

    my $datum       = $params->{datum};
    my $reden       = $params->{reden};
    my $case_id     = $params->{case_id};

    my ($datum_dag, $datum_maand, $datum_jaar) = split /-/, $datum; 

    my $givendate = DateTime->new(
        year    => $datum_jaar,
        month   => $datum_maand,
        day     => $datum_dag,
    );

    my $case = $c->model('DB::Zaak')->find($case_id);
    $case->set_verlenging($givendate);

    my $event = $case->logging->trigger('case/update/settle_date', { component => 'zaak', data => {
        case_id => $case->id,
        settle_date => $givendate->dmy,
        reason => $c->req->param('reden')
    }});

    $c->push_flash_message($event->onderwerp);
}






#sub set_aanvrager : Chained('/zaak/base'): PathPart('update/aanvrager'): Args(0) {
#    my ($self, $c) = @_;
#
#    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');
#    # Get aanvrager type
#    if ($c->stash->{zaak}->aanvrager) {
#        $c->stash->{betrokkene_type} = $c->stash->{zaak}->aanvrager_object->btype;
#    } else {
#        $c->stash->{betrokkene_type} = 'natuurlijk_persoon';
#    }
#
#    my $btype = $c->stash->{betrokkene_type};
#
#    if (!%{ $c->req->params }) {
#        $c->stash->{nowrapper} = 1;
#        $c->stash->{template} = 'zaak/widgets/set_aanvrager.tt';
#        $c->detach;
#    } elsif (
#        $c->req->params->{'ztc_behandelaar_id'} &&
#        $c->req->params->{'ztc_behandelaar_id'} =~ /$btype/
#    ) {
#        $c->stash->{zaak}->set_aanvrager($c->req->params->{'ztc_behandelaar_id'});
#    }
#}

sub set_eigenaar : Chained('/zaak/base'): PathPart('update/eigenaar'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{betrokkene_type} = 'medewerker';
    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');
    if (!%{ $c->req->params }) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaak/widgets/set_eigenaar.tt';
#        $c->stash->{url} = $c->uri_for('/zaak/' . $c->stash->{zaak}->nr .
#            '/update/behandelaar');
        $c->detach;
    } elsif ($c->req->params->{'ztc_behandelaar_id'}) {
        $c->stash->{zaak}->set_coordinator($c->req->params->{'ztc_behandelaar_id'});
#        $c->stash->{zaak}->notes->add({
#            'commenttype'   => 'actie',
#            'value'         => 'Zaakcoordinator voor zaak gewijzigd naar '
#                . $c->stash->{zaak}->behandelaar->naam
#        });
    }
}




Zaaksysteem->register_profile(
    method  => 'bulk_update_relatie',
    profile => {
        required => [ qw/zaaknr/ ],
    },
);
sub bulk_update_relatie: Chained('/') : PathPart('bulk/update/relatie') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }
        
    $c->stash->{action} = 'relatie';
    $c->forward('bulk_update');
}



Zaaksysteem->register_profile(
    method  => 'bulk_update_kenmerken',
    profile => {
        required => [ qw//],
    },
);
sub bulk_update_kenmerken: Chained('/') : PathPart('bulk/update/kenmerken') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }
    
    my $params = $c->req->params();

    my $action = $params->{action} || '';
    
    my $bulk_update_kenmerken = $c->session->{bulk_update_kenmerken} || [];

    # get new serial id
    my $new_id = 1;
    foreach my $item (@$bulk_update_kenmerken) {
        $new_id = $item->{id} if($item->{id} > $new_id);
    }
    $new_id++;


    if($action eq 'add') {
        my $bibliotheek_kenmerken_id = $params->{simpletable_new_item_id} or die "need new item id";

        my $kenmerk = $c->model('DB::BibliotheekKenmerken')->find($bibliotheek_kenmerken_id);
        my $new_record = {
            id                          => $new_id,
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            kenmerk_naam                => $kenmerk->naam,
        };

        push @$bulk_update_kenmerken, $new_record;

    } elsif($action eq 'remove') {
        my $remove_id = $params->{remove_id} or die "need remove id";
        
        $bulk_update_kenmerken = [grep { $_->{id} ne $remove_id } @$bulk_update_kenmerken];
    }
    
    foreach my $kenmerk (@$bulk_update_kenmerken) {
        my $key = 'bibliotheek_kenmerk_' . $kenmerk->{bibliotheek_kenmerken_id};
        $c->log->debug("key: " . $key);
        if($params->{$key}) {
            $kenmerk->{value} = $params->{$key};
        }
    }

    $c->session->{bulk_update_kenmerken} = $bulk_update_kenmerken;


    $c->stash->{table_config} = BULK_UPDATE_SIMPLE_TABLE_CONFIG;
    $c->stash->{table_config}->{rows} = $bulk_update_kenmerken;
    
    $c->stash->{action}     = 'kenmerk';
    $c->stash->{template}   = 'widgets/general/simple_table.tt';

    $c->stash->{nowrapper}  = 1;
}



Zaaksysteem->register_profile(
    method  => 'relatie',
    profile => {
        required => [ qw/zaaknr/],
    }
);
sub relatie : Chained('/zaak/base'): PathPart('update/relatie'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my $params = $c->req->params();

    if($params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action}    = 'relatie';    
    $c->stash->{selection} = 'one_case';

    $c->forward('bulk_update');
}

my $settings_mapping = {
    coordinator_id      => {
        method  => 'wijzig_coordinator',
        mapping => {
            coordinator_id  => 'betrokkene_identifier',
        },
        label   => 'Wijzigen coordinator',
    },
    aanvrager_id        => {
        method  => 'wijzig_aanvrager',
        mapping => {
            aanvrager_id  => 'betrokkene_identifier',
        },
        label   => 'Wijzigen aanvrager',
    },
    ou_id               => {
        method      => 'wijzig_route',
        mapping     => {
            ou_id                       => 'route_ou',
            role_id                     => 'route_role',
            change_only_route_fields    => 'change_only_route_fields',
            wijzig_route_force          => 'force',
        },
        change_check => sub {
            my ($zaak, $opts) = @_;
            return unless ($opts->{route_role} && $opts->{route_ou});

            if (
                $zaak->route_role   eq $opts->{route_role} &&
                $zaak->route_ou     eq $opts->{route_ou}
            ) {
                return;
            }

            return 1;
        },
        label       => 'Wijzigen afdeling',
    },
    streefafhandeldatum  => {
        method      => 'wijzig_streefafhandeldatum',
        mapping     => {
            streefafhandeldatum     => 'streefafhandeldatum',
        },
        label       => 'Wijzigen streefafhandeldatum',
    },
    registratiedatum  => {
        method      => 'wijzig_registratiedatum',
        mapping     => {
            registratiedatum     => 'registratiedatum',
        },
        label       => 'Wijzigen registratiedatum',
    },
    vernietigingsdatum_type  => {
        method      => 'wijzig_vernietigingsdatum',
        mapping     => {
            vernietigingsdatum_type     => 'vernietigingsdatum_type',
            vernietigingsdatum          => 'vernietigingsdatum',
            vernietigingsdatum_reden    => 'reden',
        },
        label       => 'Wijzigen vernietigingsdatum',
    },
    milestone           => {
        method      => 'wijzig_fase',
        mapping     => {
            milestone       => 'milestone',
        },
        change_check => sub {
            my ($zaak, $opts) = @_;

            return unless $opts->{milestone};

            if (
                $zaak->milestone == $opts->{milestone}
            ) {
                return;
            }

            return 1;
        },
        label       => 'Wijzigen fase',
    },
    status              => {
        method      => 'wijzig_status',
        mapping     => {
            status          => 'status'
        },
        change_check => sub {
            my ($zaak, $opts) = @_;

            return unless $opts->{status};

            if (
                $zaak->status eq $opts->{status}
            ) {
                return;
            }

            return 1;
        },
        hook => sub {
            my ($zaak, $opts) = @_;

            $zaak->set_gesloten if $opts->{status} eq 'resolved';
        },
        label       => 'Wijzigen status',
    },
    zaaktype_id     => {
        method      => 'wijzig_zaaktype',
        mapping     => {
            zaaktype_id     => 'zaaktype_id',
        },
        label       => 'Wijzigen zaaktype',
    },
};



Zaaksysteem->register_profile(
    method  => 'set_settings',
    profile => PROFILE_BULK_UPDATE_SET_SETTINGS,
);
sub set_settings : Chained('/zaak/base'): PathPart('update/set_settings'): Args(0) {
    my ($self, $c) = @_;

    unless (
        $c->check_any_user_permission('admin', 'zaak_beheer') ||
        $c->check_any_zaak_permission('zaak_beheer')
    ) {
        $c->detach('/forbidden');
    }

    my $params = $c->req->params();

    if($params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->session->{bulk_update_kenmerken} = [];

    $c->stash->{action}    = 'set_settings';    
    $c->stash->{selection} = 'one_case';

    $c->stash->{table_config} = BULK_UPDATE_SIMPLE_TABLE_CONFIG;

    $c->forward('bulk_update');
}



Params::Profile->register_profile(
    method  => '_set_settings',
    profile => {
        required => [qw/case/],
    }
);
sub _set_settings : Private {
    my ($self, $c, $args) = @_;

    my $dv = Params::Profile->check(params  => $args);
    die "invalid options for _set_settings" . Dumper $dv unless $dv->success;

    my $zaak    = $args->{case};
    unless (
        $c->check_any_user_permission('admin', 'zaak_beheer') ||
        $c->check_any_zaak_permission('zaak_beheer')
    ) {
        $c->detach('/forbidden');
    }

    my @set_settings    = ();
    my $valid           = 1;
    my $params          = $c->req->params();

    while (my ($parameter, $config) = each %{ $settings_mapping }) {
        my $method  = $config->{method};

        my $opts = {};
        
        foreach my $key (keys %{ $config->{mapping} } ) {            
            if (
                defined $params->{ $key } && 
                defined $config->{mapping}->{$key}
            ) {
                my $opts_key = $config->{mapping}->{ $key };
                my $opts_val = $params->{$key};
                
                $opts->{$opts_key} = $opts_val;
            }
        }
        
        if ($config->{change_check}) {
            next unless $config->{change_check}->($zaak, $opts);
        } else {
            next unless $params->{$parameter};
        }

        if ($config->{hook}) {
            $config->{hook}->($zaak, $opts);
        }

        push(@set_settings,
            {
                method  => $method,
                opts    => $opts,
                config  => $config,
            }
        );
    }

    my @results = ();
    for my $config (@set_settings) {
        my $method  = $config->{method};
        my $opts    = $config->{opts};
        my $label   = $config->{config}->{label};

        $c->log->debug(
            'Running method: ' . $method . ', with params: '
            . Dumper($opts)
        );

        my $msg     = $label;
        if (my $result = $zaak->$method($opts)) {
            ### These instructions are here for a proper redirect after a zaaktype
            ### wijziging. Yes, it's not as generic as we would have liked.
            if (
                blessed($result) &&
                $result->isa('Zaaksysteem::Model::DB::Zaak')
            ) {
                $zaak = $result;
            }

            $c->push_flash_message($label . ' voor zaak ' . $zaak->id .' gelukt.');

        } else {
            $c->push_flash_message($label . ' voor zaak ' . $zaak->id .' mislukt.');
        }
    }

    foreach my $param (keys %$params) {
        my ($bibliotheek_kenmerken_id) = $param =~ m|^bibliotheek_kenmerk_(\d+)$|;

        next unless $bibliotheek_kenmerken_id;

        $self->_update_kenmerk($c, {
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            value                       => $params->{$param},
            case                        => $zaak,
        });
    }

    $zaak->touch();
}


# TODO move to model layer. also add logging there - kenmerk changes are insufficiently logged
# anyway
sub _update_kenmerk : Private {
    my ($self, $c, $args) = @_;
    
    my $bibliotheek_kenmerken_id    = $args->{bibliotheek_kenmerken_id} or die "need bki";
    my $case                        = $args->{case} or die "need case";
    my $value                       = $args->{value} or die "need value";


    unless(
        $case->zaaktype_node_id->zaaktype_kenmerken->find({
            bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
        })
    ) {
        $c->log->debug("kenmerk $bibliotheek_kenmerken_id does not exist for zaak " . $case->id);
        return;
    }


    $c->model("DB")->txn_do(sub {
        eval {
            # Remove existing values for this bibliotheek_kenmerken_id
            $case->zaak_kenmerken->search({
                bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id, 
                zaak_id                     => $case->id,
            })->delete;

            # Re-create this kenmerk with new values
            $case->zaak_kenmerken->create_kenmerk({
                zaak_id                     => $case->id,
                bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
                values                      => $value,
            });

            my $event = $case->logging->trigger('case/attribute/update', {
                component => 'kenmerk',
                data => {
                    case_id => $case->id,
                    attribute_id => $bibliotheek_kenmerken_id,
                    attribute_value => $value
                }
            });

            $c->log->debug($event->onderwerp);
        };
    
        if ($@) {
            die("Error updating kenmerken: " . $@);
        }
    });
}


Zaaksysteem->register_profile(
    method  => 'bulk_update_resume',
    profile => {
        required => [ qw/reden selection/],
    },
);
sub bulk_update_resume: Chained('/') : PathPart('bulk/update/resume') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    
    
    $c->stash->{action} = 'resume';
    $c->forward('bulk_update');
}


Zaaksysteem->register_profile(
    method  => 'bulk_update_opschorten',
    profile => {
        required => [ qw/reden selection/],
    },
);
sub bulk_update_opschorten: Chained('/') : PathPart('bulk/update/opschorten') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    
    
    $c->stash->{action} = 'opschorten';    
    $c->forward('bulk_update');
}



define_profile opschorten => (
    required => [ qw/reden/],
    dependencies => {
        schedule_resume => {
            1 => [ qw/suspension_term_amount/]
        }
    }
);
sub opschorten : Chained('/zaak/base'): PathPart('update/opschorten'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    

    $c->stash->{action}    = 'opschorten';    
    $c->stash->{selection} = 'one_case';

    $c->forward('bulk_update');
}



Zaaksysteem->register_profile(
    method  => 'resume',
    profile => {
        required => [ qw/reden/],
    }
);
sub resume : Chained('/zaak/base'): PathPart('update/resume'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }    

#    unless ($c->stash->{zaak}->status eq 'stalled') {
#        $c->stash->{is_opgeschort} = 1;
#    }

    $c->stash->{action}    = 'resume';    
    $c->stash->{selection} = 'one_case';

    $c->forward('bulk_update');
}


sub vorige_status : Chained('/zaak/base'): PathPart('update/vorige_status'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer');

    if (!exists($c->req->params->{update})) {
        $c->stash->{nowrapper}  = 1;
        $c->stash->{template}   = 'zaak/widgets/vorige_status.tt';
        $c->detach;
    } elsif ($c->req->params->{'update'}) {
        if ($c->stash->{zaak}->set_vorige_fase) {
            $c->push_flash_message('Zaak succesvol omgezet naar vorige fase');
        }
    }

    $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr
        . '#zaak-elements-status'
    );
    $c->detach;
}










Zaaksysteem->register_profile(
    method  => 'afhandelen',
    profile => {
        required => [ qw/
            reden            
            system_kenmerk_resultaat
        /],
    }
);

sub afhandelen : Chained('/zaak/base'): PathPart('update/afhandelen'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my $params = $c->req->params();


    unless ($params->{update}) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaak/widgets/afhandelen.tt';
        $c->detach;
    }


    if ($c->req->is_xhr && $params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }


    $c->stash->{zaak}->resultaat($params->{system_kenmerk_resultaat});
    $c->stash->{zaak}->set_gesloten(DateTime->now());

    $c->stash->{ zaak }->logging->trigger('case/early_settle', {
        component => 'zaak',
        data => {
            case_id => $c->stash->{ zaak }->id,
            reason => $params->{ reden },
            case_result => $c->stash->{zaak}->resultaat
        }
    });

    $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr);
    $c->detach;
}



sub deelzaak : Chained('/zaak/base'): PathPart('update/deelzaak'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if (!exists($c->req->params->{update})) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaak/widgets/deelzaak.tt';
        $c->detach;
    } else {
        $c->forward('/zaak/status/start_subzaken');

        $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr
            . '#zaak-elements-status'
        );
        $c->detach;
    }
}

sub magic_string_test  : Chained('/zaak/base'): PathPart('test/magic_string'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my @magic_strings;

    {
        my $kenmerken       = $c->stash->{zaak}
            ->zaaktype_node_id
            ->zaaktype_kenmerken
            ->search(
                {
                    'bibliotheek_kenmerken_id.value_type'   => { '!='   => 'file' },
                    'me.is_group'                           => undef
                },
                {
                    prefetch    => 'bibliotheek_kenmerken_id'
                }
            );

        while (my $kenmerk = $kenmerken->next) {
            push(@magic_strings, $kenmerk->magic_string);
        }
    }

    {
        my $ZAAKSYSTEEM_STANDAARD_KENMERKEN = ZAAKSYSTEEM_STANDAARD_KENMERKEN;
        push (@magic_strings, keys %{ $ZAAKSYSTEEM_STANDAARD_KENMERKEN });
    }

    {
        my $betrokkenen = $c->stash->{zaak}->zaak_betrokkenen->search_gerelateerd;

        my $kenmerken_h = ZAAKSYSTEEM_BETROKKENE_KENMERK;
        my @kenmerken   = keys %{ $kenmerken_h };

        while (my $betrokkene = $betrokkenen->next) {
            my $magic_string = $betrokkene->magic_string_prefix;

            my $betrokkene_object = $c->stash->{zaak}->betrokkene_object(
                {
                    'magic_string_prefix'    => $magic_string
                }
            ) or next;

            for my $kenmerk_postfix (@kenmerken) {
                my $kenmerk = $magic_string . '_' . $kenmerk_postfix;

                push(@magic_strings, $kenmerk);
            }
        }
    }

    $c->stash->{string_test}     = "List of magic string kenmerken:";
    for my $magic_string (@magic_strings) {
        $c->stash->{string_test} .= "\n- $magic_string : [[$magic_string]]";
    }

    $c->stash->{string_test} = $c->model('Bibliotheek::Sjablonen')->_replace_kenmerken(
        $c->stash->{zaak}, $c->stash->{string_test}
    );

    $c->stash->{template}   = 'zaak/widgets/magic_string_test.tt';
}


sub delete_job : Chained('/zaak/base') : PathPart('delete_job') {
    my ($self, $c) = @_;

    my $job_id = $c->req->params->{job_id} 
        or die "need job_id";

    my $scheduled_job = $c->model('DB::ScheduledJobs')->find($job_id) 
        or die "could not find job";

    $scheduled_job->delete;

    $c->forward('scheduled_jobs');
}

sub reschedule : Chained('/zaak/base') : PathPart('reschedule') {
    my ($self, $c) = @_;

    $c->stash->{zaak}->reschedule_notifications();

    $c->forward('scheduled_jobs');
}


sub scheduled_jobs : Chained('/zaak/base') : PathPart('scheduled_jobs') {
    my ($self, $c) = @_;

    $c->stash->{template}  = "zaak/widgets/reschedule.tt";
    $c->stash->{nowrapper} = 1;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


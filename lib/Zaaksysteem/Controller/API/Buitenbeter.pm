package Zaaksysteem::Controller::API::Buitenbeter;

use Moose;
use namespace::autoclean;

use Data::Dumper;
use Zaaksysteem::Exception;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }


sub load_interface : Private {
    my ($self, $c) = @_;

    $c->stash->{interface} = $c->model('DB::Interface')->search_active({
        module => 'buitenbeter'
    })->first or throw("api/buitenbeter", "Buitenbeter koppeling is niet beschikbaar");
}


# trigger the import in spoof mode, useful for development and clean demonstration/testing
sub spoof : Local : ZAPI {
    my ($self, $c) = @_;

    $c->forward('load_interface');

    my $test_responses = $c->stash->{interface}->process_trigger('get_test_responses');

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => $c->stash->{interface}->get_interface_config->{interval} * 60 * 15,
    });
}


# manually trigger the actual import
sub GetNewMeldingen : Local : ZAPI {
    my ($self, $c) = @_;

    $c->forward('load_interface');

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger('GetNewMeldingen');
}


sub PostStatusUpdate : Local : ZAPI {
    my ($self, $c) = @_;

    $c->forward('load_interface');

    my $case_id = $c->req->params->{case_id};
    my $case = $c->model('DB::Zaak')->find($case_id)
        or throw('sysin/buitenbeter', "Zaak $case_id niet gevonden, systeemfout");

    my $kenmerken = $case->field_values;

    my $result = $c->stash->{interface}->process_trigger('PostStatusUpdate', {
        kenmerken => $kenmerken,
        statusCode => 'In behandeling',
        statusText => 'dit is de status text'
    });

    $c->stash->{zapi} = [$result];
}





=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;
package Zaaksysteem::Controller::API::Authorization;

use Moose;
use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head2 role

List the available roles. By specifying a parent org_unit
additional specific role may be added

=cut

sub role : Local : ZAPI {
    my ($self, $c, $parent_org_unit_id) = @_;

    $c->stash->{zapi} = $self->get_org_unit_roles($c, $parent_org_unit_id);
}


=head2 get_org_unit_roles

Gather roles per org unit, slightly inefficient. Can be optimized
by digging in the ldap interaction.

=cut

sub get_org_unit_roles {
    my ($self, $c, $parent_org_unit_id) = @_;

    my $roles = $c->model('Users')->get_array_of_roles({
        parent_ou => $parent_org_unit_id,
        cache => $c->stash->{primary_roles_cache} ||= {}
    });

    return [
        map { $self->format_role($_) }
        grep { $_ ne 'split' } @$roles
    ];
}


sub format_role {
    my ($self, $role) = @_;

    return {
        type    => 'entry',
        role_id => $role->{internal_id},
        name    => $role->{name},
        system  => $role->{system}
    };
}


=head2 org_unit

List available org_units, including the top-level
organization element - which typically is not shown, but
it's included for more flexibility and so that the depth
numbering may make sense.

=cut

sub org_unit : Local : ZAPI {
    my ($self, $c) = @_;

    my $tree = $c->model('Users')->get_tree_view({
        objectClass => ['organization', 'organizationalUnit']
    });

    $c->stash->{zapi} = [ $self->list_org_units($c, $tree) ];
}


sub list_org_units {
    my ($self, $c, $entries, $depth) = @_;

    $depth ||= 0;

    # every organizational unit should have a name and internal_id, and
    # potentially children
    my $handle_entry = sub {
        my $entry = shift;

        my $result = {
            org_unit_id => $entry->{internal_id},
            name        => $entry->{name},
            depth       => $depth,
            roles       => $self->get_org_unit_roles($c, $entry->{internal_id})
        };

        # if this unit has children, recursively fetch and add them
        return $entry->{children} ?
            ($result, $self->list_org_units($c, $entry->{children}, $depth + 1)) :
            $result;
    };

    # handle_entry may return a list, which is snugly appended by perl
    return map { $handle_entry->($_) } @$entries;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


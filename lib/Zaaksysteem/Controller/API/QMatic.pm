package Zaaksysteem::Controller::API::QMatic;

use Moose;
use namespace::autoclean;

use Data::Dumper;
use Zaaksysteem::Exception;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }


sub index : Local : ZAPI {
    my ($self, $c) = @_;

    eval {
	    my $qmatic = $c->model('DB::Interface')->search_active({ module => 'qmatic' })->first
	        or die "need qmatic interface";

        my $params = $c->req->params;
        my $action = $params->{action};

        my $result = $qmatic->process_trigger($action, $params);

        die "need result array" unless $result && @$result;

        $c->stash->{zapi} = $result;
	};

    if ($@) {
        $c->log->error("qmatic error: $@");

		my $type = $@ =~ m|^no qmatic interface was configured for case_type_id|
            ? 'qmatic/config_missing'
            : 'qmatic/unknown';

        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type 			=> $type,
            messages 		=> []
        );
	}
}


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;
package Zaaksysteem::Controller::API::Search;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;
use Data::Dumper;

use JSON;

# use Zaaksysteem::ZAPI::CRUD::Interface;
# use Zaaksysteem::ZAPI::CRUD::Interface::Column;
# use Zaaksysteem::ZAPI::CRUD::Interface::Filter;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::ObjectSubscription - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::ObjectSubscription>

=head1 DESCRIPTION

Zaaksysteem API Controller for ObjectSubscription.

=head1 METHODS

=head2 /object_subscription [GET READ]

 /object_subscription?zapi_crud=1

Returns a list of object_subscriptions.

B<Special Query Params>

=over 4

=item C<zapi_crud=1>

Use the special query parameter C<zapi_crud=1> for getting a technical CRUD
description.

=back

=cut

sub json_get_contents {
    my $filename    = shift;
    
    open(my $fh, "<:encoding(UTF-8)", $filename)
        || throw('api/search/file_error', "can't open UTF-8 encoded filename: $!");
        
    my $str = '';

    while (<$fh>) {
        $str .= $_;
    }
    
    # warn($str);
    
    close($fh);
    
    return JSON::from_json($str, { utf8  => 1 });
}

sub base
    : Chained('/api/base')
    : PathPart('search')
    : CaptureArgs(0)
{
    
}

sub view
    : Chained('base')
    : PathPart('')
    : Args(0)
    : ZAPI
{
    my ($self, $c ) = @_;
    
    $c->stash->{zapi} = json_get_contents($c->config->{root} . '/../frontend/html/test/core/crud/data/crud-data.json')->{result};   
}

sub list
    : Chained('base')
    : PathPart('list')
    : Args(1)
    : ZAPI
{
    
    my($self, $c, $type) = @_;
    
    $c->stash->{zapi} = json_get_contents($c->config->{root} . '/../frontend/html/test/widget/data/searches.json')->{result};   
}

sub grouping
    : Chained('base')
    : PathPart('grouping')
    : Args(1)
    : ZAPI
{
    my($self, $c, $type) = @_;
    
    $c->stash->{zapi} = json_get_contents($c->config->{root} . '/../frontend/html/test/widget/data/grouping.json')->{result};
}

sub groups
    : Chained('base')
    : PathPart('groups')
    : Args(2)
    : ZAPI
{
    my($self, $c, $type, $grouping_field) = @_;
    
    my $json;
    
    if($grouping_field && $grouping_field ne 'all') {
        $json = 'groups';
    } else {
        $json = 'groups-all';
    }
    
    $c->stash->{zapi} = json_get_contents($c->config->{root} . '/../frontend/html/test/widget/data/' . $json  . '.json')->{result};   
}

sub columns
    : Chained('base')
    : PathPart('columns')
    : Args(1)
    : ZAPI
{
    my ( $self, $c, $type ) = @_;
    
    $c->stash->{zapi} = json_get_contents($c->config->{root} . '/../frontend/html/test/widget/data/columns.json')->{result};   
}

sub id
    : Chained('/api/base')
    : PathPart('search')
    : CaptureArgs(1)
{
    my ($self, $c, $search_id ) = @_;
    
    $c->stash->{search_id} = $search_id;
}

sub update
    : Chained('id')
    : PathPart('update')
    : Args(0)
    : ZAPI
{
    my ($self, $c ) =  @_;
    
    $c->stash->{zapi} = json_get_contents($c->config->{root}. '/../frontend/html/test/widget/data/search.json')->{result};   
}

sub delete
    : Chained('id')
    : PathPart('delete')
    : Args(0)
    : ZAPI
{
    my ($self, $c ) =  @_;
    
    $c->stash->{zapi} = [];
}

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut 
    
    
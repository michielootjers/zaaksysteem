package Zaaksysteem::Controller::API::Mail::Intake;

use strict;
use warnings;

use Email::Address;
use MIME::Parser;

use Data::Dumper;

use base 'Catalyst::Controller';

sub handle : Private {
    my ($self, $c) = @_;
    my $message = $c->req->params->{message};

    my $parser = MIME::Parser->new();
    $parser->extract_uuencode(1);
    my $entity = $parser->parse_data($message);

    my @parts = $entity->parts_DFS;

    my $potential_owner;
    for my $p (@parts) {
        if (!$potential_owner && $p->head->get('To')) {
            (my $to) = Email::Address->parse($p->head->get('To'));
            $potential_owner = $to->user;
        }

        # No body = never an attachment
        if (!$p->bodyhandle) {
            next;
        }

        my ($filename, $fh, $ext, $dir) = $c->controller('API::Mail')->_handle_part($p);

        # These imply a message body, not a file attachment.
        if ($filename =~ q/^msg-/) {
            next;
        }

        # Attempt to find a user. If a user is found, make sure that the filename no longer 
        # contains the username.
        my $subject;
        my $rs = $c->model('DB::Subject')->search_active(
            { username => $potential_owner },
        );
        if ($rs->count) {
            $subject = $rs->next->betrokkene_identifier;
        }

        # If this matches, no matching user can be found and it doesn't match the global intake user. If so
        # we return silently. A die or exception will cause the mail to be sent to us again.
        my ($intake_user) = $c->model('DB::Config')->get('document_intake_user');
        if (!$subject && $potential_owner ne $intake_user) {
            return 1;
        }

        # Insert the file into the filestore
        my $result = $c->model('DB::File')->file_create({
            db_params => {
                intake_owner => $subject,
                created_by   => $subject || 'Onbekend',
            },
            name              => "$filename$ext",
            file_path         => "$dir$filename$ext",

        });
    }
    $c->res->body('ok');
}

1;
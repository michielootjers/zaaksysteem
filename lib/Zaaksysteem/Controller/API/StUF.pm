package Zaaksysteem::Controller::API::StUF;

use strict;
use warnings;

use Data::Dumper;
use Zaaksysteem::Backend::Sysin::Modules;

use Scalar::Util qw/blessed/;

use XML::LibXML;

use Moose;

use constant COMPONENT_KENNISGEVING => 'kennisgeving';
use constant MODEL_SBUS             => 'SBUS';

use utf8;

BEGIN {extends 'Zaaksysteem::General::SOAPController'; }

### Here for backwards compatible reasons
sub bg0204 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c)      = @_;

   $c->forward('endpoint');
}

sub stuf0204 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c)      = @_;

   $c->forward('endpoint');
}

sub stuf0301 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c)      = @_;

   $c->forward('endpoint');
}


sub endpoint : Private {
   my ( $self, $c)      = @_;

   unless ($self->verify_authorization($c)) {
        $c->stash->{soap}->fault(
           {
               code     => '403',
               reason   => 'Forbidden',
               detail   => 'AUTHORIZATION FAILURE: Invalid SSL Certificate'
           }
        );

        return;
    }

    my $xml              = $c->stash->{soap}->envelope();
    if (my $return_xml = $c->forward('handle_kennisgeving', [ $xml ])) {
        $c->stash->{soap}->literal_return($return_xml);
    }
}

sub verify_authorization {
    my ($self, $c, $xml)    = @_;

    ### SKIP SSL AUTHENTICATION, TMP
    unless (
        defined($c->engine->env->{SSL_CLIENT_VERIFY}) &&
        defined($c->customer_instance->{start_config}->{SBUS}->{issuer})
    ) {
        $c->log->error(
            'SBUS Authorization: SKIP'
        );
        return 1;
    }

    if ($c->engine->env->{SSL_CLIENT_VERIFY} ne 'SUCCESS') {
        $c->log->error(
            'SBUS Authorization: Failed: SSL_CLIENT_VERIFY: '
            .  $c->engine->env->{SSL_CLIENT_VERIFY}
        );

        return;
    }

    if ($c->engine->env->{SSL_CLIENT_VERIFY} eq 'SUCCESS') {
        $c->log->error(
            'SBUS Authorization: VERIFY OK: '
            .  $c->engine->env->{SSL_CLIENT_VERIFY}
        );

        my $conf_issuers    = $c
                            ->customer_instance
                            ->{start_config}
                            ->{SBUS}
                            ->{issuer};

        my @issuers         = (
            UNIVERSAL::isa($conf_issuers, 'ARRAY')
                ?  @{ $conf_issuers }
                : ( $conf_issuers )
        );

        for my $issuer (@issuers) {
            next unless $issuer eq $c->engine->env->{SSL_CLIENT_S_DN};

            $c->log->error(
                'SBUS Authorization: SUCCESS: CLIENT OK: '
                    . $c->engine->env->{SSL_CLIENT_S_DN}
            );

            return 1;

        }

        $c->log->error(
            'SBUS Authorization: FAILED: COULD NOT FIND ISSUER: '
                . $c->engine->env->{SSL_CLIENT_S_DN}
        );
    }

    return;
}

sub handle_kennisgeving : Private {
    my ($self, $c, $xml)    = @_;

    ### Detect entiteittype
    my ($entiteittype)      = $xml =~ /entiteittype.*?>(.*?)<\/(\w+:)?entiteit/si;

    unless ($entiteittype) {
        $c->log->error('Entiteittype not found');
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'Entiteittype not found',
                detail   => 'Entiteittype could not be recognized'
            }
        );

        return;
    }

    my ($module)            = Zaaksysteem::Backend::Sysin::Modules
                            ->find_module_by_id('stuf' . lc($entiteittype));

    my $interface;
    if ($module) {
        $interface          = $c->model('DB::Interface')->search(
            module          => $module->name,
        )->first;
    }

    unless ($module && $interface) {
        $c->log->error('Module or interface not found: ' . 'stuf' . lc($entiteittype));
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not found',
                detail   => 'StUF module for ' . $entiteittype . ' not found, be sure to configure it',
            }
        );

        return;
    }

    unless ($interface->active) {
        $c->log->error('Interface not active: ' . 'stuf' . lc($entiteittype));

        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not active',
                detail   => 'StUF module for ' . $entiteittype . ' not active, be sure to configure it',
            }
        );

        return;
    }

    $c->log->debug('Dispatching to interface');

    my ($response, $transaction);
    eval {
        $transaction        = $interface->process({
            input_data                  => $xml,
            external_transaction_id     => 'unknown',       # Will be replaced later
        });

        if ($transaction->transaction_records->count == 1) {
            my $record      = $transaction->transaction_records->first;

            $response       = $record->output;
        }
    };

    if ($@ || !$response) {
        ### XXX Unreadable logic:
        ### If $@ error, show error. Else, if transaction, show transaction_id
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'Transaction could not be processed',
                detail   => 'Transaction could not be processed: '
                    . (
                        $@
                            ? $@
                            : (
                                $transaction
                                    ? 'transaction id: ' . $transaction->id
                                    : ''
                            )
                        )
            }
        );

        return;
    }


    if (
        $response &&
        (
            $response =~ /^Error: / ||
            $response =~ /xml version/
        )
    ) {
        $response =~ s/^Error: //;
        $response =~ s/^.*?<\?xml versio/<?xml versio/;
        $response =~ s/> at .* line \d+$/>/;
        $c->log->debug('Response: ' . Data::Dumper::Dumper($response));
        return XML::LibXML->load_xml(string => $response)->documentElement();
    }

    $c->stash->{soap}->fault(
        {
            code     => '500',
            reason   => 'No response given',
            detail   => 'Unknown error occured'
        }
    );

    return;
}

# sub manual : Local {
#     my ($self, $c) = @_;

#     $c->assert_any_user_permission('admin');

#     $c->stash->{template} = 'beheer/stuf.tt';

#     if ($c->req->params->{stuf_message}) {
#         my $stuf                = $c->model(MODEL_SBUS);

#         my $response            = $stuf->response(
#             {
#                 sbus_type   => 'StUF',
#                 input_raw   => $c->req->params->{stuf_message},
#             }
#         );

#         my $return = $stuf->compile_response(
#             {
#                 response    => $response,
#                 sbus_type   => 'StUF',
#             }
#         );

#         if ($return) {
#             $c->stash->{success} = 1;

#             my $tidy    = XML::Tidy->new(xml => $return->toString);
#             my $xml     = $tidy->tidy()->toString();

#             $c->stash->{return_message} = $xml;
#         }
#     }

#     $c->detach('View::TT');
# }

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


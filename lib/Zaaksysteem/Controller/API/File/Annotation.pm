package Zaaksysteem::Controller::API::File::Annotation;

use Moose;
use Data::Dumper;
use JSON;
use Try::Tiny;

use Zaaksysteem::Exception;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'}


sub save : Chained('/api/file/base') : PathPart('annotation/save') : ZAPI {
    my ($self, $c) = @_;

    $c->forward('do_save');
}


sub delete : Chained('/api/file/base') : PathPart('annotation/delete') : ZAPI {
    my ($self, $c) = @_;

    $c->forward('do_delete');
}


sub do_save : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $annotation = $c->model('DB::FileAnnotation')->update_or_create({
        id => $params->{id},
        file_id => $c->stash->{file}->id,
        subject => $self->subject($c),
        properties => $params
    });

    $c->stash->{zapi} = [$annotation->properties];
}


sub do_delete : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $annotation = $c->model('DB::FileAnnotation')->search({
        id => $params->{id},
        file_id => $c->stash->{file}->id,
        subject => $self->subject($c)
    })->delete;

    $c->stash->{zapi} = [{
        message => 'annotation deleted'
    }];
}

sub subject {
    my ($self, $c) = @_;

    throw('case/file/annotation/no_user', 'user is not logged') unless $c->user_exists;

    return 'betrokkene-medewerker-' . $c->user->uidnumber;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


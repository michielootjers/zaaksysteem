package Zaaksysteem::Controller::API::Mail;

use strict;
use warnings;

use File::Basename;

use parent 'Catalyst::Controller';

sub intake_process : Local {
    my ($self, $c) = @_;

	# Test mail with 3 attachments. Remove if it's in the way or no longer useful.
#     $c->req->params->{message} = <<'MAILEND';
# MIME-Version: 1.0
# Received: by 10.216.169.201 with HTTP; Thu, 24 Oct 2013 06:30:45 -0700 (PDT)
# Date: Thu, 24 Oct 2013 15:30:45 +0200
# Delivered-To: marco@mintlab.nl
# Message-ID: <CA+7xZ7ftGsG9frS+XWh6it3j4JkMYm-_A=sjeY8jZrXm-qtLug@mail.gmail.com>
# Subject: Banana
# From: Marco Baan <marco@mintlab.nl>
# To: Marco Baan <mklaveren@quarterly.zaaksysteem.nl>
# Content-Type: multipart/mixed; boundary=001a11336f3e22bd9004e97ca4de

# --001a11336f3e22bd9004e97ca4de
# Content-Type: multipart/alternative; boundary=001a11336f3e22bd7b04e97ca4dc

# --001a11336f3e22bd7b04e97ca4dc
# Content-Type: text/plain; charset=ISO-8859-1



# --001a11336f3e22bd7b04e97ca4dc
# Content-Type: text/html; charset=ISO-8859-1

# <div dir="ltr"><br></div>

# --001a11336f3e22bd7b04e97ca4dc--
# --001a11336f3e22bd9004e97ca4de
# Content-Type: text/plain; charset=US-ASCII; name="test1.txt"
# Content-Disposition: attachment; filename="mkla-test1.txt"
# Content-Transfer-Encoding: base64
# X-Attachment-Id: f_hn613t1y0

# VGVzdAo=
# --001a11336f3e22bd9004e97ca4de
# Content-Type: text/plain; charset=US-ASCII; name="test2.txt"
# Content-Disposition: attachment; filename="mklaver-test2.txt"
# Content-Transfer-Encoding: base64
# X-Attachment-Id: f_hn613t2c1

# VGVzdDIK
# --001a11336f3e22bd9004e97ca4de
# Content-Type: image/jpeg; name="test3.jpg"
# Content-Disposition: attachment; filename="mklaveren-test3.jpg"
# Content-Transfer-Encoding: base64
# X-Attachment-Id: f_hn613t2l2

# QmxhCg==
# --001a11336f3e22bd9004e97ca4de--
# MAILEND


    die('No message found') unless $c->req->params->{message};

    eval {
        $c->forward('/api/mail/intake/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
        $c->res->body($@);
    }
    else {
        $c->res->body('ok');
    }
}

sub input : Local {
    my ($self, $c) = @_;

    die('No message found') unless $c->req->params->{message};

    $c->log->debug(
        'API::Mail->input: MAIL handler'
    );

    eval {
        $c->forward('/api/mail/rci/handle');
        $c->forward('/api/mail/yucat/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
    }

    $c->res->body('ok');
}

sub _retrieve_files {
    my ($self, $message) = @_;

    my $parser = new MIME::Parser;

    $parser->extract_uuencode(1);

    my $entity = $parser->parse_data($message);

    return ($parser, $entity->parts_DFS);
}

sub _handle_part {
    my ($self, $part) = @_;

    my $raw_filename = $part->head->recommended_filename;
    if (!$raw_filename) {
        $raw_filename = $part->bodyhandle->path;
    }

    my ($filename, $dir, $ext) = fileparse($raw_filename, '\.[^.]*');

    return ($filename, $part->bodyhandle, $ext, $dir);
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


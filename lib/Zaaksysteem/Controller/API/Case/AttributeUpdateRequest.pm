package Zaaksysteem::Controller::API::Case::AttributeUpdateRequest;

use Data::Dumper;
use Moose;
use Zaaksysteem::Exception;

BEGIN { extends 'Catalyst::Controller::REST' }


# this sets up the other verbs
sub approve       : Local : ActionClass('REST') {}
sub reject        : Local : ActionClass('REST') {}
sub phaserequests : Local : ActionClass('REST') {}


sub phaserequests_GET {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($params->{case_id});
    $c->assert_any_zaak_permission('zaak_edit');

    my $phase_id = $params->{phase_id} or die "need phase_id";

    my $result = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerken->search_update_field_tasks({
        phase_id => $phase_id,
        case     => $c->stash->{zaak}
    });

    # response to the frontend
    $c->detach('response', [200, {result => [scalar keys %$result ] }]);
}


sub approve_POST {
    my ($self, $c) = @_;

    $c->forward('handle_update', ['approve']);
}


sub reject_POST {
    my ($self, $c) = @_;

    $c->forward('handle_update', ['reject']);
}


sub handle_update : Private {
    my ($self, $c, $action) = @_;

    my $params = $c->req->params;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($params->{case_id});
    $c->assert_any_zaak_permission('zaak_edit');

    # find the queue of changes to this field
    my $scheduled_jobs = $c->model('DB::ScheduledJobs')->search_update_field_tasks({
        case_id   => $params->{case_id},
        kenmerken => [$params->{bibliotheek_kenmerken_id}]
    });

    my $most_recent = $self->process_most_recent_update($scheduled_jobs, $action);

    $self->log_request_handling($c, $most_recent, $action);

    # then invalidate that one and all the others
    $scheduled_jobs->set_deleted;

    # response to the frontend
    $c->detach('response', [200, { result => [{
        type    => 'case/approve/success',
        message => 'Update request has been handled succesfully'
    }] }]);
}


=head2 process_most_recent_update

citizen may send multiple updates, this process the last one,
ignore the rest

note: move to scheduled_jobs resultset?

=cut

sub process_most_recent_update {
    my ($self, $scheduled_jobs, $action) = @_;

    # process the most recent one
    my @recent = $scheduled_jobs->only_most_recent_field_update;

    throw('api/case/attributeupdaterequest', "expected only one result")
        unless scalar @recent == 1;

    my ($most_recent) = @recent;

    # either approve or reject
    throw('api/case/attributeupdaterequest', "unsupported action")
        unless $action =~ m/^(approve|reject)$/;

    $most_recent->apply_roles->$action;

    return $most_recent;
}


sub log_request_handling {
    my ($self, $c, $scheduled_job, $action) = @_;

    my $parameters = $scheduled_job->parameters;

    # When fields are used multiple times in a casetype, unexpected results
    # may happen here. the cure would be identify the phase, which means
    # a database change at the field storage level. zaak_kenmerk must start
    # working with a zaaktype_kenmerken_id instead of a bibliotheek_kenmerken_id.
    # for now i'm operating in the oblivious assumption that a field will only
    # be used once per casetype.
    my $kenmerk = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerkens->search({
        bibliotheek_kenmerken_id => $parameters->{bibliotheek_kenmerken_id}
    })->first;

    my $label = $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam;

    my $new_values = $kenmerk->bibliotheek_kenmerken_id->value_type =~ m|^bag| ?
        $c->model('Gegevens::Bag')->humanize($parameters->{value}) :
        $parameters->{value};

    $c->model('DB::Logging')->trigger(
        'case/update/piprequest', {
            component => 'zaak',
            zaak_id   => $parameters->{case_id},
            data => {
                action      => $action,
                kenmerk     => $label,
                new_values  => $new_values,
                toelichting => $parameters->{reason}
            }
        }
    );
}



sub response : Private {
    my ($self, $c, $status, $entity) = @_;

    $c->response->status($status);
    $self->_set_entity($c, $entity);
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Controller::API::User::Annotations;

use Moose;
use Data::Dumper;
use JSON;
use Try::Tiny;

use Zaaksysteem::Exception;
use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'}


=head2 clear

Clear all annotations for a user. The idea is that when a user leaves
the organisation this is a tool so they can clear up their personal
notes.

=cut

sub clear : Local : ZAPI {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    throw('api/user/annotations/method_not_post', 'method should be POST')
        unless $c->req->method eq 'POST';

    # extra check, the user will not get here per the default settings in Page.pm.
    # but in case of future changes this function will still be protected.
    throw('api/user/annotations/user_not_logged_in', 'user not logged in')
        unless $c->user_exists;

    # since this thing is pretty powerful and accessible through a simple call,
    # we add an extra parameter so the GUI needs to be damn sure before committing
    throw('api/user/annotations/confirm_missing', 'confirm missing')
        unless $params->{confirm};

    my $rs = $c->model('DB::FileAnnotation')->search({
        subject => 'betrokkene-medewerker-' . $c->user->uidnumber
    });

    # give the GUI an opportunity to show how many items have been cleared.
    my $count = $rs->count;

    $rs->delete; 

    # We can't log which notes on which files were cleared, since that
    # would defeat the purpose of cleaning up - this is an extermination
    # process so no trace are left.
    # It is up to the user interface to make this not to easy.
    $c->model('DB::Logging')->trigger('user/annotations/clear', {
        component => LOGGING_COMPONENT_USER,
        data => {}
    });

    $c->stash->{zapi} = [{
        type    => 'user/annotations/clear',
        message => 'All annotations have been cleared',
        count   => $count
    }];
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


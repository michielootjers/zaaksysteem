package Zaaksysteem::Controller::API::User::Password;

use strict;
use Data::Dumper;

use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

# this sets up the other verbs
sub password : Path : ActionClass('REST') {
    my ($self, $c) = @_;

    # since this somehow bypasses the normal loggin
    $c->log->debug("---------------\nREST request - api/user/password");
}

sub password_POST {
    my ($self, $c)     = @_;

    my $params          = $c->req->params;

    # yup, sorry nog maar een keer mergen :|
    my $username = $c->user->cn;

    $c->detach('response', [500, {
        type    => 'user/password/cant_change_admin_password',
        message => 'Not allowed to change admin password'
    }]) if $username eq 'admin';

    my $reauthenticated = $c->authenticate({
        username => $username,
        password => $params->{old_password}
    });

    $c->detach('response', [500, {
        type    => 'user/password/wrong_credentials',
        message => 'Incorrect password given, could not authenticate'
    }]) unless $reauthenticated;


    my $result = $c->model('Users')->change_password({
        new_password    => $params->{new_password},
        username        => $c->user->cn
    });

    if ($result) {

        $c->model('DB::Logging')->trigger('user/password/update', {
            component => LOGGING_COMPONENT_USER,
            data => {}
        });

        $c->detach('response', [200, {
            type    => 'user/password/success',
            message => 'Password has been changed'
        }]);

    } else {

        $c->detach('response', [500, {
            type    => 'user/password/unknown_error',
            message => 'Could not change password'
        }]);

    }
}


sub response : Private {
    my ($self, $c, $status, $entity) = @_;

    $c->response->status($status);
    $self->_set_entity($c, {result => [$entity]});
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


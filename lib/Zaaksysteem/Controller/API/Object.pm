package Zaaksysteem::Controller::API::Object;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::Constants qw[OBJECT_ACTIONS];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Object - Zaaksysteem Object API

=head1 SYNOPSIS


=head1 DESCRIPTION

A generic API for querying Zaaksysteem Objects.

=head1 API

For a basic knowledge about our JSON API, please take a look at
L<Zaaksysteem::Manual::API>

=cut

sub base : Chained('/') : PathPart('api/object') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # TODO: Some some permission-related things here? User logged in?
    # or perhaps a source ip check? Anything at all besides checking for
    # admin rights?
}

=head2 search

L<http://10.44.0.11/api/object>

    {
       "next" : "http://10.44.0.11/api/object?zql=select+%7B%7D+from+case&zapi_page=2",
       "status_code" : "200",
       "prev" : null,
       "num_rows" : "2",
       "rows" : 2,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "object_type" : "case",
             "values" : {
                "zaaktype.titel" : "Testzaaktype",
                "test_02" : "25-01-2013",
                "test_03" : [
                   "Optie A"
                ],
                "test_04" : "09",
                "test_01" : [
                   "Optie 3"
                ],
                "case.registratiedatum" : "2013-01-24T14:09:56",
                "omschrijving" : "testest",
                "test_09" : [
                   "Nee"
                ],
                "test_08" : "testuleer",
                "case.id" : 2,
                "case.parent_case": [     #### XXX FIXME TODO (Relationships)
                  {
                     "object_type" : "case",
                     "values" : {
                        "zaaktype.titel" : "Testzaaktype",
                        "test_02" : "25-01-2013",
                        "test_03" : [
                           "Optie A"
                        ],
                        "test_04" : "09",
                        "test_01" : [
                           "Optie 3"
                        ],
                        "case.registratiedatum" : "2013-01-24T14:09:56",
                        "omschrijving" : "testest",
                        "test_09" : [
                           "Nee"
                        ],
                        "test_08" : "testuleer",
                        "case.id" : 7,
                        "case.parent_case": null,
                        "case.afhandeldatum" : null,
                        "test_07" : [
                           "Nee"
                        ],
                        "toelichting" : null
                     },
                     "object_id" : 7
                  },
                ],
                "case.afhandeldatum" : null,
                "test_07" : [
                   "Nee"
                ],
                "toelichting" : null
             },
             "object_id" : 2
          },
          {
             "object_type" : "case",
             "values" : {
                "zaaktype.titel" : "Testzaaktype",
                "test_02" : "25-01-2013",
                "test_03" : [
                   "Optie A"
                ],
                "test_04" : "09",
                "test_01" : [
                   "Optie 3"
                ],
                "case.registratiedatum" : "2013-01-24T14:09:56",
                "omschrijving" : "testest",
                "test_09" : [
                   "Nee"
                ],
                "test_08" : "testuleer",
                "case.id" : 3,
                "case.afhandeldatum" : null,
                "test_07" : [
                   "Nee"
                ],
                "toelichting" : null
             },
             "object_id" : 3
          }
       ]
    }

Returns a resultset of objects for the given type. Now, only the object type
case is here to use.

=cut

define_profile search => (
    required => [qw[zql]],
    typed => {
        zql => 'Str'
    }
);

sub search : Chained('base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $query = assert_profile($c->req->params)->valid->{ zql };

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    die "not a select" unless $zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

    if($zql->cmd->distinct) {
        my $opts = $zql->cmd->dbixify_opts;

        my @results;

        my $tr = sub { my $val = shift; $val =~ s/\$/./g; return $val };

        for my $object ($rs->all) {
            push @results, {
                count => int($object->get_column('count')),
                map { $tr->($_) => $object->get_column($_) } @{ $opts->{ as } }
            };
        }

        $rs = \@results;
    }

    $c->stash->{ zapi } = $rs;

    if (grep { exists $_->{order_by} } @{ $zql->cmd->opts }) {
        $c->req->mangle_params(
            {
                zapi_order_by           => undef,
                zapi_order_by_direction => undef,
            }
        );
    }

    if ($c->req->param('describe')) {
        warn "ZQL query with GET parameter 'describe' == trueish is deprecated. Please use the SELECT WITH DESCRIPTION syntax of ZQL.";

        $c->stash->{zapi}->describe_rows(1);
        #$c->log->debug(Data::Dumper::Dumper($c->stash->{zapi}->_zql_options));
    }

}

=head2 object_actions

This action allows for the retrieval of all user actions possible on a specific
object type.

=head3 URL 

C</api/objects/actions>

=head3 PARAMETERS

=over 4

=item type

Type string, required.

=back

=head3 EXAMPLE

A retrieval for type C<case> may looks like this:

    {
        "next": null,
        "status_code": "200",
        "prev": null,
        "num_rows": 10,
        "rows": 10,
        "comment": null,
        "at": null,
        "result": [
            {
                "label": "Hervatten",
                "path": "/bulk/update/resume",
                "id": "resume"
            },
            {
                "label": "Relateren",
                "path": "/bulk/update/relatie",
                "id": "relate"
            },
            {
                "label": "Exporteren",
                "path": null,
                "id": "export"
            },
            .
            .
            .
        ]
    }

=head3 FORMAT

Each action comes with a C<id>, C<label>, and C<path> attribute.

The C<id> attribute is our main internal reference for the action.

C<path> has a special case where the path to be used is ZQL search URL
for the current view if the value for it is C<null>.

=cut

define_profile object_actions => (
    required => {
        type => 'Str'
    }
);

sub object_actions : Chained('base') : PathPart('actions') : Args(0) : ZAPI {
    my $self = shift;
    my $c = shift;

    my $type = assert_profile($c->req->params)->valid->{ type };

    my $actions = OBJECT_ACTIONS()->{ $type } // {};

    my @result;

    for my $action (keys %{ $actions }) {
        push @result, {
            id => $action,
            %{ $actions->{ $action } }
        };
    }

    $c->stash->{ zapi } = \@result;
}

=head2 begin (Private)

Disable the object API completely if the environment variable
C<DISABLE_OBJECT_API> is set.

=cut

sub begin : Private {
    my ($self, $c) = @_;

    if ($ENV{DISABLE_OBJECT_API}) {
        $c->error('Object API is disabled');
        return 0;
    }

    return 1;
}

1;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


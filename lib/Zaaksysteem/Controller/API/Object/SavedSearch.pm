package Zaaksysteem::Controller::API::Object::SavedSearch;

use Moose;
use namespace::autoclean;

use JSON;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::SavedSearch - C(R)UD for saved searches

=head1 DESCRIPTION

Creation, update and deletion of saved (ZQL) searches.

Retrieval is done using ZQL and the /api/object interface.

=head1 API

=cut

sub base : Chained('/api/object/base') : PathPart('savedsearch') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # Some some permission-related things here?
}

=head2 create

Create a new saved search.

=head3 URL

B</api/object/saved_search/create>

=head3 POST variables

=over

=item * query

The query to save.

=back

=cut

define_profile create => (
    required => ['query', 'title'],
    typed => {
        query => 'Str',
        title => 'Str',
    },
);

sub create : Chained('base') : PathPart('create') : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $obj = $c->model('DB::ObjectData')->create(
        { object_class => 'saved_search' }
    );
    $obj->discard_changes();

    $obj->save_search_data(
        owner => $c->user->id,
        query => decode_json($opts->{query}),
        title => $opts->{title},
    );

    $c->stash->{zapi} = [$obj];

    return;
}

=head2 update

Update a saved search.

=head3 URL

B</api/object/saved_search/update>

=head3 POST variables

=over

=item * saved_search_id

Identifier of the search to update.

=item * query

New query to store.

=back

=cut

define_profile update => (
    required => ['saved_search_id', 'query'],
    optional => ['title'],
    typed => {
        saved_search_id => 'Str',
        query => 'Str',
        title => 'Str',
    },
);

sub update : Chained('base') : PathPart('update') : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $saved_search = $c->model('Object')->rs->search(
        {
            object_class => 'saved_search',
            uuid         => $opts->{saved_search_id}
        },
    )->single;
    
    if(!$saved_search) {
        throw('not_found/saved_search', 'No saved search found with that id');
    }

    $saved_search->save_search_data(
        owner => $c->user->id,
        query => decode_json($opts->{query}),

        $opts->{title}
            ? (title => $opts->{title})
            : (),
    );

    $c->stash->{zapi} = [$saved_search];

    return;
}

=head2 delete

Deletes a saved search.

=head3 URL

B</api/object/saved_search/delete>

=head3 POST variables

=over

=item * saved_search_id

Identifier of the search to delete.

=back

=cut

define_profile delete => (
    required => ['saved_search_id'],
    typed => {
        saved_search_id => 'Str',
    },
);

sub delete : Chained('base') : PathPart('delete') : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $saved_search = $c->model('Object')->rs->search(
        {
            object_class => 'saved_search',
            uuid         => $opts->{saved_search_id}
        },
    )->single;
    
    if(!$saved_search) {
        throw('not_found/saved_search', 'No saved search found with that id');
    }

    $saved_search->delete;

    $c->stash->{zapi} = [];

    return;
}

__PACKAGE__->meta->make_immutable;

1;

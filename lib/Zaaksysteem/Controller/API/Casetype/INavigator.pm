package Zaaksysteem::Controller::API::Casetype::INavigator;

use Moose;
use Data::Dumper;
use Hash::Merge qw/merge/;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }


sub do_import : Local : ZAPI {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $casetype_id = $c->model('Zaaktypen')->import_inavigator_casetype($params);

    # flush session in case somebody has been editing this.
    delete $c->session->{zaaktypen}->{$casetype_id};

    $c->forward('casetype_info', [$casetype_id]);
}



# provide a lookup for attributes in this casetype
sub casetype_info : Local : Args(1) : ZAPI {
    my ($self, $c, $casetype_id) = @_;

    $c->forward('existing_casetypes');

    $c->stash->{zapi} = [{
        message => 'I-Navigator zaaktype geimporteerd',
        casetype => $c->model('Zaaktypen')->retrieve_session($casetype_id),
        existing_kenmerken => [$self->existing_kenmerken($c)],
        existing_categories => [$c->model('DB::BibliotheekCategorie')->tree],
        existing_casetypes => $c->stash->{existing_casetypes},
    }];
}


# TODO settings are in session now, need to be moved to general settings
sub restore_default_settings : Local : ZAPI {
    my ($self, $c) = @_;

    my $code = $c->req->params->{code} or throw('api/inavigator/missing_code', 'Zaaktypecode is verplicht');

    $c->user->remove_inavigator_settings($code);

    $c->stash->{zapi} = [{
        settings => $c->user->settings->{inavigator} || {},
        message => 'Instellingen teruggezet naar de beginstand.'
    }];
}


sub save_settings : Local : ZAPI {
    my ($self, $c) = @_;

    my $current = $c->user->settings->{inavigator_settings} || {};
    my $merged = merge $c->req->params, $current;
    $c->user->modify_setting('inavigator_settings', $merged);

    $c->stash->{zapi} = [{
        message => 'Instellingen zijn bewaard.'
    }];
}


sub existing_kenmerken : Private {
    my ($self, $c) = @_;

    my @existing = $c->model('DB::BibliotheekKenmerken')->search({
        'me.value_type' => 'file',
        'me.deleted'    => undef,
    })->all;

    return map {{
        id => $_->id,
        naam => $_->naam,
    }} @existing;
}


sub existing_casetypes : Private {
    my ($self, $c) = @_;

    my @existing = $c->model('DB::Zaaktype')->search({
        'zaaktype_node_id.deleted' => undef,
        'me.deleted'               => undef,
    }, {
        join => 'zaaktype_node_id',
        order_by => 'zaaktype_node_id.titel'
    })->all;

    $c->stash->{existing_casetypes} = [map {
        {
            id => $_->id,
            name => $_->zaaktype_node_id->titel,
            code => $_->zaaktype_node_id->code,
        }
    } @existing];
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


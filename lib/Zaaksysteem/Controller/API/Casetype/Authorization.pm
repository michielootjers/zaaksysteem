package Zaaksysteem::Controller::API::Casetype::Authorization;

use Moose;
use Data::Dumper;
use JSON;
use Zaaksysteem::Exception;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }


sub get : Local : Args: ZAPI {
    my ($self, $c, $casetype_id) = @_;

    # redundant security check, the first line of defense must be Page.pm
    throw ('api/casetype/authorization', 'Access violation') unless $c->user_exists;

    $c->stash->{zapi} = [$self->get_authorizations($c, $casetype_id)];
}


sub post : Local : ZAPI {
    my ($self, $c, $casetype_id) = @_;

    # redundant security check, the first line of defense must be Page.pm
    throw ('api/casetype/authorization', 'Access violation')
        unless $c->user_exists;

    throw ('api/casetype/authorization/method_not_post', 'only POST requests allowed')
        unless $c->req->method eq 'POST';

    my $authorizations;
    eval {
        $authorizations = JSON::decode_json($c->req->params->{authorizations});
    };
    if ($@) {
        throw ('api/casetype/authorization', 'incorrect json format input: ' . $@);
    }

    # input validation
    throw ('api/casetype/authorization/incorrect_input', "incorrect authorizations, must be arrayref")
        unless $authorizations && ref $authorizations && ref $authorizations eq 'ARRAY';

    my $casetype = $self->session_casetype($c, $casetype_id);

    # the whole index deal is legacy, deal with it :)
    my $index = 0;
    $casetype->{authorisaties} = { map {
        assert_valid_entry($_);
        $index += 1;
        $index => $_
    } @$authorizations };

    $c->stash->{zapi} = [$casetype->{authorisaties}];
}


sub assert_valid_entry {
    my ($entry) = @_;

    map {
        throw ('api/casetype/authorization',
            'Incorrect fields, each authorization must have ' .
            'ou_id, role_id, recht, confidential. got: ' . Dumper($entry))
        unless exists $entry->{$_}
    } qw/confidential ou_id role_id recht/;
}


sub session_casetype {
    my ($self, $c, $casetype_id) = @_;

    return $c->session->{zaaktypen}->{$casetype_id} ||=
        $c->model('Zaaktypen')->retrieve(
            id         => $casetype_id,
            as_session => 1,
        ) or die "unable to load case type into session";
}


sub get_authorizations {
    my ($self, $c, $casetype_id) = @_;

    my $casetype = $self->session_casetype($c, $casetype_id);

    map {{
        recht        => $_->{recht},
        ou_id        => $_->{ou_id},
        role_id      => $_->{role_id},
        confidential => $_->{confidential},
    }} values %{ $casetype->{authorisaties} };
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


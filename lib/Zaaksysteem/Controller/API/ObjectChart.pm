package Zaaksysteem::Controller::API::ObjectChart;
use Moose;
use namespace::autoclean;

# Temporary API for graphs using the new object_data table instead of zaak.

use Zaaksysteem::Exception;
use Zaaksysteem::ObjectChartGenerator;
use Zaaksysteem::Profile;

BEGIN { extends 'Catalyst::Controller'; }

sub base : Chained('/') : PathPart('api/object_chart') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
    # Some some permission-related things here?
}

define_profile chart => (
    required => [qw(chart_profile zql)],
    typed => {
        chart_profile => 'Str',
        zql => 'Str',
    },
);

sub chart : Chained('base') : PathPart("") : Args(0) {
	my ($self, $c) = @_;

	my $params = assert_profile($c->req->params)->valid;

    my $zql = Zaaksysteem::Search::ZQL->new($params->{zql});
    die "not a select" unless $zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select');

    my $resultset = $zql->apply_to_resultset($c->model('Object')->rs);

    my $chartgenerator = Zaaksysteem::ObjectChartGenerator->new({ resultset => $resultset });
    my $chart_profile = $chartgenerator->generate({ profile => $params->{chart_profile} });

    my $lookup = $self->departments_lookup($c);

    if($params->{chart_profile} eq 'cases_per_department') {
        my $categories = [
            map { 
                $lookup->{$_} || 'Afdeling '. $_
            } @{ 
                $chart_profile->{xAxis}->{categories}
            } 
        ];
        $chart_profile->{xAxis}->{categories} = $categories;
    } elsif (
        $params->{chart_profile} eq 'cases_per_department_per_month' ||
        $params->{chart_profile} eq 'cases_within_and_outside_term_per_month_per_department'
    ) {
        my $series = $chart_profile->{series};
        foreach my $part (@$series) {
            my $department = $part->{name};
            $part->{name} = $lookup->{$department} || 'Afdeling ' . $department;
        }
    }

    $c->stash->{json} = $chart_profile;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

sub departments_lookup {
    my ($self, $c) = @_;

    my $ous = $c->model('Groups')->search_ou;

    return { map { $_->{id} => $_->{ou} } @$ous };
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


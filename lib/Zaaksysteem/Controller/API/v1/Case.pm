package Zaaksysteem::Controller::API::v1::Case;

use Moose;

use Zaaksysteem::Types qw[
    Boolean
    UUID
    NonEmptyStr
    TelephoneNumber
    MobileNumber
    EmailAddress
];

use Zaaksysteem::Tools;
use Zaaksysteem::Tools::HashMapper;

use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::API::v1::PreparedFileBag;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];
use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Search::ESQuery;

use Moose::Util::TypeConstraints qw[enum union];
use List::Util qw[first];
use DateTime;
use DateTime::Format::DateParse;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern/] }
);

has 'api_control_module_types' => (
    is          => 'rw',
    default     => sub { ['api', 'app'] },
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Case - APIv1 controller for case objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('case') : CaptureArgs(0) : Scope('case') {
    my ($self, $c) = @_;
    my $additional_search_params;

    if ($c->req->params->{ es_query }) {
        my %query = map { $_ => $c->req->params->{ $_ } } grep {
            $_ =~ m[^query\:]
        } keys %{ $c->req->params };

        unless (scalar keys %query) {
            throw('api/v1/case/es_query_fault', sprintf(
                'Please specify a ElasticSearch query using URL mapped JSON with the "query:" root'
            ));
        }

        $c->stash->{ es_query } = Zaaksysteem::Search::ESQuery->new(
            query => inflate(\%query, nesting_separator => ':'),
            object_type => 'case'
        );
    } elsif ($c->req->params->{ zql }) {
        $c->stash->{ zql } = Zaaksysteem::Search::ZQL->new(
            $c->req->params->{ zql }
        );
    } else {
        $c->stash->{ zql } = Zaaksysteem::Search::ZQL->new(
            'SELECT {} FROM case'
        );
    }

    if ($c->stash->{ zql } && $c->stash->{zql}->cmd->from->value ne 'case') {
        throw('api/v1/case/query_fault', sprintf(
            'Cannot parse ZQL, only objecttype "case" is allowed.'
        ));
    }

    my $interface = $c->stash->{ interface };
    my $model = $c->model('Object');
    my $base_rs = $model->rs;

    my $query_constraint = eval { $interface->jpath('$.query_constraint') };

    # Applies a base query constraint to the base_rs, if one is configured.
    if ($query_constraint) {
        # Ignore user permissions for finding the query constraint
        my $query = $model->inflate_from_row(
            $model->new_resultset->find($query_constraint->{ id })
        );

        unless (defined $query) {
            throw('api/v1/case/query_constraint_not_found', sprintf(
                'This API is configured with a constraint query, but the query object could not be found'
            ));
        }

        $base_rs = $query->zql->apply_to_resultset($base_rs);
    }

    my $set = try {
        my $iterator;

        if ($c->stash->{ zql }) {
            my $rs = $c->controller('API::Object')->_search_intake(
                $c,
                $c->stash->{ zql },
                $c->stash->{ zql }->apply_to_resultset($base_rs)
            );

            $iterator = Zaaksysteem::Object::Iterator->new(
                rs => $rs,
                inflator => sub { $model->inflate_from_row(shift) }
            );
        } elsif ($c->stash->{ es_query }) {
            $iterator = Zaaksysteem::Object::Iterator->new(
                rs => $c->stash->{ es_query }->apply_to_resultset($base_rs),
                inflator => sub { $model->inflate_from_row(shift) },
            );
        }

        unless ($iterator) {
            throw('api/v1/case/query_fault', sprintf(
                'Could not build search query from request.'
            ));
        }

        return Zaaksysteem::API::v1::Set->new(
            iterator => $iterator,
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        $_->throw if blessed $_;

        throw(
            'api/v1/case',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ set } = $set;

    $c->stash->{ cases } = $set->build_iterator->rs;

}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ case } = try {
        return $c->stash->{ cases }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    unless (defined $c->stash->{ case }) {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    unless ($c->stash->{ case }->object_class eq 'case') {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL Path

C</api/v1/case>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{ cases }
    )->init_paging($c->req);

    my $object_uuids_query = $c->stash->{ result }->build_iterator->get_column('uuid')->as_query;
    my $relations = $c->model('DB::ObjectRelationships')->search(
        {
            -or => [
                { object1_uuid => { -in => $object_uuids_query } },
                { object2_uuid => { -in => $object_uuids_query } },
            ]
        }
    );
    $c->stash->{ serializer_opts }{ object_relationships } = [ $relations->all ];

    return;
}

=head2 get

=head3 URL Path

C</api/v1/case/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ case };

    if ($c->stash->{ result }) {
        my $uuid = $c->stash->{ result }->get_column('uuid');
        my $relations = $c->model('DB::ObjectRelationships')->search(
            {
                -or => [
                    { object1_uuid => $uuid },
                    { object2_uuid => $uuid },
                ]
            }
        );
        $c->stash->{ serializer_opts }{ object_relationships } = [ $relations->all ];
    }

}

=head2 casetype

=head3 URL Path

C</api/v1/case/[UUID]/casetype>

=cut

sub casetype : Chained('instance_base') : PathPart('casetype') : Args(0) : RO {
    my ($self, $c) = @_;

    # Prevent new query, use prefetched results
    my @relations = $c->stash->{ case }->object_relation_object_ids->all;

    my $relation = first { $_->object_type eq 'casetype' } @relations;

    # Safeguard for untouched case object_data rows since the introduction
    # of casetype embedded relations.
    unless (defined $relation) {
        my $zaak = $c->stash->{ case }->get_source_object;

        $relation = $zaak->_rewrite_object_casetype($c->stash->{ case });
    }

    $c->stash->{ result } = try {
        $c->model('Object')->inflate_from_relation($relation);
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/casetype/retrieval_error', 'No casetype')
    };
}

=head2 create

=head3 URL Path

C</api/v1/case/create>

=cut

define_profile create => (
    required => {
        casetype_id => UUID,
        source => enum(ZAAKSYSTEEM_CONSTANTS->{ contactkanalen }),
    },
    optional => {
        values => 'HashRef',
        requestor => 'HashRef',
        recipient => 'HashRef',
        assignee => 'HashRef',
        route => 'HashRef',
        confidentiality => enum(['public', 'internal', 'confidential']),
        open => Boolean,
        contact_details => 'HashRef',
        subjects => 'HashRef',
        files => 'HashRef',
    },
    defaults => {
        values => sub { return {} },
        open => 0
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;
    my %values = %{ $params->{ values } };

    my $casetype = try {
        $c->model('Object')->retrieve(uuid => $params->{ casetype_id })
    } catch {
        $c->log->debug($_);

        throw(
            'api/v1/case/retrieval_fault',
            'Fault during casetype retrieval, unable to continue'
        );
    };

    unless (defined $casetype) {
        throw(
            'api/v1/case/casetype_not_found',
            sprintf(
                'Casetype "%s" could not be found, unable to continue',
                $params->{ casetype_id }
            ),
            { http_code => 400 } # sic, 400 indicates client fault
        );
    }

    my $node = try {
        $c->model('DB::Zaaktype')->find($casetype->casetype_id)->zaaktype_node_id;
    } catch {
        $c->log->debug($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'The casetype was found, but could not be retrieved from backend, unable to continue.'
        ));
    };

    # Extra validation for wrapped attribute values POST'ed to us.
    for my $key (keys %values) {
        unless (ref $values{ $key } eq 'ARRAY') {
            throw('api/v1/case/create', sprintf(
                'Every attribute value must be wrapped in an array. Validation for attribute "%s" failed.',
                $key
            ), { http_code => 400 });
        }
    }

    my @subject_data;
    my $cleaner = sub {
        my $data = shift;
        my %ret;

        if ($data->{ type } eq 'subject') {
            $ret{ subject } = $data;
        } else {
            $ret{ subject_type } = $data->{ type };
            $ret{ subject_id } = $data->{ id };
        }

        return %ret;
    };

    # In preperation of deprecating the requestor parameter, we translate it
    # to the new subjects format.
    if ($params->{ requestor }) {
        push @subject_data, {
            $cleaner->($params->{ requestor }),
            magic_string_prefix => 'requestor',
            role => 'Aanvrager',
            type => 'requestor'
        }
    }

    # In preperation of deprecating the recipient parameter, we translate it
    # to the new subjects format.
    if ($params->{ recipient }) {
        push @subject_data, {
            $cleaner->($params->{ recipient }),
            magic_string_prefix => 'recipient',
            role => 'Ontvanger',
            type => 'recipient'
        }
    }

    if ($params->{ assignee }) {
        push @subject_data, {
            $cleaner->($params->{ assignee }),
            magic_string_prefix => 'assignee',
            role => 'Behandelaar',
            type => 'assignee'
        };
    }

    if (exists $params->{ subjects }) {
        # Allow 'subjects => { %subject_data }' and
        # 'subjects => [ { %subject_data }, { ... }, ... ]' call styles
        push @subject_data, ref $params->{ subjects } eq 'ARRAY'
            ? @{ $params->{ subjects } }
            : $params->{ subjects };
    }

    assert_profile($_, profile => {
        required => {
            magic_string_prefix => NonEmptyStr,
            role => NonEmptyStr
        },
        optional => {
            subject_type => enum([qw[person company employee]]),
            subject_id => union([qw[Str HashRef]]),
            subject => 'HashRef',
            type => enum([qw[requestor assignee recipient]]),
            pip_authorized => Boolean,
            send_auth_confirmation => Boolean
        },
        require_some => {
            subject => [ 1, qw[subject subject_id] ],
        },
        dependencies => {
            subject_id => [ 'subject_type' ]
        }
    }) for @subject_data;

    my (@subjects, $assignee_identifier, $requestor_identifier, $recipient_identifier);

    my %searchmap = (
        person => sub {
            return (
                { type => 'natuurlijk_persoon', intern => 0 },
                { burgerservicenummer => int(shift) }
            );
        },

        company => sub {
            my $id = assert_profile(shift, profile => {
                required => {
                    kvk_number => 'Int',
                },
                optional => {
                    branch_number => 'Str'
                }
            })->valid;

            return (
                { type => 'bedrijf', intern => 0 },
                {
                    dossiernummer => sprintf('%08d', $id->{ kvk_number }),
                    vestigingsnummer => $id->{ branch_number }
                }
            );
        },
    );

    for my $subject_args (@subject_data) {
        my ($subject, $betrokkene, $betrokkene_identifier);

        if ($subject_args->{ subject }) {
            my $ref = $c->model('Object')->inflate_from_hash(
                $subject_args->{ subject }
            );

            # Fix instantiator with subject bridge passthru
            $ref->instantiator(sub {
                $c->model('BR::Subject')->find(shift->id)
            });

            my $subject = try {
                return $ref->_instance;
            } catch {
                $c->log->error($_);

                throw('api/v1/case/subject_not_found', sprintf(
                    'Subject could not be found for provided ID "%s"',
                    $ref->id
                ));
            };

            unless (defined $subject) {
                throw('api/v1/case/subject_not_found', sprintf(
                    'Subject could not be found for provided ID "%s"',
                    $ref->id
                ));
            }

            $betrokkene_identifier = $subject->old_subject_identifier;
        } elsif ($subject_args->{ subject_type } eq 'employee') {
            $subject = $c->model('DB::Subject')->find(
                $subject_args->{ subject_id }
            );

            unless (defined $subject) {
                throw('api/v1/case/employee_not_found', sprintf(
                    'No employee could be found for provided ID "%s"',
                    $subject_args->{ subject_id }
                ));
            }

            $betrokkene_identifier = $subject->betrokkene_identifier;
        } else {
            my $subject_rs = $c->model('Betrokkene')->search(
                # Resolve the search parameters using the search mapper above
                $searchmap{ $subject_args->{ subject_type } }->(
                    $subject_args->{ subject_id }
                )
            );

            # subject_rs->first is fundamentally broken
            # but I don't dare fix it, who knows what relies on that behavior
            $betrokkene = $subject_rs->next;

            unless (defined $betrokkene) {
                throw('api/v1/case/subject_not_found', sprintf(
                    'No subject could be found for the provided ID "%s"',
                    $subject_args->{ subject_id }
                ));
            }


            $betrokkene_identifier = $betrokkene->betrokkene_identifier;
        }

        # If a type is set, the subject has some special meaning for the old
        # case creation infrastructure, hopefully we can chuck this when we
        # clean out the Zaak cruft.
        if (exists $subject_args->{ type }) {
            if ($subject_args->{ type } eq 'requestor') {
                $requestor_identifier = $betrokkene_identifier;
            }

            if ($subject_args->{ type } eq 'assignee') {
                $assignee_identifier = $betrokkene_identifier;
            }

            if ($subject_args->{ type } eq 'recipient') {
                $recipient_identifier = $betrokkene_identifier;
            }
        }

        # Final parameter mangling, the data below gets passed into
        # $zaak->betrokkene_relateren(...)
        my $relate_subject_args = {
            %{ $subject_args },
            betrokkene_identifier => $betrokkene_identifier
        };

        $relate_subject_args->{ rol } = delete $relate_subject_args->{ role };

        push @subjects, $relate_subject_args;
    }

    unless (defined $requestor_identifier) {
        my $definition = $node->zaaktype_definitie_id;

        unless ($definition->preset_client) {
            throw('api/v1/case/requestor_unresolvable', sprintf(
                'No (valid) requestor was provided, and no preset requestor available'
            ), { http_code => 400 })
        }

        $requestor_identifier = $definition->preset_client;
    }

    # Decide on the assignee/route
    my ($route_group, $route_role);

    # We may not need the route, but validate it anyway.
    if (exists $params->{ route }) {
        my $route_args = assert_profile($params->{ route }, profile => {
            required => {
                group_id => 'Int',
                role_id => 'Int'
            }
        })->valid;

        $route_group = $c->model('DB::Groups')->find($route_args->{ group_id })->object;
        $route_role = $c->model('DB::Roles')->find($route_args->{ role_id })->object;
    } elsif (!$assignee_identifier) {
        # Only set the preset owner if no assignee is set AND no route was specified
        $assignee_identifier = $node->properties->{ preset_owner_identifier };
    }

    my $kenmerken = $node->zaaktype_kenmerken->search_by_magic_strings(keys %values)->search(
        {
            required_permissions => [ undef, '{}' ], # 'specifieke behandelrechten' always off-limits
            'zaak_status_id.status' => 1
        },
        { prefetch => 'zaak_status_id' }
    );

    # File-based attributes and normal attributes need to be handled differently.
    my $normal_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
    });

    my $document_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    my $geolatlon_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'geolatlon'
    });

    # Unroll the intersection of supplied key-value-pairs and kenmerken in the
    # registration phase, and create an array of hashrefs of id => value pairs
    # Also, we don't yet support 'opplusbare velden' attributes, so deref the
    # first POST'ed value explicitly.
    my @normal_values = map { { $_->id => $values{ $_->magic_string }[0] } }
                        map { $_->bibliotheek_kenmerken_id }
                            $normal_kenmerken->all;

    my %document_values = map {
        $_->id => $values{ $_->bibliotheek_kenmerken_id->magic_string }
    } $document_kenmerken->all;

    my $contact_details;

    if (exists $params->{ contact_details }) {
        $contact_details = assert_profile($params->{ contact_details }, profile => {
            optional => {
                phone_number => TelephoneNumber,
                mobile_number => MobileNumber,
                email_address => EmailAddress
            }
        })->valid;
    }

    my $zaak = try {
        return $c->model('DB::Zaak')->create_zaak({
            zaaktype_id => $casetype->casetype_id,
            aanvraag_trigger => 'extern',
            confidentiality => $params->{confidentiality} // 'public',
            contactkanaal => $params->{ source },
            delayed_touch => 0, # doesn't seem to do anything, _touch() below
            registratiedatum => DateTime->now,
            kenmerken => \@normal_values,
            ($recipient_identifier ? (ontvanger => $recipient_identifier) : ()),
            aanvragers => [{
                betrokkene => $requestor_identifier,
                rol => 'Aanvrager',
                verificatie => 'n/a',
            }],
        });
    } catch {
        $c->log->error($_);

        throw(
            'api/v1/case/create_failed',
            'Case creation failed, unable to continue.',
            { original_error => $_ }
        );
    };

    if ($assignee_identifier) {
        try {
            $zaak->assert_assignee(
                betrokkene_id => $assignee_identifier
            );

            $zaak->set_behandelaar($assignee_identifier);

            if ($params->{ open }) {
                $zaak->open_zaak;
            }
        } catch {
            $c->log->error($_);
        };
    }

    if ($route_group || $route_role) {
        try {
            $zaak->case_actions->hijack_allocation_action({
                group => $route_group,
                role => $route_role
            });
        } catch {
            $c->log->error($_);
        };
    }

    if ($contact_details) {
        my $requestor = $zaak->aanvrager_object;

        if (exists $contact_details->{ email_address }) {
            $requestor->email($contact_details->{ email_address });
        }

        if (exists $contact_details->{ phone_number }) {
            $requestor->telefoonnummer($contact_details->{ phone_number });
        }

        if (exists $contact_details->{ mobile_number }) {
            $requestor->mobiel($contact_details->{ mobile_number });
        }
    }

    # Add in all subjects we have aggregated.
    for my $subject (@subjects) {
        # Skip requestors, ->create_zaak handles that through ancient magic
        next if $subject->{ type } eq 'requestor';

        try {
            $zaak->betrokkene_relateren($subject);
        } catch {
            $c->log->error($_);
        };
    }

    for my $zaaktype_kenmerk_id (keys %document_values) {
        my $refs = $document_values{ $zaaktype_kenmerk_id };

        for my $ref (ref $refs eq 'ARRAY' ? @$refs : $refs) {
            my $filestore_obj = $c->model('DB::Filestore')->find({
                uuid => $ref,
            });

            next unless defined $filestore_obj;

            $c->log->info('Create file found in kenmerk: ' . $filestore_obj->original_name);

            my $filecount = $filestore_obj->files->count;

            ### Check if file is coming from doc_intake
            if ($filecount > 2) {
                $c->log->error(sprintf(
                    "Found multiple references from 'file' table to filestore(%s)"
                ), $filestore_obj->uuid);

                throw(
                    'api/v1/case/create/multiple_file_references',
                    'Case creation failed, too many file references.',
                );
            } elsif ($filecount == 1) {
                my $file = $filestore_obj->files->first;

                $file->update_properties({
                    case_document_ids => [ $zaaktype_kenmerk_id ],
                    accepted => 1,
                    case_id => $zaak->id,
                    subject => $c->user->as_object
                });
            } else {
                $c->model('DB::File')->file_create({
                    case_document_ids => [ $zaaktype_kenmerk_id ],
                    name => $filestore_obj->original_name,
                    db_params => {
                        accepted => 1,
                        filestore_id => $filestore_obj->id,
                        created_by => $c->user->betrokkene_identifier,
                        case_id => $zaak->id
                    }
                });
            }
        }
    }

    # Add all files
    $self->_create_files_with_metadata({
            files           => $params->{files},
            schema          => $c->model('DB')->schema,
            user            => $c->user,
            case            => $zaak,
    }) if $params->{files};

    try {
        while (my $ztk = $geolatlon_kenmerken->next) {
            my $value = $values{ $ztk->bibliotheek_kenmerken_id->magic_string };

            next unless $value;
            next unless $ztk->properties->{ map_case_location };

            my ($lat, $lon) = split m[,], $value->[0];

            my $queue_items = $c->model('DB')->schema->default_resultset_attributes->{ queue_items };
            my $case = $zaak->object_data;

            push @{ $queue_items }, $case->queues->create_item(
                'update_case_location',
                {
                    object_id => $case->id,
                    label => 'Zaaklocatie instellen',
                    data => {
                        latitude => $lat,
                        longitude => $lon
                    }
                }
            );
        }
    } catch {
        # Case creation should not fail on case_location update failure
        $c->log->warn($_);
    };

    # Handle phase actions
    my $actions = $zaak->registratie_fase->case_actions->search({
        case_id => $zaak->id
    })->sorted;

    $actions->apply_rules({
        case => $zaak,
        milestone => 1
    });

    my @case_actions = $actions->all;

    # Get them by execution ordering (sorted just gives the ordering per type)
    my @ordered_actions = (
        (grep { $_->type eq 'subject' } @case_actions),
        (grep { $_->type eq 'allocation' } @case_actions),
        (grep { $_->type eq 'template' } @case_actions),
        (grep { $_->type eq 'case' } @case_actions),
        (grep { $_->type eq 'email' } @case_actions)
    );

    for my $action (@ordered_actions) {
        next unless $action->automatic;

        $c->log->debug(sprintf(
            'Running case registration phase action %d ("%s")',
            $action->id,
            $action->label
        ));

        $c->model('Queue')->run($zaak->fire_action(
            action => $action,
            current_user => $c->user
        ));
    }

    # Handwired rule execution... sigh
    my $rules_result = $zaak->execute_rules({ status => 1 });

    if ($rules_result) {
        if ($rules_result->{ send_external_system_message }) {
            try {
                $zaak->send_external_system_message(
                    base_url => $c->uri_for('/'),
                    rules_result => $rules_result->{ send_external_system_message }
                );
            } catch {
                $c->log->error($_);
            }
        }

        if ($rules_result->{ wijzig_registratiedatum }) {
            try {
                $zaak->process_date_of_registration_rule(%{
                    $rules_result->{ wijzig_registratiedatum }
                });
            } catch {
                $c->log->error($_);
            }
        }
    }

    $self->_reload_case_and_get($c, $zaak);
}

=head2 _create_files_with_metadata

    $self->_create_files_with_metadata({
        files           => [
            {
                reference   => '398375e4-ed8b-469c-8f27-1f8da190beb1',
                metadata    => {
                    origin              => 'Uitgaand',
                    description         => 'Besluit van de gemeente over de aanvraag',
                    document_category   => 'Besluit',
                    trust_level         => 'Zaakvertrouwelijk',
                    origin_date         => '2016-05-05T00:00:00'
                }
            }
        ]
        added_files     => {
            '398375e4-ed8b-469c-8f27-1f8da190beb1'  => '55'
        },
        schema          => $schema,
        user            => $c->user,
    })

Creates files with given filestore reference from C<files> parameter. When it is already added via
C<added_files>, it will only update metadata

=cut

define_profile '_create_files_with_metadata' => (
    required    => {
        files   => 'HashRef',
        user    => 'Zaaksysteem::Schema::Subject',
        case    => 'Zaaksysteem::Schema::Zaak',
        schema  => 'Zaaksysteem::Schema',
    },
    optional    => {
        added_files     => 'HashRef'
    },
);

# Tested, Zaaksysteem::Test::Controller::API::v1::Case;
sub _create_files_with_metadata {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;
    my $schema  = $params->{schema};
    my $user    = $params->{user};

    if ($params->{files}) {
        my @given_files = ref $params->{files} eq 'ARRAY' ? @{ $params->{files} } : ($params->{files});

        for my $given_file (@given_files) {
            my $ref = $given_file->{reference};

            unless ($ref) {
                $self->log->error("No 'reference' parameter given in 'file' paramter object");
                next;
            }

            my $filestore_obj = $schema->resultset('Filestore')->find({
                uuid => $ref
            });

            unless ($filestore_obj) {
                $self->log->error("Referenced 'filestore' uuid not found in database");
                next;
            }

            my $file          = $filestore_obj->files->first;

            if ($file && $file->get_column('case_id') && $file->get_column('case_id') ne $params->{case}->id) {
                $self->log->error("Reference to file given, but already added to another case with id: " . $file->get_column('case_id'));
                next;
            }

            if (!$file) {
                $self->log->info("Filestore found, but no referring file entry, creating it now...");

                $file = $schema->resultset('File')->file_create({
                    name => ($given_file->{name} || $filestore_obj->original_name),
                    db_params => {
                        accepted => 1,
                        filestore_id => $filestore_obj->id,
                        created_by => $user->betrokkene_identifier,
                        case_id => $params->{case}->id
                    }
                });
            } else {
                $self->log->info("File already added to case, update properties of " . $filestore_obj->original_name);

                ### Update case_id before updating properties
                $file->case_id($params->{case}->id) unless $file->get_column('case_id');
                $file->update_properties(
                    {
                        subject => $user,
                        accepted => 1,
                        ($given_file->{name} ? (name => $given_file->{name}) : ()),
                    }
                );
            }

            my $origin_date = $given_file->{metadata}->{origin_date};

            if ($origin_date) {
                $origin_date = DateTime::Format::DateParse
                    ->parse_datetime($origin_date, 'UTC');

                $origin_date = $origin_date->set_time_zone('Europe/Amsterdam') if $origin_date;
            }

            my $metadata = {
                map(
                    { $_ => $given_file->{metadata}->{ $_ } }
                    grep(
                        { exists $given_file->{metadata}->{ $_ } }          # Only existing keys in $file->{metadata}
                        qw/description trust_level origin document_category pronom_format appearance structure/
                    )
                ),
                ($origin_date ? (origin_date => $origin_date->ymd) : ()),
            };

            $self->log->info("Update metadata of file " . $filestore_obj->original_name);
            $file->update_metadata($metadata);
        }
    }

    return 1;
}


=head2 prepare_file

=head3 URL

C</api/v1/case/prepare_file>

=cut

sub prepare_file : Chained('base') : PathPart('prepare_file') : Args(0) : RW {
    my ($self, $c) = @_;

    my @uploads = map { ref $_ eq 'ARRAY' ? @$_ : $_ } values %{ $c->req->uploads };

    unless (scalar @uploads) {
        throw('api/v1/case/upload', sprintf(
            'Upload(s) missing.'
        ), { http_code => 400 });
    }

    my $filestore = $c->model('DB::Filestore');

    $c->stash->{ result } = Zaaksysteem::API::v1::PreparedFileBag->new;

    for my $upload (@uploads) {
        my $file = try {
            return $filestore->filestore_create({
                original_name => $upload->filename,
                file_path     => $upload->tempname,
            });
        } catch {
            $c->log->warn($_);

            throw('api/v1/case/upload_validation', sprintf(
                'File creation failed, unable to continue.'
            ));
        };

        $c->stash->{ result }->add($file);

        my $clean_job = Zaaksysteem::Object::Types::ScheduledJob->new(
            job => 'CleanTmp',
            interval_period => 'once',
            next_run => DateTime->now->add(minutes => 15),
            data => $file->uuid
        );

        try { $c->model('Object')->save_object(object => $clean_job) } catch {
            $c->log->warn("Non-fatal; failed to schedule temporary file cleaner job. Original error follows:", $_);
        };
    }
}

=head2 transition

=head3 URL

C</api/v1/case/[UUID]/transition>

=cut

define_profile transition => (
    optional => {
        result_id   => 'Num',
        result      => 'Str',
    }
);

sub transition : Chained('instance_base') : PathPart('transition') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $opts = assert_profile($c->req->params)->valid;

    my $zaak = try { $c->stash->{ case }->get_source_object } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    unless ($zaak->zaaktype_node_id->properties->{ api_can_transition }) {
        throw('api/v1/case/transition', sprintf(
            'Case transitioning is not enabled for this casetype.'
        ));
    }

    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/transition/closed',
            "Unable to transition case, case is closed ",
            { http_code => 409 }
            )
    }

    try {
        if(exists $opts->{ result_id } || exists $opts->{ result }) {
            if($zaak->afhandel_fase->status eq ($zaak->milestone + 1)) {
                if ($opts->{result_id}) {
                    $zaak->set_result_by_id({ id => $opts->{ result_id } });
                } else {
                    $zaak->set_resultaat($opts->{result});
                }
            }
        }

        $zaak->advance(
            object_model     => $c->model('Object'),
            betrokkene_model => $c->model('Betrokkene'),
            current_user     => $c->user,
        );
    } catch {
        ### I was hoping to use ZS::Exception::grab() for this one, but it does not allow
        ### chaining yet.
        
        my $errormsg = 'unknown error';
        if (blessed($_) && $_->isa('Zaaksysteem::Exception::Base')) {
            ### TODO: We really need information about the "why", owner not complete? Missing result?
            my $object  = $_->object;

            $errormsg   = 'No transition possible for this case, missing ' .
                join(
                    ', ',
                    map(
                        { $_ =~ s/_complete//; $_; }
                        grep (
                            { ! $object->{transition_states}->{$_} }
                            keys %{ $object->{transition_states} }
                        )
                    ));

            $c->log->error('Cannot transition case: ' . $zaak->id . ', because: ' . $errormsg);

        } else {
            $c->log->error('Cannot transition case: ' . $zaak->id . ', because: ' . $_);
        }

        throw('api/v1/case/transition', sprintf(
            $errormsg
        ))
    };

    $self->_reload_case_and_get($c, $zaak);
}

=head2 update

=head3 URL Path

C</api/v1/case/[UUID]/update>

=cut

define_profile update => (
    required => {
        values => 'HashRef'
    }
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $opts = assert_profile($c->req->params)->valid;

    my $zaak = try { $c->stash->{ case }->get_source_object } catch {
        $c->log->error($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    # Shouldn't happen, internal fault.
    unless (defined $zaak) {
        $c->log->error($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    }

    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/update/closed',
            "Unable to update case, case is closed ",
            { http_code => 409 }
            )
    }

    my @fields = keys %{ $opts->{ values } };

    my $kenmerken = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            # Only get those kenmerken we need.
            'bibliotheek_kenmerken_id.magic_string' => { -in => \@fields },

            # And only those in the current phase.
            'zaak_status_id.status' => $zaak->milestone + 1,

            # 'specifieke behandelrechten' always off-limits for API users
            required_permissions => [ undef, '{}' ]
        },
        {
            prefetch => [qw[bibliotheek_kenmerken_id zaak_status_id]]
        }
    );

    unless ($kenmerken->count) {
        # Note that this error may occur when a valid attribute name was
        # provided, but it isn't bound to the current phase.
        throw('api/v1/case/nop', sprintf(
            'Refusing to update because no supplied value resolved to an (authorized) attribute'
        ), { http_code => 400 });
    }

    my $normal_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
    });

    my $document_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    my $geolatlon_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'geolatlon'
    });

    my %new_values;
    my %document_values;

    # Iterate over all found attributes, ignoring the ones we could not
    # resolve, and build the %new_values var.
    while (my $type_kenmerk = $normal_kenmerken->next) {
        my $kenmerk = $type_kenmerk->bibliotheek_kenmerken_id;

        my $values = $opts->{ values }{ $kenmerk->magic_string };

        unless (ref $values eq 'ARRAY') {
            throw('api/v1/case/update', sprintf(
                'Every attribute value must be wrapped in an array. Validation for attribute "%s" failed.',
                $kenmerk->magic_string
            ), { http_code => 400 });
        }

        # We do not support 'opplusbare velden' yet, deref first item.
        $new_values{ $kenmerk->id } = $values->[0];
    }

    while (my $type_kenmerk = $document_kenmerken->next) {
        my $kenmerk = $type_kenmerk->bibliotheek_kenmerken_id;

        $document_values{ $type_kenmerk->id } = $opts->{ values }{ $kenmerk->magic_string };
    }

    try {
        $zaak->zaak_kenmerken->update_fields({
            new_values => \%new_values,
            zaak => $zaak
        });
        if (my $counter = keys %new_values) {
            $zaak->create_message_for_behandelaar(
                message => sprintf(
                    "%d kenmerk%s toegevoegd door een extern proces",
                    $counter, $counter > 1 ? 'en' : ''
                ),
                event_type => 'api/v1/update/attributes',
            );
        }
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/update_fault', sprintf(
            'Case update failed, unable to continue'
        ));
    };

    try {
        my $counter =0;
        for my $zaaktype_kenmerk_id (keys %document_values) {
            my $refs = $document_values{ $zaaktype_kenmerk_id };

            for my $ref (ref $refs eq 'ARRAY' ? @$refs : $refs) {
                my $filestore_obj = $c->model('DB::Filestore')->find({
                    uuid => $ref
                });

                next unless defined $filestore_obj;

                $c->model('DB::File')->file_create(
                    {
                        disable_message   => 1,
                        case_document_ids => [$zaaktype_kenmerk_id],
                        name      => $filestore_obj->original_name,
                        db_params => {
                            accepted     => 1,
                            filestore_id => $filestore_obj->id,
                            created_by =>
                                $c->user->betrokkene_identifier,
                            case_id => $zaak->id
                        }
                    }
                );

                $counter++;
            }
        }
        if ($counter) {
            $zaak->create_message_for_behandelaar(
                message => sprintf(
                    "%d document%s toegevoegd door een extern proces",
                    $counter, $counter > 1 ? 'en' : ''
                ),
                event_type => 'api/v1/update/documents',
            );
        }

    } catch {
        $c->log->error($_);

        throw('api/v1/case/update_document_fault', sprintf(
            'Case document update failed, unable to continue.'
        ));
    };

    try {
        while (my $ztk = $geolatlon_kenmerken->next) {
            my $value = $opts->{ values }{ $ztk->bibliotheek_kenmerken_id->magic_string };

            next unless $value;
            next unless $ztk->properties->{ map_case_location };

            # Case location cannot be defined through multiple values, just
            # pick the first one.
            my ($lat, $lon) = split m[,], $value->[0];

            my $queue_items = $c->model('DB')->schema->default_resultset_attributes->{ queue_items };

            push @{ $queue_items }, $c->stash->{ case }->queues->create_item(
                'update_case_location',
                {
                    object_id => $c->stash->{ case }->id,
                    label => 'Zaaklocatie instellen',
                    data => {
                        latitude => $lat,
                        longitude => $lon
                    }
                }
            );
        }
    } catch {
        # Not being able to set a case location is not a case update failure
        # condition, just warn the update has failed in log.
        $c->log->warn($_);
    };

    $self->_reload_case_and_get($c, $zaak);
}

=head2 take

=head3 URI

    C</api/v1/case/[UUID]/take>

=cut

sub take : Chained('instance_base') : PathPart('take') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_user;

    my $zaak = $c->stash->{ case }->get_source_object;
    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/take/closed',
            "Unable to 'take' case, case is closed ",
            { http_code => 409 }
            )
    }

    $zaak->open_zaak();
    $self->_reload_case_and_get($c, $zaak);
}

=head2 reject

=head3 URI

    C</api/v1/case/[UUID]/reject>

=cut

sub reject : Chained('instance_base') : PathPart('reject') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_user;

    my $zaak = $c->stash->{ case }->get_source_object;
    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/reject/closed',
            "Unable to 'reject' case, case is closed ",
            { http_code => 409 }
        );
    }

    if ($zaak->status ne 'new') {
        throw(
            'api/v1/case/reject/not_new',
            "Unable to 'reject' case, case is not in 'intake' state",
            { http_code => 409 }
        );
    }

    $zaak->reject_zaak();
    $self->_reload_case_and_get($c, $zaak);
}




sub _reload_case_and_get {
    my ($self, $c, $zaak) = @_;
    $zaak->discard_changes;
    $zaak->_touch;
    $c->stash->{case} = $zaak->object_data;
    $c->detach('get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

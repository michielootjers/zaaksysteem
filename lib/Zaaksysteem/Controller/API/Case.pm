package Zaaksysteem::Controller::API::Case;

use Moose;
use Data::Dumper;
use JSON;
use Try::Tiny;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }


sub base : Chained('/') : PathPart('api/case') : CaptureArgs(1) {
    my ($self, $c, $case_id) = @_;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($case_id);

    $c->stash->{zapi} = [];

    die "attempted access violation" unless $c->check_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');
    #throw ('api/case', 'rights violation'); # doesn't seem to do anything?
}

=head2 caseless_base

This base action exists such that APIs that are considered part of the Case
domain can chain off the API like L<Zaaksysteem::Controller::API::Case::File>
does, but without a case number.

=cut

sub caseless_base : Chained('/') : PathPart('api/case') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # Stub placeholder
}


sub confidentiality: Chained('base') : PathPart('confidentiality') : ZAPI {
    my ($self, $c) = @_;

    throw ('api/case/confidentiality', 'access violation')
    	unless $c->check_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');

    throw ('api/case/confidentiality/method_not_post', 'only POST requests allowed')
    	unless $c->req->method eq 'POST';

    my $new_value = $c->req->params->{confidentiality};

    $c->stash->{zaak}->set_confidentiality($new_value);
    $c->stash->{zaak}->update;

    $c->stash->{zapi} = [{
    	message => 'Confidentiality updated',
    	value   => $c->stash->{zaak}->confidentiality
    }];
}


=head2 request_attribute_update

request update to a field. the citizens are not allowed to change fields
without an official reviewing first, this sends a request to the official.

=cut

sub request_attribute_update : Chained('base') : PathPart('request_attribute_update') : ZAPI {
    my ($self, $c, $bibliotheek_kenmerken_id) = @_;

    my $params = $c->req->params;

    throw('api/case/request_attribute_update', "need toelichting")
        unless defined $params->{toelichting}; # optional, can't be undef

    throw('api/case/request_attribute_update', "need bibliotheek_kenmerken_id")
        unless $bibliotheek_kenmerken_id;

    my $kenmerk = $c->stash->{ zaak }->zaaktype_node_id->zaaktype_kenmerken->find({
        bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
    });

    unless($kenmerk) {
        throw('pip/update_field', "Couldn't find requested attribute.");
    }

    # get it, put on the stash where we need it
    $c->forward('stash_submitted_field_value', [ $kenmerk, $params ]);

    my $scheduled_job = $c->model('DB')->resultset('ScheduledJobs')->create_task({
        task                        => 'case/update_kenmerk',
        bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
        value                       => $c->stash->{ veldoptie_value },

        # as soon as the subjects table /user management system
        # include the citizens we could add them here
        created_by                  => $c->user_exists ? $c->user->betrokkene_identifier : undef,
        reason                      => $params->{ toelichting },
        case                        => $c->stash->{ zaak },
    });

    $self->log_update_field($c, {
        toelichting => $params->{toelichting},
        new_values  => $c->stash->{veldoptie_value},
        value_type  => $kenmerk->bibliotheek_kenmerken_id->value_type,
        kenmerk     => $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam
    });

    # this functionality will quickly become viable, so let's keep it in commented form
    #$c->forward('update_field_message', [$scheduled_job->apply_roles->description]);
    $c->stash->{ template } = 'widgets/general/veldoptie_view.tt';
    $c->stash->{ nowrapper } = 1;

    $c->stash->{ veldoptie_multiple } = $kenmerk->bibliotheek_kenmerken_id->type_multiple;
    $c->stash->{ veldoptie_type } = $kenmerk->type;
    # veldoptie_value is put on the stash by sub-action

    my $html = $c->view('TT')->render($c, 'widgets/general/veldoptie_view.tt');
    $c->stash->{zapi} = [{ attribute_value_as_html => $html }];
}


sub stash_submitted_field_value : Private {
    my ($self, $c, $kenmerk, $params) = @_;

    my $key = 'kenmerk_id_' . $kenmerk->get_column('bibliotheek_kenmerken_id');
    my $value = $params->{ $key };

    # if no checkboxes are checked, we will receive nothing, which would be OK
    if ($params->{ $key . '_checkbox' }) {
        $value ||= [];
    }

    unless (defined $value) {
        throw('pip/field_value', 'Unable to get value for attribute "%s"', $kenmerk->bibliotheek_kenmerken_id->naam);
    }

    $c->stash->{ veldoptie_value } = $value;
}

define_profile log_update_field => (
    required => [qw[kenmerk value_type]],
    optional => [qw[new_values toelichting]],
);

sub log_update_field {
    my ($self, $c, $opts) = @_;

    my $valid_opts = assert_profile($opts)->valid;

    my $new_values = $valid_opts->{value_type} =~ m|^bag| ?
        $c->model('Gegevens::Bag')->humanize($valid_opts->{new_values}) :
        $valid_opts->{new_values};

    $c->model('DB::Logging')->trigger(
        'case/pip/updatefield', {
            component => 'zaak',
            zaak_id   => $c->stash->{zaak}->id,
            data => {
                kenmerk     => $valid_opts->{kenmerk},
                new_values  => $new_values,
                toelichting => $valid_opts->{toelichting}
            }
        }
    );
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


package Zaaksysteem::Controller::Public;

use Moose;
use Zaaksysteem::Exception;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Public - Public API

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Public>

=head1 DESCRIPTION

Zaaksysteem API Controller for public (non-Zaaksysteem) use.

=head1 METHODS

=head2 /public/search_case/?case_type=Zaaktype%20Naam

Returns a resultset of the given casetype

=cut

sub search_case
    : Chained('/')
    : PathPart('public/search_case')
    : Args(0)
    : JSON
{
    my ($self, $c) = @_;

    # Check if the given search is valid
    if (!$c->req->param('search_query_id')) {
        throw(
            '/public/search_case/no_search_query_id',
            "Geen search_query_id gedefinieerd"
        );
    }
    my ($search) = $c->model('DB::SearchQuery')->search({
        id => $c->req->param('search_query_id'),
    });
    if (!$search) {
        throw(
            'public/search_case/invalid_search_query_id',
            "Geen search gevonden met gegeven search_query_id"
        );
    }

    my $output = _collect_data_for_sq($c);
    
    no strict 'refs';
    *DateTime::TO_JSON = sub {shift->iso8601};
    use strict;

    $c->stash->{json} = $output;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 _collect_data_for_sq

Helper to collect the details for search_case that we wish to output to the public.

=cut

sub _collect_data_for_sq {
    my ($c) = @_;

    $c->stash->{SearchQuery_search_query_id} = $c->req->param('search_query_id');
    my $sq = Zaaksysteem::Controller::Search->_search_query($c);
    my @cases = $sq->results({c => $c})->all;

    my @result;
    for my $case (@cases) {
        # Check if case type node has 'is_public' property
        if (!$case->zaaktype_node_id->is_public) {
            next;
        }

        push @result, {
            id => $case->id,
            zaaktype => $case->zaaktype_node_id->titel,
            registratiedatum => $case->registratiedatum,
            afhandeldatum    => $case->afhandeldatum,
            status           => $case->status,
            fase             => 'Naam van Fase komt hier',
            onderwerp        => $case->onderwerp,
            resultaat        => $case->resultaat,
        };
    }

    return @result;
}

1;

package Zaaksysteem::Controller::Plugins::Digid;

use strict;
use warnings;
use Data::Dumper;

use Moose;
use Moose::Util qw/apply_all_roles does_role/;
use JSON;
use MIME::Base64;

BEGIN { extends 'Catalyst::Controller'; }

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA
/;

sub login : Chained('/') : PathPart('auth/digid'): Args() {
    my ($self, $c, $do_auth) = @_;

    if ($c->user_exists) {
        $c->delete_session;
    }

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->stash->{ success_endpoint } = $c->req->param('success_endpoint');

    $c->stash->{ idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_citizen') ];

    if($c->session->{ _saml_error }) {
        my %dispatch = (
            'urn:oasis:names:tc:SAML:2.0:status:AuthnFailed' => 'cancelled',
            'urn:oasis:names:tc:SAML:2.0:status:NoAuthnContext' => 'context_insufficient',
            'urn:oasis:names:tc:SAML:2.0:status:PartialLogout' => 'partial_logout',
            'urn:oasis:names:tc:SAML:2.0:status:RequestDenied' => 'denied'
        );

        $c->stash->{ saml_error } = $dispatch{ $c->session->{ _saml_error } } || 'unknown';

        $c->log->debug(sprintf('SAML Protocol Exchange came back with error: ', $c->session->{ _saml_error }));
    }

    $c->stash->{template} = 'plugins/digid/login.tt';
}

sub logout : Chained('/') : PathPart('auth/digid/logout'): Args() {
    my ($self, $c) = @_;

    delete $c->session->{ _saml };
    $c->stash->{template}   = 'plugins/digid/login.tt';
    $c->stash->{logged_out} = 1;
    $c->detach;
}


sub _zaak_create_secure_digid : Private {
    my ($self, $c) = @_;

    my $saml_state = $c->session->{ _saml } || {};

    if (
        $c->req->params->{authenticatie_methode} eq 'digid' ||
        $c->session->{_zaak_create}->{extern}->{verified} eq 'digid'
    ) {
        if($saml_state->{ success }) {
            $c->session->{_zaak_create}->{extern} = {};

            ### Check if we are allowed to crate this zaaktype
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
                = 'natuurlijk_persoon';
            $c->session->{_zaak_create}->{extern}->{verified}
                = 'digid';
            $c->session->{_zaak_create}->{extern}->{id}
                = $saml_state->{ uid };

            $c->stash->{aanvrager_type} = 'natuurlijk_persoon'
        } else {
            my $arguments = {};
            $arguments->{'authenticatie_methode'} = 'digid' if ($c->req->params->{authenticatie_methode});
            $arguments->{'ztc_aanvrager_type'} = 'natuurlijk_persoon' if ($c->req->params->{ztc_aanvrager_type});
            $arguments->{'sessreset'} = 1 if ($c->req->params->{sessreset});
            $arguments->{'zaaktype_id'} = $c->req->params->{zaaktype_id} if (
                $c->req->params->{zaaktype_id} &&
                $c->req->params->{zaaktype_id} =~ /^\d+$/
            );

            $c->res->redirect(
                $c->uri_for(
                    '/auth/digid',
                    {
                        success_endpoint => $c->uri_for(
                            '/zaak/create/webformulier/',
                            $arguments,
                        )
                    }
                )
            );

            ### Wipe out externe authenticatie
            if (
                $c->session->{_zaak_create}->{extern} &&
                $c->session->{_zaak_create}->{verified} eq 'digid'
            ) {
                delete($c->session->{_zaak_create}->{extern});
            }

            $c->detach;
        }
    } else {

        ### Geen digiid, stop here
        return;
    }

    ### Save aanvrager data
    $c->forward('_zaak_create_aanvrager');
}

sub _zaak_create_aanvrager : Private {
    my ($self, $c) = @_;

    return unless (
        $c->req->params->{aanvrager_update}
    );

    $c->log->debug('_zaak_create_aanvrager: Aanvrager update');

    my $callerclass     = 'Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon';


    ### Only validate contact, which are all optional
    my $profile;
    if ($c->req->params->{contact_edit}) {
        $profile = $c->user_exists ? VALIDATION_CONTACT_DATA : VALIDATION_EXTERNAL_CONTACT_DATA;
    } else {
        ### Get profile from Model
        $profile         = $c->get_profile(
            'method'    => 'create',
            'caller'    => 'Zaaksysteem::Controller::Betrokkene'
        ) or die('Terrible die here');

        ### MERGE
        my $contact_profile = VALIDATION_CONTACT_DATA;
        while (my ($key, $data) = each %{ $contact_profile }) {
            unless ($profile->{$key}) {
                $profile->{$key} = $data;
                next;
            }

            if (UNIVERSAL::isa($data, 'ARRAY')) {
                push(@{ $profile->{$key} }, @{ $data });
                next;
            }

            if (UNIVERSAL::isa($data, 'HASH')) {
                while (my ($datakey, $dataval) = each %{ $data }) {
                    $profile->{$key}->{$datakey} = $dataval;
                }
                next;
            }
        }
    }

    Zaaksysteem->register_profile(
        method => '_zaak_create_aanvrager',
        profile => $profile,
    );

    if ($c->req->is_xhr) {
        $c->zvalidate;
        $c->detach;
    }

    my $dv      = $c->zvalidate;
    return unless ref($dv);

    return unless $dv->success;

    ### Post
    $c->log->debug('_zaak_create_aanvrager: Updated aanvrager');
    if ($c->req->params->{aanvrager_edit}) {
        $c->session->{_zaak_create}->{aanvrager_update} = $dv->valid;
    } elsif ($c->req->params->{contact_edit}) {
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            if (defined($c->req->params->{ $_ })) {
                $c->session->{_zaak_create}->{ $_ } =
                    $c->req->params->{ $_ };
            }
        }

    }
}



sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;

    return unless $c->session->{_zaak_create}->{extern}->{verified} eq 'digid' &&
        $c->session->{_zaak_create}->{aanvrager_update};

    if($c->req->params->{aanvrager_update}) {

        my $id = $c->model('Betrokkene')->create(
            'natuurlijk_persoon',
            {
                %{ $c->session->{_zaak_create}->{aanvrager_update} },
                'np-authenticated'   => 0,
                'np-authenticatedby' => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID,
            }
        );
    
        $c->session->{_zaak_create}->{ztc_aanvrager_id}
            = 'betrokkene-natuurlijk_persoon-' .  $id;
    }
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


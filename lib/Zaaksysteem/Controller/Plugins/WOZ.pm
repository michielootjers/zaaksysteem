package Zaaksysteem::Controller::Plugins::WOZ;

use strict;
use warnings;
use parent 'Catalyst::Controller';

use Data::Dumper;
use LWP::UserAgent;
use LWP::Simple;

use XML::LibXML;
use Crypt::OpenSSL::RSA;
use MIME::Base64;
use POSIX qw/strftime/;
use URI::Escape;
use XML::Simple;
use XML::Parser;
use JSON;


sub object : Local : Args() {
    my ($self, $c, $id) = @_;

    my $params     = $c->req->params;
    my $owner      = $params->{owner};
    my $object_id  = $params->{object_id};

    my $woz_object = $c->model('DB::WozObjects')->find({
        id => $id, # id would be enough, owner and object_id are for security
        owner => $owner,
        object_id => $object_id,
    })->TO_JSON;

    $c->res->header("application/json");

    if($woz_object) {
        $c->res->body(to_json($woz_object));
    } else {
        $c->res->body("error");
    }
}


sub settings : Local {
    my ($self, $c) = @_;

    my $settings = $c->forward('retrieve_settings');

    $c->res->header("application/json");
    $c->res->body(to_json($settings));    
}


sub retrieve_settings : Private {
    my ($self, $c) = @_;

    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    return { map { $_->key => $_->value } @$settings };
}


sub picture : Local {
    my ($self, $c) = @_;
    
    my $params = $c->req->params();
    
    my $config = $c->customer_instance->{start_config}->{woz_info};

    my $woz_object_url = 
        'https://atlas.cyclomedia.com/PanoramaRendering/RenderbyAddress/NL/' . 
        $params->{postcode} . 
        '%20' .
        $params->{huisnummer} . 
        '/?format=xml&apikey=' .
        $config->{key};

    my $ua = LWP::UserAgent->new;
    my $req = HTTP::Request->new(GET => $woz_object_url);

    $req->authorization_basic(
        $config->{username}, 
        $config->{password}
    );
    my $response = $ua->request($req);

    unless($response->code() eq '200') {
        die "couldn't get response from cyclomedia: " . $response->content;
    }

    my $parser = new XML::Parser( Style => 'Tree' );
    my $tree = $parser->parse(
        $response->content()
    );

    my $attributes = $tree->[1]->[0]
        or die "unexpected format in xml";

    my $recording_id = $attributes->{'recording-id'}
        or die "recording-id not present in xml";
    
    my $yaw = $attributes->{'yaw'}
        or die "yaw not present in xml";

    my $tid = $self->generate_tid({
        image_id    => $recording_id,
        config      => $config,
    });

    $c->stash->{json} = {
        tid             => $tid,
        woz_object_url  => $woz_object_url,
        recording_id    => $recording_id,
        yaw             => $yaw,
        postcode        => $params->{postcode},
        huisnummer      => $params->{huisnummer},
    };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


sub picture_dialog : Local {
    my ($self, $c) = @_;

    my $params                  = $c->req->params();

    $c->stash->{yaw}            = $params->{yaw};
    $c->stash->{tid}            = uri_escape($params->{tid});
    $c->stash->{recording_id}   = $params->{recording_id};
    $c->stash->{postcode}       = $params->{postcode};
    $c->stash->{huisnummer}     = $params->{huisnummer};
    
    $c->stash->{nowrapper}      = 1;
    $c->stash->{template}       = "plugins/woz/globespotter.tt";
}

        
sub generate_tid {
    my ($self, $arguments) = @_;
    
    my $image_id = $arguments->{image_id} or die "need image_id";
    my $config   = $arguments->{config}   or die "need config";

    my $account_id = $config->{account_id} or die "need account_id";
    my $issued_datetime = strftime "%Y-%m-%d %H:%M:%SZ", gmtime;

    my $tid = 'X' . $account_id . '&' . $image_id . '&' . $issued_datetime;

    my $private_key = $self->read_private_key($config);

    my $rsa_priv = Crypt::OpenSSL::RSA->new_private_key($private_key);

    my $signature = encode_base64($rsa_priv->sign($tid), '');

    $tid .= '&' . $signature;

    return $tid;
}

sub read_private_key {
    my ($self, $config) = @_;

    if (exists $config->{private_key_string}) {
        return $config->{private_key_string};
    }

    unless (exists $config->{private_key} && -e $config->{private_key}) {
        die "need private key path or string";
    }

    open FILE, $config->{private_key}
        or die "unable to read private key: " . $config->{private_key} . ": " . $!;

    my $private_key = join "", <FILE>;
    close FILE;

    return $private_key;
}


sub photo : Local {
    my ($self, $c) = @_;

    # the idea is to retrieve the woz object image from a remote server.
    # this construct will help with https: issues.
    # for now this is a placeholder.
    # my $path = $c->config->{home} . '/root/tpl/zaak_v1/nl_NL/images/woz_test_foto_huis.jpg';

    # open FILE, $path or die "could not open file path $path: $!";
    # my $content = join '', <FILE>;
    # close FILE;


    # when retrieving from remote, append woz_objectnummer to this config'd
    # url, retrieve it, and serve the result to the user.
    # if error, serve a "no image available" photo
    my $settings = $c->forward('retrieve_settings');
    my $woz_photo_base_url = $settings->{woz_photo_base_url};

    if ($woz_photo_base_url) {
        my $woz_object_id = $c->req->params->{object_id};

        $woz_photo_base_url =~ s|\[\[woz_object_id\]\]|$woz_object_id|is;

        # LWP::Simple, retrieve external photo
        my $content = get($woz_photo_base_url);

        if ($content) {
            $c->res->content_type('image/jpeg');
            $c->res->body($content);
            return;
        }
    }

    # fall through on purpose
    $c->response->status(404);
    $c->res->body('');
}


# tricky to add a pure image loading feature to ezra_dialog, so here's your dirty workaround.
# yes, i just did.
sub photo_dialog : Local {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    my $url = '/plugins/woz/photo?' . join "&", map { $_ . '=' . $params->{$_} } keys %$params;

    $c->res->content_type('text/html');
    $c->res->body('<img src="' . $url. '"/>');
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


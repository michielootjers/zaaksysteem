package Zaaksysteem::Controller::Betrokkene::Contactmoment;

use Moose;

use Params::Profile;
use Data::Dumper;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

BEGIN { extends 'Catalyst::Controller'; }

sub base : Chained('/betrokkene/betrokkene') : PathPart('contactmoment') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

=head2 create

Create a new contact moment related to a subject

=head3 URL

C</betrokkene/betrokkene-B<[type]>-B<[gmid]>/contactmoment/create>

=head3 Request method

The request method must be POST for this call.

=head3 Parameters

=over 4

=item contactkanaal (I<required>)

One of C<behandelaar>, C<balie>, C<telefoon>, C<post>, C<email> or C<webformulier>

=item content (I<required>)

Plaintext content to store

=item case_id (I<optional>)

Id of the case mentioned during contact

=back

=head3 Response

A single JSON hydrated event triggered by contact moment creation

=head3 Event

=over 4

=item C<subject/contactmoment/create>

This event reflects the fact that a contact moment has been created for this subject

=back

=cut

define_profile create => (
    required => [ qw[contactkanaal content] ],
    optional => [ qw[case_id] ]
);

sub create : Chained('base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    unless($c->req->method eq 'POST') {
        throw('request/invalid', "Invalid request method, should be POST.");
    }

    $c->model('DB::Contactmoment')->contactmoment_create({
        type => 'note',
        subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
        case_id => $c->req->param('case_id') || undef,
        created_by => $c->model('DB::Zaak')->current_user->betrokkene_identifier,
        medium => $c->req->param('contactkanaal'),
        message => $c->req->param('content')
    });

    $c->stash->{ json } = $c->model('DB::Logging')->trigger('subject/contactmoment/create', {
        component => 'betrokkene',
        zaak_id => $c->req->param('case_id') || undef,
        created_for => $c->stash->{ betrokkene }->betrokkene_identifier,
        data => {
            case_id => $c->req->param('case_id') || undef,
            subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
            contact_channel => $c->req->param('contactkanaal'),
            content => $c->req->param('content')
        }
    });

    $c->detach('Zaaksysteem::View::JSON');
}

1;

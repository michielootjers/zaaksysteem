package Zaaksysteem::Controller::Schedule;

use Data::Dumper;
use Moose;
use Try::Tiny;
use Zaaksysteem::Exception;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Schedule

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem controller for handling scheduled processes.

=head1 INSTRUCTIONS

=head1 METHODS

=head2 /schedule/run

Runs all jobs that are available on this domain/instance.

=cut

sub run
    : Chained('/')
    : PathPart('schedule/run')
    : ZAPI
{
    my ($self, $c) = @_;

    $c->model('DB::Transaction')->process_pending();

	$c->model('Zaken')->resume_temporarily_stalled_cases;
	$c->forward('check_buitenbeter_meldingen');

    $c->stash->{zapi} = [{
        result => 'success',
    }];

}


sub check_buitenbeter_meldingen : Private {
	my ($self, $c) = @_;

    my $interface = $c->model('DB::Interface')->search_active({
        module => 'buitenbeter'
    })->first or throw("api/buitenbeter", "Buitenbeter koppeling is niet beschikbaar");

    my $cases = $interface->process_trigger('GetNewMeldingen');
}


=head1 AUTHOR

vagrant,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;



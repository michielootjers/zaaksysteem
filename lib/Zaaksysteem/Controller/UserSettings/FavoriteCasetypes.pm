package Zaaksysteem::Controller::UserSettings::FavoriteCasetypes;

use Moose;
use namespace::autoclean;

use List::Util qw(max);
use List::MoreUtils qw(insert_after);
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

BEGIN {extends 'Zaaksysteem::General::ZAPIController';}

=head1 NAME

Zaaksysteem::Controller::UserSettings::FavoriteCasetypes - ZAPI Controller

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem API Controller to manage a user's favorite case types.

=head1 METHODS

=head2 add

Add a new favorite case type.

=cut

sub base : Chained('/api/base') : PathPart('favorite/casetype') : CaptureArgs(0) {}

define_profile add => (
    required => ['object_id'],
    typed => {
        'object_id' => 'Int',
    },
);

sub add : Chained('base') : PathPart('add') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $zaaktype = $c->model('DB::Zaaktype')->find($opts->{object_id});
    if (!$zaaktype) {
        throw(
            'request/invalid_parameter',
            "Zaaktype $opts->{object_id} was not found.",
        );
    }

    my $favorites = _get_favorites($c);
    my $id_max = max(map {$_->{id}} @$favorites) || 0;

    push @$favorites, {
        id          => $id_max + 1,
        casetype_id => $opts->{object_id},
    };

    _set_favorites($c, $favorites);
    $c->stash->{zapi} = [
        $favorites->[-1]
    ];
}

=head2 remove

Remove the specified case type from the user's favorite case type list.

=cut

define_profile remove => (
    required => ['favorite_id'],
    typed => {
        'favorite_id' => 'Int',
    },
);

sub remove : Chained('base') : PathPart('remove') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $favorites = _get_favorites($c);

    @$favorites = grep { $_->{id} != $opts->{favorite_id} } @$favorites;

    _set_favorites($c, $favorites);
    $c->stash->{zapi} = [];
}

=head2 move

Move a favorite case type to a new location in the list.

=cut

define_profile move => (
    required => ['favorite_id'],
    optional => ['after'],
    typed => {
        'favorite_id' => 'Int',
        'after' => 'Int',
    },
);

sub move : Chained('base') : PathPart('move') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $favorites = _get_favorites($c);

    my ($to_move) = grep { $_->{id} == $opts->{favorite_id} } @$favorites;
    @$favorites = grep { $_->{id} != $opts->{favorite_id} } @$favorites;

    if ($opts->{after}) {
        insert_after { $opts->{after} == $_->{id} } $to_move => @$favorites;
    }
    else {
        $favorites = [
            $to_move,
            @$favorites
        ];
    }

    _set_favorites($c, $favorites);

    $c->stash->{zapi} = [];
}

=head2 list

Return an (ordered) list of favorite casetype ids.

=cut

sub list : Chained('base') : PathPart('list') : Args(0) : GET : ZAPI {
    my ($self, $c) = @_;

    my $favorites = _get_favorites($c);

    $c->stash->{zapi} = [
        grep {
            defined($_->{object})
        }
        map {
            {
                type   => 'casetype',
                id     => $_->{id},
                object => $c->model('DB::Zaaktype')->find($_->{casetype_id}),
            };
        } @$favorites
    ];
}

=head2 _get_favorites

Helper function to retrieve case type favorites from userdata.

=cut

sub _get_favorites {
    my $c = shift;
    my $settings = $c->user->settings;

    return $settings->{favorite_case_types};
}

=head2 _set_favorites

Helper function to store case type favorites in userdata.

=cut

sub _set_favorites {
    my ($c, $favorites) = @_;

    my $settings = $c->user->settings;
    $settings->{favorite_case_types} = $favorites;

    $c->user->settings($settings);
    $c->user->update();

    return;
}

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

__PACKAGE__->meta->make_immutable;

1;

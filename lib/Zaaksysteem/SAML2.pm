package Zaaksysteem::SAML2;

use Moose;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use Zaaksysteem::SAML2::SP;
use Zaaksysteem::SAML2::IdP;
use Zaaksysteem::SAML2::Binding::SOAP;
use Zaaksysteem::SAML2::Binding::Redirect;
use Zaaksysteem::SAML2::Protocol::ArtifactResolve;
use Zaaksysteem::SAML2::Protocol::AuthnRequest;
use Zaaksysteem::SAML2::Protocol::Assertion;

use URI;
use URI::QueryParam;
use JSON;
use MIME::Base64;

has sp    => ( is => 'ro', isa => __PACKAGE__ . '::SP' );
has idp   => ( is => 'ro', isa => __PACKAGE__ . '::IdP' );
has uri   => ( is => 'ro');

has authenticated_identifier => (
    is      => 'rw',
    isa     => 'HashRef',
);

has authenticated_assertion => (
    is      => 'rw',
    isa     => 'Net::SAML2::Protocol::Assertion',
);

=head1 CONSTANTS

=over 4

=item SAML_TYPE_LOGIUS

This the main (only?) provider for DigiD services.

=item SAML_TYPE_KPN_LO

This is our currently supported supplier for eHerkenning services.

=back

=cut

use constant SAML_TYPE_LOGIUS => 'digid';
use constant SAML_TYPE_KPN_LO => 'eherkenning';
use constant SAML_TYPE_SPOOF => 'spoof';

=head1 CONSTRUCTORS

=head2 new_from_interfaces

Builds a new SAML2 instance with information derived from one or two
L<Zaaksysteem::Backend::Sysin::Interface> definitions. Supplying the
C<sp> parameter is optional, as it can be found using the IdP provided
in C<idp>.

=cut

define_profile new_from_interfaces => (
    required => [qw[idp]],
    optional => [qw[sp]],
    typed => {
        sp => 'Zaaksysteem::Model::DB::Interface',
        idp => 'Zaaksysteem::Model::DB::Interface'
    }
);

sub new_from_interfaces {
    my ($class, %params) = @_;
    my $opts  = assert_profile(\%params)->valid;

    unless($opts->{ sp }) {
        my $schema = $opts->{ idp }->result_source->schema;
        my $interfaces = $schema->resultset('Interface');

        ($opts->{ sp }) = $interfaces->search_module('samlsp');
    }

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(interface => $opts->{ sp });
    my $idp = Zaaksysteem::SAML2::IdP->new_from_interface(interface => $opts->{ idp });

    my $idp_entity_id = $opts->{ idp }->jpath('$.idp_entity_id');
    $sp->id($idp_entity_id) if $idp_entity_id;

    return $class->new(sp => $sp, idp => $idp);
}

=head1 Methods

=head2 handle_response($artifact_resolved_xml)

Return value: L<Net::SAML2::Protocol::Assertion> or error on failure

=cut

sub handle_response {
    my $self                = shift;
    my $responsexml         = shift;

    if ($responsexml && $responsexml !~ /xml/) {
        ### Probably base64 encoded? Ranzy check
        $responsexml        = decode_base64($responsexml);
    }

    my $xmlp                = XML::XPath->new( xml => $responsexml );

    $xmlp->set_namespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

    my $nodeset             = $xmlp->find('//samlp:Response/samlp:Status/samlp:StatusCode');

    throw(
        'saml2/handle_response/no_status',
        'No Status found in SOAP Response' 
    ) unless ($nodeset);

    my ($status)            = $nodeset->get_nodelist;

    throw(
        'saml2/handle_response/no_status',
        'No Status found in SOAP Response' 
    ) unless ($status);

    unless($status->getAttribute('Value') eq 'urn:oasis:names:tc:SAML:2.0:status:Success') {
        my ($substatus) = grep { $_->isa('XML::XPath::Node::Element'); } $status->getChildNodes;

        throw(
            'saml2/handle_response/invalid_status',
            'Invalid status received from SOAP Response: ' . $status->getAttribute('Value'),
            { status => $substatus->getAttribute('Value') }
        );
    }

    ### Validate XML response
    #my ($subject, $saml)    = $response->handle_response($saml);

    my $assertion   = Zaaksysteem::SAML2::Protocol::Assertion->new_from_xml(
        xml => $responsexml,
    );

    my $authenticated_identifier = $self->_load_authenticated_identifier($assertion);

    if ($authenticated_identifier) {
        $self->authenticated_assertion($assertion);

        $self->authenticated_identifier(
            $authenticated_identifier
        );
    }

    throw(
        'saml2/handle_response/invalid_assertion',
        'Invalid assertion'
    ) unless $self->authenticated_assertion;

    ### Retrieve subject from correct saml module
    return $self->authenticated_assertion;
}

sub _load_authenticated_identifier {
    my $self                        = shift;
    my $assertion                   = shift;

    return unless $assertion;

    my $saml_type = $self->idp->interface->jpath('$.saml_type');

    my $identifier;

    if ($saml_type eq SAML_TYPE_LOGIUS) {
        my %namespec                    = split m[:], $assertion->nameid;

        $identifier                     = {
            used_profile    => SAML_TYPE_LOGIUS,
            uid             => $namespec{ s00000000 }, # Sectorcode for BSN
            nameid          => $assertion->nameid,
            success         => 1,
        };

    } elsif ($saml_type eq SAML_TYPE_KPN_LO) {
        my $entity_id = $assertion->{ attributes }{ 'urn:nl:eherkenning:1.0:EntityConcernedID' }[0];

        my $prefix = substr($entity_id, 0, 8);

        unless ($prefix == 3) {
            throw('saml/entity_id', sprintf('Unable to determine KvK, unsupported prefix: %s', $prefix));
        }

        my $kvk = substr($entity_id, 8, 8);

        if (
            $self->idp->interface->result_source->schema->resultset('BedrijfAuthenticatie')->search({
                login => $kvk
            })->count
        ) {
            $identifier = {
                used_profile    => SAML_TYPE_KPN_LO,
                uid             => $kvk, # Sectorcode for BSN
                nameid          => $assertion->nameid,
                success         => 1,
            };
        } else {
            throw('saml2/handle_response/disabled_or_unknown', sprintf(
                'Cannot find KVK Number in zaaksysteem (bedrijf_authenticatie): %s',
                $kvk
            ));
        }
    }

    return $identifier;
}

=head2 authentication_redirect

Generates a authnrequest and returns the redirect url for use

=cut

define_profile authentication_redirect => (
    required    => [],
    optional    => [qw[relaystate]],
);

sub authentication_redirect {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    my $auth_request;
    my %sids;

    # Enable spoofmode
    if($self->idp->interface->jpath('$.saml_type') eq SAML_TYPE_SPOOF) {
        return '/auth/saml/prepare-spoof';
    }

    # Depending on the IdP supplier, we need to adjust how we signal
    # which service urls should be used and not.
    if($self->idp->interface->jpath('$.saml_type') eq SAML_TYPE_KPN_LO) {
        %sids = (
            AssertionConsumerServiceIndex => 1,
            AttributeConsumingServiceIndex => 0
        );
    } elsif($self->idp->interface->jpath('$.saml_type') eq SAML_TYPE_LOGIUS) {
        %sids = (
            AssertionConsumerServiceURL => $self->sp->url . '/consumer-post'
        );
    } else {
        throw('saml2/authn_request', 'Unable to decide on service identifiers, non-recognized saml type');
    }

    # Wrap potential die() with a proper exception so we can stacktrace this
    try {
        $auth_request = Zaaksysteem::SAML2::Protocol::AuthnRequest->new(
            issuer => $self->resolve_entity_id,
            base_url => $self->sp->url . '/saml',
            nameid_format => $self->idp->format('entity'),
            interface => $self->idp->interface,
            destination => $self->idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
            service_identifiers => \%sids
        );
    } catch {
        throw('saml2/authn_request', 'Unable to construct AuthnRequest, intercepted error: ' . $_);
    };

    ## Redirect
    my $redirect = Zaaksysteem::SAML2::Binding::Redirect->new(
        key => $self->sp->cert,
        url => $self->idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        signature_algorithm => $self->signature_algorithm
    );

    my $xml = $auth_request->as_xml;

    return $redirect->sign(
        request => "$xml",
        relaystate => $opts->{ relaystate }
    );
}

=head2 signature_algorithm

Getter for the XMLDSig signature algorithm to be used. Defaults to
L<http://www.w3.org/2000/09/xmldsig#rsa-sha1>.

If the C<saml_type> of the configured IdP is L<SAML_TYPE_KPN_LO>, the returned
URI will b L<http://www.w3.org/2001/04/xmldsig-more#rsa-sha256>.

Nothing is generically supported here tho, this is here just to keep the cruft
at bay.

=cut

sub signature_algorithm {
    my $self = shift;

    my $type = $self->idp->interface->jpath('$.saml_type');

    if($type eq SAML_TYPE_KPN_LO) {
        return  'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256';
    }
    
    # Default to SHA1, this is required for Logius at least at this moment (2013-12-10)
    return 'http://www.w3.org/2000/09/xmldsig#rsa-sha1';
}

=head2 resolve_entity_id

Getter for the configured entity_id. By default this will return the id
configured on the SP, which defaults to a URI that identfies it. The
returned id can be overridden by specifying an idp_entity_id in the IdP
configuration

=cut

sub resolve_entity_id {
    my $self = shift;

    return $self->idp->interface->jpath('$.idp_entity_id') || $self->sp->id;
}


=head2 resolve_artifact

Contacts the IdP using the given SAML Artificat

=cut

sub resolve_artifact {
    my $self = shift;

    my $resolve_request = Zaaksysteem::SAML2::Protocol::ArtifactResolve->new(
        artifact => shift,
        issuer => $self->sp->id,
        destination => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP')
    );

    my $redirect = Zaaksysteem::SAML2::Binding::SOAP->new(
        url => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP'),
        key => $self->sp->cert,
        cert => $self->sp->cert,
        idp_cert => $self->idp->cert('signing'),
        cacert => $self->idp->cacert
    );

    return $redirect->request($resolve_request->as_xml);
}

1;

package Zaaksysteem::Backend::Object;

use Moose;
use Moose::Util::TypeConstraints;
use Moose::Util qw/ensure_all_roles/;

use Zaaksysteem::Exception;
use Zaaksysteem::Search::ZQL;

=head1 NAME

Zaaksysteem::Backend::Object

=head1 SUMMARY

This model class wraps around our database layer to provide a single wrapper
around things Zaaksysteem calls objects.

=head1 USAGE

This package represents some early glue between ordinary
L<DBIx::Class::ResultSet> objects and ZQL queryable resultsets.

When creating new objects, use this pattern:

    my $obj = $c->model('DB::ObjectData')->create(
        { object_class => 'saved_search' }
    );

    $obj->add_object_attributes(
        Zaaksysteem::Backend::Object::Attribute->new(
            name       => 'owner',
            value      => 'michiel',
            value_type => 'text'
        ),
        # etc.
    );

    $obj->update();

Idiomatically the interaction to get some data via a ZQL query looks like this:

    my $rs = $c->model('Object')->rs;

The above example will query all types of objects there are, under
no logical constraints.

    use Zaaksysteem::Search::ZQL;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

The above will query the object model for full-object hydrations of any
C<case> type object.

Complexer queries can of course be constructed, you can find more on the
syntax and usage in the L<Zaaksysteem::Search::ZQL> documentation.

=head2 Extracting data from the ResultSet

Being able to construct resultsets is all fine and dandy, but how to use the
data?

    use Zaaksysteem::Search::ZQL;
    use Data::Dumper;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

    for my $row ($rs->all) {
        warn Dumper($row->TO_JSON);
    }

=head1 ATTRIBUTES

=head2 schema

This holds a reference to the L<Zaaksysteem::Schema>. Required upon
construction.

=cut

has 'schema'    => (
    'is'            => 'ro',
    'required'      => 1,
    'isa'           => 'Zaaksysteem::Schema'
);

=head2 user

=cut 

has user => ( is => 'ro', isa => 'Maybe[Zaaksysteem::Schema::Subject]' );

=head2 ldap

=cut

has ldap => ( is => 'ro', isa => 'Zaaksysteem::Backend::LDAP::Model' );

=head2 source_name

This attribute holds the source name of the table the model instance is to
interact with. Defaults to C<ObjectData>.

=cut

has source_name => (
    is => 'ro',
    isa => 'Str',
    default => 'ObjectData'
);

=head1 METHODS

=head2 rs

Returns a resultset that can be used to query hstore properties. The returned
object will be of type L<Zaaksysteem::Backend::Object::Roles::ObjectResultSet>.
The rows returned by the resultset are of type
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>.

=cut

sub rs {
    my $self = shift;
    
    my $rs = $self->schema->resultset($self->source_name);

    ensure_all_roles($rs, 'Zaaksysteem::Backend::Object::Roles::ObjectResultSet');

    $rs->hydrate_actions(1);

    # If we were instantiated without a user, admin rights are implied and
    # no capability checks should be done.
    return $rs unless $self->user;

    my $alias = $rs->current_source_alias;

    my $subquery = $self->schema->resultset('ObjectAclEntry')->search(
        $self->acl_items('read', { '=' => \[ "$alias.uuid" ] }, { '=' => \[ "$alias.class_uuid" ] }),
        { alias => 'acl' }
    )->count_rs->as_query;

    # Deref subquery. just like we can build un-escaped plain SQL with
    # the \[] syntax, as_query returns a similar datastructure
    my ($subquery_sql, @subquery_bind) = @{ ${ $subquery } };

    # Because we're injecting in the WHERE clause as plain SQL, we need a
    # condition to be furfilled, so slap it on the subquery SQL.
    $subquery_sql .= ' >= ?';
    push @subquery_bind, [ {}, 1 ];

    return $rs->search_rs(\[ $subquery_sql, @subquery_bind ]);
}

=head2 acl_items

This method returns a HASH reference that L<SQL::Abstract> will interpret as
an 'or' disjunction. It builds this OR query by looking at the current user,
his/her group (=ou) and roles. Each returned item in the OR is a normal
L<SQL::Abstract> condition.

=cut

sub acl_items {
    my $self = shift;
    my $capability = shift;
    my $object_uuid = shift;
    my $class_uuid = shift;

    my @perm_entries;

    # Handle simple object-level capabilities
    push @perm_entries, {
        object_uuid => $object_uuid,
        entity_type => 'user',
        entity_id => $self->user->username,
        verdict => 'permission',
        capability => $capability
    };

    # Oh yes, exponential query length, user_ous * user_roles.
    for my $ou ($self->ldap->user_ous($self->user)) {
        for my $role ($self->ldap->get_roles($self->user)) {
            push @perm_entries, {
                object_uuid => $class_uuid,
                entity_type => 'position',
                capability => $capability,
                verdict => 'permission',
                entity_id => sprintf(
                    '%s|%s',
                    $ou->get_value('l'),
                    $role->get_value('gidNumber')
                )
            };
        }
    }

    # If *any* of the above defined conditions is true, we are authorized
    return { -or => \@perm_entries };
}

1;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.


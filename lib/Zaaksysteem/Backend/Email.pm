package Zaaksysteem::Backend::Email;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;


has 'schema'                => (
    is                          => 'rw',
    required                    => 1,
);

has 'default_from_email'    => (
    is                          => 'rw',
    required                    => 1,
);

has 'content_type'          => (
    is                          => 'rw',
    default                     => 'text/plain'
);

has 'charset'               => (
    is                          => 'rw',
    default                     => 'utf-8'
);

has 'email_sender' => (is => 'rw', required => 1);

Params::Profile->register_profile(
    method              => 'send_from_case',
    profile             => {
        required            => [qw/
            case
            notification

            betrokkene_id
        /],
        optional            => [qw/
            default_from_email
        /]
    }
);


has 'default'   => (
    'is'        => 'rw',
    'default'   => sub {
        my $self        = shift;
        return {
            content_type        => $self->content_type,
            charset             => $self->charset,
        }
    }
);

sub send_from_case {
    my $self                            = shift;
    my $opts                            = assert_profile(shift || {})->valid;

    my $notification                    = $opts->{notification};
    my $case                            = $opts->{case};
    my $schema                          = $self->schema;
    my $default_from                    = $opts->{default_from_email} || $self->default_from_email;

    # 'to' can be undefined, as long as one of these is set as the type
    if (!$notification->{to} && $notification->{recipient_type} =~ qr/(behandelaar|coordinator|aanvrager)/) {
        my $obj_str = sprintf "%s_object", $notification->{recipient_type};
        $notification->{to} = $case->$obj_str->email;
    }

    throw('backend/email/no_recipient', "Geen ontvanger voor bericht")
        unless $notification->{to};

    my $body            = $self->parse_special_vars($case, $notification->{body});
    my $subject         = $self->parse_special_vars($case, $notification->{subject});

    my $cd_attachments  = $self->parse_special_vars($case, $notification->{case_document_attachments});

    my $email = {
        from    => $notification->{from} || $default_from,
        to      => $notification->{to},
        subject => $subject,
        body    => $body,
    };

    #$c->log->debug('mailstash: ' . Dumper($c->stash->{email}));

    if ($body && $subject) {
        $schema->txn_do(sub {

            # if (scalar keys @{ $c->error }) {
            #     die(
            #         'Cannot unset error for sending mail: '
            #         . Dumper($c->error)
            #     );
            # }

            # $c->error(0);

            my %add_args = (
                zaak_id      => $case->id,
                zaakstatus   => $case->milestone,
                filename     => $notification->{to},
                documenttype => 'mail',
                category     => '',
                subject      => $subject,
                message      => $body,
                rcpt         => $notification->{to},
                betrokkene_id => $opts->{betrokkene_id},
            );

            # Handle any possible case document attachments by splitting the mail up in parts 
            if ($cd_attachments) {
                # When using parts in emails, the original body is ignored, we place it in the first part 
                $email->{parts} = [];
                push @{ $email->{parts} }, Email::MIME->create(
                    attributes => {
                        charset => 'utf-8',
                    },
                    body => $email->{body},
                );
            }

            my @contactmoment_attachments;
            for my $a (@{ $cd_attachments }) {
                next if (!$a->{selected});
                
                my $files = $schema->resultset('File')->search(
                    {
                        'case_documents.case_document_id' => $a->{case_document_ids},
                        case_id => $case->id,
                        date_deleted => undef,
                        accepted     => 1,
                    },
                    {
                        join => {case_documents =>'file_id'},
                    },
                );

                while (my $f = $files->next) {
                    # Create Email::MIME parts 
                    my $part = Email::MIME->create( 
                        attributes => { 
                            filename     => $a->{name}.$f->extension, 
                            content_type => $f->filestore->mimetype, 
                            encoding     => 'base64', 
                            disposition  => 'attachment', 
                            name         => $a->{name}.$f->extension, 
                        }, 
                        body => io($f->filestore->get_path), 
                    );
                                  
                    push @{ $email->{parts} }, $part; 
                    push @contactmoment_attachments, {
                        filename => $f->filename,
                        file_id  => $f->id,
                        email_filename => $a->{name}.$f->extension,
                    };
                }
            }

            # !!!!DO THE MAILING !!!!!
            $self->_process_mail($email);

            # if ( scalar( @{ $c->error } ) ) {
            #     my $msg = 'Errors while sending e-mail: ' .
            #         join(',',@{ $c->error });

            #     $c->push_flash_message($msg);

            #     $c->log->error($msg);

            #     $c->error(0);
            # } else {
                my $result  = $schema->resultset('Contactmoment')
                            ->contactmoment_create({
                                type       => 'email',
                                subject_id => $add_args{betrokkene_id},
                                created_by => $add_args{betrokkene_id},
                                medium     => 'balie',
                                case_id    => $case->id,
                                email      => {
                                   %{ $email },
                                   recipient  => $notification->{to},
                                   subject    => $subject,
                                   attachments => \@contactmoment_attachments,
                                }
                            });
            # }
        });
    # } else {
    #     $c->log->error(
    #         'C:Z:Mail: Body or Onderwerp empty?' .
    #         (
    #             $c->stash->{zaak}
    #                 ? ' Zaak: ' . $c->stash->{zaak}->nr
    #                 : ''
    #         )
    #     );
    }

    if ($@) {
        warn 'Could not send email: ' . $@;
        return;
    }

    return $body;
}

sub parse_special_vars {
    my ( $self, $case, $body ) = @_;

    if ((ref($body) eq 'ARRAY' && !@$body) || !$body) {
        return $body;
    }

    my $ztt = Zaaksysteem::ZTT->new;
    
    $ztt->add_context($case);

    return $ztt->process_template($body)->string;
}


sub _process_mail {
    my ( $self, $email ) = @_;

    # croak "Unable to send mail, bad mail configuration"
    #   unless $self->sender->{mailer};

    throw(
        'mail/no_input',
        "Can't send email without a valid email structure"
    ) unless $email;

    # Default content type
    $email->{content_type} = 'text/plain';

    my $header = $email->{header} || [];
    push @$header, ( 'To' => delete $email->{to} )
      if $email->{to};
    push @$header, ( 'Cc' => delete $email->{cc} )
      if $email->{cc};
    push @$header, ( 'From' => delete $email->{from} )
      if $email->{from};
    push @$header,
      ( 'Subject' => Encode::encode( 'MIME-Header', delete $email->{subject} ) )
      if $email->{subject};
    push @$header, ( 'Content-type' => $email->{content_type} )
      if $email->{content_type};

    my $parts = $email->{parts};
    my $body  = $email->{body};

    unless ( $parts or $body ) {
        throw(
            'mail/missing_body_or_parts',
            'Can\'t send email without parts or body, check stash'
        );
    }

    my %mime = ( header => $header, attributes => {} );

    if ( $parts and ref $parts eq 'ARRAY' ) {
        $mime{parts} = $parts;
    }
    else {
        $mime{body} = $body;
    }

    $mime{attributes}->{content_type} = $email->{content_type}
      if $email->{content_type};

    # if (
    #     $mime{attributes}
    #     and not $mime{attributes}->{charset}
    #     and $self->{default}->{charset} )
    # {
    #     $mime{attributes}->{charset} = $self->{default}->{charset};
    # }

    $mime{attributes}->{charset}  = 'utf-8';

    $mime{attributes}->{encoding} = $email->{encoding} 
        if $email->{encoding};

    my $message = $self->generate_message( \%mime );

    #warn('MESSAGE: ' . Data::Dumper::Dumper(\%mime));

    if ($message) {
        my $return = $self->email_sender->send( $message,
          {
            exists $email->{envelope_from} ? ( from => $email->{envelope_from} ) : (),
            exists $email->{envelope_to}   ? ( to   => $email->{envelope_to}   ) : (),
            transport => $self->_mailer_obj,
          } );

        # return is a Return::Value object, so this will stringify as the error
        # in the case of a failure.
        throw(
            'mail/error',
            "$return"
        ) if !$return;
    }
    else {
        throw(
            "mail/error",
            "Unable to create message"
        );
        #croak "Unable to create message";
    }
}

sub _mailer_obj {
    my ($self)              = @_;
    my $transport_class     = 'SMTP';

    # borrowed from Email::Sender::Simple -- apeiron, 2010-01-26
    if ( $transport_class !~ /^Email::Sender::Transport::/ ) {
        $transport_class = "Email::Sender::Transport::$transport_class";
    }

    Class::MOP::load_class($transport_class);

    return $transport_class->new( {} );
}

sub setup_attributes {
    my ( $self, $attrs ) = @_;

    my $default_content_type = $self->default->{content_type};
    my $default_charset      = $self->default->{charset}; 

    my $e_m_attrs = {};

    if (   exists $attrs->{content_type}
        && defined $attrs->{content_type}
        && $attrs->{content_type} ne '' )
    {
        # $c->log->debug( 'C::V::Email uses specified content_type '
        #       . $attrs->{content_type}
        #       . '.' )
        #   if $c->debug;
        $e_m_attrs->{content_type} = $attrs->{content_type};
    }
    elsif ( defined $default_content_type && $default_content_type ne '' ) {
        # $c->log->debug(
        #     "C::V::Email uses default content_type $default_content_type.")
        #   if $c->debug;
        $e_m_attrs->{content_type} = $default_content_type;
    }

    if (   exists $attrs->{charset}
        && defined $attrs->{charset}
        && $attrs->{charset} ne '' )
    {
        $e_m_attrs->{charset} = $attrs->{charset};
    }
    elsif ( defined $default_charset && $default_charset ne '' ) {
        $e_m_attrs->{charset} = $default_charset;
    }

    return $e_m_attrs;
}



sub generate_message {
    my ( $self, $attr ) = @_;

    # setup the attributes (merge with defaultis)
    $attr->{attributes} = $self->setup_attributes( $attr->{attributes} );
    Email::MIME->create( %$attr );
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


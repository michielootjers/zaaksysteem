package Zaaksysteem::Backend::Object::AttributeRole;
use Moose::Role;

requires '_calculate_value', 'value';

use Zaaksysteem::Exception;

=head1 NAME

Zaaksysteem::Backend::Object::AttributeRole::DaysLeft - "Days left" calculated attribute

=head1 SYNOPSIS

    package MyDynamicAttribute;
    use Moose::Role;

    with 'Zaaksysteem::Backend::Object::AttributeRole';

    sub _calculate_value {
        my $self = shift;

        # Do work here;

        return 42;
    }
        
=head1 METHODS

=head2 value

This is a wrapper for the accessor of the C<value> attribute.

It disallows setting (which would be silly), 

=cut

around 'value' => sub {
    my $orig = shift;
    my $self = shift;

    if (@_) {
        throw(
            'DynamicAttribute/not_settable',
            'Dynamic attributes cannot be set (as the value is calculated)',
        );
    }

    my $calculated_value = $self->_calculate_value();

    return $self->$orig($calculated_value);
};

sub _get_attribute {
    my $self = shift;
    my ($attr) = @_;

    my $attribute = $self->parent_object->get_object_attribute($attr)
        or throw("DynamicAttribute/missing_attribute", "$attr attribute missing");

    return $attribute;
}

before '_calculate_value' => sub {
    my $self = shift;

    throw(
        'DynamicAttribute/no_parent_object',
        'DynamicAttribute cannot be calculated without parent object',
    ) unless defined $self->parent_object;
};

override 'TO_JSON' => sub {
    my $self = shift;

    return {
        name           => $self->name,
        attribute_type => $self->attribute_type,
        dynamic_class  => $self->dynamic_class,
    };
};

1;

package Zaaksysteem::Backend::Object::AttributeRole::CaseDestructionBlocked;
use Moose::Role;

use DateTime;
use Zaaksysteem::Exception;

with 'Zaaksysteem::Backend::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Backend::Object::AttributeRole::CaseDestructionBlocked - "Is this destruction of this case blocked" - calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Backend::Object::Attribute->new(
        dynamic_class => 'CaseDestructionBlocked',
        parent_object => $some_obj,
        ...
    );
    print "Destruction is" . ($attr->value ? '' : " not") . " blocked";

=head1 METHODS

See L<Zaaksysteem::Backend::Object::AttributeRole> for the inherited wrapper
around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $parent = $self->parent_object;
    my $case = $parent->get_source_object();

    return ($case->can_delete()) ? 0 : 1;
}

1;

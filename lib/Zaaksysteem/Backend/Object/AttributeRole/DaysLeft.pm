package Zaaksysteem::Backend::Object::AttributeRole::DaysLeft;
use Moose::Role;

use DateTime;
use Zaaksysteem::Exception;

with 'Zaaksysteem::Backend::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Backend::Object::AttributeRole::DaysLeft - "Days left" calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Backend::Object::Attribute->new(
        dynamic_class => 'DaysLeft',
        parent_object => $some_obj,
        ...
    );
    print "There are " . $attr->value . " days left";

=head1 METHODS

See L<Zaaksysteem::Backend::Object::AttributeRole> for the inherited wrapper around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $target = $self->_get_attribute('case.date_target')->value;
    my $completion = $self->_get_attribute('case.date_of_completion')->value;

    # The target can be a string ("Opgeschort"), in that case "days left" is nonsensical.
    if (!blessed($target)) {
        return undef;
    }

    if (!blessed($completion)) {
        $completion = DateTime->now();
    }

    my $delta_days = $target->delta_days($completion)->in_units('days');

    my $difference = ($completion > $target)
        ? (-1 * $delta_days)
        : ($delta_days);

    return $difference;
}

1;

package Zaaksysteem::Backend::Object::AttributeRole::CaseDestructable;
use Moose::Role;

use DateTime;
use Zaaksysteem::Exception;

with 'Zaaksysteem::Backend::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Backend::Object::AttributeRole::CaseDestructable - "Is this case destructable" - calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Backend::Object::Attribute->new(
        dynamic_class => 'CaseDestructable',
        parent_object => $some_obj,
        ...
    );
    print "This is is" . ($attr->value ? '' : "n't") . " destructable";

=head1 METHODS

See L<Zaaksysteem::Backend::Object::AttributeRole> for the inherited wrapper around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $date_destruction = $self->_get_attribute('case.date_destruction')->value;

    # No date set
    if (!defined($date_destruction)) {
        return 0;
    }

    return 1 if $date_destruction < DateTime->now();
    return 0;
}

1;

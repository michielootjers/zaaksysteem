package Zaaksysteem::Backend::Object::AttributeRole::DaysPercentage;
use Moose::Role;

use DateTime;
use List::Util qw(max);
use Zaaksysteem::Exception;

with 'Zaaksysteem::Backend::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Backend::Object::AttributeRole::DaysPercentage - "Percentage of days left" calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Backend::Object::Attribute->new(
        dynamic_class => 'DaysPercentage',
        parent_object => $some_obj,
        ...
    );
    print $attr->value . "% of allotted time has passed";

=head1 METHODS

See L<Zaaksysteem::Backend::Object::AttributeRole> for the inherited wrapper around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $target       = $self->_get_attribute('case.date_target')->value;
    my $completion   = $self->_get_attribute('case.date_of_completion')->value;
    my $registration = $self->_get_attribute('case.date_of_registration')->value;

    # The target can be a string ("Opgeschort"), in that "percentage" is nonsensical.
    if (!blessed($target)) {
        return undef;
    }

    if (blessed($completion)) {
        $completion = $completion->epoch - $registration->epoch;
    }
    else {
        $completion = DateTime->now()->epoch - $registration->epoch;
    }

    my $percentage = sprintf("%d", 100 * ($completion / max($target->epoch - $registration->epoch, 1)));

    return $percentage + 0;
}

1;

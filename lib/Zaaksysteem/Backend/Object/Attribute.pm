package Zaaksysteem::Backend::Object::Attribute;

use Moose;
use Moose::Util::TypeConstraints;
use Moose::Util qw/ensure_all_roles/;

use Zaaksysteem::Exception;

### XXX TODO Add ATTRIBUTE_TYPE: Bag, so we can parse Bag entries.
use constant ATTRIBUTE_TYPES    => {
    'bag'           => 'Zaaksysteem::Backend::Object::Attribute::BAG',
    'timestamp'     => 'Zaaksysteem::Backend::Object::Attribute::Timestamp',
    'text'          => 'Zaaksysteem::Backend::Object::Attribute::Text',
    'integer'       => 'Zaaksysteem::Backend::Object::Attribute::Integer',
    'object'        => 'Zaaksysteem::Backend::Object::Attribute::Object',

    # Special case to handle things like "streefafhandeldatum", which can be
    # either text ("Opgeschort") or a DateTime
    'timestamp_or_text' => 'Zaaksysteem::Backend::Object::Attribute::TimestampOrText',
};

use constant JSON_VIEW_COLUMNS  => [
    qw/
        name
        value
        human_value
        attribute_type
    /
];

use constant JSON_DESCRIBE_COLUMNS  => [
    qw/
        name
        attribute_type
    /
];

enum 'AttributeTypes',      [keys %{ ATTRIBUTE_TYPES() }];

=head1 NAME

Zaaksysteem::Backend::Object::Attribute - Defines a attribute, compatible with object JSON

=head1 SYNOPSIS



=head1 DESCRIPTION

This is the helloworld backend template

=head1 ATTRIBUTES

=head2 type

Defines the type of this attribute, can be one of:

=over 4

=item bag

Indicates value is a BAG entry

=item timestamp

Indicates value is a timestamp

=item text

Indicates value contains freetext

=item integer

Indicates value contains only numeric values 

=back

=cut

has 'attribute_type'        => (
    'is'            => 'ro',
    'required'      => 1,
    'isa'           => 'AttributeTypes',
);

=head2 name

The name for this attribute

=cut

has 'name'                  => (
    'is'            => 'ro',
    'required'      => 1,
    'isa'           => 'Str'
);

=head2 bwcompat_name

Name (for backwards compatibility)

=cut

has 'bwcompat_name' => (
    is       => 'ro',
    required => 0,
    isa      => 'Str | ArrayRef[Str]'
);

=head2 _json_data

Defines the contents of TO_JSON

=cut

has '_json_data'            => (
    'is'            => 'ro',
    'lazy'          => 1,
    'builder'       => '_build_json_data',
    'clearer'       => '_clear_json_data',
);

sub _build_json_data {
    my $self                = shift;

    my $rv                  = {};
    for my $col (@{ JSON_VIEW_COLUMNS() }) {
        $rv->{ $col } = $self->$col;
    }

    return $rv;
}

=head2 _describe_data

Describes this attribute

=cut


has '_describe_data'            => (
    'is'            => 'ro',
    'lazy'          => 1,
    'builder'       => '_build_describe_data',
);

sub _build_describe_data {
    my $self                = shift;

    my $rv                  = {};
    for my $col (@{ JSON_DESCRIBE_COLUMNS() }) {
        $rv->{ $col } = $self->$col;
    }

    return $rv;
}

=head2 parent_object

Object the attribute is attached to. A weak reference.

=cut

has 'parent_object' => (
    is       => 'rw',
    weak_ref => 1,
);

=head2 dynamic_class

The name of the role implementing the value of this attribute, if this is a
"dynamic" attribute.

If this is set, the specified role (prefixed with
L<Zaaksysteem::Backend::Object::AttributeRole>) will be applied to the
attribute.

=cut

has 'dynamic_class' => (
    is  => 'ro',
    isa => 'Maybe[Str]',
);

=head2 object_table

(Optional) reference to the exact table this value is from.

=cut

has 'object_table'          => (
    'is'            => 'ro',
    'isa'           => 'Str'
);

=head2 object_id

(Optional) reference to the exact table-id this value is from.

=cut

has 'object_id'             => (
    'is'            => 'ro',
);

=head2 object_row

(Optional) reference to the DBIx::Class Component for this value

=cut

has 'object_row'            => (
    'is'            => 'ro',
);

=head2 value

Machine representational value for this attribute. For a human readable value,
see below.

=cut

has 'value'                 => (
    'is'            => 'rw',
    'trigger'       => sub {
        $_[0]->_clear_human_value;
        $_[0]->_clear_json_data;
    },
    'clearer'       => '_clear_value',
);

=head2 human_value

The human readable value for this attribute, in case of the bag identifier, this could be
C<woonplaats>

=cut

has 'human_value'           => (
    'is'            => 'rw',
    'lazy'          => 1,
    'builder'       => '_build_human_value',
    'clearer'       => '_clear_human_value',
);

=head2 format

Format string that can be used by roles to format the value for human consumption.

=cut

has 'format' => (
    'is'       => 'rw',
    'isa'      => 'Defined',
    'required' => 0,
);

=head2 is_filter

This defines the attribute can be filtered on via ZQL

=cut

has 'is_filter'             => (
    'is'            => 'ro',
);

=head2 is_systeemkenmerk

This defines the attribute can be filtered on via ZQL

=cut

has 'is_systeemkenmerk'             => (
    'is'            => 'ro',
);

has 'systeemkenmerk_reference'      => (
    'is'            => 'ro',
    'isa'           => 'CodeRef'
);


no Moose::Util::TypeConstraints;

=head1 METHODS

=cut

sub BUILD {
    my $self                        = shift;

    ensure_all_roles($self, ATTRIBUTE_TYPES->{ $self->attribute_type });

    if ($self->dynamic_class) {
        ensure_all_roles(
            $self,
            "Zaaksysteem::Backend::Object::AttributeRole::" . $self->dynamic_class
        );
    }

    $self->_prepare_attribute_object;
}

=head2 TO_JSON

Stringifies this object to JSON when called with the JSON module

=cut

sub TO_JSON {
    my $self                        = shift;

    $self->_clear_json_data;
    return $self->_json_data;
}

sub FROM_JSON {
    my $class                       = shift;
    my $json                        = shift;

    $class                          = ref($class) if ref($class);

    return $class->new($json);
}

=head2 vectorize_value

This generic method implements some logic required for clean free-form text
vectors. It flattens values if embedded in an ARRAYREF, lower-cases the string
and filters anything that is not a word-like character, whitespace, or a
literal '.' or '@'.

=cut

sub vectorize_value {
    my $self = shift;

    my $str = $self->value;

    return unless defined $str;

    if(ref $str eq 'ARRAY') {
        $str = join ' ', @{ $str }
    }

    $str = lc($str);

    $str =~ s/[^\w\s\.\@]//g;

    return $str;
}

=head2 describe

Describes this object

=cut

sub describe {
    my $self                        = shift;

    return $self->_describe_data;
}

=head2 index_value

Returns the "index" value for this attribute (for hstore/search queries).

=cut

sub index_value {
    my $self = shift;

    if (ref $self->value) {
        return;
    }

    return  $self->value;
}

=head2 as_hash_entry

Return a hash key + value for this attribute.

=cut

sub as_hash_entry {
    my $self = shift;

    return ($self->name, $self);
}

=head1 INTERNAL METHODS

=head2 _prepare_attribute_object

Called directly after applying the C<Attribute::*> roles. Here you could write your own
BUILD like methods

=cut

sub _prepare_attribute_object {}

__PACKAGE__->meta->make_immutable;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.


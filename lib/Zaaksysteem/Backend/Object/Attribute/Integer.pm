package Zaaksysteem::Backend::Object::Attribute::Integer;

use Moose::Role;

has 'value' => (
    is      => 'rw',
    isa     => 'Maybe[Num]'
);

=head2 _build_human_value

On our Text role, the value is identical to human_value, unless overridden

=cut

sub _build_human_value {
    my $self = shift;

    return $self->value;
}

=head2 TO_JSON

This is a wrapper for L<Zaaksysteem::Backend::Object::Attribute#TO_JSON> that
forces the "value" attribute to look numeric for JSON.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    # Make sure number values actually ARE numbers, in the JSON output
    my $json_data = $self->$orig(@_);

    if (defined $json_data->{value}) {
        $json_data->{value} += 0;
    }

    return $json_data;
};

1;
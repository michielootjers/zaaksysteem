package Zaaksysteem::Backend::Object::Attribute::TimestampOrText;
use Moose::Role;
use Scalar::Util qw(blessed);

has 'value' => (
    is      => 'rw',
    isa     => 'Maybe[DateTime] | Maybe[Str]'
);

=head2 _build_human_value

Return the human-readable format for this field.

If the value is a DateTime instance, it will be formatted using $self->format,
if it's not, the plain value is returned (it's most likely a plain string).

=cut

sub _build_human_value {
    my $self = shift;

    if (blessed($self->value) && $self->value->isa('DateTime')) {
        return $self->value->strftime($self->format || '%d-%m-%Y') if $self->value;
    }

    return $self->value;
}

sub index_value {
    my $self = shift;

    if (blessed($self->value) && $self->value->isa('DateTime')) {
        return $self->value->iso8601;
    }

    return;
}

1;

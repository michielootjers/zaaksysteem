package Zaaksysteem::Backend::Object::Attribute::Object;

use Moose::Role;

=head2 _build_human_value

On our "Object" role, the "human value" (as returned by ZAPI calls, for
instance) is identical to the internal value.

=cut

sub _build_human_value {
    my $self                        = shift;
    return $self->value;
}

1;

package Zaaksysteem::Backend::Object::Attribute::Text;

use Moose::Role;

=head2 _build_human_value

On our Text role, the value is identical to human_value, unless overridden

=cut

sub _build_human_value {
    my $self  = shift;
    my $value = $self->value;

    if (blessed($self->value) && $self->value->isa('Zaaksysteem::Types::MappedString')) {
        return $self->value->mapped;
    }

    return $self->value;
}

=head2 index_value

Returns the indexable value.

=cut

sub index_value {
    my $self  = shift;
    my $value = $self->value;

    if (blessed($self->value) && $self->value->isa('Zaaksysteem::Types::MappedString')) {
        return $self->value->original;
    }

    return $self->value;
}

1;

package Zaaksysteem::Backend::Object::Attribute::BAG;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Object::Attribute::BAG - Role for an attribute of type 'BAG'

=head1 METHODS

=head2 _build_human_value

Return the "human-readable" (in this case, "front-end parseable") value for
this field.

=cut

sub _build_human_value {
    my $self = shift;
    return unless $self->value;
    return $self->value->{human_identifier};
}

=head2 index_value

Return the "indexable" (searchable) value for a BAG field: its (like
"openbareruimte-123456")

=cut

sub index_value {
    my $self = shift;
    return unless $self->value;

    return $self->value->{bag_id};
}

1;

package Zaaksysteem::Backend::Object::Attribute::Timestamp;

use Moose::Role;

has 'value' => (
    is      => 'rw',
    isa     => 'Maybe[DateTime]'
);

=head2 _build_human_value

On our Text role, the value is identical to human_value, unless overridden

=cut

sub _build_human_value {
    my $self                        = shift;

    return $self->value->strftime($self->format || '%d-%m-%Y') if $self->value;
}

sub index_value {
    my $self = shift;

    if ($self->value) {
        return $self->value->iso8601;
    }

    return;
}

1;

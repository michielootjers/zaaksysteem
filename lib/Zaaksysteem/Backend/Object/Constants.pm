package Zaaksysteem::Backend::Object::Constants;
use warnings;
use strict;
use utf8;

use Exporter 'import';

our @EXPORT = qw/
    OBJECT_TYPES
/;

=head1 NAME

Zaaksysteem::Backend::Object::Constants - Constants for the "Object"

=head1 CONSTANTS

=head2 OBJECT_TYPES

A hash of database-level object types, mapped to the role(s) that should be
applied to the L<Zaaksysteem::Backend::Object::Data::Component> object.

=over

=item * case

L<Zaaksysteem::Backend::Object::Data::Roles::Case>

=item * saved_search

L<Zaaksysteem::Backend::Object::Data::Roles::SavedSearch>

=back

=cut

use constant OBJECT_TYPES    => {
    'case'          => 'Zaaksysteem::Backend::Object::Data::Roles::Case',
    'saved_search'  => 'Zaaksysteem::Backend::Object::Data::Roles::SavedSearch',
};

1;

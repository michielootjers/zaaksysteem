package Zaaksysteem::Backend::Object::Data::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::ResultSet';

with qw/
    Zaaksysteem::Backend::Object::Roles::ObjectResultSet
    Zaaksysteem::Search::HStoreResultSet
    Zaaksysteem::Search::TSVectorResultSet
/;

=head1 NAME

Zaaksysteem::Backend::Object::Data::ResultSet - Returns a ResultSet according to the object principals

=head1 SYNOPSIS

    ### ZQL: Retrieve all objects of type case
    my $resultset   = $object->from_zql('select case.id, casetype.title from case');

    
    ### List of attributes requested
    print join(' , ', @{ $resultset->object_requested_attributes });
    
    # prints:
    # case.id , casetype.title

    ### Make sure every row describes hisself
    $resultset->describe_rows(1)

    ### Loop over rows
    while (my $row = $resultset->next) {

        ## Prints uuid of object
        print $row->id

        ## prints JSON representation of row
        print Data::Dumper::Dumper( $row->TO_JSON )
    }

=head1 DESCRIPTION

ResultSet returned when called from L<Zaaksysteem::Backend::Object>. Every row retrieved from
this resultset, will be blessed with L<Zaaksysteem::Backend::Object::Data::Component>.

=head1 ATTRIBUTES

=head2 describe_rows(BOOLEAN)

When set, it will describe the rows on JSON output

=head1 METHODS

=head2 hstore_column

Tell the Zaaksysteem::Search::HStoreResultSet role where to look.

=cut

sub hstore_column { 'index_hstore' }

sub text_vector_column { 'text_vector' }

=head2 find_or_create_by_object_id($object_type, $object_id)

Return value: $row
    
    resultset('ObjectData')->find_or_create_by_object_id(
        'case', $self->id
    );

=cut

sub find_or_create_by_object_id {
    my $self                        = shift;
    my ($object_class, $object_id)  = @_;

    throw(
        'object/data/resultset/find_or_create_by_object_id/invalid_params',
        'Need at least object_class and object_id'
    ) unless ($object_class && $object_id && $object_id =~ /^\d+$/);

    my $row                         = $self->find(
        {
            object_class        => $object_class,
            object_id           => $object_id
        }
    );

    return $row if $row;

    return $self->create(
        {
            object_class        => $object_class,
            object_id           => $object_id
        }
    );
}


=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

package Zaaksysteem::Backend::Object::Data::Roles::Case;

use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::Case - Object behaviors for the
'case' object_class instances

=head1 SYNOPSIS

Implements specific code behaviors exclusive to object instances with class
'case'.

=head1 CONSTANTS

=head2 ATTRIBUTES

This constant is an ARRAYREF containing a listing of magic string names that
should be included in the free-text vector.

=cut

use constant ATTRIBUTES => [qw[
    requestor.full_name
    requestor.family_name
    requestor.bsn
    requestor.email
    recipient.full_name
    recipient.family_name
    recipient.bsn
    recipient.email
    case.assignee
    assignee.email
    assignee.phone_number
    case.coordinator
    coordinator.email
    coordinator.phone_number

    case.number
    case.subject
    case.date_of_registration
    case.date_of_completion

    casetype.name
]];

use constant ACTIONS => [
    { slug => 'allocate' },
    { slug => 'acquire' },
    { slug => 'suspend' },
    { slug => 'resume' },
    { slug => 'prolong' },
    { slug => 'relate' },
    { slug => 'manage' },
    { slug => 'destroy' },
    { slug => 'publish' },
    { slug => 'export' }
];

=head1 METHODS

=head2 text_vector_terms

This method overrides L<Zaaksysteem::Backend::Object::Data::Component>'s
C<text_vector_terms> method, since case-like objects should not have a
generic free-text vector. See the C<ATTRIBUTES> constant for a listing of
magic strings we want to include in the vector.

=cut 

override text_vector_terms => sub {
    my $self = shift;
    my @terms;

    for my $attr (@{ $self->object_attributes }) {
        # Yeah, this is a bit nasty, but we can't call
        # $object->get_attribute_by_name yet, so it'll have to do.
        # TODO refactor when ZS-2178 lands in quarterly.
        next unless grep { $attr->name eq $_ } @{ ATTRIBUTES() };

        my $term = $attr->vectorize_value;
        push @terms, $term if defined $term;
    }

    return @terms;
};

=head2 get_source_object

This method overrides L<Zaaksysteem::Backend::Object::Data::Component>'s
C<get_source_object> to retrieve a C<Zaak> object based on the C<object_id> of
this row.

=cut

override get_source_object => sub {
    my $self = shift;

    my $object_id = $self->object_id;
    my $zaak = $self->result_source->schema->resultset('Zaak')->find($object_id);

    return $zaak;
};

override object_actions => sub {
    my $self = shift;

    return [ map { $_->{ slug } } @{ ACTIONS() } ];
};

1;

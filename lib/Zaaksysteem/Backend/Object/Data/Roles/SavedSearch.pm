package Zaaksysteem::Backend::Object::Data::Roles::SavedSearch;
use Moose::Role;

use Zaaksysteem::Profile;

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::SavedSearch

=head1 DESCRIPTION

Role for "saved search" objects.

=head1 METHODS

=head2 save_search_data

Save a search query (frontend-parsed ZQL).

=head3 Arguments

=over

=item * owner

Owner of this saved search.

=item * query

The search itself, as represented in a JSON data structure the frontend knows how to handle.

=back

=head3 Returns

A true value on success.

=cut

define_profile save_search_data => (
    required => [qw(owner query)],
    optional => [qw(title)],
    typed => {
        title => 'Str',
        owner => 'Str',
        query => 'HashRef',
    },
);

sub save_search_data {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $self->add_object_attributes(
        Zaaksysteem::Backend::Object::Attribute->new(
            name           => 'owner_subject_id',
            attribute_type => 'text',
            value          => $opts->{owner},
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name           => 'query',
            attribute_type => 'object',
            value          => $opts->{query},
        ),
        $opts->{title}
            ? (
                Zaaksysteem::Backend::Object::Attribute->new(
                    name           => 'title',
                    attribute_type => 'text',
                    value          => $opts->{title},
                )
              )
            : (),
    );
    $self->update();

    return 1;
}

1;

package Zaaksysteem::Backend::Object::Data::Component;
use Moose;

use Encode qw/encode_utf8/;
use JSON;
use Moose::Util qw/apply_all_roles/;
use Zaaksysteem::Backend::Object::Constants;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::Component';
with 'Zaaksysteem::Backend::Object::Roles::ObjectComponent';

=head1 NAME

Zaaksysteem::Backend::Object::Data::Component - Returns a DBIx::Class row with Object principals

=head1 SYNOPSIS

    my $row = $resultset->first;

    print $row->uuid;
    print $row->object_class;
    print $row->object_id;
    print $row->properties;
    print $row->hstore_index;

    ### Prints every attribute name available in this object
    print $_->name for @{$row->object_attributes}

    ### Updates $row->hstore_index.
    $row->reload_index;
    $row->update

    ### Or: quicker
    $row->update_index

=head1 DESCRIPTION

Row returned when row is requested from L<Zaaksysteem::Backend::Object::Data::ResultSet>. It
is inflated with the principles of our Object Management system.

=head1 DATABASE COLUMNS

=head2 uuid

Return value: UUID

Returns the UUID of this row

=head2 object_class

Return value: $STRING_OBJECT_CLASS_NAME

Returns the name of this object class, like C<case>

=head2 object_id

Return value: $INTEGER_FOR_OBJECT_CLASS

For some object classes we need a incrementing serial, object_id will contain this value. For
instance, for C<case> this will be the case number.

=head2 properties

Return value: HashRef of object properties

Will contain an HashRef of object properties

=head2 hstore_index

Return value: Pg HSTORE string

Will return the Pg::Hstore of this object

=head2 text_vector

This is a C<tsvector> column used for full-text searches on the object data.

I have no idea yet what it's gonna return. Some list of tokens in the best case.

=head2 date_created

Return value: $DATETIME

Will return the timestamp of row creation

=head2 date_modified

Return value: $DATETIME

Will return the timestamp of the last time this row had been updated

=head1 ATTRIBUTES

=head2 object_attributes

Return value: ArrayRef[L<Zaaksysteem::Backend::Object::Attribute>]

Returns an ArrayRef of L<Zaaksysteem::Backend::Object::Attribute> objects,
defining the attributes for this object.

To add one or more object attributes, use C<add_object_attributes>, don't
modify this attribute yourself!

=cut

has 'object_attributes'    => (
    'is'            => 'rw',
    'lazy'          => 1,
    'isa'           => 'ArrayRef[Zaaksysteem::Backend::Object::Attribute]',
    'builder'       => '_build_attribute_list',
    'clearer'       => '_clear_object_attributes',
);

sub _build_attribute_list {
    my $self                    = shift;
    my @rv;

    return \@rv unless ($self->properties && $self->properties->{values});

    for my $value (values %{ $self->properties->{values} }) {
        delete($value->{value}) unless length($value->{value});

        push(
            @rv,
            Zaaksysteem::Backend::Object::Attribute->new(
                %$value,

                # Don't put parent_object in $value, as that will create a
                # circular reference that can't be reaped by Perl's GC.
                $value->{dynamic_class}
                    ? (parent_object => $self)
                    : (),
            )
        );
    }

    # Special attributes for easy querying
    push @rv, Zaaksysteem::Backend::Object::Attribute->new(
        name           => 'object.id',
        attribute_type => 'text',
        value          => $self->object_id,
    );
    push @rv, Zaaksysteem::Backend::Object::Attribute->new(
        name           => 'object.uuid',
        attribute_type => 'text',
        value          => $self->uuid,
    );
    push @rv, Zaaksysteem::Backend::Object::Attribute->new(
        name           => 'object.date_created',
        attribute_type => 'timestamp',
        value          => $self->date_created,
    );
    push @rv, Zaaksysteem::Backend::Object::Attribute->new(
        name           => 'object.date_modified',
        attribute_type => 'timestamp',
        value          => $self->date_modified,
    );

    return \@rv;
}

=head1 METHODS

=head2 $row->add_object_attributes([$ATT1 [,$ATT2 [,$ATT3 ,[...]]])

Add one or more L<Zaaksysteem::Backend::Object::Attribute> instances to the
Object and make sure they're saved to the database on ->update.

Returns true on success.

=cut

sub add_object_attributes {
    my $self = shift;
    my @object_attributes = @_;

    my %values = map { $_->name, $_ } @object_attributes;

    $self->properties({ values => \%values });

    return 1;
}

=head2 $row->replace_object_attributes([$ATT1 [,$ATT2 [,$ATT3 ,[...]]]);

Replace all L<Zaaksysteem::Backend::Object::Attribute> instances on the Object and
make sure they're saved to the database on ->update.

Returns true on success.

=cut

sub replace_object_attributes {
    my $self = shift;

    $self->add_object_attributes(@_);

    return 1;
}

=head2 grant

This method modifies the ACLs linked to this object by granting the entity
specific capabilities through a positive permission.

    $object->grant($subject, qw[read write]);

The above example would grant the C<read> and C<write> capabilities for the
provided entity.

=cut

sub grant {
    my $self = shift;

    my ($entity, @capabilities) = @_;

    return unless $entity;

    my $acl = $self->result_source->schema->resultset('ObjectAclEntry');

    unless ($entity->can('security_identity')) {
        throw(
            'object/acl/entity',
            'Provided entity doesn\'t implement security identity'
        );
    }

    my %sec_id = $entity->security_identity;

    my @ret;

    for my $capability (@capabilities) {
        for my $type (keys %sec_id) {
            push @ret, $acl->create({
                object_uuid => $self->uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => $capability,
                verdict => 'permission'
            });
        }
    }

    return @ret;
}

=head2 proscribe

This method modifies the ACLs linked to this object by proscribing the entity
specific capabilities through a positive proscription. This has the reverse
effect of grant.

    $object->proscribe($subject, qw[read write]);

The above example will prohibit the C<read> and C<write> capabilities for the
provided entity.

=cut

sub proscribe {
    my $self = shift;

    my ($entity, @capabilities) = @_;

    return unless $entity;

    my $acl = $self->result_source->schema->resultset('ObjectAclEntry');

    unless ($entity->can('security_identity')) {
        throw(
            'object/acl/entity',
            'Provided entity doesn\'t implement security_identity'
        );
    }

    my %sec_id = $entity->security_identity;

    my @ret;

    for my $capability (@capabilities) {
        for my $type (keys %sec_id) {
            push @ret, $acl->create({
                object_uuid => $self->uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => $capability,
                verdict => 'proscription'
            });
        }
    }

    return @ret;
}

=head2 revoke

This method modified the ACLs linked to this object by revoking the entity
specific capabilities

    $object->revoke($subject, $verdict, qw[read write]);

The above example would revoke the C<read> and C<write> capabilities for
the entity provided.

=cut

sub revoke {
    my $self = shift;
    my ($entity, $verdict, @capabilities) = @_;

    return unless $entity;

    unless ($entity->can('security_identity')) {
        throw(
            'object/acl/entity',
            'Provided entity doesn\'t implement security identity'
        );
    }

    # Play nicely here, revoking no capabilities is safe
    return unless scalar @capabilities;

    my ($type, $id) = $entity->security_identity;

    return $self->result_source->schema->resultset('ObjectAclEntry')->search({
        object_uuid => $self->uuid,
        entity_type => $type,
        entity_id => $id,
        verdict => $verdict,
        capability => [ @capabilities ]
    })->delete_all;
}

=head2 $row->get_object_attribute($name)

Retrieve a specific object attribute by its name.

=cut

sub get_object_attribute {
    my $self = shift;
    my ($attribute_name) = @_;

    my %attribute_properties = %{ $self->properties->{values}{$attribute_name} };

    return Zaaksysteem::Backend::Object::Attribute->new(
        %attribute_properties,

        # Don't put parent_object in $value, as that will create a
        # circular reference that can't be reaped by Perl's GC.
        $attribute_properties{dynamic_class}
            ? (parent_object => $self)
            : (),
    );
}

=head2 $row->object_actions

Retrieve all slugnames for actions that can be executed on this object.

The behavior of this method is overridden by the object_class based roles
(such as L<Zaaksysteem::Backend::Object::Data::Role::Case>).

Returns: undef

=cut

sub object_actions {
    return;
}

=head2 $row->reload_index()

Return value: $TRUE_ON_SUCCESS

Will reload the hstore_index according to the object properties. PLEASE make sure you
call update after this, or it won't be saved in the database. Use C<<$row->update_index>>
if you want to update the index on the database.

=cut


sub reload_index {
    my $self                        = shift;

    # Clear "object_attributes", so they'll be regenerated from next time around.
    $self->_clear_object_attributes();

    $self->load_hstore;
    $self->load_text_vector;

    return 1;
}

=head2 $row->load_hstore()

Return value: $row->hstore_index

Will set C<hstore_index> with the correct hstore_representation of C<<$row->object_attributes>>

=cut

sub load_hstore {
    my $self                        = shift;
    my $hr = $self->_hstore_representation;

    my %hr_fixed = (
        # XXX: PostgreSQL cries if an index value is longer than 2712 bytes
        map { $_ => (defined $hr->{$_}) ? substr($hr->{$_}, 0, 1000) : undef } keys %$hr
    );
    $self->index_hstore(\%hr_fixed);
}

=head2 update_text_vector

Updates the C<text_vector> column with 'smartly' gathered strings.

This method uses C<text_vector_terms> to retrieve terms to set the
column to.

Takes a list of terms to include in the C<TSVECTOR> column (can be empty).

Does B<not> automatically C<< $row->update >>.

=cut

sub load_text_vector {
    my $self = shift;

    $self->text_vector(join(' ', grep { length > 2 } ($self->text_vector_terms, @_)));
}

=head2 text_vector_terms

This tries to 'smartly' gather relevant strings contained in the object
instance, but iterating over all attributes and selecting those that have
an implied human-readable string representation (C<text> attribute_type).

This method is mainly here to seperate the logic of collecting the terms
from the logic of updating the text vector. Extend and override all ye
want, just make sure it does something like this:

    my @strings = $row->text_vector_terms

=cut

sub text_vector_terms {
    my $self = shift;

    my @ret;

    for my $attr (@{ $self->object_attributes }) {
        next unless $attr->attribute_type eq 'text';

        my $str = $attr->vectorize_value;

        push @ret, $str if defined $str;
    }

    return @ret;
}

=head2 $row->get_source_object()

Retrieve the source object. Should be implemented in the object_class-specific
roles.

=cut

sub get_source_object { }

=head1 INTERNAL METHODS

=head2 _hstore_representation()

Return value: $HSTORE_STRING

Will return a HSTORE representation of the object_attributes, for saving into the database.

=cut

sub _hstore_representation {
    my $self = shift;

    return {
        map {
            encode_utf8($_->name) => ($_->index_value ? encode_utf8($_->index_value) : undef)
        } grep {
            !$_->dynamic_class
        } @{ $self->object_attributes }
    };
}

=head2 inflate_result

Return value: $result

Will apply roles to this Row object according to the object_class.

See L<Zaaksysteem::Backend::Object::Constants> for a list of available
extensions/object types.

=over

=item * case

Will load role L<Zaaksysteem::Backend::Object::Data::Roles::Case> when
object_class is C<case>

=item * saved_search

Will load role L<Zaaksysteem::Backend::Object::Data::Roles::SavedSearch> when
object_class is C<saved_search>

=back

=cut

sub inflate_result {
    my $self                        = shift;
    my $row                         = $self->next::method(@_);

    return $row unless $row->object_class;

    if (my $role = OBJECT_TYPES->{ $row->object_class }) {
        apply_all_roles($row, $role);
    }

    return $row;
};

=head2 update

Return value: L<DBIx:Class::Row>::update

This method will update the row in the database, and makes sure the hstore_index keeps up to date.

=cut

sub update {
    my $self                        = shift;

    if (@_ && UNIVERSAL::isa($_[0], 'HASH') && $_[0]->{properties}) {
        $self->properties($_[0]->{properties});
    }

    return $self->next::method(@_);
};

=head2 insert

Return value: L<DBIx:Class::Row>::insert

This method will insert the row in the database, and makes sure the
hstore_index keeps up to date.

=cut

sub insert {
    my $self                        = shift;

    my $row                         = $self->next::method(@_);

    if (my $role = OBJECT_TYPES->{ $row->object_class }) {
        apply_all_roles($row, $role);
    }

    $row->reload_index;
    $row->update;

    return $row;
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

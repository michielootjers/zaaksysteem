package Zaaksysteem::Backend::Object::Relationships::ResultSet;
use Moose;

BEGIN { extends 'DBIx::Class::ResultSet'; }

# Modify ->search({ object_id => FOO }) to match either object1_uuid or
# object2_uuid (which is what the caller expects)
around search => sub {
    my $orig = shift;
    my $self = shift;
    my $search = shift;

    if(exists $search->{"object_id"}) {
        my $object_id = delete $search->{"object_id"};

        $search->{"-or"} = {
            "object1_uuid" => $object_id,
            "object2_uuid" => $object_id,
        };
    }

    $self->$orig($search, @_);
};

1;

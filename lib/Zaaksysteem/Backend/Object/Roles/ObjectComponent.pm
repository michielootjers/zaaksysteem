package Zaaksysteem::Backend::Object::Roles::ObjectComponent;

use Moose::Role;


has '_is_objectcomponent' => (
    'is'        => 'ro',
    'default'   => 1,
);

has 'object_describe' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;

        my $rv                      = {
            class       => 'case',
            description => 'Zaaksysteem.nl case',
            label       => 'case',
            attributes  => []
        };

        if (
            $self->_zql_options->{requested_attributes} && 
            @{ $self->_zql_options->{requested_attributes} }
        ) {
            for my $requested_attribute (@{ $self->_zql_options->{requested_attributes} }) {
                my ($attribute) = grep (
                    { $requested_attribute eq $_->name }
                    @{ $self->object_attributes }
                );

                next unless $attribute;

                push(
                    @{ $rv->{attributes} },
                    $attribute->describe
                );
            }

        } else {
            for my $attribute (@{ $self->object_attributes }) {
                push(
                    @{ $rv->{attributes} },
                    $attribute->describe
                );
            }
        }

        return $rv;
    }
);

=head2 _zql_options

Options set by our ZQL handling, like notifying we would like to describe our object etc.

=cut

has '_zql_options'         => (
    'is'            => 'rw',
    'lazy'          => 1,
    'default'       => sub { {}; },
    'isa'           => 'HashRef'
);

has '_requested_columns'    => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;

        if (
            $self->_zql_options->{requested_attributes} &&
            scalar @{ $self->_zql_options->{requested_attributes} }
        ) {
            return $self->_zql_options->{requested_attributes};
        }

        return [ keys %{ $self->properties->{values} } ];
    },
    'isa'           => 'ArrayRef'  
);

=head2 $row->human_values

isa: ArrayRef

    my $zql     = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case');

    my $row     = $zql->apply_to_resultset($objects)->first;

    print join(',', @{ $row->human_values });
    # Prints: 1, "Testzaaktype"

Returns an ArrayRef with the humanvalues of this row, as requested by the attribute list
given in, e.g, ZQL. See example

=cut


has 'human_values'          => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;
        my @values;

        for my $col (@{ $self->_requested_columns }) {
            push(@values, $self->properties->{values}->{$col}->{human_value});
        }

        return \@values;
    },
    'isa'           => 'ArrayRef'  
);

=head2 $row->csv_data

isa: ArrayRef

    my $zql     = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case');

    my $row     = $zql->apply_to_resultset($objects)->first;

    print join(',', @{ $row->csv_data });
    # Prints: 1, "Testzaaktype"

Returns an ArrayRef with the csv values (alias for humanvalues) of this row, as requested by the attribute list
given in, e.g, ZQL. See example

=cut

has 'csv_data'              => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;

        my @data;
        for my $column (@{ $self->csv_header }) {
            push(@data, $self->properties->{values}->{$column}->{human_value});
        }

        return \@data;
    },
    'isa'           => 'ArrayRef'  
);


=head2 $row->csv_header

isa: ArrayRef

    my $zql     = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case');

    my $row     = $zql->apply_to_resultset($objects)->first;

    print join(',', @{ $row->csv_header });
    # Prints: case.id,zaaktype.titel

Returns an ArrayRef with the csv header of this row, could be an alias for whatever you would
call the listing in an SQL statement before the WHERE keyword, e.g. C<< SELECT case.id, zaaktype.titel >>

=cut

has 'csv_header'             => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;

        if (
            $self->_zql_options->{requested_attributes} &&
            scalar @{ $self->_zql_options->{requested_attributes} }
        ) {
            return $self->_requested_columns
        } else {
            return [qw/object.uuid object.id object.data_created object.date_modified/];
        }
    },
    'isa'           => 'ArrayRef'  
);


sub _initialize_zql {
    my $self                    = shift;
    $self->_zql_options(shift);
}

sub TO_JSON {
    my $self                    = shift;

    my $rv                      = {
        object_type         => $self->object_class,
        id                  => $self->id,
        object_id           => $self->object_id,
        values              => {},
    };

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub {shift->iso8601};

    for my $col (@{ $self->_requested_columns }) {
        if (exists $self->properties->{values}{$col}{dynamic_class}) {
            $rv->{values}->{$col} = $self->get_object_attribute($col)->value;
        }
        else {
            $rv->{values}->{$col} = $self->properties->{values}{$col}{value};
        }
    };

    if ($self->_zql_options->{describe_rows}) {
        $rv->{describe} = $self->object_describe;
    }

    if($self->_zql_options->{ include_row_actions }) {
        $rv->{ actions } = $self->object_actions;
    }

    # $rv->{complete}             = $self->next::method(@_) if $self->next::can;

    return $rv;
}

1;

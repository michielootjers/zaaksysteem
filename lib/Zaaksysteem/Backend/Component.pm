package Zaaksysteem::Backend::Component;

use Moose;

extends 'DBIx::Class::Row';

with 'Zaaksysteem::Backend::Component::Searchable';

=head1 NAME

Zaaksysteem::Backend::Component - Generic Component for _all_ schema files

=head1 SYNOPSIS

 $json->_json_data({})      # Attr: Contains all json_data for this row
 $json->TO_JSON()           # Returns _json_data with other data defined in Roles

=head1 DESCRIPTION

These Component methods provides some generic methods as an extension
of L<DBIx::Class>.

=head1 ATTRIBUTES

=head2 _json_data [optional]

ISA: HashRef

=cut

has '_json_data'    => (
    is          => 'rw',
    isa         => 'HashRef',
    default     => sub { return {}; },
    lazy        => 1,
);

=head1 METHODS

=head2 $self->TO_JSON

Return value: $HashRef

This call depends on a set attribute: C<< $rs->_json_data >>. When set, it will
return all data in _json_data. When C<< $rs->_json_data >> contains an empty
HashRef, it will try to use the TO_JSON functionality from L<DBIx::Class>

=cut

sub TO_JSON {
    my $self        = shift;

    unless (DateTime->can('TO_JSON')) {
        no strict 'refs';
        *DateTime::TO_JSON = sub {shift->iso8601};

        use strict;
    }

    return $self->next::method(@_) unless (
        ref $self->_json_data eq 'HASH' &&
        scalar(keys %{ $self->_json_data }) > 0
    );

    return $self->_json_data;
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

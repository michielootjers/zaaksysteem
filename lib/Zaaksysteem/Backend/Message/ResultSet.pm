package Zaaksysteem::Backend::Message::ResultSet;

use Moose;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::ResultSet';

=head2 message_create

Add a message entry to the database.

=head3 Arguments

=over

=item case_id [required]

=item event_type [required]

=item message [required]

=item subject_id [optional]

=back

=head3 Returns

A newly created Sysin::Message object.

=cut


Params::Profile->register_profile(
    method  => 'message_create',
    profile => {
        required => [qw/
        	case_id
        	event_type
        	message
        /],
        optional => [qw/
        	subject_id
        /]
    }
);

sub message_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    my $log = $self->result_source->schema->resultset('Logging')->trigger(
    	$opts->{event_type}, {
    	    component => 'zaak',
    	    zaak_id   => $opts->{case_id},
            data => {
                case_id => $opts->{case_id},
                content => $opts->{message}
            }
        }
    );

    return $self->create({
    	message => $opts->{message},
    	subject_id => $opts->{subject_id},
    	logging_id => $log->id
	});
}

1;

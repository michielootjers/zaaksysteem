package Zaaksysteem::Backend::Message::Component;

use strict;
use warnings;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

sub TO_JSON {
	my $self = shift;

	my %options;
	$options{message}    = $self->message;
    $options{subject_id} = $self->subject_id;

    # Tad ugly to do this here, but the alternative is putting this very specific code
    # in the TO_JSON for Logging. (Which is probably worse.)
    if ($self->logging->zaak_id) {
        $options{aanvrager} = $self->logging->zaak_id->aanvrager_object->display_name;
        $options{case_type} = $self->logging->zaak_id->zaaktype_node->titel;
    }

    return {
        %options,
        %{ $self->next::method() },
    }
}

=head2 update_properties

Updates a message with given properties.

=head3 Arguments

=over

=item subject_id [required]

=item is_read [optional]

=back

=head3 Returns

An updated Sysin::Message object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
        	subject_id
        /],
        optional => [qw/
        	is_read
        /]
    }
);

sub update_properties {
	my $self = shift;
    my $opts = assert_profile($_[0])->valid;

    # Make sure only the owner of the case can make changes to this message
    if ($opts->{subject_id} ne $self->subject_id) {
    	return;
    }
	return $self->update($opts);
}

1;
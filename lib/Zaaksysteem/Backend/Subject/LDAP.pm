package Zaaksysteem::Backend::Subject::LDAP;

use Moose::Role;

use constant LDAP_ATTRIBUTE_LIST => [qw/
    sn
    displayName
    givenName
    initials
    mail
    telephoneNumber
    dn
    cn
/];

has 'ex_id'         => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub { shift->id; }
);

has 'uidnumber'     => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub { shift->id; }
);


has 'naam'         => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub { shift->displayname; }
);

has 'json_property_list' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return [
            map {
                lc($_)
            } @{ LDAP_ATTRIBUTE_LIST() }
        ]
    }
);

sub update_properties_from_ldap {
    my $self            = shift;
    my $ldap_user       = shift;
    my $params          = shift || {};

    my %changes;

    for my $property (@{ LDAP_ATTRIBUTE_LIST() }) {
        my $lc_property = lc($property);

        if ($property eq 'dn') {
            $self->$lc_property($ldap_user->dn);

            $changes{$lc_property} = {
                old         => undef,
                new         => $ldap_user->$property
            } if $params->{created};
        } else {
            # Always changes when created
            $changes{$lc_property} = {
                old         => undef,
                new         => $ldap_user->get_value($property)
            } if $params->{created};

            if ($self->is_attribute_changed(
                    $self->$lc_property,
                    $ldap_user->get_value($property)
                )
            ) {
                ### Already set above
                unless ($params->{created}) {
                    $changes{$lc_property} = {
                        old         => $self->$lc_property,
                        new         => $ldap_user->get_value($property)
                    };
                }

                $self->$lc_property($ldap_user->get_value($property));
            }
        }
    }

    if ($params->{interface_id} && scalar(keys %changes)) {
        $changes{_subject_id}   = $self->id;
        $changes{_create}       = 1 if $params->{created};

        my $interface = $self->result_source->schema->resultset('Interface')->find(
            $params->{interface_id}
        );

        $interface->process_trigger('register_changes', { changes => \%changes });
    }

    $self->update;
}

sub is_attribute_changed {
    my ($self, $attribute1, $attribute2)            = @_;

    return if (!defined($attribute1) && !defined($attribute2));

    return 1 if (!defined($attribute1) && defined($attribute2));
    return 1 if (defined($attribute1) && !defined($attribute2));

    return 1 if ($attribute1 ne $attribute2);

    return;
}

1;

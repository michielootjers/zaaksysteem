package Zaaksysteem::Backend::Subject::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::Constants;

use Zaaksysteem::Backend::Sysin::Modules;

extends 'Zaaksysteem::Backend::ResultSet';

sub search_active {
    my $self        = shift;

    my $active      = $self->search(
        {
            'user_entities.date_deleted'    => undef,
        },
        {
            join => 'user_entities'
        }
    );

    return $active->search(@_);
}

=head2 $subject_resultset->create_from_ldap(\%params)

Return value: $SUBJECT_ROW

    $subject         = $c->model('DB::Subject')->create_from_ldap(
        {
            interface_id        => $interface_id,
            ldap_user           => $ldap_user           # Net::LDAP entry
        }
    );

Creates a user from given Net::LDAP::Row(PosixAccount) user into tables
user_entity and subject.

Will throw an error when user has already been created. When you are not sure,
please use C<update_or_create_from_ldap>, which will update an existing ldap_user
when it exists under the username, and creates it when it isn't.

=cut

define_profile create_from_ldap => (
    required => [qw[ldap_user interface_id]],
);

sub create_from_ldap {
    my $self            = shift;
    my $params          = assert_profile(
        shift || {}
    )->valid;

    my $ldap_user       = $params->{ldap_user};
    my $interface_id    = $params->{interface_id};

    throw(
        'subject/create_from_ldap/exists',
        'Error creating subject, entity already exists in database'
    ) if $self->result_source->schema->resultset('UserEntity')->search(
        {
            source_interface_id         => $interface_id,
            source_identifier           => $ldap_user->get_value('cn')
        }
    )->count;

    my $subject         = $self->create(
        {
            subject_type        => SUBJECT_TYPE_EMPLOYEE,
            username            => $ldap_user->get_value('cn'),
        }
    );

    my $user_entity     = $subject->user_entities->create({
        source_interface_id     => $interface_id,
        source_identifier       => $ldap_user->get_value('cn'),
        active                  => 1
    });

    $subject->update_properties_from_ldap(
        $ldap_user, 
        {
            interface_id        => $interface_id,
            created             => 1,
        }
    );
    $subject->login_entity($user_entity);

    return $subject;
}

=head2 $subject_resultset->update_or_create_from_ldap(\%params)

Return value: $SUBJECT_ROW

    $subject         = $c->model('DB::Subject')->update_or_create_from_ldap(
        {
            interface_id        => $interface_id,
            ldap_user           => $ldap_user           # Net::LDAP entry
        }
    );

Updates or creates a user from given Net::LDAP::Row(PosixAccount) user into tables
user_entity and subject.

=cut

define_profile update_or_create_from_ldap => (
    required => [qw[ldap_user interface_id]],
);

sub update_or_create_from_ldap {
    my $self            = shift;
    my $params          = assert_profile(
        shift || {}
    )->valid;

    my $ldap_user       = $params->{ldap_user};
    my $interface_id    = $params->{interface_id};

    my $user_entity     = $self
                        ->result_source
                        ->schema
                        ->resultset('UserEntity')
                        ->search(
                            {
                                'me.source_interface_id'    => $interface_id,
                                'me.source_identifier'      => $ldap_user->get_value('cn')
                            },
                            {
                                join        => 'subject_id'
                            }
                        )->first;

    if ($user_entity) {
        unless ($user_entity->active && !$user_entity->date_deleted) {
            $user_entity->active(1);
            $user_entity->date_deleted(undef);
            $user_entity->update;
        }

        my $subject             = $user_entity->subject_id;

        $subject->update_properties_from_ldap(
            $ldap_user, 
            {
                interface_id        => $interface_id,
            }
        );

        return $subject;
    }

    return $self->create_from_ldap($params);
}

1;

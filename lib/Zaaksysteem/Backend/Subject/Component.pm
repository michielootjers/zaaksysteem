package Zaaksysteem::Backend::Subject::Component;

use Moose;
use Moose::Util qw/apply_all_roles/;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;
use Data::Dumper;

BEGIN { extends qw/DBIx::Class Catalyst::Authentication::User/; }

use constant SUBJECT_TYPE_TO_ROLE => {
    'employee'      => ['Zaaksysteem::Backend::Subject::LDAP'],
};

use constant DEPRECATED_IDENTIFIER_MAPPING => {
    employee => 'medewerker',
};

with qw/
    Zaaksysteem::JSON::DbAccessors
/;

has 'login_entity'  => (
    'is'        => 'rw',
);

has 'is_active' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self            = shift;

        return $self->user_entities->search(
            {
                date_deleted    => undef,
            }
        )->count ? 1 : undef;
    }
);

has 'betrokkene_identifier' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self            = shift;

        return sprintf "betrokkene-%s-%d", (
            DEPRECATED_IDENTIFIER_MAPPING->{$self->subject_type},
            $self->id,
        );
    }
);

sub supported_features {
    return {
        session => 1
    };
}

sub TO_JSON {
    my $self        = shift;

    my $json        = {
        id                  => $self->id,
    };

    if ($self->login_entity) {
        $json->{login_entity_id}        = $self->login_entity->id
    }
    
    return $json;
}

sub _apply_subject_roles {
    my $self        = shift;

    if (SUBJECT_TYPE_TO_ROLE->{ $self->subject_type}) {
        apply_all_roles($self, @{ SUBJECT_TYPE_TO_ROLE->{ $self->subject_type} });
    }

}

=head2 new() AND inflate_result()

Added hook for creating custom accessors for the json data, for more info, see
L<DBIx::Class::Row>

=cut

sub new {
    my $class   = shift;

    my $self    = $class->next::method(@_);

    ### Apply roles depending on subject_types
    $self->_apply_subject_roles;

    $self->_generate_jsonaccessors;

    return $self;
}

sub inflate_result {
    my $class   = shift;

    my $self    = $class->next::method(@_);

    $self->_apply_subject_roles;

    $self->_generate_jsonaccessors;

    return $self;
}


sub modify_setting {
    my ($self, $key, $value) = @_;

    my $settings = $self->settings;

    $settings->{$key} = $value;

    $self->settings($settings);
    $self->update;
}


=head2 remove_inavigator_settings

settings are stored per casetype. this clears out settings for a specific casetype

=cut

sub remove_inavigator_settings {
    my ($self, $code) = @_;

    throw('subject/remove_inavigator_settings/missing_code',
        'Zaaktypecode is verplicht') unless $code;

    my $current = $self->settings->{inavigator_settings} || {};

    map { delete $current->{$_}->{$code} } keys %$current;

    $self->modify_setting('inavigator_settings', $current);
}

1;

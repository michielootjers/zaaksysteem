package Zaaksysteem::Backend::Auth::UserEntity::Component;

use Moose;

use JSON;

BEGIN { extends 'DBIx::Class' }

sub TO_JSON {
    my $self = shift;
    
    return {
        id => $self->id,
        source_interface_id => $self->get_column('source_interface_id'),
        source_identifier => $self->source_identifier,
        username => $self->username
    };
}

1;

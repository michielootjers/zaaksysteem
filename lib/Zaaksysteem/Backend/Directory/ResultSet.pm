package Zaaksysteem::Backend::Directory::ResultSet;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::Directory::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Directory::ResultSet::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Directory::ResultSet::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Directory::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Directory::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

=head2 directory_create

Create a Directory entry in the database.

=head3 Arguments

=over

=item name [required]

The name of the directory.

=item case_id [required]

The case this directory belongs to

=back

=head3 Returns

The newly created Directory object.

=cut

Params::Profile->register_profile(
    method  => 'directory_create',
    profile => {
        required => [qw/
        	name
        	case_id
        /],
        optional => [qw/
            original_name
        /],
        constraint_methods => {
            name => qr/\w+/,
            case_id => qr/^\d+$/,
        },
    }
);

sub directory_create {
	my ($self, $opts) = @_;

    my $valid = Params::Profile->check(params  => $opts);

    if (!$valid->success) {
        throw_parameter_exception(
            code  => '/directory/directory_create/invalid_parameters',
            error => 'directory_create: Invalid options given',
        );
    }

    # Check if a directory with this name and case already exists
    if ($self->find($opts)) {
        throw_general_exception(
            code  => '/directory/directory_create/directory_exists',
            error => sprintf('Found existing entry with id %d and name %s',
                ($opts->{case_id}, $opts->{name})
            ),
        );
    }

    unless ($self->validate_name($opts->{name})) {
        throw_parameter_exception(
            code  => '/directory/directory_create/invalid_parameters',
            error => 'directory_create: Invalid options given',
        );   
    }

	return $self->create({
        case_id         => $opts->{case_id},
        original_name   => $opts->{original_name} || $opts->{name},
        name            => $opts->{name}
    });
}


=head2 make_valid_name

Check that the directory name could be used on the most important file systems.
Sources: 
http://support.grouplogic.com/?p=1607
http://en.wikipedia.org/wiki/Comparison_of_file_systems#Limits

Approach is positive discrimination, we will only allow whatever
will supposedly work everywhere. It is quite laborious to find out exactly what works
on different filesystems - we would need a concise and 
authorative guide, OS documentation, and the opportunity to test.
This seems too much for this feature. The point here is that the definition
is sufficiently broad to cause very little irritation yet be secure and useful.

Anything that doesn't fit our guideline for directory names will be replaced with 
an underscore.

The next step is to look for an existing directory with that name within the case. 
If there's a match, look for (number) at the end. 
If found, increase the number at the end, repeat until we have a unique name.

A directory name is valid if this routine returns the same name as you pass in.

In addition, this needs to happen within the create method in a locked table,
because some other process may be inserting the same name. Therefore, uniqify
will test uniqueness by inserting. If inserting is succesful - we're good to
go.

=cut

sub make_valid_name {
    my ($self, $name) = @_;

    die "need something of substance" unless $name =~ m|\w|is;

    my $validName = $name;

    # reserved names in Windows. add an underscore to fix.
    my @invalidWindowsNames = qw/com1 com2 com3 com4 com5 com6 com7 com8 com9 lpt1 lpt2 lpt3 lpt4 lpt5 lpt6 lpt7 lpt8 lpt9 con nul prn/;
    if (grep {$_ eq $validName} @invalidWindowsNames) {
        $validName .= "_";
    }

    if (length $validName > 245) {
        $validName = substr($validName, 0, 245) . '...';
    }

    # Mac OS X allows no dot at the beginning
    $validName =~ s|^\.|_|;

    # Cross file system legal characters only
    $validName =~ s/[^A-Za-z0-9\!\@\#\$\%\&\.\,\;\(\)\{\}\[\]\=\ \_]//g;

    return $validName;
}

#
# a valid name will not be changed by the make_valid_name. We could split this
# into two routines, but then the regexps would live on two places and we don't
# like split personalities. 
# 
sub validate_name {
    my ($self, $name) = @_;

    return $self->make_valid_name($name) eq $name;
}

sub validify {
    my ($self, $arguments) = @_;

    my $case_id = $arguments->{case_id} or die "need case_id";
    my $name    = $arguments->{name} or die "need name";

    my $valid_name = $self->make_valid_name($name);

    return $self->uniqify($case_id, $valid_name);
}


=head2 uniqify

if the name is unique for the case, return it.

if a match is found, append a number on the end: 

test (1)
test (2)
test (...)

until it is not found, in which case were done.

=cut

sub uniqify {
    my ($self, $case_id, $name) = @_;

    return $name unless $self->find({case_id => $case_id, name => $name});

    my ($body, undef, $index) = $name =~ m|^(.*?)(\s+\((\d+)\))?$|is;

    $index ||= 0;

    my $new_candidate = $body . " (" . ($index+1) . ")";

    return $self->uniqify($case_id, $new_candidate);
}

1;

package Zaaksysteem::Backend::Tools::DottedPath;

use strict;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;
use Data::Dumper;

use Exporter 'import';
our @EXPORT_OK = qw/get_by_dotted_path set_by_dotted_path/;


=head2 dotted_path

given a hashref structure, traverse using a dotted address.
e.g. node.properties.test will amount to $hashref->{node}->{properties}->{test}

returns the value at the given address. if value given, first sets that value.
jpath($hashref, 'node.properties.test', '123') => 123

=cut

define_profile get_by_dotted_path => (
    required => [qw/target path/]
);

sub get_by_dotted_path {
    my $arguments = assert_profile(shift)->valid;

    my $target = $arguments->{target};
    my $path   = $arguments->{path};

    return $target->{$path} unless $path =~ m|\.|;

    my ($step, $remaining) = $path =~ m|(.*?)\.(.*)|;

    die('dottedpath/get_by_dotted_path', 'non-existing path requested')
        if $remaining && ref $target->{$step} ne 'HASH';

    return get_by_dotted_path({
        target => $target->{$step},
        path => $remaining
    });
}


define_profile set_by_dotted_path => (
    required => [qw/target path/],
    optional => [qw/value/]
);

sub set_by_dotted_path {
    my $arguments = assert_profile(shift)->valid;

    my $target = $arguments->{target};
    my $path   = $arguments->{path};
    my $value  = $arguments->{value};

    if ($path =~ m|\.|) {
        my ($step, $remaining) = $path =~ m|(.*?)\.(.*)|;

        # auto vivify like perl does
        set_by_dotted_path({
            target => $target->{$step} ||= {},
            path => $remaining,
            value => $value
        });
    } else {
        $target->{$path} = $value;
    }
}

1;
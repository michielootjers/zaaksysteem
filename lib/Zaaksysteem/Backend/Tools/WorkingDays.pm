package Zaaksysteem::Backend::Tools::WorkingDays;

use strict;
use Data::Dumper;
use DateTime;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Exporter 'import';
our @EXPORT_OK = qw/add_working_days/;


my @PASEN = qw/
    2013-04-01
    2014-04-21
    2015-04-06
    2016-03-28
    2017-04-17
    2018-04-02
    2019-04-22
    2020-04-13
    2021-04-05
    2022-04-18
    2023-04-10
    2024-04-01
    2025-04-21
    2025-04-06
    2027-03-29
    2028-04-17
    2029-04-02
    2030-04-22
/;

my @PINKSTEREN = qw/
    2013-05-20
    2014-06-09
    2015-05-25
    2016-05-16
    2017-06-05
    2018-05-21
    2019-06-10
    2020-06-01
    2021-05-24
    2022-06-06
    2023-05-29
    2024-05-20
    2025-06-09
    2025-05-25
    2027-05-17
    2028-06-05
    2029-05-21
    2030-06-10
/;

my @HEMELVAART = qw/
    2013-05-09
    2014-05-29
    2015-05-14
    2016-05-05
    2017-05-25
    2018-05-10
    2019-05-30
    2020-05-21
    2021-05-13
    2022-05-26
    2023-05-18
    2024-05-09
    2025-05-29
    2025-05-14
    2027-05-06
    2028-05-25
    2029-05-10
    2030-05-30
/;

my $DYNAMIC_HOLIDAYS = {map {$_ => 1} (@PASEN, @PINKSTEREN, @HEMELVAART)};

my $FIXED_HOLIDAYS = {
    '12-25' => 'Eerste Kerstdag',
    '12-26' => 'Tweede Kerstdag',
    '1-1'   => 'Nieuwjaar',
    '4-27'  => 'Koningsdag',
    '5-5'   => 'Bevrijdingsdag'
};


=head2 add_working_days

Calculate the next working day given a start date and interval.
E.g. it 23 november 2014 now, and in 30 working days the case
must be closed - which date is the deadline?

=cut

define_profile add_working_days => (
    required => [qw[datetime working_days]],
    constraint_methods => {
        working_days => qr/^\-?\d+$/
    }
);

sub add_working_days {
    my $arguments = assert_profile(shift)->valid;

    my $next_day     = $arguments->{datetime}->clone;
    my $working_days = $arguments->{working_days};

    my $offset       = $working_days > 0 ? 1 : -1;

    while ($working_days != 0) {

        do { $next_day = $next_day->add(days => $offset) }
            while is_dutch_holiday($next_day);

        $working_days -= $offset;
    }

    return $next_day;
}


sub is_dutch_holiday {
    my ($datetime) = @_;

    # day_of_week:
    # Returns the day of the week as a number, from 1..7, 
    # with 1 being Monday and 7 being Sunday.
    return 
        $datetime->day_of_week > 5 ||
        exists $FIXED_HOLIDAYS->{$datetime->month . '-' . $datetime->day} ||
        exists $DYNAMIC_HOLIDAYS->{$datetime->strftime('%F')};
}


1;
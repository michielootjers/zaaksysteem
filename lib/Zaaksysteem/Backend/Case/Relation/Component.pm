package Zaaksysteem::Backend::Case::Relation::Component;

use Moose;

BEGIN { extends 'DBIx::Class::Row'; }

sub TO_JSON {
    my $self = shift;

    return {
        left => $self->get_column('case_id_a'),
        right => $self->get_column('case_id_b'),
        seq_left => $self->order_seq_a,
        seq_right => $self->order_seq_b
    };
}

=head2 to_string

Returns a string representation of the relation, in the format C<[I<case_id_a> E<lt>-E<gt> I<case_id_b>]>.

=cut

sub to_string {
    my $self = shift;

    return sprintf(
        '[%d <-> %d]',
        $self->get_column('case_id_a'),
        $self->get_column('case_id_b')
    );
}

=head2 cases

Returns a list of case objects in this relation.

=cut

sub cases {
    my $self = shift;

    return $self->case_id_a, $self->case_id_b;
}

=head2 view_in_context

Returns a L<Zaaksysteem::Zaken::RelationView> in the context of a case. This means operations on the relation will be done B<ON THE OTHER CASE ID IN THE RELATION>.

=head3 Parameters

=over 4

=item case_id

The I<case_id> of the case to base the context of the view on

=back

=cut

sub view_in_context {
    Zaaksysteem::Zaken::RelationView->new(shift, shift);
}

=head2 delete

Wrapper around DBIx::Class::Row's delete method, inject events in the logging table automagically.

=cut

after delete => sub {
    my $self = shift;

    for my $case ($self->cases) {
        $case->logging->trigger('case/update/relation', { component => 'zaak', data => {
            case_id => $case->id,
            relation_id => $self->view_in_context($case->id)->case_id,
            deleted => 1
        }});
    }
};

1;

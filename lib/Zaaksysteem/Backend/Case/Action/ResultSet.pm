package Zaaksysteem::Backend::Case::Action::ResultSet;

use Moose;
use Zaaksysteem::Exception;

# TODO: Improve documentation and move to Zaaksysteem::Profile

BEGIN { extends 'DBIx::Class::ResultSet'; }

sub type {
    return shift->search({ type => shift });
}

sub milestone {
    return shift->search(
        { 'casetype_status_id.status' => shift },
        { join => 'casetype_status_id' }
    );
}

sub current {
    return shift->search(
        { 'casetype_status_id.status' => { '=' => \'case_id.milestone' } },
        { join => [ 'casetype_status_id', 'case_id' ] }
    );
}

sub active {
    return shift->search({ automatic => 1 });
}

sub sorted {
    my ($self) = @_;

    return $self->search({}, { order_by => { -asc => 'me.id' }, prefetch => ['casetype_status_id']});
}


sub create_from_case {
    my ($self, $case) = @_;

    map { $self->create_from_phase($case, $_) } $case->zaaktype_node_id->zaaktype_statuses;
    $case->case_actions->sorted->apply_rules({case => $case});
}


=head2 create_from_phase

Given a specifc phase, create action records for every automatic action
that needs to be performed. These can include emails, template, changing
the allocations and creating a subcase (relatie).

=cut

sub create_from_phase {
    my ($self, $case, $phase) = @_;

    my @actions;

    push(@actions, map { $self->create_from_zaaktype_relatie($case, $phase, $_) } $phase->zaaktype_relaties->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    push(@actions, map { $self->create_from_template($case, $phase, $_) } $phase->zaaktype_sjablonens->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    push(@actions, map { $self->create_from_email($case, $phase, $_) } $phase->zaaktype_notificaties->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    # allocation has to happen after templates and emails so that previous assignee (behandelaar)
    # is still available for use in magic strings.
    unless ($phase->is_last) {
        push(
            @actions,
            $self->create({
                    case_id            => $case->id,
                    casetype_status_id => $phase->id,
                    type               => 'allocation',
                    label     => $phase->ou_id . ', ' . $phase->role_id,
                    automatic => $phase->role_set,
                    data      => {
                        description      => 'Toewijzing',
                        ou_id            => $phase->ou_id,
                        role_id          => $phase->role_id,
                        original_ou_id   => $phase->ou_id,
                        original_role_id => $phase->role_id,
                        role_set         => $phase->role_set,
                    }
                }
            )
        );
    }

    return @actions;
}

# subcase is much sweeter, however currently a subcase is an instance
# and zaaktype_relatie is part of the case type, so i'm avoiding
# future confusion.
sub create_from_zaaktype_relatie {
    my ($self, $case, $phase, $zaaktype_relatie) = @_;

    my $rzi = $zaaktype_relatie->relatie_zaaktype_id;
    my $label = $rzi ? $rzi->zaaktype_node_id->titel : $zaaktype_relatie->zaaktype_node_id->titel;

    return $self->create({
            case_id            => $case->id,
            casetype_status_id => $phase->id,
            type               => 'case',
            label              => $label,
            automatic          => $zaaktype_relatie->status || 0,
            data               => {$zaaktype_relatie->get_columns}
    });
}

sub create_from_email {
    my $self  = shift;
    my $case  = shift;
    my $phase = shift;
    my $email = shift;

    return $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'email',
        label => $email->bibliotheek_notificaties_id->label,
        automatic => !$email->intern_block && $email->automatic ? 1 : 0,
        data => {
            rcpt => $email->rcpt,
            subject => $email->bibliotheek_notificaties_id->subject,
            body => $email->bibliotheek_notificaties_id->message,
            email => $email->email,
            description => 'E-mail',
            behandelaar => $email->behandelaar,
            intern_block => $email->intern_block,
            case_document_attachments => $email->case_document_attachments,
            automatic_phase => $email->automatic, #needed when rules disapply
            bibliotheek_notificaties_id => $email->get_column('bibliotheek_notificaties_id')
        }
    });
}


sub create_from_template {
    my ($self, $case, $phase, $template) = @_;

    if (!$template->bibliotheek_sjablonen_id->filestore_id) {
        throw("ZS/B/C/A/RS/create_from_template", "No filestore found!");
    }

    return $self->create({
        case_id            => $case->id,
        casetype_status_id => $phase->id,
        type               => 'template',
        label              => $template->bibliotheek_sjablonen_id->naam,
        automatic          => $template->automatisch_genereren,
        data               => {
            description => 'Sjabloon',
            bibliotheek_sjablonen_id => $template->get_column('bibliotheek_sjablonen_id'),
            bibliotheek_kenmerken_id => $template->get_column('bibliotheek_kenmerken_id'),
            filename              => $template->bibliotheek_sjablonen_id->filestore_id->original_name,
            target_format         => $template->target_format || 'odt',
            automatisch_genereren => $template->automatisch_genereren,
        }
    });
}

sub apply_rules {
    my ($self, $options) = @_;

    my $case = $options->{ case } or die "need case";
    my $milestone = $options->{ milestone } || ($case->milestone + 1);

    my %rule_templates_lookup;
    my %rule_cases_lookup;
    my %scheduled_mails;
    my %toewijzing;
    my %action_counter;

    return $self->reset if $case->is_afgehandeld;

    # We're only going to apply rules to actions in the current milestone, thats the only one that matters
    my $rules_result = $case->execute_rules({ status => $milestone });

    # Build lookup tables
    if(exists $rules_result->{ templates }) {
        %rule_templates_lookup = map { $_ => 1 } @{ $rules_result->{ templates } };
    }

    if(exists $rules_result->{ cases }) {
        %rule_cases_lookup = map { $_ => 1 } @{ $rules_result->{ cases } };
    }

    while(my $action = $self->next()) {
        # Yeah, the counter state is a bit complex, but let me explain!
        # Since rules are applied to email notifications by their ordering, we need to check the position of the action
        # But we also need to track the phase it's in, otherwise we just increment the count for stati in the first phase
        # And that gives screwy results.
        my $counter = ++$action_counter{ sprintf('%s-%d', $action->type, $action->casetype_status_id->status) };

        # Ignore tainted actons, and actions in a different milestone.
        next if($action->tainted || $action->casetype_status_id->status != ($milestone));

        if($action->type eq 'template') {

            # first look if a rule wants to enable. if not, look at the static casetype setting and use that
            my $desired_value = exists $rule_templates_lookup{$counter} || $action->data->{automatisch_genereren};

            $action->set_automatic({
                automatic => $desired_value
            });

        } elsif($action->type eq 'case') {  # yes, identical to template.

            my $desired_value = exists $rule_cases_lookup{$counter} || $action->data->{status};

            $action->set_automatic({
                automatic => $desired_value
            });

        } elsif($action->type eq 'email') {

            if(exists $rules_result->{schedule_mail}->{ $counter }) {
                unless($action->automatic) {
                    $action->automatic(1);
                    $action->update;
                }

                my $send_date = $rules_result->{schedule_mail}->{$counter}->{send_date};
                if($send_date && $action->data->{send_date} ne $send_date) {
                    my $data = $action->data;
                    $data->{send_date} = $send_date->iso8601();
                    $action->data($data);
                    $action->update;
                }
            } else {
                # fall back to default behaviour. don't uncheck when
                unless($action->data->{automatic_phase}) {
                    if($action->automatic) {
                        $action->automatic(0);
                        $action->update;
                    }
                }
            }
        } elsif($action->type eq 'allocation') {
            $self->update_allocation({
                action      => $action,
                rule_action => $rules_result->{toewijzing}
            });
        }
    }

    $self->reset;
}


=head2 update_allocation

Allocation actions work as follows. From ZTB phase management a hardcoded allocation action
can be set. This is mandatory for the first phase, optional for the middle phases, and unavailable
for the last phase.

Rules can be created to override the phase allocation. Although multiple rules can be created, only the
final one is executed. This allows for conditional overriding.

So:
- Phase allocation (if present)
- Final rule allocation (if present)

Case_action rows are created for every phase but the final one, with the 'automatic' flag determining
if the action is executed at phase advancing.

If rules are present they can override the standard behaviour.
The outcome of rules is influenced by user input. When the user input changes back and disables the rules, the ZTB
settings need to remain available in the action.

=cut

sub update_allocation {
    my ($self, $arguments) = @_;

    my $action      = $arguments->{action}      or die "need action";
    my $rule_action = $arguments->{rule_action}; #can be undef

    my $desired_values;
    my $action_data = $action->data;

    # first decide what we really want
    if ($rule_action) {
        $desired_values = {
            role_id   => $rule_action->{role_id},
            ou_id     => $rule_action->{ou_id},
            automatic => 1
        };
    } else {
        $desired_values = {
            role_id   => $action_data->{original_role_id},
            ou_id     => $action_data->{original_ou_id},
            automatic => $action_data->{role_set}
        };
    }

    # then see if that's different from the actual sitation, in which case, update
    unless (
        $action_data->{role_id} eq $desired_values->{role_id} &&
        $action_data->{ou_id}   eq $desired_values->{ou_id}   &&
        $action->automatic      eq $desired_values->{automatic}
    ) {
        $action_data->{role_id} = $desired_values->{role_id};
        $action_data->{ou_id}   = $desired_values->{ou_id};

        $action->data($action_data);
        $action->automatic($desired_values->{automatic});
        $action->update();
    }
}


1;

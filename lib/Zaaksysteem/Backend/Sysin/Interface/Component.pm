package Zaaksysteem::Backend::Sysin::Interface::Component;

use Moose;

use JSON::Path qw[];

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;
use Zaaksysteem::Backend::Sysin::Modules;

extends 'DBIx::Class';

has 'module_object' => (
    is      => 'rw',
    lazy    => 1,
    default => sub {

        my $module  = Zaaksysteem::Backend::Sysin::Modules->find_module_by_id(
            shift->module
        );

        return $module;
    }
);

=head2 $interface->interface_update

Update the properties of an existing Sysin::Interface.

=head3 Arguments

=over

=item name

=item case_type_id

=item max_retries

Defines the number of automated retries on the transactions belonging to this interface before the process gives up.

=item interface_config

The JSON hash containing all relevant configuration details.

=item multiple

Defines whether or not this interface will always receive a single mutation or multiple mutations.

=item active

Defines whether or not the Interface should be considered as 'in production'.

=back

=head3 Returns

An updated Sysin::Interface::Component object. The object used to call interface_update should also be updated.

=cut

Params::Profile->register_profile(
    method  => 'interface_update',
    profile => {
        missing_optional_valid  => 1,
    	required => [],
        optional => [qw/
        	case_type_id
        	interface_config
        	max_retries
        	multiple
            active
            name
        /],
        constraint_methods => {
        	case_type_id => qr/^\d+$/,
            max_retries  => qr/^\d+$/,
            multiple     => qr/^(0|1){0,1}$/,
            active       => qr/^(0|1){0,1}$/,
            name         => qr/^\w[\w\s]+$/,
            module       => sub {
                my ($dfv, $val)     = @_;

                return 1 if (
                    grep { $_->name eq $val }
                    Zaaksysteem::Backend::Sysin::Modules->list_of_modules
                );

                return;
            },
            case_type_id  => sub {
                my ($dfv, $val)     = @_;
                my ($entry);

                return unless ref($val) || $val =~ /^\d+/;

                my $schema      = $dfv->get_input_data->{schema};

                my $zt_id       = (ref($val) ? $val->id : $val);

                return 1 if (
                    ($entry = $schema->resultset('Zaaktype')->find($zt_id)) &&
                    $entry->active
                    && !$entry->deleted
                );

                return;
            }
        },
        defaults => {
            active      => 0,
            multiple    => 0,
        }
    }
);

sub interface_update {
	my $self        = shift;
    my $opts        = assert_profile(
        {
            %{ shift() },
            schema  => $self->result_source->schema
        }
    )->valid;

    $self->_merge_interface_config($opts);

    $opts           = $self
                    ->result_source
                    ->resultset
                    ->_prepare_options(
                        {
                            %{ $opts },
                            module => $self->module,
                        }
                    );

    return $self->update($opts, @_);
}

=head2 $interface->_merge_interface_config(\%OPTIONS)

Merges the current interface config with the ones given in update, prevents
the interface config from beeing overwritten

B<OPTIONS>

=over 4

=item interface_config

The interface_config to merge with the values already in the database

=back

=cut

sub _merge_interface_config {
    my $self            = shift;
    my $opts            = shift;

    return unless (
        $opts &&
        exists($opts->{interface_config}) &&
        UNIVERSAL::isa($opts->{interface_config}, 'HASH') &&
        $self->interface_config
    );

    my %given_config    = %{ $opts->{interface_config} };

    my $db_json         = $self->interface_config;
    my %db_config       = %{
        $self
            ->result_source
            ->resultset
            ->_decode_interface_config(
                $self->interface_config
            )
    };

    $opts->{interface_config} = { (%db_config, %given_config) };
}

=head2 $interface->interface_update

Delete the interface from the system

=cut

sub interface_delete {
    my $self    = shift;

    $self->update({date_deleted => DateTime->now});
    return 1;
}

=head2 $interface->get_attribute_mapping

Returns a list of attributes, which can be configured, and an instruction how
to fill this attribute list

=cut

sub get_attribute_mapping {
    my $self    = shift;

    return $self->module_object->get_attribute_mapping($self);
}

=head2 $interface->set_attribute_mapping

Sets a list of attributes, which can be configured, and an instruction how
to fill this attribute list

=cut

sub set_attribute_mapping {
    my $self    = shift;
    my $params  = shift;

    return $self->module_object->set_attribute_mapping($self, $params);
}

=head2 $interface->get_trigger_definition($action)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#get_trigger_definition>

=cut

sub get_trigger_definition {
    my ($self, $action)    = @_;

    return $self->module_object->get_trigger_definition($action);
}

=head2 $interface->process_trigger($action, \%params)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#process_trigger>

=cut

sub process_trigger {
    my ($self, $action, $params)    = @_;
    $params                         ||= {};

    return $self->module_object->process_trigger({
        interface   => $self,
        params      => $params,
        action      => $action
    });
}

=head2 $interface->get_interface_form

Convenience method dispatching to attached interface module for generating
the interface form.

=cut

sub get_interface_form {
    my $self    = shift;

    return $self->module_object->generate_interface_form($self);
}

=head2 $interface->process

Return value: $ROW_TRANSACTION

Convenient method to L<Zaaksysteem::Backend::Sysin::Modules#process>, see this
man page for more information.

=cut

sub process {
    my $self        = shift;
    my $options     = shift || {};

    return $self->module_object->process(
        {
            %{ $options },
            interface   => $self,
        }
    );
}

sub run_test {
    my $self        = shift;
    my $test_id     = shift;


    return $self->module_object->run_test(
        $self,
        $test_id
    );
}

=head2 $interface->TO_JSON

=cut

sub TO_JSON {
    my $self                    = shift;
    my $opts                    = shift || {};

    my $values                  = { $self->get_columns };

    ### Decode interface config to perl, so JSON can re-encode it...
    $values->{interface_config} = $self
        ->result_source
        ->resultset
        ->_decode_interface_config($values->{interface_config});

    unless ($opts->{ignore_errors}) {
        $values->{transaction_errors} = $self->transactions->get_column('error_count')->sum;
    }

    return $values;
}

=head2 $interface->get_properties

=cut

sub get_interface_config {
    my $self    = shift;

    $self->result_source
        ->resultset
        ->_decode_interface_config($self->interface_config);
}

sub update_interface_config {
    my $self    = shift;
    my $config  = shift;

    $self->interface_config(
        $self   ->result_source
                ->resultset
                ->_encode_interface_config($config)
    );

    $self->update;

}

sub jpath {
    my $self = shift;

    return JSON::Path->new(shift)->value($self->get_interface_config);
}

1;

package Zaaksysteem::Backend::Sysin::Interface::Roles::InterfaceConfig;

use JSON;
use Moose::Role;
use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Profile;

around '_prepare_options' => sub {
    my $method              = shift;
    my $self                = shift;
    my ($opts)              = @_;

    $self->_load_module_defaults($opts);

    if (ref $opts->{interface_config} eq 'HASH') {
        $opts->{interface_config} =
          $self->_encode_interface_config($opts->{interface_config});
    }

    return $self->$method(@_);
};

=begin private

=head2 _load_module_defaults

Loads the module defaults in case they are not set.

=head3 SYNOPSYS

$self->_load_module_defaults({
    module      => 'name of module',
    max_retries => $integer,
    multiple    => $boolean,
});

=head3 ARGUMENTS

=over

=item module [required]

The name of the module

=item max_retries [optional]

Integer

=item multiple [optional]

Boolean value

=back

=head3 RETURNS

Nothing. But beware, we change the options you supply us.

=cut

sub _load_module_defaults {
    my ($self, $opts) = @_;

    my $module = Zaaksysteem::Backend::Sysin::Modules->find_module_by_id($opts->{module});

    if (!defined $opts->{max_retries}) { 
        $opts->{max_retries} = $module->max_retries;
    }

    if (!defined $opts->{multiple}) { 
        $opts->{multiple} = $module->is_multiple;
    }
}

sub _encode_interface_config {
    my $self        = shift;
    my $properties  = shift;

    return JSON::encode_json($properties);
}

sub _decode_interface_config {
    my $self        = shift;
    my $properties  = shift;

    return JSON::decode_json($properties);
}

1;

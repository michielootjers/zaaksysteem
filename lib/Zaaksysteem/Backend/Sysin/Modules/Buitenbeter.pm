package Zaaksysteem::Backend::Sysin::Modules::Buitenbeter;

use Moose;
use File::Spec::Functions qw(catfile);
use File::Temp;
use List::MoreUtils qw/all/;
use Try::Tiny;
use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;

use Zaaksysteem::Backend::Tools::DottedPath qw/get_by_dotted_path/;
use Zaaksysteem::Constants;
use Zaaksysteem::Constants::Locks qw(BUITENBETER_GET_NEW_MELDINGEN_LOCK);
use Zaaksysteem::Geo;
use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

# 15 minutes
use constant QUARTER => 60 * 15;


# these may be useful for error handling
    # result
    # errorText

use constant BUITENBETER_FIELDS => qw/
    datum
    lattitude
    longitude
    straat
    huisnummer
    plaats
    omschrijving
    probleem
    attachment
    buitenbeter_id
    extern_id
    melder.geslacht
    melder.voornaam
    melder.achternaam
    melder.straat
    melder.huisnummer
    melder.postcode
    melder.plaats
    melder.telefoonnummer
    melder.email
    melder.contactTelefoon
    melder.contactEmail
/;

use constant BUITENBETER_ATTRIBUTES => qw/
    datum
    geo_locatie
    bag_adres
    straat
    huisnummer
    plaats
    omschrijving
    probleem
    buitenbeter_id
    melder.geslacht
    melder.voornaam
    melder.achternaam
    melder.straat
    melder.huisnummer
    melder.postcode
    melder.plaats
    melder.telefoonnummer
    melder.email
    melder.contactTelefoon
    melder.contactEmail
/;

use constant INTERFACE_ID               => 'buitenbeter';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'Url Buitenbeter',
        required    => 1,
        description => 'Buitenbeter SOAP server'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_username',
        type        => 'text',
        label       => 'Gebruikersnaam',
        required    => 1,
        description => 'Gebruikersnaam'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password',
        type        => 'password',
        label       => 'Wachtwoord',
        required    => 1,
        description => 'Wachtwoord'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contracttype',
        type        => 'text',
        label       => 'Contracttype',
        required    => 1,
        description => 'Contracttype'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gemeente_id',
        type        => 'text',
        label       => 'Gemeente ID',
        required    => 1,
        description => 'Gemeente ID'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_interval',
        type        => 'select',
        label       => 'Interval',
        default     => '30',
        description => 'Interval om nieuwe meldingen op te halen',
        required    => 1,
        data        => {
            options     => [
                map { {value => $_, label => $_ . ' minuten'}} qw/15 30 60/
            ]
        }
    ),

];



use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Buitenbeter',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 1,
    has_attributes                  => 1,
    attribute_list                  => [
        map {{
            external_name   => $_,
            attribute_type  => 'magic_string'
        }} (BUITENBETER_ATTRIBUTES)
    ],
    trigger_definition  => {
        GetNewMeldingen => {
            method => 'GetNewMeldingen',
            update => 1
        },
        PostStatusUpdate => {
            method => 'PostStatusUpdate',
            update => 1
        },
        get_test_responses => {
            method => 'get_test_responses',
            update => 1
        },
    },
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'buitenbeter_test_connection',
                description => 'Test verbinding naar profiel URL'
            },
            {
                id          => 2,
                label       => 'Test spoof',
                name        => 'spoof_test',
                method      => 'buitenbeter_test_spoof',
                description => 'Test import meldingen (maakt 2 zaken aan)'
            },
            {
                id          => 3,
                label       => 'Handmatige import',
                name        => 'refresh',
                method      => 'buitenbeter_test_GetNewMeldingen',
                description => 'Haal nieuwe meldingen op van de externe server'
            }
        ],
    },
};

has test_responses => (is => 'rw', default => sub { {} });

has cases => (is => 'rw', default => sub { [] });

has interface => (is => 'rw');

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


sub buitenbeter_test_connection {
    my ($self, $interface) = @_;

    $self->test_host_port($interface->get_interface_config->{endpoint});
}


sub buitenbeter_test_spoof {
    my ($self, $interface) = @_;

    my $test_responses = $interface->process_trigger('get_test_responses');

    return $interface->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => $interface->get_interface_config->{interval} * QUARTER,
        allow_duplicates => 1
    });
}

sub buitenbeter_test_GetNewMeldingen {
    my ($self, $interface) = @_;

    my $cases = $interface->process_trigger('GetNewMeldingen', {
        spoof_time => $interface->get_interface_config->{interval} * QUARTER,
    });

    return @$cases ? $cases : ['Geen nieuwe meldingen beschikbaar'];
}


=head2 GetNewMeldingen

Retrieve the list with new messages from the Buitenbeter server.
If any of these messages is new, create a case from it.
return a list with new cases for testability.

Supports a spoof mode which will work from a locally maintained list.
The purposes is the develop the rest of the flow, and provide content
if the actual server is not reachable or responsing - or it's cleaner
to not burden it with a lot of debug messages.

Parameters:

    spoof_time - don't use system time but use a given time, otherwise the test
                 would fail every unscheduled interval.

=head3 Returns

A list with newly created case objects.

=cut

sub GetNewMeldingen {
    my ($self, $params, $interface) = @_;

    $self->interface($interface);
    my $config = $interface->get_interface_config;

    return [] unless $self->check_schedule({
        time => exists $params->{spoof_time} ? $params->{spoof_time} : time,
        interval_minutes => $config->{interval}
    });

    $self->test_responses(delete $params->{test_responses}) if $params->{test_responses};
    my $schema = $interface->result_source->schema;

    return [] unless $schema->try_lock(BUITENBETER_GET_NEW_MELDINGEN_LOCK);

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => 'xml',
        processor_params => {
            processor => '_process_GetNewMeldingen',
            allow_duplicates => $params->{allow_duplicates}
        },
    });

    # directly post updates. out of transaction because this
    # borks up the transaction stack somewhere.
    foreach my $case_id (@{ $self->cases }) {
        my $case = $interface->result_source->schema->resultset('Zaak')->find($case_id);

        $self->interface->process_trigger('PostStatusUpdate', {
            case_id => $case->id,
            kenmerken => $case->field_values,
            statusText => 'Melding is geaccepteerd in Zaaksysteem',
            statusCode => 'Ontvangen',
            test_responses => $self->test_responses,
        });
    }

    $schema->unlock(BUITENBETER_GET_NEW_MELDINGEN_LOCK);

    return $self->cases;
}


=head2 find_case_with_buitenbeter_id

As a safeguard against a growing database with duplicate melding, before
inserting a new case, see if we already have a case linked to a melding.

We're gonna take a slight shortcut to avoid instantiating case objects and
calling field_values, which are not in an optimized state.

Find all Buitenbeter case ids.
Find out which kenmerk holds the buitenbeter id
Query ZaakKenmerk table for a match.

=cut

sub find_case_with_buitenbeter_id {
    my ($self, $buitenbeter_id) = @_;

    my $bibliotheek_kenmerken_id = $self->get_mapped_bibliotheek_kenmerken_id('buitenbeter_id');

    my $casetype_id = $self->interface->case_type_id->id;
    my $schema = $self->interface->result_source->schema;

    return $schema->resultset('ZaakKenmerk')->search({
        zaak_id => {
            -in => $schema->resultset('Zaak')->search_extended({
                zaaktype_id => $casetype_id,
                deleted => undef
            })->get_column('id')->as_query
        },
        bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
        value => $buitenbeter_id
    })->count;
}


sub _process_GetNewMeldingen {
    my $self            = shift;
    my $record          = shift;

    my $transaction     = $self->process_stash->{transaction};
    my $params          = $transaction->get_processor_params();
    my $interface       = $transaction->interface;

    my $config = $interface->get_interface_config;

    try {
        my $response = $self->callSOAP($interface, 'GetNewMeldingen', {
            gemeente_id  => $config->{gemeente_id},
            contracttype => $config->{contracttype}
        });

        $transaction->input_data($record->input($self->process_stash->{input}));
        $record->output($self->process_stash->{output});

        my (@cases, @errors);
        foreach my $melding (@{$response->{meldingen}{Melding}}) {
            my $melding_id = ref $melding eq 'HASH' ? $melding->{buitenbeter_id} : 0;
            try {
                $self->assert_valid_melding($melding);

                if (  !$self->find_case_with_buitenbeter_id($melding_id)
                    || $params->{allow_duplicates})
                {
                        my $case = $self->create_case($melding);
                        $self->register_mutations($case);
                        push @cases, $case->id;
                }
            }
            catch {
                push(@errors, $melding_id);
                $self->log->error(
                    sprintf(
                        "Unable to create BuitenBeter melding %d as a case: %s",
                        $melding_id, $_
                    )
                );
            };
        }
        my @preview;
        if (@cases) {
            $self->cases(\@cases);
            push(
                @preview,
                sprintf(
                    'Nieuwe melding%s geregistreerd. Zakennr: %s',
                    @cases > 1 ? 'en' : '',
                    join(", ", @cases)
                )
            );
        }
        if (@errors) {
            push(
                @preview,
                sprintf(
                    'Foutieve melding%s externe ID%s: %s',
                    @errors > 1 ? 'en' : '',
                    @errors > 1 ? "'s" : '',
                    join(", ", @errors)
                )
            );
        }

        if (@preview) {
            $record->preview_string(join(". ", @preview));
        }
        else {
            $record->preview_string(
                'Geen nieuwe meldingen via BuitenBeter opgehaald.');
        }
    }
    catch {
        if (eval {$_->isa('Throwable::Error')}) {
            my $err = $_->as_string;
            $record->output($err);
            $record->preview_string($err);
        }
        # ClamAv::Error::Client errors
        elsif (eval {$_->isa('Error::Simple')}) {
            my $err = $_->stringify;
            $record->output($err);
            $record->preview_string($err);
        }
        else {
            $record->output($_);
            $record->preview_string($_);
        }
        die $_;
    };
}



=head2 register_mutations

register mutations.

=cut

sub register_mutations {
    my ($self, $case) = @_;

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        create              => 1,
        table               => 'Zaak',
        table_id            => $case->id
    );

    my $kenmerken = $case->field_values;

    foreach my $key (keys %$kenmerken) {
        $mutation->add_mutation({
            old_value   => '',
            new_value   => $kenmerken->{$key},
            column      => 'kenmerk.' . $key,
        });
    }

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation
    );
}



=head2 assert_valid_melding

Determine wether a given melding has the correct format.

To test:
- Pass incorrect and correct meldingen, see if they throw

=cut

sub assert_valid_melding {
    my ($self, $melding) = @_;

    throw('sysin/buitenbeter', 'Systeemfout: Melding niet gevonden')
        unless ref $melding eq 'HASH';

    for (BUITENBETER_FIELDS) {
        my $valid = $_ =~ m|^melder\.(\w+)$| ?
            exists $melding->{melder}->{$1} :
            exists $melding->{$_};

        throw('sysin/buitenbeter/invalid_field', 'Veld ' . $_ . ' niet aanwezig in melding')
            unless $valid;
    }
}



=head2 create_case

Given a buitenbeter melding record, create a new case

my $melding = {
      'attachment' => <BINARY JPG DATA>
      'buitenbeter_id' => 41051,
      'datum' => '2014-01-31T10:42:44',
      'extern_id' => 'NIL',
      'foto_url' => '@http://meldingtest.yucat.com:6080/emps_image/13400',
      'hash' => 'NIL',
      'huisnummer' => '59',
      'lattitude' => bless( {
                            '_e' => [
                                      13
                                    ],
                            '_es' => '-',
                            '_m' => [
                                      '429120725',
                                      '515349'
                                    ],
                            'sign' => '+'
                          }, 'Math::BigFloat' ),
      'locatieomschrijving' => 'NIL',
      'longitude' => bless( {
                            '_e' => [
                                      14
                                    ],
                            '_es' => '-',
                            '_m' => [
                                      '474231697',
                                      '446232'
                                    ],
                            'sign' => '+'
                          }, 'Math::BigFloat' ),
      'melder' => {
                  'achternaam' => 'Thiele',
                  'contactEmail' => 1,
                  'contactTelefoon' => 1,
                  'email' => 'F.thiele@yucat.com',
                  'geslacht' => 'M',
                  'huisnummer' => '248',
                  'plaats' => 'Driebergen',
                  'postcode' => '3972LK',
                  'straat' => 'Hoofdstraat',
                  'telefoonnummer' => '1234567890',
                  'voornaam' => 'Frank'
                },
      'omschrijving' => 'Test yucat',
      'plaats' => 'Roosendaal',
      'postcode' => '',
      'probleem' => 'Kapotte straatverlichting',
      'straat' => 'Dunantstraat'
    };

=cut



sub create_case {
    my ($self, $melding) = @_;

    $self->assert_valid_melding($melding);

    my $preset_client = $self->interface
        ->case_type_id
        ->zaaktype_node_id
        ->zaaktype_definitie_id
        ->preset_client or throw('sysin/buitenbeter', 'Vooringevulde aanvrager niet ingesteld');

    my $aanvrager = {
        'betrokkene' => $preset_client,
        'verificatie' => 'medewerker'
    };

    # binary, let's keep it out of the general flow
    # yup we're getting rid of attachment here, because during development
    # Dumping, looking at 20K of binary ain't too easy on the eyes.
    my $attachment = delete $melding->{attachment};

    my $kenmerken = $self->map_attributes($melding);

    my $opts = {
        aanvraag_trigger    => 'extern',
        contactkanaal       => 'email',
        onderwerp           => 'BuitenBeter',
        zaaktype_id         => $self->interface->case_type_id->id,
        aanvragers          => [$aanvrager],
        kenmerken           => $kenmerken,
        registratiedatum    => DateTime->now(),
    };

    my $case = $self->interface->result_source->schema->resultset('Zaak')->create_zaak($opts);


    if ($attachment) {
        $self->save_attachment({
            attachment => $attachment,
            case_id => $case->id,
            preset_client => $preset_client
        });
    }

    return $case;
}


=head2 save_attachment

store a given binary file on disk, link it to a given case_id

case should have a file attachment.

=cut

define_profile save_attachment => (
    required => [qw/attachment case_id preset_client/]
);

sub save_attachment {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $schema = $self->interface->result_source->schema;
    my $fh = File::Temp->new( UNLINK => 1);

    binmode $fh;
    print $fh $params->{attachment};
    close $fh;

    $schema->resultset('File')->file_create({
        db_params => {
            created_by => $params->{preset_client},
            case_id    => $params->{case_id}
        },
        name      => 'attachment.jpg',
        file_path => $fh->filename,
    });

}


=head2 map_attributes

Buitenbeter will return a structure containing attributes. These
attributes need to be filled in a new case. Using the interface
configuration, for each attribute we will try to find a local
bibliotheek_kenmerk within the given casetype.

So input be like:
{ attribute1 => value1, attribute2 => value2 }

Output be like:
[{ bibliotheek_kenmerken_id1 => value1}, {etc..}]

=cut

sub map_attributes {
    my ($self, $melding) = @_;

    my $casetype = $self->interface->case_type_id;

    # contains naam => id pairs
    my %magic_strings_lookup = map {
        $_->bibliotheek_kenmerken_id->magic_string => {
            id => $_->bibliotheek_kenmerken_id->id,
            value_type => $_->bibliotheek_kenmerken_id->value_type
        }
    } $casetype->zaaktype_node_id->zaaktype_kenmerken->search({
        bibliotheek_kenmerken_id => {
            -not => undef
        }
    })->all;

    my @attributes = grep {
        $_->{internal_name}->{searchable_object_id}
    } @{ $self->interface->get_interface_config->{attribute_mapping} };

    return [map {
        $self->_format_attribute(\%magic_strings_lookup, $_, $melding)
    } grep {
        exists $_->{internal_name}{searchable_object_id} &&
        exists $magic_strings_lookup{$_->{internal_name}{searchable_object_id}}
    } @attributes];
}


sub _format_attribute {
    my ($self, $magic_strings_lookup, $attribute, $melding) = @_;

    my $object_id = $attribute->{internal_name}{searchable_object_id};
    my $bibliotheek_kenmerk = $magic_strings_lookup->{$object_id};

    return {
        $bibliotheek_kenmerk->{id} => $self->preprocess_value({
            melding => $melding,
            external_name => $attribute->{external_name},
            value_type => $bibliotheek_kenmerk->{value_type}
        })
    };
}


=head2 preprocess_value

The SOAP stack interprets floats as Math::BigFloat. since the case creation
logic has no knowledge about that, stringify.

To test:

ok $self->preprocess_value({ pi => Math::BigFloat->new('3.14')}, 'pi') eq '3.14';

ok $self->preprocess_value({ straat}, 'melder.straat') eq '3.14';

ok $self->preprocess_value({ }, 'something else') eq 'kaas';

=cut

define_profile preprocess_value => (
    required => [qw/melding external_name value_type/]
);
sub preprocess_value {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $melding = $params->{melding};
    my $external_name = $params->{external_name};

    my $value = get_by_dotted_path({target => $melding, path => $external_name});

    if ($external_name eq 'geo_locatie') {
        return $self->lookup_geo_address({
            latitude => $melding->{lattitude}->bstr,
            longitude => $melding->{longitude}->bstr
        })->{result};
    }

    return $self->lookup_bag_address($melding) if
        $params->{value_type} =~ m|^bag_| &&
        $external_name eq 'bag_adres' &&
        $melding->{huisnummer} =~ m/^\d+$/;

    return ref $value && ref $value eq 'Math::BigFloat' ? $value->bstr : $value;
}



=head2 lookup_geo_address

=cut

define_profile lookup_geo_address => (
    required => [qw/longitude latitude/]
);

sub lookup_geo_address {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    # hide problems in the lookup in a result structure.
    # client can decide wether to deal with issues.
    try {
        my $geocoder = Zaaksysteem::Geo->new(query => $params->{latitude} . ' ' . $params->{longitude});
        $geocoder->geocode;
        return {
            success => $geocoder->success,
            result => $geocoder->success ? $geocoder->TO_JSON->[0]->{identification} : ''
        };
    } catch {
        return {
            success => 0,
            result => '',
            warning => "Exception thrown when looking up Geo address: " . burp($_)
        };
    };
}



=head2 lookup_bag_address

Find the out bag identification for the melder address. If not found, return
an empty string causes the BAG adres field to remain empty.

Test:

# put some address in bag
importbagaddress('Dam', '23', '24433434');

my $identificatie = $module->lookup_bag_address({
    'straat' => 'Dam',
    'huisnummer' => '23',
    'plaats' => 'Amsterdam'
});
ok $identificatie eq 'nummeraanduiding-34433434', "Found bag address";

+ the negative scenarios.
Also, what needs to happen to the other fields.
What if the config is not correct.

=cut

define_profile lookup_bag_address => (
    required => [qw/straat huisnummer plaats/],
    constraint_methods => {
        huisnummer => qr/^\d+$/
    }
);
sub lookup_bag_address {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    # now from the magic_string we need the bibliotheek_kenmerken_id
    my $schema = $self->interface->result_source->schema;

    my $woonplaats = $schema->resultset('BagWoonplaats')->search({
        naam => $params->{plaats}
    })->first;

    return '' unless $woonplaats && $woonplaats->identificatie;

    my $openbareruimte = $schema->resultset('BagOpenbareruimte')->search({
        naam => $params->{straat},
        woonplaats => $woonplaats->identificatie
    })->first;

    if ($openbareruimte) {
        my $hoofdadres = $openbareruimte->hoofdadressen->search({
            huisnummer => $params->{huisnummer}
        })->first;

        return 'nummeraanduiding-' . $hoofdadres->identificatie if $hoofdadres;
    }
    return '';
}



define_profile PostStatusUpdate => (
    required => [qw/
        kenmerken
        statusCode
        case_id
    /],
    optional => [qw/
        test_responses
        statusText
        toelichting
    /],
);

sub PostStatusUpdate {
    my $self      = shift;
    my $params    = assert_profile(shift)->valid;
    my $interface = shift;

    $self->interface($interface);
    $self->test_responses(delete $params->{test_responses}) if $params->{test_responses};

    $params->{buitenbeter_id} = $self->get_buitenbeter_id($params->{kenmerken});

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => 'xml',
        processor_params => {
            %$params,
            processor => '_process_PostStatusUpdate',
        },
    });
}


sub _process_PostStatusUpdate {
    my $self            = shift;
    my $record          = shift;

    my $transaction     = $self->process_stash->{transaction};
    my $params          = $transaction->get_processor_params();
    my $interface       = $transaction->interface;

    if (!$params->{toelichting}) {
        $params->{toelichting} = $params->{statusText};
    }

    try {
        $self->callSOAP($interface, 'PostStatusUpdate', $params);
    } catch {
        throw('sysin/buitenbeter', 'Buitenbeter statusupdate kon niet worden verwerkt', {fatal => 1});
    };

    $transaction->input_data($record->input($self->process_stash->{input}));
    $record->output($self->process_stash->{output});
    $record->preview_string('Status update "'. $params->{statusCode} . '" verstuurd voor zaak ' . $params->{case_id});


    $self->add_log_entry({
        case_id => $params->{case_id},
        input => $self->process_stash->{input},
        output => $self->process_stash->{output},
    });
}


define_profile add_log_entry => (
    required => [qw/case_id input output/]
);
sub add_log_entry {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $logging = $self->interface->result_source->schema->resultset('Logging');

    $logging->trigger('case/send_external_system_message', {
        component => 'case',
        component_id  => $params->{case_id},
        zaak_id       => $params->{case_id},
        data          => {
            destination => 'Buitenbeter',
            input => $params->{input},
            output => $params->{output}
        }
    });
}



=head2 get_buitenbeter_id

find out in which kenmerk the melding_id (= buitenbeter_id) is stored. it's possible to not
make this setting, which is a useless scenario so we'll bork out.

=cut

sub get_buitenbeter_id {
    my ($self, $kenmerken) = @_;

    $kenmerken ||= {};

    my $bibliotheek_kenmerken_id = $self->get_mapped_bibliotheek_kenmerken_id('buitenbeter_id');

    # should never happen, indicates disharmonious program structure. need to reharmonize
    return $kenmerken->{$bibliotheek_kenmerken_id} ||
        throw ('sysin/buitenbeter', 'Systeemfout: Buitenbeter_id is leeg in de zaak');
}


=head2 get_mapped_bibliotheek_kenmerken_id

Figure out to what bibliotheek_kenmerken_id a given field is mapped to.

=cut

sub get_mapped_bibliotheek_kenmerken_id {
    my ($self, $field) = @_;

    my ($buitenbeter_id_mapping) = grep {
        $_->{external_name} eq $field;
    } @{ $self->interface->get_interface_config->{attribute_mapping} };

    my $magic_string = $buitenbeter_id_mapping->{internal_name}{searchable_object_id}
        or throw('sysin/buitenbeter/buitenbeter_id_config_error',
            'Buitenbeter_id is niet gekoppeld aan een kenmerk, controleer de configuratie.');

    # now from the magic_string we need the bibliotheek_kenmerken_id
    my $schema = $self->interface->result_source->schema;

    my $kenmerk = $schema->resultset('BibliotheekKenmerken')->search({
        magic_string => $magic_string
    })->first or throw('sysin/buitenbeter',
        'Buitenbeter_id is niet gekoppeld aan een kenmerk, controleer de configuratie');

    return $kenmerk->id;
}


=head2 get_test_xml

If a stored xml file is available for given action, should return it.
Otherwise should throw
=cut

sub get_test_xml {
    my ($self, $interface, $action) = @_;

    # if ran from the testsuite, config->{home} is not set. init it on current dir.
    my $home = $interface->result_source->schema->default_resultset_attributes->{ config }->{home} || '.';

    # use the test xml to generate an actual melding
    open my $file, "<", "$home/t/inc/buitenbeter/$action.xml"
        or throw ('sysin/buitenbeter', "Testbestand voor $action kon niet geopend worden");
    my $content = join "", <$file>;
    close $file;

    return $content;
}


=head2 get_test_responses

Provide test responses. Primary use is for the test suite. Put in
the module so it can be reached from the frontend in case a
frontend test needs to be run or demo without uploading to an external
server.

=cut

sub get_test_responses {
    my ($self, $params, $interface) = @_;

    return { map { $_ => $self->get_test_xml($interface, $_) }
        (qw/GetNewMeldingen PostStatusUpdate/)
    };
}



sub callSOAP {
    my ($self, $interface, $action, $params) = @_;

    my $home = $interface->result_source->schema->default_resultset_attributes->{ config }->{home} || '.';

    my @xml_definitions = map {
        $home . '/share/wsdl/buitenbeter/MeldingService-xsd' . $_ .'.svc'
    } qw/0 1 2/;

    my $soap = Zaaksysteem::SOAP->new(
        wsdl_file       => 'buitenbeter/buitenbeter.wsdl',
        soap_action     => 'http://tempuri.org/IMeldingService/' . $action,
        soap_port       => 'BasicHttpBinding_IMeldingService',
        soap_endpoint   => $interface->get_interface_config->{endpoint},
        xml_definitions => [@xml_definitions],
        test_responses  => $self->test_responses,
    );

    $params->{username} = $interface->get_interface_config->{username};
    $params->{password} = $interface->get_interface_config->{password};

    my ($answer, $trace) = $soap->dispatch($action, { request => $params });

    $self->process_stash->{input} = $self->test_responses->{$action} ? 'Test bericht: ' . $action : $trace->request->content;
    $self->process_stash->{output} = $self->test_responses->{$action} || $trace->response->content;

    return $answer->{parameters}->{$action . 'Result'};
}


=head2 check_schedule

Poor man's cron, find out if at this quarterly run we need to act.
This determined by dividing the epoch quarter by the interval, if there
is no remainder, we're up.
e.g. say the interval is 2 quarters (30 minutes)
then only on the even epoch quarters we need to act (epoch_quarter % 2 =s= 0)
e.g. say the interval is 4 quarters (60 minutes)
then only act if epoch_quarter % 4 == 0

Time is passed as a variable so a testscript can spoof.

Returns a boolean indicating wether an action needs to be executed
in this run.

=cut

define_profile check_schedule => (
    required => [qw/time interval_minutes/]
);

sub check_schedule {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    # how many quarters since epoch?
    my $epoch_quarter = int($params->{time} / QUARTER);

    # how many quarters in an interval?
    my $interval_quarters = int($params->{interval_minutes} / 15);

    return !($epoch_quarter % $interval_quarters);
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 PostStatusUpdate

TODO: Fix the POD

=cut

=head2 QUARTER

TODO: Fix the POD

=cut

=head2 add_log_entry

TODO: Fix the POD

=cut

=head2 buitenbeter_test_GetNewMeldingen

TODO: Fix the POD

=cut

=head2 buitenbeter_test_connection

TODO: Fix the POD

=cut

=head2 buitenbeter_test_spoof

TODO: Fix the POD

=cut

=head2 callSOAP

TODO: Fix the POD

=cut


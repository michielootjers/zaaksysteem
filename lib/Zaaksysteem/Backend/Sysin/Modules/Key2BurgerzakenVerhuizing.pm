package Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::SOAP;
use Zaaksysteem::Constants;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenGeneric
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'key2burgerzakenverhuizing';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'Url key2burgerzaken',
        required    => 1,
        description => 'Vanuit Centric zal er een zogenoemde SOAP url worden'
            .' opgegeven. Deze URL gebruikt zaaksysteem.nl om mee te "praten".'
            .' Voorbeeld: http://HOST.TLD/BOKoppelvlakPIV/PROD/Versie2_0/Service.asmx'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_action_bvh07',
        type        => 'select',
        label       => 'Actie op Status BVH07',
        data        => {
            options     => [
                {
                    value    => '',
                    label    => 'Geen actie',
                },
                {
                    value    => 'close',
                    label    => 'Afhandelen',
                }
            ],
        },
        required    => 0,
        description => 'Wanneer de status BVH07 wordt doorgegeven, welke actie moet'
            .' er plaatsvinden op de aangemaakte zaak? Afhandelen betekent dat een'
            .' zaak wordt afgehandeld en het resultaat wordt gezet op onderstaand'
            .' zaakresultaat.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_resultaat_bvh07',
        type        => 'select',
        label       => 'Zaakresultaat op Status BVH07',
        data        => {
            options     => [
                map { {value => $_, label => ucfirst($_)} }
                @{ ZAAKSYSTEEM_OPTIONS->{RESULTAATTYPEN} }
            ]
            #     {
            #         value    => '',
            #         label    => 'Geen actie',
            #     },
            #     {
            #         value    => 'close',
            #         label    => 'Afhandelen',
            #     }
            # ],
        },
        required    => 1,
        description => 'Wanneer de status BVH07 wordt doorgegeven, wordt onderstaand'
            .' resultaat gezet. Zorg ervoor dat u het resultaat overneemt zoals u'
            .' deze heeft opgegeven in uw zaaktype.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_use_bag',
        type        => 'checkbox',
        label       => 'Gebruik BAG voor adres verhuizing',
        description => 'Hier kunt u aangeven of de BAG gebruikt wordt voor het bepalend'
            . ' van het nieuwe adres.'
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Key2Burgerzaken (Verhuizing)',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => ['text'],
    module_type                     => [INTERFACE_ID],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 1,
    is_casetype_interface           => 1,
    has_attributes                  => 1,
    attribute_list                  => [
        {
            external_name   => 'Verhuisdatum',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'IndicatieInwonend',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Meeverhuizers.BurgerservicenummerMeeverhuizer',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Statuscode',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Statusomschrijving',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'IndicatieMeldenAanBurger',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'BackOfficeID',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Adresgegevens.Postcode',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huisnummer',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },      
        {
            external_name   => 'Adresgegevens.Huisletter',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huisaanduiding',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huistoevoeging',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Contactgegevens.Emailadres',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'Contactgegevens.TelefoonnummerPrive',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'Contactgegevens.TelefoonnummerMobiel',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'BagAdres',
            attribute_type  => 'magic_string',
            use_bag         => 1,
        },
    ],
    trigger_definition  => {
        request_verhuizing   => {
            method  => 'request_verhuizing',
            #update  => 1,
        },
    },
    case_hooks                  => [
        {
            when        => 'case_register',
            trigger     => 'request_verhuizing'
        }
    ],
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'key2buz_test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};


###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head2 stuf_test_connection

=cut

sub key2buz_test_connection {
    my $self                        = shift;
    my $interface                   = shift;

    ### Connect with our StUF ServiceBus server.
    $self->test_host_port(
        $interface->get_interface_config->{endpoint}
    ) if $interface->get_interface_config->{endpoint};

    ### Success
    return 1;
}

sub get_attribute_mapping {
    my $self        = shift;
    my ($interface) = @_;

    my $config      = $self->next::method(@_);

    #warn(Data::Dumper::Dumper($interface->get_interface_config));

    if ($interface->get_interface_config->{use_bag}) {
        warn('grep use bag');
        $config->{attributes}     = [
            grep { !exists($_->{no_bag}) || !$_->{no_bag} }
            @{ $config->{attributes} }
        ];
    } else {
        warn('remove use bag');
        $config->{attributes}     = [
            grep { !exists($_->{use_bag}) || !$_->{use_bag} }
            @{ $config->{attributes} }
        ];
    }

    return $config;
}

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing - Key2Burgerzaken Verhuizingen interface API

=head1 SYNOPSIS

    my $transaction = $interface->process_trigger(
        'request_verhuizing',
        {
            case        => $CASE_ROW,
        }
    );


=head1 DESCRIPTION

This is the Key2Burgerzaken module, which will handle all transactions between zaaksysteem
and a Centric Key2Burgerzaken application regarding BinnenVerhuizingen.

=head1 TRIGGERS

=head2 $module->request_verhuizing(\%OPTIONS,$interface)

Return value: $TRANSACTION

    my $transaction = $interface->process_trigger(
        'request_verhuizing',
        {
            case        => $CASE_ROW,
        }
    );


Triggers a "Verhuizing" to Key2Burgerzaken. Creates a transaction in our database,
and mutation information in mutation tables.

B<Params>

=over 4

=item case_id

ID of case to use for sending information to Key2Burgerzaken.

=item case

Instead of an id, a C<$CASEROW> can be used for sending information to Key2Burgerzaken.

=back

=cut

Params::Profile->register_profile(
    method      => 'request_verhuizing',
    profile     => {
        required        => ['case'],
        optional        => ['case_id'],
        defaults    => {
            case        => sub {
                my ($dfv)       = @_;

                if ($dfv->get_filtered_data->{'case_id'}) {
                    return $dfv->get_input_data
                                ->{schema}
                                ->resultset('Zaak')
                                ->find($dfv->get_filtered_data->{'case_id'});
                }

                return;
            },
        }
    }
);

sub request_verhuizing {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    my $params                      = assert_profile(
        {
            %{ $raw_params },
            schema  => $interface->result_source->schema
        }
    )->valid;

    my $case                        = $params->{case};

    my $xml_params                  = $self->_collect_params_from_case($interface, $case);

    $xml_params->{Zaakgegevens}     = {
        'ZaakID'        => $case->id,
        'DatumAanvraag' => $case->registratiedatum->ymd,
    };

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor   => '_process_verhuizing',
                xml_params  => $xml_params,
                case_id     => $case->id,
            },
            external_transaction_id => 'unknown',
            input_data              => 'xml',
            #direct                  => 1,
        }
    );

    return $transaction;
}

=head1 INTERNAL METHODS

=head2 PROCESSORS

=head2 _process_verhuizing($transaction_record)

Return value: $STRING_OUTPUT

Do not use this function directly, instead let it play by trigger:
L<Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing#request_verhuizing>.

Sends a C<BinnenVerhuisaanvraag> to a Centric Key2Burgerzaken application. Received
information will populate a case with the the options which can be configured
in the attribute map of this integration module.

B<Options>

=over 4

=item BackOfficeID

The backoffice ID received from Centric

=item Statuscode

The status code received from centric, e.g. C<BVH07>

=item StatusOmschrijving

A human readable description of the status code received from centric

=back

=cut


sub _process_verhuizing {
    my $self            = shift;
    my $record          = shift;
    my $mutations;

    my $transaction     = $self->process_stash->{transaction};

    my $params          = $transaction->get_processor_params();

    ### Dispatch
    my $soap                        = Zaaksysteem::SOAP->new(
        wsdl_file       => 'key2burgerzaken/koppelvlak_v2.wsdl',
        soap_action     => 'urn:Centric/ITS/GBA/v1.0/BOAanvraag/Aanvraag',
        soap_service    => 'Service',
        soap_port       => 'ServiceSoap',
        soap_endpoint   => $transaction->interface->get_interface_config->{endpoint},
    );

    my ($answer, $trace)            = $soap->dispatch(
        'Aanvraag',
        { BinnenVerhuisaanvraagRequest => $params->{xml_params} }
    );
    
    $transaction->input_data($record->input($trace->request->content));

    if ($answer) {
        $self->_populate_case(
            $record,
            {
                xml_hash    => $answer
                                ->{AanvraagResult}
                                ->{BinnenVerhuisaanvraagResponse},
                case_id     => $params->{case_id},
            }
        )
    }

    $record->output($trace->response->content);
}



=head2 _process_row

The default processor for incoming StatusUpdateResponses from Centric. It will use
the Statuscode and Statusomschrijving to update a case. See C<_process_verhuizing>
for more information about status info.

=cut

sub _process_row {
    my $self                = shift;
    my ($record, $object)   = shift;

    my $transaction         = $record->transaction_id;
    $transaction->direction('incoming');
    $transaction->update;

    my $xs                  = XML::Simple->new();
    my $data                = $xs->XMLin($self->process_stash->{transaction}->input_data, ForceArray => 0, KeyAttr => 'statusUpdateRequest');

    my $xml_hash            = $data->{'soap:Body'}->{'statusUpdateRequest'};

    my $case_id             = $xml_hash->{Zaakgegevens}->{ZaakID};


    $self->_populate_case(
        $record,
        {
            xml_hash    => $xml_hash,
            case_id     => $case_id
        }
    );

    $record->output(XML::LibXML::Document->createDocument->toString);
}

=head2 HELPERS

=head2 _collect_params_from_case($interface, $case)

Collects information for a BinnenVerhuisRequest from the given case, with this hash
the C<_process_verhuizing> function is able to construct a valid XML message.



=cut

sub _collect_params_from_case {
    my $self                        = shift;
    my ($interface, $case)          = @_;

    my $xml_params                  = {};

    $self->_collect_params_from_case_requestor(@_, $xml_params);

    my $attributes                  = {};
    for my $attribute (@{ $interface->get_interface_config->{attribute_mapping} }) {
        my $external_name           = $attribute->{external_name};
        my $internal_name           = $attribute->{internal_name};

        my $value = '';
        if ($internal_name) {
            $value = $case->value_by_magic_string(
                {
                    magic_string    => $internal_name->{searchable_object_id},
                    plain           => 1
                }
            );
        }

        if ($external_name eq 'Verhuisdatum') {
            $value      =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
        }


        if (
            $interface->get_interface_config->{use_bag}
        ) {
            if ($external_name eq 'BagAdres') {
                my $address             = $interface
                                        ->result_source
                                        ->schema
                                        ->resultset('BagNummeraanduiding')
                                        ->get_address_data_by_source_identifier($value);

                $attributes->{'Adresgegevens.Postcode'}         = $address->{postcode};
                $attributes->{'Adresgegevens.Huisnummer'}       = $address->{huisnummer};
                $attributes->{'Adresgegevens.Huisletter'}       = $address->{huisletter};
                $attributes->{'Adresgegevens.Huistoevoeging'}   = $address->{huisnummertoevoeging};

                next;
            } elsif ($external_name =~ /^Adresgegevens\./) {
                next;
            }
        } else {
            if ($external_name eq 'BagAdres') {
                next;
            }
        }

        # get kenmerk value by magic string - functionality not ready yet
        $attributes->{ $external_name } = $value;
    }

    ### Params collected, create hash
    $xml_params->{Aanvraaggegevens}->{$_} = $attributes->{$_}
        for qw/
            Verhuisdatum
            IndicatieInwonend
        /;

    for my $adresitem (grep(/^Adresgegevens\./, keys %{ $attributes })) {
        my $looseprefix = $adresitem;
        $looseprefix  =~ s/^Adresgegevens\.//;

        next unless $attributes->{$adresitem};

        my $value       = '';
        $value          = $attributes->{$adresitem} if $attributes->{$adresitem};

        ### Fix postcode, make sure it is [1-9][0-9]{3}[A-Z]{2}
        if ($looseprefix eq 'Postcode') {
            $value          =~ s/\s//;
            $value          = uc($value);
        }

        $xml_params->{Aanvraaggegevens}->{'Adresgegevens'}->{$looseprefix} = $value;
    }

    for my $adresitem (grep(/^Contactgegevens\./, keys %{ $attributes })) {
        my $looseprefix = $adresitem;
        $looseprefix  =~ s/^Contactgegevens\.//;

        next unless $attributes->{$adresitem};

        my $value       = '';
        $value          = $attributes->{$adresitem} if $attributes->{$adresitem};

        ### Fix postcode, make sure it is [1-9][0-9]{3}[A-Z]{2}
        if ($looseprefix eq 'Postcode') {
            $value          =~ s/\s//;
            $value          = uc($value);
        }

        $xml_params->{Contactgegevens}->{$looseprefix} = $value;
    }

    if ($attributes->{'Meeverhuizers.BurgerservicenummerMeeverhuizer'}) {
        warn('Meeverhuizers: ' . Data::Dumper::Dumper($attributes->{'Meeverhuizers.BurgerservicenummerMeeverhuizer'}));

        my @bsns = split(
            ',',
            $attributes->{'Meeverhuizers.BurgerservicenummerMeeverhuizer'},
        );

        ### Field contains list of magic strings. Replace it with value from case
        for (my $i = 0; $i < scalar(@bsns); $i++) {
            if ($bsns[$i] =~ /\[\[/) {
                my $string = $bsns[$i];
                $string    =~ s/[\[\]]//g;

                $bsns[$i]  = $case->value_by_magic_string(
                    {
                        magic_string    => $string,
                        plain           => 1
                    }
                );
            }
        }

        $xml_params->{Aanvraaggegevens}->{Meeverhuizers} = {
            'Meeverhuizer'      => [],
        };

        for my $bsn (@bsns) {
            next unless $bsn =~ /^\d+$/;

            push(
                @{ $xml_params->{Aanvraaggegevens}->{Meeverhuizers}->{Meeverhuizer} },
                {
                    BurgerservicenummerMeeverhuizer     => $bsn,
                }
            );
        }

    }

    return $xml_params;
}

=head2 _collect_params_from_case_requestor($interface, $case, $xml_params)

Fills C<$xml_params> with information about the case_requestor of this case

=cut

sub _collect_params_from_case_requestor {
    my $self                        = shift;
    my ($interface, $case, $xml_params)  = @_;

    $xml_params->{Aanvraaggegevens}->{BurgerservicenummerAanvrager}  =
        $case->systeemkenmerk(
            'aanvrager_burgerservicenummer'
        );
}

=head2 _populate_case(\%options)

Populates a case with information from a xml_hash which is a C<HashRef> representation
of the given XML message from Centric.

B<Options>

=over 4

=item xml_hash

A C<HashRef> representation of the xml message from Centric

=item case_id

The case_id to work on

=back


B<Populates>

=over 4

=item BackOfficeID

The backoffice ID received from Centric

=item Statuscode

The status code received from centric, e.g. C<BVH07>

=item StatusOmschrijving

A human readable description of the status code received from centric

=back

=cut

Params::Profile->register_profile(
    method      => '_populate_case',
    profile     => {
        required        => [qw/xml_hash case_id/],
    }
);

sub _populate_case {
    my $self                        = shift;
    my $record                      = shift;
    my $options                     = assert_profile(shift || {})->valid;

    my $xml_hash                    = $options->{xml_hash};

    my $case                        = $record
                                    ->result_source
                                    ->schema
                                    ->resultset('Zaak')
                                    ->find($options->{case_id});

    my $interface_attrs             = $record
                                    ->transaction_id
                                    ->interface
                                    ->get_interface_config->{attribute_mapping};

    my $mapped_interface_attrs      = {
        map {
            $_->{external_name} => $_->{internal_name}->{searchable_object_id}
        } grep { ref($_->{internal_name}) } @{ $interface_attrs }
    };

    ### Collect all magic strings
    my $zt_kenmerken   = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => [ values %{ $mapped_interface_attrs }]
        },
        {
            join        => 'bibliotheek_kenmerken_id',
        }
    );


    
    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                        ->new(
                            table       => 'Zaak',
                            table_id    => $case->id,
                            create      => 1
                        );

    my $kenmerken_map   = {};
    while (my $zt_kenmerk = $zt_kenmerken->next) {
        $kenmerken_map->{ $zt_kenmerk->bibliotheek_kenmerken_id->magic_string } = 
            $zt_kenmerk->bibliotheek_kenmerken_id->id;
    }

    my $update_fields   = {};
    if ($xml_hash->{Backofficegegevens}) {

        if ($mapped_interface_attrs->{BackOfficeID}) {
            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{BackOfficeID} }
            } = $xml_hash->{Backofficegegevens}->{BackOfficeID};

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{BackOfficeID},
                    new_value   => $xml_hash->{Backofficegegevens}->{BackOfficeID},
                }
            );
        }
    }

    if ($xml_hash->{Status}) {
        if ($mapped_interface_attrs->{Statuscode}) {
            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{Statuscode} }
            } = $xml_hash->{Status}->{Statuscode};

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{Statuscode},
                    new_value   => $xml_hash->{Status}->{Statuscode},
                }
            );
        }

        if ($mapped_interface_attrs->{Statusomschrijving}) {
            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{Statusomschrijving} }
            } = $xml_hash->{Status}->{StatusOmschrijving};

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{Statusomschrijving},
                    new_value   => $xml_hash->{Status}->{StatusOmschrijving},
                }
            );
        }

        if (
            $mapped_interface_attrs->{IndicatieMeldenAanBurger}
        ) {
            my $new_value = (
                $xml_hash->{Status}->{IndicatieMeldenAanBurger} eq 'true'
                    ? 'Melden aan burger'
                    : 'Niet melden aan burger'
            );

            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{IndicatieMeldenAanBurger} }
            } = $new_value;

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{IndicatieMeldenAanBurger},
                    new_value   => $new_value,
                }
            );
        }
    }

    $case->zaak_kenmerken->update_fields(
        {
            new_values  => $update_fields,
            zaak_id     => $case->id,
        }
    );
    $case->touch();

    $record->preview_string('Zaak: ' . $case->id . ': ' . $case->onderwerp);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    if (
        $xml_hash->{Status}->{Statuscode} eq 'BVH07' &&
        $record ->transaction_id
                ->interface
                ->get_interface_config->{action_bvh07} eq 'close'
    ) {
        $case->resultaat(
            $record ->transaction_id
                    ->interface
                    ->get_interface_config->{resultaat_bvh07}
        );

        $case->set_gesloten(DateTime->now());


        $case->logging->trigger('case/early_settle', {
            component => 'zaak',
            data => {
                case_id     => $case->id,
                reason      =>
                    'Gesloten ivm key2burgerzaken transactie: ' . 
                    $self->process_stash->{transaction}->id,
                case_result => $case->resultaat
            }
        });
    }
}

=head1 EXAMPLES

B<BinnenVerhuizing XML Request>

    <?xml version="1.0" encoding="UTF-8"?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
        <SOAP-ENV:Body>
            <tns:AanvraagRequest xmlns:tns="urn:Centric/ITS/GBA/v1.0/BOAanvraag" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <tns:BinnenVerhuisaanvraagRequest>
                    <tns:Zaakgegevens>
                        <tns:ZaakID>90</tns:ZaakID>
                        <tns:DatumAanvraag>2013-08-10</tns:DatumAanvraag>
                    </tns:Zaakgegevens>
                    <tns:Aanvraaggegevens>
                        <tns:BurgerservicenummerAanvrager>987654321</tns:BurgerservicenummerAanvrager>
                        <tns:Verhuisdatum>1970-01-01</tns:Verhuisdatum>
                        <tns:IndicatieInwonend>0</tns:IndicatieInwonend>
                        <tns:Adresgegevens>
                            <tns:Postcode>1051JL</tns:Postcode>
                            <tns:Huisnummer>7</tns:Huisnummer>
                            <tns:Huistoevoeging>521</tns:Huistoevoeging>
                        </tns:Adresgegevens>
                    </tns:Aanvraaggegevens>
                    <tns:Contactgegevens>
                        <tns:Emailadres>michiel@example.com</tns:Emailadres>
                        <tns:TelefoonnummerPrive>0612345678</tns:TelefoonnummerPrive>
                        <tns:TelefoonnummerMobiel>0612345678</tns:TelefoonnummerMobiel>
                    </tns:Contactgegevens>
                </tns:BinnenVerhuisaanvraagRequest>
            </tns:AanvraagRequest>
        </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>

B<BinnenVerhuizing XML Response>

    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soap:Body>
            <AanvraagResponse xmlns="urn:Centric/ITS/GBA/v1.0/BOAanvraag">
                <BinnenVerhuisaanvraagResponse>
                    <Zaakgegevens>
                        <ZaakID>90</ZaakID>
                    </Zaakgegevens>
                    <Backofficegegevens>
                        <ApplicatieID>PIV</ApplicatieID>
                        <BackOfficeID>5583</BackOfficeID>
                    </Backofficegegevens>
                    <Status>
                        <Datum>2013-08-13</Datum>
                        <Statuscode>BVH01</Statuscode>
                        <StatusOmschrijving>De aanvraag is ter beoordeling aangeboden aan een ambtenaar.</StatusOmschrijving>
                        <IndicatieMeldenAanBurger>true</IndicatieMeldenAanBurger>
                    </Status>
                </BinnenVerhuisaanvraagResponse>
            </AanvraagResponse>
        </soap:Body>
    </soap:Envelope>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1; #__PACKAGE__->meta->make_immutable();

package Zaaksysteem::Backend::Sysin::Modules::AuthLDAP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Backend::Sysin::Transaction::Mutation;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'authldap';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_autocreate_user',
        type => 'checkbox',
        required => 1,
        label => 'Auto-aanmaak gebruikers',
        description => 'Indien het wenselijk is gebruikers te importeren bij de eerste keer dat ze zich aanmelden op het Zaaksysteem, vink dit aan'
    ),
];

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'LDAP Authenticatie',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [],
    is_multiple => 1,
    is_manual => 1,
    retry_on_error => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface => 0,
    trigger_definition  => {
        register_changes   => {
            method  => 'register_changes',
        },
    },
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() }, @_ );
};

sub register_changes {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_changes',
                changes         => $raw_params->{changes},
            },
            external_transaction_id => 'unknown',
            input_data              => 'json',
            #direct                  => 1,
        }
    );

    return $transaction;
}

sub _process_changes {
    my $self            = shift;
    my $record          = shift;
    my $mutations;

    my $transaction     = $self->process_stash->{transaction};

    my $params          = $transaction->get_processor_params();

    my $changes         = $params->{changes};

    my $mutation        = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        table               => 'Subject',
        table_id            => $changes->{_subject_id}
    );

    my $subject         = $transaction
                        ->result_source
                        ->schema
                        ->resultset('Subject')
                        ->search({ id => $changes->{_subject_id} })
                        ->first;

    throw(
        'sysin/modules/authldap',
        'Cannot find subject by id: ' . $changes->{_subject_id}
    ) unless $subject;

    if ($changes->{_create}) {
        $mutation->create(1);
    }
    else {
        $mutation->update(1);
    }


    my $human_readable_changes = sprintf("User %s changed\n", $subject->cn || $subject->username);
    for my $column (keys %{ $changes }) {
        my $columndata = $changes->{ $column };
        next unless UNIVERSAL::isa($changes->{$column}, 'HASH');

        $mutation->add_mutation(
            {
                old_value   => $columndata->{old},
                new_value   => $columndata->{new},
                column      => $column,
            }
        );

        $human_readable_changes .= 'Attribuut "' . $column
                                . '" gewijzigd naar "'
                                . $columndata->{new} . "\"\n";
    }

    $self->process_stash->{row}->{mutations} = [ $mutation ];

    $record->input($human_readable_changes);

    $record->output('SUCCESFULL');
}

1;

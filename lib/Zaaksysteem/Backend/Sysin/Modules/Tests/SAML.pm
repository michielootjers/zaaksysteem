package Zaaksysteem::Backend::Sysin::Modules::Tests::SAML;

use Moose::Role;

use Zaaksysteem::Exception;
use Zaaksysteem::SAML2::SP;

sub test_sp {
    my $self = shift;
    my $interface = shift;

    # SP package must itself be sufficiently robust to handle
    # misconfigured interfaces right off the bat. This isn't a
    # unit testing suite ya'know
    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        interface => $interface
    );
}

sub test_idp {
    my $self = shift;
    my $interface = shift;

    my $idp = Zaaksysteem::SAML2::IdP->new_from_interface(
        interface => $interface
    );
}

1;

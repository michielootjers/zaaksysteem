package Zaaksysteem::Backend::Sysin::Modules::QMatic;

use Moose;
use Time::HiRes qw/tv_interval gettimeofday/;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;

use Zaaksysteem::Constants;

use Zaaksysteem::Exception;
use Data::Dumper;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;


use constant INTERFACE_ID               => 'qmatic';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'Url QMatic',
        required    => 1,
        description => 'QMatic SOAP server'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoofmode',
        type        => 'checkbox',
        label       => 'Spoofmode',
        required    => 1,
        description => 'Work locally with dummy data, useful when QMatic is not responding. Development feature'
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'QMatic',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 1,
    trigger_definition  => {
        getProduct => {
            method => 'getProduct',
            update => 1
        },
        getAvailableProducts => {
            method => 'getAvailableProducts',
            update => 1
        },
        getAvailableAppointmentDays => {
            method => 'getAvailableAppointmentDays',
            update => 1
        },
        getAvailableAppointmentTimes => {
            method => 'getAvailableAppointmentTimes',
            update => 1
        },
        deleteAppointment => {
            method => 'deleteAppointment',
            update => 1
        },
        bookAppointment => {
            method => 'bookAppointment',
            update => 1
        },
    },
    case_hooks                  => [],
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'qmatic_test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};

has response => (is => 'rw', default => sub { undef });

sub dynamic_attribute_list {
    my ($self, $interface) = @_;

    my $products = $self->getAvailableProducts({}, $interface);

    return [ map {
        {
            attribute_type => 'magic_string',
            all_casetypes  => 1,
            checked        => 1,
            linkId         => $_->{linkId},
            appointLength  => $_->{appointLength},
            external_name  => $_->{name} . ' (' . $_->{appointLength} . ' minuten)'
        }
    } @$products ];
}


around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


sub qmatic_test_connection {
    my ($self, $interface) = @_;

    $self->test_host_port($interface->get_interface_config->{endpoint});
}


=head2 getProduct

Reverse lookup of QMatic product using the magic_string

This mapping is manually configured in the interface

=cut

sub getProduct {
    my ($self, $params, $interface) = @_;

    my $magic_string = $params->{magic_string};
    my $mapping = $interface->get_interface_config->{attribute_mapping};

    foreach my $item (@$mapping) {
        my $object_id = $item->{internal_name}->{searchable_object_id};

        if ($object_id && $object_id eq $magic_string) {
            return $item;
        }
    }
}


sub getAvailableProducts {
    my ($self, $params, $interface) = @_;

    return $self->callSOAP($interface, 'getAvailableProducts', {});
}


sub getAvailableAppointmentDays {
    my ($self, $params, $interface) = @_;

    my $days = $self->callSOAP($interface, 'getAvailableAppointmentDays', {
        productLinkID => $params->{productLinkID}
    });

    return [$self->addTimezones(@$days)];
}


sub getAvailableAppointmentTimes {
    my ($self, $params, $interface) = @_;

    my $times = $self->callSOAP($interface, 'getAvailableAppointmentTimes', {
        productLinkID => $params->{productLinkID},
        appDate => $self->removeTimezone($params->{appDate})
    });

    return [$self->addTimezones(@$times)];
}


sub deleteAppointment {
    my ($self, $params, $interface) = @_;

    my $result = $self->callSOAP($interface, 'deleteAppointment', {
        appointmentId => $params->{appointmentId}
    });

    # zapi likes arrays
    return [$result];
}


sub bookAppointment {
    my ($self, $params, $interface) = @_;

    if ($params->{previous_appointment_id}) {

        my $deleted_result = $self->deleteAppointment({
            appointmentId => $params->{previous_appointment_id},
        }, $interface);

        warn "Delete appointment result: " . Dumper $deleted_result;

        die "error deleting appointment" unless $deleted_result->[0]->{appointmentStatus} eq '1';
    }

    my $aanvrager = $self->retrieve_aanvrager($interface, $params->{aanvrager});

    my $appointment_info = {
        productLinkID => $params->{productLinkID},
        appDate => $self->removeTimezone($params->{appDate}),
        appTime => $self->removeTimezone($params->{appTime}),
        name => $aanvrager->naam,
        address => $aanvrager->street_address,
        town => $aanvrager->city,
        state => '',
        zipCode => $aanvrager->postal_code,
        country => 'Nederland',
        phone => $aanvrager->telefoonnummer || '',
        phone2 => $aanvrager->mobiel || '',
        email => $aanvrager->email || '',
        piNo => $aanvrager->btype eq 'bedrijf' ? $aanvrager->dossiernummer : $aanvrager->burgerservicenummer,
        accountNo => '',
        cardNum => '',
        title => '',
        born => '', #$aanvrager->btype eq 'natuurlijk_persoon' ? $aanvrager->geboortedatum : '-',
        vipLevel => '0',
        gender => $aanvrager->btype eq 'natuurlijk_persoon' ? $aanvrager->geslachtsnaam : '-',
        social => '',
        family => '',
        customerClass => '',
        comments => ''
    };

    my $result =  $self->callSOAP($interface, 'bookAppointment', $appointment_info);

    # zapi likes arrays.
    return [$result];
}

sub callSOAP {
    my ($self, $interface, $action, $params) = @_;

    if ($interface->get_interface_config->{spoofmode}) {
        return $self->spoof($action, $params);
    }

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => 'xml',
        processor_params => {
            processor => '_process_qmatic',
            action => $action,
            params => $params,
        },
    });

    return $self->response;
}

sub _process_qmatic {
    my $self             = shift;
    my $record           = shift;

    my $transaction      = $self->process_stash->{transaction};
    my $processor_params = $transaction->get_processor_params;
    my $interface        = $transaction->interface;
    my $action = $processor_params->{action};
    my $params = $processor_params->{params};

    my $call = $self->compile_call($interface, $action);

    my $t0 = [gettimeofday];

    my ($answer, $trace) = $call->($params);
    $transaction->input_data($record->input($trace->request->content));
    $record->output($trace->response->content);
    $interface->result_source->schema->log->info("QMatic '$action' request took " . tv_interval($t0) . ' ms.');

    $self->response($answer->{parameters}->{$action . 'Return'});
}



sub compile_call {
    my ($self, $interface, $action) = @_;

    my $home = $interface->result_source->schema->default_resultset_attributes->{config}->{home}
        or die "need home config";

    my $wsdl = XML::Compile::WSDL11->new($home . '/share/wsdl/qmatic/calendar-service.wsdl') or die $!;

    my $transport = XML::Compile::Transport::SOAPHTTP->new(
        timeout => 10,
        address => $interface->get_interface_config->{endpoint}
    );

    $transport->userAgent->default_header('SOAPAction', '');

    return $wsdl->compileClient(
        operation => $action,
        transport => $transport->compileClient()
    );
}


sub spoof {
    my ($self, $action, $params) = @_;

    my $spoof = {
        deleteAppointment            => { appointmentStatus => '1' },
        bookAppointment              => { appointmentId => time()  },
        getAvailableAppointmentDays  => [
            '2013-12-25T12:00:00.000+0100',
            '2013-12-26T12:00:00.000+0100',
            '2013-12-27T12:00:00.000+0100',
            '2013-12-28T12:00:00.000+0100',
            '2013-12-29T12:00:00.000+0100',
            '2013-12-30T12:00:00.000+0100',
            '2014-01-02T12:00:00.000+0100',
        ],
        getAvailableAppointmentTimes => [
            '1970-01-01T13:15:00.000+0100',
            '1970-01-01T12:10:00.000+0100',
            '1970-01-01T13:20:00.000+0100',
            '1970-01-01T14:30:00.000+0100',
            '1970-01-01T15:40:00.000+0100',
            '1970-01-01T16:45:00.000+0100',
        ],
        getAvailableProducts         => [ map {
            {linkId => $_, name => $_, appointLength => 34}
        } qw/boter kaas eieren/]
    };

    return $spoof->{$action};

}
=head2 retrieve_aanvrager

boilerplate to get to the aanvrager object

=cut

sub retrieve_aanvrager {
    my ($self, $interface, $betrokkene_string) = @_;

    my $betrokkene_model = $interface->result_source->schema->resultset('Zaak')->betrokkene_model;

    return $betrokkene_model->get_by_string($betrokkene_string);
}







sub _generate_interface_form_attribute_mapping {
    my $self    = shift;
    my $f       = shift;

    return unless $self->has_attributes;

    my $title = "Kenmerken koppelen";

    my $fieldsets = $f->fieldsets;
    push @$fieldsets,
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => $title,
            description => 'Uw koppeling maakt gebruik van attributen, welke weer'
                . ' gekoppeld kunnen worden aan velden binnen het zaaksysteem.'
                . ' Druk op de knop "' . $title . '" om deze velden aan elkaar te koppelen'
        );

    my $actions = $fieldsets->[-1]->actions;

    push $actions,
        {
            "name"          => "attribute_mapping",
            "label"         => $title,
            "type"          => "popup",
            "importance"    => "secondary",
            "disabled"      => "!isFormValid()",
            "data"          => {
                "template_url"  => "/html/sysin/links/mapping.html",
                "title"         => $title
            }
        };
}


=head2 addTimezones

The given dates are in UTC format which the frontend automatically
localizes. Since the dates are really in Dutch time, this gives
an undesired output. Here, we make the timezone explicit, causing
the frontend to render the right timezone difference (which is none).
This needs be reversed before communicating back to QMatic.

=cut

sub addTimezones {
    my ($self, @dates) = @_;

    $_ =~ s/Z$/+0100/ for @dates;

    return @dates;
}


=head2 removeTimezone

Because the times are really in Dutch local time, we need to make
the timezone explicit. However, since QMatic expects no timezone
information, this needs to be reverted.

=cut

sub removeTimezone {
    my ($self, $date) = @_;

    $date =~ s/\+0100$/Z/;

    return $date;
}


=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

package Zaaksysteem::Backend::Sysin::Modules::Email;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::Backend::Email;
use Zaaksysteem::Backend::Mailer;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'email';

use constant INTERFACE_CONFIG_FIELDS    => [
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_how_hot',
    #     type        => 'text',
    #     label       => 'How hot is it in here',
    #     required    => 1,
    # )
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Ingeplande Email',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 0,
    retry_on_error                  => 1,
    trigger_definition  => {
        schedule_mail   => {
            method  => 'schedule_mail',
            #update  => 1,
        },
        delete_pending  => {
            method  => 'delete_pending',
            #update  => 1,
        },
        get_pending  => {
            method  => 'get_pending',
            #update  => 1,
        },
    },
};


has 'email_sender' => (
    is => 'rw',
    lazy => 1,
    default => sub { Zaaksysteem::Backend::Mailer->new }
);

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

Params::Profile->register_profile(
    method      => 'schedule_mail',
    profile     => {
        required        => [qw/case_id notification/],
        optional        => [qw/scheduled_for/],
    }
);

sub schedule_mail {
    my $self                        = shift;
    my $params                      = assert_profile(shift || {})->valid;
    my $interface                   = shift;

    # {
    #     case_id                     => $zaak_id,
    #     notification                => {
    #         id                            => $bibliotheek_notificaties_id,
    #         recipient_type              => $recipient_type,
    #         behandelaar                 => $behandelaar,
    #         email                       => $email,
    #         case_id                     => $zaak_id,
    #     }
    # }

    my $case                        = $interface 
                                    ->result_source
                                    ->schema
                                    ->resultset('Zaak')
                                    ->find($params->{case_id});


    my $notificatie                 = $interface
                                    ->result_source
                                    ->schema
                                    ->resultset('BibliotheekNotificaties')
                                    ->find(
                                        $params->{notification}->{id}
                                    );

    my $description                 =
        "\nZaak ID      : " . $params->{case_id} .
        "\nOntvanger    : " . $params->{recipient_type} .
        "\nBerichtlabel : " . $notificatie->label .
        "\n\n==============" .
        "\nSubject    : " . $notificatie->subject .
        "\nBericht:\n" . $notificatie->message;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_mail',
                notification    => $params->{notification},
                case_id         => $params->{case_id},
                label           => $notificatie->label,
            },
            external_transaction_id => 'mail-case-' . $case->id,
            input_data              => $description,
            direct                  => 0,
            schedule                => $params->{scheduled_for},
        }
    );
}

sub delete_pending {
    my $self                        = shift;

    my $transactions                = $self->get_pending(@_);

    while (my $transaction = $transactions->next) {
        $transaction->error_fatal(1);
        $transaction->input_data('Rescheduled');
        $transaction->date_next_retry(DateTime->now());
        $transaction->error_count(1);
        $transaction->update;
    }

    return 1;
}

Params::Profile->register_profile(
    method      => 'get_pending',
    profile     => {
        required        => [qw/case_id/],
    }
);

sub get_pending {
    my $self                        = shift;
    my $params                      = assert_profile(shift || {})->valid;
    my $interface                   = shift;

    my $transactions                = $interface->transactions->search(
        {
            date_next_retry         => { '>' => DateTime->now() },
            external_transaction_id => 'mail-case-' . $params->{case_id},
        }
    );

    return $transactions;
}

sub _process_mail {
    my $self                        = shift;
    my ($record, $params)           = @_;

    my $case                        = $record 
                                    ->result_source
                                    ->schema
                                    ->resultset('Zaak')
                                    ->find($params->{case_id});

    my $notificatie                 = $record
                                    ->result_source
                                    ->schema
                                    ->resultset('BibliotheekNotificaties')
                                    ->find(
                                        $params->{notification}->{id}
                                    );

    my $prepared_notification       = $case->prepare_notification({
        recipient_type  => $params->{notification}->{recipient_type},
        behandelaar     => $params->{notification}->{behandelaar}, 
        email           => $params->{notification}->{email},
        body            => $notificatie->message,
        subject         => $notificatie->subject,
    });

    my $mailer  = Zaaksysteem::Backend::Email->new(
        default_from_email      => $record 
                                ->result_source
                                ->resultset
                                ->{attrs}
                                ->{config}
                                ->{gemeente}
                                ->{zaak_email},
        schema                  => $record->result_source->schema,
        email_sender            => $self->email_sender,
    );

    my $body;
    eval {
        $body = $mailer->send_from_case(
            {
                case                => $case,
                notification        => $prepared_notification,
                betrokkene_id       => (
                    $record->result_source->resultset->{attrs}->{current_user}
                        ? 'betrokkene-medewerker-' . $record->result_source->resultset->{attrs}->{current_user}->uidnumber
                        : $case->aanvrager_object->rt_setup_identifier
                )
            }
        );
    };

    if ($@ && $@ =~ /no recipients/) {
        throw(
            'sysin/modules/email/mailer_problem',
            "\n\nE-mail niet verstuurd: e-mailadres is niet opgegeven door aanvrager of behandelaar.",
            {
                fatal => 1
            }
        );
        return;
    } elsif ($@) {
        throw(
            'sysin/modules/email/mailer_problem',
            "Problem bij het mailen van ingeplande e-mail: $@",
            {
                fatal => 1
            }
        );
    }

    $record->output(
        "Zaak ID      : " . $params->{case_id} .
        "\nOntvanger    : " . ($params->{recipient_type}||'') .
        "\nBerichtlabel : " . $notificatie->label .
        "\n\n==============" .
        "\nSubject    : " . $notificatie->subject .
        "\nBericht:\n" . $body
    );

}


1;

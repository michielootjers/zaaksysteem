package Zaaksysteem::Backend::Sysin::Modules::STUFNNP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufnnp';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_stuf_version',
        type        => 'select',
        label       => 'Versie van StUF',
        data        => {
            options     => [
                {
                    value    => '0204',
                    label    => '0204',
                },
                {
                    value    => '0310',
                    label    => '0310',
                }
            ],
        },
        required    => 1,
        description => 'Afhankelijk van de makelaar, kan er gekozen worden voor'
            .' StUF versie 0204 of 0310. Raadpleeg de leverancier van de makelaar'
            .' voor de te gebruiken versie.'
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling NNP',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
        # search          => {
        #     method  => 'search_nnp',
        #     #update  => 1,
        # },
        # import      => {
        #     method  => 'import_nnp',
        #     #update  => 1,
        # },
    },
    # has_attributes                  => 1,
    # attribute_list                  => [
    #     {
    #         external_name   => 'PRS.a-nummer',
    #         internal_name   => 'a_nummer',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.bsn-nummer',
    #         internal_name   => 'burgerservicenummer',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voornamen',
    #         internal_name   => 'voornamen',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorletters',
    #         internal_name   => 'voorletters',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorvoegselGeslachtsnaam',
    #         internal_name   => 'voorvoegsel',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsnaam',
    #         internal_name   => 'geslachtsnaam',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geboortedatum',
    #         internal_name   => 'geboortedatum',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsaanduiding',
    #         internal_name   => 'geslachtsaanduiding',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.datumOverlijden',
    #         internal_name   => 'datum_overlijden',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.indicatieGeheim',
    #         internal_name   => 'indicatie_geheim',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.aanduidingNaamgebruik',
    #         internal_name   => 'aanduiding_naamgebruik',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    # ]
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'NNP'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'Bedrijf'
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFPRS - STUFPRS engine for StUF PRS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/431-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 TRIGGERS

=head2 search($params)

=cut

=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_NATUURLIJK_PERSOON

Creates a new L<Zaaksysteem::DB::Component::NatuurlijkPersoon> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $organization_params = $object->get_params_for_organization;

    my $org_rs              = $record
                            ->result_source
                            ->schema
                            ->resultset('Bedrijf');

    ### BACKWARDS compatability
    my ($entry);
    if (
        !(
            $entry = $self->_create_subject_bedrijf_bwcompat(
                @_, $organization_params, $org_rs
            )
        )
    ) {
        $entry              = $org_rs->create(
            {
                %{ $organization_params },
                authenticated       => 1,
                authenticatedby     => 'kvk'
            }
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'Bedrijf',
                                table_id    => $entry->id,
                                create      => 1
                            );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    $record->preview_string(
        $entry->handelsnaam
    );

    return $entry;
}




=head2 $module->_create_subject_bedrijf_persoon_bwcompat(@params)

Makes sure we do not create duplicates for already created natuurlijk_personen. Just
make sure the "afnemerindicatie" is set and the subject gets updated.

 Params:
 transaction_record, $rowobject, $subject_params, $natuurlijk_persoon

=cut

sub _create_subject_bedrijf_bwcompat {
    my $self                        = shift;
    my ($record, $object, $subject_params, $bedrijf_rs)   = @_;

    my $entry                       = $bedrijf_rs->search(
        {
            dossiernummer           => $subject_params->{dossiernummer},
            subdossiernummer        => $subject_params->{subdossiernummer},
            authenticated           => 1,
            authenticatedby         => 'kvk'
        }
    )->first;

    if ($entry) {
        $entry->update($subject_params);
    }

    return $entry;
}


=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut

sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $bedrijf;
    eval {
        $bedrijf            = $self->get_entry_from_subscription(@_, 'Bedrijf');
    };

    if ($@) {
        ### We probably did not find a subscription for this entry, check via bwcompat
        my $subject_params  = $object->get_params_for_organization;

        die($@) unless $subject_params->{dossiernummer};

        my $entries         = $record
                            ->result_source
                            ->schema
                            ->resultset('Bedrijf')
                            ->search(
                                {
                                    fulldossiernummer           => $subject_params->{fulldossiernummer},
                                    authenticated               => 1,
                                    authenticatedby             => 'kvk'
                                }
                            );

        if ($entries->count) {
            $bedrijf                = $entries->first;

            ### Make sure this won't happen again
            $self->_add_subscription_for_entry($record, $object, $bedrijf);
        } else {
            die($@);
        }
    }

    $self->_update_subject_bedrijf(
        @_, $bedrijf
    );

    $record->preview_string(
        $bedrijf->handelsnaam
    );

    return $bedrijf;    
}

sub _update_subject_bedrijf {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;

    my $subject_params      = $object->get_params_for_organization;
    my %old_values          = $entry->get_columns;

    unless ($entry->update($subject_params)->discard_changes) {
        throw(
            'sysin/modules/stufnnp/process/np_update_error',
            'Impossible to update Bedrijf in our database, unknown error'
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'Bedrijf',
                                table_id    => $entry->id,
                                update      => 1
                            );

    ### Log mutation
    $mutation_record->from_dbix($entry, \%old_values);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}


=head2 DELETE SUBJECT

=head2 stuf_delete_entry

=cut


sub stuf_delete_entry {
    my $self                = shift;
    my ($record, $object, $subscription)    = @_;


    my $bedrijf                             = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'Bedrijf',
                                                $subscription
                                            );

    $record->preview_string(
        $bedrijf->handelsnaam
    );

    $bedrijf->deleted_on(DateTime->now());
    $bedrijf->update;

    $self->_remove_subscription_from_entry($record, $object, $bedrijf);
}






=head1 INTERNAL METHODDS



=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

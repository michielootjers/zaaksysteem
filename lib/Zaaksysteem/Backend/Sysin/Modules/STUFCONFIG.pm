package Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufconfig';

use constant INTERFACE_CONFIG_FIELDS    => [
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_zs_cert',
    #     type        => 'file',
    #     label       => 'Public key (zaaksysteem)',
    #     required    => 0,
    # ),
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_zs_key',
    #     type        => 'file',
    #     label       => 'Private key (zaaksysteem)',
    #     required    => 0,
    # ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gemeentecode',
        type        => 'text',
        label       => 'Gemeente code',
        required    => 1,
        description => 'Voor een lijst van gemeente codes, zie:  (zie http://nl.wikipedia.org/wiki/Gebruiker:Michiel1972/Gemeentecodes)',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_cert',
        type        => 'file',
        label       => 'Public key (Makelaar)',
        #required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_ontvanger',
        type        => 'text',
        label       => 'Naam ontvanger (e.g. CGM)',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_sync_url',
        type        => 'text',
        label       => 'Makelaar url (SYNC)',
        description => 'Voorbeeld: http://MAKELAAR_URL/ListenerStUFBG204Synchroon)',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_async_url',
        type        => 'text',
        label       => 'Makelaar url (ASYNC)',
        description => 'Voorbeeld: http://MAKELAAR_URL/ListenerStUFBG204Asynchroon)',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_stuf_supplier',
        type        => 'select',
        label       => 'Aanbieder van ServiceBus',
        data        => {
            options     => [
                {
                    value    => 'centric',
                    label    => 'Centric',
                },
                {
                    value    => 'pink',
                    label    => 'Pink',
                },
                {
                    value    => 'vicrea',
                    label    => 'Vicrea',
                }
            ],
        },
        required    => 1,
        description => 'Elke leverancier heeft haar eigen manier van interpretatie '
            .'van het StUF protocol. Om deze smaken uit elkaar te houden, vragen wij '
            .'u uw leverancier aan te kruizen.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_bidirectional',
        type        => 'checkbox',
        label       => 'Tweeweg verkeer inschakelen',
        description => 'Wanneer zaaksysteem.nl is gekoppeld met een makelaar voor'
            .' verkeer naar de makelaar toe, dient dit vinkje ingeschakeld te staan. '
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_makelaar_search',
        type        => 'checkbox',
        label       => 'Rechtstreeks zoeken in makelaar',
        description => 'Wanneer zaaksysteem.nl is gekoppeld met een makelaar voor'
            .' verkeer naar de makelaar toe, en rechtstraaks zoeken gewenst is. '
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_automatic_desubscription_moved_prs',
        type        => 'checkbox',
        label       => 'PRS verwijderen bij verhuizen',
        description => 'Een PRS verwijderen en (bij tweewegkoppeling met makelaar)'
            .' afnemerindicatie verwijderen via de makelaar wanneer'
            .' een PRS is verhuisd naar een andere gemeente. LET OP: Bag moet zijn geimporteerd'
            .' om te kunnen detecteren of een persoon binnen de eigen kernen blijft of niet.'
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling Configuratie',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u uw verbinding
            met uw servicebus.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'stuf_test_connection',
                description => 'Test verbinding naar opgegeven Service Bus'
            }
        ],
    },
    # trigger_definition  => {
    #     disable_subscription   => {
    #         method  => 'disable_subscription',
    #         #update  => 1,
    #     },
    # },
};


###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG - StUF configuration

=head1 SYNOPSIS

=head2 stuf_test_connection

=cut

sub stuf_test_connection {
    my $self                        = shift;
    my $interface                   = shift;

    ### Connect with our StUF ServiceBus server.
    $self->test_host_port(
        $interface->get_interface_config->{mk_async_url}
    ) if $interface->get_interface_config->{mk_async_url};

    $self->test_host_port(
        $interface->get_interface_config->{mk_sync_url}
    ) if $interface->get_interface_config->{mk_sync_url};

    ### Success
    return 1;
}

=head1 DESCRIPTION

STUF Configuration for StUF related queries

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

package Zaaksysteem::Backend::Sysin::Modules::SAMLSP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with 'Zaaksysteem::Backend::Sysin::Modules::Tests::SAML';

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'samlsp';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_cert',
        type        => 'file',
        label       => 'Public key van het PKIoverheid certificaat',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_key',
        type        => 'file',
        label       => 'Private key van het PKIoverheid certificaat',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contact_id',
        type        => 'spot-enlighter',
        label       => 'Contactpersoon',
        description => 'Zoek hier naar een medewerker die dienst doet als contactpersoon voor de <abbr title="Identity Provider">IdP</abbr>',
        data => {
            restrict => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label => 'naam',
            resolve => 'id'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_webservice',
        type        => 'text',
        label       => 'Webdienst base URL',
        required    => 1,
        description => 'Dit is de URL die gebruikt wordt in de SAML Protocol Exchange. Omdat Zaaksysteem mogelijk vanaf meerdere URLs benaderd kan worden is het van belang er een vast te zetten. Controleer dat het domein in deze URL gelijk is aan het domein waarvoor PKIoverheid certificaten zijn afgegeven',
        data => {
            placeholder => 'http://mijn.gemeente.nl/auth/saml'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_application_name',
        type        => 'text',
        label       => 'Applicatie naam',
        required    => 1,
    ),
];

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'SAML 2.0 Service Provider',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => ['text'],
    is_multiple => 0,
    is_manual => 1,
    retry_on_error => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface => 0,
    test_interface => 1,
    # Commented out because of mid-merge compat
    # test_definition => {
    #     description => qq|
    #         Om te controleren of het Zaaksysteem correct is geconfigureerd
    #         als SAML Service Provider kunt u hieronder een aantal tests
    #         uitvoeren. Hiermee controleert u of wij voor Identity Providers
    #         bereikbaar zijn.
    #     |,
    #     tests => [
    #         {
    #             id => 1,
    #             label => 'Test configuratie',
    #             name => 'instantiation_test',
    #             method => 'test_sp'
    #         }
    #     ]
    # }
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig( %{ MODULE_SETTINGS() } );
};

1;

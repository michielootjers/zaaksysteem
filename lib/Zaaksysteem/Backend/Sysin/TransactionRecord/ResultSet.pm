package Zaaksysteem::Backend::Sysin::TransactionRecord::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'DBIx::Class::ResultSet';

Params::Profile->register_profile(
    method  => 'transaction_record_create',
    profile => {
        required => [qw/
            transaction_id
            input
        /],
        optional => [qw/
            output
            is_error
            date_executed
        /],
        constraint_methods => {
            transaction_id         => qr/^\d+$/,
        },
    }
);

sub transaction_record_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    return $self->create($opts);
}


1;

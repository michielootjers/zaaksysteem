package Zaaksysteem::Backend::Sysin::TransactionRecordToObject::ResultSet;

use Moose;

use JSON;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::ResultSet';

has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            date_deleted     => undef,
        }
    }
);

=head2 transaction_record_to_object_create

Add a TransactionRecordToObject entry to the database.

=head3 Arguments

=over

=item transaction_record_id [required]

The transaction record this belongs to.

=item local_table [required]

The table it influenced.

=item local_id [required]

The ID of the record it influenced.

=item mutations [optional]

A JSON hash containing the changes made.

=back

=head3 Returns

A newly created Sysin::TransactionRecordToObject object.

=cut

Params::Profile->register_profile(
    method  => 'transaction_record_to_object_create',
    profile => {
        required => [qw/
            transaction_record_id
            local_table
            local_id
        /],
        optional => [qw/
            mutation_type
            mutations
        /],
        constraint_methods => {
            mutation_type => qr/(update|create|delete|unchanged)/,
            transaction_record_id => qr/^\d+$/,
        },
    }
);

sub transaction_record_to_object_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    # Check if the record actually exists
    my ($record) = $self->result_source->schema
        ->resultset($opts->{local_table})
        ->find({id => $opts->{local_id}});

    if (!$record) {
        throw '/sysin/transaction_record_to_object/record_not_found', sprintf(
            "Record with ID %d in class '%s' not found.",
            ($opts->{local_id}, $opts->{local_table})
        );
    }

    # Convert mutations to json
    if ($opts->{mutations} && ref($opts->{mutations})) {
        $opts->{mutations}  = JSON
                            ->new
                            ->utf8
                            ->convert_blessed
                            ->encode($opts->{mutations})
    }

    return $self->create($opts);
}

=head2 search_filtered

Does a search with a given filter. Input is as you would pass to
DBIx::Class.

B<Filters>

=cut

Params::Profile->register_profile(
    method  => 'search_filtered',
    profile => {
        required => [],
        optional => [qw/
            transaction_record_id
            local_table
            local_id
            date_deleted
        /],
        defaults => {
            'me.date_deleted' => undef,
        }
    }
);

sub search_filtered {
    my $self    = shift;
    my $options = assert_profile(shift || {})->valid;

    return $self->search(
        $options,
    );
}


1;

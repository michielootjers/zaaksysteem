package Zaaksysteem::Backend::Sysin::Transaction::Role::KCC;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Sysin::Transaction::Role::KCC - Role for KCC transactions

=head1 SYNOPSIS

  $transaction->apply_roles()->TO_JSON();

=head1 DESCRIPTION

Role that can be applied to KCC transactions, to insert some extra data into
their JSON representation.

=head1 METHOD_MODIFIERS

=head2 around _build__json_data

Wrapper around _build__json_data that inserts some extra information (like the
relevant ContactData object) into the generated JSON data.

=cut

around '_build__json_data' => sub {
    my $orig = shift;
    my $self = shift;

    my $json_data = $self->$orig(@_);

    $json_data->{betrokkene} = [
        map { $_->as_hashref } @{ $self->betrokkene }
    ];

    return $json_data;
};

=head2 betrokkene

Read-only accessor method to get the ContactData record for a KCC transaction.

=cut

sub betrokkene {
    my $self = shift;

    my @contact_data = $self->result_source->schema->resultset('ContactData')->find_by_phonenumber(
        $self->processor_params->{phonenumber}
    )->all;

    my @rv;
    for my $cd (@contact_data) {
        my $betrokkene_model = $self->result_source->schema->betrokkene_model;

        push @rv, $betrokkene_model->get({}, $cd->identifier);
    }

    return \@rv;
}


1;

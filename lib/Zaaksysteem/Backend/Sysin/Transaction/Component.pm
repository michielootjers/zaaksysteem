package Zaaksysteem::Backend::Sysin::Transaction::Component;

use Moose;
use Moose::Util qw/apply_all_roles/;

use JSON;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::Component';

my %INTERFACE_MODULE_ROLE_MAP = (
    'kcc' => ['Zaaksysteem::Backend::Sysin::Transaction::Role::KCC'],
);

=head1 NAME

Zaaksysteem::Backend::Sysin::Transaction::Component - Transaction Component

=head1 SYNOPSIS

 $row->transaction_update(\%params)     # Updates a transaction

 $row->transaction_delete(\%params)     # Deletes a transaction

=head1 DESCRIPTION

These methods provides transaction specific actions for our CRUD interface

=head1 ATTRIBUTES

=head2 _json_data

ISA: HashRef

Defines the json_data for this row.

=cut

has '_json_data'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'builder'   => '_build__json_data',
);

sub _build__json_data {
    my $self                    = shift;

    unless (DateTime->can('TO_JSON')) {
        no strict 'refs';
        *DateTime::TO_JSON          = sub { shift->iso8601 };
        use strict;
    }

    my $pub_info = {
        id                      => $self->id,
        input_data              => $self->input_data,
        input_file              => $self->input_file,
        date_created            => $self->date_created,
        date_deleted            => $self->date_deleted,
        date_last_retry         => $self->date_last_retry,
        date_next_retry         => $self->date_next_retry,
        automated_retry_count   => $self->automated_retry_count,
        interface_id            => $self->interface->TO_JSON({ignore_errors => 1 }),
        processed               => $self->processed,
        error_count             => $self->error_count,
        error_fatal             => $self->error_fatal,
        success_count           => $self->success_count,
        total_count             => $self->total_count,
        external_transaction_id => $self->external_transaction_id,
        direction               => $self->direction,
        state                   => $self->state,
        processor_params        => $self->processor_params,
        result_preview          => $self->preview_data
    };

    return $pub_info;
}

=head2 METHODS

=head2 apply_roles

Check if there's a role for the Transaction using the interface module name,
and apply if if there is.

=cut

sub apply_roles {
    my ($self) = shift;

    my $type = $self->interface->module;

    if (exists $INTERFACE_MODULE_ROLE_MAP{$type}) {
        apply_all_roles($self, @{ $INTERFACE_MODULE_ROLE_MAP{$type} });
    }

    return $self;
}

=head2 transaction_update

Updates a transaction entry in the database.

=head3 Arguments

=over

=item interface_id

=item external_transaction_id

The identifier in the originating system. (The calling party)

=item automated_retry_count

Set the number of automated retries already done. Defaults to none.

=item input_data && input_file

Either input_data (raw XML, for example) or input_file (a CSV-file) is required. Setting both is not a good idea.

=item date_last_retry

Last time a retry attempt was done.

=item date_next_retry

When the transaction-runner needs to try executing the request again. Defaults to now() so it will be executed for the first time.

=back

=head3 Returns

A newly created Sysin::Transaction object.

=cut

Params::Profile->register_profile(
    method  => 'transaction_update',
    profile => {
        required => [qw/
        /],
        optional => [qw/
			interface_id
			external_transaction_id
	        automated_retry_count
        	input_data
        	input_file
        	date_last_retry
        	date_next_retry
        /],
        constraint_methods => {
        	interface_id 		  => qr/\d+/,
        	automated_retry_count => qr/\d+/,
        },
    }
);

sub transaction_update {
	my $self = shift;
    my $opts = assert_profile($_[0])->valid;
    return $self->update($opts);
}

=head2 transaction_delete()

Will mark this transaction as deleted

=cut

sub transaction_delete {
    my $self    = shift;

    $self->transaction_records->update({
        date_deleted => DateTime->now,
    });
    $self->update({date_deleted => DateTime->now});

    return [];
}

=head2 transaction_retry()

Mark this transaction for retry.

=cut

sub transaction_retry {
    my $self    = shift;

    return if (
        $self->error_fatal ||
        $self->date_deleted
    );

    return $self->update({date_next_retry => DateTime->now});
}

sub process {
    my $self    = shift;

    ### Reprocesses this transaction
    $self->interface_id->module_object->process_transaction($self);
}

sub get_processor_params {
    my $self = shift;
    return $self->processor_params;
}

sub state {
    my $self                        = shift;

    ### Returns one of 3 different states:
    ### success
    ### error
    ### pending
    if ($self->processed && !$self->error_count && !$self->error_fatal) {
        return 'success';
    }

    if ($self->date_next_retry && !$self->processed && !$self->error_fatal) {
        return 'pending';
    }

    return 'error';

}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

package Zaaksysteem::Backend::Mailer;

use Email::Sender::Simple qw/ sendmail /;
use Moose;

=head2 send

Wrap around a selected sendmail function. This way we can easily
pass the mailer as an object, which allows us to pass a mock object
in the same way.

=cut

sub send {
    my $self = shift;

    return sendmail(@_);
}

1;

package Zaaksysteem::Backend::BibliotheekSjablonen::Component;
use strict;
use warnings;

use File::Spec::Functions qw(catfile);
use File::Temp;
use Moose;
use OpenOffice::OODoc;
use Zaaksysteem::Constants qw(SJABLONEN_TARGET_FORMATS MIMETYPES_ALLOWED);
use Zaaksysteem::Tools;
use Zaaksysteem::ZTT;
use JSON::XS;

with 'MooseX::Log::Log4perl';

extends 'DBIx::Class';

define_profile file_create => (
    required => [qw/case name subject/],
    optional => [qw/target_format case_document_ids/]
);

=head2 file_create(%options)

my $file = $sjabloon->file_create(
    target_format     => $format,
    name              => $name,
    case_document_ids => $ids,
    case              => $case,
    subject           => $subject,
);

Create a file from a template.

=head3 ARGUMENTS

=over

=item target_format [OPTIONAL] odt or pdf, default to odt

=item name          [REQUIRED] Document name

=item case_document_ids  [OPTIONAL] Case document ID's

=item case          [REQUIRED] Zaaksysteem case

=item subject       [REQUIRED] subject

=back

=head3 RETURNS

An Zaaksysteem::Schema::File object when succesful.
Dies in case of failures.

=cut

sub file_create {
    my ($self, $opts) = @_;
    $opts = assert_profile($opts)->valid;

    my $t0 = Zaaksysteem::StatsD->statsd->start;


    if ($self->interface_id) {
        return $self->create_file_from_external_template($opts);
    }

    my $target_format = $self->_assert_target_format($opts->{target_format} || 'odt');
    my $tmp_dir = $self->_tmp_dir();
    my $tmp_name = $opts->{name} . '.' . $target_format;
    my $tmp_file = catfile($tmp_dir, $tmp_name);

    $self->_filestore_operations({
        file   => $tmp_file,
        dir    => "$tmp_dir",
        format => $target_format,
        case   => $opts->{case},
    });

    my %optional;
    if ($opts->{case_document_ids}) {
        $optional{case_document_ids} = [$opts->{case_document_ids}];
    }

    my $generator = $self->_generator_string(
        $target_format,
        MIMETYPES_ALLOWED
    );

    my %db_params = (
        case_id    => $opts->{ case }->id,
        created_by => $opts->{ subject }
    );

    if ($generator) {
        $db_params{ generator } = $generator;
    }

    my $create_hash = {
        db_params         => \%db_params,
        file_path         => $tmp_file,
        name              => $tmp_name,
        publish_type_name => 'private',
        %optional
    };

    if ($opts->{case}->is_in_phase('registratie_fase') || $opts->{case}->is_in_phase('afhandel_fase')) {
        $create_hash->{db_params}{accepted} = 1;
    }

    if (!-f $tmp_file) {
        throw(
            '/sjablonen/file_create/general_exception',
            "Could not find file $tmp_file",
        );
    }
    my $file = $self->result_source->schema->resultset('File')->file_create($create_hash);
    if (!$file) {
        throw(
            '/sjablonen/file_create/general_exception',
            "Could not create file $tmp_file",
        );
    }

    unlink($tmp_file);

    Zaaksysteem::StatsD->statsd->end('sjablonen.file_create.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('sjablonen.file_create', 1);

    return $file;
}

=head2 create_file_from_external_template

Creating the file at our remote location

=cut

define_profile create_file_from_external_template => (
    required => [qw/case name subject/],
    optional => [qw/target_format case_document_ids/]
);

sub create_file_from_external_template {
    my ($self, $opts) = @_;
    $opts = assert_profile($opts)->valid;

    if (!$self->get_column('interface_id')) {
        throw("template/inteface/undefined", "Unable to generate template, no interface defined");
    }

    # Switch after testing
    my $interface = $self->interface_id;
    if ($interface->is_active) {
        my %params = (
            case                   => $opts->{case}->id,
            document_title         => $opts->{name},
            template_external_name => $self->template_external_name,
            subject                => $opts->{subject}->subject->_table_id,
            case_document_ids      => $opts->{case_document_ids}
        );

        my $transaction;
        if ($interface->get_interface_config->{spoofmode}) {
            $self->log->trace("Spoofmode enabled with params" . dump_terse(\%params));
            $transaction = $interface->process_trigger('spoofmode', \%params);
        } else {
            $self->log->trace("Creating file at remote location with params:" . dump_terse(\%params));
            $transaction = $interface->process_trigger('create_file_from_template', \%params);
        }

        my $transaction_record = $transaction->records(
            undef,
            { order_by => { '-desc' => 'id' } }
        )->first;

        if ($transaction->success_count) {
            if ($interface->module eq 'xential') {
                return decode_json($transaction_record->output);
            }
            else {
                return $transaction->processor_params->{result};
            }
        }
        else {
            return $transaction_record->output;
        }
    }
    else {
        $self->log->error("No active interface found with ID " . $self->get_column('interface_id'));
        throw("template/inteface/not_active", "Unable to generate template, interface inactive");
    }


}
=head2 _assert_target_format

my $bool = $self->_assert_target_format($format);

Check if target_format is correct.

=head3 RETURNS

Dies if you supply an incorrect format.

=cut

sub _assert_target_format {
    my ($self, $target_format) = @_;

    if (!defined $target_format) {
        throw(
            '/sjablonen/file_create/general_exception',
            "target_format is undefined."
        );
    }

    $target_format = lc($target_format);
    if (!SJABLONEN_TARGET_FORMATS->($target_format)) {
        throw(
            '/sjablonen/file_create/general_exception',
            "target_format '$target_format' is unsupported for sjabloon conversion."
        );
    }
    return $target_format;

}

define_profile _filestore_operations => (
    required => [qw/file dir format case/],
    constraint_methods => {
        format => SJABLONEN_TARGET_FORMATS
    },
    typed    => {
        format => 'Str',
        file   => 'Str',
        dir    => 'Str',
        case   => 'Zaaksysteem::Schema::Zaak',
    },
);

sub _filestore_operations {
    my ($self, $opts) = @_;
    $opts = assert_profile($opts)->valid;

    # TODO: Actually check the mime-type of the file
    # Zaaksysteem::DocumentValidator - perhaps the frontend could do this.
    if ($opts->{format} eq 'odt') {
        my $ztt = Zaaksysteem::ZTT->new(cache => $opts->{ case }->_ztt_cache);

        $ztt->add_context($opts->{case});

        my $odf_document = $self->_get_odf_document_handle({tmpdir => $opts->{dir}});
        my $document = $ztt->process_template($odf_document)->document;
        $document->save($opts->{file});
    }
    else {
        $self->filestore->convert({
            target_format => $opts->{format},
            target_file   => $opts->{file},
            case_id       => $opts->{case}->id,
            magic_strings_convert => 1,
        });
    }
    return 1;
}

sub _tmp_dir {
    my ($self) = @_;

    return File::Temp->newdir();
}

define_profile _get_odf_document_handle => (
    required => [qw(tmpdir)],
    typed    => { tmpdir => 'Str' },
);

sub _get_odf_document_handle {
    my ($self, $options) = @_;
    $options = assert_profile($options)->valid();

    my $tmp_dir = $options->{tmpdir};

    # Add in params checking later
    my ($filestore) = $self->filestore;

    my $encoding = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    odfWorkingDirectory($tmp_dir);

    my $path = $filestore->ustore->getPath($filestore->uuid);

    unless($path) {
        throw(
            '/sjablonen/file_create/general_exception',
            "not able to retrieve path for filestore entry: ". $filestore->uuid
        );
    }

    unless(-r $path) {
        throw(
            '/sjablonen/file_create/general_exception',
            "could not get read from path: ". $path
        );
    }

    my $document = odfDocument(
        file            => $path,
        local_encoding  => $encoding,
    );

    # check if document is found. a likely explanation for not finding it is moving a database
    # without moving the file structure with it
    unless($document) {
        throw(
            '/sjablonen/file_create/general_exception',
            sprintf("Could not get handle on odfDocument with filestore UUID %s [%s]", $filestore->uuid, $path)
        );
    }

    return $document;
}

=head2 _generator_string

Generates a "PKG/VERSION" string for the given format.

=cut

sub _generator_string {
    my $self = shift;
    my $format = shift;
    my $allowed = shift;

    if ($format eq 'odt') {
        return sprintf("zaaksysteem/%s", $Zaaksysteem::VERSION // '0')
    }

    my $spec = $allowed->{ sprintf('.%s', $format) };

    return unless $spec->{ conversion };

    # Hard-coded version strings valid at time of writing, polling these
    # would incur too much overhead, or rely on dpkg.
    return {
        xpstopdf     => 'libgxps/0.2.2-1',
        xhtml2pdf    => 'xhtml2pdf/3.0.32',
        jodconverter => 'libreoffice/3.5.7-0ubuntu9',
        none         => 'libreoffice/3.5.7-0ubuntu9',
        imagemagick  => 'imagemagick/6.6.9.7-5ubuntu3.3'
    }->{ $spec->{ conversion } };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

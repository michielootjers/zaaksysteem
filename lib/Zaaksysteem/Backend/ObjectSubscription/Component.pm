package Zaaksysteem::Backend::ObjectSubscription::Component;

use Moose;
use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Component';

has '_json_data'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;

        unless (DateTime->can('TO_JSON')) {
            no strict 'refs';
            *DateTime::TO_JSON          = sub { shift->iso8601 };
            use strict;
        }

        my $pub_info = {
            id          => $self->id,
            local_table => $self->local_table,
            local_id    => $self->local_id,
            date_create    => $self->date_created,
            date_deleted   => $self->date_deleted,
            external_id    => $self->external_id,
            object_preview => $self->object_preview,
            interface_id   => ($self->interface_id ? $self->interface_id->TO_JSON({ignore_errors => 1 }) : undef),
        };

        return $pub_info;
    },
);

=head2 object_subscription_delete()

Will mark this record as deleted

=cut

sub object_subscription_delete {
    my $self        = shift;

    ### Make sure we call the proper interface
    my $interface   = $self->interface_id;

    ### Will make sure a call will be send to the outside world
    $interface->process_trigger(
        'disable_subscription',
        {
            subscription_id     => $self,
        },
    );

    # $self->update({
    #     date_deleted => DateTime->now,
    # });
    return 1;
}

1;
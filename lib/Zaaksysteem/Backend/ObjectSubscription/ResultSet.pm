package Zaaksysteem::Backend::ObjectSubscription::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::ResultSet';


has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            date_deleted     => undef,
        }
    }
);

=head2 object_subscription_create

Creates a ObjectSubscription entry.

=head3 Arguments

=over

=item interface_id [required]
=item local_table [required]
=item local_id [required]
=item external_id [required]

=back

=head3 Returns

A newly created Sysin::Transaction object.

=cut

Params::Profile->register_profile(
    method  => 'object_subscription_create',
    profile => {
        required => [qw/
            interface_id
            local_table
            local_id
            external_id
        /],
        optional => [qw/
            object_preview
        /],
        constraint_methods => {
            interface_id => qr/^\d+$/,
            local_table  => qr/^\w+$/,
            local_id     => qr/^\d+$/,
        },
    }
);

sub object_subscription_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    # Check if the record actually exists
    my ($record) = $self->result_source->schema
        ->resultset($opts->{local_table})
        ->find({id => $opts->{local_id}});

    if (!$record) {
        throw '/sysin/object_subscription_create/record_not_found', sprintf(
            "Record with ID %d in class '%s' not found.",
            ($opts->{local_id}, $opts->{local_table})
        );
    }

    return $self->create($opts);
}

=head2 search_filtered

Does a search on transactions with a given filter. Input is as you would pass to
DBIx::Class.

B<Filters>

=cut

Params::Profile->register_profile(
    method  => 'search_filtered',
    profile => {
        required => [],
        optional => [qw/
            interface_id
            local_table
            local_id
        /],
        constraint_methods => {
            is_error => qr/(0|1)/,
        },
        defaults => {
            'me.date_deleted' => undef,
        }
    }
);

sub search_filtered {
    my $self    = shift;
    my $options = assert_profile(shift || {})->valid;

    return $self->search(
        $options,
        {
            prefetch    => ['interface_id'],
            order_by    => {-desc => 'me.date_created'},
        }
    );
}

=head1 INTERNAL METHODS

=head2 _prepare_options

Allows Moose Roles to work on the options given to a create or update call.

=cut

sub _prepare_options { shift; return shift; }


1;

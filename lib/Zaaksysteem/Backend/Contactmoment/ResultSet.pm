package Zaaksysteem::Backend::Contactmoment::ResultSet;

use strict;
use warnings;

use Moose;
use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::Contactmoment::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Contactmoment::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Contactmoment::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

=head2 contactmoment_create

TBA

=cut

Params::Profile->register_profile(
    method  => 'contactmoment_create',
    profile => {
        required => [qw/
            type
            created_by
            medium
        /],
        optional => [qw/
            subject_id
            message
            case_id
        /],
        constraint_methods => {
            type => qr/(email|note)/,
        },
        require_some => {
            subject_or_case => [1, qw/subject_id case_id/],
        },
    }
);

sub contactmoment_create {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/contactmoment/contactmoment_create/invalid_parameters',
            error => "Invalid options given: @invalid"
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/contactmoment/contactmoment_create/missing_parameters',
            error => "Missing options: @missing"
        );
    }

    my $cm = $self->create({
        subject_id => $opts->{subject_id} || undef,
        case_id    => $opts->{case_id} || undef,
        created_by => $opts->{created_by},
        medium     => $opts->{medium},
        type       => $opts->{type},
    });

    my $result;
    if ($opts->{type} eq 'email') {
        $cm->create_email({
            %{ $opts->{email} },
        });

        $self->result_source->schema->resultset('Logging')->trigger('email/send', {
            component => 'email',
            component_id  => $cm->id,
            zaak_id       => $opts->{case_id} || undef,
            betrokkene_id => $opts->{subject_id} || undef,
            data => {
                case_id             => $opts->{case_id} || undef,
                recipient           => $opts->{email}{recipient},
                subject             => $opts->{email}{subject},
                contactmoment_id    => $cm->id,
                attachments         => $opts->{email}{attachments} || undef,
                content             => $opts->{email}{body}
            }
        });
    }
    elsif ($opts->{type} eq 'note') {
        $cm->create_note({message => $opts->{message}});
    }
    return $cm->discard_changes;
}

1;

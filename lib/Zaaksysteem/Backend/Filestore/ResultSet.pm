package Zaaksysteem::Backend::Filestore::ResultSet;

use strict;
use warnings;

use ClamAV::Client;
use Digest::MD5::File qw(file_md5_hex);
use File::Basename;
use File::MimeInfo::Magic;
use File::stat;
use Params::Profile;
use Moose;
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::FileType' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Filetype exception',
        alias       => 'throw_filetype_exception',
    },
);

=head2 filestore_create

Creates a Filestore entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added to the file storage.

=item original_name [required]

The name the file originally has or had. This is always required to prevent
issues where a file storage (uuid-based) file name gets used.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=back

=head3 Returns

The newly created Filestore object.

=cut

Params::Profile->register_profile(
    method  => 'filestore_create',
    profile => {
        required => [qw/
            original_name
            file_path
        /],
        optional => [qw/
            ignore_extension
        /]
    }
);

sub filestore_create {
    my $self = shift;
    my $opts = $_[0];

    # Check database parameters
    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/filestore/filestore_create/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/filestore/filestore_create/missing_parameters',
            error => "Missing options: @missing",
        );
    }

    my $db_params;
    my $file_path = $opts->{file_path};

    $self->clamscan($file_path);

    # Check if the filetype is allowed
    unless ($opts->{ignore_extension}) {
        $self->assert_allowed_filetype($opts->{original_name});
    }

    # Open the file and add it to the filestore
    open(my $file, $file_path) or die "Unable to open file: $file_path";
    my $store = $self->ustore;
    my $uuid  = $store->add($file);
    $db_params->{uuid} = $uuid;

    # File database properties
    my $stat = stat($file);
    $db_params->{md5}      = file_md5_hex($file_path);
    $db_params->{size}     = $stat->size;
    $db_params->{mimetype} = mimetype($file_path);
    $db_params->{original_name} = $opts->{original_name};

    my $create = $self->create($db_params);

    return $create->discard_changes;
}


=head2 clamscan($path)

Scan a new file, throw exception if it contains a virus.

=cut

sub clamscan {
    my ($self, $path) = @_;

    throw('filestore/clamscan/file_not_found', "Bestand niet gevonden: $path")
        unless -e $path;

    my $scanner = ClamAV::Client->new();

    throw("filestore/clamscan/scanner_not_available", "Virusscanner niet operationeel")
        if not defined($scanner) or not $scanner->ping();

    open my $file, "<", $path or
        throw('filestore/clamscan/file_not_found', 'Bestand niet gevonden');

    my $virus = $scanner->scan_stream($file);

    close $file;

    throw("filestore/clamscan/virus_found", "Virus aangetroffen in bestand, niet toegevoegd.")
        if $virus;
}


sub find_by_uuid {
    shift->search({ uuid => shift })->first;
}

=head2 ustore

Get a File::UStore object.

=cut

sub ustore {
    my $self = shift;
    my ($path) = $self->result_source->schema->resultset('Config')
        ->get_value('filestore_location');

    require File::UStore;
    my $store = File::UStore->new(
        path     =>  $path,
        prefix   => 'zs_',
        depth    => 5,
    );
    return $store;
}

=head2 $self->assert_allowed_filetype($filename)

Check whether or not a filetype is allowed within the file storage.

Returns true or error.

=cut

sub assert_allowed_filetype {
    my $self = shift;
    my ($name) = @_;

    my ($extension) = $self->get_file_extension($name);

    if (!$extension) {
        throw_filetype_exception(
            code  => '/filestore/assert_allowed_filetype/no_extension',
            error => "File does not have an extension: $name",
        );
    }

    # Match against lowercase
    $extension = lc($extension);

    if (!MIMETYPES_ALLOWED->{$extension}) {
        throw(
            '/filestore/assert_allowed_filetype/extension_not_allowed',
            "Bestandstype niet toegestaan: $extension",
        );
    }
    return 1;
}


=head2 get_file_extension

Central utility method to determine the file extension based on a file path.
Looks for a dot (.) and 1 to 5 characters at the end of the given path.

E.g. /tmp/something/sub/filename.ext => yields '.ext'

Return file extension including the dot (.)

=cut

sub get_file_extension {
    my ($self, $path) = @_;

    my ($extension) = $path =~ qr/(\.[0-9A-Za-z]{1,5}$)/;

    return $extension;
}


=head2

Send a document through the JODconverter. Typically requires these parameters:

 Content      => $to_be_converted_data,
 Content_Type => $mimetype_of_source_format,
 Accept       => $mimetype_of_target_format,

=cut

sub send_to_converter {
    my $self = shift;
    my %params = @_;

    my $converter_url = $self->result_source->schema->resultset('Config')->get_value('jodconverter_url');

    use HTTP::Request::Common;
    my $ua = LWP::UserAgent->new;
    my $result = $ua->request(POST $converter_url,
        %params,
    );
    return $result->content;
}

1;

package Zaaksysteem::Backend::File::MailDissector;

use Moose::Role;
use Zaaksysteem::OutlookMailParser;
use Data::Dumper;
use Zaaksysteem::Constants;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;


=head2 handle_accepted

Outlook emails will be converted to MIME.

MIME mails will be dissected into parts, these parts
will be added to the case in a folder.

=cut

sub  handle_accepted {
    my ($self, $arguments) = @_;

    my $properties = $arguments->{properties} or die "need properties";

    my $original_name = $self->filestore_id->original_name();
    my $schema = $self->result_source->schema;
    my $tmpDir = $schema->resultset('Config')->get_value('tmp_location');

    my $outlookMailParser = Zaaksysteem::OutlookMailParser->new({tmpDir => $tmpDir});

    my $body = $outlookMailParser->parseMSG({
        path            => $self->filestore_id->get_path(), 
        original_name   => $original_name
    });

    return unless $body;

    my $rs_directory = $schema->resultset('Directory');
    my $validified = $rs_directory->validify({
        case_id => $self->case->id,
        name => $original_name
    });

    my $directory = $rs_directory->directory_create({
        name    => $validified,
        original_name => $original_name,
        case_id => $self->case_id->id
    });

    # save the body as a PDF
    my $pdfPath = $self->saveBodyAsPDF($body);

    $self->addMIMEPartAsFile({
        directory_id    => $directory->id,
        path            => $pdfPath,
        name            => $body->{name} . '.pdf',
        subject         => $properties->{subject}
    });


    # then all the attachments
    foreach my $attachment (@{ $outlookMailParser->attachments }) {

        $self->addMIMEPartAsFile({
            directory_id    => $directory->id,
            path            => $attachment->{path},
            name            => $attachment->{name},
            subject         => $properties->{subject}
        });
    }

    # get rid of the original file, no sense in having duplicates.
    # yo dawg, i heard you like update_properties, so i put update_properties in your update_properties
    $self->update_properties({
        deleted => 1, 
        subject => $properties->{subject}
    });
}


sub saveBodyAsPDF {
    my ($self, $body) = @_;

    my $resultset = $self->result_source->schema->resultset('Filestore');
    my $filestore = $resultset->filestore_create({
        file_path        => $body->{path},
        original_name    => $body->{name},
    });

    my $extension = $resultset->get_file_extension($body->{path});

    return $filestore->convert_to_pdf($extension, $body->{path} . ".pdf", 0, $self->case->id);
}



=head2 addMIMEPartAsFile

insert a new file within the given directory

=cut

define_profile addMIMEPartAsFile => (
    required    => [qw[directory_id path subject name]]
);
sub addMIMEPartAsFile {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $path = $arguments->{path};

    my $filestore = $self->result_source->schema->resultset('Filestore');
    my $extension = $filestore->get_file_extension($path);

    return unless $extension && MIMETYPES_ALLOWED->{lc($extension)};

    my $resultset = $self->result_source->schema->resultset('File');

    my $file = $resultset->file_create({
        file_path => $path,
        name      => $arguments->{name},
        db_params => {
            directory_id    => $arguments->{directory_id},
            accepted        => 1,
            publish_website => 1,
            publish_pip     => 1,
            created_by      => $arguments->{subject},
            case_id         => $self->case_id->id,
        },
        metadata => {
            description => 'Onderdeel van een Outlook E-mail bestand',
        },
    });

}


1;
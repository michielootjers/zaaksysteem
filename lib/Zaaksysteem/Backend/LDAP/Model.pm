package Zaaksysteem::Backend::LDAP::Model;

use Moose;

use Net::LDAP;
use Zaaksysteem::Exception;

=head1 NAME

Zaaksysteem::Backend::LDAP::Model - Conveniently query a LDAP server

=head1 SYNOPSIS

This module abstracts some LDAP interactions for intuitive use within
Zaaksysteem.

=head1 ATTRIBUTES

=over 4

=item ldap

Holds a reference to a L<Net::LDAP> instance. Required as a parameter when
constructing via C<new()>.

=item base_dn

String serialization of the DN that should be used while querying. Required
argument.

=item ldapconfig

Configuration item, must contain at least a key C<rolefilter>. Should be a
L<sprintf>-compatible format with one C<%s> substring.

=item ldapcache

Reference to a thing that behaves like a HASH reference. Author needs to
supplement this info.

=back

=cut

has ldap        => ( is => 'ro', isa => 'Net::LDAP', required => 1 );
has base_dn     => ( is => 'ro', isa => 'Str', required => 1 );
has ldapconfig  => ( is => 'ro', isa => 'HashRef', required => 1 );
has ldapcache   => ( is => 'ro', isa => 'HashRef', required => 1 );

=head1 METHODS

=head2 find_user

=cut

sub find_user {
    my $self = shift;
    my $entity = shift;

    unless (eval { $entity->isa('Zaaksysteem::Model::DB::Subject'); }) {
        throw('ldap/find_user', sprintf(
            'Unable to find_user on a "%s" object',
            ref $entity
        ));
    }

    my $rs = $self->search(sprintf(
        '(&(objectClass=posixAccount)(cn=%s))',
        $entity->login_entity->source_identifier
    ));

    return $rs->pop_entry;
}

=head2 user_roles

=cut

sub user_roles {
    my $self = shift;
    my $user_entity = shift;

    return unless $user_entity;

    $self->ldapcache->{user_roles}     ||= {};
    if (
        defined($self->ldapcache->{user_roles}->{$user_entity->dn}) &&
        $self->ldapcache->{user_roles}->{$user_entity->dn}
    ) {
        return @{ $self->ldapcache->{user_roles}->{$user_entity->dn} };
    }

    my $user = $self->find_user($user_entity);

    my $rs = $self->search(
        sprintf($self->ldapconfig->{rolefilter}, $user->dn),
        attrs => [qw[cn]]
    );

    my @user_roles = map { $_->get_value('cn') } $rs->entries;

    $self->ldapcache->{user_roles}->{$user_entity->dn} = \@user_roles;

    return @user_roles;

}

=head2 get_roles

Convenience method that returns all C<posixGroup> objects where C<memberUid>
is the C<dn> of the supplied user entity object.

    my @roles = $c->model('LDAP')->get_roles($user_entity);

=cut

sub get_roles {
    my $self        = shift;
    my $user_entity     = shift;

    $self->ldapcache->{get_roles}     ||= {};
    if (
        defined($self->ldapcache->{get_roles}->{$user_entity->dn}) &&
        $self->ldapcache->{get_roles}->{$user_entity->dn}
    ) {
        return @{ $self->ldapcache->{get_roles}->{$user_entity->dn} };
    }

    my $user    = $self->find_user($user_entity);

    my @entries = $self->search(sprintf(
        '(&(objectClass=posixGroup)(memberUid=%s))',
        $user->dn
    ))->entries;

    $self->ldapcache->{get_roles}->{$user_entity->dn} = \@entries;

    return @entries;
}

=head2 user_ou

Returns a L<Net::LDAP::Entry> object matching the supplied user's C<ou>.

    my $ou_object = $c->model('LDAP')->user_ou($user_entity);

Takes one argument, which must be a L<Zaaksysteem::Backend::Auth::UserEntity::Component>
instance.

=cut

sub user_ou {
    my $self                        = shift;
    my $user_entity                 = shift;

    $self->ldapcache->{user_ou}     ||= {};
    if (
        defined($self->ldapcache->{user_ou}->{$user_entity->dn}) &&
        $self->ldapcache->{user_ou}->{$user_entity->dn}
    ) {
        return $self->ldapcache->{user_ou}->{$user_entity->dn};
    }
    
    my $user = $self->find_user($user_entity);

    my ($ou) = $user->dn =~ m[ou=(.*?),];

    my $rs = $self->search(
        sprintf(
            '(&(objectClass=organizationalUnit)(ou=%s))',
            $ou
        ),
        base => $self->base_dn,
    );

    return ($self->ldapcache->{user_ou}->{$user_entity->dn} = $rs->pop_entry);
}

=head2 user_ous

This method allows for easy retrieval of the entire path up the OU tree for
a specific user. It works by decomposing the user's DN, and querying LDAP with
an OR query containing the individual OU names.

    my @ous = $c->model('LDAP')->user_ous($user_entity);

=cut

sub user_ous {
    my $self = shift;
    my $user_entity = shift;

    # Decompose the user DN into [ { ou => 'Dept' }, ... ]
    my @dn_parts = grep { exists $_->{ ou } }           # find all ou components
        map { { split m/=/, $_ } }                      # by splitting on ou=Dept
        split m/,/, $self->find_user($user_entity)->dn; # split on route components

    return $self->search(sprintf(
        '(&(objectClass=organizationalUnit)(|%s))',
        join('', map { sprintf('(ou=%s)', $_->{ ou }) } @dn_parts)
    ))->entries;
}

=head2 search

This is the main entry point to this model. It's not much more than a
convenient way of calling L<Net::LDAP>'s C<search> method. Prefills the
C<base_dn> key in the subcall with the C<base_dn> attribute value.

Returns a L<Net::LDAP::Search> object instance.

    my $rs = $c->model('LDAP')->search('(&(objectClass=organizationalUnit)(ou=Management)');

=cut

sub search {
    my $self = shift;
    my $filter = shift;

    my $rs = $self->ldap->search(
        filter => $filter,
        base => $self->base_dn,
        @_
    );

    return $rs;
}

__PACKAGE__->meta->make_immutable;

package Zaaksysteem::Backend::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'DBIx::Class::ResultSet';

=head1 NAME

Zaaksysteem::Backend::ResultSet - Generic ResultSet for _all_ schema files

=head1 SYNOPSIS

 $resultset->search_active  # Returns all active records, it depends on the
                            # table what active actually means

 $resultset->search_freeform('Jones')   # Find all zaaktype which include jones in
                                        # their description or title.

=head1 DESCRIPTION

These ResultSet methods provides some generic methods as an extension
of L<DBIx::Class>.

=head1 ATTRIBUTES

=head2 _active_params

ISA: HashRef

Defines the parameters which defines the activeness of a row, like a deleted is
unset or an active column which needs to be unset, see C<search_active> below

=cut

has '_active_params' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub { return {}; },
);

=head1 METHODS

=head2 $rs->find_active(\%SEARCH_PARAMS)

Return value: $component_row

This call depends on a set attribute: C<< $rs->_active_params >>. When set, it will
include these params in the actual search. This will return the first row, or throws
an error when it finds more than one row.

Example C<< $rs->_active_params >>

    has '_active_params' => (
        is          => 'ro',
        lazy        => 1,
        default     => sub {
            return {
                'me.active'     => 1,
                'me.deleted'    => undef,
            }
        }
    );

=cut

sub find_active {
    my $self        = shift;
    my ($where)     = @_;

    unless (UNIVERSAL::isa($where, 'HASH')) {
        throw(
            'backend/resultset/find_active/invalid_where',
            'Cannot run find_active without a parameter HASH like search_active'
        );
    }

    my $rs = $self->search_active(
        $where
    );

    if ($rs->count > 1) {
        throw(
            'backend/resultset/find_active/multiple_rows',
            'find_active returns more than one row, which is impossible'
        );   
    }

    return $rs->search->first;
}

=head1 METHODS

=head2 $rs->search_active(\%search_params, \%options)

Return value: $resultset

This call depends on a set attribute: C<< $rs->_active_params >>. When set, it will
include these params in the actual search.

Example C<< $rs->_active_params >>

    has '_active_params' => (
        is          => 'ro',
        lazy        => 1,
        default     => sub {
            return {
                'me.active'     => 1,
                'me.deleted'    => undef,
            }
        }
    );

=cut

sub search_active {
    my $self        = shift;

    my $active      = $self->search(
        {
            %{ $self->_active_params },
        },
        @_
    );

    return $active->search(@_);
}

=head2 search_freeform(\$text, \%options)

Return value: $resultset

 $rs->search_freeform('test', { prefetch => ['zaaktype_node_id']});

Searches within Searchable for freeform text. Will die when no searchable
column exists.

=cut

sub search_freeform {
    my $self        = shift;
    my $term        = shift || '';

    $term           =~ s/^\s+|\s+$//g;

    return $self->search_active(
        {
            search_term => { 'ilike' => '%' . lc($term) . '%' },
        },
        @_
    );
}

=head2 action_for_selection(\%selection_params);

Return value: 1 or L<Zaaksysteem::Exception>

=cut

Params::Profile->register_profile(
    method  => 'action_for_selection',
    profile => {
        required            => [qw/
            selection_type
        /],
        constraint_methods  => {
            selection_type      => qr/^all|subset$/,
        },
        dependencies        => {
            selection_type => {
                'subset'    => [qw/selection_id/],
            }
        }
    }
);

sub action_for_selection {
    my $self                = shift;
    my $trigger             = shift;
    my $ids                 = [];

    my $opts    = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    if (
        exists($opts->{selection_id}) &&
        $opts->{selection_id}
    ) {
        $ids        = (
            UNIVERSAL::isa($opts->{selection_id}, 'ARRAY')
                ? $opts->{selection_id}
                : [ $opts->{selection_id} ]
        );
    }

    my $rs      = $self->_action_by_selection_resultset(@{ $ids });

    my (%errored_rows);
    eval {
        $self->result_source->schema->txn_do(sub {
            while (my $row = $rs->next) {
                $errored_rows{ $row->id }   =
                    $row->TO_JSON;

                $row->$trigger; 
            }

            # die('rollback');
        });
    };


    if ($@) {
        $self->_throw_selection_error(
            {
                selection_type  => $opts->{selection_type},
                error_ids       => \%errored_rows,
                trigger         => $trigger,
            }
        );

    }

    my $completed_rs = $self->_action_by_selection_resultset(@{ $ids });

    return $completed_rs;
}

sub _throw_selection_error {
    my $self            = shift;
    my $opts            = shift;

    if ($opts->{selection_type} eq 'all') {
        throw(
            'zapi/selection/error',
            'Could not ' . $opts->{trigger} . ' complete resultset'
        );
    }

    my @errors;
    push(
        @errors,
        {
            is_error        => 1,
            selection_id    => $_,
            messages        => []
        }
    ) for keys %{ $opts->{error_ids} };

    throw(
        'zapi/selection/error',
        'Could not ' . $opts->{trigger} . ' subset',
        \@errors
    );
}

sub _action_by_selection_resultset {
    my $self                = shift;
    my @ids                 = @_;
    my $search_params       = {};

    if (scalar(@ids)) {
        $search_params->{'me.id'} = { -in => \@ids };
    }

    return $self->search($search_params);
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

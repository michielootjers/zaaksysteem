package Zaaksysteem::Profiles;

use strict;
use warnings;

use Data::Dumper;
use Zaaksysteem::Constants;

use utf8;

require Exporter;
our @ISA        = qw/Exporter/;
our @EXPORT     = qw/
    PROFILE_NOTITIE_CREATE
    PROFILE_JOB_CREATE
    PROFILE_DOCUMENT_CREATE
    PROFILE_JOB_UPDATE_KENMERK
    PROFILE_WIJZIG_BETROKKENE
    PROFILE_WIJZIG_ROUTE
    PROFILE_WIJZIG_FASE
    PROFILE_WIJZIG_STATUS
    PROFILE_BULK_UPDATE_SET_SETTINGS
    PROFILE_ZAAK_DOCUMENTS_ADD_MAIL

    ZAPI_PROFILES
/;


use constant 'PROFILE_NOTITIE_CREATE'     => {
    required => [ qw[medium subject_id] ],
    optional => [ qw[content type onderwerp] ],

    msgs => PARAMS_PROFILE_DEFAULT_MSGS,

    constraint_methods => {
        onderwerp => qr/^.{1,256}$/,
        contactkanaal => qr/^\w{1,128}$/,
    },

    require_some => {
        content_or_onderwerp => [1, qw/content onderwerp/],
    },

    field_filters => {
        medium => ['lc'],
    },
};

use constant 'PROFILE_JOB_CREATE'     => {
    'required'          => [qw/
        bericht
        contactkanaal
        betrokkene
        naam
        opts
        task
        task_context
    /],
    constraint_methods  => {
        'contactkanaal'   => qr/^\w{1,128}$/,
    },
    field_filters       => {
        contactkanaal   => ['lc'],
    },
    msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant 'PROFILE_JOB_UPDATE_KENMERK'     => {
    'required'          => [qw/
        kenmerk_id
        betrokkene
        zaak_id
    /],
    'optional'          => [qw/
        kenmerk_value
        toelichting
    /],
    constraint_methods  => {
        'kenmerk_id'        => qr/^\d+$/,
    },
    field_filters       => {
        kenmerk_value       => sub {
            my $field   = shift;

            if (UNIVERSAL::isa($field, 'ARRAY')) {
                my @results;
                for my $value (@$field) {
                    push(@results, ALLOW_ONLY_TRUSTED_HTML->($field));
                }

                return @results;
            }

            return ALLOW_ONLY_TRUSTED_HTML->($field);
        },
        toelichting         => ALLOW_ONLY_TRUSTED_HTML,
    },
    msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant 'PROFILE_DOCUMENT_CREATE'     => {
    'required'      => [qw/
        naam

        store_type
    /],
    'optional'      => [qw/
        id
        folder_id
        zaak_id
        milestone
        betrokkene_dsn
        zaaktype_kenmerken_id
        verplicht
        pip
        versie
        filestore_id
        notitie_id
        queue
        ontvangstdatum
        document_type
    /],
    constraint_methods => {
        'onderwerp'   => qr/^.{256}$/,
        'contactkanaal'   => qr/^\w{128}$/,
    },
    require_some        => {
        zaak_or_betrokkene  => [1, qw/betrokkene_dsn zaak_id/],
        minimal_one_store   => [1, qw/filestore_id notitie_id job_id/]
    },
    defaults            => {
        'document_type'     => 'Anders',
        'versie'            => 1,
        'ontvangstdatum'    => sub {
            return DateTime->now();
        },
        'milestone'         => sub {
            my ($dfv) = @_;

            return $dfv->get_filtered_data->{'milestone'}
                if (
                    defined($dfv->get_filtered_data->{'milestone'}) &&
                    $dfv->get_filtered_data->{'milestone'}
                );

            if (
                defined($dfv->get_filtered_data->{'zaak_id'}) &&
                $dfv->get_filtered_data->{'zaak_id'}
            ) {
               return 1;
            }

            return undef;
        }
    },
};

use constant PROFILE_WIJZIG_BETROKKENE  => {
    required    => ['betrokkene_identifier'],
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant PROFILE_WIJZIG_ROUTE           => {
    required            => [qw/route_ou route_role/],
    optional            => [qw/change_only_route_fields force/],
    constraint_methods  => {
        route_ou        => qr/^\d+/,
        route_role      => qr/^\d+/,
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant PROFILE_WIJZIG_FASE            => {
    required            => [qw/milestone/],
    constraint_methods  => {
        milestone           => qr/^\d+/,
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant PROFILE_BULK_UPDATE_SET_SETTINGS => {
    required => [qw//],
    optional => [qw/vernietigingsdatum_type vernietigingsdatum/],
    constraint_methods  => {
        vernietigingsdatum_type => sub {
            my ($dfv, $val) = @_;

            my $vernietigingsdatum = $dfv->{__INPUT_DATA}->{vernietigingsdatum};
            if($val eq 'termijn') {
                unless(
                    $vernietigingsdatum =~ /^\d{2}\-\d{2}\-\d{4}$/
                ) {
                    warn "returning undef";
                    return; 
                }
            }
            return 1;
        },
        vernietigingsdatum  => sub {
            my ($dfv, $val) = @_;

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};


use constant PROFILE_ZAAK_DOCUMENTS_ADD_MAIL => {
    required            => [qw/mailtype rcpt_type/],
    optional            => [qw/medewerker_betrokkene_id zaaktype_notificatie_id/],
    dependencies        => {
       rcpt_type => sub {
            my $dfv  = shift;
            my $type = shift;
    
            return [ 'medewerker_betrokkene_id' ] if ($type eq 'medewerker');
            return [ ];
        },    
       mailtype => sub {
            my $dfv  = shift;
            my $type = shift;
    
            return [ 'zaaktype_notificatie_id' ] if ($type eq 'bibliotheek_notificatie');
            return [ ];
        },    
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant ZAPI_PROFILES                  => {
    'helloworld.create'       => {
        required        => [qw/
            subject
            description
        /],
        constraint_methods  => {
            subject     => qr/^[\w\s]+$/,
            description => qr/^[\w\s]+$/,
        },
        msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
    },
    'helloworld.update'       => {
        required        => [qw/
            id
            subject
            description
        /],
        constraint_methods  => {
            subject     => qr/^[\w\s]+$/,
            description => qr/^[\w\s]+$/,
        },
        msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
    },
};


1;

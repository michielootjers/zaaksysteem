package Zaaksysteem::Zaken::RelationView;

use Moose;
use namespace::autoclean;

use constant RELATION_TYPES => {
    plain => 'Gerelateerd',
    continuation => 'Vervolgzaak',
    initiator => 'Initiatorzaak'
};

has 'relation' => ( is => 'rw', isa => 'Zaaksysteem::Backend::Case::Relation::Component' );

has 'case_id' => ( is => 'rw', isa => 'Int' );

has 'order_seq_gs' => (
    is => 'ro',
    isa => 'CodeRef',
    lazy => 1,
    traits => [ 'Code' ],
    handles => { order_seq => 'execute' },
    default => sub {
        my $self = shift;

        if($self->relation->get_column('case_id_a') eq $self->case_id) {
            return sub { $self->relation->order_seq_a(@_) };
        } else {
            return sub { $self->relation->order_seq_b(@_) };
        }
    }
);

has 'case_gs' => (
    is => 'ro',
    isa => 'CodeRef',
    lazy => 1,
    traits => [ 'Code' ],
    handles => { case => 'execute' },
    default => sub {
        my $self = shift;

        if($self->relation->get_column('case_id_a') eq $self->case_id) {
            return sub { $self->relation->case_id_a };
        } else {
            return sub { $self->relation->case_id_b };
        }
    }
);

has 'type_gs' => (
    is => 'ro',
    isa => 'CodeRef',
    lazy => 1,
    traits => [ 'Code' ],
    handles => { type => 'execute' },
    default => sub {
        my $self = shift;

        if($self->relation->get_column('case_id_a') eq $self->case_id) {
            return sub { $self->relation->type_a(@_) };
        } else {
            return sub { $self->relation->type_b(@_) };
        }
    }
);

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    die 'need exactly 2 arguments' unless scalar(@_) == 2;

    my $relation = shift;
    my $case_id = shift;

    $self->$orig({
        relation => $relation,
        case_id => $relation->get_column('case_id_a') eq $case_id ?
            $relation->get_column('case_id_b') :
            $relation->get_column('case_id_a')
    });
};

# Some passthru methods
sub update { shift->relation->update(@_); }
sub to_string { shift->relation->to_string(@_); }
sub id { shift->relation->id; }

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->relation->id,
        order => $self->order_seq,
        type => RELATION_TYPES->{ $self->type } // 'Onbekend',
        case => $self->case
    };
}

__PACKAGE__->meta->make_immutable;


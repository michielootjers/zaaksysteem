package Zaaksysteem::Zaken::DelayedTouch;

use Moose;
use Moose::Meta::Attribute::Native::Trait::Array;

use Zaaksysteem::Exception;

=head1 NAME

Zaaksysteem::Zaken::DelayedTouch - Defines a touch object, for delayed touching
of cases from an external location, like Catalyst

=head1 SYNOPSIS

    my $touch_object = Zaaksysteem::Zaken::DelayedTouch->new;

    $touch_object->add_case(4434);

    ### Later
    my $db_schema = $c->model('DB');

    $touch_object->execute($db_schema);

=head1 DESCRIPTION

Delayes touching and indexing of cases by collecting all case numbers and touching at
the end of the execution cycle.

=head1 ATTRIBUTES

=head2 cases

Return value: $ARRAY_CASE_NUMBERS

    print join(',', @{ $touch_object->cases });

    # 424,343,2424,13

Returns a list of case numbers.

=cut

has 'cases' => (
    is      => 'ro',
    isa     => 'ArrayRef[Num]',
    default => sub { [] },
);

=head2 METHODS

=head2 add_case($CASE_NUMBER)

Return value: $COUNT_CASES

    print $touch_object->add_case(44);

    # 1

Adds a case to the delayed object for later touching.

=cut

sub add_case {
    my $self                        = shift;
    my $case_id                     = shift;

    return scalar(@{ $self->cases }) unless $case_id =~ /^\d+$/;

    return scalar(@{ $self->cases }) if grep { $_ eq $case_id } @{ $self->cases };

    push(@{ $self->cases }, $case_id);

    return scalar(@{ $self->cases });
}

=head2 execute($SCHEMA)

Return value: $BOOL_SUCCESS

    $touch_object->execute($BOOL_SUCCESS);

Retrieves every single case in C<< $touch_object->cases >> from the database, and runs
C<< $row->_touch >> on it.

=cut

sub execute {
    my $self                        = shift;
    my $schema                      = shift;

    throw(
        'delayedtouch/invalid_schema',
        'Invalid schema given: ' . ref($schema)
    ) unless UNIVERSAL::isa($schema, 'Zaaksysteem::Schema');

    foreach my $id (@{ $self->cases }) {
        my $case = $schema->resultset('Zaak')->find($id);

        $case->_touch if $case;
    }

    return 1;
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;

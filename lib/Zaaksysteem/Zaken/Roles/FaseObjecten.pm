package Zaaksysteem::Zaken::Roles::FaseObjecten;

use Moose::Role;
use Data::Dumper;
use DateTime::Format::ISO8601;
use DateTime;
use Email::Valid;
use Zaaksysteem::Profile;
use Zaaksysteem::Exception;
use Zaaksysteem::ZTT;
use Zaaksysteem::Zaken::AdvanceResult;

with 'Zaaksysteem::Zaken::Roles::ZaakSetup';

sub zaak_cache {
    my $self           = shift;
    my $caching_object = shift;

    my $calling_sub = [caller(1)]->[3];

    if ($caching_object) {
        $self->{_zaak_cache} = {}
          unless $self->{_zaak_cache};

        return ($self->{_zaak_cache}->{$calling_sub} = $caching_object);
    }

    if ($self->{_zaak_cache}) {
        return $self->{_zaak_cache}->{$calling_sub}
          if $self->{_zaak_cache}->{$calling_sub};
    }

    return;
}

sub flush_cache {
    my $self            = shift;

    return unless $self->{_zaak_cache};

    delete($self->{_zaak_cache});
}

sub _set_fase {
    my $self        = shift;
    my $milestone   = shift;

    return unless $milestone;

    $self->milestone($milestone);
    $self->flush_cache;
    $self->update;
}


sub set_volgende_fase {
    my $self            = shift;

    my $volgende_fase   = $self->volgende_fase;
    return unless $volgende_fase;

    my $milestone = $volgende_fase->status;

    $self->_set_fase($milestone) or return;

    if ($self->is_afhandel_fase) {
        $self->set_gesloten;
    } else {
        $self->logging->trigger('case/update/milestone', {
            component => 'zaak',
            data => {
                case_id => $self->id,
                phase_id => $self->huidige_fase->id
            }
        });
    }

    $self->flush_cache;
    return 1;
}


sub set_vorige_fase {
    my $self            = shift;

    my $vorige_fase     = $self->vorige_fase;

    return unless $vorige_fase;

    return unless $self->can_vorige_fase;

    if ( $self->is_afgehandeld) {
        $self->set_heropen;
    }

    $self->flush_cache;
    $self->_set_fase($vorige_fase->status);
}

sub set_heropen {
    my $self            = shift;

    return unless grep { $self->status eq $_ }
        qw/
            resolved
            overdragen
            stalled
        /;

    if ( $self->status =~ /resolved|overdragen/ ) {
        $self->afhandeldatum        (undef);
        $self->vernietigingsdatum   (undef);
    }

    $self->wijzig_status({status => 'open'});

    $self->logging->trigger('case/reopen', { component => 'zaak', data => {
        case_id => $self->id
    }});

    $self->flush_cache;
    $self->update;
}

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_OPTIONS/;


sub set_gesloten {
    my  $self   = shift;
    my  $time   = shift;

    $time       ||= DateTime->now;

    $self->afhandeldatum        ($time);

    ### Get vernietigingsdatum
    $self->set_vernietigingsdatum;

    if (
        $self->status ne 'resolved' &&
        $self->status ne 'overdragen'
    ) {
        $self->wijzig_status({status => 'resolved'});
    }

    $self->flush_cache;

    $self->logging->trigger('case/close', { component => 'zaak', data => {
        case_id => $self->id,
        timestamp => $time->datetime,
        case_result => $self->resultaat,
    }});

    $self->update;
}

=head2 set_vernietigingsdatum

Apply a destruction date on a case.

If there is no result the destruction date is 1 year in the future.
If there is a result the vernietigingsdatum will be set according to the rules of the resulttype.

=head3 RETURNS

undef if there is no 'afhandeldatum'.
Returns a DateTime object if successful.
Dies in case of an error.

=cut

sub set_vernietigingsdatum {
    my $self = shift;
    my $afhandeldatum = $self->afhandeldatum;

    return undef if !$afhandeldatum;

    my $afhandel_clone = $afhandeldatum->clone();

    # Geen resultaat: 1 year default
    unless ($self->resultaat) {
        return $self->vernietigingsdatum($afhandel_clone->add(years => 1));
    }

    my $resultaat = $self->zaaktype_node_id->zaaktype_resultaten->search(
        {resultaat => $self->resultaat}
    )->single;

    if (!$resultaat) {
        throw(
            'case/set_vernietigingsdatum',
            sprintf("Unable to find resultaat '%s' while it is defined", $self->resultaat),
        );
    }

    my $dt = $afhandeldatum->clone();
    $dt->add(days => $resultaat->bewaartermijn);

    if (ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$resultaat->bewaartermijn} eq 'Bewaren'
     && $self->status ne 'overdragen') {
        $self->wijzig_status({status => 'overdragen'});
    }

    my $vd = $self->vernietigingsdatum;
    if (!$vd || $dt ne $vd) {
        $self->vernietigingsdatum($dt);
        $self->logging->trigger('case/update/purge_date', {
                component => 'zaak',
                data      => {
                    purge_date => $dt->dmy,
                    case_id    => $self->id,
                },
            }
        );
    }
    return $self->vernietigingsdatum;
}


sub fasen {
    my $self    = shift;

    return $self->zaaktype_node_id->zaaktype_statussen(
        undef,
        {
            order_by    => { -asc   => 'status' }
        }
    );
}


sub huidige_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => $self->milestone,
    })->first);
}


sub volgende_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => ($self->milestone + 1)
    })->first);
}


sub vorige_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => ($self->milestone - 1)
    })->first);
}


sub registratie_fase {
    my $self    = shift;

    # TODO: no return $self->zaak_cache here?

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search(
        undef,
        {
            order_by    => { -asc => 'status' },
            rows        => 1,
        }
    )->first)
}


sub afhandel_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search(
        undef,
        {
            order_by    => { -desc => 'status' },
            rows        => 1,
        }
    )->first);
}

=head2 is_in_phase($phase)

my $bool = $case->is_in_phase('registratie_fase');

Check if a zaak is in the specified fase (phase).

=head3 ARGUMENTS

=over

=item phase [REQUIRED]

=back

=head3 RETURNS

A boolean, 1 if true, 0 if false

=cut

sub is_in_phase {
    my ($self, $phase) = @_;

    if (!$self->can($phase)) {
        throw(
            'case/phase',
            "Unable to determine phase '$phase' for $self"
        );
    }

    return $self->$phase->status eq $self->huidige_fase->status;
}

sub is_afhandel_fase {
    my $self    = shift;
    return $self->is_in_phase('afhandel_fase');

    if ($self->afhandel_fase->status eq $self->huidige_fase->status) {
        return 1;
    }

    return;
}

sub is_afgehandeld {
    my $self    = shift;

    return 1 if ($self->status eq 'resolved');
    return 1 if ($self->status eq 'overdragen');

    if ($self->afhandel_fase->status eq $self->milestone) {
        return 1;
    }

    return;
}


sub is_open {
    my $self    = shift;

    return 1 if ($self->status =~ /new|open/);
    return;
}



sub is_volgende_afhandel_fase {
    my $self    = shift;

    return unless $self->volgende_fase;

    if ($self->afhandel_fase->status eq $self->volgende_fase->status) {
        return 1;
    }

    return;
}


=head2 can_volgende_fase

Determine wether the case is ready for the next move. This routine is expanded (using
around) throughout the other roles for the class. The results are bundled together in
an object, as to allow specific feedback on which items are not ready yet.

=cut

sub can_volgende_fase {
    my $self    = shift;

    my $advance_result = new Zaaksysteem::Zaken::AdvanceResult;

    unless($self->is_volgende_afhandel_fase && !$self->resultaat) {
        $advance_result->result_complete(1);
    }

    return $advance_result;
}

before can_volgende_fase => sub {
    my $self = shift;
    $self->log->debug("!! Start can_volgende_fase");
};

after can_volgende_fase => sub {
    my $self = shift;
    $self->log->debug("End can_volgende_fase");
};


sub can_vorige_fase {
    my $self    = shift;

    return 1;
}


=head2 advance

Move to the next phase, or already in last phase, close case.
Then perform phase transition actions.

This sub is written to accomodate for the following scenario:
- a subcase if finished
- it signals its parent that it's finished
- the parent case is advanced to the next phase
- all default phase actions, as configured in the 'zaaktype' are fired.

=cut

sub advance {
    my ($self, $opts) = @_;

    # evil; to be EXTERMINATED asap!
    my $c = $opts->{context} or die 'need context ($c) object';

    my $advance_result = $self->can_volgende_fase;

    unless ($advance_result->can_advance) {
        throw(
            'case/advance',
            'Unable to advance case due to can_volgende_fase checks',
            $advance_result
        );
    }

    $self->send_external_system_messages;

    $self->set_volgende_fase;

    return $self->fire_phase_actions({ context => $c });
}


=head2 send_external_system_messages

Before advancing, see if the rule demand messages to be sent in this phase.
Currently only Buitenbeter is implemented.

To test:
Create a casetype with a external message rule.
Create a case.
Create an interface.

$case->send_external_system_messages;

=cut

sub send_external_system_messages {
    my $self = shift;

    my $rules_result = $self->execute_rules({status => $self->milestone + 1});

    if ($rules_result && $rules_result->{send_external_system_message}) {
        my $message = $rules_result->{send_external_system_message};

        # the rule is designed to be more generic, since only buitenbeter is implemented
        # i hardcoded it. other code needs to be written to make other interfaces work
        # anyway, so the benefit of complete genericity would be very limited.
        my $interface = $self->result_source->schema->resultset('Interface')->search_active({
            module => 'buitenbeter'
        })->first or throw("api/buitenbeter", "Buitenbeter koppeling is niet beschikbaar");

        $interface->process_trigger('PostStatusUpdate', {
            case_id => $self->id,
            kenmerken => $self->field_values,
            statusText => $message->{message},
            statusCode => $message->{status},
        });
    }
}


sub fire_phase_actions {
    my ($self, $options) = @_;

    my $c = $options->{context} or die "need context";

    my $actions_rs = $self->case_actions_cine->current->active->sorted;

    my @flash_messages;

    my $allocation_action = 0;

    while(my $action = $actions_rs->next()) {
        $allocation_action ||= $action->type eq 'allocation';
        push(@flash_messages, $self->fire_action({
            context => $c,
            action  => $action,
        }));
    }

    my $redirect = $allocation_action > 0 || $self->is_afgehandeld;
    return {
        redirect_to_dashboard => $redirect,
        flash_messages => \@flash_messages
    };
}


=head2 fire_action

Return 1 when allocation action performed - redirect to dashboard is necessary

=cut

sub fire_action {
    my ($self, $options) = @_;

    my $action  = $options->{action} or die "need action";
    my $context = $options->{context} or die "need context";

    my $type    = $action->type or die "need action type";

    if($type eq 'email') {
        return $self->mail_action({
            context         => $context,
            case_action     => $action,
        });
    } elsif($type eq 'template') {
        $self->template_action({
            context         => $context,
            case_action     => $action,
        });

        return "Sjabloon aangemaakt";
    } elsif($type eq 'allocation') {
        $self->allocation_action({
            ou_id   => $action->data->{ou_id},
            role_id => $action->data->{role_id},
            change_only_route_fields => $options->{change_only_route_fields},
        });

        return "Zaak toegewezen";
    } elsif($type eq 'case') {
        my $subcase_event = $self->start_subcase({
            context => $context,
            case_action => $action,
        });

        return $subcase_event->onderwerp;
    }

    return "ID-10T Error";
}

sub start_subcase {
    my ($self, $arguments) = @_;

    my $case_action = $arguments->{case_action} or die "need case_action";
    my $c           = $arguments->{context}     or die "need context";


    # odd, but some of the fields differ in naming (e.g. automatisch_behandelen)
    my $settings    = $case_action->data;
    my $action_data = $case_action->data;

    # these are the exceptions and the checks
    $settings->{ou_id}                      = $action_data->{ou_id} or die "need ou_id";
    $settings->{role_id}                    = $action_data->{role_id} or die "need role_id";
    $settings->{type_zaak}                  = $action_data->{relatie_type} or die "need relatie_type";
    $settings->{aanvrager_type}             = $action_data->{eigenaar_type} or die "need eigenaar_type";
    $settings->{actie_kopieren_kenmerken}   = $action_data->{kopieren_kenmerken};
    $settings->{zaaktype_id}                = $action_data->{relatie_zaaktype_id} or die "need relatie_zaaktype_id";
    $settings->{actie_automatisch_behandelen} = $action_data->{automatisch_behandelen};

    # If you create an automatic subcase in the first phase (registration), and the case
    # is created without setting the behandelaar, there's a doom scenario. There's a few workarounds,
    # this one at least created the subcase. When the case gets a behandelaar, the behandelaar is responsible
    # for taking ownership of that case.
    # A better fix is to make it impossible to create this scenario, however that is a substantial change
    # in Zaaktypebeheer, and hardly feasible without the current workflow.
    if($settings->{aanvrager_type} eq 'behandelaar' && !$self->behandelaar) {
        $c->push_flash_message("Deelzaak kon niet worden aangemaakt met 'behandelaar = aanvrager' omdat behandelaar niet is ingesteld.");
        $settings->{aanvrager_type} = 'aanvrager';
    }

    $settings->{onderwerp} = $self->onderwerp;

    my $subcase = $c->model('DB::Zaak')->create_relatie(
        $self, # fishy ## noshit
        %$settings
    );

    $subcase->fire_phase_actions({ context => $c });

    if($settings->{required} && $settings->{relatie_type} eq 'deelzaak') {
        $self->register_required_subcase({
            subcase_id              => $subcase->id,
            required                => $settings->{required},
            parent_advance_results  => $settings->{parent_advance_results},
        });
    }

    return $self->logging->trigger('case/subcase', { component => 'zaak', data => {
        subcase_id => $subcase->id,
        type => $action_data->{ relatie_type }
    }});
}






sub mail_action {
    my ($self, $options) = @_;

    my $c               = $options->{context}       or die "need context";
    my $case_action     = $options->{case_action}   or die "need case_action";

    my $action_data = $case_action->data;

    if(my $send_date = $action_data->{send_date}) {

        my $send_date_dt = $action_data->{schedule_test} ?
            DateTime->from_epoch(epoch => $send_date) :
            DateTime::Format::ISO8601->parse_datetime($send_date);

        $self->result_source->schema->resultset('ScheduledJobs')->create_zaak_notificatie({
            bibliotheek_notificaties_id => $action_data->{bibliotheek_notificaties_id},
            scheduled_for   => $send_date_dt,
            recipient_type  => $action_data->{rcpt},
            behandelaar     => $action_data->{behandelaar},
            email           => $action_data->{email},
            zaak_id         => $self->id,
        });

        return "E-mail ingepland";

    } else {
        my $prepared_notification = $self->prepare_notification({
            recipient_type  => $action_data->{rcpt},
            behandelaar     => $action_data->{behandelaar},
            email           => $action_data->{email},
            context         => $c,
            body            => $action_data->{body},
            subject         => $action_data->{subject},
            case_document_attachments => $action_data->{case_document_attachments},
        });

        $c->log->debug("Zaak::FaseObjecten: sending mail directly:" . Dumper $prepared_notification);

        # Since we're abusing the controller for now, make sure the relevant
        # case is on the stash. This is very dangerous but the alternative has
        # a lot of impact. BTW, alternative is under construction and will
        # be merged into the 2014-July release.
        my $saved_case = $c->stash->{zaak};
        $c->stash->{zaak} = $case_action->case_id;
        $c->forward("/zaak/mail/send", [$prepared_notification, { no_messages => 1}]);
        $c->stash->{zaak} = $saved_case;
    }

    return "E-mail verstuurd";
}


sub template_action {
    my ($self, $options) = @_;

    my $c               = $options->{context}       or die "need context";
    my $case_action     = $options->{case_action}   or die "need case_action";

    my $action_data = $case_action->data;

    my $bibliotheek_sjablonen_id = $action_data->{bibliotheek_sjablonen_id}
        or die "need bibliotheek_sjablonen_id";

    my $sjabloon = $self->result_source->schema->resultset('BibliotheekSjablonen')->find($bibliotheek_sjablonen_id)
        or die "need sjabloon";

    my $case_sjabloon = $self->zaaktype_node_id->zaaktype_sjablonen->search({
        bibliotheek_kenmerken_id => $action_data->{bibliotheek_kenmerken_id}
    })->single;

    my %file_create_opts = (
        name          => $sjabloon->filestore_id->name_without_extension,
        case          => $self,
        subject       => $self->_get_subject($c),
        target_format => $action_data->{target_format} || $case_sjabloon->target_format,
    );

    if($action_data->{ bibliotheek_kenmerken_id }) {
        my $case_type_attribute = $self->result_source->schema->resultset('ZaaktypeKenmerken')->search(
            zaaktype_node_id => $self->get_column('zaaktype_node_id'),
            bibliotheek_kenmerken_id => $action_data->{ bibliotheek_kenmerken_id }
        )->first;

        if ($case_type_attribute) {
            $file_create_opts{ case_document_ids } = $case_type_attribute->id;
        }
    }

    $sjabloon->file_create(\%file_create_opts);
}


sub _get_subject {
    my ($self, $c) = @_;

    ### Only when user exists...when from the outside, this is possible a
    ### case create. And we use the aanvrager key.
    my $can = eval { $c->can('user_exists') };
    if ($can && $c->user_exists) {
        return $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => 'medewerker',
            },
            $c->user->uidnumber,
        )->betrokkene_identifier;
    }
    return $self->aanvrager_object->betrokkene_identifier;
}



#
# For every phase a new allocation can be automatically set.
#
sub allocation_action {
    my ($self, $options) = @_;

    my $role_id = $options->{role_id}   or die "need role_id";
    my $ou_id   = $options->{ou_id}     or die "ou_id";

    # Next time? There won't be no next time
    my $volgende_fase = $self->volgende_fase;

    if (!$volgende_fase) {
        throw("ZS/Z/R/FO", "Aint no next phase, no point");
    }

    $self->wijzig_route({
        route_ou    => $ou_id,
        route_role  => $role_id,
        change_only_route_fields => $options->{ change_only_route_fields }
    });
}

=head2 notification_recipient

Determine recipient for a zaaktype_notification.

Four cases:
- Aanvrager:    zaak->aanvrager
- Coordinator:  zaak->coordinator
- Behandelaar:  betrokkene(behandelaar betrokkene_id)->email
- Overige:      notificatie->email (incl. magic string feature)

Catalyst context is necessary for Sjablonen and Betrokkene.

=cut

sub notification_recipient {
    my ($self, $options) = @_;

    #my $context         = $options->{context}           or die "need context";
    my $recipient_type  = $options->{recipient_type}    or die "need recipient_type";
    my $behandelaar     = $options->{behandelaar};      # optional
    my $email           = $options->{email};            # optional

    my $betrokkene_sub = sub {
        my ($betrokkene_id) = $behandelaar =~ /(\d+)$/;

        my $betrokkene_object = $self->result_source->schema->betrokkene_model->get({
                extern  => 1,
                type => 'medewerker'
            },
            $betrokkene_id
        );

        return $betrokkene_object->email;
    };

    my $dispatch_table = {
        aanvrager   => sub { $self->aanvrager_object->email },
        behandelaar => $betrokkene_sub,
        medewerker  => $betrokkene_sub,
        coordinator => sub { $self->coordinator_object->email },
        overig      => sub {
            my $ztt = Zaaksysteem::ZTT->new;
            $ztt->add_context($self);

            return $ztt->process_template($email)->string;
        },
    };

    # TODO: JW will look at this
    my $to;
    eval {
        $to = $dispatch_table->{$recipient_type}->();
        die "E-mail address is not defined" if !defined $to;
        die "Invalid e-mail address: $to" unless Email::Valid->address($to);
    };

    if($@) {
        $self->log->error("Could not find recipient for $recipient_type: $@");
    }

    return $to;
}


=head2 prepare_notification

Based on a number of input parameters, preprocess the notification.
Purpose is a singular way to process this.

Returns a structure with body, subject and to parameters.

=cut

define_profile prepare_notification => (
    required => [qw/recipient_type body subject/],
    optional => [qw/behandelaar subject case_document_attachments email/]
);
sub prepare_notification {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    return {
        body            => Encode::encode("utf-8", $params->{body}),
        subject         => $params->{subject},
        to              => $self->notification_recipient($params),
        recipient_type  => $params->{recipient_type},
        case_document_attachments => $params->{case_document_attachments},
    };
}

1;



=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


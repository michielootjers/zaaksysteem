package Zaaksysteem::Zaken::Roles::HStore;
use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::HStore - Role that adds hstore column updating ability.

=head1 METHODS

=head2 update_hstore

Update the "hstore_properties" column with the value of the
C<object_attributes> attribute.

=cut

sub update_hstore {
    my $self = shift;
    return if $ENV{DISABLE_HSTORE_UPDATES};
    
    my $case_object = $self->object_data;
    return if not $case_object;

    $case_object->class_uuid($self->zaaktype_id->_object->uuid);

    $self->clear_object_attributes();
    $case_object->replace_object_attributes(@{ $self->object_attributes });

    $self->_update_object_relationships($case_object);
    $self->_update_object_acl($case_object);

    $case_object->reload_index;
    $case_object->update;
}

sub _update_object_acl {
    my $self = shift;
    my $case_object = shift;

    my $acl = $self->result_source->schema->resultset('ObjectAclEntry');

    $self->result_source->schema->txn_do(sub {
        $case_object->object_acl_entries->delete_all;

        my @entries;

        # Iterator over aanvrager, coordinator and behandelaar objects
        # Skip unless defined, or skip when btype != medewerker
        # Store in hashmap indexed on betrokkene_identifier, so we don't
        # generate double entries
        my %subjects = map { $_->betrokkene_identifier => $_ } grep
            { defined && $_->btype eq 'medewerker' }
            (
                $self->aanvrager_object,
                $self->coordinator_object,
                $self->behandelaar_object
            );

        # Iterator over involved subjects, creating appropriate
        # ACL entries along the way.
        for my $security_id (keys %subjects) {
            push @entries, $case_object->grant(
                $subjects{ $security_id }, qw[read write]
            );
        }

        # Next we're going to fix the confidentiality, search for all the
        # auth roles *not* matching the current confidentiality state of the
        # case, and proscribe the capabilities described there.
        # TODO: Calculate A \ B complement and proscribe only items
        # exlusively in set A or B depending on confidentiality state
        my $auths = $self->zaaktype_id->zaaktype_authorisations->search({
            confidential => $self->confidentiality ne 'confidential'
        });

        my %map = ( zaak_edit => 'write', zaak_read => 'read' );

        for my $auth ($auths->all) {
            next unless exists $map{ $auth->recht };

            $case_object->proscribe($auth, $map{ $auth->recht });
        }
    });
}

sub _update_object_relationships {
    my $self = shift;
    my ($case_object) = @_;

    my $schema = $self->result_source->schema;

    # Delete all relationships (don't worry, we're going to re-create them).
    $schema->resultset('ObjectRelationships')->search({object_id => $case_object->uuid})->delete;

    # Parent/children
    my $parent = $self->pid;
    if($parent && !$parent->is_deleted) {
        my $parent_object = $parent->object_data;
        $schema->resultset('ObjectRelationships')->create(
            {
                object1_uuid => $parent_object,
                type1        => 'parent',

                object2_uuid => $case_object,
                type2        => 'child',
            },
        );
    }

    my $children = $self->zaak_children->search;
    while(my $child = $children->next) {
        next if $child->is_deleted;

        my $child_object = $child->object_data;
        $schema->resultset('ObjectRelationships')->create(
            {
                object1_uuid => $case_object,
                type1        => 'parent',

                object2_uuid => $child_object,
                type2        => 'child',
            },
        );
    }

    # Other case relationships
    my $case_relations = $schema->resultset('CaseRelation')->search({ case_id => $self->id });
    while (my $cr = $case_relations->next) {
        if ($cr->get_column('case_id_a') == $self->id) {
            # We're A
            my $b_object = $cr->case_id_b->object_data;
            next if not $b_object;

            $schema->resultset('ObjectRelationships')->create(
                {
                    object1_uuid => $case_object->id,
                    type1        => $cr->type_a,

                    object2_uuid => $b_object,
                    type2        => $cr->type_b,
                },
            );
        }
        else {
            # We're B
            my $a_object = $cr->case_id_a->object_data;
            next if not $a_object;

            $schema->resultset('ObjectRelationships')->create(
                {
                    object1_uuid => $a_object,
                    type1        => $cr->type_a,

                    object2_uuid => $case_object->id,
                    type2        => $cr->type_b
                },
            );
        }
    }

    return;
}

1;

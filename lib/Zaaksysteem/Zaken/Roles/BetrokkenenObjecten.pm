package Zaaksysteem::Zaken::Roles::BetrokkenenObjecten;

use Moose::Role;
use Data::Dumper;

with 'Zaaksysteem::Zaken::Betrokkenen';

use Zaaksysteem::Constants;
use Zaaksysteem::Betrokkene;
use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

has 'aanvrager_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return unless $self->aanvrager;

        return $self->_load_betrokkene_object(
            $self->aanvrager
        );
    }
);

has 'ontvanger_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;
        my $ontvanger   = $self->zaak_betrokkenen->ontvanger;

        return unless $ontvanger;

        return $self->_load_betrokkene_object(
            $ontvanger
        );
    }
);

has 'behandelaar_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return unless $self->behandelaar;

        return $self->_load_betrokkene_object(
            $self->behandelaar
        );
    }
);

has 'coordinator_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return unless $self->coordinator;

        return $self->_load_betrokkene_object(
            $self->coordinator
        );
    }
);

has betrokkene_model => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->result_source->schema->resultset('Zaak')
            ->betrokkene_model;
    }
);

=head2 pip_authorized_betrokkenen

Returns the list of "betrokkene"-objects related to this case that have the
pip_authorized flag set.

=cut

sub pip_authorized_betrokkenen {
    my $self = shift;

    return map {
        $self->_load_betrokkene_object($_)
    } $self->zaak_betrokkenen->search({pip_authorized => 1})->all;
}

has 'ou_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return unless $self->route_ou;

        return $self->result_source->schema->resultset('Groups')->search(
            {
                id => $self->route_ou,
            }
        )->first;
    }
);

Params::Profile->register_profile(
    'method'    => 'betrokkene_object',
    'profile'   => {
        'optional'      => [qw/
        /],
        'require_some'  => {
            'magic_string_or_rol'   => [
                1,
                'magic_string_prefix',
                'rol',
                'betrokkene_id'
            ],
        },
    }
);

sub betrokkene_object {
    my ($self, $opts)   = @_;
    my $dv              = Params::Profile->check(params => $opts);
    my $search          = {};

    die('Parameters incorrect:' . Dumper($dv)) unless $dv->success;

    if ($opts->{magic_string_prefix}) {
        $search->{magic_string_prefix}  = $opts->{magic_string_prefix};
    } elsif ($opts->{rol}) {
        $search->{rol}                  = $opts->{rol};
    } else {
        $search->{id}                   = $opts->{betrokkene_id};
    }

    my $betrokkene = $self->zaak_betrokkenen->search(
        $search
    );

    return unless $betrokkene->count == 1;

    $betrokkene     = $betrokkene->first;

    if (
        $self->{_betrokkene_object_cache} &&
        $self->{_betrokkene_object_cache}->{$betrokkene->id}
    ) {
        return $self->{_betrokkene_object_cache}->{$betrokkene->id};
    }

    $self->{_betrokkene_object_cache} = {} unless
        $self->{_betrokkene_object_cache};


    return $self->{_betrokkene_object_cache}->{ $betrokkene->id }
        = $self->_load_betrokkene_object(
            $betrokkene
        );
}

=head2 get_zaak_betrokkenen

Returns a search of C<ZaakBetrokkenen>, for the supplied C<search> parameters.

=cut

sub get_zaak_betrokkenen {
    my $self = shift;

    return $self->zaak_betrokkenen->search(
        { deleted => undef }
    )->search(@_);
}

=head2 get_betrokkene_objecten

Returns an array of C<Betrokkene::Object> instances, for all C<ZaakBetrokken>
rows matched by the supplied C<search> parameters.

=cut

sub get_betrokkene_objecten {
    my $self = shift;

    my $betrokkenen_rs = $self->get_zaak_betrokkenen(@_);

    my @rv;
    while (my $betrokkene = $betrokkenen_rs->next) {
        if (!$self->{_betrokkene_object_cache}->{ $betrokkene->id }) {
            $self->{_betrokkene_object_cache}->{ $betrokkene->id } =
                $self->_load_betrokkene_object(
                    $betrokkene
                );
        }

        push @rv, $self->{_betrokkene_object_cache}->{ $betrokkene->id };
    }

    return @rv;
}

sub set_coordinator {
    my $self        = shift;
    my $identifier  = shift;

    my $bo          = $self->result_source
        ->schema
        ->resultset('Zaak')
        ->betrokkene_model;

    ## Set betrokkene-TYPE-ID
    my $betrokkene_ident  = $bo->set($identifier);

    ### Retrieve betrokkene ID from ident (GMID-ID)
    my ($gm_id, $betrokkene_id) = $betrokkene_ident =~ /(\d+)-(\d+)$/;

    $self->_betrokkene_delete($self->coordinator);

    $self->coordinator($betrokkene_id);
    $self->coordinator_gm_id($gm_id);
    $self->update;
};

sub set_behandelaar {
    my $self        = shift;
    my $identifier  = shift;

    $self->assert_assignee(betrokkene_id => $identifier);

    my $schema = $self->result_source->schema;
    my $bo = $schema->resultset('Zaak')->betrokkene_model;

    ## Set betrokkene-TYPE-ID
    my $betrokkene_ident  = $bo->set($identifier);

    ### Retrieve betrokkene ID from ident (GMID-ID)
    my ($gm_id, $betrokkene_id) = $betrokkene_ident =~ /(\d+)-(\d+)$/;

    $self->_betrokkene_delete($self->behandelaar);

    $self->behandelaar($betrokkene_id);
    $self->behandelaar_gm_id($gm_id);

    # Check messages and assign them to the new behandelaar.
    my $messages = $schema->resultset('Message')->search(
        {
            'logging.zaak_id' => $self->id,
        },
        {
            join => 'logging'
        }
    );
    $messages->update({subject_id => $identifier});

    $self->update;
};



sub set_aanvrager {
    my $self        = shift;
    my $identifier  = shift;

    ### Check if aanvrager is same type as identifier
    my $btype;
    if ($self->aanvrager_object) {
        $btype = $self->aanvrager_object->btype;
    }

    if ($btype && $identifier !~ /$btype/) {
        $self->log->warn("Identifier is not correct");
        return;
    }

    my $bo = $self->betrokkene_model;

    ## Set betrokkene-TYPE-ID
    my $betrokkene_ident  = $bo->set($identifier);

    ### Retrieve betrokkene ID from ident (GMID-ID)
    my ($gm_id, $betrokkene_id) = $betrokkene_ident =~ /(\d+)-(\d+)$/;

    ### Delete current betrokkene
    $self->_betrokkene_delete($self->aanvrager);

    $self->aanvrager($betrokkene_id);
    $self->aanvrager_gm_id($gm_id);
    $self->update;
    $self->discard_changes;


    if ($self->aanvrager_object->can('uuid')) {
        my $schema = $self->result_source->schema;
        my $subject_br = Zaaksysteem::BR::Subject->new(
            schema => $schema,
        );

        my $subject = $subject_br->find($self->aanvrager_object->uuid);
        $subject_br->enable_subscription($subject);
    }

    $self->_betrokkene_zaak_id($self->aanvrager);
};

sub _betrokkene_zaak_id {
    my $self        = shift;
    my $betrokkene  = shift;

    return unless $betrokkene;

    $betrokkene->zaak_id($self->id);
    $betrokkene->update;
}

sub _betrokkene_delete {
    my $self        = shift;
    my $betrokkene  = shift;

    return unless $betrokkene;

    $betrokkene->deleted(DateTime->now());
    $betrokkene->update;
}

sub _load_betrokkene_object {
    my $self    = shift;
    my $object  = shift;

    my $searchid        = $object->id;
    my $searchintern    = 1;

    if (
        $object->betrokkene_type eq 'org_eenheid'
    ) {
        $searchid = $object->gegevens_magazijn_id;
        $searchintern = 0;
    }

    return $self->result_source->schema->resultset('Zaak')->betrokkene_model->get(
        {
            'intern'    => $searchintern,
            'type'      => $object->betrokkene_type,
        }, $searchid
    );
}

after '_handle_logging' => sub {
    my $self            = shift;

    my $changed_data    = $self->_get_latest_changes;

    my @types           = qw/behandelaar coordinator aanvrager/;

    for my $type (@types) {
        if (exists($changed_data->{$type})) {
            if (!$changed_data->{$type} && $changed_data->{_is_insert}) {
                next;
            }

            $self->trigger_logging('case/relation/update', { component => 'betrokkene', data => {
                subject_name => $self->$type ? $self->$type->naam : '&lt;Geen betrokkene&gt;',
                subject_relation => ucfirst(lc($type))
            }});
        }
    }

    return $changed_data;

};


sub is_betrokkene_compleet {
    my $self    = shift;

    return 1 if $self->behandelaar;
    return;
}


around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    if($self->is_betrokkene_compleet) {
        $advance_result->ok('owner_complete');
    } else {
        $advance_result->fail('owner_complete');
    }

    return $advance_result;
};

define_profile betrokkene_relateren => (%{ BETROKKENE_RELATEREN_PROFILE() });

sub betrokkene_relateren {
    my $self = shift;

    my $opts = assert_profile(shift)->valid;
    my $identifier = shift;

    my $bo = $self->result_source->schema->resultset('Zaak')->betrokkene_model;

    my $magic_string_prefix =
        $self->betrokkenen_relateren_magic_string_suggestion(
            $opts
        ) or return;

    # Don't add duplicated
    my $current_betrokkenen = $self->zaak_betrokkenen->search(
        {
            '-and' => [
                { deleted => undef },
                {
                    '-or' => [
                        { magic_string_prefix   => $magic_string_prefix },
                        { rol                   => $opts->{rol} }
                    ]
                }
            ]
        }
    );

    return if $current_betrokkenen->count;

    my $retval;

    my $schema = $self->result_source->schema;

    eval {
        $schema->txn_do(sub {

            ## Set betrokkene-TYPE-ID
            my $betrokkene_ident = $bo->set($opts->{betrokkene_identifier});

            ### Retrieve betrokkene ID from ident (GMID-ID)
            my ($gm_id, $betrokkene_id) = $betrokkene_ident =~ /(\d+)-(\d+)$/;

            ### Retrieve betrokkene_id from database and manipulate
            my $betrokkene = $schema->resultset('ZaakBetrokkenen')->find($betrokkene_id);
            if (!$betrokkene) {
                throw('betrokkene/find', "No betrokkene found with id $betrokkene_ident");
            }

            $betrokkene->zaak_id($self->id);
            $betrokkene->verificatie('medewerker');
            $betrokkene->rol($opts->{rol});
            $betrokkene->magic_string_prefix($magic_string_prefix);
            $betrokkene->pip_authorized($opts->{pip_authorized} // '' eq '1' ? 1 : 0);
            $betrokkene->update;

            $self->update;

            $retval = $self->trigger_logging(
                'case/subject/add',
                {
                    component => LOGGING_COMPONENT_ZAAK,
                    data      => {
                        case_id         => $self->id,
                        case_subject_id => $betrokkene->id,
                        subject_id      => $betrokkene_id,
                        subject_name    => $betrokkene->naam,
                        role            => $opts->{rol},
                        params          => $opts
                    }
                }
            );
        });
    };

    if ($@) {
        warn('There was a problem creating this betrokkene:' .
            $@
        );
        return;
    }

    return $retval;
}

Params::Profile->register_profile(
    'method'    => 'betrokkenen_relateren_magic_string_suggestion',
    'profile'   => {
        'optional'      => [qw/
        /],
        'require_some'  => {
            'magic_string_or_rol'   => [
                1,
                'magic_string',
                'rol',
            ],
        },
    }
);

sub betrokkenen_relateren_magic_string_suggestion {
    my $self            = shift;
    my $opts            = shift;

    my $dv              = Params::Profile->check(params => $opts);

    die('Parameters incorrect:' . Dumper($dv)) unless $dv->success;

    ### Collect used columns in Zaaksysteem
    my @used_columns    = ();

    ### Collect used columns in this zaak
    my $betrokkenen = $self->zaak_betrokkenen->search(
        {
            'magic_string_prefix'   => { 'is not'   => undef }
        }
    );

    while (my $betrokkene = $betrokkenen->next) {
        push(@used_columns, $betrokkene->magic_string_prefix . '_naam');
    }

    return BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(
        \@used_columns, $dv->valid('magic_string_prefix'), $dv->valid('rol')
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION

TODO: Fix the POD

=cut

=head2 BETROKKENE_RELATEREN_PROFILE

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_ZAAK

TODO: Fix the POD

=cut

=head2 betrokkene_object

TODO: Fix the POD

=cut

=head2 betrokkene_relateren

TODO: Fix the POD

=cut

=head2 betrokkenen_relateren_magic_string_suggestion

TODO: Fix the POD

=cut

=head2 is_betrokkene_compleet

TODO: Fix the POD

=cut

=head2 set_aanvrager

TODO: Fix the POD

=cut

=head2 set_behandelaar

TODO: Fix the POD

=cut

=head2 set_coordinator

TODO: Fix the POD

=cut


package Zaaksysteem::Zaken::Roles::ZTT;

use Moose::Role;
use Zaaksysteem::Constants;

use Zaaksysteem::Backend::Object::Attribute;
use Zaaksysteem::Attributes;

=head1 NAME

Zaaksysteem::Zaken::Roles::ZTT - Magic String handling and Zaaksysteem Template Toolkit

=head1 SYNOPSIS

    my @magic_strings = @{ $case->list_magic_strings };

    ### Returns
    # zaak_nummer, zaaktype_titel, [...]

    my $object_attributes = $case->object_attributes

    ### Returns an arrayref with L<Zaaksysteem::Backend::Object::Attribute> objects


=head1 DESCRIPTION

Component Magic Strings within zaaksysteem, technical documentation

=head1 ATTRIBUTES

=head2 object_attributes

Return value: ArrayRef[Zaaksysteem::Backend::Object::Attribute]

     my $object_attributes = $case->object_attributes

Returns a list of L<Zaaksysteem::Backend::Object::Attribute>, together with their values

=cut

has 'object_attributes'    => (
    'is'            => 'ro',
    'lazy'          => 1,
    'isa'           => 'ArrayRef[Zaaksysteem::Backend::Object::Attribute]',
    'builder'       => '_build_attribute_list',
    'clearer'       => 'clear_object_attributes'
);



has '_list_case_attributes'     => (
    'is'            => 'ro',
    'lazy'          => 1,
    'builder'       => '_build_magic_string_list',
);

=head2 list_magic_strings

Return value: ArrayRef[Str]

    my @magic_strings = @{ $case->list_magic_strings };

    ### Returns
    # zaak_nummer, zaaktype_titel, [...]

Returns a list of magic strings

=cut

has 'list_magic_strings'        => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self                    = shift;

        return [ map { $_->name } @{ $self->_list_case_attributes }];
    }
);


sub get_string_fetchers {
    my $self = shift;

    my @fetchers;

    # Simple case, translate systemattributes
    push @fetchers, sub {
        my $tagname = shift->name;

        my $value;
        eval {
            $value = $self->systeemkenmerk($tagname);
        };

        # guns don't kill people, uncaught exceptions are a different story :)
        # the proper solution would be a test wether a magic string is
        # retrievable.
        # i'm proposing to leave this warn in, even when in generates a lot of
        # logging, because changes in other parts will go unnoticed.
        if ($@) {
            $self->result_source->schema->log->debug("Non-fatal error retrieving systeemkenmerk value $tagname: $@");
        }

        return unless $value;

        my %params = (value => $value);
        $params{type} = 'image' if $tagname eq 'behandelaar_handtekening';

        return Zaaksysteem::ZTT::Element->new(%params);
    };

    # Involved subjects
    push @fetchers, sub {
        my $tag = shift;

        my $betrokkenen = $self->zaak_betrokkenen->search_gerelateerd;

        # Usual magic strings here look like 'aanvrager_naam'
        my ($prefix, $tagname) = split m[_], $tag->name;

        # TODO remove this iterator, search the db directly for related subjects
        # that match the prefix
        for my $betrokkene ($betrokkenen->all) {
            next unless $prefix eq $betrokkene->magic_string_prefix;

            return Zaaksysteem::ZTT::Element->new(
                value => (ZAAKSYSTEEM_BETROKKENE_SUB->(
                    $self,
                    $self->betrokkene_object({ magic_string_prefix => $prefix }),
                    $tagname
                ) || '')
            );
        }
    };

    # Case attributes
    push @fetchers, sub {
        my $tag = shift;

        my $attribute = $self->zaaktype_node_id->zaaktype_kenmerken->search(
            { 'library_attribute.magic_string' => $tag->name },
            { join => 'library_attribute' }
        )->first;

        return unless $attribute;

        my $library_attribute = $attribute->bibliotheek_kenmerken_id;

        my $field_values = $self->field_values({
            bibliotheek_kenmerken_id => $library_attribute->id
        });

        my $element = Zaaksysteem::ZTT::Element->new(
            attribute => $attribute,
            value => $field_values->{ $library_attribute->id }
        );

        if ($attribute->referential && $self->pid) {

            $field_values = $self->pid->field_values({
                bibliotheek_kenmerken_id => $library_attribute->id
            });

            $element->value($field_values->{ $library_attribute->id });
        }

        return $element;
    };

    return @fetchers;
}

sub get_context_iterators {
    my $self = shift;

    return {
        zaak_relaties => [ map { $_->case } $self->zaak_relaties ] || []
    };
}


sub _build_magic_string_list {
    my $self                        = shift;
    my $magic_strings               = [];

    $self->_collect_zaaktype_attributes ($magic_strings);
    $self->_collect_system_attributes   ($magic_strings);
    $self->_collect_subject_attributes  ($magic_strings);
    $self->_collect_dynamic_attributes  ($magic_strings);

    return $magic_strings;
}

sub _build_attribute_list {
    my $self   = shift;
    my $object = $self->object_data;

    my $kenmerken                   = $self->field_values;

    for my $magic_string (@{ $self->_list_case_attributes }) {
        $magic_string->parent_object($object);

        if ($magic_string->is_systeemkenmerk) {
            eval {
                my $value = $magic_string->systeemkenmerk_reference->($self);

                $magic_string->value($value) if defined $value;
            };
        } elsif ($magic_string->object_table eq 'zaak_betrokkenen') {
            ### XXX TODO, FIX THIS

        } elsif ($magic_string->object_table eq 'zaaktype_kenmerken') {
            eval {
                my $bibliotheek_kenmerk = $magic_string->object_row->bibliotheek_kenmerken_id;

                my $type = $bibliotheek_kenmerk->value_type;
                my $type_definition = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{$type};

                my $value = $kenmerken->{ $bibliotheek_kenmerk->id };

                if (exists $type_definition->{object_search_filter}) {
                    $value = $type_definition->{object_search_filter}->($self->result_source->schema, $value);
                }

                $magic_string->value($value);
            };
        }

        ### XXX TODO TURN THIS THING ON
        $self->log->error('Finding magic_string value failed for: ' . $magic_string->name . ' - ' . $@) if $@;
    }

    return $self->_list_case_attributes;
}

sub _collect_zaaktype_attributes {
    my $self                        = shift;
    my $magic_strings               = shift;

    my $kenmerken               = $self
                                ->zaaktype_node_id
                                ->zaaktype_kenmerken
                                ->search(
        {
            'bibliotheek_kenmerken_id.value_type'   => { '!='   => 'file' },
            'me.is_group'                           => undef
        },
        {
            prefetch    => 'bibliotheek_kenmerken_id'
        }
    );

    while (my $kenmerk = $kenmerken->next) {
        my $bibliotheek_kenmerk = $kenmerk->bibliotheek_kenmerken_id;

        my $type = $bibliotheek_kenmerk->value_type;
        my $type_definition = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{$type};

        next unless $type_definition;

        my $magic_string    = Zaaksysteem::Backend::Object::Attribute->new(
            name                => "attribute." . $bibliotheek_kenmerk->magic_string,
            attribute_type      => $type_definition->{object_search_type},
            object_table        => 'zaaktype_kenmerken',
            object_id           => $kenmerk->id,
            object_row          => $kenmerk,
        );

        push(@{ $magic_strings }, $magic_string);
    }
}

sub _collect_system_attributes {
    my $self                        = shift;
    my $magic_strings               = shift;

    push (
        @{ $magic_strings },
        @{ ZAAKSYSTEEM_SYSTEM_ATTRIBUTES()->() }
    );
}

sub _collect_subject_attributes {
    my $self                        = shift;
    my $magic_strings               = shift;

    # {
    #     my $betrokkenen = $case->zaak_betrokkenen->search_gerelateerd;

    #     my $kenmerken_h = ZAAKSYSTEEM_BETROKKENE_KENMERK;
    #     my @kenmerken   = keys %{ $kenmerken_h };

    #     while (my $betrokkene = $betrokkenen->next) {
    #         my $magic_string = $betrokkene->magic_string_prefix;

    #         my $betrokkene_object = $case->betrokkene_object(
    #             {
    #                 'magic_string_prefix'    => $magic_string
    #             }
    #         ) or next;

    #         for my $kenmerk_postfix (@kenmerken) {
    #             my $kenmerk = $magic_string . '_' . $kenmerk_postfix;

    #             push(
    #                 @{ $magic_strings },
    #                 Zaaksysteem::Backend::Object::Attribute->new(
    #                     name            => $kenmerk,
    #                     attribute_type  => 'text',
    #                     object_table    => 'zaak_betrokkenen',
    #                     object_id       => $betrokkene->id,
    #                     object_row      => $betrokkene
    #                 )
    #             );
    #         }
    #     }
    # }
}

sub _collect_dynamic_attributes {
    my $self = shift;
    my ($magic_strings) = @_;

    push @$magic_strings, (
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name  => 'dagen',
            name           => 'case.days_left',
            attribute_type => 'integer',
            dynamic_class  => 'DaysLeft',
            object_table   => '',
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            bwcompat_name  => 'voortgang',
            name           => 'case.progress_days',
            attribute_type => 'integer',
            dynamic_class  => 'DaysPercentage',
            object_table   => '',
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name           => 'case.destructable',
            attribute_type => 'integer',
            dynamic_class  => 'CaseDestructable',
            object_table   => '',
        ),
        Zaaksysteem::Backend::Object::Attribute->new(
            name           => 'case.destruction_blocked',
            attribute_type => 'integer',
            dynamic_class  => 'CaseDestructionBlocked',
            object_table   => '',
        ),
    );

    return;
}

1;

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

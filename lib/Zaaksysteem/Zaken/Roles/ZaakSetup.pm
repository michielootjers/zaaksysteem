package Zaaksysteem::Zaken::Roles::ZaakSetup;

use Moose::Role;
use Data::Dumper;

use File::Copy;

use Zaaksysteem::Constants qw/
    ZAKEN_STATUSSEN
    ZAKEN_STATUSSEN_DEFAULT
    LOGGING_COMPONENT_ZAAK
    ZAAK_CREATE_PROFILE
/;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

### Roles
with
    'Zaaksysteem::Zaken::Roles::BetrokkenenSetup',
    'Zaaksysteem::Zaken::Roles::BagSetup',
    'Zaaksysteem::Zaken::Roles::KenmerkenSetup',
    'Zaaksysteem::Zaken::Roles::RelatieSetup';

has 'log'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        if (
            defined($self->{attrs}) &&
            defined($self->{attrs}->{log})
        ) {
            return $self->{attrs}->{log}
        }

        use Catalyst::Log;

        return Catalyst::Log->new;
    }
);

has 'z_betrokkene'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        shift->result_source->schema->betrokkene_model;
    }
);


{

    sub find_as_session {
        my $self            = shift;
        my $zaak            = shift;

        unless (
            $zaak &&
            (
                ref($zaak) ||
                $zaak =~ /^\d+$/
            )
        ) {
            die('Kan niet dupliceren, geen zaak of zaaknr meegegeven');
        }

        unless (ref($zaak)) {
            $zaak = $self->find($zaak)
                or die('Geen zaak gevonden met nummer: ' .  $zaak);
        }

        my $pp_profile      = Params::Profile->get_profile(
            method  => 'create_zaak'
        );

        ### _get_zaak_as_session
        my %zaak_data   = $zaak->get_columns;
        my %zaak_copy;
        for my $col (keys %zaak_data) {
            next if grep( { $_ eq $col } qw/
                id
                pid
                relates_to
                vervolg_van
                related_because
                vervolg_because
                child_because

                zaaktype_node_id

                last_modified
                created

                days_perc
                days_running
                days_left

                aanvrager
                coordinator
                behandelaar
                locatie_zaak
                locatie_correspondentie
            /);

            $zaak_copy{$col} = $zaak_data{$col};
        }

        ### _get_betrokkenen
        for my $btype (qw/behandelaar coordinator aanvrager/) {
            next unless $zaak->$btype;


            my $btype_p     = $btype . 's';

            my $get_sub     = $btype . '_object';

            next unless($zaak->$get_sub);
            $zaak_copy{$btype_p} = [{
                betrokkene_type => $zaak->$get_sub->btype,
                betrokkene      => $zaak->$get_sub->betrokkene_identifier,
                verificatie     => ($zaak->$btype->verificatie || 'medewerker'),
            }]
        }

        ### _get_locatie
        for my $locatie (qw/locatie_zaak locatie_correspondentie/) {
            next unless $zaak->$locatie;

            $zaak_copy{$locatie}    = {
                bag_type        => $zaak->$locatie->bag_type,
                bag_id          => $zaak->$locatie->bag_id,
            }
        }

        ### _get_kenmerken
        my $kenmerken           = $zaak->field_values();

        if (scalar(keys %{ $kenmerken })) {
            $zaak_copy{kenmerken} = [];

            while (my ($kenmerk_id, $kenmerk_value) = each %{ $kenmerken }) {
                push(
                    @{ $zaak_copy{kenmerken} },
                    { $kenmerk_id   => $kenmerk_value }
                );
            }
        }

        return \%zaak_copy;
    }

    define_profile create_zaak => %{ZAAK_CREATE_PROFILE()};
    sub create_zaak {
        my ($self, $opts) = @_;
        $opts = assert_profile($opts)->valid;

        $self->log->debug('create_zaak started');
        if (!$self->_validate_zaaktype($opts)) {
            throw(
                type => '/Zaaksysteem/Zaak/create_zaak',
                message => 'Invalid case type',
                object => $opts
            );
        }

        my $zaak;
        $self->result_source->schema->txn_do(
            sub {
                $zaak = $self->_create_zaak($opts);

                $self->result_source->schema->resultset('Checklist')->create_from_case($zaak);
                $self->result_source->schema->resultset('CaseAction')->create_from_case($zaak);

                $zaak->logging->trigger(
                    'case/create', {
                        component => LOGGING_COMPONENT_ZAAK,
                        data      => {case_id => $zaak->id}});

                $self->log->info('Created zaak in DB: ' . $zaak->id);
                ### Zaak loaded, now ask zaak to bootstrap himself with given options
                $zaak->_bootstrap($opts);
            });

        $self->log->debug('create_zaak ended');
        return $zaak;
    }

    sub _validate_zaaktype {
        my ($self, $opts) = @_;
        my ($ztn);

        if ($opts->{zaaktype_node_id}) {
            $ztn = $self->result_source->schema->resultset('ZaaktypeNode')->find(
                $opts->{zaaktype_node_id},
                {
                    prefetch    => 'zaaktype_id'
                }
            );

            unless ($ztn) {
                throw('Zaken/_validate_zaaktype', "Cannot find zaaktype_node_id for $opts->{zaaktype_node_id}");
            }

            $opts->{zaaktype_id} = $ztn->zaaktype_id->id;
        } else {
            my $zt = $self->result_source->schema->resultset('Zaaktype')->find(
                $opts->{zaaktype_id},
                {
                    prefetch    => 'zaaktype_node_id'
                }
            );

            unless ($zt) {
                throw('Zaken/_validate_zaaktype', "Cannot find zaaktype_node_id for $opts->{zaaktype_id}");
            }

            $opts->{zaaktype_node_id} = $zt->zaaktype_node_id->id;

            $ztn = $self->result_source->schema->resultset('ZaaktypeNode')->find(
                $opts->{zaaktype_node_id}
            );
        }
        return $ztn;
    }

    sub _create_betrokkenen {
        my ($self, $opts) = @_;

        return unless $opts->{aanvragers};

        # DUMMY
        $opts->{aanvrager} = $self->result_source->schema->resultset('ZaakBetrokkenen')->create(
            {
                betrokkene_type         => 'natuurlijk_persoon',
                betrokkene_id           => 1,
                gegevens_magazijn_id    => 24640,
                verificatie             => 'digid',
            },
        );

        push(@{ $opts->{_update_zaak_ids} },
            $opts->{aanvrager}
        );
    }

#    define_profile _create_zaak => (
#        required => [qw()],
#        optional => [qw(confidentiality override_zaak_id created registratiedatum)],
#        typed    => {
#            confidentiality => 'Bool',
#            override_zaak_id => 'Bool',
#        },
#        defaults => {
#            confidentiality => 0,
#        },
#    );

    sub _create_zaak {
        my ($self, $opts) = @_;
        my (%create_params);

        $self->log->debug('Zaaksetup _create_zaak: started');

        $create_params{ $_ } = $opts->{ $_ }
            for $self->result_source->columns;
        # TODO: define_profile..
        # The code above defaults to undef, which results in an error because
        # confidentiality is not allowed to be undef. the database has the proper
        # default value. a more constructive solution is to avoid the above construct
        # altogether.

        ### Delete autoincrement integer, UNLESS IT IS SET and OVERRIDE
        ### zaak_nr is true
        unless ($opts->{override_zaak_id}) {
            delete($create_params{id});
        }

        $create_params{created} = $create_params{registratiedatum};

        my $zaak = $self->create(
            \%create_params
        );

        if (!$zaak) {
            throw(
                type => "Zaaksysteem/Case",
                message => "Unable to create case!",
                object => \%create_params,
            );
        }

        $self->log->debug(
            sprintf('Zaaksetup _create_zaak: created zaak with id: %d', $zaak->id)
        );

        return $zaak;
    }
}

{
    Params::Profile->register_profile(
        method  => 'create_relatie',
        profile => {
            required        => [ qw/
                zaaktype_id
                type_zaak
            /],
            'optional'      => [ qw/
                subject
                start_delay
                actie_kopieren_kenmerken
                actie_automatisch_behandelen
                role_id
                ou_id
                behandelaar_id
                behandelaar_type
            /],
            'require_some'  => {
                'aanvrager_id_or_aanvrager_type' => [
                    1,
                    'aanvrager_id',
                    'aanvrager_type'
                ],
            },

            # this doesn't seem to have influence
            'constraint_methods'    => {
                type_zaak => qr/^gerelateerd|vervolgzaak|vervolgzaak_datum|deelzaak|wijzig_zaaktype$/,
            },
        }
    );

    sub create_relatie {
        my ($self, $zaak, %opts) = @_;
        my ($nid, $aanvrager_id, $behandelaar_id);

        ### VALIDATION
        my $dv = Params::Profile->check(
            params  => \%opts,
        );

        throw "create_relatie: invalid options:" . Dumper($dv) unless $dv->success;

        ### Aanvrager information
        if ($dv->valid('aanvrager_type')) {
            if ($dv->valid('aanvrager_type') eq 'aanvrager') {
                $aanvrager_id = $zaak->aanvrager_object->betrokkene_identifier;
            } elsif ($dv->valid('aanvrager_type') eq 'behandelaar') {
        		throw "create_relatie: need zaak->behandelaar_object for zaak id: " . $zaak->id . $zaak->behandelaar
                    unless $zaak->behandelaar_object;
                $aanvrager_id = $zaak->behandelaar_object->betrokkene_identifier;
            }
        } else {
            $aanvrager_id = $dv->valid('aanvrager_id');
        }

        my $behandelaars    = [];
        if ($dv->valid('behandelaar_type')) {
            if ($zaak->behandelaar_object && $dv->valid('behandelaar_type') eq 'behandelaar') {
                $behandelaar_id = $zaak->behandelaar_object->betrokkene_identifier;
            }
        } elsif ($dv->valid('behandelaar_id')) {
            $behandelaar_id = $dv->valid('behandelaar_id');
        }

        if ($behandelaar_id) {
            $behandelaars   = [{
                betrokkene          => $behandelaar_id,
                verificatie         => 'medewerker',
            }];
        }

        my $type_zaak_msg = ucfirst($dv->valid('type_zaak'));

        if ($type_zaak_msg =~ /Vervolgzaak/) {
            $type_zaak_msg = 'Vervolgzaak';
        }

        ### Subject
        my $subject = $dv->valid('subject') ||
            $type_zaak_msg . ' van zaaknummer: ' .  $zaak->id;

        ### First, search for acturele zaaktype_node_id
        my $zaaktype_node_id;
        {
            my $zt = $self->result_source->schema->resultset('Zaaktype')->find(
                $dv->valid('zaaktype_id')
            );

            unless ($zt) {
                $self->log->debug('Zaaktype_id not found or complete', $dv->valid('zaaktype_id'));

                return;
            }

            $zaaktype_node_id = $zt->zaaktype_node_id->id;
        }

        # type_zaak is an alias for relatie_type, to be corrected
        my $registratiedatum = $self->determine_registratiedatum(
            $dv->valid('type_zaak'),
            $dv->valid('start_delay')
        );


        ### Zaak informatie
        my %zaak_opts = (
            zaaktype_node_id          => $zaaktype_node_id,
            behandelaars              => $behandelaars,
            onderwerp                 => $subject,
            registratiedatum          => $registratiedatum,
            relatie                   => $dv->valid('type_zaak'),
            contactkanaal             => $zaak->contactkanaal,
            aanvraag_trigger          => $zaak->aanvraag_trigger,
            route_role                => ($dv->valid('role_id') || undef),
            route_ou                  => ($dv->valid('ou_id') || undef),
            aanvragers                => [{
                betrokkene                  => $aanvrager_id,
                verificatie                 => 'medewerker',
            }],
            actie_kopieren_kenmerken  => (
                $dv->valid('actie_kopieren_kenmerken')
                    ? 1
                    : undef
            )
        );

        $self->log->debug(
            'Creating case with options:',
            Dumper(\%zaak_opts)
        );

        # Add zaak to zaakopts after debug log, prevent spam
        $zaak_opts{ zaak } = $zaak;

        my $nieuwe_zaak = $self->create_zaak(\%zaak_opts);

        return unless $nieuwe_zaak;

        if ($dv->valid('actie_automatisch_behandelen')) {
            $nieuwe_zaak->open_zaak;
        }

        return $nieuwe_zaak;
    }
}

=head2 determine_registratiedatum

Follow-up cases (vervolgzaken) can be created with a delay of either
a set date (relatie_type: vervolgzaak_datum) or an interval in days (relatie_type: vervolgzaak).

=cut

sub determine_registratiedatum {
    my ($self, $relatie_type, $start_delay) = @_;

    if ($relatie_type eq 'vervolgzaak' && $start_delay =~ m|^\d+$|) {
        return DateTime->now->add(days => $start_delay);
    }

    if ($relatie_type eq 'vervolgzaak_datum' && $start_delay =~ m|^\d+-\d+-\d+$|) {

        my ($day, $month, $year) = $start_delay =~ m|^(\d+)-(\d+)-(\d+)$|;

        return DateTime->new(
            year    => $year,
            day     => $day,
            month   => $month,
        );
    }

    return DateTime->now();
}



sub duplicate {
    my ($self, $zaak, $opts) = @_;

    unless (
        $zaak &&
        (
            ref($zaak) ||
            $zaak =~ /^\d+$/
        )
    ) {
        die('Kan niet dupliceren, geen zaak of zaaknr meegegeven');
    }

    unless (ref($zaak)) {
        $zaak = $self->find($zaak)
            or die('Geen zaak gevonden met nummer: ' .  $zaak);
    }

    ### Retrieve aanvragers / kenmerken / bag data etc
    my $new_zaak_session    = $self->find_as_session($zaak);

    $opts                  ||= {};
    my $old_uuid = delete $new_zaak_session->{ uuid };

    ### Change behandelaar to current user
    $opts->{behandelaars}   = [
        {
            betrokkene  => $self->current_user->betrokkene_identifier,
            verificatie => 'medewerker',
        }
    ];

    ### Commit new zaak
    my ($new_zaak);

    eval {
        $self->result_source->schema->txn_do(sub {
            if ($opts->{simpel}) {
                $opts->{registratiedatum}   = DateTime->now();
                $opts->{streefafhandeldatum}= undef;

                warn('SIMPEL VERSION!!!!');
            }

            $new_zaak            = $self->create_zaak({
                %{ $new_zaak_session },
                %{ $opts }
            });

            if ($opts->{simpel}) {
                $new_zaak->milestone(1);
                $new_zaak->set_heropen;
            } else {

                $zaak->duplicate_checklist_items({target => $new_zaak});
                $zaak->duplicate_relations({target => $new_zaak});

                ### Copy files
                my $case_files  = $zaak->files->search(
                    {},
                    {
                        order_by => { '-asc' => 'id' }
                    }
                );
                $case_files->update({case_id => $new_zaak->id});

                # when changing case type, old case is deleted, new case is created.
                # on deletion nothing may be preserved, so don't copy logging either.
                unless($opts->{dont_copy_log}) {
                    ### Copy logboek
                    my $zaak_logging  = $zaak->logging->search(
                        {},
                        {
                            order_by => { '-asc' => 'id' }
                        }
                    );
                    while (my $logging = $zaak_logging->next) {
                        $logging->copy(
                            {
                                zaak_id => $new_zaak->id
                            }
                        );
                    }
                }
            }
        });
    };

    if ($@) {
        $self->log->error(
            'Failed duplicating zaak: ' . $zaak->id
            . ': ' . $@
        );
        return;
    }

    $zaak->logging->trigger('case/duplicate', { component => LOGGING_COMPONENT_ZAAK, data => {
        duplicate_id => $new_zaak->id,
        case_id => $zaak->id
    }});


    $new_zaak->logging->trigger('case/duplicated', { component => LOGGING_COMPONENT_ZAAK, data => {
        case_id => $new_zaak->id,
        duplicated_from => $zaak->id
    }});

    return $new_zaak;
}


=head2 duplicate_relations

Copy relations of case to target case

=cut

define_profile duplicate_relations => (
    required => [qw/target/]
);
sub duplicate_relations {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $target = $params->{target};
    my $rs = $self->result_source->schema->resultset('CaseRelation');

    $rs->add_relation($target->id, $_->case_id) for $rs->get_sorted($self->id);
}


=head2 duplicate_checklist_items

A case can have a checklist for each phase (why not link them to the phase?).
When duplicating a case, copy all items to the new case. The checklist is
already assumed to be duplicated.

To test:
Create two cases.
Add a checklist to the first, populate it with items.
Call this routine, after that the second case should have the same.

Much room for improvement there is.
The duplication process should be delegated to the checklist resultset.

=cut

define_profile duplicate_checklist_items => (
    required => [qw/target/]
);
sub duplicate_checklist_items {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $target = $params->{target};

    my $schema = $self->result_source->schema;
    my $checklist_rs = $schema->resultset('Checklist');
    my $checklists = $self->checklists->search;

    my $rs_items = $schema->resultset('ChecklistItem');

    while (my $checklist = $checklists->next) {

        # get the corresponding checklist_id in the new case
        my $new_checklist = $checklist_rs->search({
            case_id => $target->id,
            case_milestone => $checklist->case_milestone
        })->first;

        # the addition of new checklists happens elsewhere, right now
        # i will only account for those that have been added.
        if ($new_checklist) {
            my $new_id = $new_checklist->id;

            my @items = $rs_items->search({ checklist_id => $checklist->id })->all;

            foreach my $item (@items) {
                my $columns = {$item->get_columns};
                delete $columns->{id};
                $columns->{checklist_id} = $new_id;
                $rs_items->create($columns);
            }
        }
    }
}



sub _copy_document {
    my $self        = shift;
    my $document    = shift;
    my $nieuwe_doc  = shift;

    return 1 if $document->documents_mails->count;

    my $files_dir   = $self->config->{files} . '/documents';

    die('File not found: ' . $files_dir . '/' . $document->id)
        unless ( -f $files_dir . '/' . $document->id);

    die('Failed copying: ' . $files_dir . '/' . $document->id
       . ' TO ' . $files_dir . '/' . $nieuwe_doc->id
    ) unless copy(
        $files_dir . '/' . $document->id,
        $files_dir . '/' . $nieuwe_doc->id
    );

    return 1;
}


sub wijzig_zaaktype {
    my ($self, $zaak, $opts) = @_;

    unless($opts && $opts->{zaaktype_id}) {
        throw "wijzig_zaaktype: need zaaktype_id";
    }

    my $zaaktype = $self->result_source->schema->resultset('Zaaktype')->find($opts->{zaaktype_id});

    $opts->{zaaktype_node_id} = $zaaktype->zaaktype_node_id->id;

    $opts->{dont_copy_log} = 1;

    my $change;

    $self->result_source->schema->txn_do(sub {
        $change = $self->duplicate($zaak, $opts)
            or return;

        $zaak->logging->trigger('case/update/case_type', {
            component => LOGGING_COMPONENT_ZAAK,
            data => {
                case_id => $zaak->id,
                casetype_id => $zaaktype->id,
                new_case_id => $change->id
            }
        });

        $zaak->vernietigingsdatum(DateTime->now->add(days => -1));
        $zaak->status('resolved');
        $zaak->update;
        $zaak->set_deleted;
    });

    return $change;
}


sub execute_rules {
    my ($self, $options) = @_;

    my $status = $options->{ status }; # optional
    my $cache = $self->result_source->schema->cache;

    if (!$cache) {
        throw("ZS/Z/R/ZS", "No cache found");
    }

    if(!defined $options->{ cache } || $options->{ cache }) {
        my $retval = $cache->get({
            key => 'execute_rules',
            params => $options
        });

        return $retval if $retval;
    }

    ### What are we doing here...
    return if $self->is_afgehandeld;

    my $field_values_params = {};

    $field_values_params->{ fase } = $status if $status;
    $field_values_params->{ cache } = $options->{ cache } if defined($options->{ cache });

    my $rules_result = $self->zaaktype_node_id->rules($options)->execute({
        kenmerken => $self->field_values($field_values_params),
        aanvrager => $self->aanvrager_object,
        contactchannel => $self->contactkanaal,
        payment_status => $self->payment_status,
        casetype => $self->zaaktype_node_id,
        confidentiality => $self->confidentiality
    });

    $self->_set_case_result($rules_result);

    return $cache ? $cache->set({
        key         => 'execute_rules',
        value       => $rules_result,
        params      => $options
    }) : $rules_result;
}


sub _set_case_result {
    my ($self, $rules_result) = @_;

    # TODO check for possible problems
    if(my $set_case_result_action = $rules_result->{set_case_result}) {
        eval {
            my $resultaat_index = $set_case_result_action->{value} - 1;
            my @results = $self->zaaktype_node_id->zaaktype_resultaten->search({}, {order_by => {'-asc' => 'id'}})->all;

            my $new_resultaat = $results[$resultaat_index];
            $self->set_resultaat($new_resultaat->resultaat);
            $self->update;
        };
        if($@) {
            warn "something went terribly wrong, but we just continue" . $@;
        }
    }
    return $rules_result;
}


sub result_info {
    my $self = shift;

    return $self->zaaktype_node_id->zaaktype_resultaten->search({
        resultaat => $self->resultaat
    })->first;
}


sub notifications {
    my ($self) = @_;

    return scalar $self->zaaktype_node_id->zaaktype_notificaties->search({}, {
        prefetch => 'bibliotheek_notificaties_id',
        order_by => {'-asc' => 'me.id'},
    });
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


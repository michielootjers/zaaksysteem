package Zaaksysteem::Zaken::Roles::KenmerkenSetup;

use Moose::Role;
use Data::Dumper;

use Zaaksysteem::Exception;

around '_create_zaak' => sub {
    my $orig                = shift;
    my $self                = shift;
    my ($opts)              = @_;
    my ($zaak_kenmerken);

    $self->log->info('Role [KenmerkenSetup]: started');

    my $zaak = $self->$orig(@_);

    ### Mangle kenmerken with defaults
    my $mangled_properties  = $zaak
                            ->zaaktype_node_id
                            ->zaaktype_kenmerken
                            ->mangle_defaults(
                                $opts->{kenmerken} || []
                            );


    # mangle_defaults may return a hashref
    my $ref = ref $mangled_properties;
    if ($ref eq 'ARRAY' && @$mangled_properties) {
        $zaak->zaak_kenmerken->create_kenmerken({
            zaak_id     => $zaak->id,
            kenmerken   => $mangled_properties
        });
    }
    elsif ($ref eq 'HASH') {
        throw('KenmerkenSetup', "Mangled options are a HASHREF, Unable to do something");
    }

    ### Recache field values
    delete($zaak->{cached_field_values});
    $zaak->touch();

    $self->log->debug('Kenmerken toegevoegd');

    return $zaak;
};

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


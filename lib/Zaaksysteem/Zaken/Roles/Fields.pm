package Zaaksysteem::Zaken::Roles::Fields;

use Moose::Role;
use Data::Dumper;



sub visible_fields {
    my ($self, $args) = @_;
    
    my $field_values    = $args->{field_values}     or die "need field_values";
    my $rules_result    = $args->{rules_result};    # optional;

    my $fase            = $args->{phase};           # optional
    my $mandatory       = $args->{mandatory};       # optional
    my $include_docs    = $args->{include_documents};

    my $result = $self->execute_rules({
        status => $fase && $fase->status,
    });

    my $fields_rs = $fase ?
        $fase->zaaktype_kenmerken->search() :
        $self->zaaktype_node_id->zaaktype_kenmerken->search();

    my $search_query = {
            'me.is_group'                           => undef,
            'me.bibliotheek_kenmerken_id'           => { 'is not'   => undef },
    };

    if (!$include_docs) {
        $search_query->{'bibliotheek_kenmerken_id.value_type'} = {
            '!=' => 'file'
        };
    }

    $fields_rs = $fields_rs->search(
        $search_query,
        {
            'order_by'  => 'me.id',
            'prefetch'  => 'bibliotheek_kenmerken_id',
        }
    );

    if ($result->{verberg_kenmerk}) {
    
        $fields_rs = $fields_rs->search({
            'me.bibliotheek_kenmerken_id' => {
                'not in'    => [ keys %{ $result->{verberg_kenmerk} } ],
            },
        });
    }

    if($fase  && $mandatory) {
        $fields_rs = $fields_rs->search({
            'me.value_mandatory' => 1,
        });
    }

    my $fields = {};
    while (my $row = $fields_rs->next) {

        my $bibliotheek_kenmerken_id = $row->bibliotheek_kenmerken_id->id;

        $fields->{ $bibliotheek_kenmerken_id } = 1;

        last if(
            $result->{pause_application} &&
            $result->{last_kenmerk} eq $bibliotheek_kenmerken_id
        );
    }

    $rules_result = $result if $rules_result;

    return $fields;
}


=head2 phase_fields_complete

Checks if all required fields for this phase are filled,
taking into account the fields that are hidden by rules.

=cut

sub phase_fields_complete {
    my ($self, $args) = @_;

    my $fase = $args->{phase} or die "need phase";

    my $given_kenmerken = $args->{custom_fields}; # optional
    
    $given_kenmerken ||= $self->field_values({ fase => $fase->status });

    my $kenmerken  = { %{ $given_kenmerken } };

    my $rules_result;
    my $required_fields = $self->visible_fields({
        phase           => $fase,
        mandatory       => 1,
        field_values    => $given_kenmerken,
        result          => $rules_result,
    });


    #$kenmerken  = { %{ $given_kenmerken } };

    ### PAUZE?

    if ($rules_result->{pauzeer_aanvraag}) {
        my $key = [ keys %{ $rules_result->{pauzeer_aanvraag} } ]->[0];
        return {
            'succes'    => 0,
            'pauze'     => $rules_result->{pauzeer_aanvraag}->{$key},
        }
    }

    foreach my $bibliotheek_kenmerken_id (keys %$required_fields) {
        my $value = $kenmerken->{$bibliotheek_kenmerken_id};
        $value = ref $value && ref $value eq 'ARRAY' ? join "", @$value : $value;

        next if length($value);

        return {
            'succes'    => 0,
            'required'  => 1,
        };
    }

    return {
        'succes'    => 1
    };
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


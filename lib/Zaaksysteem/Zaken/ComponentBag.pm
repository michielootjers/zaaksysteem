package Zaaksysteem::Zaken::ComponentBag;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub update_bag {
    my  $self   = shift;
    my  $params = shift;

    if ($params) {
        return 1 unless (
            UNIVERSAL::isa($params, 'HASH') &&
            scalar(%{ $params })
        );
    } else {
        $params = $self;
    }

    ### Get default resultset
    if (my $fixed_row = $self->result_source->schema->resultset('BagNummeraanduiding')->_retrieve_zaakbag_data(
            $params
        )
    ) {
        $self->update($fixed_row);
    }
}

sub columns_bag {
    my $self        = shift;
    my %columns_bag;

    my %cols        = $self->get_columns;
    while (my ($colname, $colvalue) = each %cols) {
        next if grep { $colname eq $_ } qw/zaak_id pid id/;

        $colname    =~ s/bag_//;
        $colname    =~ s/_id//;

        $columns_bag{$colname} = $colvalue;
    }

    return %columns_bag;
}

sub _verify_bagdata {
    my $self    = shift;

    ### Get default resultset
    return $self->result_source->schema->resultset('BagNummeraanduiding')->_retrieve_zaakbag_data(
        $self,
        @_
    );
}

sub maps_adres {
    my $self    = shift;

    #return '' unless $self->bag_nummeraanduiding_id;

    ### Get gegevens_model
    my ($nummeraanduiding, $openbareruimte);

    if ($self->bag_nummeraanduiding_id) {
        $nummeraanduiding    = $self->result_source->schema->resultset('BagNummeraanduiding')->search(
            {
                identificatie   => $self->bag_nummeraanduiding_id
            }
        )->first or return '';

        $openbareruimte     = $nummeraanduiding->openbareruimte;
    } elsif ($self->bag_openbareruimte_id) {
        $openbareruimte     = $self->result_source->schema->resultset('BagOpenbareruimte')->search(
            {
                identificatie   => $self->bag_openbareruimte_id
            }
        )->first or return '';
    }

    #return '' unless $nummeraanduiding->openbareruimte;

    my $address = 'Netherlands, '
        . $openbareruimte->woonplaats->naam . ', '
        . $openbareruimte->naam;

    if ($nummeraanduiding) {
        $address .= ', ' . $nummeraanduiding->huisnummer;
    }

    return $address;
}

=head2 verblijfsobject_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_verblijfobject_id.

=cut

sub verblijfsobject_id {
    my $self = shift;
    return unless $self->bag_verblijfsobject_id;
    return "verblijfsobject-" . $self->bag_verblijfsobject_id;
}

=head2 openbareruimte_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_verblijfobject_id.

=cut

sub openbareruimte_id {
    my $self = shift;
    return unless $self->bag_openbareruimte_id;
    return "openbareruimte-" . $self->bag_openbareruimte_id;
}

=head2 nummeraanduiding_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_nummeraanduiding_id.

=cut

sub nummeraanduiding_id {
    my $self = shift;
    return unless $self->bag_nummeraanduiding_id;
    return "nummeraanduiding-" . $self->bag_nummeraanduiding_id;
}

=head2 pand_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_pand_id.

=cut

sub pand_id {
    my $self = shift;
    return unless $self->bag_pand_id;
    return "pand-" . $self->bag_pand_id;
}

=head2 standplaats_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_standplaats_id.

=cut

sub standplaats_id {
    my $self = shift;
    return unless $self->bag_standplaats_id;
    return "standplaats-" . $self->bag_standplaats_id;
}

=head2 ligplaats_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_ligplaats_id.

=cut

sub ligplaats_id {
    my $self = shift;
    return unless $self->bag_ligplaats_id;
    return "ligplaats-" . $self->bag_ligplaats_id;
}

sub insert {
    my $self    = shift;

    my $rv      = $self->next::method(@_);

    if (ref($self->zaak_id)) {
        $self->zaak_id->touch;
    }

    return $rv;
};

sub update {
    my $self    = shift;

    my $rv      = $self->next::method(@_);

    if (ref($self->zaak_id)) {
        $self->zaak_id->touch;
    }

    return $rv;
};

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


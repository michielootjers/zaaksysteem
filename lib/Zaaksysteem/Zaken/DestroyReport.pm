package Zaaksysteem::Zaken::DestroyReport;

use Data::Dumper;
use Moose;
use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

=head2 cases

holds a structure like:
    my $cases = {
        64 => {
            errors => ['Is niet vernietigbaar'],
            warnings => ['Heeft deelzaken die niet vernietigd zijn', 'nog wat']
        },
        65 => {},
        66 => {
            errors => ['Heeft niet status vernietiging']
        }
    };

=cut

has cases => ( is => 'rw', default => sub { {} } );


define_profile add_case => (
    required => [qw/case_id/],
    optional => [qw/errors warnings/]
);
sub add_case {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $cases = $self->cases;
    my $case_id = $arguments->{case_id};

    $cases->{$case_id} = {
        errors   => $arguments->{errors},
        warnings => $arguments->{warnings}
    };
    $self->cases($cases);
}


sub has_warnings {
    my ($self) = @_;

    my $cases = $self->cases;
    grep { $_->{warnings} && @{ $_->{warnings} } } values %$cases;
}


sub has_errors {
    my ($self) = @_;

    my $cases = $self->cases;
    grep { $_->{errors} && @{ $_->{errors} } } values %$cases;
}


sub report {
    my $self = shift;

    my $cases = $self->cases;

    # filter only cases with actual warnings or errors
    my @case_ids = grep {
        $cases->{$_}->{warnings} || $cases->{$_}->{errors}
    } keys %$cases;

    return {map { $_ => $cases->{$_} } @case_ids};
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


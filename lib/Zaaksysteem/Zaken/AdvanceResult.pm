package Zaaksysteem::Zaken::AdvanceResult;

use Moose;

use Zaaksysteem::Exception qw(throw);

use constant ADVANCE_CHECKS => [qw/
    fields_complete
    pip_updates_complete
    checklist_complete
    result_complete
    subcases_complete
    owner_complete
    documents_complete
/];


has	[ @{ ADVANCE_CHECKS() }] => (
    is => 'rw',
    isa => 'Bool',
    default => 0,
);

sub can_advance {
      my $self = shift;

      my $rv = $self->TO_JSON();

      my @faulty_checks = grep { !$rv->{$_} } keys %$rv;
      return 1 if !@faulty_checks;

      # I would like to die, but not sure what will break..
      return 0;
      throw('ZS/Z/AR', sprintf("unable to advance: %s", join(', ', @faulty_checks)));
}

sub TO_JSON {
    my $self    = shift;

    my $rv      = {};

    for my $check (@{ ADVANCE_CHECKS() }) {
        $rv->{$check} = $self->$check;
    }

    return $rv;
}


1;

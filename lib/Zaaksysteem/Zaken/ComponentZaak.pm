package Zaaksysteem::Zaken::ComponentZaak;

use strict;
use warnings;

use Moose;

use Data::Dumper;

use Date::Calendar;
use Date::Calendar::Profiles qw/$Profiles/;
use DateTime::Format::Strptime;
use DateTime;

use Zaaksysteem::Zaken::Jobs;

use Data::UUID;

use Zaaksysteem::Constants;
use Zaaksysteem::Backend::Tools::WorkingDays qw/add_working_days/;
use Zaaksysteem::Backend::Tools::Term qw/calculate_term/;
use Zaaksysteem::ZTT;
use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

extends 'DBIx::Class';

with
    'Zaaksysteem::Zaken::Roles::MetaObjecten',
    'Zaaksysteem::Zaken::Roles::BetrokkenenObjecten',
    'Zaaksysteem::Zaken::Roles::FaseObjecten',
    'Zaaksysteem::Zaken::Roles::DocumentenObjecten',
    'Zaaksysteem::Zaken::Roles::DeelzaakObjecten',
    'Zaaksysteem::Zaken::Roles::KenmerkenObjecten',
    'Zaaksysteem::Zaken::Roles::RouteObjecten',
    'Zaaksysteem::Zaken::Roles::ChecklistObjecten',
    'Zaaksysteem::Zaken::Roles::Acties',
    'Zaaksysteem::Zaken::Roles::Publish',
    'Zaaksysteem::Zaken::Roles::Export',
    'Zaaksysteem::Zaken::Roles::Fields',
    'Zaaksysteem::Zaken::Roles::Schedule',
    'Zaaksysteem::Zaken::Roles::ZTT',
    'Zaaksysteem::Zaken::Roles::HStore',
    'Zaaksysteem::Zaken::Hooks';

use constant RELATED_OBJECTEN => {
    'fasen'     => 'Fasen',
    'voortgang' => 'Voortgang',
    'sjablonen' => 'Sjablonen',
    'locaties'  => 'Locaties',
    'acties'    => 'Acties'
};

=head1 NAME

Zaaksysteem::Zaken::ComponentZaak - A row representing a case.

=head1 SYNOPSIS

    my $question    = $c->model('DB::Zaak')->find(1);

    ### Attributes
    $zaak->id;
    $zaak->status;
    $zaak->registratiedatum;
    [...]


=head1 DESCRIPTION

This POD is a startpoint for getting to the case related documentation.

=cut

has 'jobs'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return Zaaksysteem::Zaken::Jobs->new(
            'zaak'  => $self,
        );
    }
);

has 'te_vernietigen'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return unless $self->vernietigingsdatum;

        return 1 if $self->vernietigingsdatum < DateTime->now();

        return;
    }
);

has 'object_data'       => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        my $object_data = $self->result_source->schema->resultset('ObjectData');

        # We don't want deleted objects to enter the hstore/object_data table
        if($self->is_deleted) {
            my $case_object = $object_data->find({ object_class => 'case', object_id => $self->id });

            if($case_object) {
                $case_object->delete;
            }

            return;
        }

        return $object_data->find_or_create_by_object_id(
            'case', $self->id
        );
    }
);

sub nr  {
    my $self    = shift;

    $self->id( @_ );
}

sub set_resultaat {
    my $self            = shift;

    my $oudresultaat    = $self->resultaat;
    my $result          = $self->resultaat(@_);

    return if $oudresultaat && $oudresultaat eq $result; 

    if (@_) {
        $self->set_vernietigingsdatum;
    }

    my $event_type = 'case/update/result';
    my $data = {
        case_id     => $self->id,
        result      => $self->resultaat,
        old_result  => $oudresultaat
    };

#    unless(my $recent_event = $self->logging->find_recent({
#        data        => $data,
#        zaak_id     => $self->id,
#        component   => 'zaak',
#        event_type  => $event_type,
#    })) {
#        warn "found no recent similar";
    $self->logging->trigger($event_type, {
        component   => 'zaak',
        zaak_id     => $self->id,
        data        => $data,
    });
#    }


    $self->update;
}


sub zaaktype_definitie {
    my $self    = shift;

    return $self->zaaktype_node_id->zaaktype_definitie_id;
}



#
# TODO this should be called 'field_value' .. it's not limited to 
# systeemkenmerken.
#
# my $field_value = $case->field_value({field_name => $field_name});
#
sub systeemkenmerk {
    my $self    = shift;
    my $label   = shift;

    die "gimme label" unless $label;

    if (my ($kenmerk_id) = $label =~ /^kenmerk\D*(\d+)$/) {
        my $kenmerken = $self->zaak_kenmerken->search(
            {
                bibliotheek_kenmerken_id    => $kenmerk_id
            }
        );

        my $count = $kenmerken->count;
        my $kenmerk = $kenmerken->first;
        return unless $kenmerk;

        my $value = $kenmerk->value;

        ### Make sure we handle arrays correctly:
        if ($count > 1) {
            while(my $multiple_kenmerk = $kenmerken->next()) {
                $value .= ", \n" . $multiple_kenmerk->value;
            }
        }

        my $replace_value = $value;

        ### Make sure valuta get's showed correctly
        if ($kenmerk->bibliotheek_kenmerken_id->value_type =~ /valuta/) {
            $replace_value =~ s/\,/\./g; # perl like dots in numbers
            $replace_value = sprintf('%01.2f', $replace_value);
            $replace_value =~ s/\./,/g;  # and we like commas. ok we should think about this
        }

        ### Make sure we show bag items the 'correct' way
        if ($kenmerk->bibliotheek_kenmerken_id->value_type =~ /^bag/) {

            $replace_value = $self->result_source->schema->resultset('BagNummeraanduiding')
                ->get_human_identifier_by_source_identifier($replace_value);
        }

        return $replace_value;
    }

    if (exists ZAAKSYSTEEM_STANDAARD_KOLOMMEN->{$label}) {
        return ZAAKSYSTEEM_STANDAARD_KOLOMMEN->{$label}->($self) || '';
    }

    unless (exists ZAAKSYSTEEM_STANDAARD_KENMERKEN->{$label}) {
        die "need zaaksysteem_standaard_kenmerken config ($label)";
        return '';
    }

    return ZAAKSYSTEEM_STANDAARD_KENMERKEN->{$label}->($self) || '';
}


=head2 is_late

Every case has an expiry date, calculated by the registration date +
the given expiry time, which is set in casetype management. If the expiry
date has passed and the case has not been closed yet, the case is considered
to be late.

Calculations happen on whole days. So first the timestamps are reduced to
dates, from which they are compared.

=cut

sub is_late {
    my $self = shift;

    my $reference = $self->is_afgehandeld ? $self->afhandeldatum : DateTime->now;

    # display a warning even when the case is already closed
    return $reference->truncate(to => 'day')->epoch >
        $self->streefafhandeldatum->truncate(to => 'day')->epoch;
}


# zaak->zaaktype_node_id->properties->{$property}
sub zaaktype_property {
    my ($zaak, $property) = @_;

    die "need zaak"     unless $zaak;
    die "need property" unless $property;

    my $zaaktype_node = $zaak->zaaktype_node_id or die "need zaaktype_node_id";
    
    my $properties = $zaaktype_node->properties or return; # older zaaktypen don't have this field. the return value is undef.

    return $properties->{$property};
}


=head2 zaaktype_resultaat

Returns the selected result object for this case. Used for systeemkenmerken.

=cut

sub zaaktype_resultaat {
    my ($zaak) = @_;

    return unless $zaak->resultaat;

    return $zaak->zaaktype_node_id->zaaktype_resultaten->search({ resultaat => $zaak->resultaat })->first;
}



sub status_perc {
    my $self            = shift;

    my $numstatussen    = $self->zaaktype_node_id->zaaktype_statussen->count;

    return 0 unless $numstatussen;

    # Force it down to an integer.
    return 0 + sprintf("%.0f", ($self->milestone / $numstatussen) * 100);
}


sub open_zaak {
    my $self    = shift;

    my $former_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar ? $self->behandelaar->id : undef,
        coordinator => $self->coordinator ? $self->coordinator->id : undef,
    };

    $self->status('open');

    my $current_user = $self->result_source
        ->schema
        ->resultset('Zaak')
        ->current_user;


    unless ($self->behandelaar) {
        $self->set_behandelaar($current_user->betrokkene_identifier);
    }

    unless ($self->coordinator) {
        $self->set_coordinator($current_user->betrokkene_identifier);
    }

    $self->update;

    my $new_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar->id,
        coordinator => $self->coordinator->id,
    };

    my $logging_row = $self->logging->trigger('case/accept', { component => 'zaak', data => {
        case_id         => $self->id,
        acceptee_name   => $current_user->naam,
        former_state    => $former_state,
        new_state       => $new_state,
    }});

    return $logging_row->id;
}


sub unrelate {
    my ($self, $related_id) = @_;

    if($self->relates_to && $self->relates_to->id == $related_id) {
        $self->relates_to(undef);
        $self->update;
    } else {
        my $related_case = $self->result_source->schema->resultset('Zaak')->find($related_id);

        $related_case->relates_to(undef);
        $related_case->update;
    }
}

sub set_verlenging {
    my $self    = shift;
    my $dt      = shift;

    $self->streefafhandeldatum($dt);
    $self->set_vernietigingsdatum;
    $self->update;
}


sub _bootstrap {
    my ($self, $opts)   = @_;

    $self->_bootstrap_datums($opts);
    $self->_bootstrap_route($opts);

    $self->update;
}


sub _bootstrap_datums {
    my ($self, $opts)   = @_;

    my $now             = DateTime->now;

    ### Registratiedatum not set, default to now
    if ($opts->{registratiedatum}) {
        $self->registratiedatum(
            $opts->{registratiedatum}
        );
        $now    = $opts->{registratiedatum};
    } elsif (!$self->registratiedatum) {
        $self->registratiedatum($now);
    }

    ### Streefbare afhandeling
    if ($opts->{streefafhandeldatum}) {
        $self->streefafhandeldatum(
            $opts->{streefafhandeldatum}
        );
    } else {
        my ($norm, $type)   = (
            $self->zaaktype_node_id->zaaktype_definitie_id->servicenorm,
            $self->zaaktype_node_id->zaaktype_definitie_id->servicenorm_type
        );

        if ($opts->{streefafhandeldatum_data}) {
            $norm   = $opts->{streefafhandeldatum_data}->{termijn};
            $type   = $opts->{streefafhandeldatum_data}->{type};
        }

        my $calculated = calculate_term({
            start  => $now,
            amount => $norm,
            type   => $type
        });

        $self->streefafhandeldatum($calculated);
    }
}

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};
    delete $self->{_column_data}->{object_type};

    $self->{_column_data}->{uuid} = $self->result_source->resultset->_generate_uuid
        unless (
            exists $self->{_column_data}->{uuid} &&
            $self->{_column_data}->{uuid}
        );

    $self->_handle_changes({insert => 1});
    $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $columns = shift;

    $self->set_inflated_columns($columns) if $columns;
    $self->_handle_changes;
    $self->next::method(@_);
}

sub _handle_changes {
    my $self    = shift;
    my $opt     = shift;
    my $changes = {};

    if ($opt && $opt->{insert}) {
        $changes = { $self->get_columns };
        $changes->{_is_insert} = 1;
    } else {
        $changes = { $self->get_dirty_columns };
    }

    $self->{_get_latest_changes} = $changes;

    $self->_set_search_string();

    return 1;
}

sub _set_search_string {
    my ($self) = @_;

    my $search_string = $self->zaaktype_node_id->titel || '';
    
    if($self->id) { # case may not exist yet
        $search_string .= " " . $self->id;
    }
    
    if($self->aanvrager && $self->aanvrager->naam) {
        $search_string .= " " . $self->aanvrager->naam;
    }
    
    if($self->behandelaar) {
        $search_string .= " " . $self->behandelaar->naam;
    }
    
    if($self->registratiedatum) {
        $search_string .= " " . $self->registratiedatum;
    }
    #$search_string .= " " . $self->locatie_zaak->id,
    
    if($self->onderwerp) {
        $search_string .= " " . $self->onderwerp;
    }
    
    if($self->zaaktype_node_id->zaaktype_trefwoorden) {
        $search_string .= " " . $self->zaaktype_node_id->zaaktype_trefwoorden;
    }   

    $self->search_term($search_string);
}

after 'insert'  => sub {
    my $self    = shift;

    $self->_handle_logging;
    $self->touch();
};

after 'update'  => sub {
    my $self    = shift;

    $self->_handle_logging;
    $self->touch();
};

=head2 touch(\%opts)

 $case->touch();

Touches a cases, and updates magic strings in onderwerp accordingly,
updates the search index etc.

=cut

has 'touched' => (
    is  => 'rw'
);

sub touch {
    my $self = shift;

    ### Allready in touch loop
    return if $self->touched;

    ### Check whether we want to postpone our touching
    if (
        $self->result_source->schema->default_resultset_attributes->{delayed_touch}
    ) {
        return $self->result_source->schema->default_resultset_attributes->{delayed_touch}->add_case(
            $self->id
        );
    } else {
        return $self->_touch;
    }
}

sub _touch {
    my $self                        = shift;

    $self->touched(1);

    eval {
        $self->result_source->schema->txn_do(sub {
            eval {
                $self->_refresh_onderwerp;
            };
            if (my $e = $@) {
                $self->result_source->schema->log(
                    sprintf("Error _refresh_onderwerp failed for %d: %s", $self->id, $e)
                );
            }
            $self->last_modified(DateTime->now);
            $self->update;
        });
    };

    if($@) {
        die("Could not touch case: $@");
        return;
    }

    eval {
        $self->result_source->schema->txn_do(sub {
            $self->update_hstore;
        });
    };

    warn('Failed propagating ObjectData: ' . $@) if $@;

    $self->touched(0);

    return 1;
}

sub _refresh_onderwerp {
    my $self        = shift;

    if (
        $self->zaaktype_node_id &&
        $self->zaaktype_node_id->zaaktype_definitie_id && 
        $self->zaaktype_node_id->zaaktype_definitie_id->extra_informatie
    ) {
        my $ztt = Zaaksysteem::ZTT->new;
        $ztt->add_context($self);

        my $extra_info = $self->zaaktype_node_id->zaaktype_definitie_id->extra_informatie;

        my $toelichting = $ztt->process_template($extra_info)->string;

        if ($toelichting) {
            $self->onderwerp(substr($toelichting,0,255));
        }
    }
}


=head2 can_delete

A case can be deleted when a set of rules is matched. This set
continues to evolve over time.

Status on Januari 2014:
- case is already deleted - necessary for recursive behaviour, we're gonna ask
  all relations if they're also cool with deletion, if they're already deleted
  they will be okay.
- Case must be closed
- vernietigingsdatum must have passed
- No unclosed ancestors, typically a deelzaak cannot be closed unless its hoofdzaak
  has been closed.
- No unclosed vervolgzaken

For several scenarios warnings have been required, so the mechanism has been split
up into errors and warnings. Warnings need to be confirmed by the GUI, since the
backend has no way on knowing about that, it just reports the warnings and carries
on with its business.

=cut

has 'can_delete'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->is_deleted ||
            !@{ $self->deletion_errors };
    }
);

=head2 deletion_errors

Returns a list with strings pointing out why this case can't be deleted.

=cut

has 'deletion_errors'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        my @errors = ();

        push @errors, "Zaak is nog niet afgehandeld"
            unless $self->is_afgehandeld;

        push @errors, "Bewaartermijn is niet verstreken"
            unless $self->te_vernietigen;

        push @errors, map {
            my $err;
            if ($_->is_afgehandeld) {
                $err = "Bewaartermijn van hoofdzaak " . $_->id . " niet verstreken";
            }
            else {
                $err = 'Hoofdzaak ' . $_->id . ' is nog niet afgehandeld';
            }
            $err;
        } grep { !$_->can_delete } $self->ancestors;

        push @errors, map {
            my $err;
            if($_->is_afgehandeld) {
                $err = "Bewaartermijn van deelzaak " . $_->id . " niet verstreken";
            }
            else {
                $err = "Deelzaak " . $_->id . " is nog niet afgehandeld";
            }
            $err;
        } $self->active_children;

        return \@errors;
    }
);


=head2 deletion_warnings

Returns a list with warnings that need to be read and confirmed by the user
before this case may be deleted.

=cut

has 'deletion_warnings'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        my @warnings = ();

        my @relations = $self->result_source->schema->resultset('CaseRelation')->get_all($self->id);

        # warn for related cases that are still active
        push @warnings, map {
            my $msg;
            if ($_->case->is_afgehandeld) {
                $msg = 'Bewaartermijn voor gerelateerde zaak ' . $_->case->id . ' is nog niet verstreken';
            }
            else {
                $msg = 'Gerelateerde zaak ' . $_->case->id . ' is nog niet afgehandeld';
            }
            $msg;
        } grep { !$_->case->can_delete } @relations;

        return \@warnings;
    }
);


=head2 ancestors

Return a list of ancestors (parent, grandparent, etc.) of the current case.

=cut

sub ancestors {
    my ($self) = @_;

    my $current = $self;

    my @visited;
    while ($current = $current->pid) {
        # Infinite loop protection.
        if (grep { $_->id == $current->id } @visited) {
            warn "Case " . $self->id . " has a loop in its parent hierarchy.\n";
            last;
        }

        push @visited, $current;
    }

    return @visited;
}

=head2 active_children

Return a list of active child cases.

=cut

sub active_children {
    my ($self, @children) = @_;

    # recursion protection, the db model allows my grandpa to be my son.
    # if this loop happens, no delete possible, because there is a living parent - me!
    if (@children && $self->id ~~ [@children]) {
        warn "glitch in the matrix, space-time continuum breached. case " . $self->id . " is own parent";
        return ($self); # if i am my active child, i'll return myself
    }

    grep { !$_->ready_for_destruction || $_->active_children(@children, $self->id) }
        $self->zaak_children->search->all;
}


# we can't use can_delete because of potential infinite recursion problems
sub ready_for_destruction {
    my $self = shift;

    return $self->is_deleted || ($self->is_afgehandeld && $self->te_vernietigen);
}



sub set_deleted {
    my $self = shift;

    throw('case/set_deleted/cant_delete',
        'Zaak ' . $self->id . ' kan niet verwijderd worden')
        unless $self->can_delete;

    # TODO children, relaties. cascade?

        $self->result_source->schema->txn_do(sub {
            my $case_id = $self->id;

            my $current_user = $self->result_source
                ->schema
                ->resultset('Zaak')
                ->current_user->naam;

            $self->_delete_zaak;

            # When the subject of a case no longer has any active cases pointing to it,
            # any object subscriptions that exist should be closed.
            # TODO: move this to subject code after dropout merge.
            if($self->aanvrager->betrokkene_type eq 'natuurlijk_persoon') {
                my ($object_subscription) = $self->result_source->schema->resultset('ObjectSubscription')->search({
                    date_deleted => undef,
                    local_table => 'NatuurlijkPersoon',
                    local_id    => $self->aanvrager_object->gmid,
                });

                if ($object_subscription) {
                    my $has_active_case;
                    my $np_case_subject = $self->result_source->schema->resultset('ZaakBetrokkenen')->search({
                        betrokkene_type => 'natuurlijk_persoon',
                        gegevens_magazijn_id   => $self->aanvrager->gegevens_magazijn_id,
                        deleted         => undef,
                    });

                    while (my $case_subject = $np_case_subject->next()) {
                        for my $case ($case_subject->zaak_aanvragers) {
                            # Case needs to have status deleted, unless it's the one we are
                            # currently trying to delete.
                            if (!$case->is_deleted && $case->id != $self->id) {
                                $has_active_case = 1;
                                last;
                            }
                        }
                    }
                    if (!$has_active_case) {
                        $object_subscription->object_subscription_delete;
                    }
                }
            }

            $self->logging->trigger('case/delete', { component => 'zaak', data => {
                case_id         => $case_id,
                acceptee_name   => $current_user,
            }});
        });

    return 1;
}

sub is_deleted {
    my $self = shift;

    return $self->deleted && $self->deleted <= DateTime->now();
}

sub _delete_zaak {
    my $self = shift;

    my @relation_views = $self->zaak_relaties();
    for my $x (@relation_views) {
        $x->relation->delete();
    }

    $self->logging->delete_all;

    $self->status('deleted');
    $self->deleted(DateTime->now());
    $self->update;
}

sub zaak_relaties {
    my $self = shift;

    return $self->result_source->schema->resultset('CaseRelation')->get_sorted(
        $self->get_column('id')
    );
}

sub _get_latest_changes {
    my $self    = shift;

    return $self->{_get_latest_changes};
}

sub _handle_logging {}

sub duplicate {
    my $self    = shift;
    $self->result_source->schema->resultset('Zaak')->duplicate( $self, @_ );
}

sub wijzig_zaaktype {
    my $self    = shift;
    $self->result_source->schema->resultset('Zaak')->wijzig_zaaktype($self, @_);
}

# CINE for create-if-not-exists
sub case_actions_cine {
    my $self = shift;

    # Don't apply rules to unsorted action resultsets, results go all screwy
    # as the actions may not be retrieved in order.
    my $rs = $self->case_actions(@_)->sorted;

    $rs->apply_rules({ case => $self });

    return $rs if $rs->count;

    $rs->create_from_case($self);

    return $rs->reset;
}

sub format_payment_status {
    my $self = shift;

    my $payment_status = $self->payment_status;

    if($payment_status) {
        return ZAAKSYSTEEM_CONSTANTS->{payment_statuses}->{$payment_status} || $payment_status;
    }
}

sub format_payment_amount {
    my $self = shift;

    my $payment_amount = $self->payment_amount;
    my $formatted = $payment_amount ? sprintf("%0.2f", $payment_amount) : '-';
    $formatted =~ s/\./,/g;

    return $formatted;
}

sub set_payment_status {
    my $self = shift;
    
    my $status = shift;
    my $amount = shift;

    $self->payment_status($status);
    $self->update();
    
    $self->logging->trigger('case/payment/status', {
        component => 'zaak',
        zaak_id => $self->id,
        data => {
            case_id => $self->id,
            status_code => $self->payment_status,
            status => $self->format_payment_status,
            amount => $amount
        }
    });
}

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        uuid => $self->uuid,
        pid => $self->pid,

        afhandeldatum => $self->afhandeldatum,
        contactkanaal => $self->contactkanaal,
        created => $self->created,
        deleted => $self->deleted,
        last_modified => $self->last_modified,
        milestone => $self->milestone,
        object_type => $self->object_type,
        onderwerp => $self->onderwerp,
        registratiedatum => $self->registratiedatum,
        streefafhandeldatum => $self->streefafhandeldatum,
        vernietigingsdatum => $self->vernietigingsdatum,
        zaaktype_id => $self->zaaktype_id,
        zaaktype_node_id => $self->zaaktype_node_id,
        days_perc => $self->status_perc,
        resultaat => $self->resultaat,

        route_ou => $self->route_ou,
        route_role => $self->route_role,

        locatie_correspondentie => $self->locatie_correspondentie,
        locatie_zaak => $self->locatie_zaak,

        aanvraag_trigger => $self->aanvraag_trigger,
        aanvrager => $self->aanvrager_object,
        behandelaar => $self->behandelaar_object,
        coordinator => $self->coordinator_object,
    };
}

sub check_queue_coworker_changes {
    my ($self, $user_id) = @_;

    return
        $self->zaaktype_node_id->properties->{queue_coworker_changes} &&
        $self->behandelaar &&
        $self->behandelaar->gegevens_magazijn_id ne $user_id;
}

=head2 object_type

Return the object_type (used while zaken are "dual" zaak/object_data)

XXX Temporary, remove once "zaak" is in object_data!

=cut

sub object_type { return 'case' }

sub set_confidentiality {
    my ($self, $value) = @_;

    my $old = $self->confidentiality;
    my $new = $self->confidentiality($value);

    $self->logging->trigger('case/update/confidentiality', {
        component => 'zaak',
        zaak_id   => $self->id,
        data      => {
            new => $new,
            old => $old
        }
    });

    $self->update;
}


=head2 display_flash_messages

On display of a case some flash messages need to be shown to the user.
This generates a list with the message, view layer can relay.


=cut

sub display_flash_messages {
    my $self = shift;

    my @messages;

    if (
        $self->aanvrager_object &&
        $self->aanvrager_object->can('messages') &&
        $self->aanvrager_object->messages &&
        scalar(keys %{ $self->aanvrager_object->messages })
    ) {
        my $aanvrager_messages = $self->aanvrager_object->messages;

        push @messages, 'Let op: ' .
            join ', ',
            map { ucfirst($_) }
            values %{ $self->aanvrager_object->messages };
    }

    my $payment_status = $self->payment_status || '';
    if ($payment_status eq CASE_PAYMENT_STATUS_FAILED) {
        push @messages, {
            message     => 'Let op, betaling niet succesvol',
            type        => 'error'
        };
    }

    if ($payment_status eq CASE_PAYMENT_STATUS_PENDING) {
        push @messages, {
            message => 'Let op, betaling (nog) niet afgerond',
            type => 'error'
        };
    }

    return @messages;
}

=head2 active_files

Retrieve a consistent selection of active files.

=cut

sub active_files { shift->files->active_files }


1; #__PACKAGE__->meta->make_immutable;

=head1 COMPONENTS

=head2 Magic Strings

=over 4

=item L<Zaaksysteem::Zaken::Roles::ZTT> - Magic string handling

=back


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


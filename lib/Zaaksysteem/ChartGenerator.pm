package Zaaksysteem::ChartGenerator;

use Moose;
use namespace::autoclean;
use Data::Dumper;

has 'resultset' => (
    is => 'rw', 
    required => 1,
);


has 'behandelaars_cache' => (
    is => 'rw',
    default => sub { {} },
);

with
    'Zaaksysteem::ChartGenerator::Roles::Status',
    'Zaaksysteem::ChartGenerator::Roles::Handling_time',
    'Zaaksysteem::ChartGenerator::Roles::Average_handling_time_per_month',
    'Zaaksysteem::ChartGenerator::Roles::Cases_per_department',
    'Zaaksysteem::ChartGenerator::Roles::Cases_per_owner_per_month',
    'Zaaksysteem::ChartGenerator::Roles::Cases_per_department_per_month',
    'Zaaksysteem::ChartGenerator::Roles::Cases_within_and_outside_term_per_month',
    'Zaaksysteem::ChartGenerator::Roles::Cases_within_and_outside_term_per_month_per_department',
    'Zaaksysteem::ChartGenerator::Roles::Average_handling_time_per_owner';


=head2 unsparsify

hash matrix is sparse, will look like this:
{
    'januari 2013' => {
        'department1' => 23,
        'department2' => 75,
    },
    'februari 2013' => {
        'department1' => 2,
        'department3' => 67,
    }
}

to make the chart work for highcharts, $hash_matrix needs
to written like this, with zeroes where the was no results
from the database. unsparsed.
[{
    'name' => 'department1',
    'data' => [23, 0, 2],
},{
    'name' => 'department2',
    'data' => [75, 0, 0],
},{
    'name' => 'department3',
    'data' => [0, 0, 67],
}]
=cut

sub unsparsify {
    my ($self, $arguments) = @_;

    my $unique_items = $arguments->{unique_items} or die "need unique_items";
    my $hash_matrix = $arguments->{hash_matrix} or die "need hash_matrix";
    my $categories = $arguments->{categories} or die "need categories";
    my $y_objects = $arguments->{y_objects}; # optional, points are represented as objects instead of simple values
    my $series = [];

    foreach my $unique_item (sort keys %$unique_items) {
        my $data = [];
        foreach my $category (@$categories) {
            my $values = $hash_matrix->{$category};

            if($y_objects) {
                my $object = $values->{$unique_item} || { y => int 0 };
                push @{ $data }, $object;
            } else {
                my $count = $values->{$unique_item} || 0;
                push @{ $data }, int $count;
            }

        }
        push @$series, { name => $unique_item, data => $data };
    }

    return $series;
}



=head2 generate

Generate a chart profile, looks for a given profile function and executes.
Main goal is to limit access.

=cut

sub generate {
    my ($self, $arguments) = @_;

    my $profile = $arguments->{profile} or die "need profile";

    # gatekeeper
    die "unknown profile $profile" unless grep { $_ eq $profile } qw/
        status
        handling_time
        average_handling_time_per_month
        average_handling_time_per_owner
        cases_per_department
        cases_per_department_per_month
        cases_per_owner_per_month
        cases_within_and_outside_term_per_month
        cases_within_and_outside_term_per_month_per_department
    /;

    my $chart_profile = $self->$profile;
    warn Dumper $chart_profile;
    return $chart_profile;
}


sub BUILD {
    my ($self) = @_;

    # get rid of some candy from up the chain so DBIx::Class can do grouping magic
    delete($self->resultset->{attrs}->{'+as'});
    delete($self->resultset->{attrs}->{'+select'});
    delete($self->resultset->{attrs}->{'prefetch'});
    delete($self->resultset->{attrs}->{'order_by'});
}


__PACKAGE__->meta->make_immutable;

1;

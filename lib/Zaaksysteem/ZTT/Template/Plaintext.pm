package Zaaksysteem::ZTT::Template::Plaintext;

use Moose;

use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::Element;
use Zaaksysteem::ZTT::Tag;
use Zaaksysteem::ZTT::Selection::Plaintext;
use Zaaksysteem::Exception;

BEGIN { extends 'Zaaksysteem::ZTT::Template' };

has string => ( is => 'rw', isa => 'Str', required => 1 );

sub tag_selections {
    my $self = shift;

    my @retval;

    # Get all '[[ thing]]' tags. 
    my @tags = $self->string =~ /(\[\[\s*(\w+)\s*\]\])/g;

    my $pos = 0;

    while (my $selection = shift @tags) {
        my $tagname = shift @tags;

        my $start = index($self->string, $selection, $pos);

        push @retval, Zaaksysteem::ZTT::Selection::Plaintext->new(
            start => $start,
            selection => $selection,
            tag => Zaaksysteem::ZTT::Tag->new( name => $tagname )
        );

        $pos = $start + length $selection;
    }

    return @retval;
}

sub inline_iterators {
    my $self = shift;

    my @retval;

    my @tags = $self->string =~ /(\[\[\s*itereer:(\w+):([\w\.]+)\s*\]\])/g;

    my $pos = 0;

    while(my $selection = shift @tags) {
        my $iterator_type = shift @tags;
        my $tagname = shift @tags;

        my $start = index($self->string, $selection, $pos);

        push @retval, Zaaksysteem::ZTT::Selection::Plaintext->new(
            start => $start,
            selection => $selection,
            iterate => $iterator_type,
            tag => Zaaksysteem::ZTT::Tag->new( name => $tagname )
        );

        $pos = $start + length $selection;
    }

    return @retval;
}

sub sections {
    my $self = shift;

    my @retval;

    my $open_tag_match = qr[{{ itereer over\s*(\w+)];
    my $close_tag_match = qr[}}];

    my $replacement_tag = '{{ __ITERATOR__ }}';

    my @template_lines;

    my @iterator_blocks;
    my @current_iterator_block_lines; 

    my $iterator_block_type;

    # Newlines can be \n or \r, or any combination of this depending on the platform
    for my $line (split m/[\n]/, $self->string) {

        # new subtemplate found
        if (my ($block_type) = $line =~ $open_tag_match) {
            $iterator_block_type = $block_type;

            # new subtemplate found, clear in-progress lines
            undef @current_iterator_block_lines;

            next;
        }

        # new subtemplate-end found
        if ($line =~ $close_tag_match) {

            # Add current iterator block lines to iterator blocks array
            push @iterator_blocks, [
                $iterator_block_type,
                join("\n", @current_iterator_block_lines)
            ];

            # Add replacement tag for it in original doc
            push @template_lines, $replacement_tag;

            # Clear iterator block type
            undef $iterator_block_type;

            next;
        }

        # just normal lines in (sub)template
        if(defined $iterator_block_type) {

            # In subtemplate, add line to current subtemplate block
            push @current_iterator_block_lines, $line;
        } else {

            # not in a subtemplate, add line to main template
            push @template_lines, $line;
        }
    }

    $self->string(join "\n", @template_lines);

    for my $subtemplate (@iterator_blocks) {
        my ($iterator_type, $subtemplate) = @{ $subtemplate };

        push @retval, Zaaksysteem::ZTT::Selection->new(
            selection => $replacement_tag,
            iterate => $iterator_type,
            subtemplate => $subtemplate
        );
    }

    return @retval;
}

sub replace {
    my $self = shift;

    my $text = $self->string;

    my $selection = shift;
    my $selected_text = $selection->selection;

    my $replacement = shift;

    return unless $replacement;

    $replacement->sanitize;

    my $value;

    if($replacement->type eq 'richtext') {
        $value = flatten_richtext($replacement->value);
    } elsif($replacement->type eq 'plaintext') {
        $value = $replacement->value;
    } else {
        throw('ztt/template/plaintext', sprintf(
            "Don't know how to handle elements of type '%s'",
            $replacement->type
        ));
    }

    $value ||= '';

    $text =~ s[\Q$selected_text\E][$value];

    $self->string($text);
}

sub iterate {
    my $self = shift;
    my $contexts = shift;
    my $selection = shift;
    my $subtemplate = $selection->subtemplate;

    my @sub_texts;

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new;
        $ztt->add_context($context);

        push @sub_texts, $ztt->process_template($subtemplate)->string;
    }

    # Do the actual replace
    $self->replace($selection, Zaaksysteem::ZTT::Element->new(
        value => join "\n", @sub_texts)
    );
}

sub iterate_inline {
    my $self = shift;
    my $contexts = shift;
    my $selection = shift;

    my @values;

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new;
        
        $ztt->add_context($context);

        push @values, $ztt->process_template(sprintf('[[%s]]', $selection->tag->name))->string;
    }

    $self->replace(
        $selection,
        Zaaksysteem::ZTT::Element->new(value => join(', ', @values))
    );
}

sub flatten_richtext {
    my @elements = @{ shift() };
    my @lines;

    my $link_cleaner = sub {
        my $element = shift;
        my $text = $element->as_trimmed_text;

        for my $a ($element->find('a')) {
            my $flattened = sprintf('%s (%s)', $a->as_trimmed_text, $a->attr('href'));
            my $title = $a->as_trimmed_text;

            $text =~ s[\Q$title\E][$flattened];
        }

        return $text;
    };

    my %dispatch = (
        p => sub { $link_cleaner->(shift) },

        ul => sub {
            my $element = shift;

            return join("\n", map
                { sprintf('%s %s', chr(0x2022), $link_cleaner->($_)) }
                $element->find('li')
            )
        },

        ol => sub {
            my $element = shift;
            my $iter = 1;

            return join("\n", map
                { sprintf('%d. %s', $iter++, $link_cleaner->($_)) }
                $element->find('li')
            );
        }
    );

    for my $element (@elements) {
        next unless exists $dispatch{ $element->tag };

        push @lines, $dispatch{ $element->tag }->($element);
    }

    return join "\n\n", @lines;
}

1;

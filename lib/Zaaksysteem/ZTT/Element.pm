package Zaaksysteem::ZTT::Element;

use Moose;
use Data::Dumper;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use HTML::TreeBuilder;

has attribute => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::ZaaktypeKenmerken',
);

has value => ( is => 'rw' );
has type => ( is => 'rw', isa => 'Str', default => 'plaintext' );

has cleaners => (
    is => 'ro',
    default => sub { return [
        
        # This sanitization check goes on top, if the attribute
        # is a file, we don't want to do any processing at all
        sub {
            my $self = shift;

            return 1 if shift eq 'file';
        },

        # If the returned value is an array, we flatten it beforehand
        sub {
            my $self = shift;

            if (UNIVERSAL::isa($self->value, 'ARRAY')) {
                $self->value(join(", \n", @{ $self->value }));
            }

            return;
        },

        # In case of a richtext attribute, we return the HTML::Elements
        # we actually wanna look at, and let the template class figure it out
        sub {
            my $self = shift;
            return unless shift eq 'richtext';

            # Force string context
            my $html = '' . $self->value;
            $html =~ s[<br \/>][\n]g;

            my $builder = HTML::TreeBuilder->new;
            $builder->implicit_body_p_tag(1);
            $builder->p_strict(1);
            $builder->no_space_compacting(1);

            my $tree = $builder->parse($html);
            $builder->eof;

            $self->value([ $tree->find(qw[p ul ol]) ]);
            $self->type('richtext');

            return;
        },

        sub {
            my $self = shift;

            return unless shift =~ m[valuta];

            my $value = $self->value || '';

            return unless length $value;

            $value =~ s[,][.]g;
            $value = sprintf('%01.2f', $value);
            $value =~ s[\.][,]g;

            $self->value($value);

            return;
        },

        sub {
            my $self = shift;

            return unless shift =~ m[^bag];

            my $value = $self->value || '';

            return unless $value =~ m[\w+\-\d+];

            my $rs = $self->attribute->result_source->schema->resultset('BagNummeraanduiding');

            my $bag = $rs->get_record_by_source_identifier($value);

            $self->value($bag->to_string) if $bag;

            return;
        }
    ]; }
);

sub sanitize {
    my $self = shift;

    # run all the cleaners. if one of these returns truthy, don't run the rest
    for my $code (@{ $self->cleaners }) {
        last if $self->$code(
            $self->attribute ? $self->attribute->bibliotheek_kenmerken_id->value_type : ''
        );
    }

    return $self;
}

1;

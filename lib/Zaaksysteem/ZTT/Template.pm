package Zaaksysteem::ZTT::Template;

use Moose;

use Zaaksysteem::Exception;

use Zaaksysteem::ZTT::Modification;

use Zaaksysteem::ZTT::Template::Plaintext;
use Zaaksysteem::ZTT::Template::OpenOffice;

sub new_from_thing {
    my $class = shift;
    my $thing = shift;

    # If the thing is not a reference to anything, assume it's at least stringable
    unless (ref $thing) {
        return Zaaksysteem::ZTT::Template::Plaintext->new(string => "$thing");
    }
    
    if (eval { $thing->isa('OpenOffice::OODoc::Document'); }) {
        return Zaaksysteem::ZTT::Template::OpenOffice->new(document => $thing);
    }

    throw('ztt/template/factory', sprintf(
        'Unable to figure out what the supplied template actually is and how to handle it (%s)',
        ref $thing
    ));
}

sub tag_selections { }

sub sections { }

sub modifications {
    my $self = shift;

    my @mods;

    for my $selection ($self->sections) {
        push @mods, Zaaksysteem::ZTT::Modification->new(
            type => 'iterate',
            selection => $selection
        );
    }

    for my $selection ($self->tag_selections) {
        push @mods, Zaaksysteem::ZTT::Modification->new(
            type => 'replace',
            selection => $selection
        );
    }

    for my $selection ($self->inline_iterators) {
        push @mods, Zaaksysteem::ZTT::Modification->new(
            type => 'iterate_inline',
            selection => $selection
        );
    }

    return @mods;
}

1;

package Zaaksysteem::ZTT::Modification;

use Moose;

has type => ( is => 'ro', isa => 'Str', required => 1 );
has selection => ( is => 'ro', isa => 'Zaaksysteem::ZTT::Selection', required => 1);
has data => ( is => 'rw', isa => 'HashRef' );

sub as_string {
    my $self = shift;
    
    return sprintf(
        "at %s, %s %s",
        $self->selection->as_string,
        $self->type,
        'data'
    );
}

1;

package Mail::Track::Message::Body;
use Moose;

use File::Basename;
use MIME::QuotedPrint qw(decode_qp);
use Encode qw(encode decode);

# See: http://msdn.microsoft.com/en-us/library/gg672007(v=exchg.80).aspx
use constant BODY_TYPES =>
    [qw|text/plain text/html text/enriched text/calendar|];

=head1 NAME

Mail::Track::Message::Body - Mail message body object

=head1 SYNOPSIS

    my $body        = $mail_message->body;

    $body->as_text;
    $body->as_html;


=head1 DESCRIPTION

Returned Body object for L<Mail::Track::Message>,
please DO NOT USE THIS OBJECT DIRECTLY. Use L<Mail::Track>
or if you dare: B<Mail::Track::Message>

=head1 ATTRIBUTES

=head2 as_text

isa: Str

Returns the body as plain text

=cut

has as_text => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    builder => '_build_as_text',
);

=head2 as_html

isa: Str

Returns the body as html

=cut

has as_html => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    builder => '_build_as_html',
);

=head2 parts

isa: MayBe[ArrayRef]

A MIME::Message normally consists of multiple body parts. A C<<text/plain>> version,
and most of the time also a rich version, like a C<<text/html>> version. We collect
the different parts in this ArrayRef

=cut

has 'parts' => (
    'is'      => 'ro',
    'isa'     => 'Maybe[ArrayRef]',
    'lazy'    => 1,
    'default' => sub { []; },
);

=head2 entity

isa: MIME::Message

Result object of the body part

=cut

has entity => (
    is  => 'ro',
    isa => 'MIME::Entity',
);

has encoding => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return 'UTF-8';
    },
);

=head1 INTERNAL METHODS

=head2 get_body_content_type($mime_entity)

Return value: $CONTENT_TYPE or undef

    my $content_type = Mail::Track::Message::Body->get_body_content_type($part)

Returns the content_type of the body, but only if the content_type is allowed
according to the C<BODY_TYPES> constant. This way you could also test if the part
contains a body allowable content_type

=cut

sub get_body_content_type {
    my $self   = shift;
    my $entity = shift;

    my $content_type = lc($entity->effective_type);

    return unless grep { $content_type eq $_ } @{ BODY_TYPES() };

    return $content_type;
}

=head2 _build_body_from_parts($TYPE)

Return value: $BOOLEAN_TRUE

Completes this object by filling the as_html and as_text attributes

B<Types>: defines the return value

=over 4

=item plain

Returns the plain text value for this body

=item rich

Returns the rich (HTML) value for this body

=back

=cut

sub _build_body_from_parts {
    my $self = shift;

    return { as_text => '', as_html => '' } unless @{ $self->parts };

    my (@plain_body, @rich_body);
    for my $preferred_type (@{ BODY_TYPES() }) {
        if ($preferred_type eq 'text/plain') {
            @plain_body = $self->_parts_to_body($preferred_type);
        }
        else {
            @rich_body = $self->_parts_to_body($preferred_type);
        }

        last if scalar @rich_body;
    }


    if (!scalar(@plain_body) && scalar(@rich_body)) {
        @plain_body = @rich_body;
    }

    return {
        as_text => join("\n", @plain_body),
        as_html => join("\n", @rich_body),
    };
}

sub _parts_to_body {
    my ($self, $preferred_type) = @_;

    my @body;
    foreach (@{ $self->parts }) {
        next if $preferred_type ne $_->effective_type;
        my $enc = $_->head->mime_attr('content-type.charset');
        my $txt = decode_qp($_->bodyhandle->as_string || '');
        push(@body, encode($self->encoding, decode($enc ? $enc :$self->encoding, $txt)));
    }
    return @body;
}

sub _build_as_text {
    my $self = shift;

    return $self->_build_body_from_parts->{as_text};
}

sub _build_as_html {
    my $self = shift;

    return $self->_build_body_from_parts->{as_html};
}


1;

__END__

=head1 EXAMPLES

See L<#SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

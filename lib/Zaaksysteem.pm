package Zaaksysteem;

=head1 NAME

Zaaksysteem - Case management system for improving business processes

=head1 SYNOPSIS

  # When cloned from source, you can start the local development server by running:

  bash$ ./script/zaaksysteem_server.pl

  ### Alternative, start fastcgi process with two instances

  bash# ./script/dev_fastcgi.pl

=head1 DESCRIPTION

Zaaksysteem is a case management solution for businesses. Althoug we primarily
focues on local government projects. The program is widely used in other SaaS
deployments

=head1 DOCUMENTATION

User documentation can be found on our wiki page:
L<http://wiki.zaaksysteem.nl/>

For development documentation, which gives insight in our API documentation. But also for
connecting Frontend code to our Backend, please start by reading our Manual:

L<Zaaksysteem::Manual>

This document gives insight in the API for Zaaksysteem

=head1 METHODS

B<Please, do NOT create any methods in this namespace, please use:>

L<Zaaksysteem::General::Actions>

You will be able to test them better, and documentation is in one place

=cut

use Moose;

use Data::Dumper;
use Sys::Hostname;
use IO::Socket;
use Try::Tiny;
use JSON::Path;

use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Exception;
use Zaaksysteem::Request;
use Zaaksysteem::Cache;

use Time::HiRes qw/gettimeofday tv_interval/;

use Catalyst qw/
    ConfigLoader
    Static::Simple

    Authentication
    Authorization::Roles

    Params::Profile

    Unicode::Encoding
    I18N
    ClamAV

    +Zaaksysteem::StatsD
    +Zaaksysteem::XML::Compile
    CustomErrorMessage
/;

# Plugins which will be loaded based on env variables
my @additional_plugins = qw(
    Session
    Session::State::Cookie
);
## ZS-2385/ZS-2645
my $memcache_or_file = 'File';
if ($ENV{ZS_MEMCACHED}) {
    $memcache_or_file = 'Memcached';
}
push(@additional_plugins, "Session::Store::$memcache_or_file");

use CatalystX::RoleApplicator;

extends qw/Catalyst Zaaksysteem::General/;

# Changed the version to adhere Perl best practises, using rc notation could break stuff
our $VERSION = 'v3.17.3';

#extends 'Catalyst';
#with 'CatalystX::LeakChecker';
#with 'CatalystX::LeakChecker';

__PACKAGE__->request_class('Zaaksysteem::Request');
__PACKAGE__->apply_request_class_roles(qw/
    Zaaksysteem::TraitFor::Request::Params
/);

# Configure the application.
#
# Note that settings in zaaksysteem.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

sub _assert_configfile {
    foreach ($ENV{ZAAKSYSTEEM_CONF}, '/etc/zaaksysteem/zaaksysteem.conf', 'etc/zaaksysteem.conf') {
        next if !defined $_;
        return $_ if (-f $_);
    }
    throw('Zaaksysteem', "No configfiles found, unable to start Zaaksysteem");
}

my $configfile  = _assert_configfile;

__PACKAGE__->config(
    'Plugin::ConfigLoader' => {
        file => $configfile
    },
    'name'              => 'Zaaksysteem',
    'View::TT'          => {
        tpl       => 'zaak_v1',
        locale    => 'nl_NL',
    },
    'View::JSON' => {
        allow_callback  => 0,
        expose_stash    => 'json',
    },
    'View::JSONlegacy' => {
        allow_callback  => 0,    # defaults to 0
        expose_stash    => [ qw(json) ],
    },
    'View::Email'          => {
        stash_key => 'email',
        template_prefix => 'tpl/zaak_v1/nl_NL',
        default => {
            content_type => 'text/plain',
            charset => 'utf-8',
            from    => 'info@zaaksysteem.nl',
            view        => 'TT',
        },
        sender => {
            'mailer'    => 'Sendmail'
        }
    },
    'default_view'      => 'TT',
    'static'            => {
        ignore_extensions => [ qw/tmpl tt tt2/ ],
    },

    recaptcha => {
        # Keys for https://www.google.com/recaptcha/admin/site?sideid=316632375
        #pub_key => '6Lc3bd8SAAAAAMbd21cpDAXUZfdT3m0Q_FAocTdZ',
        #priv_key => '6LcTR98SAAAAAL-5OlXaxoac-OUjvS7M6IeRSD-1',

        # Keys for https://www.google.com/recaptcha/admin/site?siteid=316622611
        #pub_key => '6LcTR98SAAAAAHveMZhxipvBZKo9OwA_WMqWX9EX',
        #priv_key => '6LcTR98SAAAAAL-5OlXaxoac-OUjvS7M6IeRSD-1',

        # Keys for # https://www.google.com/recaptcha/admin/site?siteid=316632983
        #pub_key => '6LeXb98SAAAAAL1tl8vNTFRhaOpWJtzUEn0FmGUb',
        #priv_key => '6LeXb98SAAAAADFQHXt7ZvSR3Rm5IT4aOlVx9Lba',

        pub_key => '6LfWuOkSAAAAAE9lve8PsEIBSf7FkBeTeE4Wa-i4',
        priv_key => '6LfWuOkSAAAAABYINXupw1FyybAADV7I4FIgp9TE',
        options => { theme => 'clean', lang => 'nl' }
    },

    'custom-error-message' => {
        'error-template' => 'error.tt',
    },
);

__PACKAGE__->mk_classdata($_) for qw/
    _additional_static
/;

__PACKAGE__->log(Zaaksysteem::Log::CallingLogger->new());

### Define state of machine, dev?
{

    my $otap = 'dev';
    my $hostname = hostname();
    if ($hostname =~ /^app/) {
        $otap = 'prod';
    }
    elsif ($hostname =~ /^(demo|accept|test)/) {
        $otap = $1
    }

    my $storage = "/tmp/ezra_$otap";
    if ($otap eq 'dev') {
        $storage .= '_' . getpwuid($<) // $<;
    }

    __PACKAGE__->config(
        otap => $otap,
        'Plugin::Session' => {
            cookie_secure => 2,
            storage       => $storage,
        },
    );
}

### Define config location
{
    use File::Basename;
    my $working_directory = dirname($configfile);
    __PACKAGE__->config->{config_directory} = $working_directory;
}

# Start the application
__PACKAGE__->setup(@additional_plugins);

__PACKAGE__->xml_compile->add_class([qw/
    Zaaksysteem::StUF::0204::Instance
    Zaaksysteem::StUF::0301::Instance
/
]);

# Configure the customers
{
    __PACKAGE__->mk_classdata('customer');

    ### Load customer configuration from customer.d
    if (-d __PACKAGE__->config->{config_directory} . '/customer.d') {
        my $cfgs        =  Config::Any->load_files(
            {
                files => [
                    glob(
                        __PACKAGE__->config->{config_directory}
                        . '/customer.d/*.conf'
                    )
                ],
                use_ext => 1,
                flatten_to_hash => 1,
            }
        );

        for my $configfile (values %{ $cfgs }) {
            for my $customer (keys %{ $configfile }) {
                __PACKAGE__->config->{customers} = {} unless UNIVERSAL::isa(
                    __PACKAGE__->config->{customers}, 'HASH'
                );

                __PACKAGE__->config->{customers}->{$customer} = $configfile->{ $customer };
            }

        }
    }

    if (
        __PACKAGE__->config->{customers} &&
        UNIVERSAL::isa(__PACKAGE__->config->{customers}, 'HASH')
    ) {
        __PACKAGE__->customer({});
        for my $host (keys %{ __PACKAGE__->config->{customers} }) {
            __PACKAGE__->customer->{$host} = {
                'dbh'           => undef,
                'dbgh'          => undef,
                'ldaph'         => undef,
                'start_config'  => __PACKAGE__->config
                    ->{customers}
                    ->{$host},
                'run_config'    => undef,
            };
        }
    } else {
        die('Error: no customers defined in zaaksysteem.conf');
    }

    sub customer_instance {
        my $c           = shift;

        my $hostname;

        if (!ref($c) && $ENV{'ZAAKSYSTEEM_CURRENT_CUSTOMER'}) {
            $hostname = $ENV{'ZAAKSYSTEEM_CURRENT_CUSTOMER'}
        } else {
            $hostname    = $c->req->uri->host;
        }

        unless (__PACKAGE__->customer->{ $hostname }) {
            ### Second try (subdomains etc)
            for my $host (keys %{ __PACKAGE__->config->{customers} }) {
                ### XXX TODO
            }

            die(
                'Could not find configuration for hostname: '
                . $hostname
            );
        }

        my $customerdata    = __PACKAGE__->customer->{ $hostname };

        if ($customerdata->{start_config}->{dropdir}) {
            __PACKAGE__->config->{dropdir} = $customerdata
                ->{start_config}
                ->{dropdir};
        }

        my $tt_template = 'zaak_v1';
        if ($customerdata->{start_config}->{template}) {
            $tt_template = $customerdata->{start_config}->{template};
        }
        if ($customerdata->{start_config}->{customer_id}) {
            __PACKAGE__->config->{gemeente_id} = $customerdata->{start_config}->{customer_id}
        }

        __PACKAGE__->_additional_static([
            __PACKAGE__->config->{root},
            __PACKAGE__->config->{root} . '/tpl/'
                . $tt_template
                . '/' . __PACKAGE__->config->{'View::TT'}->{locale}
        ]);

        __PACKAGE__->config->{static}->{include_path} = __PACKAGE__->_additional_static;

        if ($customerdata->{start_config}->{customer_info}) {
            __PACKAGE__->config->{gemeente} =
                $customerdata->{start_config}->{customer_info};
        }

        my $tmp_version = $VERSION;
        $tmp_version =~ s/_/rc/;
        $tmp_version =~ s/^v//;

        __PACKAGE__->config->{'SVN_VERSION'} = $tmp_version;

        if ($customerdata->{start_config}->{publish}) {
            __PACKAGE__->config->{publish} =
                $customerdata->{start_config}->{publish};
        }

        return $customerdata;
    }
}

__PACKAGE__->config->{ 'Plugin::Captcha' } = {
    session_name => 'captcha_string',
    new => {
        width => 300,
        height => 100,
        scramble => 1,
        ptsize => 34,
        frame => 2,
        rndmax => 5,
        thickness => 1,
        lines => 25,
        color => '#FFCC00',
    },
    create => ['ttf', 'circle', '#3E8FA4', '#999',],
    particle => [1500],
    out => {force => 'jpeg'}
};

sub _setup_authentication_interface {
    my $c           = shift;

    my $realm       = $c->get_auth_realm('default');

    my $interface   = $c->model('DB::Interface')->find_by_module_name('authldap');

    unless($interface) {
        throw('auth/interface', sprintf(
            'Unable to locate interface with module "authldap"',
        ));
    }

    $realm->credential->interface($interface);

    $c->model('DB')->schema->default_resultset_attributes->{ current_user } = $c->model('DB')->schema->current_user(undef);
}

sub _finish_graphing {
    my $c           = shift;
    my $t0          = shift;

    my $result      = int(tv_interval ( $t0, [gettimeofday])*1000);

    my $status      = $c->response->status;
    if (scalar @{ $c->error }) {
        $status = 500;
    }
    #$c->log->debug('Request overall took: ' . $result . ' / status: ' . $c->response->status);
    $c->statsd->timing('request.time', $result);
    $c->statsd->increment('response.status.' . $status, 1);
}

around 'dispatch' => sub {
    my $orig    = shift;
    my $c       = shift;

    my $t0      = [gettimeofday];

    $c->_setup_authentication_interface();

    my $ret     = $c->$orig(@_);

    my $schema  = $c->model('DB')->schema;

    $schema->default_resultset_attributes->{ current_user } = $schema->current_user(undef);
    $schema->_clear_schema();


    $c->_finish_graphing($t0);

    return $ret;
};

### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;

    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if($case->status eq 'open' || $case->status eq 'stalled') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if ($case->status eq 'stalled') {
        $c->push_flash_message(sprintf(
            '<span class="flash-urgent">Zaak opgeschort: %s</span>',
            $case->reden_opschorten || ''
        ));
    }
}



=head2 can_change

Determines wether the current user is allowed to make changes
on the current case.

=cut

sub can_change {
    my ($c, $opt) = @_;

    my $zaak = $c->stash->{zaak};
    return unless $zaak;

    # only once per request
    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
        $c->_can_change_messages;
    }


    return if $zaak->is_afgehandeld && !$opt->{ignore_afgehandeld};

    ### Zaak beheerders mogen wijzigingen aanbrengen ondanks dat ze geen
    ### behandelaar.
    return 1 if $c->check_any_zaak_permission('zaak_beheer');

    ### Override when we have the correct permissions, and we have a
    ### coordinator and behandelaar
    return 1 if (
        $zaak->behandelaar &&
        $zaak->coordinator &&
        $c->check_any_zaak_permission('zaak_beheer','zaak_edit')
    );

    if (
        !$zaak->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $zaak->behandelaar &&
            $zaak->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber &&
            (
                !$zaak->coordinator ||
                $zaak->coordinator->gegevens_magazijn_id ne $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        );

        return;
    }

    return 1;
}


=pod

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub check_queue_coworker_changes {
    my $c    = shift;
    my $zaak = $c->stash->{zaak};

    # admins always get what they want, this doesn't need to be queued
    return if $c->check_any_user_permission('admin');

    return unless $c->user;

    return $zaak->check_queue_coworker_changes($c->user->uidnumber);
}



# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#
sub push_flash_message {
    my $c       = shift;
    my $message;

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result;

    $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
        };
    }

    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    $c->flash->{result} = $result;

    return $result;
}

sub uri_format_ampersands {
    my ($c, $uri) = @_;

    $uri =~ s|&(?!amp;)|&amp;|gis;

    return $uri;
}

sub format_error {
    my ($c, $error) = @_;
    my $error_ref   = ref $error;

    $c->res->status(500);

    local $Data::Dumper::Terse = 1;

    # Pretty Exception::Class errors
    if (UNIVERSAL::isa($error, 'Exception::Class::Base')) {
        my @trace_args;

        # Stracktraces can be -huge-, only add them in developer mode.
        if ($c->debug) {
            @trace_args = map {
                {
                    package    => $_->package,
                    line       => $_->line,
                    filename   => $_->filename,
                    subroutine => $_->subroutine,
                    args_passed_to_sub => [$_->args],
                }
            } $error->trace->frames;
        }

        $c->log->error('Global exception caught', Dumper $error->as_string);

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => $error->code || 'unhandled',
            category        => $error_ref,
            messages        => [$error->as_string],
        };
    } elsif (UNIVERSAL::isa($error, 'Zaaksysteem::Exception::Base')) {
        my $formatter = sub {
            my $frame = shift;

            return {
                package => $frame->package,
                filename => $frame->filename,
                line => $frame->line,
                subroutine => $frame->subroutine,
                args_passwd_to_sub => []
            };
        };

        $c->log->error('Global exception caught', $error->TO_STRING);

        my @trace_args;

        if($c->debug) {
            @trace_args = map { $formatter->($_) } $error->stack_trace->frames;
            $c->log->debug('Stacktrace dump', $error->stack_trace->as_string);
        }

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => 500,
            category        => ref $error,
            messages        => [ $error->message ]
        };
    }
    # Non-Exception::Class errors - i.e. regular die's
    else {
        $c->log->error('Global exception caught', Dumper $error);

        return {
            debug           => $c->debug,
            body_parameters => [$c->req->body_parameters],
            error_code      => 'unhandled',
            category        => sprintf("%s", $c->action),
            messages        => Dumper $error,
            stacktrace      => 'Not (yet) available for non-exception errors',
        };
    }
}

sub cache {
    my $c   = shift;

    return $c->stash->{__zs_cache} if ref $c->stash->{__zs_cache} eq 'Zaaksysteem::Cache';

    $c->stash->{__zs_cache_store} = {};

    return Zaaksysteem::Cache->new(storage => $c->stash->{__zs_cache_store});
}

sub query_session {
    my $self = shift;

    return JSON::Path->new(shift)->value($self->session);
}


1;

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2013, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


Test file numbering scheme
==========================

* 000 - 099: Initialization, very basic tests, like code compilation, POD syntax, example tests.
* 100 - 109: Utility modules
* 110 - 119: Backend::ResultSet,
* 150 - 199: Backend::Sysin
* 200 - 250: FileStore, files
* 250 - 299: (unassigned)
* 300 - 399: Database objects (resultset, component)
* 400 - 499: (unassigned)
* 500 - 599: Controllers
* 600 - 649: ZAPI
* 650 - 699: SBUS
* 700 - 799: StUF
* 800 - 899: (unassigned)
* 990 - 999: Cleanup

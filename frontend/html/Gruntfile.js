/*global module,require*/

module.exports = function ( grunt ) {
	
	var gettext = require('gettext');
		
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		loc: {
			i18n: '../../lib/**/*.po',
			swig: 'src/**/*.swig',
			html: '../../root/html'
		},
		watch: {
			default: {
				files: [ '<%= loc.swig %>','<%= loc.i18n %>' ],
				tasks: [ 'build' ],
				options: {
					nospawn: true
				}
			},
			templates: {
				files: [ '<%= loc.swig %>'  ],
				tasks: [ 'wrapAndConcat', 'swig', 'translate' ],
				options: {
					nospawn: true
				}
			},
			translate: {
				files: [ '<%= loc.i18n %>' ],
				tasks: [ 'translate' ],
				options: {
					nospawn: true
				}
			}
		},
		translate: {
			build: {
				files: {
					src: '<%= loc.i18n %>'
				}
			}
		},
		swig: {
			development: {
				root: 'src/',
				cwd: 'src/',
				src: [ '**/*.swig', '!**/_*.swig' ],
				dest: '<%= loc.html %>/compiled/',
				production: false
			}
		},
		replace: {
			config: {
				cwd: '<%= loc.html %>/compiled',
				src: [ '**/*.html', '!**/node_modules/**/*.html' ],
				dest: [ '../' ],
				replacements: [
					{
						from: /%%(.*?)%%/g
					}
				]
			}
		},
		wrapAndConcat: {
			development: {
				'src/directives.swig': [ '/**/directives/**/*.swig' ]
			}
		},
		clean: {
			options: {
				force: true
			},
			src: [ '<%= loc.html %>' ]
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-swig');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-text-replace');
	
	grunt.registerMultiTask('wrapAndConcat', 'wrap directive partials in one swig file', function ( ) {
				
	});
	
	grunt.registerMultiTask('translate', 'translate html files with .po data', function ( ) {
			
		var files,
			replaceObj = grunt.config.get('replace'),
			replaceConfig = grunt.util._.clone(replaceObj.config, true),
			localeObj,
			toLoad = [],
			locales = [],
			done = this.async();
			
		files = grunt.file.expand({
			
		}, this.data.files.src);
		
		grunt.log.debug('found translation files: ' + files);
		
		function compileLocales ( ) {
			var locale,
				i,
				l;
				
			grunt.log.debug('Compiling locale', locales);
			
			for(i = 0, l = locales.length; i < l; ++i) {
				locale = locales[i];
				localeObj = grunt.util._.clone(replaceConfig, true);
				localeObj.replacements[0].to = (function ( l ) {
					return function ( matchedWord, index, fullText, regexMatches ) {
						gettext.setlocale('LC_ALL', l);
						return gettext.gettext(regexMatches[0]);
					};
				})(locale);
				localeObj.dest += locale + '/';
				replaceObj[locale] = localeObj;
			}
			
			grunt.config.set('replace', replaceObj);
			
			for(i = 0, l = locales.length; i < l; ++i) {
				locale = locales[i];
				grunt.log.debug('Running task replace: ' + locale);
				grunt.task.run([ 'replace:' + locale ]);
			}
			
			done();
			
		}
		
		function completeRead ( path ) {
			var index = toLoad.indexOf(path);
			if(index !== -1) {
				toLoad.splice(index, 1);
				if(toLoad.length === 0) {
					compileLocales();
				}
			}
		}
		
		files.forEach(function ( path ) {
			var locale = path.match(/^(.+)\/([^\/]+)\.po$/)[2];
			grunt.log.debug('Reading ' + path + ' (' + locale + ')');
			try {
				if(locales.indexOf(locale) === -1) {
					locales.push(locale);
				}
				toLoad.push(path);
				gettext.loadLanguageFile(path, locale, function ( ) {
					completeRead(path);
				});
			} catch ( error ) {
				grunt.log.error(error);
				completeRead(path);
			}
			
		});
	});
	
	grunt.registerTask('default', [ 'watch:default' ] );
	grunt.registerTask('dev', [ 'watch:templates', 'watch:translate' ] );
	grunt.registerTask('build', [ 'clean', 'wrapAndConcat', 'swig', 'translate' ] );
};
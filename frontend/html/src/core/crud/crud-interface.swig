<div data-ng-controller="nl.mintlab.core.crud.CrudInterfaceController" class="crud" data-ng-class="{ 'crud-loading': loading, 'crud-column-managed': hasColumnManagement() }">
	
	<div class="crud-pagination crud-pagination-top clearfix" data-zs-pagination data-zs-pagination-current="<[currentPage]>" data-zs-pagination-total="<[numPages]>" data-zs-pagination-num-rows="<[perPage]>" data-ng-show="items.length">
	</div>
	
	<div class="crud-filters" data-ng-show="filters.length" data-zs-form-template-parser="<[getFormConfig()]>">
		
	</div>
	
	<div class="crud-table-wrapper">
	
		<div class="crud-loader">
		</div>
	
		<div data-ng-controller="nl.mintlab.core.crud.CrudTableController" class="table crud-table" zs-crud-table-auto-size>

			<div class="table-header crud-header" data-ng-class="{'crud-hidden': hasVisibleActions()}" data-ng-controller="nl.mintlab.core.crud.CrudColumnManagementController">
				<div class="table-row crud-table-head-row" data-zs-sort data-zs-sort-direction="horizontal" data-zs-crud-table-auto-size-row>

					<div class="table-cell table-cell-header crud-table-select-cell" data-ng-show="options.select!='none'" data-zs-crud-table-auto-size-cell>
						<input type="checkbox" data-ng-click="handleSelectAllClick(e$vent)" data-ng-checked="isAllSelected(item)"/>
					</div>

					<div class="table-cell table-cell-header crud-table-column-header crud-table-column-header-<[getCssColumnId(column)]>" data-ng-repeat="column in columns track by column.id" data-ng-draggable="hasColumnManagement()&&!column.locked" data-ng-drag-mimetype="zs/column" data-zs-sortable="column" data-ng-class="{'crud-table-column-header-locked': column.locked }" data-zs-crud-table-auto-size-cell>
						<button class="crud-table-column-header-label-button" data-ng-click="sortOn(column)">
							<span class="column-label"><[column.label]></span>
							<span class="column-sort-asc column-sort" data-ng-show="isSortedOn(column)&&isReversed()">&#9650;</span>
							<span class="column-sort-desc column-sort" data-ng-show="isSortedOn(column)&&!isReversed()">&#9660;</span>
						</button>
						<div class="crud-table-column-header-remove" data-ng-if="hasColumnManagement()&&!column.locked">
							<button class="crud-table-column-header-remove-button" data-ng-click="removeColumn(column)">
								<i class="icon-font-awesome icon-remove"></i>
							</button>
						</div>
					</div>

					<div class="table-cell table-cell-header crud-table-column-header-management" data-ng-if="hasColumnManagement()" data-zs-popup-menu data-zs-crud-table-auto-size-cell>
						<button class="crud-table-column-header-management-button button button-secondary" data-ng-class=" { 'crud-table-column-header-management-button-open': open }"><i class="icon-font-awesome icon-cog"></i>
						</button>
						<div class="crud-column-popup-menu popup-menu" data-ng-class="{'crud-column-popup-menu-loading': loading }" data-zs-popup-menu-list>
							<div class="crud-column-popup-menu-query">
								<input class="crud-column-popup-menu-query-input" type="text" data-ng-model="$parent.query" data-ng-required="false" data-zs-form-field data-zs-placeholder="'Zoek binnen alle kenmerken'">
								<div class="spot-enlighter-loading">
								</div>
							</div>
							<ul class="crud-column-popup-menu-list">
								<li class="crud-column-popup-menu-list-item" data-ng-repeat="column in columnResults track by column.id" data-ng-class="{ 'crud-column-popup-menu-list-item-active': isSelected(column) }">
									<button class="crud-column-popup-menu-list-item-button" data-ng-click="toggleColumn(column);closePopupMenu()" >
										<i class="crud-column-popup-menu-list-item-button-icon icon-font-awesome icon-ok"></i>
										<span class="crud-column-popup-menu-list-item-button-label"><[column.label]></span>
									</button>
								</li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>

			<div data-ng-if="hasVisibleActions()" class="table-header crud-actions">
				<div class="table-row crud-table-head-row">
					<div class="crud-table-select-cell" data-ng-show="options.select!='none'">
						<input type="checkbox" data-ng-click="handleSelectAllClick($event)" data-ng-checked="isAllSelected()"/>
					</div>
					<div class="crud-table-action-cell">
						<ul data-ng-include="'/html/core/crud/action-menu.html'">
						</ul>
					</div>
				</div>
			</div>
			
			<div data-ng-if="(isAllSelected() && numPages > 1)" class="table-header crud-select-all-wrapper">
				<div class="table-row crud-table-head-row">
					<div>
						<div class="crud-select-all">
							<div data-ng-show="zsCrud.getSelectionType()=='subset'">Alleen de objecten op deze pagina zijn geselecteerd. <button class="crud-select-all-button" data-ng-click="setSelectionType('all')">Selecteer alle <[numRows]> objecten.</button></div>
							<div data-ng-show="zsCrud.getSelectionType()=='all'">Alle <[numRows]> objecten zijn geselecteerd. <button class="crud-select-all-button" data-ng-click="setSelectionType('subset')">Selecteer alleen de objecten op deze pagina.</button></div>
						</div>
					</div>
				</div>
			</div>

			<div class="table-body" data-zs-selectable-list="options.select!=='none'">
				<div class="table-row crud-table-item-row crud-table-item-row-empty" data-ng-if="isEmptyResultSet()">
						Geen resultaten	
				</div>
				
				<a class="table-row crud-table-item-row <[isSelected(item) && 'crud-table-item-row-selected'||'']>" data-ng-class="getItemStyle(item)" data-ng-repeat="item in items track by $index" data-zs-selectable data-zs-select-data="item" data-zs-contextmenu="hasVisibleActions()&&'/html/core/crud/context-menu.html'||''" data-ng-href="<[getUrl(item)]>" data-ng-include="'/html/core/crud/crud-item-row.html'" data-ng-if="getUrl(item)" data-zs-crud-table-auto-size-row>
					
				</a>
				<div class="table-row crud-table-item-row <[isSelected(item) && 'crud-table-item-row-selected'||'']>" data-ng-class="getItemStyle(item)" data-ng-repeat="item in items track by $index" data-zs-selectable data-zs-select-data="item" data-zs-contextmenu="hasVisibleActions()&&'/html/core/crud/context-menu.html'||''" data-ng-if="!getUrl(item)" data-ng-include="'/html/core/crud/crud-item-row.html'" data-zs-crud-table-auto-size-row>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="crud-pagination crud-pagination-bottom clearfix" data-zs-pagination data-zs-pagination-current="<[currentPage]>" data-zs-pagination-total="<[numPages]>" data-zs-pagination-num-rows="<[perPage]>" data-ng-show="items.length">
	</div>

</div>

<script type="text/ng-template" id="/html/core/crud/crud-item-row.html">
	<div class="table-cell crud-table-item-cell crud-table-select-cell" data-ng-show="options.select!='none'">
		<input type="checkbox" data-ng-checked="isSelected(item)"/>
	</div>
	<div class="table-cell crud-table-item-cell crud-table-value-cell crud-table-value-cell-<[getCssColumnId(column)]>" data-ng-repeat="column in columns track by column.id">
		<div class="crud-table-item-cell-content" data-ng-if="getTemplateType(column)==='resolve'">
			<[getColumnValue(column, item)]>
		</div>
		<div class="crud-table-item-cell-content" data-ng-if="getTemplateType(column)==='template'" data-zs-template="<[column.template]>">
		</div>
		<div class="crud-table-item-cell-content" data-ng-if="getTemplateType(column)==='url'" data-ng-include="column.templateUrl">
		</div>
	</div>
	<div class="table-cell crud-table-item-cell crud-table-value-cell crud-table-value-cell-column_management" data-ng-if="hasColumnManagement()">
		<div class="Crud-table-item-cell-content"></div>
	</div>
</script>

<script type="text/ng-template" id="/html/core/crud/action-menu.html">
	<li class="crud-action" data-ng-repeat="action in actions | filter:isVisible:action track by action.id " data-ng-include="'/html/core/crud/action-type-' + action.type + '.html'" data-ng-controller="nl.mintlab.core.crud.CrudActionController">
	</li>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-delete.html">
	<button data-ng-click="handleActionClick(action, $event)" class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-update.html">
	<button data-ng-click="handleActionClick(action, $event)" class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-download.html">
	<button data-ng-click="handleActionClick(action, $event)"  class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-download.html">
	<button data-ng-click="handleActionClick(action, $event)"  class="button button-smaller button-secondary">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-url.html">
	<a data-ng-href="<[getActionUrl(action)]>" target="_blank" class="button button-smaller button-secondary">
		<[action.label]>
	</a>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-ezra-dialog.html">
	<button class="button button-secondary button-smaller" data-zs-ezra-dialog="getEzraDialogParams()">
		<[action.label]>
	</button>
</script>

<script type="text/ng-template" id="/html/core/crud/action-type-dropdown.html">
	<div class="action-menu-dropdown" data-zs-popup-menu data-zs-popup-menu-behaviour="<[behaviour]>" data-ng-controller="nl.mintlab.core.crud.CrudActionDropDownController">
		<button class="action-menu-dropdown-button button button-smaller button-secondary dropdown-toggle">
			<[action.label]>
		</button>
		<ul class="action-menu-dropdown-list popup-menu" data-zs-popup-menu-list>
			<li class="action-menu-dropdown-list-item" data-ng-repeat="action in action.data.children | filter:isVisible:action track by $index" data-ng-include="'/html/core/crud/action-type-' + action.type + '.html'" data-ng-controller="nl.mintlab.core.crud.CrudActionController">
			</li>
			<li class="action-menu-dropdwn-list-item-none" data-ng-show="!hasVisibleActions(action.data.children)">
				Geen acties beschikbaar
			</li>
		</ul>
	</div>
</script>

<script type="text/ng-template" id="/html/core/crud/popup-modal.html">
	<div data-zs-modal data-zs-init="title=action.data.title">
		<div data-ng-include="action.data.url">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/core/crud/context-menu.html">
	<ul class="context-menu crud-context-menu" data-ng-include="'/html/core/crud/action-menu.html'">
	</ul>
</script>

<script type="text/ng-template" id="/html/core/crud/crud-item-action-menu.html">	
	<div class="crud-item-action-menu clearfix" data-zs-popup-menu data-ng-controller="nl.mintlab.core.crud.CrudItemActionMenuController">
		<button class="crud-item-action-menu-open-button button-xsmall button-secondary button" data-ng-click="handleOpenMenuClick($event)">
			<i class="icon-font-awesome icon-caret-down icon-only"></i>
		</button>
		<ul class="crud-item-action-menu-list popup-menu" data-ng-include="'/html/core/crud/action-menu.html'" data-zs-popup-menu-list data-ng-if="isPopupMenuOpen()">
		
		</ul>
	</div>
</script>

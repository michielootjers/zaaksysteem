/*global module*/
module.exports = function ( grunt ) {
	
	grunt.initConfig({
		hub: {
			default: {
				src: [ '*/Gruntfile.js' ]
			},
			dev: {
				src: [ '*/Gruntfile.js' ],
				tasks: [ 'dev' ]
			},
			build: {
				src: [ '*/Gruntfile.js' ],
				tasks: [ 'build' ]
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-hub');
	
	grunt.registerTask('default', [ 'hub' ]);
	grunt.registerTask('dev', [ 'hub:dev']);
	grunt.registerTask('build', [ 'hub:build' ]);
	
};
<div class="sysin-links block-fullscreen" data-ng-controller="nl.mintlab.sysin.links.LinkController">
	<div class="block-header header-with-actions clearfix">
		<h1 class="block-header-title">%%Koppelingen Configuratie%%</h1>	
	    <div role="navigation" class="button-group header-actions">
	    	<div data-zs-popup-menu data-zs-popup-menu-positioning="smart" class="clearfix">
	    		<button
	    			data-zs-popup-menu-position-reference
	    			title="Voeg Item Toe"
	    			class="button button-secondary button dropdown-toggle">
	    			<i class="icon icon-font-awesome icon-plus"></i>
				</button>
	        	<ul class="ng-cloak zs-popup-menu-list popup-menu" data-zs-popup-menu-list>
	            	<li>
	                    <button data-ng-click="openPopup()" data-zs-popup="'/html/sysin/links/new.html'">
	                    	<span>%%Voeg koppeling toe%%</span>
	                    </button>
	                </li>
		        </ul>
		    </div>
	    </div>
	</div>
	
	<div class="blockcontent panel-component sysin-links-content">
		<div class="panel sysin-links-list">
			<ul>
				<li class="sysin-links-list-item" data-ng-class="{ 'sysin-links-list-item-active': activeLink==link, 'sysin-links-list-item-error': link.transaction_errors }" data-ng-repeat="link in interfaces | orderBy:'name'">
					<button class="sysin-links-list-item-button" data-ng-click="showLink(link)">
						<span class="sysin-links-list-item-button"><[link.name]> (<[link.id]>)</span>
					</button>
					<div class="sysin-links-list-item-action-wrapper">
						<div class="sysin-links-list-item-error" data-ng-show="link.transaction_errors">
							<a href="/beheer/sysin/transactions/?interface_id=<[link.id]>&is_error=true">
								<i class="icon-font-awesome icon-warning-sign"></i>
							</a>
						</div>
						<div class="sysin-links-list-item-actions" data-zs-popup-menu>
							<button class="sysin-links-list-item-actions-open button button-xsmall button-secondary button-on-hover">
								<i class="icon-font-awesome icon-caret-down"></i>
							</button>
							<ul class="action-bar-dropdown-menu doc-dropdown-menu">
								<li class="action-bar-button">
									<button data-ng-click="openPopup()" data-zs-popup="'/html/sysin/links/rename.html'" class="doc-dropdown-menu-item">
										%%Naam wijzigen%%
									</button>
								</li>
								<li class="action-bar-button">
									<button data-ng-click="removeLink(link)" class="doc-dropdown-menu-item">
										%%Verwijderen%%
									</button>
								</li>
								<li class="action-bar-button">
									<button class="doc-dropdown-menu-item" data-ng-click="openPopup()" data-zs-popup="'/html/sysin/manual-process.html'">
										%%Handmatige upload%%
									</button>
								</li>
								<li class="action-bar-button">
									<a class="doc-dropdown-menu-item" href="/beheer/sysin/transactions/?interface_id=<[link.id]>">
										%%Bekijk de transacties voor deze koppeling%%
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="panel sysin-links-detail">
			<div data-zs-form-template-parser="<[getLinkFormUrl(activeLink)]>" data-ng-show="activeLink">
			</div>
		</div>
	</div>
	
	<script type="text/zs-scope-data">
		{
			"renameForm": {
				"name": "renamelink",
				"title": "%%Naam wijzigen%%",
				"description": "",
				"fieldsets": [
					{
						"name": "",
						"title": "",
						"description": "",
						"fields": [
							{
								"name": "name",
								"type": "text",
								"value": "<[link.name]>"
							}
						]
					}
				],
				"actions": [
					{
						"name": "submit",
						"type": "submit",
						"label": "%%Wijzig%%",
						"data": {
							"url": "/sysin/interface/<[link.id]>/update",
							"success": "closePopup()"
						}
					}
				]
			},
			"addForm": {
				"name": "addlink",
				"title": "%%Koppeling toevoegen",
				"description": "",
				"fieldsets": [
					{
						"name": "",
						"title": "",
						"description": "",
						"fields": [
							{
								"name": "module",
								"label": "Module",
								"type": "select",
								"data": {
									"options": "<[$parent.getModuleOptions()]>"
								},
								"value": "<[$parent.modules[0].name]>"
							},
							{
								"name": "name",
								"label": "Naam",
								"type": "text",
								"required": true,
								"value": ""
							}
						]
					}
				],
				"actions": [
					{
						"name": "submit",
						"type": "submit",
						"label": "%%Voeg koppeling toe%%",
						"data": {
							"url": "/sysin/interface/create",
							"success": "closePopup()"
						}
					}
				],
                "promises": [
                    {
                        "watch": "module",
                        "then": "name=getModuleByName(module).label"
                    }
                ]
			}
		}
	</script>

</div>		


<script type="text/ng-template" id="/html/sysin/links/rename.html">
	<div data-zs-modal data-ng-init="title='%%Naam wijzigen%%'">
		
		<div data-zs-form-template-parser="<[renameForm]>">
		</div>
		
	</div>
</script>

<script type="text/ng-template" id="/html/sysin/links/new.html">
	<div data-zs-modal data-ng-init="title='%%Koppeling toevoegen%%'">
		
		<div data-zs-form-template-parser="<[addForm]>">
		</div>
		
	</div>
</script>

<script type="text/ng-template" id="/html/sysin/links/mapping.html">
	<div data-ng-controller="nl.mintlab.sysin.links.DataMappingController">

		<ul class="mapping-list">
			<li class="mapping-list-item" data-ng-repeat="mapping in mappings">
				<div class="mapping-optional"> 
					<input type="checkbox" data-ng-disabled="!mapping.optional" data-ng-model="mapping.checked" data-ng-change="save()"/>
				</div>
				<div class="mapping-name">
					<input type="hidden" data-ng-disabled="mapping.attribute_type!='freeform'" data-ng-blur="save()" data-ng-model="mapping.external_name"/><strong><[mapping.label || mapping.external_name]></strong>
				</div>
				<div class="mapping-value">
					<input type="text" data-ng-model="mapping.internal_name" data-ng-show="mapping.attribute_type!='magic_string'&&mapping.attribute_type!='objecttype'" data-ng-blur="save()" />
					<div class="spot-enlighter-wrapper" data-ng-show="mapping.attribute_type=='magic_string'">
						<input type="text" data-ng-model="mapping.internal_name" data-ng-change="save()" data-zs-placeholder="'%%Type om te zoeken%%'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="getSpotEnlighterRestrict(mapping)" data-zs-spot-enlighter-label="searchable_object_label"/>
					</div>
					<div class="spot-enlighter-wrapper" data-ng-show="mapping.attribute_type=='objecttype'">
						<input type="text" data-ng-model="mapping.internal_name" data-ng-change="save()" data-zs-placeholder="'%%Type om te zoeken%%'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'objecttype'" data-zs-spot-enlighter-label="label"/>
					</div>
				</div>
				<div class="mapping-remove row-actions">
					<button data-ng-click="removeMapping(mapping)" data-ng-disabled="mapping.attribute_type!='freeform'">
						<i class="icon-font-awesome icon-remove"></i>
					</button>
				</div>
			</li>
		</ul>
		
		<div class="data-mapping-add" data-ng-show="attributeType!='freeform'">
			<form name="newMapping">
				<input type="text" data-ng-model="newMappingName" data-ng-required="true" />
				<button class="button button-secondary" type="submit" data-ng-disabled="newMapping.$invalid" data-ng-click="!newMapping.$invalid&&addMapping(newMappingName)">
					<i class="icon-font-awesome icon-plus"></i>
				</button>
			</form>
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/sysin/links/test.html">
	<div class="interface-test-runner" data-ng-controller="nl.mintlab.sysin.links.InterfaceTestController">
		<div class="interface-test-options">
			<div class="interface-test-description">
				<[description]>
			</div>
			<button class="btn btn-primary" data-ng-click="runAllTests()" data-ng-disabled="!tests.length">
				Start alle testen
			</button>
		</div>
		<ul class="interface-test-list">
			<li class="interface-test-list-item" data-ng-repeat="test in tests">
				<div class="interface-test-list-item-meta">
					<div class="interface-test-list-item-name">
						<strong><[test.label]></strong>
					</div>
					<div class="interface-test-list-item-desc">
						<[test.description]>
					</div>
					<div class="interface-test-list-item-error" data-ng-show="test.error_message"><[test.error_message]></div>
					<div class="interface-test-list-item-result" data-ng-show="test.result">
						Zaken: <[test.result.join(", ")]>
					</div>
				</div>
				<div class="interface-test-list-item-status">
					<div class="interface-test-list-item-result interface-test-list-item-result-<[test.status]>">
						<i class="icon-font-awesome" data-ng-class="{'icon-ok': test.status === 'ok', 'icon-warning-sign': test.status === 'failed'}"></i>
					</div>
				</div>
				<div class="interface-test-list-item-start">
					<button class="button button-secondary button-small" data-ng-click="runTest(test)" data-ng-disabled="test.status==='running'">
						Start test
					</button>
				</div>

			</li>
		</ul>
	</div>
</script>

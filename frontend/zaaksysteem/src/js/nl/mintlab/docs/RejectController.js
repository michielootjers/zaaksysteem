/*global angular*/
(function () {
    "use strict";

    angular.module('Zaaksysteem.docs')
        .controller('nl.mintlab.docs.RejectController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {

            $scope.reject = function (entity) {
                $scope.rejectFile([entity], $scope.rejection_reason);
                $scope.closePopup();
            };

        }]);
}());
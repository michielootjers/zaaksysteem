/*global angular,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseTemplateController', [ '$rootScope', '$scope', 'smartHttp', 'translationService', function ( $rootScope, $scope, smartHttp, translationService ) {
			
			$scope.getCaseDocById = function ( id ) {
				var caseDocs = $scope.getCaseDocs() || [],
					caseDoc,
					i,
					l;
					
				for(i = 0, l = caseDocs.length; i < l; ++i) {
					caseDoc = caseDocs[i];
					if(caseDoc.bibliotheek_kenmerken_id.id === id) {
						return caseDoc;
					}
				}
				
				return null;
				
			};
			
			$scope.getExtension = function ( template ) {
				return '.' + (template.target_format || 'odt').toLowerCase();
			};
			
			$scope.saveTemplate = function ( values ) {

				$scope.action.data.filename = values.name;
				$scope.action.data.bibliotheek_kenmerken_id = values.caseDocument ? values.caseDocument.bibliotheek_kenmerken_id.id : null;
				
				var data = {
					id: $scope.action.id,
					filename: $scope.action.data.filename,
					'bibliotheek_kenmerken_id': $scope.action.data.bibliotheek_kenmerken_id,
					'target_format': values.targetFormat
				};
				
				return smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/data',
					data: data
				})
					.success(function ( response ) {
						
						var itemData = response.result[0];
						for(var key in itemData) {
							$scope.action[key] = itemData[key];
						}
						
						$scope.targetFormat = $scope.action.data.target_format;
						
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Instellingen voor "%s" opgeslagen', $scope.action.label)
						});
					})
					.error(function ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Instellingen voor "%s" konden niet worden opgeslagen', $scope.action.label)
						});
					});
			};
			
			
			$scope.filename = $scope.action.data.filename;
			$scope.caseDocument = $scope.getCaseDocById($scope.action.data.bibliotheek_kenmerken_id);
			$scope.targetFormat = $scope.action.data.target_format;
			
			$scope.$watch('templates', function ( /*nw, old*/ ) {
				var templates = $scope.templates || [];
				for(var i = 0, l = templates.length; i < l; ++i) {
					if(templates[i].bibliotheek_sjablonen_id.id === $scope.action.data.bibliotheek_sjablonen_id) {
						$scope.template = templates[i];
						break;
					}
				}
			});

			$scope.actions = [
				{
					name: 'submit',
					type: 'primary',
					label: 'Aanmaken'
				},
				{
					name: 'save',
					type: 'secondary',
					label: 'Opslaan',
					click: [ '$values', function ( values) {
						return $scope.saveTemplate(values);
					}]
				}
			];
			
			
		}]);
	
})();

/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case.relation')
		.directive('zsPlannedCaseList', [ 'objectService', 'zqlService', 'systemMessageService', 'translationService', 'dateFilter', function ( objectService, zqlService, systemMessageService, translationService, dateFilter ) {
			
			var buildUrl = window.zsFetch('nl.mintlab.utils.buildUrl');
			
			function tr ( ) {
				return translationService.get.apply(translationService, arguments);
			}
			
			return {
				scope: true,
				require: [ 'zsPlannedCaseList', '^?zsCaseView' ],
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this,
						zsCaseView,
						loadedJobs = [],
						config = {
							name: 'planned_cases',
							columns: [
								{
									id: 'casetype',
									label: 'Zaaktype',
									resolve: 'label',
									locked: true,
									dynamic: true,
									sort: false
								},
								{
									id: 'pattern',
									label: 'Patroon',
									resolve: 'plannedCaseList.getPattern($item)',
									locked: true,
									dynamic: true,
									sort: false
								},
								{
									id: 'reach',
									label: 'Bereik',
									resolve: 'plannedCaseList.getReach($item)',
									locked: true,
									dynamic: true,
									sort: false
								},
								{
									id: 'actions',
									label: 'Acties',
									locked: true,
									templateUrl: '/html/core/crud/crud-item-action-menu.html',
									dynamic: true,
									sort: false
								}
							],
							options: {
								select: 'all'
							},
							actions: [
								{
									id: 'edit',
									type: 'popup',
									label: 'Bewerken',
									data: {
										url: '/html/case/relations/relations.html#planned-case-edit-dialog'
									},
									when: 'selectedItems.length===1&&plannedCaseList.canChange()'
								},
								{
									id: 'export',
									type: 'download',
									label: 'Download als .xls',
									data: {
										url: '<[plannedCaseList.getExportUrl(selectedItems, \'xls\')]>'
									}
								},
								{
									id: 'delete',
									type: 'confirm', 
									label: 'Verwijderen',
									data: {
										label: 'Weet u zeker dat u deze zaakplanning(en) wil verwijderen?',
										verb: 'Verwijderen',
										click: 'plannedCaseList.handleDeleteClick($selection)'
									},
									when: 'selectedItems.length===1&&plannedCaseList.canChange()'
								}
							]
						};
					
					function onCaseUpdate ( ) {
						loadScheduledJobs();
					}
					
					function loadScheduledJobs ( ) {
						var relations = getPlannedCaseRelations(),
							ids = _.pluck(relations, 'related_object_id'),
							toLoad = _.difference(ids, loadedJobs);
							
						if(toLoad.length) {
							loadedJobs = _.unique(loadedJobs.concat(ids));
							
							zqlService.query('SELECT {} FROM scheduled_job WHERE object.uuid IN (' + _.map(ids, zqlService.escape).join(',') + ')', { deep_relations: 1 })	
								.success(function ( response ) {
									_.each(response.result, function ( job ) {
										
										var relation = _.find(zsCaseView.getCase().related_objects, { related_object_type: 'scheduled_job', related_object_id: job.id });
										
										relation.related_object = job;
									});
								})
								.error(function ( ) {
									
								});
						}
						
					}
					
					function getPlannedCaseRelations ( ) {
						var caseObj = zsCaseView ? zsCaseView.getCase() : null,
							relations = null;
							
						if(caseObj) {
							// get job relations from case relations
							relations = _.filter(caseObj.related_objects, function ( relationship ) {
								var isScheduledJob = relationship.related_object_type === 'scheduled_job' && relationship.related_object.values.job === 'CreateCase';
								return isScheduledJob;
							});
						}
						
						return relations;
					}
					
					ctrl.link = function ( controllers ) {
						zsCaseView = controllers[0];
						
						loadScheduledJobs();
					
						if (zsCaseView) {
							zsCaseView.updateListeners.push(onCaseUpdate);

							$scope.$on('$destroy', function ( ) {
								_.pull(zsCaseView.updateListeners, onCaseUpdate);
							});
						}
						

					};
					
					ctrl.getCrudConfig = function ( ) {
						return config;
					};
					
					ctrl.getPlannedCases = function ( ) {
						var caseObj = zsCaseView ? zsCaseView.getCase() : null,
							plannedCases = null;
							
						if(caseObj) {
							plannedCases = _.pluck(getPlannedCaseRelations(), 'related_object');
							plannedCases = _.sortBy(plannedCases, function ( plannedCase ) {
								return Date.parse(plannedCase.values.next_run);
							});
						}
						
						return plannedCases;
					};
					
					ctrl.getPattern = function ( $item ) {
						var pattern,
							moreThanOnce = $item.values.interval_value > 1;
						
						switch($item.values.interval_period) {
							case 'once':
							pattern = 'Eenmalig';
							break;
							
							case 'days':
							pattern = 'Elke ' + (moreThanOnce ? '%s dagen' : 'dag');
	                     	break;
	                     	
	                     	case 'weeks':
	                     	pattern = 'Elke ' + (moreThanOnce ? '%s weken' : 'week');
	                     	break;
	                     	
	                     	case 'months':
	                     	pattern = 'Elke ' + (moreThanOnce ? '%s maanden' : 'maand');
	                     	break;
	                     	
	                     	case 'years':
	                     	pattern = 'Elk ' + (moreThanOnce ? '%s jaar' : 'jaar');
	                     	break;
						}
						
						return tr(pattern || '', $item.values.interval_value || '');
					};
					
					ctrl.getReach = function ( $item ) {
						var reach = '';
						
						reach = dateFilter($item.values.next_run);
						
						if($item.values.runs_left > 1) {
							reach = tr('Start op') + ' ' + reach + ' (' + ($item.values.runs_left) + ' ' + tr('herhalingen') + ')';
						}
						
						return reach;
					};
					
					ctrl.getExportUrl = function ( selectedItems, format ) {
						
						var ids = _.pluck(selectedItems, 'id'),
							params = {
								zql: 'SELECT {} FROM scheduled_job WHERE object.uuid in (' + _.map(ids, zqlService.escape).join(',') + ')',
								zapi_format: format,
								zapi_no_pager: 1
							},
							url;
							
						url = buildUrl('/api/object/search', params);
						
						return url;
					};
					
					ctrl.handleDeleteClick = function ( $selection ) {
						var job = $selection[0],
							caseObj = zsCaseView.getCase(),
							relation = _.find(caseObj.related_objects, { related_object: job });
							
						_.pull(caseObj.related_objects, relation);
						
						objectService.deleteObject(job)
							.then(function ( ) {
								systemMessageService.emit('info', 'De zaakplanning is verwijderd.');
							})
							['catch'](function ( ) {
								caseObj.related_objects.push(relation);
								throw systemMessageService.emitError('De zaakplanning kon niet verwijderd worden. Probeer het later opnieuw.');
							});
					};
					
					ctrl.handlePlannedCaseEdit = function ( plannedCase, $values ) {
						
						var values = {
								interval_period: $values.interval_period,
								interval_value: $values.interval_value,
								runs_left: $values.runs_left,
								next_run: $values.next_run
							};
							
						objectService.updateObject(plannedCase, values, { deep_relations: true })
							['catch'](function ( ) {
								throw systemMessageService.emitSaveError();
							});
						
					};
					
					ctrl.canChange = function ( ) {
						return zsCaseView ? zsCaseView.canChange() : false;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'plannedCaseList',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();

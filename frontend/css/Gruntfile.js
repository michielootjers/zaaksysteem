/*global module*/

var lrserver = require('tiny-lr')();

module.exports = function ( grunt ) {
	
	grunt.initConfig({
		watch: {
			sass: {
				files: [ '**/*.scss' ],
				tasks: [ 'sass:compile', 'livereload' ],
				options: {
					spawn: false
				}
			}
		},
		sass: {
			compile: {
				options: {
					unixNewlines: false,
					noLineComments: true,
					cache: true
				},
				files: [{
					expand: true,
					cwd: 'src',
					src: [ '**/*.scss' ],
					dest: '../../root/tpl/zaak_v1/nl_NL/css/',
					ext: '.css'
				}]
			},
			build: {
				options: {
					unixNewlines: false,
					noLineComments: true,
					cache: false
				},
				files: [{
					expand: true,
					cwd: 'src',
					src: [ '**/*.scss' ],
					dest: '../../root/tpl/zaak_v1/nl_NL/css/',
					ext: '.css'
				}]
			}
		}
		
	});
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	
	grunt.registerTask('default', [ 'watch:sass' ] );
	grunt.registerTask('dev', [ 'watch' ] );
	
	grunt.registerTask('build', [ 'sass:build' ] );
	
	grunt.registerTask('livereload', function ( ) {
		var self = this,
			name = self.name || 'livereload',
			changed = grunt.config([name, 'changed' ]);
			
		if(changed && changed.length) {
			lrserver.changed({body:{files: changed }});
		}
	});
	
	grunt.event.on('write', function ( filepath ) {
		var config = grunt.config('livereload') || {},
			changed = config.changed || [];
		
		changed.push(filepath);
		
		config.changed = changed;
		
		grunt.config('livereload', config);
	});
	
	grunt.file.write = (function ( ) {
		
		var toWrap = grunt.file.write;
		
		return function ( filepath ) {
			toWrap.apply(this, arguments);
			grunt.event.emit('write', filepath);
		};
		
	})();
	
	lrserver.listen(35729, function(err) { 
		if(err) {
			grunt.fail.warn('Failed to start LR server: ' + err);
		}
	});
	
	
	
};
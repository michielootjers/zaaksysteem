/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.object')
		.controller('nl.mintlab.object.ObjectSearchController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			$scope.object = null;
			
			
			$scope.removeObjectSubscription = function ( ) {
				smartHttp.connect({
					method: 'POST',
					url: '/beheer/sysin/transaction/delete',
					data: {
						selection_type: 'subset',
						selection_id: null
					}
				})
					.success(function ( /*response*/ ) {
						// var object = response.data[0];
						// console.log(object);
					})
					.error(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: 'Afnemerindicatie kon niet worden verwijderd'
						});
					});
			};
			
			$scope.getRecordMutations = function ( item ) {
				var mutations = [];
				if(item.parsed_mutations) {
					return item.parsed_mutations;
				}
				// parsing the mutations on the fly every time results
				// in a strange digestion loop
				try {
					mutations = JSON.parse(item.mutations);
					item.parsed_mutations = mutations;
				} catch ( error ) {
					
				}
				return mutations;
			};
			
			$scope.$on('detail.item.change', function ( ) {
				safeApply($scope, function ( event, item ) {
					$scope.object = item;
				});
			});
			
			$scope.init = function ( ) {
				// smartHttp.connect({
				// 	method: 'GET',
				// 	url: '/sysin/transaction_record_to_object/',
				// 	params: {
				// 		local_id: $scope.localId,
				// 		local_table: $scope.localTable
				// 	}
				// })
				// 	.success(function ( response ) {
				// 		var data = response.result[0];
				// 		$scope.objectSubscription = data ? data.object_subscription : null;
				// 	})
				// 	.error(function ( /*response*/ ) {
						
				// 	});
			};
			
		}]);
	
})();
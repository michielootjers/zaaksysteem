/*global angular,fetch*/
(function ( ) {

    angular.module('Zaaksysteem.docs')
        .controller('nl.mintlab.docs.FilePreviewController', [ '$scope', '$window', 'smartHttp', 'flexpaperService', function ( $scope, $window, smartHttp, flexpaperService ) {

            $scope.$on('popupclose', function ( /*event, key, value*/ ) {
                $scope.reloadData();
            });

            $scope.showPdfPreview = function (event) {
                if (flexpaperService.canDisplay()) {
                    if (flexpaperService.canDisplayInIFrame()) {
                        $scope.openPopup();
                    } else {
                        $window.open($scope.flexpaperUrl);
                    }
                } else {
                    $scope.onFileNameClick(event);
                }
            };

            $scope.onNameClick = function ( event ) {
                if ($scope.entity.getEntityType() === 'folder') {
                    $scope.onFolderNameClick(event);
                } else {
                    $scope.showPdfPreview(event);
                }
            };

        }]);
})();

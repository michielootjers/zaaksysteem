/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.GlobalIntakeActionController', [ '$scope', 'smartHttp', 'caseDocumentService', function ( $scope, smartHttp, caseDocumentService ) {
			
			var forEach = angular.forEach,
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.caseId = null;
			
			function processCaseDocData ( data ) {
				$scope.caseDocs = data || [];
			}
		
			$scope.assignFiles = function ( ) {
				
				var	caseId = $scope.caseId.id,
					files = $scope.selectedFiles.concat(),
					queue = [];
				
				$scope.loading = true;
				
				$scope.deselectAll();
				
				function onComplete ( file ) {
					var index = indexOf(files, file);
					files.splice(index, 1);
					if(!files.length) {
						$scope.loading = false;
						$scope.closePopup();
					}
				}
					
				forEach(files, function ( file ) {
					
					$scope.root.remove(file);
					
					file.case_id = caseId;
					
					queue.push(file);
					
					smartHttp.connect({
						url: 'file/update',
						method: 'POST',
						data: {
							file_id: file.id,
							case_id: caseId,
							case_document_ids: file.case_type_document_id ? [ file.case_type_document_id.id ] : []
						}
					})
						.success(function ( ) {
							onComplete(file);
							$scope.$broadcast('systemMessage', {
								type: 'info',
								code: 'assign-file-success',
								data: {
									files: files
								}
							});
						})
						.error(function ( ) {
							onComplete(file);
							$scope.root.add(file);
							file.case_id = null;
							file.case_type_document_id = null;
						});
				});
				
			};
			
			$scope.getFileNames = function ( ) {
				var fileNames = [];
				forEach($scope.selectedFiles, function ( file ) {
					fileNames.push(file.name + file.extension);
				});
				
				return fileNames.join(', ');
			};
			
			$scope.getCaseDocLabel = function ( caseDoc ) {
				var label = '';
				
				if(caseDoc) {
					label = caseDoc.label || caseDoc.bibliotheek_kenmerken_id.naam;
				}
				return label;
			};
			
			$scope.$watch('caseId', function ( ) {
				$scope.caseDocs = [];
				
				if($scope.caseId) {
					
					$scope.loading = true;
					
					caseDocumentService.getCaseDocumentsByCaseId($scope.caseId.id)
						.then(function ( data ) {
							processCaseDocData(data);	
						})
						['finally'](function ( ) {
							$scope.loading = false;
						});
				}
			});
		
	}]);
	
})();
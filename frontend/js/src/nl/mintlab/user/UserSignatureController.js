/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.user')
		.controller('nl.mintlab.user.UserSignatureController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.uploadId = 0;
			$scope.hasSignature = false;
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			$scope.removeSignature = function ( ) {
				smartHttp.connect({
					method: 'POST',
					url: '/api/betrokkene/signature/delete',
					data: {
						uuid: $scope.uuid
					}
				})
					.success(function ( /*response*/ ) {
						$scope.hasSignature = false;
					})
					.error(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error'
						});
					});
			};
			
			$scope.$on('upload.start', function ( ) {
				safeApply($scope, function ( ) {
					$scope.loading = true;
				});
			});
			
			$scope.$on('upload.end', function ( ) {
				safeApply($scope, function ( ) {
					$scope.loading = false;
				});
			});
			
			$scope.$on('upload.complete', function ( $event, upload ) {
				safeApply($scope, function ( ) {
					if(!upload.error) {
						$scope.uploadId++;
						$scope.hasSignature = true;
					}
				});
			});
			
			$scope.$on('upload.error', function ( /*event, upload*/ ) {
				safeApply($scope, function ( ) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Er ging iets fout bij het uploaden. De afbeelding moet een .jpg, .gif of .png zijn, en 350 bij 120 pixels.')
					});
				});
			});
			
		}]);
	
})();
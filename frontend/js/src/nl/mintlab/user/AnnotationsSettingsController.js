/*global angular,fetch*/
(function () {

    angular.module('Zaaksysteem.user')
        .controller('nl.mintlab.user.AnnotationsSettingsController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
            
            var safeApply = fetch('nl.mintlab.utils.safeApply');
            
            $scope.clearAnnotations = function ( ) {
                console.log('bitch please');

                smartHttp.connect( {
                    method: 'POST',
                    url: '/api/user/annotations/clear',
                    data: { confirm: true }
                })
                    .success(function ( data ) {
                        var count = data.result[0].count;

                        $scope.$emit('systemMessage', {
                            type: 'info', 
                            content: translationService.get('Alle opgeslagen annotaties (' + count + ') zijn gewist.')
                        });
                    })
                    .error(function ( data ) {
                        var error = data.result ? data.result[0] : null,
                            type = error ? error.type : '',
                            errorObj = {
                                content: 'Annotaties konden niet worden gewist',
                                type: 'error'
                            };
                                        
                        $scope.$emit('systemMessage', errorObj);
                    });
            };
            

        }]);
}());
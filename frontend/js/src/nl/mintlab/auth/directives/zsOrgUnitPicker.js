/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.auth')
		.directive('zsOrgUnitPicker', [ 'organisationalUnitService', function ( organisationalUnitService ) {
			
			return {
				scope: true,
				require: 'ngModel',
				templateUrl: '/html/directives/auth/org-unit-picker.html',
				link: function ( scope, element, attrs, ngModel ) {
					
					scope.org_unit_id = null;
					scope.role_id = null;
					
					var units = [];
					
					organisationalUnitService.getUnits().then(function ( data ) {
						// var unit,
						// 	role;
						
						units = data;
						if(ngModel.$viewValue) {
							scope.org_unit_id = ngModel.$viewValue.org_unit_id;
							scope.role_id = ngModel.$viewValue.role_id;
						}
						//  else {
						// 	unit = scope.getUnits()[0];
						// 	if(unit) {
						// 		scope.org_unit_id = unit.org_unit_id;
						// 		role = scope.getRolesForUnit(unit.org_unit_id)[0];
						// 		if(role) {
						// 			scope.role_id = role.role_id;
						// 		}
						// 		ngModel.$setViewValue({
						// 			org_unit_id: scope.org_unit_id,
						// 			role_id: scope.role_id
						// 		});
						// 	}
						// }
					});
					
					scope.getUnits = function ( ) {
						return _.filter(units, function ( unit ) {
							return unit.depth > 0;
						});
					};
					
					scope.getRolesForUnit = function ( orgUnitId ) {
						if(!orgUnitId) {
							orgUnitId = 0;
						}
						var unitObj = _.find(units, function ( u ) {
							return u.org_unit_id === orgUnitId;
						});
						return unitObj ? unitObj.roles : [];
					};
					
					scope.getRoleName = function ( role ) {
						return (!role.system ? '* ' : '') + role.name;
					};
					
					scope.handleUnitChange = function ( ) {
						var roleId,
							roles,
							roleIsAvailable,
							unit = _.find(units, function ( u ) {
								return u.org_unit_id === scope.org_unit_id;
							});
							
							
						if(unit) {
							roleId = unit.role_id;
							roles = scope.getRolesForUnit(unit.org_unit_id);
							roleIsAvailable = !!_.find(roles, function ( r ) {
								return roleId === r.role_id;
							});
						}
						
						if(!roleIsAvailable && roles) {
							scope.role_id = roles[0].role_id;
						} else {
							scope.role_id = null;
						}
						
					};
					
					scope.$watch('org_unit_id', function ( nwValue, oldValue/*, scope*/ ) {
						var obj;
						if(nwValue !== oldValue) {
							obj = ngModel.$viewValue || {};
							ngModel.$setViewValue({
								org_unit_id: nwValue,
								role_id: nwValue ? obj.role_id : null
							});
						}
					});
					
					scope.$watch('role_id', function ( nwValue, oldValue/*, scope*/ ) {
						var obj;
						if(nwValue !== oldValue) {
							obj = ngModel.$viewValue || {};
							ngModel.$setViewValue({
								org_unit_id: obj.org_unit_id,
								role_id: nwValue
							});
						}
					});
					
					ngModel.$formatters.push(function ( val ) {
						if(val) {
							scope.org_unit_id = val.org_unit_id;
							scope.role_id = val.role_id;
						} else {
							scope.org_unit_id = null;
							scope.role_id = null;
						}
						
						return val;
					});
					
				}
			};
			
		}]);
	
})();
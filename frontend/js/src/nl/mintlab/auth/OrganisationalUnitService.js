/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.auth')
		.service('organisationalUnitService', [ '$q', 'smartHttp', function ( $q, smartHttp ) {
			
			var organisationalUnitService = {},
				loading,
				deferred;
			
			organisationalUnitService.getUnits = function ( ) {
				if(deferred) {
					return deferred.promise;
				}
				
				deferred = $q.defer();
				
				loading = true;
				
				smartHttp.connect({
					method: 'GET',
					url: '/api/authorization/org_unit',
					params: {
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						deferred.resolve(response.result);
					})
					.error(function ( /*response*/ ) {
						deferred.reject();
					})
					['finally'](function ( ) {
						loading = false;
					});
				
				return deferred.promise;
			};
			
			return organisationalUnitService;
			
		}]);
	
})();
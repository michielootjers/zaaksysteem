/*global angular,window,$,document,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem', [
		'ngRoute',
		'Zaaksysteem.admin',
		'Zaaksysteem.auth',
		'Zaaksysteem.form',
		'Zaaksysteem.core',
		'Zaaksysteem.docs',
		'Zaaksysteem.timeline',
		'Zaaksysteem.kcc',
		'Zaaksysteem.case',
		'Zaaksysteem.net',
		'Zaaksysteem.data',
		'Zaaksysteem.sysin',
		'Zaaksysteem.object',
		'Zaaksysteem.notification',
		'Zaaksysteem.pip',
		'Zaaksysteem.user',
		'Zaaksysteem.widget' 
	])
		.config([ '$httpProvider', '$interpolateProvider', 'smartHttpProvider', '$anchorScrollProvider', function ( $httpProvider, $interpolateProvider, smartHttpProvider, $anchorScrollProvider) {
					
			$interpolateProvider.startSymbol('<[');
			$interpolateProvider.endSymbol(']>');
			
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.withCredentials = true;
			
			$anchorScrollProvider.disableAutoScrolling();
			
			var prefix = '/',
				loc = window.location.href;
				
			if(loc.match(/^http:\/\/localhost/)) {
				prefix = 'http://dev1.munt.zaaksysteem.nl:3009/';
			}
			
			smartHttpProvider.defaults.prefix = prefix;
			
		}])
		.run([ '$rootScope', '$compile', '$cookies', function ( $rootScope, $compile, $cookies ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			$rootScope.$on('legacyDomLoad', function ( event, jq ) {
				var i,
					l,
					el,
					scope;
					
				for(i = 0, l = jq.length; i < l; ++i) {
					el = jq[i];
					scope = angular.element(el).scope();
					safeApply(scope, function ( ) {
						$compile(el)(scope);
					});
				}
			});
			
			// FIXME(dario): this is very unclean, perhaps a define/fetch() method?
			window.getXSRFToken = function ( ) {
				return $cookies['XSRF-TOKEN'];
			};

            if($cookies['XSRF-TOKEN']) {
                $.ajaxSetup({
                    headers: { 'X-XSRF-TOKEN': $cookies['XSRF-TOKEN'] }
                });
            }
			
			$(document).ajaxSend(function ( event, xhr/*, options*/ ) {
				var accessToken = $cookies['XSRF-TOKEN'];
				xhr.setRequestHeader('X-XSRF-TOKEN', accessToken);
			});
			
		}]);
})();

// console.log('user agent: ' + navigator.userAgent);

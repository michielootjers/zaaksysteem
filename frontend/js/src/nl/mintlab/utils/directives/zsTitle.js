/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsTitle', [ '$document', '$timeout', 'templateCompiler', function ( $document, $timeout, templateCompiler ) {
		
		var setMouseEnabled = fetch('nl.mintlab.utils.dom.setMouseEnabled'),
			fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
			getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
			tpl = '/partials/directives/tooltip/tooltip.html',
			body = $document.find('body'),
			tooltip,
			span,
			arrow,
			origin;
			
		function createTooltip ( element ) {
			tooltip = angular.element(element);
			tooltip.css('position', 'fixed');
			span = tooltip.find('span').eq(0);
			arrow = angular.element(tooltip[0].querySelector('.title-tooltip-arrow'));
			setMouseEnabled(tooltip[0], false);
			body.append(element);
			hideTooltip();
		}
		
		function setPosition ( ) {
			var viewportWidth = getViewportSize().width,
				elementWidth = origin[0].clientWidth,
				elementHeight = origin[0].clientHeight,
				tooltipWidth = tooltip[0].clientWidth,
				tooltipHeight = tooltip[0].clientHeight,
				arrowWidth = arrow[0].clientWidth,
				topLeft,
				rect,
				registration,
				offsetY = 0,
				x = 0,
				y = 0,
				arrowX,
				d;
				
			tooltip.css('left', '');
			tooltip.css('top', '');
			arrow.css('left', '');
			
			topLeft = fromLocalToGlobal(origin[0], { x: 0, y: 0 });
			rect = { x: topLeft.x, y: topLeft.y, width: elementWidth, height: elementHeight };
				
			if(rect.y - offsetY - tooltipHeight >= 0) {
				registration = 'top';
			} else {
				registration = 'bottom';	
			}
			
			tooltip.attr('data-zs-title-registration', registration);
			
			switch(registration) {
				case 'top':
				y = rect.y - tooltipHeight;
				break;
				
				case 'bottom':
				y = rect.y + rect.height/2 - tooltipHeight/2;
				break;
			}
			
			x = rect.x + rect.width/2 - tooltipWidth/2;
			arrowX = tooltipWidth/2 - arrowWidth/2;
			
			d = x - 0;
			if(d < 0 ) {
				x -= d;
				arrowX += d;
			}
			
			d = x + tooltipWidth - viewportWidth;
			if(d > 0) {
				x -= d;
				arrowX += d;
			}
			
			tooltip.css('left', x + 'px');
			tooltip.css('top', y + 'px');
			arrow.css('left', arrowX + 'px');
			
		}
				
		function showTooltip ( ) {
			tooltip.addClass('tooltip-visible');
			tooltip.removeClass('tooltip-hidden');
			setPosition();
		}
		
		function hideTooltip ( ) {
			tooltip.removeClass('tooltip-visible');
			tooltip.addClass('tooltip-hidden');
		}
		
		function setOrigin ( element ) {
			origin = element;
			if(origin) {
				setPosition();
			}
		}
			
		templateCompiler.getElement(tpl).then(createTooltip);
		
		
		return {
			scope: true,
			link: function ( scope, element, attrs ) {
				
				var label;
				
				function onMouseOver ( ) {
					label = attrs.zsTitle;
					
					if(!label) {
						setOrigin(null);
						return;
					}
					
					span.text(label);
					
					setOrigin(element);
					showTooltip();
				}
				
				function onMouseOut ( ) {
					hideTooltip();
				}
				
				element.bind('mouseover', onMouseOver);
				element.bind('mouseout', onMouseOut);
				
				var destroyUnbind = scope.$on('$destroy', function ( ) {
					destroyUnbind();
					hideTooltip();
				});
				
			}
		};
		
	}]);
	
})();
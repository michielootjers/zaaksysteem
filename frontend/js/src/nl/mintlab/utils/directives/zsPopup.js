/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopup', [ '$timeout', '$document', 'templateCompiler', function ( $timeout, $document, templateCompiler ) {
			
			var fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				body = $document.find('body');
			
			return {
				scope: true,
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var popup,
						isOpen,
						compiler;
					
					var options = this.options = {
						followMouse: false,
						registration: 'top'
					};
					
					function openPopup ( x, y ) {
						
						isOpen = true;
						x = x || 0;
						y = y || 0;
					
						templateCompiler.getCompiler($scope.$eval($attrs.zsPopup)).then(function ( cpl ) {
							compiler = cpl;
							
							$timeout(function ( ) {
								var point;
								
								compiler($scope, function ( clonedElement/*, scope*/ ) {
									
									popup = clonedElement;
									
									body.append(popup);
									
									if(!options.followMouse) {
										point = fromGlobalToLocal(popup[0].offsetParent, { x: x, y: y });
										popup.css('top', point.y + 'px');
										popup.css('left', point.x + 'px');
									} else {
										//TODO(dario): implement followMouse
									}
									
									$scope.$emit('popupopen', popup);
								});
							});
						});
					}
					
					function closePopup ( ) {
						if(popup) {
							$scope.$emit('popupclose', popup);
							isOpen = false;
							popup.remove();
							popup = null;
						}
					}
					
					$scope.openPopup = function ( /*event or x, y*/ ) {
						var x,
							y,
							event;
						
						if(typeof arguments[0] === "number") {
							x = arguments[0];
							y = arguments[1];
						} else if(arguments[0]) {
							event = arguments[0];
							x = event.pageX;
							y = event.pageY;
							event.stopPropagation();
						}
						
						openPopup(x, y);
					};
					
					$scope.closePopup = function ( ) {
						closePopup();
					};
					
					$scope.isOpen = function ( ) {
						return isOpen;
					};
					
				}]
			};
			
		
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	var MIN_OFFSET = 3;
	
	angular.module('Zaaksysteem')
		.directive('ngDraggable', [ '$window', '$document', '$compile', '$timeout', 'dropManager', 'templateCompiler', function ( $window, $document, $compile, $timeout, dropManager, templateCompiler  ) {
			
			var getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				setMouseEnabled = fetch('nl.mintlab.utils.dom.setMouseEnabled'),
				element = $document[0].createElement('div'),
				body = $document.find('body'),
				hasPartialDragNDropSupport,
				hasFullDragNDropSupport,
				defaultMimetype = 'text/json',
				link,
				compile;
				
			hasFullDragNDropSupport = 'draggable' in element;
			hasPartialDragNDropSupport = !hasFullDragNDropSupport && !!element.dragDrop;
			
			link = function ( scope, element, attrs ) {
				
				var	enabled = false;
				var that = this;
				
				if(attrs.ngDragMimetype === undefined) {
					attrs.ngDragMimetype = defaultMimetype;
					element.attr('data-ng-drag-mimetype', defaultMimetype);
				}
				
				if(attrs.ngDragImage) {
					templateCompiler.getCompiler(scope.$eval(attrs.ngDragImage)).then(function ( cpl ) {
						that.setDragImage(cpl);
					});
				}
				
				function evaluateDirective ( ) {
					var shouldBeEnabled = attrs.ngDraggable !== '' ? scope.$eval(attrs.ngDraggable) : true;
					if(shouldBeEnabled !== enabled) {
						if(shouldBeEnabled) {
							enabled = true;
							that.enable();
						} else {
							enabled = false;
							that.disable();
						}
					}
				}
				
				scope.$watch(function ( ) {
					evaluateDirective();
				});
				
			};
			
			if(hasFullDragNDropSupport) {
				
				compile = function ( ) {
					return function ( scope, element, attrs ) {
					
						var that = {},
							mimetype,
							dragImage;
						
						that.enable = function ( ) {
							element.attr('draggable', 'true');
							element.bind('dragstart', onDragStart);
							element.bind('dragend', onDragEnd);
						};
						
						that.disable = function ( ) {
							element.removeAttr('draggable');
							element.unbind('dragstart', onDragStart);
							element.unbind('dragend', onDragEnd);
						};
						
						that.setDragImage = function ( cpl ) {
							dragImage = cpl;
						};
						
						function onDragStart ( event ) {
							event.stopPropagation();
							initDrag(event);
						}
						
						function onDragEnd ( event ) {
							exitDrag(event);
						}
						
						function initDrag ( event ) {
							var dataTransfer = event.dataTransfer,
								data = scope.$eval(attrs.ngDragData),
								dropId = dropManager.createDrop(mimetype, data);
								
							function applyDragImage ( ) {
								if(dragImage) {
									dragImage(scope, function ( clonedElement/*, scope*/ ) {
										body.append(clonedElement);
										dataTransfer.setDragImage(clonedElement[0], 0, 0);
										$timeout(function ( ) {
											clonedElement.remove();
										});
									});
								}	
							}
							
							if(!scope.$$phase && !scope.$root.$$phase) {
								scope.$apply(applyDragImage);
							} else {
								applyDragImage();
							}
							
							dataTransfer.setData('text', dropId);
							scope.$emit('startdrag', element, mimetype);
						}
						
						function exitDrag ( /*event*/ ) {
							scope.$emit('stopdrag', element, mimetype);
						}
						
						link.call(that, scope, element, attrs);
						mimetype = attrs.ngDragMimetype;
					
					};
				};
				
			} else {
				compile = function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function ( scope, element, attrs ) {
				
						var that = {},
							mimetype,
							prevDroppable = null,
							dragImage,
							dragImageRendered,
							offset,
							origin;
							
						// use addEventListener because somehow bind only triggers 
						// the event the first time
				
						that.enable = function ( ) {
							addEventListener(element[0], 'mousedown', onMouseDown);
						};
						
						that.disable = function ( ) {
							removeEventListener(element[0], 'mousedown', onMouseDown);
						};
						
						that.setDragImage = function ( cpl ) {
							dragImage = cpl;
						};
						
						function onMouseDown ( event ) {
							
							origin = getMousePosition(event);
							
							body.bind('mouseup', onMouseUp);
							body.bind('mousemove', onInitMouseMove);
							
							return cancelEvent(event, false);
						}
						
						function onInitMouseMove ( event ) {
							var pos = getMousePosition(event);
							
							if(!(Math.abs(origin.x - pos.x) >= MIN_OFFSET || Math.abs(origin.y - pos.y) >= MIN_OFFSET)) {
								return;
							}
							
							body.unbind('mousemove', onInitMouseMove);
							startDrag(event);
							return cancelEvent(event, true);
						}
						
						function startDrag ( event ) {
							
							body.bind('mousemove', onMouseMove);
							body.bind('keyup', onKeyUp);
							
							if(dragImage) {
								dragImage(scope, function ( clonedElement/*, scope*/ ) {
									dragImageRendered = clonedElement;
									body.append(dragImageRendered);
									setMouseEnabled(dragImageRendered[0], false);
									positionDragImage(event, true);
								});
							}
							
							element.addClass('drag-active');
							scope.$emit('startdrag', element, mimetype);
						}
						
						function attemptDrop ( ) {
							if(prevDroppable) {
								prevDroppable.scope().performDrop(getDropData(), mimetype);
								prevDroppable.removeClass('drag-over');
								prevDroppable = null;
							}
							stopDrag();
						}
						
						function cancelDrag ( ) {
							if(prevDroppable) {
								prevDroppable.removeClass('drag-over');
								prevDroppable = null;
							}
							stopDrag();
						}
						
						function stopDrag ( ) {
							
							body.unbind('mousemove', onInitMouseMove);
							body.unbind('mousemove', onMouseMove);
							body.unbind('mouseup', onMouseUp);
							// unbinding an unregistered listener results in errors in IE8 (via angular's indexOf)
							try {
								body.unbind('keyup', onKeyUp);
							} catch ( error ) {
								
							}
							
							if(dragImageRendered) {
								dragImageRendered.remove();
								dragImageRendered = null;
							}
							element.removeClass('drag-active');
							scope.$emit('stopdrag', element, mimetype);
						}
						
						function positionDragImage ( event, isInitialMove ) {
							var mousePos,
								pos;
								
							if(!dragImageRendered) {
								return;
							}
							
							mousePos = getMousePosition(event);
							pos = fromLocalToGlobal(body[0], mousePos);
							
							if(isInitialMove) {
								if(attrs.ngDragImage) {
									offset = { x: 0, y: 0 };
								} else {
									offset = fromGlobalToLocal(element[0], pos);
								}
							}
							
							pos.y -= offset.y;
							pos.x -= offset.x;
							
							dragImageRendered.css('top', pos.y + 'px');
							dragImageRendered.css('left', pos.x + 'px');
							
						}
						
						function onMouseMove ( event ) {
							var mousePos = getMousePosition(event),
								pos = mousePos,
								droppable = dropManager.getDroppable(pos, element);
								
							positionDragImage(event);
							
							if(prevDroppable !== droppable) {
								if(prevDroppable) {
									prevDroppable.removeClass('drag-over');
								}
								prevDroppable = droppable;
								if(droppable) {
									droppable.addClass('drag-over');
								}
							}
						}
						
						function onMouseUp ( /*event*/ ) {
							attemptDrop();
						}
						
						function onKeyUp ( event ) {
							if(event.keyCode === 27) {
								cancelDrag();
							}
						}
						
						function getDropData ( ) {
							return scope.$eval(attrs.ngDragData);
						}
						
						that.setDragImage(function ( scope, callback ) {
							var clone = element.clone();
							clone.addClass('as-drag-image');
							callback(clone,scope);
						});
						
						link.call(that, scope, element, attrs);
						
						element.find('*').attr('unselectable', 'on');
						
						mimetype = element.attr('data-ng-drag-mimetype');
					};
				};
			}
			
			return {
				compile: compile,
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					return {
						isEnabled: function ( ) {
							return $attrs.ngDraggable !== '' ? $scope.$eval($attrs.ngDraggable) : true;
						}
					};
				}],
			};
			
		} ]);
	
})();
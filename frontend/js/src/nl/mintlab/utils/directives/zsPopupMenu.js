/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopupMenu', [ '$document', function ( $document ) {
			
			var contains = fetch('nl.mintlab.utils.dom.contains');
			
			return {
				scope: false,
				controller: [ function ( ) {
					var list;
					
					return {
						setList: function ( element ) {
							if(list) {
								list.removeClass('zs-popup-menu-list');
							}
							list = element;
							if(list) {
								list.addClass('zs-popup-menu-list');
							}
						},
						getList: function ( ) {
							return list;
						}
					};
				}],
				compile: function ( ) {
					
					return function link ( scope, element, attrs ) {
						
						var isOpen = null,
							button = element.find('button'),
							behaviour = 'click';
						
						function onDocumentClick ( event ) {
							scope.$apply(function ( ) {
								var clickTarget = event.target;
								
								if(!(contains(element[0], clickTarget))) {
									closeMenu();
								}
							});
						}
						
						function onElementMouseOut ( /*event*/ ) {
							scope.$apply(function ( ) {
								closeMenu();
							});
						}
						
						function openMenu ( ) {
							
							if(isOpen) {
								return;
							}
							
							isOpen = true;							
							
							element.addClass('zs-popup-menu-open');
							element.removeClass('zs-popup-menu-closed');
						
							if(behaviour === 'click') {
								$document.bind('click', onDocumentClick);
							} else {
								element.bind('mouseleave', onElementMouseOut);
							}
							
							
							scope.$emit('zs-popup-menu.open');
							
						}
						
						function closeMenu ( ) {
							
							if(!isOpen) {
								return;
							}
							
							isOpen = false;
							
							element.addClass('zs-popup-menu-closed');
							element.removeClass('zs-popup-menu-open');
							
							if(behaviour === 'click') {
								$document.unbind('click', onDocumentClick);
							} else {
								element.unbind('mouseleave', onElementMouseOut);
							}
							
							scope.$emit('zs-popup-menu.close');
						}
						
						scope.openPopupMenu = function ( ) {
							openMenu();	
						};
						
						scope.closePopupMenu = function ( ) {
							closeMenu();	
						};
						
						scope.isPopupMenuOpen = function ( ) {
							return isOpen;
						};
						
						function onTrigger ( /*evt*/ ) {
							scope.$apply(function ( ) {
								if(isOpen) {
									closeMenu();
								} else {
									openMenu();
								}
							});
						}
						
						function unbind ( ) {
							var type = behaviour === 'hover' ? 'mouseenter' : 'click';
							button.unbind(type, onTrigger);
						}
						
						function bind ( ) {
							var type = behaviour === 'hover' ? 'mouseenter' : 'click';
							button.bind(type, onTrigger);
						}
						
						attrs.$observe('zsPopupMenuBehaviour', function ( ) {
							unbind();
							behaviour = attrs.zsPopupMenuBehaviour === 'hover' ? 'hover' : 'click';
							bind();
						});
						
						button.addClass('zs-popup-menu-button');
						
						element.find('ul').eq(0).addClass('zs-popup-menu-list');
						element.addClass('zs-popup-menu');
						element.addClass('zs-popup-menu-closed');
						
						closeMenu();
						
					};
					
				}
			};
		}]);
	
})();
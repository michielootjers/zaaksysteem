/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPagination', [ function ( ) {
			
			return {
				scope: {
					'currentPage': '@zsPaginationCurrent',
					'numPages': '@zsPaginationTotal',
					'numRows': '@zsPaginationNumRows'
				},
				templateUrl: '/html/directives/pagination/pagination.html',
				compile: function ( ) {
					
					return function link ( scope/*, element, attrs*/ ) {
						
						scope.getAvailablePages = function ( ) {
							var numPages = parseInt(scope.numPages, 10),
								currentPage = parseInt(scope.currentPage, 10),
								pages = [ 1, numPages ],
								middle = Math.max(3, Math.min(numPages-2, currentPage));
							
							pages.push(middle-1, middle, middle+1);
							
							pages = _.unique(_.filter(pages,
										function ( num ) {
											return num >= 1 && num <= numPages;
										}));
										
							pages.sort(function ( a, b) {
								return a > b ? 1 : a < b ? -1 : 0;
							});
							
							return pages;
						};
						
						scope.showPage = function ( page ) {
							scope.$emit('zs.pagination.page.change', page);
						};
						
						scope.hasPage = function ( page ) {
							return _.indexOf(scope.getAvailablePages(), page) !== -1;
						};
						
						scope.onNumRowsChange = function ( ) {
							scope.$emit('zs.pagination.numrows.change', scope.numRows);
						};
						
						scope.$watch('numRows', function ( ) {
							scope.numRows = parseInt(scope.numRows, 10);
						});
					};
					
				}
				
			};
			
		}]);
	
})();
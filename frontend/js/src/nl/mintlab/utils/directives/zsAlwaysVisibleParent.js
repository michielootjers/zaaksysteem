/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsAlwaysVisibleParent', [ function ( ) {
			
			return {
				controller: [ '$scope', function ( $scope ) {
					
					var element;
					
					function trigger ( ) {
						$scope.$broadcast('zs.always.visible.reference.update', element);
					}
					
					return {
						getElement: function ( ) {
							return element;
						},
						setElement: function ( el ) {
							element = el;
							trigger();
						},
						trigger: trigger
					};
				}]
			};
			
		}]);
	
})();
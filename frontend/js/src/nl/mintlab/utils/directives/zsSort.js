/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSort', [ '$document', function ( $document ) {
			
			var getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
				getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				scope: false,
				controller: [ '$scope', '$element', '$attrs', function ( $scope/*, $element, $attrs*/ ) {
					var sortables = [],
						tracking;
					
					return {
						track: function ( element ) {
							tracking = element;
							$scope.$emit('zs.sort.track.start', element);
						},
						untrack: function ( element ) {
							tracking = null;
							$scope.$emit('zs.sort.track.stop', element);
						},
						getTrackedElement: function ( ) {
							return tracking;
						},
						register: function ( element, data ) {
							sortables.push({
								element: element,
								data: data
							});
						},
						unregister: function ( element/*, data*/ ) {
							_.pull(sortables, function ( sortable ) {
								return sortable.element === element;
							});
						},
						getDataFor: function ( element ) {
							var sortable = _.find(sortables, function ( sortable ) {
									return sortable.element === element;
								});
							
							return sortable ? sortable.data : undefined;
						},
						getSortables: function ( ) {
							return sortables;
						}
					};
				}],
				link: function ( scope, element, attrs, controller ) {
						
					var trackingEl;
					
					function track ( el ) {
						
						trackingEl = el;
						trackingEl.addClass('zs-sorting');
						
						$document.bind('dragover', onDragOver);
						$document.bind('drop', onDrop);
						$document.bind('mousemove', onMouseMove);
					}
					
					function untrack ( el ) {
						
						var data = controller.getDataFor(el);
						
						el.removeClass('zs-sorting');
						trackingEl = null;
						
						$document.unbind('dragover', onDragOver);
						$document.unbind('drop', onDrop);
						$document.unbind('mousemove', onMouseMove);	
						
						scope.$emit('zs.sort.commit', data);
					}
					
					function onDragOver ( event ) {
						var pos = getMousePosition(event);
						positionTrackedElement(pos);
						cancelEvent(event);
					}
					
					function onDrop ( event ) {
						return cancelEvent(event);
					}
					
					function onMouseMove ( event ) {
						var pos = getMousePosition(event);
						positionTrackedElement(pos);
					}
					
					function positionTrackedElement ( pos ) {
						var before,
							beforeData,
							trackedData;
						
						before = getBefore(attrs.zsSortDirection, pos);
						
						beforeData = before;
						trackedData = controller.getDataFor(controller.getTrackedElement());
						
						if(beforeData !== trackedData) {
							scope.$apply(function emitUpdate ( ) {
								scope.$emit('zs.sort.update', trackedData, beforeData);	
							});	
						}
					}
					
					function getBefore ( type, pos ) {
						
						var x,
							left,
							width,
							rect,
							before,
							el,
							i,
							l,
							sortables = controller.getSortables(),
							sortable,
							children = element[0].childNodes,
							elements = [];
						
						switch(type) {
							default:
							case 'vertical':
							x = 'y';
							left = 'top';
							width = 'height';
							break;
							
							case 'horizontal':
							x = 'x';
							left = 'left';
							width = 'width';
							break;
						}
						
						for(i = 0, l = children.length; i < l; ++i) {
							el = children[i];
							sortable = _.find(sortables, function ( s ) {
								return s.element[0] === el;
							});
							if(sortable) {
								rect = getViewportPosition(el);
								elements.push({ position: rect[left], rect: rect, sortable: sortable });
							}
						}
						
						elements = _.sortBy(elements, 'position');
											
						for(i = 0, l = elements.length; i < l; ++i) {
							el = elements[i];
							sortable = el.sortable;
							rect = el.rect;
							if(pos[x] <= rect[left] + rect[width]/2) {
								before = sortable.data;
								break;
							}
						}
						
						if(pos[x] > rect[left] + rect[width]/2) {
							before = null;
						}
						
						return before;
						
					}
					
					scope.$on('zs.sort.track.start', function ( event, element ) {
						track(element);
					});
					
					scope.$on('zs.sort.track.stop', function ( event, element ) {
						untrack(element);
					});
					
				}
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('script', [ '$rootScope', function ( $rootScope ) {
			
			return {
				restrict: 'E',
				scope: true,
				terminal: 'true',
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						if(attrs.type === 'text/zs-scope-data') {
							var data,
								parentScope;
								
							if(attrs.zsScopeDataRoot !== undefined) {
								parentScope = $rootScope;
							} else {
								// restrict: 'E' always creates new scope
								parentScope = scope.$parent;
							}
								
							try {
								data = JSON.parse(element[0].innerHTML);
								for(var key in data) {
									parentScope[key] = data[key];
								}
								scope.$emit('zs.scope.data.apply', parentScope, data);
							} catch ( error ) {
								console.log('error', error);
								scope.$emit('zs.scope.data.error', parentScope, error);
							}
							
						}
					};
					
				}
			};
			
		}]);
	
})();
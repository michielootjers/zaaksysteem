/*global angular,ezra_dialog,$,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsEzraDialog', [ function ( ) {
			
			var cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				link: function ( scope, element, attrs ) {
					
					var isOpen = false,
						isDestroyed = false;
					
					function safeApply ( fn ) {
						var phase = scope.$root.$$phase;
						if(phase === '$apply' || phase === '$digest') {
							fn();
						} else {
							scope.$apply(fn);
						}
					}
					
					function onDialogOpen ( ) {
						isOpen = true;
						safeApply(function ( ) {
							scope.$emit('zs.ezra.dialog.open');
						});
					}
					
					function onDialogClose ( ) {
						isOpen = false;
						$.ztWaitStop();
						
						safeApply(function ( ) {
							scope.$emit('zs.ezra.dialog.close');
						});
						
						if(!isDestroyed) {
							removeListeners();
						}
					}
					
					function addListeners ( ) {
						$('#dialog').on('dialogopen', onDialogOpen);
						$('#dialog').on('dialogclose', onDialogClose);
					}
					
					function removeListeners ( ) {
						$('#dialog').off('dialogopen', onDialogOpen);
						$('#dialog').off('dialogclose', onDialogClose);
					}
					
					function onClick ( event ) {
						scope.$apply(function ( ) {
							var params = scope.$eval(attrs.zsEzraDialog) || {};
							addListeners();
							ezra_dialog(params, function ( ) {
							});
							cancelEvent(event);
						});
					}
					
					element.bind('click', onClick);
					
					scope.$on('$destroy', function ( ) {
						element.unbind('click', onClick);
						isDestroyed = true;
						if(!isOpen) {
							removeListeners();
						}
					});
				}
			};
			
		}]);
})();
/*global fetch,define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.fromGlobalToLocal', function ( ) {
		
		var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element, point ) {
			var docPos = getViewportPosition(element),
				x = point.x - docPos.x,
				y = point.y - docPos.y;
				
			return { x: x, y: y };
		};
	});
		
})();
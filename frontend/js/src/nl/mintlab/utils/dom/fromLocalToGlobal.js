/*global define,fetch*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.fromLocalToGlobal', function ( ) {
		
		var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element, point ) {
			var docPos = getViewportPosition(element),
				x = docPos.x + point.x,
				y = docPos.y + point.y;
				
			return { x: x, y: y };
		};
		
	});
	
})();
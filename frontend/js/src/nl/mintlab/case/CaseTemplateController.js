/*global angular,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseTemplateController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.getCaseDocById = function ( id ) {
				var caseDoc,
					i,
					l;
					
				for(i = 0, l = $scope.caseDocs.length; i < l; ++i) {
					caseDoc = $scope.caseDocs[i];
					if(caseDoc.bibliotheek_kenmerken_id.id === id) {
						return caseDoc;
					}
				}
				
				return null;
				
			};
			
			$scope.getExtension = function ( template ) {
				return '.' + (template.target_format || 'odt').toLowerCase();
			};
			
			$scope.useTemplate = function ( ) {
				var filename = $scope.filename,
					caseDocId = $scope.caseDocument ? $scope.caseDocument.id : null,
					targetFormat = $scope.targetFormat;
				
				$('.tab-documents > a').click();
				var templateControllerScope = angular.element($('[data-ng-controller="nl.mintlab.docs.TemplateController"]')[0]).scope();
				templateControllerScope.createFileFromTemplate(filename, $scope.template, caseDocId, targetFormat).then(
					function onSuccess ( ) {
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Sjabloon "%s" toegevoegd', $scope.filename + '.' + targetFormat)
						});

// HACK for presentation for Desom.
$('.ezra_load_zaak_element_loaded').removeClass('ezra_load_zaak_element_loaded');
// END
						$scope.closePopup();
					}, function onError ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Sjabloon "%s" kon niet worden toegevoegd', $scope.filename + '.' + targetFormat)
						});
						$scope.closePopup();
				});
			};
			
			$scope.saveTemplate = function ( ) {
				
				$scope.action.data.filename = $scope.filename;
				$scope.action.data.bibliotheek_kenmerken_id = $scope.caseDocument ? $scope.caseDocument.bibliotheek_kenmerken_id.id : null;
				
				var data = {
					id: $scope.action.id,
					filename: $scope.action.data.filename,
					bibliotheek_kenmerken_id: $scope.action.data.bibliotheek_kenmerken_id,
					target_format: $scope.targetFormat
				};
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/data',
					data: data
				})
					.success(function ( data ) {
						
						var itemData = data.result[0];
						for(var key in itemData) {
							$scope.action[key] = itemData[key];
						}
						
						$scope.targetFormat = $scope.action.data.target_format;
						
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Instellingen voor "%s" opgeslagen', $scope.action.label)
						});
						$scope.closePopup();
					})
					.error(function ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Instellingen voor "%s" konden niet worden opgeslagen', $scope.action.label)
						});
						$scope.closePopup();
					});
			};
			
			
			$scope.filename = $scope.action.data.filename;
			$scope.caseDocument = $scope.getCaseDocById($scope.action.data.bibliotheek_kenmerken_id);
			$scope.targetFormat = $scope.action.data.target_format;
			
			$scope.$watch('templates', function ( /*nw, old*/ ) {
				var templates = $scope.templates || [];
				for(var i = 0, l = templates.length; i < l; ++i) {
					if(templates[i].bibliotheek_sjablonen_id.id === $scope.action.data.bibliotheek_sjablonen_id) {
						$scope.template = templates[i];
						break;
					}
				}
			});
			
			
		}]);
	
})();
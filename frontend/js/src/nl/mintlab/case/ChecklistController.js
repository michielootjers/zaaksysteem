/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.ChecklistController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.addItem = function ( label ) {
				var item = {
						id: -1,
						label: label,
						user_defined: true,
						checked: false
					};
				
				$scope.checklistItems.push(item);
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/checklist/add_item',
					data: {
						label: label,
						milestone: $scope.phaseId
					}
				})
					.success(function ( data ) {
						var itemData = data.result[0];
						for(var key in itemData) {
							item[key] = itemData[key];
						}
					})
					.error(function ( ) {
						var index = indexOf($scope.checklistItems, item);
						$scope.checklistItems.splice(index, 1);
					});
			};
			
			$scope.removeItem = function ( item ) {
				
				var index = indexOf($scope.checklistItems, item);
				
				$scope.checklistItems.splice(index, 1);
				
				smartHttp.connect( {
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/checklist/remove_item',
					data: {
						item_id: item.id
					}
				})
					.success(function ( /*data*/ ) {
						
					})
					.error(function ( /*data*/ ) {
						$scope.checklistItems.push(item);
					});
			};
			
			$scope.updateItem = function ( item ) {
				
				var data = {
					
				};
				
				if($scope.closed) {
					item.checked = !item.checked;
					return;
				}
				
				data[item.id] = item.checked;
				
				smartHttp.connect( {
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/checklist/update',
					data: {
						data: data
					}
				})
					.success(function ( /*data*/ ) {
						
					})
					.error(function ( /*data*/ ) {
						
					});
				
			};
			
		}]);
	
})();
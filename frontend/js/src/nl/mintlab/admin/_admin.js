/*global angular*/
(function () {
    "use strict";

    angular.module('Zaaksysteem.admin', [ 'Zaaksysteem.admin.casetype' ]);

})();

/*global angular,fetch*/
(function ( ) {

	angular.module('Zaaksysteem')
		.controller('nl.mintlab.message.SystemMessageController', [ '$scope', '$rootScope', '$sce', 'dataStore', 'translationService', function ( $scope, $rootScope, $sce, dataStore, translationService ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				safeApply = fetch('nl.mintlab.utils.safeApply');

			$scope.messages = [
			];

			function addMessage ( message ) {
				if(message.type === 'error' && !message.content) {
					message.content = translationService.get('Er is een onbekende fout opgetreden. Ververs de pagina en probeer het opnieuw.')
				}
				$scope.messages.push(message);
			}

			$scope.closeMessage = function ( message ) {
				var index = indexOf($scope.messages, message);
				if(index !== -1) {
					$scope.messages.splice(index, 1);
				}
			};
			
			$scope.shouldAutohide = function ( message ) {
				return message.type !== 'error';	
			};
			
			$scope.getTimeout = function ( message ) {
				return $scope.shouldAutohide(message) ? undefined : 0;
			};
			
			$scope.getMessageContent = function ( message ) {
				return $sce.trustAsHtml(message.content);
			};

			$rootScope.$on('systemMessage', function ( event, message ) {
				safeApply($scope, function ( ) {
					addMessage(message);
				});
			});
			
			dataStore.observe('messages', function ( event, messages ) {
				
				if(!messages) {
					// FIXME
					return;
				}
				
				function addToScope ( ) {
					var i,
						l,
						message;

					for(i = 0, l = messages.length; i < l; ++i) {
						message = messages[i];
						if(indexOf($scope.messages, message) === -1) {
							addMessage(message);
						}
					}
				}
				
				if(!$scope.$$phase && !$scope.$root.$$phase) {
					$scope.$apply(addToScope);
				} else {
					addToScope();
				}
					
			});
			
			$scope.$on('zsTimerComplete', function ( event ) {
				safeApply($scope, function ( ) {
					var message = event.targetScope.message;
					if(message) {
						$scope.closeMessage(message);
					}
					event.stopPropagation();
				});
			});

		}]);

})();
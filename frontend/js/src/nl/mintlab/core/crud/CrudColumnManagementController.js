/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudColumnManagementController', [ '$scope', function ( $scope ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				q;
			
			$scope.columnResults = [];
			$scope.loading = false;
			$scope.query = '';
			
			function updateColumns ( ) {
				if($scope.query !== q) {
					q = $scope.query;
					$scope.loading = true;
					$scope.getColumns($scope.query)
						.then(function ( cols ) {
							$scope.columnResults = cols;
						})
						['finally'](function ( ) {
							$scope.loading = false;
						});
				} else {
					$scope.loading = false;
				}
			}
			
			$scope.toggleColumn = function ( column ) {
				var col = _.find($scope.columns, { 'id': column.id });
				
				if(!col) {
					$scope.$emit('crud.column.add', column);
				} else {
					$scope.$emit('crud.column.remove', col);
				}
				
			};
			
			$scope.removeColumn = function ( column ) {
				var col = _.find($scope.columns, { 'id': column.id });
				
				if(col) {
					$scope.$emit('crud.column.remove', col);
				}
			};
			
			$scope.isSelected = function ( column ) {
				return _.find($scope.columns, { 'id': column.id });
			};
			
			$scope.$on('zs-popup-menu.open', function ( ) {
				safeApply($scope, function ( ) {
					updateColumns();
				});
			});
			
			$scope.$watch('query', function ( ) {
				$scope.loading = true;
			});
			
			$scope.$on('form.change.committed', function ( ) {
				safeApply($scope, function ( ) {
					updateColumns();
				});
			});
			
			$scope.$on('zs.sort.update', function ( event, data, before ) {
				$scope.$emit('crud.column.sort.update', data, before);
			});
			
			$scope.$on('zs.sort.commit', function ( /*event, data, before*/ ) {
				$scope.$emit('crud.column.sort.commit');
			});
			
		}]);
	
})();
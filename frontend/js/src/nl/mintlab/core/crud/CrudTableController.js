/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudTableController', [ '$scope', '$parse', '$sce', function ( $scope, $parse, $sce ) {
			
			$scope.sortOn = function ( column ) {
				var reversed;
				if(column.sort === false) {
					return;
				}
				
				if($scope.isSortedOn(column)) {
					reversed = !$scope.isReversed();
				} else {
					reversed = false;
				}
				$scope.sort(column.id, reversed);
			};
			
			$scope.isSortedOn = function ( column ) {
				return $scope.getSortBy() === column.id;
			};
			
			$scope.isReversed = function ( ) {
				return $scope.isSortReversed();
			};
			
			$scope.reverse = function ( ) {
				$scope.sort($scope.getSortBy(), !$scope.isReversed());
			};
			
			$scope.getColumnValue = function ( column, item ) {
				var resolve = column.resolve + (column.filter ? '|' + column.filter : ''),
					getter = $parse(resolve),
					val = getter(item);
				
				if(val === undefined || val === null) {
					val = '';
				}
				if(_.isArray(val)) {
					val = val.join(', ');
				}
				return val;
			};
			
			$scope.getColumnTemplate = function ( column, item ) {
				var tpl = column.template ? column.template : $scope.getColumnValue(column, item);
				return tpl;
			};
			
			$scope.getColumnHtml = function ( column ) {
				return $sce.trustAsHtml(column.template);
			};
			
			$scope.getTemplateType = function ( column ) {
				var templateType = 'resolve';
				if(column.template) {
					templateType = 'template';
				} else if(column.templateUrl) {
					templateType = 'url';
				}
				return templateType;
			};
			
			$scope.getCssColumnId = function ( column ) {
				return column.id.replace(/\./g, '_');
			};
			
		}]);
	
})();
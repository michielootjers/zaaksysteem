/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.zql')
		.service('zqlService', [ 'zqlEscapeFilter', function ( zqlEscapeFilter ) {
			
			var zqlService = {};
			
			function isEmpty ( val ) {
				return val === '' || val === null || val === undefined || (_.isArray(val) && val.length === 0);
			}
			
			function getDateRangePart ( prop ) {
				var part = '';
				part += prop.name + ' BETWEEN ';
				part += new Date(prop.value.from).toISOString() + ' AND ' + new Date(prop.value.to).toISOString();
				return part;
			}
			
			function getAttrPart ( prop ) {
				var part = '',
					parts = [];
					
				parts = _.map(prop.value.attributes, function ( attr ) {
					var name = attr.object.column_name,
						val = attr._value,
						operator = attr._operator || '=',
						part = '';
						
					if(!isEmpty(val)) {
						if(attr.object.value_type === 'date') {
							part = getPart(name, new Date(val.from).toISOString(), operator, false);
						} else {
							part = getPart(name, val, operator);
						}
					}
					
					return part;
				});
				
				parts = _.filter(parts, function ( pt ) {
					return !!pt;
				});
				
				if(parts.length) {
					if(prop.value.grouping === 'and') {
						part = parts.join(' AND ');
					} else {
						part = parts.join(' OR ');
					}
				}
				
				return part;
			}
			
			function getPart ( name, value, operator, escape ) {
				var part = '',
					isNot = false;
				
				if(escape === undefined) {
					escape = true;
				}
				
				if(operator === undefined) {
					operator = '=';
				}
				
				if(operator === '!=') {
					isNot = true;
					operator = '=';
				}
				
				if(_.isArray(value) && value.length > 1) {
					part += name + ' IN (';
					part += _.map(value, function ( val ) {
						return escape ? zqlEscapeFilter(val) : val;
					}).join(',');
					part += ')';
				} else {
					if(_.isArray(value)) {
						value = value[0];
					}
					part += name + ' ' + operator + ' ' + (escape ? zqlEscapeFilter(value) : value);
				}
				
				part = (isNot ? 'NOT' : '') + '(' + part + ')';
				
				return part;
			}
			
			zqlService.getConditions = function ( properties ) {
				var conditions;
				
				conditions = _.map(properties, function ( prop ) {
					var part = '';
					
					switch(prop.type) {
						case 'date-range':
						part = getDateRangePart(prop);
						break;
						
						case 'attributes':
						part = getAttrPart(prop);
						break;
						
						case 'property-group':
						part = '(' + _.map(prop.value, function ( val ) {
							return getPart(val.name, val.value);
						}).join(' ' + prop.relation + ' ') + ')';
						break;
						
						default:
						part = getPart(prop.name, prop.value);
						break;
					}
					
					return part;
				});
				
				conditions = _.filter(conditions, function ( cond ) {
					return !!cond;
				});
				
				return conditions;
			};
			
			return zqlService;
			
		}]);
	
})();
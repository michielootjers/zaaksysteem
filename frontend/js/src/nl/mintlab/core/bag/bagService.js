/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.bag')
		.factory('bagService', [ function ( ) {
			
			var bagService = {},
				streetLabel = 'streetname + (number && (\' \' + number) || \'\' ) + \', \' + city',
				bagLabel = 'street + (number && (\' \' + number) || \'\' ) + \', \' + city';
							
			bagService.getStreetLabel = function ( ) {
				return streetLabel;
			};
			
			bagService.getLabel = function ( ) {
				return bagLabel;	
			};
			
			return bagService;
			
		}]);
	
})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.FormFieldsetController', [ '$scope', function ( $scope ) {
			
			$scope.toggleCollapse = function ( ) {
				$scope.fieldset.collapsed = !$scope.fieldset.collapsed;
			};
			
			$scope.isChanged = function ( ) {
				var isChanged = !_.every($scope.fieldset.fields, function ( field ) {
					return $scope.isDefaultValue(field);
				});
				
				return isChanged;
			};
			
			$scope.revert = function ( event ) {
				_.each($scope.fieldset.fields, function ( field ) {
					$scope.revertField(field);
					if(event) {
						$scope.$emit('form.change.committed', field);
					}
				});
				if(event) {
					event.stopPropagation();
				}
			};
			
		}]);
	
})();
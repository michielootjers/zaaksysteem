/*global angular,$,loadWebform,console*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsReloadLegacyForm', [ function ( ) {
			
			return {
				scope: false,
				link: function ( scope, element, attrs ) {
					
					var deregister;
					
					function onEvent ( ) {
						scope.reloadLegacyForm();
					}
					
					scope.reloadLegacyForm = function ( ) {
						var form = element[0].tagName.toLowerCase() !== 'form' ? $(element[0]).closest('form') : $(element[0]),
							params = null;
							
						if(attrs.zsReloadLegacyFormParams) {
							try {
								params = scope.$eval(attrs.zsReloadLegacyFormParams);
							} catch ( error ) {
								console.log('Error parsing parameters: ' + attrs.zsReloadLegacyForm);
								params = {};
							}
						} else {
							params = {};
						}
						
						params.form = form;
							
						loadWebform(params);
					};
					
					attrs.$observe('zsReloadLegacyFormOn', function ( ) {
						if(deregister) {
							deregister();
							deregister = null;
						}
						if(attrs.zsReloadLegacyFormOn) {
							deregister = scope.$on(attrs.zsReloadLegacyFormOn, onEvent);
						}
					});
					
					
					
				}
			};
			
		}]);
	
})();
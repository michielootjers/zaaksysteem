/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.service('formService', [ 'bagService', function ( bagService ) {
			
			var formsByName = {},
				attributeMap = {},
				bagStreetLabel = bagService.getStreetLabel(),
				bagLabel = bagService.getLabel();
				
				
			attributeMap = {
				"url": {
					"type": "url"
				},
				"text": {
					"type": "text"
				},
				"richtext": {
					"type": "richtext"
				},
				"image_from_url": {
					"type": "url"
				},
				"text_uc": {
					"type": "text",
					"data": {
						"transform": "uppercase"
					}
				},
				"numeric": {
					"type": "number"
				},
				"valuta": {
					"type": "price"
				},
				"valutain": {
					"type": "price"
				},
				"valutaex": {
					"type": "price"
				},
				"valutain6": {
					"type": "price"
				},
				"valutain21": {
					"type": "price"
				},
				"valutaex21": {
					"type": "price"
				},
				"date": {
					"type": "date"
				},
				"googlemaps": {
					// "type": "map"
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag",
						"resolve": "id",
						"multi": false,
						"label": bagLabel
					}
				},
				"textarea": {
					"type": "textarea"
				},
				"option": {
					"type": "radio"
				},
				"select": {
					"type": "select"
				},
				"checkbox": {
					"type": "checkbox-list"
				},
				"file": {
					"type": "file"
				},
				"calendar": {
					"type": "appointment"
				},
				"bag_straat_adres": {
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag",
						"resolve": "id",
						"multi": false,
						"label": bagLabel
					}
				},
				"bag_straat_adressen": {
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag",
						"resolve": "id",
						"multi": true,
						"label": bagLabel
					}
				},
				"bag_adres": {
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag",
						"resolve": "id",
						"multi": false,
						"label": bagLabel
					}
				},
				"bag_adressen": {
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag",
						"resolve": "id",
						"multi": false,
						"label": bagLabel
					}
				},
				"bag_openbareruimte": {
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag-street",
						"resolve": "id",
						"multi": false,
						"label": bagStreetLabel
					}
				},
				"bag_openbareruimtes": {
					"type": "spot-enlighter",
					"data": {
						"restrict": "bag-street",
						"resolve": "id",
						"multi": true,
						"label": bagStreetLabel
					}
				}
			};
			
			return {
				register: function ( form ) {
					formsByName[form.getName()] = form;
				},
				unregister: function ( form ) {
					delete formsByName[form.getName()];	
				},
				get: function ( name ) {
					return formsByName[name];
				},
				getFormFieldForAttributeType: function ( attrType ) {
					var type = attributeMap[attrType] || attributeMap.text;
					return _.cloneDeep(type);
				}
			};
			
		}]);

})();
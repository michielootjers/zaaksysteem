/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.ObjectSearchController', [ '$scope', function ( $scope ) {
			
			var indexOf = _.indexOf,
				objectUnwatch;
			
			$scope.limit = Number.MAX_VALUE;
			
			$scope.objects = [];
						
			$scope.addObject = function ( obj ) {
				var index = indexOf($scope.objects, obj);
				if(index === -1) {
					
					while($scope.objects.length >= $scope.limit) {
						$scope.removeObject($scope.objects[0]);
					}
					
					$scope.objects.push(obj);
				}
			};
			
			$scope.removeObject = function ( obj ) {
				var index = indexOf($scope.objects, obj);
				if(index !== -1) {
					$scope.objects.splice(index, 1);
				}
			};
			
			objectUnwatch = $scope.$watch('objects', function ( nw/*, old*/ ) {
				if(nw && nw.length && $scope.limit === 1) {
					$scope.newObject = nw[0];
				}
			});
			 
			$scope.$watch('newObject', function ( nw, old ) {
				if(nw) {
					$scope.addObject(nw);
					if($scope.limit > 1) {
						$scope.newObject = null;
					}
				}
				if(!nw && old && $scope.limit === 1) {
					$scope.removeObject(old);
				}
			});
			
			$scope.$watch('limit', function ( nw/*, old*/ ) {
				if(!nw) {
					throw new Error('Limit has to be larger than 0');
				}
			});
			
		}]);
	
})();
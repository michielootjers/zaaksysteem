/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsPriceFormField', [ function ( ) {
			
			return {
				require: 'ngModel',
				priority: 100,
				link: function ( scope, element, attrs, ngModel ) {
					
					ngModel.$formatters.push(function ( val ) {
						return parseFloat(val);
					});
					
				}
			};
			
		}]);
	
})();
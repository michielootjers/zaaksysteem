/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsValidation', [ '$parse', function ( $parse ) {
			
			return {
				scope: false,
				require: 'ngModel',
				link: function ( scope, element, attrs, ngModel ) {
					
					function validate ( val ) {
						var isValid = $parse(attrs.zsValidation)(scope, {
							$value: val
						});
						
						if(isValid === undefined) {
							isValid = true;
						} else {
							isValid = !!isValid;
						}
						
						ngModel.$setValidity('zs-valid', isValid);
						
						return isValid ? val : undefined;
					}
					
					if(attrs.zsValidation) {
						scope.$watch(function ( ) {
							validate(ngModel.$viewValue);
						});
					}
					
				}
			};
			
		}]);
	
})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormField', [ '$timeout', function ( $timeout ) {
			
			var TIMEOUT = 400;
			
			return {
				priority: 100,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0],
							canceler;
						
						function commitChange ( ) {
							scope.$emit('form.change.committed', scope.field);
						}
						
						function cancelTimer ( ) {
							if(canceler) {
								$timeout.cancel(canceler);
							}
							canceler = null;
						}
						
						function startTimer ( ) {
							canceler = $timeout(onTimer, TIMEOUT, false);
						}
						
						function onTimer ( ) {
							canceler = null;
							commitChange();
						}
						
						function onKeyUp ( event ) {
							scope.$apply(function ( ) {
								cancelTimer();
								if(event.keyCode === 13 || event.keyCode === 27) {
									commitChange();
								} else {
									startTimer();
								}
							});
						}
						
						function onFocusOut ( ) {
							scope.$apply(commitChange);
						}
						
						if(_.indexOf(['text', 'number', 'email', 'url' ], element.attr('type')) !== -1)  {
							if(attrs.zsSpotEnlighter === undefined) {
								element.bind('focusout', onFocusOut);
							} else {
								ngModel.$viewChangeListeners.push(commitChange);
							}
							element.bind('keyup', onKeyUp);
						} else {
							ngModel.$viewChangeListeners.push(commitChange);
						}
						
						scope.$on('$destroy', function ( ) {
							cancelTimer();
							element.unbind('focusout', onFocusOut);
							element.unbind('keyup', onKeyUp);
						});
						
						// TODO: implement configurable autosave
						
					};
					
				}
			};
			
		}]);
	
})();
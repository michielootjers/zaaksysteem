/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/form/form.html';
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormTemplateParser', [ '$q', '$timeout', 'smartHttp', 'templateCompiler', '$compile', '$document', function ( $q, $timeout, smartHttp, templateCompiler, $compile, $document ) {			
			return {
				scope: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var elSource,
							formName;
						
						function clearScope ( ) {
							
							var el = element[0],
								childNodes = el.childNodes,
								scopeChild = scope.$$childHead;
							
							while(childNodes.length) {
								el.removeChild(childNodes[0]);
							}
							
							while(scopeChild) {
								scopeChild.$destroy();
								scopeChild = scopeChild.$$nextSibling;
							}
							
							formName = '';
							
							scope.zsForm = {
								name: formName,
								options: {},
								fieldsets: [],
								fields: [],
								actions: [],
								promises: []
							};
							
						}
						
						function recompile ( ) {
							
							var clone,
								elClone,
								i,
								l,
								attrs,
								j,
								m;
							
							if(elSource) {
								
								clone = $document[0].createElement('div');
								for(i = 0, l = elSource.length; i < l; ++i) {
									elClone = elSource[i].cloneNode(true);
									if(elClone.nodeName.toLowerCase() === 'script') {
										elClone = angular.element('<script>' + elSource[i].innerHTML+ '</script>')[0];
										attrs = elSource[i].attributes;
										for (j = 0, m = attrs.length; j < m; ++j) {
											if(attrs[j].specified) {
												elClone.setAttribute(attrs[j].name, attrs[j].value);
											}
										}
									}
									clone.appendChild(elClone);
								}
								
								$compile(clone.childNodes)(scope, function ( clonedElement ) {
									for(i = 0, l = clonedElement.length; i < l; ++i) {
										element[0].appendChild(clonedElement[i]);
									}
								}); 
								
							}
						}
						
						function loadConfig ( url ) {
							
							if(!url) {
								clearScope();
								return;
							}
							
							smartHttp.connect({
								url: url,
								method: 'GET'
							})
								.success(function onSuccess ( data ) {
									clearScope();
									setConfig(data.result[0]);
									recompile();
								})
								.error(function onError ( ) {
									clearScope();
								});
						}
						
						function loadTemplate ( url ) {
							elSource = null;
							if(url) {
								templateCompiler.getElement(url).then(function ( element ) {
									elSource = element;
									recompile();
								});
							}
						}
						
						function setConfig ( config ) {
							var data = config.data,
								fields = [],
								i,
								l;
							
							for(i = 0, l = config.fieldsets ? config.fieldsets.length : 0; i < l; ++i) {
								if(config.fieldsets[i].fields) {
									fields = fields.concat(config.fieldsets[i].fields);
								}
							}
							
							formName = config.name;
							
							scope.zsForm.name = config.name;
							scope.zsForm.fieldsets = config.fieldsets || [];
							scope.zsForm.fields = fields || [];
							scope.zsForm.actions = config.actions || [];
							scope.zsForm.promises = config.promises || [];
							scope.zsForm.options = config.options || {};
														
							for(var key in data) {
								scope[key] = data[key];
							}
						}
						
						scope.getFormName = function ( ) {
							return formName;
						};
						
						attrs.$observe('zsFormTemplateParser', function ( ) {
							var obj;
							try {
								obj = JSON.parse(attrs.zsFormTemplateParser);
								clearScope();
								setConfig(obj);
								recompile();
							} catch ( e ) {
								loadConfig(attrs.zsFormTemplateParser);
							}
						});
						
						attrs.$observe('zsFormTemplateUrl', function ( ) {
							loadTemplate(attrs.zsFormTemplateUrl || TEMPLATE_URL);
						});
						
						clearScope();

					};
				}
				
			};
			
		}]);
	
})();
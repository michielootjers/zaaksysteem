/*global angular*/
(function ( ) {
	angular.module('Zaaksysteem.form')
		.directive('zsDateRangeFormField', [ '$document', 'dateFilter', 'translationService', function ( $document, dateFilter, translationService ) {
			
			return {
				scope: true,
				require: 'ngModel',
				link: function ( scope, element, attrs, ngModel ) {
					
					var toPick = null;
					
					scope.dateRangeType = null;
					scope.from = NaN;
					scope.to = NaN;
					
					function set ( from, to ) {
						var obj = {
								from: from,
								to: to
							};
						
						scope.from = from;
						scope.to = to;
						if(isNaN(from) || isNaN(to)) {
							obj = null;
						}
						ngModel.$setViewValue(obj);
					}
					
					function setLastWeek ( ) {
						var now = new Date(),
							weekday = now.getDay() - 1,
							from = new Date(now.getFullYear(), now.getMonth(), now.getDate() - weekday - 7),
							to = new Date(from.getFullYear(), from.getMonth(), from.getDate() + 7);
							
						set(from.getTime(), to.getTime()-1);
						
					}
					
					function setThisWeek ( ) {
						var now = new Date(),
							weekday = now.getDay() - 1,
							from = new Date(now.getFullYear(), now.getMonth(), now.getDate() - weekday),
							to = new Date(from.getFullYear(), from.getMonth(), from.getDate() + 7);
							
						set(from.getTime(), to.getTime()-1);
					}
					
					function setLastMonth ( ) {
						var now = new Date(),
							from = new Date(now.getFullYear(), now.getMonth()-1, 1),
							to = new Date(now.getFullYear(), now.getMonth(), 1);
							
						set(from.getTime(), to.getTime()-1);
					}
					
					function setThisMonth ( ) {
						var now = new Date(),
							from = new Date(now.getFullYear(), now.getMonth(), 1),
							to = new Date(now.getFullYear(), now.getMonth() + 1, 1);
							
						set(from.getTime(), to.getTime()-1);
					}
					
					scope.confirmRange = function ( ) {
						if(toPick) {
							set(toPick.from, toPick.to);
						}
					};
					
					scope.isValidRange = function ( ) {
						var from,
							to;
							
						if(toPick) {
							from = toPick.from;
							to = toPick.to;
						} else {
							from = scope.from;
							to = scope.to;	
						}
						
						from = new Date(from).getTime();
						to = new Date(to).getTime();
							
						return !(isNaN(from) || isNaN(to));
					};
					
					scope.getFrom = function ( ) {
						return scope.from;
					};
					
					scope.getTo = function ( ) {
						return scope.to;	
					};
					
					scope.getMin = function ( ) {
						return scope.min;	
					};
					
					scope.getMax = function ( ) {
						return scope.max;	
					};
					
					scope.getDateRangeLabel = function ( ) {
						var label,
							from = scope.from,
							to = scope.to,
							dateFrom,
							dateTo;
							
						if(isNaN(new Date(from).getTime()) || isNaN(new Date(to).getTime())) {
							label = translationService.get('Alles');
						} else {
							dateFrom = new Date(scope.from);
							dateTo = new Date(scope.to);
							
							if(dateFrom.getFullYear() === dateTo.getFullYear() && dateFrom.getMonth() === dateTo.getMonth() && dateFrom.getDate() === dateTo.getDate()) {
								label = dateFilter(dateFrom.getTime(), 'mediumDate');
							} else {
								label = dateFilter(dateFrom.getTime(), 'mediumDate') + ' - ' + dateFilter(dateTo.getTime(), 'mediumDate');
							}
							
						}
						
						return label;
					};
					
					scope.onDateRangeClick = function ( dateRangeType ) {
						scope.dateRangeType = dateRangeType;
						if(scope.dateRangeType === 'custom_range') {
							scope.openPopup();
						}
					};
					
					scope.$watch('dateRangeType', function ( ) {
						switch(scope.dateRangeType) {
							
							default:
							set(NaN);
							break;
							
							case 'last_week':
							setLastWeek();
							break;
							
							case 'this_week':
							setThisWeek();
							break;
							
							case 'last_month':
							setLastMonth();
							break;
							
							case 'this_month':
							setThisMonth();
							break;
							
							case 'custom_range':
							break;
							
						}
					});
					
					scope.$on('date.range.select', function ( event, from, to ) {
						toPick = {
							from: from,
							to: to
						};
					});
					
					scope.$on('popupclose', function ( ) {
						toPick = null;
					});
					
					ngModel.$formatters.push(function ( val ) {
						if(angular.isObject(val)) {
							set(val.from, val.to);
						} else {
							set(NaN, NaN);
						}
					});
					
				}
			};
			
		}]);
})();
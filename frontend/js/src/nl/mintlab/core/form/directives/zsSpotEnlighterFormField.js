/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsSpotEnlighterFormField', [ '$parse', function ( $parse ) {
			
			return {
				require: 'ngModel',
				scope: true,
				link: function ( scope, element, attrs, ngModel ) {
					
					function isMultiple ( ) {
						var multiple = attrs.zsSpotEnlighterFormFieldMultiple;
						if(multiple === 'false' || multiple === '0') {
							multiple = false;
						}
						return multiple;
					}
					
					function getObjId ( obj ) {
						var resolve = attrs.zsSpotEnlighterResolve || 'id';
						return $parse(resolve)(obj);
					}
					
					function getObjById ( objId ) {
						return _.find(ngModel.$viewValue, function ( obj ) {
							var id = getObjId(obj);
							return objId === id;
						});
					}
					
					scope.add = function ( obj ) {
						var val = ngModel.$viewValue ? ngModel.$viewValue.concat() : [];
						
						if(!scope.has(obj)) {
							val.push(obj);
							ngModel.$setViewValue(val);
						}
					};
					
					scope.remove = function ( obj ) {
						var val = ngModel.$viewValue ? ngModel.$viewValue.concat() : [],
							index;
							
						obj = getObjById(getObjId(obj));
						
						if(scope.has(obj)) {
							index = _.indexOf(val, obj);
							if(index !== -1) {
								val.splice(index, 1);
								ngModel.$setViewValue(val);
							}
						}
						
					};
					
					scope.has = function ( obj ) {
						return getObjById(getObjId(obj));
					};
					
					scope.getObjLabel = function ( obj ) {
						var label = $parse(attrs.zsSpotEnlighterLabel)(obj);
						return label;
					};
					
					scope.$watch('obj', function ( ) {
						if(isMultiple() && scope.obj) {
							scope.add(scope.obj);
							scope.obj = null;
						} else if(!isMultiple()) {
							ngModel.$setViewValue(scope.obj);
						}
					});
					
					ngModel.$formatters.push(function ( val ) {
						if(isMultiple()) {
							ngModel.$setViewValue([]);						
							_.each(val, function ( obj ) {
								scope.add(obj);
							});
						} else {
							scope.obj = val;
						}
						return val;
					});
					
					attrs.$observe('zsSpotEnlighterFormFieldMultiple', function ( ) {
						
					});
					
				}
			};
			
		}]);
	
})();
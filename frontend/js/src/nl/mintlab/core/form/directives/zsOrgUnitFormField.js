/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsOrgUnitFormField', [ function ( )  { 
			
			return {
				require: 'ngModel',
				link: function ( scope, element, attrs, ngModel ) {
					
					function getModelArray ( ) {
						return ngModel.$modelValue ? ngModel.$modelValue.concat() : [];
					}
					
					scope.addOrgUnit = function ( ) {
						var array = getModelArray();
						
						array.push({
							orgUnit: {
								org_unit_id: null,
								role_id: null
							}
						});
						
						ngModel.$setViewValue(array);
					};
					
					scope.removeOrgUnit = function ( item ) {
						var array = getModelArray();
							
						_.pull(array, item);
						
						ngModel.$setViewValue(array);
					};
					
					scope.$watch('scope[field.name]', function ( ) {
						ngModel.$setViewValue(getModelArray());
					}, true);
				}
			};
			
		}]);
	
})();
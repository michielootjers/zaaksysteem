/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core', [ 'Zaaksysteem.core.detail', 'Zaaksysteem.core.zql', 'Zaaksysteem.core.bag' ]);
	
})();
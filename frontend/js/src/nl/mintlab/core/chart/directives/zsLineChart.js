/*global angular,$,console*/
(function () {
    "use strict";

    angular.module('Zaaksysteem')
        .directive('zsLineChart', [ 'smartHttp', '$timeout', '$q', function (smartHttp, $timeout, $q) {

            return {
                scope: {
                    zql: '=zsLineChartZql',
                    profile: '=zsLineChartProfile',
                    message: '=zsLineChartMessage',
                    loading: '=zsLineChartLoading'
                },
                link: function (scope, element/*, attrs*/) {

                    var container = $(element).find('#search_query_chart_container'),
                        timeoutCancel;

                    function render(chartData) {
                        if (!chartData.chart) {
                            chartData.chart = {};
                        }

                        chartData.chart.renderTo = 'search_query_chart_container';

                        //see how wide the chart is, so the exported graph will look the same
                        chartData.exporting = { width : container.width() };

                        if (scope.zsLineChartProfile === 'afhandeltermijn') {
                            chartData.tooltip = {
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b>: ' + this.y + ' %';
                                }
                            };
                        }

                        if (chartData.series) {
                            container.highcharts(chartData);
                            scope.message = '';
                        } else {
                            // necessary to work around highcharts behaviour
                            container.html('');
                            scope.message = 'Geen resultaten';
                        }
                    }

                    function reloadData() {
                        if (timeoutCancel) {
                            $timeout.cancel(timeoutCancel);
                            timeoutCancel = null;
                        }
                        scope.loading = true;
                        timeoutCancel = $q.defer();

                        smartHttp.connect({
                            url: '/api/object_chart',
                            method: 'GET',
                            params: {
                                zql: scope.zql,
                                chart_profile: scope.profile.value
                            },
                            timeout: timeoutCancel
                        })
                            .success(function (response) {
                                render(response.json);
                            }).
                            error(function (response) {
                                scope.message = response.result[0].messages.join(', ');
                            })
                            ['finally'](function ( ) {
                                timeoutCancel = null;
                                scope.loading = false;
                            });

                    }

                    scope.$watch('zql', function () {
                        reloadData();
                    });
                    scope.$watch('profile', function () {
                        reloadData();
                    });
                }
            };

        }]);
}());
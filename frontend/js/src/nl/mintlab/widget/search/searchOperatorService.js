/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.service('searchOperatorService', [ 'translationService', 'formService', function ( translationService, formService ) {
			
			var operators = {
					"numeric": [
						{
							"value": "=",
							"label": "is gelijk aan"
						},
						{
							"value": "!=",
							"label": "is niet gelijk aan"
						},
						{
							"value": ">",
							"label": "is groter dan"
						},
						{
							"value": "<",
							"label": "is kleiner dan"
						},
						{
							"value": ">=",
							"label": "is tenminste"
						},
						{
							"value": "<=",
							"label": "is ten hoogste"
						}
					],
					"date": [
						{
							"value": "=",
							"label": "is gelijk aan"
						},
						{
							"value": "!=",
							"label": "is niet gelijk aan"
						},
						{
							"value": ">",
							"label": "later dan"
						},
						{
							"value": "<",
							"label": "eerder dan"
						},
						{
							"value": ">=",
							"label": "is tenminste"
						},
						{
							"value": "<=",
							"label": "is ten hoogste"
						}
					],
					"text": [
						{
							"value": "=",
							"label": "is gelijk aan"
						},
						{
							"value": "!=",
							"label": "is niet gelijk aan"
						},
						{
							"value": "like",
							"label": "bevat"
						},
						{
							"value": "not_like",
							"label": "bevat niet"
						}
					],
					"checkbox": [
						{
							"value": "",
							"label": "Minstens &eacute;&eacute;n"
						},
						{
							"value": "CHECKBOX_AND",
							"label": "Alle"
						}
					]
				},
				map = {
					"text": operators.text,
					"number": operators.numeric,
					"date": operators.date,
					"textarea": operators.text,
					"price": operators.numeric
				};
				
			return {
				getOperatorsForAttributeType: function ( attrType ) {
					var formType = formService.getFormFieldForAttributeType(attrType),
						operators = map[formType.type];
						
					return operators;
				}
			};
		}]);
	
})();
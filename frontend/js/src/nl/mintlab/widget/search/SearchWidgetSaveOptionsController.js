/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetSaveOptionsController', [ '$scope', '$http', 'formService', function ( $scope, $http, formService ) {
			
			function normalize ( value ) {
				if(value === null || value === undefined || value === '' || (_.isArray(value) && value.length === 0)) {
					value = undefined;
				}
				return value;
			}
			
			$scope.isEditable = function ( ) {
				var search = $scope.activeSearch,
					isEditable = false;
				if(search) {
					isEditable = search.editable;
				}
				return isEditable;
			};
			
			$scope.isChanged = function ( ) {
				var form = formService.get('searchWidgetForm'),
					search = $scope.activeSearch,
					values,
					fields,
					isChanged;
					
				if(!form || !search) {
					return false;
				}
				
				values = form.getValues(false);
				fields = form.getFields();
				
				isChanged = _.some(fields, function ( field ) {
					
					if(field.ignore) {
						return false;
					}
					
					var fieldIsChanged,
						fieldVal = normalize(values[field.name]),
						searchVal = normalize(search.values[field.name]);
						
					fieldIsChanged = !_.isEqual(fieldVal, searchVal) && !(searchVal === undefined && form.isDefaultValue(field));
					
					return fieldIsChanged;
				});
				
				return isChanged;
			};
			
			$scope.saveChanges = function ( ) {
				var search = $scope.activeSearch,
					form = formService.get('searchWidgetForm');
					
				search.values = form.getValues(false);
				
				$scope.search.saveFilter($scope.activeSearch);
				
			};
			
		}]);
	
})();
/*global angular,_,EventEmitter*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.service('searchService', [ '$q', '$http', function ( $q, $http ) {
						
			function createSearch ( params ) {
				var search = new EventEmitter(),
					filters = null,
					filterPromise;
					
				if(!params) {
					params = {};
				}
				
				search.addFilter = function ( filter ) {
					filters.push(filter);
				};
				
				search.saveFilter = function ( filter ) {
					var deferred = $q.defer(),
						url,
						data;
					
					if(filter.id === undefined) {
						url = '/api/object/savedsearch/create';
						data = {
							title: filter.label,
							query: JSON.stringify(filter)
						};
					} else {
						url =  '/api/object/savedsearch/update';
						data = {
							saved_search_id: filter.id,
							title: filter.label,
							query: JSON.stringify(filter)
						};
					}
						
					$http({
						method: 'POST',
						url: url,
						data: data
					})
						.success(function ( response ) {
							var searchData = response.result[0];
							
							filter.label = searchData.values.title;
							filter.id = searchData.id;
							
							deferred.resolve(filter);
						})
						.error(function ( response ) {
							deferred.reject(response.result[0]);
						});
						
					return deferred.promise;
				};
				
				search.removeFilter = function ( filter ) {
					var deferred = $q.defer(),
						index = _.indexOf(filters, filter);
					
					filters.splice(index, 1);
					
					if(filter.id) {
						$http({
							method: 'POST', 
							url: '/api/object/savedsearch/delete',
							data: {
								saved_search_id: filter.id
							}
						})	
							.success(function ( ) {
								deferred.resolve(filter);
							})
							.error(function ( response ) {
								deferred.reject(response.result[0]);
								filters.splice(index, 0, 1);
							});
					}
						
					return deferred.promise;
				};
				
				search.getSavedFilters = function ( ) {
					var deferred;
						
					if(!filterPromise) {
						deferred = $q.defer();
						
						filterPromise = deferred.promise;
						
						$http({
							method: 'GET', 
							url: '/api/object',
							params: {
								'zql': 'SELECT {} from saved_search WHERE owner_subject_id = ' + params.ownerId
							}
						})
							.success(function ( response ) {
								filters = _.map(response.result, function ( filter ) {
									return {
										id: filter.id,
										label: filter.values.query.label,
										values: filter.values.query.values,
										columns: filter.values.query.columns,
										editable: filter.values.query.editable
									};
								});
								deferred.resolve(filters);
							})
							.error(function ( response) {
								deferred.reject(response.result[0]);
							});
					}
					
					return filterPromise;
				};
				
				return search;
			}
			
			return {
				create: function ( params ) {
					return createSearch( params);
				}
			};
			
		}]);
	
})();
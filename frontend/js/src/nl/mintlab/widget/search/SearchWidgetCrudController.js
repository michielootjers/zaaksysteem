/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetCrudController', [ '$scope', 'zqlEscapeFilter', function ( $scope, zqlEscapeFilter ) {
			
			$scope.getSelectionZql = function ( ) {
				var selection = $scope.zsCrud.getSelection(),
					selectionType = $scope.zsCrud.getSelectionType(),
					zqlWhere = $scope.getZqlWhere(),
					zqlMatching = $scope.getZqlMatching(),
					resolve = $scope.zsCrud.getResolve(),
					where = '',
					matching = '',
					ids;
					
				if(selectionType === 'subset') {
					where = resolve.replace(/values\['?([A-Za-z\._]+)'?\]/, '$1') + ' IN ';
					ids = _.map(selection, function ( item ) {
						return zqlEscapeFilter($scope.zsCrud.resolveItemId(item));
					});
					where += '(' + ids.join(',') + ')';
				} else {
					where = zqlWhere;
					matching = zqlMatching;
				}
				
				return '/api/object/?zapi_no_pager=1&zql=SELECT ' + $scope.getZqlColumns() + ' FROM ' + $scope.objectType + (where ? (' WHERE ' + where) : '') + (matching ? 'MATCHING ' + matching : '');
			};
			
			$scope.getEzraDialogSelectionParameters = function ( /*action*/ ) {
				var selection = $scope.zsCrud.getSelection(),
					selectionIds;
					
				selectionIds = _.map(selection, function ( item ) {
					return $scope.zsCrud.resolveItemId(item);
				});
				
				return {
					'selected_case_ids': selectionIds.join(','),
					// TODO: move this somewhere else
					'selection': 'selected_cases',
					'zql': $scope.getZql()
				};
			};
			
			$scope.$on('zs.search.bulk.success', function ( ) {
				$scope.zsCrud.reloadData();
			});
			
		}]);
	
})();
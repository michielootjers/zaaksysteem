/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetController', [ '$scope', '$http', '$q', 'searchService', 'formService', 'translationService', 'zqlService', 'zqlEscapeFilter', 'bagService', function ( $scope, $http, $q, searchService, formService, translationService, zqlService, zqlEscapeFilter, bagService ) {
			
			var	groupAll = {
					"label": "Alles",
					"value": null,
					"count": NaN
				},
				crudUrl = null,
				groupingUrl = null,
				sortBy = null,
				sortOrder = null,
				httpCancel,
				resultOptions = [];
				
			$scope.filtersLocked = true;
			$scope.groupings = [];
			$scope.groups = [
				groupAll
			];
			
			$scope.openGroup = null;
			$scope.loading = false;
			$scope.showLocation = false;

			$scope.chartOptions = [
				{ value: 'status', label: 'Geregistreerd/afgehandeld' },
				{ value: 'handling_time', label: 'Binnen afhandeltermijn/Buiten afhandeltermijn' },
				{ value: 'average_handling_time_per_month', label: 'Gemiddelde behandeltijd per maand' },
				{ value: 'average_handling_time_per_owner', label: 'Gemiddelde behandeltijd per behandelaar' },
				{ value: 'cases_per_department', label: 'Zaken per afdeling' },
				{ value: 'cases_per_department_per_month', label: 'Zaken per afdeling per maand' },
				{ value: 'cases_per_owner_per_month', label: 'Zaken per behandelaar per maand' },
				{ value: 'cases_within_and_outside_term_per_month', label: 'Zaken binnen/buiten termijn per maand' },
				{ value: 'cases_within_and_outside_term_per_month_per_department', label: 'Zaken binnen/buiten termijn per maand per afdeling' },
			];

			$scope.chartProfile = $scope.chartOptions[0];
			
			function setSearchForm ( ) {
				var form = formService.get('searchWidgetForm'),
					key,
					values,
					scope,
					fieldsets,
					fields,
					field,
					fieldset,
					hasChangedValue,
					i,
					l,
					j,
					m;
					
				if(!form || !$scope.activeSearch) {
					return;
				}
				
				scope = form.scope;
				fieldsets = scope.zsForm.fieldsets;
				
				values = $scope.activeSearch.values;
				
				scope.resetForm();
				
				for(key in values) {
					form.setValue(key, values[key]);
				}
				
				for(i = 0, l = fieldsets.length; i < l; ++i) {
					fieldset = fieldsets[i];
					fields = fieldset.fields;
					hasChangedValue = false;
					for(j = 0, m = fields.length; j < m; ++j) {
						field = fields[j];
						if(!scope.isDefaultValue(field)) {
							hasChangedValue = true;
							break;
						}
					}
					fieldset.collapsed = !(_.indexOf([ 'fulltext' ], fieldset.name) !== -1 || hasChangedValue);
				}				
				
				form.scope.submitForm();
			}
			
			function escapeForZql ( val ) {
				return zqlEscapeFilter(val);
			}
			
			function reloadGroups ( ) {
				var grouping = $scope.getZqlGrouping(),
					where = $scope.getZqlWhere(),
					matching = $scope.getZqlMatching(),
					url;
					
				if(grouping) {
					url = '/api/object/?zql=SELECT DISTINCT ' + grouping + ' FROM ' + $scope.objectType + (matching ? ' MATCHING ' + matching : '') + (where ? ' WHERE (' + where + ')' : '');
				}
				
				if(url) {
					
					if(url === groupingUrl) {
						return;
					}
					
					$scope.loading = true;
					
					$http({
						method: 'GET',
						url: url,
						params: {
							zapi_no_pager: 1
						}
					})
						.success(function ( response ) {
							var group,
								groups = [ ],
								grouping = $scope.activeGrouping,
								groupsById = {};
								
							_.each(response.result, function ( gr ) {
								var label = gr[grouping.resolve];
								
								if(grouping.translations) {
									label = grouping.translations[label];
								}
								
								if(!label) {
									label = translationService.get('Geen');
								}
								
								group = {
									value: gr[grouping.by],
									label: label,
									count: gr.count
								};
								
								if(groupsById[group.value]) {
									group = groupsById[group.value];
									group.count += gr.count;
								} else {
									groupsById[group.value] = group;
									groups.push(group);	
								}
								
							});
							
							groups = _.reject(groups, { value: null });
							
										
							$scope.groups = groups;
							
							setCrudUrl();
							
						})
						.error(function ( /*response*/ ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het ophalen van de groeperingen.')
							});
						})
							['finally'](function ( ) {
								$scope.loading = false;
							});
				} else {
					$scope.groups = [ groupAll ];
					$scope.loading = false;
					setCrudUrl();	
					
				}
				
				groupingUrl = url;
				
			}
			
			function setCrudUrl ( ) {
				crudUrl = '/api/object/?zql=' + $scope.getZql();
			}
			
			function getColumnFromAttr ( attr ) {
				var column = {
						id: attr.object.column_name,
						label: attr.label,
						resolve: 'values["' + attr.object.column_name + '"]',
						sort: {
							type: attr.object.value_type === 'integer' ? 'numeric': 'alphanumeric'
						}
					};
					
				switch(attr.object.value_type) {
					case 'valuta':
					case 'valutain':
					case 'valutaex':
					case 'valutain6':
					case 'valutain21':
					case 'valutaex21':
					column.filter = 'currency';
					column.sort.type = 'numeric';
					break;
					
					case 'date':
					column.filter = 'date:mediumDate';
					break;
					
					case 'numeric':
					column.filter = 'number';
					break;
					
					case 'option':
					case 'checkbox':
					column.templateUrl = '/html/widget/search/column-type-list.html';
					break;
					
					case 'bag_straat_adres':
					case 'bag_straat_adressen':
					case 'bag_adres':
					case 'bag_adressen':
					case 'bag_openbareruimte':
					case 'bag_openbareruimtes':
					column.resolve = 'values["' + column.id + '"].human_identifier';
					break;
				}
					
				setColumnTemplate(column);
						
				return column;
			}
			
			function setColumnTemplate ( column ) {
				var template = $scope.columns.templates[column.id];
				
				if(template) {
					if(_.isObject(template)) {
						column.templateUrl = template.url;
					} else {
						column.template = template;
					}
				}
			}
			
			function setActions ( ) {
				var actions = ($scope.actions || []).concat();
				
				_.each($scope.actions, function ( action ) {
					if(action.data && action.data.children) {
						actions = actions.concat(action.data.children);
					}
				});
				
				_.each(actions, function ( action ) {
					if(action.type === 'ezra-dialog') {
						action.when = 'isActionAllowed(\'' + action.id + '\', selectedItems)';
					}
				});
				
			}
			
			$scope.getZql = function ( ) {
				var where = $scope.getZqlWhere(),
					matching = $scope.getZqlMatching(),
					zql = 'SELECT ' + $scope.getZqlColumns() + ' FROM ' + $scope.objectType,
					isGrouped = $scope.activeGrouping && $scope.activeGrouping.by !== null && $scope.openGroup,
					column,
					as = '';
				
				if(matching) {
					zql += ' MATCHING ' + matching;
				}
					
				if(where || isGrouped) {
					zql += ' WHERE ';
					if(where) {
						zql += where;
					}
					if(isGrouped) {
						zql += (where ? ' AND ' : '') + $scope.activeGrouping.by + ' = ' + escapeForZql($scope.openGroup);
					}
					zql += '';
				}
				
				if(sortBy && sortOrder) {
					column = _.find($scope.activeSearch.columns, { id: sortBy } );
					if(column && column.sort && column.sort.type === 'numeric') {
						as = ' NUMERIC';
					}
					zql += as + ' ORDER BY ' + sortBy + (sortOrder === 'asc' ?  ' ASC' : ' DESC');
				}
				
				return zql;
			};
			
			$scope.getZqlWhere = function ( ) {
				var form = formService.get('searchWidgetForm'),
					where = '',
					values,
					unresolvedValues = {},
					fields,
					properties = [],
					conditions = [],
					orgUnits = [];
					
				if(!form) {
					return;
				}
				
				values = form.scope.getValues(true);
				unresolvedValues = form.scope.getValues(false);
				
				fields = form.scope.zsForm.fields;
				
				_.each(fields, function ( field ) {
					var val = values[field.name],
						vals;
					
					if(!field.ignore && (val !== undefined && val !== null) && !(_.isArray(val) && val.length === 0)) {
						
						switch(field.type) {
							case 'org-unit':
							_.each(val, function ( item ) {
								
								if(!item.orgUnit.role_id) {
									return;
								}
								
								vals = [];
								
								if(item.orgUnit.org_unit_id) {
									vals.push({
										name: field.name.split('$')[0],
										value: item.orgUnit.org_unit_id,
										type: field.type
									});
								}
								
								vals.push({
									name: field.name.split('$')[1],
									value: item.orgUnit.role_id,
									type: field.type
								});
								
								orgUnits.push({
									name: field.name,
									type: 'property-group',
									relation: 'AND',
									value: vals
								});
								
							});
							break;
							
							default:
							properties.push({
								name: field.name,
								value: val,
								type: field.type
							});
							break;
						}
					}
				});
				
				
				properties = _.reject(properties, function ( prop ) {
					return prop.name === 'fulltext';
				});
				
				if(properties.length) {
					conditions = zqlService.getConditions(properties);
					if(conditions.length) {
						where = conditions.join(' AND ');
					}
				}
				if(orgUnits.length) {
					if(where) {
						where += ' AND ';
					}
					where += '(' + zqlService.getConditions(orgUnits).join(' OR ') + ')';
				}
				
				return where;
			};
			
			$scope.getZqlMatching = function ( ) {
				var form = formService.get('searchWidgetForm'),
					matching = '',
					values,
					field;
					
				if(!form) {
					return;
				}
				
				values = form.scope.getValues(true);
				
				field = _.find(form.scope.zsForm.fields, function ( field ) {
					return field.name === 'fulltext';
				});
				
				if(values[field.name]) {
					matching = escapeForZql(values[field.name]);
				}
				
				return matching;
			};
			
			$scope.getZqlColumns = function ( ) {
				var search = $scope.activeSearch,
					columns = [];
				
				if(!search) {
					return;
				}
				
				_.each(search.columns, function ( col ) {
					if(col.data && col.data.columns) {
						columns = columns.concat(col.data.columns);
					} else {
						columns.push(col.id);
					}
				});
				
				_.each($scope.columns.always, function ( colId) {
					columns.push(colId);
				});
				
				if($scope.showLocation && $scope.map) {
					_.each($scope.map.columns, function ( colId ) {
						if(_.indexOf(columns, colId) === -1) {
							columns.push(colId);
						}
					});
				}
				
				return columns.join(', ');
			};
			
			$scope.getZqlGrouping = function ( ) {
				var grouping;
				
				if($scope.activeGrouping && $scope.activeGrouping.by) {
					grouping = [ $scope.activeGrouping.by ];
					if($scope.activeGrouping.by !== $scope.activeGrouping.resolve) {
						grouping.push($scope.activeGrouping.resolve);
					}
					grouping = grouping.join(', ');
				}
				
				return grouping;
			};
			
			$scope.getResultOptions = function ( ) {
				return resultOptions;
			};
			
			$scope.handleLockClick = function ( ) {
				$scope.filtersLocked = !$scope.filtersLocked;
			};
			
			$scope.setSearch = function ( search ) {
				$scope.activeSearch = search;
				setSearchForm();
			};
			
			$scope.getCrudUrl = function ( ) {
				return crudUrl;
			};
			
			$scope.setOpenGroup = function ( id ) {
				$scope.openGroup = id;
				setCrudUrl();
			};
			
			$scope.getActions = function ( ) {
				return $scope.actions;
			};
			
			$scope.getItemLink = function ( ) {
				return $scope.link;	
			};
			
			$scope.getItemResolve = function ( ) {
				return $scope.resolve;	
			};
			
			$scope.getItemStyle = function ( ) {
				return $scope.style;	
			};
			
			$scope.getDefaultSort = function ( ) {
				return $scope.sort;	
			};
			
			$scope.getSearchColumns = function ( $query ) {
				var deferred = $q.defer(),
					columns = [];
				
				if(httpCancel) {
					httpCancel.resolve();
				}
				
				httpCancel = $q.defer();
				$http({
					method: 'GET',
					url: '/objectsearch/attributes',
					params: {
						query: $query,
						all: $query ? 0 : 1
					},
					timeout: httpCancel
				})
					.success(function ( response ) {
						columns = _.map(response.json.entries, function ( attr ) {
							return getColumnFromAttr(attr);
						});
					})
					['finally'](function ( ) {
						deferred.resolve(columns);
					});
				
				return deferred.promise;	
			};
			
			$scope.getDefaultColumns = function ( ) {
				return _.map($scope.columns['default'], function ( col ) {
					var clone = _.clone(col);
					setColumnTemplate(clone);
					return clone;
				});
			};
			
			$scope.revertChanges = function ( ) {
				var form = formService.get('searchWidgetForm'),
					search = $scope.activeSearch,
					values = search.values;
					
				form.reset();
					
				for(var key in values) {
					form.setValue(key, values[key]);
				}
				
				setCrudUrl();
			};
			
			$scope.setShowLocation = function ( showLocation ) {
				$scope.showLocation = showLocation;
			};
			
			$scope.init = function ( ) {
				$scope.search = searchService.create( { objectType: $scope.objectType, ownerId: $scope.ownerId });
			};
			
			$scope.isActionAllowed = function ( id, selectedItems ) {
				
				return _.some(selectedItems, function ( item ) {
					return _.indexOf(item.actions, id) !== -1;
				});
			};
			
			$scope.$watch('activeGrouping', function ( ) {
				reloadGroups();
				setSearchForm();
			});

			$scope.$on('form.ready', function ( event ) {
				if(event.targetScope.getName() === 'searchWidgetForm') {
					setSearchForm();
				}
			});
			
			$scope.$on('form.submit.attempt', function ( ) {
				reloadGroups();
			});
			
			$scope.$on('crud.sort.change', function ( $event, by, order ) {
				sortBy = by;
				sortOrder = order;
				setCrudUrl();
			});
			
			$scope.$on('crud.column.add', function ( $event, column ) {
				var columns = $scope.activeSearch.columns;
					
				columns.splice(_.findLastIndex(columns, { 'locked': false }) - 1, 0, column);
				setCrudUrl();
			});
			
			$scope.$on('crud.column.remove', function ( $event, column ) {
				_.remove($scope.activeSearch.columns, function ( col ) {
					return col.id === column.id;
				});
				setCrudUrl();
			});
			
			$scope.$on('crud.column.sort.update', function ( event, column, before ) {
				var columns = $scope.activeSearch.columns.concat(),
					index = _.findIndex(columns, { 'id': column.id }),
					to = before ? _.findIndex(columns, { 'id': before.id }) : columns.length-3;
					
				if(index === to) {
					return;
				}
					
				column = _.find(columns, { 'id': column.id });
				columns = _.pull(columns, column);
				
				if(before) {
					to = _.findIndex(columns, { 'id': before.id } );
					columns.splice(to, 0, column);
				} else {
					columns.splice(columns.length-1, 0, column);
				}
				
				$scope.activeSearch.columns = columns;
			});
			
			$http({
				method: 'GET',
				url: '/api/search/grouping/' + $scope.objectType
			})
				.success(function ( response ) {
					$scope.groupings = response.result;
					$scope.activeGrouping = $scope.groupings[0];
					setSearchForm();
				})
				.error(function ( /*response*/ ) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Er ging iets fout bij het ophalen van de groeperingen voor de zoekopdracht')
					});
				});
				
			// todo: find a generic solution for this
				
			$http({
				method: 'GET',
				url: '/api/case/results',
				params: {
					zapi_no_pager: 1
				}
			})
				.success(function ( response ) {
					resultOptions = response.result;
				})
				.error(function  ( /*response*/ ) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Er ging iets fout bij het ophalen van de mogelijkheden voor resultaat')
					});
				});
				
			$scope.$on('zs.scope.data.apply', function ( event, parentScope/*, data*/ ) {
				
				if($scope === parentScope) {
					
					setActions();
					
					event.stopPropagation();
					init();
					
				}
			});
			
			$scope.$on('zs.search.bulk.attempt', function ( /*event*/ ) {
				$scope.loading = true;
			});
			
			$scope.$on('zs.search.bulk.success', function ( event, message ) {
				$scope.loading = false;
				$scope.$emit(message);
			});
			
			$scope.$on('zs.search.bulk.error', function ( event, message ) {
				$scope.loading = false;
				$scope.$emit(message);
			});
			
			$scope.$watch('showLocation', function ( ) {
				setCrudUrl();
			});
			
			function init ( ) {
				var sort = $scope.sort;
				
				if(sort) {
					sortBy = sort.by;
					sortOrder = sort.order || 'asc';
				}
				
				$scope.$broadcast('search.initialized');
			}
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.widget.search.FilterController', [ '$scope', function ( $scope ) {
			
			$scope.expanded = false;
			$scope.setEditMode = false;
			
			$scope.setExpanded = function ( exp ) {
				$scope.expanded = exp;
			};
			
			$scope.setEditMode = function ( mode ) {
				$scope.newName = $scope.filter.label;
				$scope.editMode = mode;
			};
			
			$scope.saveName = function ( ) {
				$scope.filter.label = $scope.newName;
				$scope.editMode = false;
				$scope.search.saveFilter($scope.filter);
			};
			
			$scope.cancelNameChange = function ( ) {
				$scope.editMode = false;	
			};
			
			$scope.onKeyUp = function ( $event ) {
				if($event.keyCode === 27) {
					$scope.cancelNameChange();
				} else if($event.keyCode === 13) {
					$scope.saveName();
				}
			};
			
		}]);
	
})();
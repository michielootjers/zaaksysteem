/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetResultController', [ '$scope', '$parse', 'formService', function ( $scope, $parse, formService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				crud = null,
				numRows = 20,
				crudItems = [];
				
			$scope.displayMode = 'list';
				
			function rebuildCrud ( ) {
				var filters,
					form = formService.get('searchWidgetForm'),
					params = {},
					columnIds = [],
					i,
					l;
					
				if(!form || !$scope.activeSearch) {
					return null;
				}
				
				filters = form.scope.getValues(true);
				
				for(i = 0, l = $scope.activeSearch.columns.length; i < l; ++i) {
					columnIds.push($scope.activeSearch.columns[i].id);
				}
				
				params.columns = columnIds;
				
				for(var key in filters) {
					if(filters[key] !== null) {
						params[key] = filters[key];
					}
				}
				
				crud = {
					"url": $scope.getCrudUrl(),
					"options": {
						"select": "multi",
						"link": $scope.getItemLink(),
						"resolve": $scope.getItemResolve(),
						"sort": $scope.getDefaultSort()
					},
					"actions": $scope.getActions() || [],
					"columns": $scope.activeSearch.columns,
					"style": $scope.getItemStyle(),
					"numrows": numRows
				};
			}
			
			$scope.getCrud = function ( group ) {
				if(group.value !== $scope.openGroup) {
					return null;
				}
				return crud;
			};
			
			$scope.onGroupClick = function ( group ) {
				if($scope.openGroup === group.value) {
					$scope.setOpenGroup(null);
				} else {
					$scope.setOpenGroup(group.value);
				}
			};
			
			$scope.setDisplayMode = function ( mode ) {
				$scope.displayMode = mode;
			};
			
			$scope.getLocations = function ( ) {
				var locations = [],
					mapConfig = $scope.map || {},
					locationResolve = mapConfig.resolve,
					parser = $parse(locationResolve),
					location;
					
				if(!locationResolve) {
					return locations;
				}
				
				_.each(crudItems, function ( crudItem ) {
					var lat,
						lng,
						expl,
						data = {},
						mapping = mapConfig.mapping || {};
											
					location = parser(crudItem);
					
					if(location) {
						expl = location.substr(1, location.length-2).split(',');
						lat = parseFloat(expl[0]);
						lng = parseFloat(expl[1]);
						
						for(var key in mapping) {
							data[key] = $parse(mapping[key])(crudItem);
						}
						
						locations.push({
							item: crudItem,
							marker: {
								latitude: lat,
								longitude: lng,
								popup_data: data,
								no_popup: 1
							}
						});
					}
				});
				
				return locations;
			};
			
			$scope.$watch('groups', function onGroupChange ( ) {
				var hasOpenGroup = !!_.find($scope.groups, function ( group ) {
					return group.value === $scope.openGroup;
				});
				
				if(!hasOpenGroup) {
					$scope.setOpenGroup($scope.groups.length === 1 ? $scope.groups[0].value : null);
				}
				
			}, true);
			
			$scope.$watch(function onChange ( ) {
				rebuildCrud();
			});
			
			$scope.$on('zs.pagination.numrows.change', function onNumRowsChange ( event, nr ) {
				safeApply($scope, function ( ) {
					numRows = nr;
				});
			});
			
			$scope.$on('zs.crud.item.change', function ( event, items ) {
				crudItems = items;
			});
			
		}]);
	
})();
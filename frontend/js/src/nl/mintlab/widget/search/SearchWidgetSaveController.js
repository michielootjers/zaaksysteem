/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetSaveController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			var defaultFilters = [];
			
			$scope.filters = [];
			$scope.collapsed = false;
			
			function getDefaultSearch ( ) {
				return {
					id: undefined,
					label: translationService.get('Nieuwe zoekopdracht'),
					editable: true,
					columns: $scope.getDefaultColumns(),
					values: {}
				};
			}
			
			function setFilters ( ) {
				$scope.search.getSavedFilters().then(function ( filters ) {
					$scope.filters = defaultFilters.concat(filters);
				});
			}
			
			$scope.createSearch = function ( ) {
				var search = getDefaultSearch();
				
				$scope.addSearch(search);
				$scope.setSearch(search);
				
				return search;
			};
			
			$scope.addSearch = function ( search ) {
				$scope.search.addFilter(search);
				setFilters();
			};
			
			$scope.removeSearch = function ( search ) {
				$scope.search.removeFilter(search);
				setFilters();
				if(search === $scope.activeSearch) {
					$scope.setSearch($scope.filters[0]);
				}
			};
			
			$scope.$on('search.initialized', function onSearchInit ( ) {
				defaultFilters = _.map($scope.filters, function ( filter ) {
					if(filter.columns === undefined) {
						filter.columns = $scope.getDefaultColumns();
					}
					return filter;
				});
				$scope.filters = defaultFilters.concat();
				$scope.setSearch($scope.filters[0]);
				
				$scope.search.getSavedFilters()
					.then(function ( filters ) {
						$scope.filters = $scope.filters.concat(filters);
					},
					function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het ophalen van de opgeslagen zoekopdrachten')
						});
					});
			});
			
		}]);
	
})();
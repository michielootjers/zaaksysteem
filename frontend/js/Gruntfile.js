/*global module*/
module.exports = function ( grunt ) {
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		loc: {
			src: [ 'src/nl/mintlab/utils/namespace.js', 'src/**/_*.js', 'src/**/*.js'  ]
		},
		watch: {
			default: {
				files: [ 'src/**/*.js' ],
				tasks: [ 'build' ],
				options: {
					nospawn: true
				}
			},
			dev: {
				files: [ 'src/**/*.js' ],
				tasks: [ 'concat:dist' ],
				options: {
					nospawn: true
				}	
			}
		},
		concat: {
			dist: {
				src: '<%= loc.src %>',
				dest: '../../root/js/zaaksysteem-app.js',
				process: true
			}
		},
		uglify: {
			options: {
				banner: '/*! Zaaksysteem - v<%= pkg.version %> - ' +
				'<%= grunt.template.today("yyyy-mm-dd") %>\n' +
				'* http://mintlab.nl/\n' +
				'* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
				'Mintlab b.v. <info@mintlab.nl>; Licensed EUPL */'
			},
			build: {
				options: {
					sourceMap: '../../root/js/zaaksysteem-app.source-map.js'
				},
				src: '<%= loc.src %>',
				dest: '../../root/js/zaaksysteem-app.min.js',
				process: true
			}
		}
		
	});
	
	grunt.util.linefeed = '\n';
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	
	grunt.registerTask('default', [ 'watch:default' ]);
	grunt.registerTask('dev', [ 'watch:dev' ]);
	
	grunt.registerTask('build', [ 'concat', 'uglify:build' ]);
};
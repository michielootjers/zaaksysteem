var path = require('path'),
	webpack = require('webpack'),
	ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports =
	{
		devtool: 'source-map',
		output: {
			path: path.join(__dirname, '/../root/js/'),
			publicPath: '/js/',
			filename: '[name].bundle.js'
		},
		entry: {
			zs: './index.js'
		},
		module: {
			loaders: [
				{
					test: /.*\.js$/,
					loader: 'babel-loader',
					exclude: /node_modules/,
					query: {
						presets: [ 'babel-preset-es2015' ].map(require.resolve),
						plugins: [ 'babel-plugin-add-module-exports' ].map(require.resolve)
					}
				},
				{
					test: /.*\.html$/,
					loader: 'html?minimize=false',
					exclude: /node_modules/
				},
				{
					test: /\.scss$/,
					loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'	),
					exclude: /node_modules/
				},
				{
					test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
					loader: 'url-loader?name=[name].[ext]&limit=10000'
				}
			]
		},
		resolve: {
			root: [
				path.resolve('../client'),
				path.resolve('./')
			],
			alias: {
				angular: path.join(__dirname, './angular-index.js')
			}
		},
		plugins: [
			new webpack.optimize.UglifyJsPlugin({
				comments: false
			}),
			new webpack.optimize.DedupePlugin(),
			new webpack.DefinePlugin({
				ENV: 'production',
				DEV: false,
				PROD: true
			}),
			new ExtractTextPlugin('../css/[name].css', {
				allChunks: true
			})
		]
	};

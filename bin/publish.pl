#!/usr/bin/perl -w

use strict;
use warnings;

use Net::SCP::Expect;
use Net::FTP;
use Data::Dumper;
use File::Path qw/remove_tree/;
use LWP::Simple;
use POSIX qw(strftime);
use Clone qw/clone/;
use LWP::UserAgent;
use HTTP::Request;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Config::Any;

# logging for ede available at: http://acc.ede.nl/?type=1362673971

# redirect all error output to stdout, because this script will be called from
# another perl process
*STDERR = *STDOUT;


my ($hostname, $profile, $source_directory) = @ARGV;
die "need hostname" unless $hostname;
die "need profile" unless $profile;
die "need source_directory" unless $source_directory;


exit main();

sub main {
    warn "Running publish script with hostname = '$hostname', profile = '$profile', source_directory = '$source_directory'.\n";

    my $customer_confs = Config::Any->load_files({
        files => [ glob("/etc/zaaksysteem/customer.d/*.conf") ],
        use_ext => 1,
        flatten_to_hash => 1
    });

    warn "Locating publication configuration...\n";

    my $customer;

    for my $customers (values %{ $customer_confs }) {
        next unless grep { $_ eq $hostname } keys %{ $customers };

        warn "Located config for $hostname\n";

        $customer = $customers->{ $hostname };
    }

    unless($customer) {
        my $path = "$FindBin::Bin/../etc/zaaksysteem.conf";

        my $zsconf = Config::Any->load_files({
            files => [ $path ],
            use_ext => 1,
        });

        unless($zsconf) {
            die('need config');
        }

        eval {
            $customer = $zsconf->[0]->{ $path }->{ customers }->{ $hostname };
        };
    }

    die('need customer') unless $customer;

    my $destination         = $customer->{publish}->{$profile};
    
    my $destination_clone = clone $destination;
    $destination_clone->{password} = '********';
    warn "Configuration: " . Dumper $destination_clone;

#
# these functions are half-generic, scp has been written for a link with Verseon/Ede, 
# ftp has been constructed around Perfectview. i suggest that when a new item is 
# added things are made more generic. can't justify doing that now.
#
    my $success;
    warn "Starting file transfer, protocol: '" . $destination->{protocol} . "'\n";

    warn "Files to be transferred:\n" . `ls -al $source_directory` . "\n";
    if($destination->{protocol} eq 'scp') {
        $success = scp({
            destination     => $destination,
            source          => $source_directory,
        });

    } elsif($destination->{protocol} eq 'ftp') {
        $success = ftp({
            destination     => $destination,
            source          => $source_directory,
        });
    } else {
        die "unknown protocol $destination->{protocol}";
    }

    $source_directory =~ s|\.$||is;
    remove_tree($source_directory);

    if($success) {
        warn "Succesfully transferred files.\n";
    } else {
        warn "Error, could not transfer.\n";
    }

    # invert success value here, because we use the exit value to 
    # communicate to the caller if the action was succesful. 
    # exit 0 means => all is well. exit 1 means => trouble in paradise, houston we have problem..
    return !$success;
}


sub ftp {
    my ($opts) = @_;

    my $destination         = $opts->{destination}      or die "need destination";
    my $source              = $opts->{source}           or die "need source";

    die "username not configured"           unless $destination->{username};
    die "password not configured"           unless $destination->{password};
    die "csv_filename not configured"       unless $destination->{csv_filename};
    die "notify_filename not configured"    unless $destination->{notify_filename};

    warn "Starting FTP transfer to " . $destination->{destination} . "\n";

    my $ftp = Net::FTP->new(
        $destination->{destination}, Debug => 0
    ) or die "Cannot connect to host: $@";

    warn "Logging in to remote FTP with username: '" . $destination->{username} . "'\n";

    $ftp->login(
        $destination->{username},
        $destination->{password}
    ) or die "Cannot login ", $ftp->message;

    warn "Putting " . $destination->{csv_filename} . "\n";

    $ftp->put(
        $source . '/' . $destination->{csv_filename}
    ) or die "put failed ", $ftp->message;

    if($destination->{notify_filename}) {
        my $filename = $source . '/'. $destination->{notify_filename};
        warn "Putting notify file:" . $filename . "\n";

        my $content = _get_header_file_content();

        warn "Notify file content:" . $content . "\n";

        open FILE, ">" .$filename
            or die "could not open file $filename for writing: " . $!;
        print FILE $content;
        close FILE;

        $ftp->put($filename) 
            or die "put failed ", $ftp->message;    

        warn "Done putting notify file. Removing locally.\n";
        system("rm " . $filename);
    }
    $ftp->quit;
    
    if($destination->{notify_url}) {
        warn "Retrieving url '" . $destination->{notify_url} . "' to notify receiving server of updates.\n";
        my $response = get($destination->{notify_url});
        warn "Response: " . $response . "\n";
        if($response eq 'Succes.') {
            return 1;
        } else {
            warn "Error notifying receiving server.\n";
        }
    }
    return 0;
}
    

sub scp {    
    my ($opts) = @_;

    my $destination         = $opts->{destination}      or die "need destination";
    my $source              = $opts->{source}           or die "need source";

    die "username not configured"           unless $destination->{username};
    die "password not configured"           unless $destination->{password};
    die "destination not configured"        unless $destination->{destination};

    my $port = $destination->{ port } || 22;

    warn "Starting SCP transfer to: " . $destination->{destination} . ", port: $port\n";

    my $scpe = Net::SCP::Expect->new(
        auto_yes    => 1,  # yes, add this host to the list of known hosts
        recursive   => 1,
        port        => $port
    );

    warn "Logging in to remote SCP with username: '" . $destination->{username} . "'\n";

    $scpe->login(
        $destination->{username}, 
        $destination->{password}
    );

    warn "Scp-ing $source to $destination->{destination}\n";

    eval {
        $scpe->scp(
            $source,
            $destination->{destination}
        );
    };

    if($@) {
        warn "Error during SCP transfer:\n" . $@;
        return 0;
    }

    if($destination->{notify_url}) {
        sleep 2; # sleep to give receiving system time to recover from previous onslaught.
        warn "Retrieving notify url:'" . $destination->{notify_url} . "' to notify receiving system.\n";
        
        my $ua = LWP::UserAgent->new;
        my $request = HTTP::Request->new(GET => $destination->{notify_url});        
        my $response = $ua->request($request);
        
        warn "Response code: " . $response->code . "\n";
        
        if($response->code() eq '503') {
            warn "503: Server too busy. Try again in a few minutes.";
        }

        unless($response->code() eq '200') {
            warn "Error: Response code " . $response->code() . " from notify url.\n";
            return 0;
        }
    }
    
    return 1;
}

sub _get_header_file_content {
    
    # or for GMT formatted appropriately for your locale:
    my $time = strftime "%H:%M:%S", gmtime;
    my $date = strftime "%d-%m-%Y", gmtime;


    my $file_content = <<END_FILE_CONTENT;



----- Dit bestand is gegenereerd met de Zaaksysteem.nl module -----
Datum       : $date
Tijd        : $time
Gebruiker   : Mintlab
Module      : Gevonden voorwerpen
END_FILE_CONTENT

    return $file_content;
}

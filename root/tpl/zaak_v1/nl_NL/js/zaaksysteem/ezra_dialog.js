/*

EzraDialog is the standard way to show a dialog. The html content for the dialog is retrieved
from the server, then the callback function is executed to enable specific jquery components.

options:
- title (default '')
- url  (required)
- cgi_params (optional, will be sent as cgi parameters)
- callback function with jquery inits (will be executed after loading)
- layout (default 'medium', other options are 'small' and 'large')
- which dialog (default '#dialog')

up for discussion:
- putting callbacks in html using the rel attribute. currently not supported.

todo
- verticaal centreren

*/

function debug(object) {
    //console.log(object);
}

$(document).ready( function() {
  

    $(document).on('click', '.ezra_dialog', function() {
        var obj = $(this);

        // enables dialog togeter with qtip - which replaces the title tag
        var title = obj.attr('title');
        var rel_options = getOptions(obj.attr('rel'));

        if(title == undefined && rel_options != undefined && rel_options.hasOwnProperty('title')) {
            title = rel_options.title;
        }
        var dialog_options = {
            url         : obj.attr('href'),
            title       : title
        };
        
        var rel_options = getOptions(obj.attr('rel'));
        if(rel_options['ezra_dialog_layout']) {
            dialog_options['layout'] = rel_options['ezra_dialog_layout'];
        }

        ezra_dialog(dialog_options);
        return false;
    });

});


function ezra_dialog(options, callback) {

    var start = new Date().getTime();
    if(!options) {
        debug('options missing');
        return;
    }
    
    var url = options['url'];
    if(!url) {
       debug('url parameter missing');
       return;
    }

    var title      = options['title']      || '';
    var cgi_params = options['cgi_params'] || '';
    var layout     = options['layout']     || 'medium';
    var which      = options['which']      || '#dialog';

	$.ztWaitStart();

    var dialog_element = $(which);
    if(!dialog_element) {
        debug('dialog element ' + which + ' not found');
        return;
    }

    var dialog_content = dialog_element.find('.dialog-content');
    dialog_content.html('');

    // always tidy up, so no confusing information can linger. a precaution.
	dialog_element.dialog(
		'option',
		'beforeclose',
		function() {
            dialog_content.html('');
		}
	);


    // remove classes added
    var classList = dialog_content.attr('class').split(/\s+/);
    for(var i = 0; i< classList.length; i++) {
        if (classList[i] != 'dialog-content') {
            dialog_content.removeClass(classList[i]);
        }
    }


    dialog_content.load(
        url,
        cgi_params,
        function(responseText, textStatus, XMLHttpRequest) {
            //console.log([responseText,textStatus,XMLHttpRequest]);
            $.ztWaitStop();
                
            var width   = 580;
            if(layout == 'small') {
                width = 355;
            }
            if(layout == 'mediumsmall') {
                width = 460;
            }
            if(layout == 'mediumlarge') {
                width = 700;
            }
            if(layout == 'large') {
                width = 800;
            }
            if(layout == 'xlarge') {
                width = 960;
            }
            if(layout == 'medium-accordion') {
                width = 580;
                dialog_content.addClass('dialog-content-accordion');
            }
            if(layout == 'mediumlarge-accordion') {
                width = 700;
                dialog_content.addClass('dialog-content-accordion');
            }
            if(layout == 'large-accordion') {
                width = 800;
                dialog_content.addClass('dialog-content-accordion');
            }
            if(layout == 'new-layout-large') {
                width = 820;
                dialog_content.addClass('dialog-new-layout');
            }
            var height  = 'auto';

            dialog_element.dialog({
                position    : ['center',35],
                width       : width,
                height      : height,
                maxHeight   : 540,                
                autoOpen    : false,
                modal       : true,
                resizable   : false,
                draggable   : true,
                zIndex      : 3200,
                title       : title,
                open: function(event, ui) { $(this).scrollTop(0); }
            }).addClass('smoothness').dialog('open');

            if(textStatus != 'success') {
                $(this).html('Er is een probleem opgetreden, ververs de pagina');
                return;
            }

            $('#accordion').accordion({
                autoHeight: false
            });
            ezra_tooltip_handling();
            initializeEverything(dialog_content);
            
            if(callback) {
                callback(dialog_content);
            }
        }
    );
}


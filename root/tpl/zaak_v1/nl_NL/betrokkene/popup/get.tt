[% USE Scalar %]
[%
    BMAP.medewerker = 'Behandelaar';
    BMAP.natuurlijk_persoon = 'Persoon';

    brontype = {
        'bedrijf'               => 'Niet-natuurlijk persoon (Organisatie)',
        'natuurlijk_persoon'    => 'Natuurlijk persoon',
        'medewerker'            => 'Behandelaar',
    };
%]
[% IF betrokkene.btype == 'natuurlijk_persoon' || betrokkene.btype == 'bedrijf' %]
    <form method="POST" class="ezra_subject_details zvalidate" action="[% c.uri_for('/betrokkene/info/update/' _ betrokkene.gmid ) %]">
        <input type="hidden" name="betrokkene_type" value="[% betrokkene.btype %]">
[% END %]

<input type="hidden" name="zaak" value="[% zaak.nr %]">
[% IF betrokkene.btype == 'bedrijf' %]
    [% INCLUDE widgets/betrokkene/view_bedrijf.tt %]
[% ELSE %]
    <table>
        <tr>
            <td class="td180 label">Type:</td>
            <td class="td225">[% BMAP.${betrokkene.btype} %]</td>
        </tr>
        <tr>
            <td class="td180 label">Authentiek:</td>
            <td class="td225">
                [% IF betrokkene.authenticated %]
                    <i class="icon-betrokkene-gm icon"></i>
                [% ELSE %]
                    <i class="icon-betrokkene-zs icon"></i>
                [% END %]
            </td>
        </tr>
        [% IF betrokkene.btype == 'natuurlijk_persoon' %]
            <tr>
                <td class="td180 label">Bron:</td>
                <td class="td225">
                    [% IF betrokkene.authenticated %]
                        GBA
                    [% ELSE %]
                        [% betrokkene.authenticatedby %]
                    [% END %]
                </td>
            </tr>
        [% ELSE %]
            <tr>
                <td class="td180 label">Bron:</td>
                <td class="td225">[% (betrokkene.btype == 'medewerker' ? 'LDAP' : 'GBA (Gegevensmagazijn)') %]</td>
            </tr>
        [% END %]
        [% IF betrokkene.btype == 'medewerker' %]
            <tr>
                <td class="td180 label">Status:</td>
                <td class="td225">[% (betrokkene.is_active ? 'Actief' : 'Inactief') %]</td>
            </tr>
        [% END %]
        [% IF betrokkene.btype == 'natuurlijk_persoon' %]
            <tr>

                <td class="td180 label">BSN:</td>
                <td class="td225">[% betrokkene.burgerservicenummer %]</td>
            </tr>
            <tr>
                <td class="td180 label">Voornamen:</td>
                <td class="td225">[% betrokkene.voornamen %]</td>
            </tr>
            <tr>
                <td class="td180 label">Tussenvoegsels:</td>
                <td class="td225">[% betrokkene.voorvoegsel %]</td>
            </tr>
            <tr>
                <td class="td180 label">Achternaam:</td>
                <td class="td225">[% betrokkene.geslachtsnaam %]</td>
            </tr>
        [% END %]
        [% IF betrokkene.btype == 'medewerker' %]
            <tr>
                <td class="td180 label">Naam:</td>
                <td class="td225">[% betrokkene.display_name %]</td>
            </tr>
            <tr>
                <td class="td180 label">Telefoon:</td>
                <td class="td225">[% betrokkene.telefoonnummer %]</td>
            </tr>
            <tr>
                <td class="td180 label">Functie:</td>
                <td class="td225"></td>
            </tr>
            <tr>
                <td class="td180 label">Afdeling:</td>
                <td class="td225">[% betrokkene.org_eenheid.naam %]</td>
            </tr>
            <tr>
                <td class="td180 label">E-mailadres:</td>
                <td class="td225">[% betrokkene.email %]</td>
            </tr>
        [% END %]
        [% IF betrokkene.btype == 'natuurlijk_persoon' %]
            <tr>
                <td class="td180 label">Geboortedatum:</td>
                <td class="td225">[% betrokkene.geboortedatum.dmy %]</td>
            </tr>
            <tr>
                <td class="td180 label">Overlijdensdatum:</td>
                <td class="td225">[% betrokkene.datum_overlijden.dmy %]</td>
            </tr>
            <tr>
                <td class="td180 label">Geslacht:</td>
                <td class="td225">[% (betrokkene.geslachtsaanduiding == 'M' ? 'Man' : 'Vrouw') %]</td>
            </tr>
            <tr>
                <td class="td180 label">Straatnaam:</td>
                <td class="td225">[% betrokkene.straatnaam %]</td>
            </tr>
            <tr>
                <td class="td180 label">Huisnummer:</td>
                <td class="td225">[% betrokkene.huisnummer %]</td>
            </tr>
            <tr>
                <td class="td180 label">Huisletter:</td>
                <td class="td225">[% betrokkene.huisletter %]</td>
            </tr>
            <tr>
                <td class="td180 label">Toevoeginghuisnr:</td>
                <td class="td225">[% betrokkene.huisnummertoevoeging %]</td>
            </tr>
            <tr>
                <td class="td180 label">Postcode:</td>
                <td class="td225">[% betrokkene.postcode %]</td>
            </tr>
            <tr>
                <td class="td180 label">Plaats:</td>
                <td class="td225">[% betrokkene.woonplaats %]</td>
            </tr>
            <tr>
                <td class="td180 label">Telefoonnummer:</td>
                <td
                    class="td225 validator-next-line">
                    <input type="text" class="input_large" value="[% betrokkene.telefoonnummer %]" name="npc-telefoonnummer">
                    [% PROCESS widgets/general/validator.tt %]
                </td>
            </tr>
            <tr>
                <td class="td180 label">Mobiel:</td>
                <td class="td225 validator-next-line">
                <input type="text" class="input_large" value="[% betrokkene.mobiel  %]" name="npc-mobiel">
                [% PROCESS widgets/general/validator.tt %]
                </td>
            </tr>
            <tr>
                <td class="td180 label">E-mailadres:</td>
                <td class="td225 validator-next-line">
                <input type="text" class="input_large" value="[% betrokkene.email %]" name="npc-email">
                [% PROCESS widgets/general/validator.tt %]
                </td>
            </tr>
        [% END %]
    </table>
    <br />

[% END %]

[% IF betrokkene.btype != 'medewerker' %]
    <div class="form-actions">
        <input type="submit" value="Opslaan" class="button140" id="dialog-submit"/>
    </div>
[% END %]

</form>

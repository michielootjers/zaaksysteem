#! perl
use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;

use JSON;
use Test::Deep;
use File::Spec::Functions;

BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    openbareruimte_begindatum       => '1830-01-01',
    openbareruimte_identificatie    => '0363300000000059',
    openbareruimte_naam             => 'Donker Curtiusstraat',
    openbareruimte_status           => 'Naamgeving uitgegeven',
    openbareruimte_inonderzoek      => 'N',
    openbareruimte_type             => 'Weg',

    woonplaats_identificatie        => '1024',
    woonplaats_naam                 => 'Amsterdam',
};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0310', '/opr/101-opr-create.xml'),
        '0310'
    );

    is($stuf->entiteittype, 'OPR', 'Found entiteittype OPR');

    my $r02_params = $stuf->get_params_for_opr;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats');

    my $rv = Data::FormValidator->check(
        $r02_params,
        Params::Profile->get_profile('method' => 'Zaaksysteem::Backend::BagWoonplaats::ResultSet::bag_create_or_update')
    );

    for my $key (keys %{ $VALIDATION_MAP }) {
        my $givenvalue = $rv->valid($key);
        my $wantedvalue = $VALIDATION_MAP->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    #note(explain(my $bla = $rv->valid));

    ok($rv->success, 'Valid data for BagWoonplaats profile');

}, 'Check OPR native params');


zs_done_testing;

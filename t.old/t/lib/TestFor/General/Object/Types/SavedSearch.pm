package TestFor::General::Object::Types::SavedSearch;

use base 'ZSTest';

use TestSetup;
use Zaaksysteem::Object::Types::SavedSearch;

sub zs_object_types_savedsearch : Tests {
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                title => 'foo',
                query => 'bar',
            );

            my $obj = Zaaksysteem::Object::Types::SavedSearch->new(%opts);
            isa_ok($obj, "Zaaksysteem::Object::Types::SavedSearch");
            is($obj->type, 'saved_search', "Type is correct");

            foreach (qw(title query)) {
                throws_ok(
                    sub {
                        Zaaksysteem::Object::Types::SavedSearch->new(%opts,
                            $_ => '');
                    },
                    qr/Attribute \Q($_)\E does not pass the type constraint because.*NonEmptyStr.*/,
                    "Empty $_ is not legit"
                );
            }


        },
        'SavedSearch object'
    );
}

sub zs_object_types_savedsearch_zql : Tests {
    my $ss = Zaaksysteem::Object::Types::SavedSearch->new(
        title => 'derp',
        query => '{ "zql": "SELECT c, b, a FROM derp" }'
    );

    is_deeply $ss->query_data, { zql => 'SELECT c, b, a FROM derp' },
        'decoded query content matches';

    isa_ok $ss->zql, 'Zaaksysteem::Search::ZQL',
        'decoded query inflates to ZQL instance';

    is_deeply [ $ss->zql->cmd->probe_fieldnames ], [qw[c b a]],
        'decoded query exposes correct fieldname probes';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

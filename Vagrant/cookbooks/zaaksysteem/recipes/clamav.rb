apt_package "clamav" do
    action :install
end

apt_package "clamav-daemon" do
    action :install
end

service "clamav-daemon" do
    supports :status => true, :restart => true, :reload => true
    action [ :enable, :start ]
end

cron "freshclam" do
    action :create

    # Randomize minute mark so we don't hammer the clamav servers
    minute rand(0..59)
    hour "12"

    command "freshclam --quiet"
end

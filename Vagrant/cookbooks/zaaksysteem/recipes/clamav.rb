apt_package "clamav" do
    action :install
end

cron "freshclam" do
    action :create

    # Randomize minute mark so we don't hammer the clamav servers
    minute rand(0..60)
    hour "12"

    command "freshclam --quiet"
end

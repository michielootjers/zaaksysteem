#
# Cookbook Name:: zaaksysteem
# Recipe:: default
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe "postgresql::server";
include_recipe "postgresql::ruby";

apt_package 'postgresql-contrib'


### Database creation
postgresql_connection_info = {
    :host       => "127.0.0.1",
    :port       => node['zaaksysteem']['database']['config']['port'],
    :username   => node['zaaksysteem']['database']['username']['postgres'],
    :password   => node['zaaksysteem']['database']['password']['postgres']
}

postgresql_database_user 'vagrant' do
    connection postgresql_connection_info
    password node['zaaksysteem']['database']['password']['postgres']
    action :create
end

postgresql_database_user 'root' do
    connection postgresql_connection_info
    password node['zaaksysteem']['database']['password']['postgres']
    action :create
end

postgresql_database 'zaaksysteem' do
  connection postgresql_connection_info
  action :create
end

postgresql_database 'zs_test' do
  connection postgresql_connection_info
  action :create
end

postgresql_database_user 'vagrant' do
    connection postgresql_connection_info
    database_name "zaaksysteem"
    privileges [:all]
    action :grant
end

execute "vagrant_superuser" do
    user "postgres"
    command "echo 'ALTER USER vagrant SUPERUSER' | psql template1"
end

execute "vagrant_password" do
    user "postgres"
    command "echo 'ALTER USER vagrant PASSWORD \"#{node['zaaksysteem']['database']['password']['postgres']}\"'"
end

execute "database_test" do
    cwd "/vagrant"
    user "vagrant"
    command "/bin/bash dev-bin/load_database.sh zs_test db/test-template.sql"
    only_if '/bin/bash /vagrant/dev-bin/is_database_loaded.sh zs_test', :user => 'postgres'
end

execute "database_zaaksysteem" do
    cwd "/vagrant"
    user "vagrant"
    command "/bin/bash dev-bin/load_database.sh zaaksysteem db/test-template.sql"
    only_if '/bin/bash /vagrant/dev-bin/is_database_loaded.sh zaaksysteem', :user => 'postgres'
end

bash "set_filestore_and_tmp_dir" do
    user "vagrant"
    code <<-EOH
        echo "DELETE FROM config WHERE parameter IN ('filestore_location','tmp_location);" | psql zaaksysteem
        echo "INSERT INTO config (parameter,value) VALUES('filestore_location','/srv/storage'); INSERT INTO config (parameter,value) VALUES('tmp_location','/tmp/store');" | psql zaaksysteem
    EOH
end

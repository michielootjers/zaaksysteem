#
# Cookbook Name:: zaaksysteem
# Recipe:: development
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

### Development tools
%w{subversion w3m curl screen git}.each do |pkg|
    apt_package pkg do
        action :install
    end
end

### NodeJS
apt_repository "nodejs" do
  uri "http://ppa.launchpad.net/chris-lea/node.js/ubuntu"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "C7917B12"
end

%w{nodejs}.each do |pkg|
    apt_package pkg do
        action :install
    end
end

execute "npm_configure" do
    cwd "/"
    user "root"
    command 'npm config set ca ""'
end

execute "install_grunt_cli" do
    cwd "/"
    user "root"
    command "npm install -g grunt-cli"
end

execute "install_bower_cli" do
    cwd "/"
    user "root"
    command "npm install -g bower"
end

execute "install_npm_js_packages" do
    command "su vagrant -l -c 'cd /vagrant/frontend/js && npm install'"
    only_if 'test -f /vagrant/frontend/js/package.json'
end

execute "install_npm_packages" do
    command "su vagrant -l -c 'cd /vagrant/frontend && npm install'"
    only_if 'test -f /vagrant/frontend/package.json'
end

execute "install_npm_html_packages" do
    command "su vagrant -l -c 'cd /vagrant/frontend/html && npm install'"
    only_if 'test -f /vagrant/frontend/html/package.json'
end

execute "install_npm_css_packages" do
    command "su vagrant -l -c 'cd /vagrant/frontend/css && npm install'"
    only_if 'test -f /vagrant/frontend/css/package.json'
end

execute "bower_install" do
    command "su vagrant -l -c 'cd /vagrant/frontend/js && bower install'"
    only_if 'test -f /vagrant/frontend/js/bower.json'
end

cookbook_file "/etc/init/grunt-watcher.conf" do
    source "grunt-watcher.conf"
    mode 00700
    owner "root"
    group "root"
    not_if 'test -f /vagrant/.no_grunt_watcher'
end

execute "start_grunt_watcher" do
    command "start grunt-watcher || restart grunt-watcher"
    not_if 'test -f /vagrant/.no_grunt_watcher'
end

import login from './../../../../functions/common/auth/login';
import logout from './../../../../functions/common/auth/logout';
import loginAs from './../../../../functions/common/auth/loginAs';
import createWidget from './../../../../functions/intern/plusActions/newWidget/createWidget';

describe('when viewing the dashboard', ( ) => {

	beforeAll(( ) => {

		logout();

		loginAs('dashboardempty');

	});

	describe('and creating a case intake widget', ( ) => {

		beforeAll(( ) => {

			createWidget('Zoekopdracht', 'Zaakintake');

		});

		it('the dashboard should contain a functional case intake', ( ) => {

			expect($('[data-name="intake"] [href="/intern/zaak/44"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			$('.widget-header-remove-button').click();

		});

	});

	describe('and creating a favorite casetype widget', ( ) => {

		beforeAll(( ) => {

			createWidget('Favoriete zaaktypen');

		});

		it('the dashboard should contain a functional favorite casetype widget', ( ) => {

			expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');

		});

		afterAll(( ) => {

			$('.widget-header-remove-button').click();

		});

	});

	afterAll(( ) => {

		logout();

		login();

	});

});

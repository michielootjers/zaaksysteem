import angular from 'angular';
import uiRouter from 'angular-ui-router';
import createCaseRegistrationModule from './../shared/case/createCaseRegistration';
import createContactModule from './../shared/contact/createContact';
import createContactMomentModule from './views/root/createContactMoment';
import sessionServiceModule from './../shared/user/sessionService';
import contextualActionServiceModule from './../shared/ui/zsContextualActionMenu/contextualActionService';
import snackbarServiceModule from './../shared/ui/zsSnackbar/snackbarService';
import zsInternModule from './zsIntern';
import assign from 'lodash/assign';
import startsWith from 'lodash/startsWith';
import mapKeys from 'lodash/mapKeys';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import omitBy from 'lodash/omitBy';
import pickBy from 'lodash/pickBy';
import difference from 'lodash/difference';

angular.module('rootRoute', [
	sessionServiceModule,
	zsInternModule,
	createCaseRegistrationModule,
	createContactModule,
	createContactMomentModule,
	contextualActionServiceModule,
	snackbarServiceModule,
	uiRouter
])
	.config( [ '$stateProvider', ( $stateProvider ) => {

		let orig = $stateProvider.state;

		$stateProvider.state = ( ...rest ) => {

			let [ name, opts ] = rest;

			if (!opts) {
				opts = name;
				name = opts.name;
			}

			if (name.indexOf('.') === -1) {
				assign(opts, {
					parent: 'root'
				});
			}

			orig(...rest);

			return $stateProvider;
		};

		orig('root', {
			template: '<ui-view></ui-view>',
			resolve: {
				user: [ '$window', '$rootScope', 'sessionService', 'snackbarService', ( $window, $rootScope, sessionService, snackbarService ) => {
					
					let userResource = sessionService.createResource($rootScope, { cache: { every: 15 * 60 * 1000 } });

					return userResource.asPromise()
						.then( ( ) => {
							return userResource;
						})
						.catch( ( err ) => {

							console.error(err);

							$window.location = '/auth/login';

							return snackbarService.error('De gebruiker kon niet worden geladen. U wordt doorverwezen naar het loginscherm.');
						});

				}],
				settingsResource: [ '$rootScope', '$q', 'resource', 'snackbarService', ( $rootScope, $q, resource, snackbarService ) => {

					let settingsResource = resource('/api/v1/general/config', { scope: $rootScope });

					return settingsResource.asPromise()
						.then(( ) => settingsResource)
						.catch( ( error ) => {

							snackbarService.error('Configuratieinstellingen van het dashboard konden niet geladen worden. Neem contact op met uw beheerder voor meer informatie.');

							return $q.reject(error);

						});

				}]
			},
			controller: [ '$scope', 'composedReducer', 'user', function ( $scope, composedReducer, userResource ) {

				let ctrl = this,
					permissions;

				let includesRoles = ( session, roles ) => {
					return difference(roles, session.instance.logged_in_user.system_roles).length < roles.length;
				};

				permissions = composedReducer({ scope: $scope }, userResource)
					.reduce(( user ) => {

						return {
							contact: includesRoles(user, [ 'Administrator', 'Zaaksysteembeheerder', 'Contactbeheerder' ]),
							contactmoment: includesRoles(user, [ 'Administrator', 'Zaaksysteembeheerder', 'Contactbeheerder' ]),
							case: true
						};
					});

				ctrl.getPermissions = permissions.data;

			}],
			onActivate: [
				'$document', '$location', '$state', 'snackbarService', 'user', 'settingsResource',
				( $document, $location, $state, snackbarService, userResource, settingsResource ) => {

				let params =
						mapKeys(
							pickBy($location.search(), ( value, key ) => startsWith(key, 'flash_message')),
							( value, key ) => key.replace('flash_message_', '')
						),
					msg,
					snackType = 'info',
					actions = [];

				switch (params.action) {

					case 'create_case':
					if (params.case_id && $state.current.name.indexOf('case') === -1 ) {
						msg = `Zaak geregistreerd onder zaaknummer ${params.case_id}`;
						if (params.assignee_result === 'failed') {
							msg += ', maar kon niet worden toegewezen aan een behandelaar.';
						}
						actions = [
							{
								type: 'link',
								link: $state.href('case', { caseId: params.case_id }),
								label: 'Zaak openen'
							}
						];
					} else {
						msg = 'Zaak is door u in behandeling genomen';
					}
					break;

					case 'subject_no_address':
					msg = 'Betrokkene niet gevonden: geen (correct) adres aanwezig';
					snackType = 'error';
					break;

					case 'subject_not_found':
					msg = 'Betrokkene niet gevonden';
					snackType = 'error';
					break;

				}

				if (msg) {
					snackType = snackType === 'info' ? snackbarService.info(msg, { actions }) : snackbarService.error(msg);
				}

				$location.search(omitBy($location.search, ( value, key ) => startsWith(key, 'flash_message')));

				userResource.onUpdate(( ) => {

					let company = userResource.data().instance.account,
						name = company.instance.company;

					$document.find('body').addClass(`customer-${name.toLowerCase()}`);

				});

				settingsResource.onUpdate(( ) => {

					let settings =
							mapValues(
									pickBy(
										keyBy(settingsResource.data(), 'reference'),
									( value, key ) => startsWith(key, 'newrelic')
								),
								'instance.value'
							);

					if (settings.newrelic_application_id && settings.newrelic_license_key) {
						require([ './../shared/integration/newRelic' ], ( newRelic ) => {
							newRelic(settings.newrelic_application_id, settings.newrelic_license_key, $document[0]);
						});
					}


				});

			}],
			actions: [
				{
					name: 'zaak',
					label: 'Zaak aanmaken',
					iconClass: 'folder-outline',
					template:
						`<create-case-registration
							on-close="close($promise)"
							casetype-id="casetypeId"
							data-file="params.file"
							data-requestor="params.requestor"
						></create-case-registration>`,
					params: {
						casetypeId: null
					},
					when: [ 'viewController', ( ctrl ) => !!ctrl.getPermissions().case ]
				},
				{
					name: 'contact',
					label: 'Contact aanmaken',
					iconClass: 'account-plus',
					template: '<create-contact on-close="close($promise)"></create-contact>',
					when: [ 'viewController', ( ctrl ) => !!ctrl.getPermissions().contact ]
				},
				{
					name: 'contact-moment',
					label: 'Contactmoment toevoegen',
					iconClass: 'comment-plus-outline',
					template:
						`<create-contact-moment
							on-submit="close($promise)"
							data-subject="params.subject"
							case-id="params.caseId"
						></create-contact-moment>`
				}
			]
		});

	}]);

let context = require.context('./views', true, /^\.\/[^\/]+\/route\.js$/),
	routes = context.keys().map(context);

const HOME = '/intern/';

export default angular.module('Zaaksysteem.intern.routing',
		[
			'rootRoute'
		]
			.concat(routes)
	)
	.constant('HOME', HOME)
	.config([ '$locationProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider', ( $locationProvider, $urlRouterProvider, $urlMatcherFactoryProvider ) => {

		$locationProvider.html5Mode(true);

		$urlMatcherFactoryProvider.strictMode(false);

		// $urlRouterProvider.otherwise(`/niet-gevonden`);


	}])
	.run([ '$rootScope', ( $rootScope ) => {
		$rootScope.$on('$stateNotFound', ( ...rest ) => {
			console.log(...rest);
		});

		$rootScope.$on('$stateChangeError', ( ...rest ) => {
			console.log(...rest);
		});

	}])
	.name;

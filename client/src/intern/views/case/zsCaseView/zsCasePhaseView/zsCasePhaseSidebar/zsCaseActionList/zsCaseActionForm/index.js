import angular from 'angular';
import vormFieldsetModule from './../../../../../../../../shared/vorm/vormFieldset';
import email from './../../../../../forms/email';
import subCase from './../../../../../forms/subCase';
import templateForm from './../../../../../forms/template';
import allocation from './../../../../../forms/allocation';
import subject from '../../../../../forms/subject';
import vormValidatorModule from './../../../../../../../../shared/vorm/util/vormValidator';
import composedReducerModule from './../../../../../../../../shared/api/resource/composedReducer';
import zsIconModule from './../../../../../../../../shared/ui/zsIcon';
import seamlessImmutable from 'seamless-immutable';
import get from 'lodash/get';
import last from 'lodash/last';
import map from 'lodash/map';
import find from 'lodash/find';
import template from './template.html';

export default
	angular.module('zsCaseActionForm', [
		vormFieldsetModule,
		vormValidatorModule,
		composedReducerModule,
		zsIconModule
	])
		.directive('zsCaseActionForm', [ 'composedReducer', 'vormValidator', 'dateFilter', ( composedReducer, vormValidator, dateFilter ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					action: '&',
					templates: '&',
					caseDocuments: '&',
					requestor: '&',
					phases: '&',
					onActionSave: '&',
					onActionExecute: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						form,
						fields,
						values = seamlessImmutable({}),
						verb,
						action = ctrl.action(),
						reducers = [],
						locals,
						validityReducer;

					let getData = ( ) => {
						return reducers.reduce(( value, reducer ) => {
							return reducer(value);
						}, values);
					};

					switch (action.type) {

						case 'case': {


							let startDate =
									action.data.start_delay
									&& action.data.start_delay.match(/^(\d{1,4}-?){3}$/) ?
										action.data.start_delay.split('-') : null,
								startAfter =
									action.data.start_delay
									&& action.data.start_delay.match(/^\d+$/) ?
									Number(action.data.start_delay)
									: NaN;

							form = subCase(
								{
									requestorName: get(ctrl.requestor(), 'instance.name'),
									phaseOptions: map(ctrl.phases(), ( phaseObj ) => {

										return {
											value: String(phaseObj.seq),
											label: phaseObj.name
										};
									}).reverse(),
									isLastPhase: find(ctrl.phases(), { id: action.data.zaaktype_status_id }) === last(ctrl.phases())
								}
							);

							values =
								values.merge({
									allocation: {
										unit: String(action.data.ou_id),
										role: String(action.data.role_id)
									},
									subcase_requestor_type: action.data.eigenaar_type,
									resolve_in_phase: action.data.required,
									type: action.data.relatie_type,
									requestor_type: action.data.eigenaar_searchtype,
									requestor: action.data.eigenaar_type === 'anders' && action.data.eigenaar_id ?
										{
											label: action.data.eigenaar_id,
											data: {
												id: action.data.eigenaar_id.match(/\d+$/)[0]
											}
										}
										: null,
									copy_attributes: Number(action.data.kopieren_kenmerken) === 1,
									automatic_assignment: Number(action.data.automatisch_behandelen) === 1,
									start_date: startDate ?
										new Date(
											Number(startDate[2]),
											Number(startDate[1]) - 1,
											Number(startDate[0])
										)
										: null,
									start_after: !isNaN(startAfter) ?
										startAfter
										: null
								});
							verb = 'Starten';

							reducers = reducers.concat( ( vals ) => {

								return {
									ou_id: get(vals.allocation, 'unit'),
									role_id: get(vals.allocation, 'role'),
									eigenaar_type: vals.subcase_requestor_type,
									required: vals.resolve_in_phase,
									relatie_type: vals.type,
									eigenaar_searchtype: vals.subcase_requestor_type === 'anders' ?
										vals.requestor_type
										: null,
									eigenaar_id: vals.subcase_requestor_type === 'anders' ?
										`betrokkene-${vals.requestor_type}-${get(vals.requestor, 'data.id')}`
										: null,
									kopieren_kenmerken: vals.copy_attributes ? 1 : 0,
									automatisch_behandelen: vals.automatic_assignment ? 1 : 0,
									start_delay: vals.type === 'vervolgzaak_datum' ?
										dateFilter(vals.start_date, 'dd-MM-yyyy')
										: (
											vals.type === 'vervolgzaak' ?
											String(vals.start_after)
											: null
										)

								};

							});
						}
						break;

						case 'template': {
							form = templateForm(
								{
									templates: seamlessImmutable(ctrl.templates()).asMutable( { deep: true }),
									caseDocuments: seamlessImmutable(ctrl.caseDocuments()).asMutable( { deep: true }) || []
								},
								{
									showTemplateSelect: false
								}
							);

							values = values.merge({
								template: action.data.bibliotheek_sjablonen_id,
								name: action.data.filename,
								filetype: action.data.target_format,
								case_document: action.data.bibliotheek_kenmerken_id
							});

							reducers = reducers.concat( ( vals ) => {

								return {
									filename: vals.name,
									target_format: vals.filetype,
									bibliotheek_kenmerken_id: vals.case_document
								};

							});

							verb = 'Aanmaken';
						}
						break;
						
						case 'email': {
							form = email(
								{
									requestorName: get(ctrl.requestor(), 'instance.name')
								},
								{
									showTemplateSelect: false
								}
							);

							values = values.merge({
								recipient_type: action.data.rcpt,
								email_subject: action.data.subject,
								email_content: action.data.body,
								recipient_cc: action.data.cc,
								recipient_bcc: action.data.bcc,
								recipient_address: action.data.rcpt === 'overig' ?
									action.data.email
									: null,
								recipient_medewerker: action.data.rcpt === 'behandelaar' && action.data.behandelaar ?
									{
										data: {
											id: Number(action.data.behandelaar.split('-')[2])
										},
										label: action.data.betrokkene_naam || action.data.behandelaar
									}
									: null,
								case_documents: action.data.case_document_attachments
							});

							verb = 'Versturen';

							reducers = reducers.concat(( vals ) => {
								return {
									rcpt: vals.recipient_type,
									subject: vals.email_subject,
									body: vals.email_content,
									cc: vals.recipient_cc,
									bcc: vals.recipient_bcc,
									email: vals.recipient_type === 'behandelaar' ?
										get(vals.recipient_medewerker, 'data.id')
										: (vals.recipient_type === 'overig' ? vals.recipient_address : null),
									betrokkene_naam: vals.recipient_type === 'behandelaar' ?
										get(vals.recipient_medewerker, 'label')
										: null,
									case_document_attachments: vals.case_documents.map(caseDoc => caseDoc.case_document_ids)
								};
							});
						}
						break;

						case 'allocation': {
							form = allocation(
								{
									
								}
							);

							values = values.merge({
								allocation: {
									unit: String(action.data.ou_id),
									role: String(action.data.role_id)
								}
							});

							reducers = reducers.concat( ( vals ) => {

								return {
									ou_id: vals.allocation.unit,
									role_id: vals.allocation.role
								};

							});


							verb = 'Wijzigen';
						}
						break;

						case 'subject': {

							form = subject(
								{
									name: action.data.naam,
									role: action.data.rol,
									magic_string_prefix: action.data.magic_string_prefix,
									is_authorized: action.data.gemachtigd ? 'Ja' : 'Nee',
									email_confirmation: action.data.notify ? 'Ja' : 'Nee'
								}
							);

						}
						break;

					}

					values = seamlessImmutable(form.getDefaults()).merge(values);
					fields = form.fields();

					locals = seamlessImmutable({ $values: values, expanded: false });

					validityReducer = composedReducer( { scope }, ( ) => values, ( ) => locals)
						.reduce( ( vals, loc ) => {

							let validation = vormValidator(fields, vals, null, loc);

							return validation;
						});

					ctrl.getFields = ( ) => fields;

					ctrl.getValues = ( ) => values;

					ctrl.handleChange = ( name, value ) => {

						values = form.processChange(name, value, values);

						locals = locals.merge({ $values: values }, { deep: true });

					};

					ctrl.handleSaveClick = ( ) => {
						ctrl.onActionSave({ $data: getData() });
					};

					ctrl.handleExecuteClick = ( ) => {
						ctrl.onActionExecute({ $data: getData() });
					};

					ctrl.getVerb = ( ) => verb;

					ctrl.isValid = ( ) => get(validityReducer.data(), 'valid');

					ctrl.getValidity = ( ) => get(validityReducer.data(), 'validations');

					ctrl.getLocals = ( ) => locals;

					ctrl.canSave = ( ) => {
						return ctrl.isValid();
					};

					ctrl.canExecute = ( ) => {
						return !action.automatic && ctrl.isValid();
					};

					ctrl.isExpanded = ( ) => locals.expanded;

					ctrl.canExpand = ( ) => form.getCapabilities().expand;

					ctrl.canEdit = ( ) => {

						let canEdit = form.getCapabilities().edit;

						if (canEdit === undefined) {
							return true;
						}

						return canEdit;
					};

					ctrl.handleExpandClick = ( ) => {
						locals = locals.merge({ expanded: !locals.expanded });
					};

					ctrl.getExpandLabel = ( ) => {

						return ctrl.isExpanded() ?
							'Verberg geavanceerde opties'
							: 'Toon geavanceerde opties';

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;

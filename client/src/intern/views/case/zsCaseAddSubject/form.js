import angular from 'angular';
import capitalize from 'lodash/capitalize';

export default ( magicStringResource, roles ) => {

	return {
		fields:
			[
				{
					name: 'relation_type',
					label: 'Relatietype',
					template: 'radio',
					data: {
						options: [
							{
								value: 'natuurlijk_persoon',
								label: 'Burger'
							},
							{
								value: 'bedrijf',
								label: 'Organisatie'
							}
						]
					},
					required: true
				},
				{
					name: 'related_subject',
					label: 'Betrokkene',
					template: 'object-suggest',
					data: {
						objectType: [ '$values', ( values ) => values.relation_type ]
					},
					required: true
				},
				{
					name: 'related_subject_role',
					label: 'Rol betrokkene',
					template: 'select',
					data: {
						options:
							roles ?
								roles
									.asMutable()
									.concat( { instance: { label: 'anders' } })
									.map(role => {
										return {
											value: capitalize(role.instance.label),
											label: capitalize(role.instance.label)
										};
									})
								: []
					}
				},
				{
					name: 'related_subject_role_freeform',
					label: 'Andere rol',
					template: 'text',
					when: [ '$values', ( values ) => values.related_subject_role === 'Anders' ],
					required: true
				},
				{
					name: 'magic_string_prefix',
					label: 'Magic string prefix',
					template: {
						inherits: 'text',
						display: ( el ) => {

							return angular.element(
								`<div>
									${el[0].outerHTML}
									<zs-spinner is-loading="vm.invokeData('pending')" class="spinner-tiny"></zs-spinner>
								</div>`
							);

						}
					},
					data: {
						pending: ( ) => magicStringResource.state() === 'pending'
					},
					disabled: true,
					required: true
				},
				{
					name: 'pip_authorized',
					label: 'Gemachtigd voor deze zaak',
					template: 'checkbox'
				},
				{
					name: 'notify_subject',
					label: 'Verstuur bevestiging per email',
					template: 'checkbox'
				}
			]
	};

};

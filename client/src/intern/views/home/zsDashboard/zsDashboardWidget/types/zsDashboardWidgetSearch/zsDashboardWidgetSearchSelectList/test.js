import angular from 'angular';
import 'angular-mocks';
import zsDashboardWidgetSearchSelectList from './index.js';
import savedSearchesServiceModule from './../../../../../../../../shared/ui/zsSpotEnlighter/savedSearchesService';
import shortid from 'shortid';

describe('zsDashboardWidgetSearchSelectList', ( ) => {

	let $rootScope,
		$compile,
		$httpBackend,
		$document,
		scope,
		apiCacher,
		savedSearchesService,
		el,
		ctrl;

	let setBackendResponse = ( response ) => {
		$httpBackend.expectGET(/\/api\/object\/search\//)
			.respond(response);

		$compile(el)(scope);

		ctrl = el.controller('zsDashboardWidgetSearchSelectList');
		
		$httpBackend.flush();
	};

	beforeEach(angular.mock.module(zsDashboardWidgetSearchSelectList, savedSearchesServiceModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', '$document', 'apiCacher', 'savedSearchesService', ( ...rest ) => {

		[ $rootScope, $compile, $httpBackend, $document, apiCacher, savedSearchesService ] = rest;

		el = angular.element('<zs-dashboard-widget-search-select-list on-search-select="handleSelect($search)"/>');

		$document.find('body').append(el);
		
		scope = $rootScope.$new();
		
		scope.handleSelect = jasmine.createSpy('select');

	}]));

	describe('without error', ( ) => {
		
		beforeEach( ( ) => {
			setBackendResponse(
				[
					{
						id: shortid(),
						label: 'foobar',
						values: {
							query: JSON.stringify({ 'zql': 'foo' })
						}
					}
				]
			);
		});

		it('should have a controller', ( ) => {

			expect(ctrl).toBeDefined();

		});

		it('should call handleSelect', ( ) => {

			let data = {
				title: 'foo',
				zql: ''
			};

			ctrl.handleItemSelect(data);

			expect(scope.handleSelect).toHaveBeenCalled();
			expect(scope.handleSelect).toHaveBeenCalledWith(jasmine.objectContaining(data));
			
		});

		it('should return the predefined searches', ( ) => {

			expect(ctrl.getPredefinedSearches().length).toBe(savedSearchesService.getPredefinedSearches().length);

		});

		it('should create data', ( ) => {

			expect(ctrl.getUserDefinedSearches().length).toBe(1);

		});

		it('should request new data when the query changes', ( ) => {

			ctrl.filterBy = 'foo';

			$httpBackend.expectGET(/.*?/)
				.respond([]);

			scope.$digest();

			$httpBackend.flush();

		});

		it('should return the resource', ( ) => {

			expect(ctrl.getResource()).not.toBeUndefined();

		});

	});


	describe('with error', ( ) => {
		
		beforeEach(angular.mock.inject([() => {

			setBackendResponse(400);

		}]));

		it('should only have the predefined searches', ( ) => {

			expect(ctrl.getUserDefinedSearches().length).toBe(0);

		});

	});

	afterEach( ( ) => {

		apiCacher.clear();

	});
});

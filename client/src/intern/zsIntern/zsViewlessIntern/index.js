import angular from 'angular';
import template from './../template.html';
import controller from './../controller';

export default angular.module('zsViewlessIntern', [
		controller
	])
		.directive('zsViewlessIntern', [ ( ) => {

			let tpl = template
				.replace(/<ui-view>.*?<\/ui-view>/, '');

			return {
				restrict: 'E',
				template: tpl,
				scope: {
					onOpen: '&',
					onClose: '&'
				},
				bindToController: true,
				controller,
				controllerAs: 'vm'
			};

		}])
		.name;

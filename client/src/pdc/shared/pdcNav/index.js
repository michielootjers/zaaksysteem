import angular from 'angular';
import template from './index.html';
import uiRouter from 'angular-ui-router';
import zsIcon from '../../../shared/ui/zsIcon';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import zsUiViewProgressModule from './../../../shared/ui/zsNProgress/zsUiViewProgress';
import get from 'lodash/get';
import pick from 'lodash/pick';
import shortid from 'shortid';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';

import './styles.scss';

export default
	angular.module('Zaaksysteem.pdc.pdcNav', [
		uiRouter,
		zsIcon,
		composedReducerModule,
		zsUiViewProgressModule,
		snackbarServiceModule
	])
		.directive('pdcNav', [ '$window', '$http', '$document', '$rootScope', '$state', '$timeout', 'composedReducer', 'zsStorage', ( $window, $http, $document, $rootScope, $state, $timeout, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					titleBind: '&',
					onButtonClick: '&',
					views: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						styleReducer,
						tabReducer,
						titleReducer;

					styleReducer = composedReducer( { scope: $scope }, ctrl.appConfig() )
						.reduce( config => {

							let style = {
								container: {
									'background-image': `url(${config.instance.interface_config.header_bgimage})`
								},
								bg: {
									'background-color': config.instance.interface_config.header_bgcolor
								}
							};

							return style;

						});

					tabReducer = composedReducer( { scope: $scope }, ctrl.appConfig() )
						.reduce( config => {

							return config.instance.interface_config.objects.map( object => {
								return {
									id: shortid(),
									label: object.objecttype_subject_attribute,
									object: object.objecttype
								};
							});

						});

					titleReducer = composedReducer( { scope: $scope }, ctrl.appConfig() )
						.reduce( ( config ) => {
							return config.instance.interface_config.title;
						});

					ctrl.getState = ( ) => $state.current.name;

					ctrl.getContainerStyle = ( ) => get(styleReducer.data(), 'container');

					ctrl.getBgContainerStyle = ( ) => get(styleReducer.data(), 'bg');

					ctrl.getViewportTitle = titleReducer.data;

					ctrl.getTabs = ( ) => tabReducer.data();

					$rootScope.$on('pdc.view.scroll', ( event, options ) => {
						
						let container = $element[0].querySelector('.pdc-nav__tabs-slidecontainer'),
							slider = container.querySelector('.pdc-nav__tabs-slide'),
							percentage = options.percentage,
							scaleX = 1 / ctrl.views().length,
							translateX = (percentage * scaleX) - (scaleX / 2);

						slider.style.transform = slider.style.webkitTransform = `translateX(${Math.round(translateX * 100)}%) scaleX(${scaleX})`;

						if (!angular.equals(
							pick(slider.style, 'transitionTimingFunction', 'transitionDuration'),
							pick(options, 'transitionTimingFunction', 'transitionDuration')
						)) {

							slider.style.transitionTimingFunction = options.transitionTimingFunction;
							slider.style.transitionDuration = options.transitionDuration;
						}

					});

				}],
				controllerAs: 'pdcNav'
			};
		}])
		.directive('zsScroll', ['$window', ($window) => {

			return {
				restrict: 'A',
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let el = $element[0],
						tabsEl = el.querySelector('.pdc-nav__tabs'),
						containerEl = el.querySelector('.pdc-nav__container'),
						titleEl = el.querySelector('.pdc-nav__title'),
						containerHeight = containerEl.clientHeight,
						tabsHeight = tabsEl.clientHeight,
						maxOpacity = 0.25,
						lastSet = NaN;

					let getMultiplier = ( ) => Math.max(0, Math.min(1, $window.pageYOffset / (containerHeight - tabsHeight)));

					let setDimensions = ( ) => {

						let multiplier = getMultiplier(),
							maxMargin = titleEl.clientWidth + parseFloat(titleEl.style.paddingLeft || 0) + parseFloat(titleEl.style.paddingRight || 0);

						tabsEl.style.marginRight = `${multiplier * maxMargin}px`;
					};

					let setStyle = ( ) => {

						let multiplier = getMultiplier(),
							opacity = (1 - multiplier) * maxOpacity;

						if (lastSet === multiplier) {
							return;
						}

						lastSet = multiplier;

						if (multiplier >= 1) {
							$element.addClass('small');
						} else {
							$element.removeClass('small');
						}

						el.querySelector('.pdc-nav__bg').style.opacity = opacity;

						setDimensions();

					};

					angular.element($window).bind('scroll', setStyle);
					angular.element($window).bind('resize', setDimensions);

					$scope.$$postDigest(setStyle);
					$scope.$$postDigest(setDimensions);

				}]
			};
		}])
		.name;

import angular from 'angular';
import 'angular-mocks';
import appServiceModule from '.';
import every from 'lodash/every';

describe('appService', ( ) => {

	let rules,
		appService,
		defaultState = {
			foo: 'bar'
		};

	beforeEach(( ) => {
		rules = [
			{
				actionName: 'foo_change',
				key: 'foo',
				reducer: jasmine.createSpy('reduce').and.callFake(( foo/*, actionData*/ ) => {
					return foo.split('').reverse().join('');
				})
			},
			{
				actionName: 'foo_change',
				key: '*',
				reducer: jasmine.createSpy('reduce').and.callFake(( state/*, actionData*/ ) => {
					return state.merge({
						foo: [ state.foo, state.foo ].join('')
					});
				})
			}
		];
	});

	beforeEach(angular.mock.module(appServiceModule, [ 'appServiceProvider', ( appServiceProvider ) => {

		appServiceProvider.setDefaultState(defaultState);

		rules.forEach(( rule ) => {
			appServiceProvider.reduce(rule.actionName, rule.key, rule.reducer);
		});

	}]));

	beforeEach(angular.mock.inject([ 'appService', ( ...rest ) => {

		[ appService ] = rest;

	}]));

	it('should return an immutable state', ( ) => {

		expect(appService.state()).toBeDefined();

		expect(appService.state()).toEqual(defaultState);
		
		expect(( ) => {
			appService.state().foo = 'foo';
		}).toThrow();

		expect(appService.state().foo).not.toBe('foo');

	});

	it('should allow the consumer to dispatch an action', ( ) => {

		expect(typeof appService.dispatch === 'function').toBe(true);

	});

	it('should call the listeners subscribed to an action', ( ) => {

		let listener = jasmine.createSpy('foo');

		appService.on('foo', listener);

		appService.dispatch('foo');

		expect(listener).toHaveBeenCalled();

	});

	describe('when dispatching an action', ( ) => {

		let data = { foo: 'bar' },
			state;

		beforeEach( ( ) => {
			
			state = appService.state();

			appService.dispatch('foo_change', data);

		});

		it('should call the registered reducers for that action', ( ) => {

			expect(every(rules, rule => rule.reducer.calls.count() > 0)).toBe(true);

		});

		describe('and state key is not given', ( ) => {

			it('should transform the entire state object', ( ) => {

				expect(rules[0].reducer).toHaveBeenCalled();

				expect(rules[0].reducer).toHaveBeenCalledWith(state[rules[0].key], data);

			});

		});

		describe('and state key is given', ( ) => {

			it('should only transform the part of the state that the reducer registered for', ( ) => {

				expect(rules[1].reducer).toHaveBeenCalled();

				expect(rules[1].reducer).toHaveBeenCalledWith(state, data);

			});

		});

	});

});

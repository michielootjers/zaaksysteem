import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormInvokeModule from './../../vormInvoke';
import template from './template.html';
import get from 'lodash/get';
import assign from 'lodash/assign';

export default
	angular.module('vorm.types.checkboxList', [
		vormTemplateServiceModule,
		vormInvokeModule
	])
		.directive('vormCheckboxList', [ 'vormInvoke', ( vormInvoke ) => {

			return {
				restrict: 'E',
				require: [ 'vormCheckboxList', 'ngModel' ],
				scope: {
					delegate: '&',
					templateData: '&'
				},
				template,
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this,
						ngModel;

					ctrl.link = ( controllers ) => {
						[ ngModel ] = controllers;
					};

					ctrl.handleCheckboxClick = ( value ) => {
						let newModelValue = assign({}, ngModel.$modelValue, { [value]: !ctrl.isChecked(value) });

						ngModel.$setViewValue(newModelValue, 'click');
					};

					ctrl.isChecked = ( value ) => !!get(ngModel.$modelValue, value, false);

					ctrl.getOptions = ( ) => vormInvoke(ctrl.templateData().options);


				}],
				controllerAs: 'vormCheckboxList',
				link: ( scope, element, attrs, controllers ) => {

					controllers.shift().link(controllers);

				}
			};

		}])
		.run([ 'vormTemplateService', function ( vormTemplateService ) {
			
			const el = angular.element(
				'<vorm-checkbox-list ng-model></vorm-checkbox-list>'
			);
			
			vormTemplateService.registerType('checkbox-list', {
				control: el,
				display: angular.element(
					`<ul>
						<li ng-repeat="(key, value) in delegate.value">
							{{::key}}
						</li>
					</ul>`
				)
			});
			
		}])
		.name;

import isArray from 'lodash/isArray';
import mapValues from 'lodash/mapValues';
import emailRegex from 'email-regex';
import iban from 'iban';

export default
	( attributes, values, messages = { } ) => {

		return mapValues(
			attributes,
			attr => {

				let required = attr.required,
					type = attr.type,
					value = values[attr.magic_string];

				if (!isArray(value)) {
					value = [ value ];
				} else if (value.length === 0) {
					value = [ null ];
				}

				return value.map(
					( val, index ) => {

						let isEmpty = val === null || val === undefined || val === '',
							validation = {};

						if (isEmpty && required && index === 0) {
							validation.required = messages.required || true;
						}

						switch (type) {
							
							case 'bankaccount':

							if (!isEmpty && !iban.isValid(val)) {
								validation.bankaccount = messages.bankaccount || true;
							}
							
							break;

							case 'email':
							if (!isEmpty && !emailRegex({ exact: true }).test(val)) {
								validation.email = messages.email || true;
							}
							break;

							case 'numeric':
							if (!isEmpty && isNaN(Number(val))) {
								validation.number = messages.number || true;
							}
							break;

						}

						return Object.keys(validation).length > 0 ? validation : null;

					}
				);

			}
		);

	};

import extend from 'extend';
import Emitter from 'quill/core/emitter';
import BaseTheme, { BaseTooltip } from 'quill/themes/base';
import LinkBlot from 'quill/formats/link';
import { Range } from 'quill/core/selection';

// This is an adaptation of the default Quill 'Snow' theme. It is currently unused.
// Once Quill gets better support for themes, this could be used again.

const TOOLBAR_CONFIG = [
	[{ header: ['1', '2', '3', false] }],
	['bold', 'italic', 'underline', 'link'],
	[{ list: 'ordered' }, { list: 'bullet' }],
	['clean']
];

class ZSSnowTooltip extends BaseTooltip {
	constructor(quill, bounds) {
		super(quill, bounds);
		this.preview = this.root.querySelector('a.ql-preview');
	}

	listen() {
		super.listen();

		let tooltip = this;

		// on action add
		tooltip.root.querySelector('a.ql-action').addEventListener('click', function(event) { // eslint-disable-line
			if (tooltip.root.classList.contains('ql-editing')) {
				tooltip.save();
			} else {
				tooltip.edit('link', tooltip.preview.textContent);
			}
			event.preventDefault();
		});

		// on action remove
		tooltip.root.querySelector('a.ql-remove').addEventListener('click', function(event) { // eslint-disable-line
			if (tooltip.linkRange !== null) {
				tooltip.restoreFocus();
				tooltip.quill.formatText(tooltip.linkRange, 'link', false, Emitter.sources.USER);
				delete tooltip.linkRange;
			}
			event.preventDefault();
			tooltip.hide();
		});

		// on selection change
		this.quill.on(Emitter.events.SELECTION_CHANGE, function(range) { // eslint-disable-line

			// if no range is selected
			if (range === null) {
				return;
			}

			// if range length is 0, try to get a link, if there is a link on the cursor position, show preview box
			if (range.length === 0) {

				// The following line causes a problem. tooltip.quill.scroll.descendant(LinkBlot, range.index) won't obtain a value.
				// See https://stackoverflow.com/questions/41131547/building-custom-quill-editor-theme?noredirect=1#comment69470899_41131547
				// for more info.
				let [link, offset] = tooltip.quill.scroll.descendant(LinkBlot, range.index);
				
				if (link !== null) {

					tooltip.linkRange = new Range(range.index - offset, link.length());
					
					let preview = LinkBlot.formats(link.domNode);

					tooltip.preview.textContent = preview;
					tooltip.preview.setAttribute('href', preview);
					tooltip.show();
					tooltip.position(tooltip.quill.getBounds(tooltip.linkRange));

					return;
				}
			} else {
				delete tooltip.linkRange;
			}
			tooltip.hide();
		});
	}

	show() {
		super.show();
		this.root.removeAttribute('data-mode');
	}
}

class ZSSnowTheme extends BaseTheme {
	constructor(quill, options) {
		if (options.modules.toolbar !== null && options.modules.toolbar.container === null) {
			options.modules.toolbar.container = TOOLBAR_CONFIG;
		}
		super(quill, options);
	}

	extendToolbar(toolbar) {
		this.tooltip = new ZSSnowTooltip(this.quill, this.options.bounds); //eslint-disable-line
		if (toolbar.container.querySelector('.ql-link')) {
			this.quill.keyboard.addBinding({ key: 'K', shortKey: true }, function(range, context) { //eslint-disable-line
				toolbar.handlers['link'].call(toolbar, !context.format.link); //eslint-disable-line
			});
		}
	}
}

ZSSnowTheme.DEFAULTS = extend(true, {}, BaseTheme.DEFAULTS, {
	modules: {
		toolbar: {
			handlers: {
				link: function(value) { // eslint-disable-line

					if (value) {

						let range = this.quill.getSelection(),
							tooltip,
							preview;

						if (range === null || range.length === 0) {
							return;
						}

						preview = this.quill.getText(range);

						if (/^\S+@\S+\.\S+$/.test(preview) && preview.indexOf('mailto:') !== 0) {
							preview = `mailto:${preview}`;
						}

						tooltip = this.quill.theme.tooltip;

						tooltip.edit('link', preview);

					} else {
						this.quill.format('link', false);
					}
				}
			}
		}
	}
});

ZSSnowTooltip.TEMPLATE =
`<span class="title">
  Bezoek link
</span>
<a class="ql-preview" target="_blank" href="about:blank"></a>
<input class="ql-input" type="text" />
<a class="ql-action">
  <i class="mdi mdi-check"></i>
  Invoegen
</a>
<a class="ql-remove">
  <i class="mdi mdi-close"></i>
  Sluiten
</a>`;


export default ZSSnowTheme;

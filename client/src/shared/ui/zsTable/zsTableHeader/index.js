import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsTableHeader', [
	])
		.directive('zsTableHeader', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					columns: '&',
					onColumnClick: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.handleColumnClick = ( column ) => {
						ctrl.onColumnClick({ $columnId: column.id });
					};

				}],
				controllerAs: 'zsTableHeader'
			};

		}])
		.name;

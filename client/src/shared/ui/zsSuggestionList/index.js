import angular from 'angular';
import template from './template.html';
import controller from './controller';

import zsKeyboardNavigableList from '../zsKeyboardNavigableList';

export default
	angular.module('shared.ui.zsSuggestionList', [
			zsKeyboardNavigableList
		])
		.directive('zsSuggestionList', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					keyInputDelegate: '&',
					suggestions: '&',
					onSelect: '&',
					label: '@'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					controller.call(this, $scope);

				}],
				controllerAs: 'zsSuggestionList'
			};

		}])
		.name;

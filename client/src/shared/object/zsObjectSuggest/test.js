import angular from 'angular';
import 'angular-mocks';
import SomaEvents from 'soma-events';

import zsObjectSuggestModule from '.';

describe('zsObjectSuggest', ( ) => {

	let $rootScope,
		$compile,
		$httpBackend,
		$document,
		scope,
		el,
		ctrl,
		type = 'casetype';

	let createEvent = ( eventType, options = { contains: true } ) => {

		let event = new SomaEvents.Event(eventType),
			related;

		if (options.contains) {
			related = el.children().eq(1)[0];
		} else {
			related = null;
		}

		event.relatedTarget = related;

		return event;

	};

	beforeEach(angular.mock.module(zsObjectSuggestModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', '$document', ( ...rest ) => {

		[ $rootScope, $compile, $httpBackend, $document ] = rest;

		scope = $rootScope.$new();

		el = angular.element(`<zs-object-suggest data-type="'${type}'" on-select="handleSelect($object)"/>`);

		$compile(el)(scope);

		ctrl = el.controller('zsObjectSuggest');


	}]));

	it('should have a controller', ( ) => {

		expect(ctrl).toBeDefined();

	});

	it('should return model options', ( ) => {

		expect(ctrl.getModelOptions()).toBeDefined();

	});

	describe('when activated with a query', ( ) => {

		let data =
			[
				{
					object: {
						zs_object: {
							id: 1,
							label: 'label'
						}
					}
				}
			];

		beforeEach( ( ) => {

			ctrl.query = 'foo';

		});

		describe('when resolved', ( ) => {

			beforeEach( ( ) => {

				$httpBackend.expectGET(/.*/)
					.respond(data);

				ctrl.open();

				scope.$digest();

			});

			it('should fetch data', ( ) => {

				expect($httpBackend.flush).not.toThrow();

			});

			it('should fetch new data when the query changes', ( ) => {

				$httpBackend.flush();

				ctrl.query = 'bar';

				$httpBackend.expectGET(/.*/)
					.respond(data);

				scope.$digest();

				$httpBackend.flush();


			});

			it('should cancel requests', ( ) => {

				expect(ctrl.isOpen()).toBe(true);

				ctrl.close();

				expect(ctrl.isOpen()).toBe(false);

				scope.$digest();

				$httpBackend.verifyNoOutstandingRequest();

			});

			it('should return the suggestions', ( ) => {

				expect(ctrl.getSuggestions().length).toBe(0);

				$httpBackend.flush();

				expect(ctrl.getSuggestions().length).toBe(data.length);

			});

			it('should clear the query and call the onSelect handler', ( ) => {

				let spy = jasmine.createSpy('select'),
					object = { foo: 'bar' };

				scope.handleSelect = spy;

				ctrl.handleSelect({ data: object });

				expect(spy).toHaveBeenCalled();
				expect(spy).toHaveBeenCalledWith(jasmine.objectContaining({ data: object }));

				expect(ctrl.query).not.toBe('foo');

			});

			it('should close after a select', ( ) => {

				ctrl.handleSelect( { data: 'foo' });

				expect(ctrl.isOpen()).toBe(false);

			});

			it('should re-open if the input is clicked or gains focus', ( ) => {

				ctrl.handleSelect( { data: 'foo' });

				expect(ctrl.isOpen()).toBe(false);

				el.find('input').triggerHandler('click');

				expect(ctrl.isOpen()).toBe(true);

			});

		});

		describe('when rejected', ( ) => {

			beforeEach( ( ) => {

				$httpBackend.expectGET(/.*?/)
					.respond(400);

				ctrl.open();

				$httpBackend.flush();

			});

			it('should return an error message', ( ) => {

				expect(ctrl.getStateLabel()).toBe('Resultaten konden niet worden opgehaald.');

			});

		});

		describe('when pending', ( ) => {

			beforeEach( ( ) => {

				$httpBackend.expectGET(/.*?/)
					.respond(200, data);

				ctrl.query = 'foo';

				ctrl.open();

				scope.$digest();

			});

			it('should return no message', ( ) => {

				expect(ctrl.getStateLabel()).toBe('');

			});

			afterEach( ( ) => {
				$httpBackend.flush();
			});

		});

	});


	describe('when gaining focus', ( ) => {

		beforeEach( ( ) => {

			ctrl.open();

			scope.$digest();

		});

		describe('from an outside element', ( ) => {

			it('should activate', ( ) => {

				let event = createEvent('focusin', { contains: false });

				spyOn(ctrl, 'open');

				el[0].dispatchEvent(event);

				expect(ctrl.open).toHaveBeenCalled();

			});

		});

		describe('from a child element', ( ) => {

			it('should not activate', ( ) => {

				let event = createEvent('focusin', { contains: true });

				spyOn(ctrl, 'open');

				el[0].dispatchEvent(event);

				expect(ctrl.open).not.toHaveBeenCalled();


			});

		});

	});

	describe('when losing focus', ( ) => {

		beforeEach( ( ) => {

			ctrl.open();

			scope.$digest();

		});

		describe('to an outside element', ( ) => {

			it('should deactivate', ( ) => {

				let event = createEvent('focusout', { contains: false });

				spyOn(ctrl, 'close');

				el[0].dispatchEvent(event);

				expect(ctrl.close).toHaveBeenCalled();

			});

		});

		describe('to a child element', ( ) => {

			it('should not deactivate', ( ) => {

				let event = createEvent('focusout', { contains: true });

				spyOn(ctrl, 'close');

				el[0].dispatchEvent(event);

				expect(ctrl.close).not.toHaveBeenCalled();

			});

		});

	});

	describe('on mousedown', ( ) => {

		let event;

		beforeEach( ( ) => {
			event = new SomaEvents.Event('mousedown');

			ctrl.open();

			scope.$digest();

		});

		describe('outside', ( ) => {

			it('should close', ( ) => {

				$document[0].dispatchEvent(event);

				expect(ctrl.isOpen()).toBe(false);
 
			});

		});

		describe('inside', ( ) => {

			it('should not close', ( ) => {

				el.find('ul')[0].dispatchEvent(event);

				expect(ctrl.isOpen()).toBe(true);

			});

		});

	});

	afterEach(( ) => {

		scope.$destroy();

	});

});

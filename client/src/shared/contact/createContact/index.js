import angular from 'angular';
import template from './template.html';
import vormFieldsetModule from './../../vorm/vormFieldset';
import inputModule from './../../vorm/types/input';
import selectModule from './../../vorm/types/select';
import radioModule from './../../vorm/types/radio';
import seamlessImmutable from 'seamless-immutable';
import resourceModule from '../../api/resource';
import vormValidatorModule from '../../vorm/util/vormValidator';
import get from 'lodash/get';
import first from 'lodash/head';
import composedReducerModule from '../../api/resource/composedReducer';
import keyBy from 'lodash/keyBy';
import mapKeys from 'lodash/mapKeys';
import keys from 'lodash/keys';

export default
	angular.module('createContact', [
			inputModule,
			selectModule,
			radioModule,
			vormFieldsetModule,
			resourceModule,
			vormValidatorModule,
			composedReducerModule
		])
		.directive('createContact', [ '$http', '$httpParamSerializerJQLike', '$q', '$compile', 'resource', 'composedReducer', 'vormValidator', 'snackbarService', ( $http, $httpParamSerializerJQLike, $q, $compile, resource, composedReducer, vormValidator, snackbarService ) => {

			return {
				restrict: 'E',
				scope: {
					isPristine: '&',
					onClose: '&'
				},
				template,
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						valid = false,
						pristine = true,
						fields,
						citFields,
						corrFields,
						fieldReducer,
						persDomesticAddrFields,
						persForeignAddrFields,
						orgDomesticAddrFields,
						orgForeignAddrFields,
						orgFields,
						values = seamlessImmutable({
							'betrokkene_type': 'natuurlijk_persoon',
							'np-landcode': '6030',
							'vestiging_landcode': '6030',
							'rechtsvorm' : '01'
						}),
						countriesResource,
						legalEntitiesResource;

					countriesResource = resource({
						url: '/api/v1/general/country_codes',
						params: {
							rows_per_page: 400
						}
					}, {
						scope,
						cache: {
							every: 30 * 10000
						}
					})
						.reduce( ( requestOptions, data ) => {
							return ( data || seamlessImmutable([]) )
								.map( country => {
									return seamlessImmutable({
										label: get(country, 'instance.label'),
										value: get(country, 'instance.dutch_code')
									});
								})
								.asMutable({ deep: true });
						});

					legalEntitiesResource = resource({
						url: '/api/v1/general/legal_entity_types',
						params: {
							rows_per_page: 60
						}
					}, {
						scope,
						cache: {
							every: 30 * 10000
						}
					})
						.reduce( ( requestOptions, data ) => {

							return ( data || seamlessImmutable([]) )
								.map( legalEntityType => {
									return seamlessImmutable({
										label: get(legalEntityType, 'instance.label'),
										value: get(legalEntityType, 'instance.code')
									});
								})
								.asMutable({ deep: true });
						});

					fields =
						[
							{
								name: 'betrokkene_type',
								label: 'Type betrokkene',
								template: 'radio',
								data: {
									options: [
										{
											value: 'natuurlijk_persoon',
											label: 'Burger'
										},
										{
											value: 'bedrijf',
											label: 'Organisatie'
										}
									]
								},
								required: true
							}
						];

					citFields =
						[
							{
								name: 'np-burgerservicenummer',
								label: 'BSN',
								template: 'text',
								required: false,
								data: {

								}
							},
							{
								name: 'np-voornamen',
								label: 'Voornamen',
								template: 'text',
								required: false
							},
							{
								name: 'np-voorvoegsel',
								label: 'Voorvoegsel',
								template: 'text',
								required: false
							},
							{
								name: 'np-geslachtsnaam',
								label: 'Achternaam',
								template: 'text',
								required: true
							},
							{
								name: 'np-geslachtsaanduiding',
								label: 'Geslacht',
								template: 'radio',
								data: {
									options: [
										{
											value: 'M',
											label: 'Man'
										},
										{
											value: 'V',
											label: 'Vrouw'
										}
									]
								},
								required: true
							},
							{
								name: 'np-landcode',
								label: 'Land',
								template: 'select',
								data: {
									options: [ countriesResource.data ]
								},
								required: true
							}
						];

					corrFields =
						[
							{
								name: 'np-correspondentie_straatnaam',
								label: 'Correspondentie straat',
								template: 'text',
								required: true
							},
							{
								name: 'np-correspondentie_huisnummer',
								label: 'Correspondentie huisnummer',
								template: 'text',
								required: true
							},
							{
								name: 'np-correspondentie_huisnummertoevoeging',
								label: 'Correspondentie huisnummer toevoeging',
								template: 'text',
								required: false
							},
							{
								name: 'np-correspondentie_postcode',
								label: 'Correspondentie postcode',
								template: 'text',
								required: true
							},
							{
								name: 'np-correspondentie_woonplaats',
								label: 'Correspondentie woonplaats',
								template: 'text',
								required: true
							}
						];

					persDomesticAddrFields = [
							{
								name: 'briefadres',
								label: 'Briefadres',
								template: 'checkbox',
								data: {
									checkboxLabel: 'Ja'
								},
								required: false
							},
							{
								name: 'np-straatnaam',
								label: 'Straat',
								template: 'text',
								required: ( ) => !get(values, 'briefadres', false)
							},
							{
								name: 'np-huisnummer',
								label: 'Huisnummer',
								template: 'text',
								required: ( ) => !get(values, 'briefadres', false)
							},
							{
								name: 'np-huisnummertoevoeging',
								label: 'Huisnummer toevoeging',
								template: 'text',
								required: false
							},
							{
								name: 'np-postcode',
								label: 'Postcode',
								template: 'text',
								required: ( ) => !get(values, 'briefadres', false)
							},
							{
								name: 'np-woonplaats',
								label: 'Woonplaats',
								template: 'text',
								required: ( ) => !get(values, 'briefadres', false)
							},
							{
								name: 'np-in_gemeente',
								label: 'Binnengemeentelijk',
								template: 'checkbox',
								data: {
									checkboxLabel: 'Ja'
								},
								required: false
							}
						];

					persForeignAddrFields =
						[
							{
								name: 'np-adres_buitenland1',
								label: 'Adresregel 1',
								template: 'text',
								required: true
							},
							{
								name: 'np-adres_buitenland2',
								label: 'Adresregel 2',
								template: 'text',
								required: false
							},
							{
								name: 'np-adres_buitenland3',
								label: 'Adresregel 3',
								template: 'text',
								required: false
							}
						];

					orgFields =
						[
							{
								name: 'vestiging_landcode',
								label: 'Vestiging land',
								template: 'select',
								data: {
									options: [ countriesResource.data ]
								},
								required: true
							},
							{
								name: 'rechtsvorm',
								label: 'Rechtsvorm',
								template: 'select',
								data: {
									options: [ legalEntitiesResource.data ]
								},
								required: true,
								when: [ '$values', ( vals ) => vals.vestiging_landcode === '6030' ]
							},
							{
								name: 'dossiernummer',
								label: 'KVK-nummer',
								template: 'text',
								required: true,
								when: [ '$values', ( vals ) => vals.vestiging_landcode === '6030' ]
							},
							{
								name: 'vestigingsnummer',
								label: 'Vestigingsnummer',
								template: 'text',
								required: false,
								when: [ '$values', ( vals ) => vals.vestiging_landcode === '6030' ]
							},
							{
								name: 'handelsnaam',
								label: 'Handelsnaam',
								template: 'text',
								required: true
							}
						];

					orgDomesticAddrFields =
						[
							{
								name: 'vestiging_straatnaam',
								label: 'Vestiging straat',
								template: 'text',
								required: true
							},
							{
								name: 'vestiging_huisnummer',
								label: 'Vestiging huisnummer',
								template: 'text',
								required: true
							},
							{
								name: 'vestiging_huisletter',
								label: 'Vestiging huisletter',
								template: 'text',
								required: false
							},
							{
								name: 'vestiging_huisnummertoevoeging',
								label: 'Vestiging toevoeging',
								template: 'text',
								required: false
							},
							{
								name: 'vestiging_postcode',
								label: 'Vestiging postcode',
								template: 'text',
								required: true
							},
							{
								name: 'vestiging_woonplaats',
								label: 'Vestiging woonplaats',
								template: 'text',
								required: true
							}
						];

					orgForeignAddrFields =
						[
							{
								name: 'vestiging_adres_buitenland1',
								label: 'Vestiging Adresregel 1',
								template: 'text',
								required: true
							},
							{
								name: 'vestiging_adres_buitenland2',
								label: 'Vestiging Adresregel 2',
								template: 'text',
								required: false

							},
							{
								name: 'vestiging_adres_buitenland3',
								label: 'Vestiging Adresregel 3',
								template: 'text',
								required: false
							}
						];

					fieldReducer = composedReducer( { scope }, ( ) => values)
						.reduce( ( vals ) => {

							let visibleFields = fields.concat();

							if (vals.betrokkene_type === 'natuurlijk_persoon') {
								visibleFields = visibleFields.concat(citFields);

								if (vals['np-landcode'] === '6030') {
									visibleFields = visibleFields.concat(persDomesticAddrFields);
								} else {
									visibleFields = visibleFields.concat(persForeignAddrFields);
								}

								if (vals.briefadres) {
									visibleFields = visibleFields.concat(corrFields);
								}

							} else if (vals.betrokkene_type === 'bedrijf') {
								visibleFields = visibleFields.concat(orgFields);

								if (vals.vestiging_landcode === '6030') {
									visibleFields = visibleFields.concat(orgDomesticAddrFields);
								} else {
									visibleFields = visibleFields.concat(orgForeignAddrFields);
								}
							}

							return visibleFields;

						});

					ctrl.isPristine = ( ) => pristine;

					ctrl.values = ( ) => values;

					ctrl.isValid = ( ) => valid.valid;

					ctrl.getValidity = ( ) => valid.validations;

					ctrl.handleChange = ( name, value ) => {

						pristine = false;

						values = values.merge({ [name]: value });

						valid = vormValidator(ctrl.getFields(), values);

					};

					ctrl.getFields = fieldReducer.data;

					ctrl.handleSubmit = ( ) => {

						let params = {
								'confirmed' : 1,
								'do_validation' : 1
							},
							promise;

						params = values.merge( params );

						promise = $http({
									url: '/betrokkene/0/bewerken',
									method: 'GET',
									params
							})
							.then(( response ) => {

								let result = response.data.json,
									success = (result.success === '1');

								if (!success) {
									return $q.reject(result);
								}

								// On successful validation from backend, redo request
								params = params.without('do_validation');

								return $http({
									url: '/betrokkene/0/bewerken',
									method: 'GET',
									params
								});

							});

						ctrl.onClose({ $promise: promise });

						snackbarService.wait('Contactpersoon wordt aangemaakt', {
							promise,
							then: ( result ) => {

								let message = get(result, 'data.json.bericht'),
									url = first(message.match(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/) );

								return {
									message: 'Het contact is aangemaakt.',
									timeout: 90000,
									actions: [ { type: 'link', label: 'Contact bekijken', link: url } ]
								};

							},
							catch: ( result ) => {

								let errorMessages = [],
									fieldsByName = keyBy(
										ctrl.getFields(),
										'name'
									);

								errorMessages = keys(
													mapKeys( get(result, 'msgs'), ( value, key ) => {
														return `<strong>${fieldsByName[key].label}</strong>: ${value}`;
													})
												);
								
								return `Het contact kon niet worden aangemaakt. <ul><li>${errorMessages.join('</li><li>')}</ul>`;
							}

						});

					};

				}],
				controllerAs: 'vm'
			};
		}])
		.name;
